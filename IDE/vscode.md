




## COMPARAISON ENTRE 2 FICHIERS DANS VSCODE   

Sélectionner 2 fichiers dans VSCode (touche `ctrl`)           
Clic D: Compare selected            
![vscodeCompare01](./images/vscodeCompare01.png)          

S'affiche la comparaison des 2 fichiers             
![vscodeCompare02](./images/vscodeCompare02.png)
