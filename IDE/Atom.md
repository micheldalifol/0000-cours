# ATOM

## GENERALITES
Atom est un IDE. Il est possible de le connecter à un git, de faire des appels APIs et de lancer un terminal.
Il comporte de très nombreux packages et a une grosse communauté.

[La documentation officielle](https://atom.io/docs) est partagée en 3 grandes parties:
* documentation générale
* utilisation des APIs
* communauté

## INSTALLATION DE ATOM
### Installation sur MacOSX
Utilisation du gestionaire de packages Homebrew

```bash
brew install atom
```
Ceci permet d'installer proprement **atom** ainsi que **apm** qui est le gestionnaire de packages d'atom.
Il est donc possible d'installer tous les paquets nécessaires via `apm <commande> <package>`

### Installation des packages atom
L'ensemble des packages atom sont [référencés dans cette page](https://atom.io/packages). En selectionnant un package voir avez la description de celui-ci ainsi que la commande apm d'installation.
Il est aussi possible de passer par l'interface graphique d'atom ("la palette")  

#### Utilisation de **apm**
Nous avons l'ensemble des commandes **apm** de la manière suivante:

```bash
dali@dali ~/home % apm

apm - Atom Package Manager powered by https://atom.io

Usage: apm <command>

where <command> is one of:
    ci, clean, config, dedupe, deinstall, delete, dev, develop, disable, docs,
    enable, erase, featured, home, i, init, install, link, linked, links, list,
    ln, lns, login, ls, open, outdated, prune, publish, rebuild,
    rebuild-module-cache, remove, rm, search, show, star, starred, stars, test,
    uninstall, unlink, unpublish, unstar, update, upgrade, view.

Run `apm help <command>` to see the more details about a specific command.

Options:
  --color        Enable colored output                                      [booléen] [défaut: true]
  -v, --version  Print the apm version
  -h, --help     Print this usage message

  Prefix an option with `no-` to set it to false such as --no-color to disable
  colored output.
```

Il est donc très simple de faire:
* une recherche: `apm search <package>`
* une installation: `apm install <package>`
* une desinstallation: `apm deinstall <package>`
* une nettoyage: `apm outdated`
* un update: `apm update <package>`
* un upgrade: `apm upgrade <package>`

#### Installation automatisée
Le plus intéressante et facile est de créer un script d'installation qui va installer atom avec la commande `brew install atom` et de faire un script qui va installer tous les thèmes et packages nécessaires au bon fonctionnement d'Atom avec la commande `apm install <theme | package>`
