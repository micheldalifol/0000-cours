# PPS APPELS API
serveur pps : https://pleasantpps.fr
[documentation](https://pleasantpasswords.com/info/pleasant-password-server/m-programmatic-access/restful-api/restful-api-v6)

PPS (Pleasant Password) est un serveur qui contient les logins et mots de passes d'une société par exemple.
Il peut être utilisé par appels API si cela est autorisé dans sa configuration.
Pour cela aller dans Settings/General/Enable Allow User Access to be modified via API
La version utilisée est la v6.


### USER ACCESS
#### GET
##### Liste des niveaux d'accès système
```http
https://pleasantpps.fr/api/v6/rest/accesslevels
```

>affiche la liste complete de toutes les permissions

##### Récupère les niveaux d'accès qui ont accès à l'entrée ou au dossier demandé
###### Entry
```http
https://pleasantpps.fr/api/v6/rest/entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/accesslevels
```

###### Folder
```http
https://pleasantpps.fr/api/v6/rest/folders/cd52d662-0771-4eb6-9309-d12aff60b6bb/accesslevels
```


##### Liste les utilisateurs qui ont accès à l'entrée ou au dossier
###### Entry
```http
https://pleasantpps.fr/api/v6/rest/entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/useraccess
```


###### Folder
```http
https://pleasantpps.fr/api/v6/rest/folders/cd52d662-0771-4eb6-9309-d12aff60b6bb/useraccess
```


#### POST
##### Update l'accès utilisateur d'une entrée ou d'un dossier
###### Entry
```bash
https://pleasantpps.fr/api/v6/rest/entries/ae47feae-13db-4f14-85ae-a42ce10b9a90/useraccess
{
	"UserId": "788017e9-0ee0-460e-8de4-abb5016f65c5",
	"RoleId": "",
	"ZoneId": "",
	"PermissionSetId": "6fe3319c-21f0-48b0-a274-22fcca660de3",
	"AccessExpiry": "2020-12-31"
}
```


#### DELETE
##### Delete l'accès utilisateur d'une entrée ou d'un dossier
###### Entry
```bash
https://pleasantpps.fr/api/v6/rest/entries/ae47feae-13db-4f14-85ae-a42ce10b9a90/useraccess/4bd6b778-8933-4fb4-8586-abbc0140b9e2
{ "Action": "Archive", "Comment": "Usage Comment" }
```


###### Folder
```bash

```


### FOLDERS
#### GET
##### Récupère l'ID du dossier root
```http
https://pleasantpps.fr/api/v6/rest/folders/root
```

##### récupère l'intégralité de l'arborescence visible d'un user
```http
https://pleasantpps.fr/api/v6//folders
```

##### récupère l'arborescence d'un dossier avec ses sous-dossiers et ses entrées
```http
https://pleasantpps.fr/api/v6/rest/folders/cd52d662-0771-4eb6-9309-d12aff60b6bb?recurseLevel=0
```
-> recurseLevel=0 par défaut à la valeur 1

#### POST
##### ajoute un dossier au systeme
```bash
https://pleasantpps.fr/api/v6/rest/folders
{
    "CustomUserFields": {},
    "CustomApplicationFields": {},
    "Children": [],
    "Credentials": [],
    "Tags": [],
    "Name": "New",
    "ParentId": "cd52d662-0771-4eb6-9309-d12aff60b6bb",
    "Notes": null,
    "Expires": null
}
```


#### PATCH
##### Mise à jour partielle du dossier
```bash
https://pleasantpps.fr/api/v6/rest/folders/cd52d662-0771-4eb6-9309-d12aff60b6bb
{
	"Name": "Updated Group Name"
}
```

#### PUT
##### Mise à jour complete du dossier
```bash
https://pleasantpps.fr/api/v6/rest/folders/1cd52d662-0771-4eb6-9309-d12aff60b6bb
{
	"Item":
	{
		"CustomUserFields": {},
		"CustomApplicationFields": {},
		"Children": [],
		"Credentials": [],
		"Tags": [],
		"Id": "1cceb0dc-8b20-4b11-bf2d-556c9292496a",
		"Name": "Update",
		"ParentId": "c04f874b-90f7-4b33-97d0-a92e011fb712",
		"Notes": null,
		"Expires": null
	},
	"Comment": "Usage Comment"
}
```

#### DELETE
##### suppression d'un dossier
```bash
https://pleasantpps.fr/api/v6/rest/Folders/d599dd2c-f8f2-4638-9091-5ae66d8962bd
{
	"Action": "Delete",
	"Comment": "Usage Comment"
}
```

### ENTRIES
#### GET
##### récupère une seule entrée
```http
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd
```

##### récupére un password à une entrée
```http
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/Password
```

##### récupère les pièces jointes jointes d'une entrée
```http
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/Attachments
```

##### récupère une seule pièce jointe d'une entrée
```http
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/Attachments/ee20446c-3f5e-419b-9d8c-a93600fe401f
```

  -> ee20446c-3f5e-419b-9d8c-a93600fe401f: ID de la pièce jointe à récupérer



#### POST
##### Ajoute une nouvelle entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries
{
    "CustomUserFields": {},
    "CustomApplicationFields": {},
    "Tags": [],
    "Name": "New Entry API Name",
    "Username": "MyUserName",
    "Password": "MyPassword01",
    "Url": "",
    "Notes": "",
    "GroupId": "cd52d662-0771-4eb6-9309-d12aff60b6bb",
    "Expires": null
}
```

##### ajouter un password à une entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/Password
{
	"Comment": "Usage Comment"
}
```

##### Ajoute de nouvelles pièces jointes à une entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/Attachments
{
    "CredentialObjectId": "34307337-6b90-4b6f-bc1d-3dba50fa9cbd",
    "FileName": "added4.txt",
    "FileData": "UGFzc01h"
}
```

#### PATCH
##### Update partielle d'une seule entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries/4307337-6b90-4b6f-bc1d-3dba50fa9cbd
{
	"Name": "Updated Name"
}
```


#### PUT
##### Update complete d'une seule entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries/4307337-6b90-4b6f-bc1d-3dba50fa9cbd
{
	"Item":
	{
	    "CustomUserFields": {},
	    "CustomApplicationFields": {},
	    "Tags": [],
	    "Id": "13caaa57-2186-467e-b107-bc29f8c3533e",
	    "Name": "Full Update",
	    "Username": "ABCDEF",
	    "Password": null,
	    "Url": "",
	    "Notes": "",
	    "GroupId": "c04f874b-90f7-4b33-97d0-a92e011fb712",
	    "Expires": null
	},
	"Comment": "Usage Comment"
}
```

##### Ajoute des pièces jointes à une entrée
```bash
https://pleasantpps.fr/api/v6rest/Entries/13caaa57-2186-467e-b107-bc29f8c3533e/Attachments
[
	 {
        "CredentialObjectId": "13caaa57-2186-467e-b107-bc29f8c3533e",
        "AttachmentId": "3BF23EF5-1888-40C2-B727-EEFC522CFD74",
        "FileName": "updates9.txt",
        "FileData": "UllYNg==",
        "FileSize": 4
    },
	{
        "CredentialObjectId": "13caaa57-2186-467e-b107-bc29f8c3533e",
        "AttachmentId": "00000000-0000-0000-0000-000000000000",
        "FileName": "added2.txt",
        "FileData": "UllYNg==",
        "FileSize": 4
    }
]
```

##### Met à jour une pièce jointe d'une entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries/34307337-6b90-4b6f-bc1d-3dba50fa9cbd/Attachments/ee20446c-3f5e-419b-9d8c-a93600fe401f
{
    "CredentialObjectId": "4307337-6b90-4b6f-bc1d-3dba50fa9cbd",
    "AttachmentId": "ee20446c-3f5e-419b-9d8c-a93600fe401f",
    "FileName": "updates2.txt",
    "FileData": "UGFzc01h",
}
```

  -> ee20446c-3f5e-419b-9d8c-a93600fe401f: ID de la pièce jointe à récupérer


#### DELETE
##### Supprimer une entrée
```bash
https://pleasantpps.fr/api/v6/rest/Entries/4307337-6b90-4b6f-bc1d-3dba50fa9cbd
{
	"Action": "Delete",
	"Comment": "Usage Comment"
}
```
