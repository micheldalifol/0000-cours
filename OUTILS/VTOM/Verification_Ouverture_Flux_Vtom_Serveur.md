# Vérifier si un flux entre vtom et un serveur est ouvert

## A partir du serveur vtom vers la cible

```bash
>/dev/tcp/a-<serveurCible>.<domaine>/<portUtiliseVTom>
```

## A partir de la cible vers vtom

```bash
>/dev/tcp/<serveur>.<domaine>/<portUtiliseVTom>
```

**Si la main est rendue, le port est ouvert**
