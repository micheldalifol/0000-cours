# PROTOCOLE UDP


## DEFINITION

**UDP** est un autre protocole réseau par-dessus IP, tout comme TCP.

Il s'agit d'un protocole non connecté : un socket UDP ne représente pas une liaison entre la machine locale et une machine distante, mais un point d'échange permettant la réception depuis plusieurs sources et l'envoi à plusieurs destinataires.

En utilisant UDP, nous ne manipulons plus un flux de données, mais ce qu'on appelle des datagrammes. Le datagramme est notre unité d'envoi, le terme de paquet UDP peut aussi apparaître et correspond généralement à la même chose.

## FONCTIONNEMENT DU PROTOCOLE UDP

UDP est un protocole de transport en mode non connecté et surtout non fiable. Sa seule garantie est que si le datagramme arrive à destination, il arrive entièrement et intact, sans aucune altération ni partiellement. Par contre il peut ne jamais arriver, ou en plusieurs exemplaires et les datagrammes reçus peuvent être désordonnés par rapport à leur envoi : si vous envoyez plusieurs datagrammes A, B et C dans cet ordre, ils pourraient arriver désordonnés B, C, A (en plus de pouvoir être manquants ou dupliqués ce qui donnerait à la réception C, C, B ou encore B, C, B, A, A et toutes les autres possibilités imaginables). Et tout comme il est impossible de savoir si un datagramme a été remis, il est impossible de savoir si le destinataire existe, et s'il est toujours présent et valide ou non.


## POURQUOI UTILISER UDP

Imaginez que vous envoyez régulièrement, à chaque frame, votre position à une machine distante.
Seule la donnée la plus récente vous intéresse : sa position la plus à jour.
Dans un tel cas, perdre un datagramme est un moindre mal et préférable à ce que le client se concentre sur le renvoi d'une donnée que l'on sait obsolète. Le datagramme est perdu : tant pis, on est déjà en train d'envoyer le suivant qui devrait lui arriver.

Un en-tête TCP varie de 20 à 60 octets selon les options, tandis qu'un en-tête UDP fait toujours 8 octets.

=> Tout ceci (maîtriser les délais, les pertes et la taille des données envoyées) fait qu'implémenter son protocole de fiabilité en utilisant UDP est souvent préférable à utiliser TCP si vous envoyez des données critiques et dont l'intérêt n'est qu'immédiat ou très court (seule la toute dernière position est intéressante). Et puisque tous deux sont des protocoles qui transfèrent leurs données via IP en interne, ce n'est pas une idée si farfelue qu'il y parait à première vue et c'est effectivement totalement faisable.



## MANIPULER UN SOCKET UDP

La manipulation d'un socket UDP est assez simple.
    -> En effet, notre socket permet basiquement deux actions : envoyer des données, et en recevoir. Il n'y a plus de connexion à effectuer ou à accepter, et, tant que le socket est ouvert, aucune erreur d'envoi ou réception ne devrait survenir.
    => Le seul prérequis à l'utilisation d'un socket UDP est de créer le socket.
