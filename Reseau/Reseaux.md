# RESEAUX


## DEFINITIONS

Le reseau permet l'echange d'informations au format numerique entre plusieurs systemes informatiques.

Un reseau numérique est constitué de plusieurs ordinateurs reliées entre eux physiquement par des liaisons.

Un reseau numerique permet l'echange de donnees entre machines distantes, qui sont reliees de liaison en liaison par des machines intermediaires si necessaire.


## DOMAINES D'UTILISATION DES RESEAUX

* permettre le partage des ressources
* accroitre la resistance aux pannes
* diminuer les couts
* acces à des services distants (db, applications, ...)
* communication (mail, teleconf, ...)


## ECHANGER LES INFORMATIONS

On utilise pour cela des protocoles:
- pour les applications
- pour transporter/router l'information
- pour émettre l'information sur 1 support physique

Ces protocoles doivent gérer:
- les erreurs
- la fragmentation/assemblage de donnees

Ils sont normalises.


## TRANSMISSION

* Mode connecté: TCP
* IP: Internet Protocol
>    principe de paquets: on decoupe les infos en petits morceaux de longueur variables avec des infos sur l'emetteur et le destinataire

Format du paquet IP:

            supervision: 8 octets
            adresse source: 4 octets
            adresse dest: 4 octets
            donnees: x octets

Format du paquet Ethernet:

            adresse dest: 6 octets
            adresse source: 6 octets
            supervision: 2 octets
            donnees: x octets
            supervision: 4 octets


Format du paquet ATM:

            header: 5 octets
            information: 48 octets




## ARCHITECTURE DES RESEAUX

Les reseaux ont une architecture en couches
**norme OSI**: Open Systems Interconnection

diviser pour mieux regner
ensemble de couches superposées les unes sur les autres

voir image modele-OSI.png

- couche 1: physique
    support physique (ex: pc) -> fournit le moyen physique de transmettre des donnees
- couche 2: liaisons de donnees
    elle utilise la couche 1.
    gestion des liaisons de donnees
    detection et reprise sur erreur
    procedure de transmission
- couche 3: reseau
    fournit les moyens d'établir, libérer maintenir des connexions entre des systemes ouverts
    fonctionnalités: adressage; routage; controle de flux
    mode connecté/non-connecté
- couche 4: transport
    independance des reseaux sous-jacents
    accepte les donnees de la couche session
    optimise les ressources reseau
    fonctionnalité de bout-en-bout
    dependance des reseaux:QoS
    protocoles de transport: TCP; UDP
- couche 5: session
    responsable de la synchronisation
    fonction de type:gestion dialogue; port reprise...
    orchestration
    gestion des transactions
- couche 6: presentation
    s'interesse à la syntaxe et la semantique des infos
- couche 7: application
    moyen d'acceder à l'environnement OSI
    echange les infos par l'intermediaire des entites d'application
    (terminal, mail, ...)


## EXEMPLE TCP/IP

TCP/IP est structuré en 4 couches:
- liens: interface avec le reseau
- reseau(IP): gere la circulation des paquets en assurant le routage
- transport: assure la communication de bout-en-bout entre les machines source et dest et leurs intermediaires par les protocoles TCP (Transmission Control Protocol) et UDP (User Datagram Protocol)
- application: programmes utilisateurs(ftp; telnet; smtp...)
