# SCANNER LES PORTS

## DEFINITION

> Un port est l'adresse d'une application sur 1 machine.
[liste des ports logiciels](https://fr.wikipedia.org/wiki/Liste_de_ports_logiciels)

## QUELS PORTS SONT OUVERTS SUR MA MACHINE

```bash
netstat -an      #liste les ports ouverts
netstat -anpe    #pour avoir des détails sur le PID, le processus et le user
netstat -antp    #liste les ports tcp
```

>Avec un protocole TCP chaque connexion doit être établie avant d'avoir la moindre donnée.
- **LISTEN**, l'application est en écoute et en attente de requêtes de la part de clients sur Internet.
- **ESTABLISHED**, l'application a établi la communication suite à une demande de requête et on considère donc la connexion comme établie.


## FONCTIONNEMENT D'UNE CONNEXION UDP  

Admettons qu'on ait une application qui fonctionne avec le protocole udp sur le port 53, on le voit avec:
```bash
netstat -an | grep udp
#OU
tcpdump -n -i eth0 udp and port 53

#  -> Si le port est ouvert, l'application répond en UDP
#  -> Si le port est fermé, l'application répond avec un message d'erreur ICMP port unreachable
```

## FONCTIONNEMENT D'UNE CONNEXION TCP

`tcpdump -n -i eth0 port http`

La connexion est établie en 3 temps:
- Envoi d'un segment avec le flag **SYN** (S dans le premier paquet)
- Réponse d'un segment avec les flags **SYN et ACK** de positionnés (S et ack dans le second paquet)
- Réponse enfin d'un segment avec le flag **ACK** (ack positionné, mais sans le S !)
 -> Si le port est ouvert, réponse avec un segment SYN + ACK
 -> Si le port est fermé, réponse avec un segment RST






## OUTIL QUI SCANNE TOUTES LES PLAGES DE PORTS: [nmap](https://nmap.org/man/fr/man-port-scanning-techniques.html)


[nmap guide de référence](https://nmap.org/man/fr/index.html)
* nmap: Network Mapper
* nmap est un outil qui permet de scanner de façon automatique toute une plage de ports, toute une plage d'adresses;

```bash
nmap -sS <ipAdress>     #scanne tous les ports de la machine dont l'adresse est <ipAdress>
#-sS      => je fais un scan avec le segment SYN (seulement les ports qui utilisent tcp)
nmap -sU <ipAdress>      #scanne tous les ports qui utilisent udp            
nmap -sT <ipAdress>      #scanne les ports qui utilisent tcp (quand SYN n'est pas utilisable)
nmap -sV <ipAdress>      #active la détection de version
nmap -sP <ipAdress>		 #scanne tous les hosts sur le reseau via un ping
#Remarques:
#- si <ipAdress>:  un ordinateur unique ex : 192.168.1.1
#- si <ipAdress>:  un « network » complet ex : 192.168.1.0/24
#- si <ipAdress>:  une plage ou range d’un réseau ex : 192.168.1.1-255
```
