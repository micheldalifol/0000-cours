#!/bin/bash
sudo apt-get update && apt-get upgrade -y
sudo apt-get install apache2 unzip -y
sudo systemctl enable apache2
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install --bin-dir /usr/local/bin --install-dir /usr/local/aws-cli --update
chmod 777 /var/www/html/index.html
echo '<html><h1>Bootstrap Demo</h1><h3>Availibilty Zone: ' > /var/www/html/index.html 
curl http://169.254.169.254/latest/meta-data/placement/availability-zone >> /var/www/html/index.html 
echo '<h3>Instane ID: ' >> /var/www/html/index.html 
curl http://169.254.169.254/latest/meta-data/instance-id >> /var/www/html/index.html 
echo '<h3>Public IP: ' >> /var/www/html/index.html 
curl http://169.254.169.254/latest/meta-data/public-ipv4 >> /var/www/html/index.html 
echo '<h3>Local IP: ' >> /var/www/html/index.html 
curl http://169.254.169.254/latest/meta-data/local-ipv4 >> /var/www/html/index.html 
echo '</h3></html> ' >> /var/www/html/index.html
sudo apt-get install mysql-server -y
sudo systemctl enable mysql