#!/bin/bash

# update the instance
yum update -y

# install apache service
yum install httpd -y

# start apache service
service httpd start

# configure apache
cd /var/www/html
echo "<html><body><h1>Hello Cloud Gurus</h1></body></html>" > index.html