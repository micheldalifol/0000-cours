# DOCUMENTATION OFFICIELLE AWS LAMBDA

Exécutez du code sans vous soucier des serveurs ou des clusters

___________________________________________________________________________________________________      

## LAMBDA  

[Documentation AWS Officielle: Lambda](https://aws.amazon.com/fr/lambda/)          


`AWS Lambda` est un service de calcul qui exécute votre code en réponse à des événements et gère automatiquement les ressources de calcul, ce qui en fait le moyen le plus rapide de transformer une idée en une application moderne, de production et serverless       

- **Pas de gestion de serveurs** : Exécutez du code sans avoir à allouer une infrastructure ou à devoir en gérer une. Écrivez et chargez uniquement du code sous forme de fichier .zip ou d'image de conteneur            

- **Autoscalling**: Répondez automatiquement à des demandes d'exécution de code, allant de douzaines d'événements par jour à des centaines de milliers par seconde, et ce, à n'importe quelle échelle       

- **Tarification à l'usage**: Économisez en payant uniquement pour le temps de calcul utilisé (en millisecondes) plutôt que pour une infrastructure d'allocation en cas de pic de capacité        

- **Optimisation des performances**: Optimisez le temps et les performances d'exécution de code avec la taille de mémoire des fonctions appropriée. Répondez à des pics de demandes à deux chiffres en millisecondes avec la simultanéité allouée        

- **Traitez rapidement des données à grande échelle**: Répondez à une demande imprévisible et gourmande en ressources pour monter en puissance instantanément et passer à plus de 18 000 vCPU. Créez des flux de traitement rapidement et facilement grâce à une suite d'autres offres serverless et de déclencheurs d'événements         

- **Exécutez des backends Web et mobile interactifs**: Vous pouvez combiner AWS Lambda à d'autres services AWS pour créer des expériences en ligne sûres, stables et évolutives        

- **Obtenez des ML Insights puissants**: Traitez les données au préalable avant de les fournir à votre modèle de machine learning (ML). Avec l'accès Amazon Elastic File System (EFS), AWS Lambda s'occupe de la gestion et de l'allocation de l'infrastructure afin de simplifier la mise à l'échelle               

- **Créez des applications d'événements**: Créez des fonctions d'événements pour une communication facile entre les services découplés. Réduisez les coûts en exécutant des applications pendant les périodes de pic de demande sans crash ou allocation excédentaire des ressources           


________________________________________________________________________________________________________________________        

## LAMBDA - CAS D'UTILISATION



Lambda est un service de calcul idéal pour les scénarios d'application qui doivent augmenter la capacité rapidement, et la réduire à zéro lorsqu'elle n'est pas demandée. Par exemple, vous pouvez utiliser Lambda pour :

 - **Traitement de fichiers** : utilisez Amazon Simple Storage Service (Amazon S3) pour déclencher le traitement des données Lambda en temps réel après un chargement.

 - **Traitement des flux** : utilisez Lambda et Amazon Kinesis pour traiter des données de flux en temps réel pour le suivi de l'activité des applications, le traitement des ordres de transaction, l'analyse du flux de clics, le nettoyage des données, le filtrage des logs, l'indexation, l'analyse des réseaux sociaux, la télémétrie des données des appareils de l'Internet des objets (IoT) et les métriques.

 - **Applications Web** : associez Lambda à d'autres AWS services pour créer de puissantes applications Web qui évoluent automatiquement à la hausse ou à la baisse et s'exécutent dans une configuration hautement disponible sur plusieurs centres de données.

 - **Backends IoT** : créez des backends serverless à l'aide de Lambda pour gérer les demandes Web, mobiles, IoT et API tierces. 

 - **Backends mobiles** : créez des backends à l'aide de Lambda et API Amazon Gateway pour authentifier et traiter les demandes. API AWS Amplify Utilisez-le pour intégrer facilement vos interfaces iOS, Android, Web et React Native.

Lorsque vous utilisez Lambda, vous n'êtes responsable que de votre code. Lambda gère le parc informatique qui offre un équilibre entre mémoire-CPU, réseau et autres ressources pour exécuter votre code. Étant donné que Lambda gère ces ressources, vous ne pouvez ni vous connecter à des instances de calcul, ni personnaliser le système d'exploitation sur les runtimes fournis. Lambda accomplit des activités opérationnelles et d'administration en votre nom, dont la gestion de la capacité, la surveillance et la journalisation de vos fonctions Lambda



________________________________________________________________________________________________________________________        

## LAMBDA - FONCTIONS PRINCPALES

Les fonctions clés suivantes vous aident à développer des applications Lambda évolutives, sécurisées et facilement extensibles       

- **[Variables d'environnement](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/configuration-envvars.html)**: Utilisez des variables d'environnement pour ajuster le comportement de votre fonction sans mettre à jour le code        

- **[Versions](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/configuration-versions.html)**: Gérez le déploiement de vos fonctions avec des versions, afin que, par exemple, une nouvelle fonction puisse être utilisée pour les tests bêta sans affecter les utilisateurs de la version de production stable      

- **[Images de conteneur](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/images-create.html)**: Créez une image de conteneur pour une fonction Lambda en utilisant une image de base AWS fournie ou une image de base alternative afin de pouvoir réutiliser vos outils de conteneur existants ou déployer des charges de travail plus importantes qui reposent sur des dépendances importantes, telles que le machine learning     

- **[Couches](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/chapter-layers.html)**: Empaquetez les bibliothèques et autres dépendances pour réduire la taille des archives de déploiement et rendre plus rapide le déploiement de votre code   

- **[Extensions Lambda](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/runtimes-extensions-api.html)**: Complétez vos fonctions Lambda avec des outils de surveillance, d'observabilité, de sécurité et de gouvernance    

- **[Fonction URLs](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/urls-configuration.html)**: Ajoutez un point de terminaison HTTP (S) dédié à votre fonction Lambda        

- **[Streaming des réponses](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/configuration-response-streaming.html)**: Configurez votre fonction Lambda URLs pour qu'elle renvoie les charges utiles de réponse aux clients à partir des fonctions Node.js, afin d'améliorer les performances du délai jusqu'au premier octet (TTFB) ou de renvoyer des charges utiles plus importantes     

- **[Contrôles de simultanéité et de mise à l'échelle](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/lambda-concurrency.html)**: Contrôlez avec précision la mise à l'échelle et la réactivité de vos applications de production      

- **[Signature de code](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/configuration-codesigning.html)**: Vérifiez que seuls les développeurs approuvés publient du code non modifié et fiable dans vos fonctions Lambda    

- **[Réseaux privés](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/configuration-vpc.html)**: Créez un réseau privé pour les ressources telles que les bases de données, les instances de mémoire cache ou les services internes        

- **[Accès au système de fichiers](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/configuration-filesystem.html)**: Configurez une fonction pour monter un Amazon Elastic File System (Amazon EFS) dans un répertoire local, afin que votre code de fonction puisse accéder aux ressources partagées et les modifier en toute sécurité et avec une simultanéité élevée         

- **[Lambda SnapStart pour Java](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/snapstart.html)**: Améliorez les performances de démarrage pour les exécutions Java jusqu'à 10 fois sans coût supplémentaire, généralement sans modification du code de votre fonction     



________________________________________________________________________________________________________________________        

## LAMBDA - CREATION DE FONCTIONS


===========================================================================================================================          

### LAMBDA - CREATION DE FONCTIONS - AVEC LA CONSOLE AWS


 - **Pré-requis**: avec un accès qui permet la création et modification d'un fonction lambda       


- **Creation de la fonction depuis la console AWS**:     

 1. Aller dans le service `Lambda`         
 2. Sélectionner `Create Function`        
 3. Sélectionner `Author from scratch`        
 4. Renseigner un nom pour la fonction (exemple: `mdtest20240909`)     
 5. Dans `Runtime` choississez `Python x.xx`
 6. Laissez l'architecture en `x86_64` puis créer la fonction


Par défaut, lambda crée un fonction `Hello World` dans le langage sélectionné en "5."         
![aws_lambda-functionCreated](../images/docOfficielle/aws_lambda-functionCreated.png)          

Lambda crée également unrôle d'exécution pour votre événement. Un rôle d'exécution est un rôle AWS Identity and Access Management (IAM) qui accorde à une fonction Lambda l'autorisation d'accès AWS services et de ressources. Pour votre fonction, le rôle créé par Lambda accorde des autorisations de base pour CloudWatch écrire dans Logs

Dans l'exemple le role créé est le suivant: `service-role/mdtest20240909-role-bzusts43`
 - qui a comme trust relationship: 
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
```

et comme policy: **AWSLambdaBasicExecutionRole-b382661c-1213-4cc5-8a85-02f239a1c9aa**   
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:<region>:<accountID>:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:<region>:<accountID>:log-group:/aws/lambda/mdtest20240909:*"
            ]
        }
    ]
}
```




- **Modification de la fonction avec le code souhaité**: faire un copié/collé du code souhaité à la place de la fonction `Hello World`     


- **Déployer la fonction pour qu'elle soit utilisable à travers Lambda**:           
![aws_lambda-functionDeploy](../images/docOfficielle/aws_lambda-functionDeploy.png)                      




===========================================================================================================================          

### LAMBDA - INVOQUER LA FONCTION LAMBDA - AVEC LA CONSOLE AWS


Ppur invoquer une fonction lambda il faut d'abord créer un évènement à envoyer à la fonction           
Un évènement est un fichier json contenant au moins 2 parires clé-valeur             

![aws_lambda-function_eventCreation](../images/docOfficielle/aws_lambda-function_eventCreation.png)                

Sauver l'évènement    

Si vous cliquez sur `Test` le résultat de l'évènement va s'afficher

Exemple: les valeurs de l'évènement sont 
```json
{
  "length": 6,
  "width": 7
}
```
Le resultat de celui-ci sera
```bash
Test Event Name
myTestEvent

Response
"{\"area\": 42}"

Function Logs
START RequestId: 2d0b1579-46fb-4bf7-a6e1-8e08840eae5b Version: $LATEST
The area is 42
[INFO]	2023-08-31T23:43:26.428Z	2d0b1579-46fb-4bf7-a6e1-8e08840eae5b	CloudWatch logs group: /aws/lambda/myLambdaFunction
END RequestId: 2d0b1579-46fb-4bf7-a6e1-8e08840eae5b
REPORT RequestId: 2d0b1579-46fb-4bf7-a6e1-8e08840eae5b	Duration: 1.42 ms	Billed Duration: 2 ms	Memory Size: 128 MB	Max Memory Used: 39 MB	Init Duration: 123.74 ms

Request ID
2d0b1579-46fb-4bf7-a6e1-xxxx
```





===========================================================================================================================          

________________________________________________________________________________________________________________________        

# FIN