# DOCUMENTATION OFFICIELLE AWS FIREHOSE

Chargez de manière fiable des flux en temps réel dans des lacs de données, des entrepôts de données et des services analytiques    

___________________________________________________________________________________________________      

## DATA FIREHOSE  

[Documentation AWS Officielle: Firehose](https://aws.amazon.com/fr/firehose/)          


Capturez, transformez et chargez facilement des données de streaming. Créez un flux de diffusion, sélectionnez la destination et commencez à diffuser des données en temps réel en quelques clics     
Allouez et mettez à l'échelle automatiquement les ressources de calcul, de mémoire et de réseau sans administration permanente     
Transformez les données de streaming non traitées dans des formats tels que `Apache Parquet` et partitionnez dynamiquement les données de streaming sans besoin de créer vos propres pipelines de traitement     
Connectez-vous à plus de 30 services AWS entièrement intégrés et à des destinations de streaming telles que `Amazon S3` et `Amazon Redshift`


`Amazon Data Firehose` constitue le moyen le plus simple d’acquérir, de transformer et de diffuser des flux de données en quelques secondes vers des data lakes, des entrepôts de données et des services d’analytique.     
Pour utiliser `Amazon Data Firehose`, vous devez configurer un flux avec une source, une destination et les transformations requises.     
`Amazon Data Firehose` traite le flux en continu, le met à l’échelle automatiquement en fonction de la quantité de données disponibles et le diffuse en quelques secondes       

**Source**:             
  Sélectionnez la source de votre flux de données, par exemple une rubrique dans `Amazon Managed Streaming for Kafka` (MSK), un flux dans `Kinesis Data Streams`, ou écrivez des données à l’aide de l’`API Firehose Direct PUT`.     
  `Amazon Data Firehose` est intégré à plus de 20 services AWS.      
  Vous pouvez donc configurer un flux à partir de sources telles qu’`Amazon CloudWatch Logs`, les logs d’`ACL Web AWS WAF`, les logs d’`AWS Network Firewall`, `Amazon SNS` ou `AWS IoT`         



**Transformation des données (faculutatif)**:           
 Indiquez si vous souhaitez convertir votre flux de données dans des formats tels que `Parquet` ou `ORC`, décompresser les données, effectuer des transformations de données personnalisées à l’aide de votre propre fonction `AWS Lambda` ou partitionner dynamiquement les enregistrements d’entrée en fonction d’attributs à diffuser à différents emplacements



**Destination**:         
 Sélectionnez une destination pour votre flux, telle qu’`Amazon S3`, `Amazon OpenSearch Service`, `Amazon Redshift`, `Splunk`, `Snowflake` ou un point de terminaison HTTP personnalisé



![aws_firehose-diagram](../images/docOfficielle/aws_firehose-diagram.png)                 





=======================================================================================================================        

### DATA FIREHOSE  - CONCEPTS 


[Documentation AWS - Amazon Data Firehose](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/what-is-this-service.html)                      


Lorsque vous débutez avec `Amazon Data Firehose`, vous pouvez tirer parti de la compréhension des concepts suivants.

**Stream Firehose**                   
  Vous utilisez `Amazon Data Firehose` en créant un flux Firehose, puis en lui envoyant des données.

**Enregistrer**          
  Les données présentant un intérêt que votre producteur de données envoie à un flux Firehose. Un enregistrement peut atteindre 1000 Ko.

**Producteur de données**               
  Un serveur Web qui envoie des données de log à un flux Firehose est un producteur de données. Vous pouvez également configurer votre flux Firehose pour lire automatiquement les données d'un flux de données Kinesis existant et les charger dans des destinations


**Taille de la mémoire tampon et intervalle entre la mémoire tampon**            
 `Amazon Data Firehose` met en mémoire tampon les données de streaming entrantes jusqu'à une certaine taille ou pendant une certaine période avant de les diffuser vers les destinations



=======================================================================================================================        

### DATA FIREHOSE  - COMPRENDRE LE FLUX DE DONNEES DANS AMAZON DATA FIREHOSE 


**Vers Amazon s3**:         
  Pour les destinations `Amazon S3`, les données de streaming sont délivrées à votre bucket S3. Si la transformation de données est activée, vous pouvez éventuellement sauvegarder les données source dans un autre bucket `Amazon S3`

  ![aws_firehose-streamS3-diagram](../images/docOfficielle/aws_firehose-streamS3-diagram.png)                       


**Vers Amazon Redshift**:     
  Pour les destinations `Amazon Redshift`, les données de streaming sont d'abord délivrées à votre bucket S3. `Amazon Data Firehose` émet ensuite une commande `Amazon **COPY** Redshift` pour charger les données de votre bucket S3 vers votre cluster `Amazon Redshift`. Si la transformation de données est activée, vous pouvez éventuellement sauvegarder les données source dans un autre bucket `Amazon S3`         

  ![aws_firehose-stream_redshift-diagram](../images/docOfficielle/aws_firehose-stream_redshift-diagram.png)                    


**Vers Opensearch**:                
  Pour les destinations de `OpenSearch` service, les données de streaming sont transmises à votre cluster de `OpenSearch` services et peuvent éventuellement être sauvegardées simultanément dans votre bucket S3

  ![aws_firehose-stream-opensearch-diagram](../images/docOfficielle/aws_firehose-stream-opensearch-diagram.png)            



**Vers Splunk**:             
  Pour les destinations `Splunk`, les données de streaming sont remises à `Splunk` et peuvent éventuellement être sauvegardées dans votre bucket S3 simultanément           

  ![aws_firehose-stream-splunk-diagram](../images/docOfficielle/aws_firehose-stream-splunk-diagram.png)                     



=======================================================================================================================        
___________________________________________________________________________________________________      

## DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE 


=======================================================================================================================  

### DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE - 1 - CHOISIR LA SOURCE ET LA DESTINATION 


[Doc AWS](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/create-name.html)           

Vue dans la console pour une creation d'un firehose stream     
![aws_firehose-stream-creation](../images/docOfficielle/aws_firehose-stream-creation.png)                 


| Type de source | Use cases (services, agents...) |
| :------: | :----- |
| **Direct PUT**  | AWS SDK, Lambda, Cloudwatch Logs, Cloudwatch events, IOT, EventBridge, SES, SNS, WAF, API Gateway, Pinpoint, Route53 Resolver, Fluentbit, Fluend, Snowflake, ...  |
| **Amazon Kinesis Data** | qui utilise un flux de données Kinesis en source |
| **Amazon MSK** | Amason MSK |


Pour les destinations, la liste déroulantes des destinations possibles: S3, Redshift, Opensearch, Datadog, Dynatrace, Snowflake, Splunk....


=======================================================================================================================  

### DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE - 2 - CONFIGURER LA SOURCE 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/configure-source.html)           


Au niveau de la source définir les parametres pour avoir en cible firehose   

[Exemple avec cloudwatch Logs](./aws_cloudwatch.md#cloudwatch-log-groups---subscription-filters--log-groups---amazon-data-firehose)





=======================================================================================================================  

### DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE - 3 - CONFIGURER LA TRANSFORMATION 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/create-transform.html)           



![aws_firehose-stream-transformation](../images/docOfficielle/aws_firehose-stream-transformation.png)               




=======================================================================================================================  

### DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE - 4 - CONFIGURER LA DESTINATION 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/create-destination.html)           



**Cas d'un s3**:           

![aws_firehose-stream-s3DestinationSettings01](../images/docOfficielle/aws_firehose-stream-s3DestinationSettings01.png)            
![aws_firehose-stream-s3DestinationSettings02](../images/docOfficielle/aws_firehose-stream-s3DestinationSettings02.png)            








=======================================================================================================================  

### DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE - 5 - CHOISIR LA SUVEGARDE 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/create-configure.html)           









=======================================================================================================================  

### DATA FIREHOSE : CREATION D'UN STREAM FIREHOSE - 6 - CONFIGURER LA MEMOIRE TAMPON 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/firehose/latest/dev/buffering-hints.html)           











=======================================================================================================================  

___________________________________________________________________________________________________      

# FIN  
