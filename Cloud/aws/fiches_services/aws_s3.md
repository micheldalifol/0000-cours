# DOCUMENTATION OFFICIELLE AWS S3

Ce fichier est fait à partir de la documentation officielle AWS + des tutos sur les sujets qui ne sont pas clairs        
Il est renseigné en fonction des besoins liés aux différents projets          

___________________________________________________________________________________________________  

## S3: SIMPLE STORAGE SERVICE  

[Documentation AWS Officielle: S3](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/GetStartedWithS3.html)          


=============================================================================    

### S3 : DEFINITIONS 


Amazon S3 travaille avec des **buckets** et des **objets**      
Un **bucket** est un **conteneur d'objets**     
Un **objet** est **1 fichier avec toutes les méta données qui le décrivent**   

Pour stocker 1 objet il faut:    
 - créer 1 bucket      
 - télécharger l'objet dans ce bucket       

Lorsqu'1 objet est dans 1 bucket on peut:         
 - ouvrir l'objet     
 - télécharger l'objet        
 - déplacer l'objet          

Une fois l'objet ou le bucket "inutile" on peut nettoyer les ressources     

=============================================================================    

### S3 : CONFIGURATION D'AMAZON S3 

................................................................................    

#### S3 : CONFIG - CREER UN S3 BUCKET

1. Se connecter à la console AWS           
2. Choisir S3 dans les services          
3. Créer un bucket      
  ➡️ la page de creation du bucket s'ouvre      
4. Définir un nom **unique** de bucket       
  ➡️ être unique dans 1 partition      
   - 1 partition est 1 regroupement de regions (3 actuellement): `aws`, `aws-cn` (Chine), `aws-us-gov` (GovCloudUS)     
   - entre 3 et 63 caractères                
   - uniquement des caractères minuscules, chiffres, points(.) et traits d'unions (-)            
   - commencer et terminer par 1 lettre ou 1 chiffre         
5. Définir la région d'appartenance du bucket    
 ➡️ limite de latence, couts, législations....    
 ➡️ 1 objet d'1 région ne la quitte jamais SAUF si transfert explicite     
![aws_s3_createBucket_generalConfig](../images/docOfficielle/aws_s3_createBucket_generalConfig.png)   

6. Partie **Objet Ownership**: pour déscactiver ou activer les listes ACL et controler la propriété des objets dans le bucket    
  ➡️ **Listes ACL désactivées**: propriétaire du bucket appliqué (par defaut)     
   le owner du bucket a le controle total sur chaque objet      
   CE QUI EST RECOMMANDE sauf des cas particuliers     
  ➡️**Listes ACL activées**:          
   - **bucket owner preferred**: le owner du bucket possède les nouveaux objets que d'autres écrivent avec la liste ACL `bucket-owner-full-control`    
   - **objet writer**: le compte qui télécharge 1 objet est propriétaire de cet objet    
![aws_s3_createBucket_ObjetOwnership](../images/docOfficielle/aws_s3_createBucket_ObjetOwnership.png)         

7. Partie **Block Public Access settings for this bucket**: pour bloquer les accès public au bucket     
  ➡️ recommandé de conserver bloqué     
![aws_s3_createBucket_blockPublicAccess](../images/docOfficielle/aws_s3_createBucket_blockPublicAccess.png) 

8. Partie **Bucket versioning** (Facultatif): permet de mettre en place le versioning des objets (cela est payant)     
![aws_s3_createBucket_versioning](../images/docOfficielle/aws_s3_createBucket_versioning.png)          

9. Partie **Tags** (Facultatif): possibilité d'ajouter des tags qui sont des paires clé-valeur pour catégoriser le stockage      
![aws_s3_createBucket_tag](../images/docOfficielle/aws_s3_createBucket_tag.png)     

10. Partie **Default encryption**: 3 types d'encryption proposés    
  ➡️ **SSE-S3**   ➡️ par defaut (clé gérée par Amazon S3)         
  ➡️ **SSE-KMS**  ➡️ clé gérée par AWS (`aws/s3` dans le service KMS). Il faut saisir l'arn de la clé KMS       
  ➡️ **DSSE-KMS** ➡️ chiffrement KMS double couche    

![aws_s3_createBucket_encrypt](../images/docOfficielle/aws_s3_createBucket_encrypt.png)  

11. Partie **Avanced Settings**: vérouillage d'objets S3    
  ➡️ cela active le versioning. Il est possible de "suspendre" ce lock mais pas de le supprimer     
![aws_s3_createBucket_advancedSettings](../images/docOfficielle/aws_s3_createBucket_advancedSettings.png)

12. Créer le bucket


................................................................................    

#### S3 : CONFIG - CHARGER UN OBJET

1 objet peut-être n'importe quel fichier: texte, photo, video...

1. Ouvrir le console AWS et aller dans le service S3    
2. Sélectionner le bucket souhaité         
3. Cliqué sur **Upload**       
4. Choissiser les fichiers à charger    
5. Charger  


................................................................................    

#### S3 : CONFIG - TELECHARGER UN OBJET

Il n'est possible de télécharger qu'1 seul objet à la fois

1. Ouvrir le console AWS et aller dans le service S3    
2. Sélectionner le bucket souhaité et l'objet         
3. Cliqué sur **Download**       



................................................................................    

#### S3 : CONFIG - COPIER UN OBJET

Il n'est possible de copier qu'1 seul objet à la fois

1. Ouvrir le console AWS et aller dans le service S3    
2. Créer un dossier et le configurer      
3. Aller dans le bucket dans lequel se trouve l'objet à copier et sélectionner l'objet        
4. Cliquer sur `Actions/Copy` et sélectionner en destination le dossier créé    
  ➡️la cible est de la forme `s3://nom-compartiment/nom-dossier/`    
5. Copier




................................................................................    

#### S3 : CONFIG - SUPPRIMER UN OBJET

Lorsqu'un objet ou bucket n'est plus utile il est conseillé des les supprimer afin de ne pas avoir de frais supplémentaires       

................................................................................    



=============================================================================    

### S3 : CAS D'UTILISATION COURANT DE S3 

- **sauvegarde et stockage**: le plus commun        
- **hébergement d'applications**: site web statique      
- **hébergement de multimédias**: infra hautement disponible pour accéder aux médias       
- **livraison de logiciels**: héberger les applications pour que les clients puissent les télécharger  




=============================================================================    

### S3 : CONTROLER LES ACCESS DES BUCKETS ET OBJETS 

AWS fournit de la sécurité pour cela ➡️ choisir en fonction du besoin    

................................................................................    

#### S3 : CONTROL ACCESS - BLOCK PUBLIC ACCESS

[Documentation officielle AWS - S3 block public access](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/access-control-block-public-access.html)            

La fonction **s3 block public access** fournit 4 parametres pour les points d'accès, les buckets et les comptes afin d'aider à gérer les accès public aux ressources s3             
Par défaut, les nouveaux compartiments, points d'accès et objets n'autorisent pas l'accès public           
Toutefois, les utilisateurs peuvent modifier ces policies pour autoriser l'accès       

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;      

##### S3 : CONTROL ACCESS - BLOCK PUBLIC ACCESS - PARAMETRES

Les 4 paramètres sont les suivants. Ils peuvent être appliqués de manière combinée à des points d'accès, des buckets ou des comptes.    
Appliqués à 1 compte il s'applique à tous les buckets et points d'entrées appartenant à ce compte    
De même, appliqué à un bucket il s'applique à tous les points d'entrée de ce bucket    


| Nom Parametre | Description |
| :---------- | :---------- | 
| **`BlockPublicAcls`**    | <p>Configuration sur **`TRUE`** entraine:</p> <p>Tout appel `PUT` échoue si une liste ACL est publique</p> <p>Les points d'accès ne sont pas associés à des ACLs ➡️ appliqué ce paramètre à 1 point d'accès l'applique au bucket sous-jacent  </p>|
| **`IgnorePublicAcls`**    | <p>Configuration sur **`TRUE`** entraine S3 à ignorer toutes les ACLs publiques sur 1 bucket et tout objet qu'il contient</p> <p>Ce paramètre vous permet de bloquer en toute sécurité l'accès public accordé par les ACL, tout en autorisant les appels `PUT` Object qui incluent une ACL publique</p><p>Les points d'accès ne sont pas associés à des ACLs ➡️ appliqué ce paramètre à 1 point d'accès l'applique au bucket sous-jacent</p> |
| **`BlockPublicPolicy`**    | <p>Configuration sur **`TRUE`**: S3 rejette les appels vers 1 politique `PUT` si la bucket policy autorise l'accès public, donc aussi tous les `PUT` vers point d'accès du bucket si elle autorise l'accès public</p> <p>Si **`TRUE`** sur un point d'accès ➡️ S3 rejette les appels `PUT` de ce point d'accès</p><p>**Il est recommandé d'utiliser ce parametre sur un compte**</p> |
| **`RestrictPublicBuckets`**    | <p>Configuration sur **`TRUE`**: l'accès à 1 point d'accès ou bucket avec 1 stratégie publique est limité aux **principals** du service AWS et aux users autorisés dans le compte owner du bucket et des points d'accès</p><p></p> |


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;      

................................................................................    


#### S3 : CONTROL ACCESS - IAM

[Documentation officielle AWS- IAM dans S3](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/s3-access-control.html)          

Par defaut toutes les ressources d'Amazon S3 (bucket, objet, sous-ressources associées) sont privées     
Seul le owner de la ressource (compte AWS) qui l'a créée peut y accéder.    
Il peut aussi accorder à d'autres instaces des autorisations d'accès en rédigeant une stratégie d'accès ➡️ il s'agit des **resource-based policies**      

Dans AWS, une ressource est une entité avec laquelle vous pouvez travailler     
Dans Amazon S3, les buckets et les objets sont des ressources qui possèdent chacune des sous-ressources associées.

<u>Les sous-ressources de buckets contiennent les éléments suivants:</u>     
- `lifecycle` : [Stocke des informations de configuration de cycle de vie](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/object-lifecycle-mgmt.html)        
- `website`: [Stocke les informations de configuration d'un site web](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/WebsiteHosting.html)         
- `versioning`: [Enregistre la configuration de contrôle de version](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/API/API_PutBucketVersioning.html)       
- `policy` et `acl`: Stocke les informations d'autorisation d'accès pour le bucket    
- `cors`: [Gère la configuration de votre bucket pour autoriser les demandes cross-origin](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/cors.html)    
- `object ownership`: [Permet au propriétaire du bucket de s'approprier les nouveaux objets](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/about-object-ownership.html)           
- `logging`: Permet de demander à Amazon S3 d'enregistrer les logs au bucket


<u>Les sous-ressources d'objets contiennent les éléments suivants:</u>       
- `acl`: [Stocke une liste d'autorisations d'accès sur l'objet](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/acl-overview.html)              
- `restore`: [Supporte la restauration temporaire d'un objet archivé](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/API/API_RestoreObject.html)       







................................................................................    

#### S3 : CONTROL ACCESS - BUCKET POLICIES

[Documentation officielle AWS- Bucket Policies dans S3](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/bucket-policies.html)       




................................................................................    


#### S3 : CONTROL ACCESS - ACL

[Documentation officielle AWS- ACL dans S3](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/acls.html)






................................................................................    


#### S3 : CONTROL ACCESS - S3 OBJECT OWNERSHIP

[Documentation officielle AWS- S3 Object Ownership](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/about-object-ownership.html)








................................................................................    


#### S3 : CONTROL ACCESS - IAM ACCESS ANALYZER FOR S3


[Documentation officielle AWS- IAM Analyzer dans S3](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/access-analyzer.html)





................................................................................    


=============================================================================    

___________________________________________________________________________________________________  

# FIN