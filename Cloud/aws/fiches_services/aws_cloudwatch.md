# DOCUMENTATION OFFICIELLE AWS CLOUDWATCH

Observez et surveillez les ressources et les applications sur AWS, sur site et sur d'autres clouds    

___________________________________________________________________________________________________      

## CLOUDWATCH  

[Documentation AWS Officielle: Cloudwatch](https://aws.amazon.com/fr/cloudwatch/)        


Amazon CloudWatch est un service qui surveille les applications, répond à l'évolution des performances, optimise l'utilisation des ressources et fournit des informations sur l'état des opérations.      
En collectant des données sur l'ensemble des ressources AWS, CloudWatch donne une visibilité sur les performances à l'échelle du système et permet aux utilisateurs de définir des alarmes, de réagir automatiquement aux modifications et d'obtenir une vision unifiée de l'état des opérations.           



___________________________________________________________________________________________________      

## CLOUDWATCH - AVANTAGES 


- **Visualiser et analyser vos données grâce à une observabilité de bout en bout** : Collecter, accéder et analyser les données de vos ressources et applications à l'aide de puissants outils de visualisation        


- **Fonctionner efficacement grâce à l'automatisation** : Améliorer les performances opérationnelles à l'aide d'alarmes et d'actions automatisées définies pour s'activer à des seuils prédéterminés


- **Obtenir rapidement une vue intégrée de votre AWS ou d'autres ressources**  : Intégrer de manière transparente plus de 70 services AWS pour une surveillance simplifiée et une capacité de mise à l'échelle    


- **Surveiller de manière proactive et obtenir des informations exploitables pour améliorer l'expérience des utilisateurs finaux** : Dépanner les problèmes opérationnels grâce à des informations exploitables issues des logs et des métriques de vos tableaux de bord CloudWatch          



Amazon CloudWatch collecte et visualise les logs, les métriques et les données d'événements en temps réel dans des tableaux de bord automatisés afin de rationaliser la maintenance de votre infrastructure et de vos applications    



___________________________________________________________________________________________________      

## CLOUDWATCH LOG GROUPS



[Documentation AWS - Cloudwath LogGroups](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html)

Il est possible d'utiliser Amazon CloudWatch Logs pour surveiller, stocker et accéder à vos fichiers logs à partir d'instances Amazon Elastic Compute Cloud (Amazon EC2) AWS CloudTrail, de Route 53 et d'autres sources          

CloudWatch Logs permet de centraliser les logs de tous les systèmes, applications et services AWS que vous utilisez, au sein d'un seul service hautement évolutif.     
Vous pouvez ensuite facilement les consulter, y rechercher des codes ou modèles d'erreur spécifiques, les filtrer en fonction de champs spécifiques ou les archiver en toute sécurité pour une analyse future.      
CloudWatch Les logs vous permettent de voir tous vos logs, quelle que soit leur source, sous la forme d'un flux unique et cohérent d'événements classés par ordre chronologique.       
CloudWatch Logs permet d'interroger vos logs à l'aide d'un langage de requête puissant, d'auditer et de masquer les données sensibles dans les logs, et de générer des métriques à partir des logs à l'aide de filtres ou d'un format de log intégré       

CloudWatch Logs prend en charge 2 classes de logs.     
 - Les groupes de logs de la classe de CloudWatch logs *Logs Standard* prennent en charge toutes les fonctionnalités CloudWatch des logs.    
 - Les groupes de CloudWatch logs de la classe Logs *Infrequent Access* entraînent des frais d'ingestion moins élevés et prennent en charge un sous-ensemble des fonctionnalités de la classe Standard.


[Documentation AWS - Cloudwatch Logs - Classes de logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CloudWatch_Logs_Log_Classes.html)                  

=======================================================================================================================        

### CLOUDWATCH LOG GROUPS - FONCTIONALITES


- **2 classes de CloudWatch logs pour plus de flexibilité** : Logs propose 2 classes de logs afin que vous puissiez disposer d'une option rentable pour les logs auxquels vous accédez rarement. Vous disposez également d'une option complète pour les logs qui nécessitent une surveillance en temps réel ou d'autres fonctionnalités

- **Interrogez les données de vos logs** : vous pouvez utiliser `CloudWatch Logs Insights` pour rechercher et analyser de manière interactive les données de vos logs. Vous pouvez effectuer des requêtes pour vous aider à répondre de manière plus efficace aux problèmes opérationnels. `CloudWatch Logs Insights` inclut un langage de requête spécialement conçu avec quelques commandes simples mais puissantes

- **Détecter et déboguer à l'aide de Live Tail** : vous pouvez utiliser `Live Tail` pour résoudre rapidement les incidents en consultant une liste des nouveaux événements du log au fur et à mesure de leur ingestion. Vous pouvez afficher, filtrer et mettre en évidence les logs ingérés en temps quasi réel, ce qui vous permet de détecter et de résoudre rapidement les problèmes. Vous pouvez filtrer les logs en fonction des termes que vous spécifiez et mettre en évidence les logs qui contiennent les termes spécifiés pour vous aider à trouver rapidement ce que vous cherchez

- **Surveillez les logs des instances Amazon EC2** : vous pouvez utiliser les `CloudWatch logs` pour surveiller les applications et les systèmes à l'aide des données des logs. Par exemple, `CloudWatch Logs` peut suivre le nombre d'erreurs qui se produisent dans les logs de vos applications et vous envoyer une notification chaque fois que le taux d'erreurs dépasse un seuil que vous spécifiez. `CloudWatch Logs` utilise les données de vos logs à des fins de surveillance ; aucune modification de code n'est donc requise. Par exemple, vous pouvez surveiller les logs des applications pour détecter des termes littéraux spécifiques (tels que _NullReferenceException_) ou compter le nombre d'occurrences d'un terme littéral à une position donnée dans les données des logs (tels que les codes d'état « 404 » dans un access log Apache). Lorsque le terme que vous recherchez est trouvé, `CloudWatch Logs` rapporte les données selon une CloudWatch métrique que vous spécifiez. Les données des logs sont chiffrées, pendant le transit et pendant le repos        

- **Surveiller les événements `AWS CloudTrail` enregistrés** : vous pouvez créer des alarmes CloudWatch (`cloudwatch Alarms`) et recevoir des notifications concernant une activité d'API particulière telle qu'elle est capturée, `CloudTrail` et utiliser la notification pour résoudre les problèmes

- **Auditez et masquez les données sensibles** : si vos logs contiennent des données sensibles, vous pouvez les protéger grâce à des politiques de protection des données. Ces politiques vous permettent d'auditer et de masquer les données sensibles. Si vous activez la protection des données, les données sensibles correspondant aux identifiants de données que vous sélectionnez sont masquées par défaut   

- **Conservation des logs** : par défaut, les logs sont conservés indéfiniment et n'expirent jamais. Vous pouvez ajuster la stratégie de conservation pour chaque log group. Elle peut être indéfinie ou comprise entre 10 ans et un jour     

- **Archiver les données du log** : vous pouvez utiliser `CloudWatch logs` pour stocker les données de vos logs dans un espace de stockage hautement durable. L'agent `CloudWatch Logs` permet d'envoyer rapidement des données de log avec ou sans rotation depuis un hôte vers le service de logisation. Vous pouvez ensuite accéder aux données brutes des logs lorsque vous en avez besoin

- **Consigner les requêtes DNS de `Route 53`** : vous pouvez utiliser CloudWatch les logs pour enregistrer les informations relatives aux requêtes DNS reçues par `Route 53` 



![aws_logs-cloudwacthConsole](../images/docOfficielle/aws_logs-cloudwacthConsole.png)          




=======================================================================================================================        

### CLOUDWATCH LOG GROUPS - TARIFICATION


L'utilisation de cloudwatch est payant en fonction du volume de logs à stocker et indexer        


=======================================================================================================================        

### CLOUDWATCH LOG GROUPS - UTILISATION DES LOG GROUPS ET LOG STREAM   

[Documentation AWS - Cloudwatch Logs - Log Groups et Log Stream](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/logs/Working-with-log-groups-and-streams.html)              


Un log stream est une séquence d'événements du logs qui partagent la même source. Chaque source distincte de `CloudWatch logs` dans Logs constitue un flux de logs distinct.

Un log group est un groupe de flux de logs qui partagent les mêmes paramètres de conservation, de surveillance et de contrôle d'accès.     
Vous pouvez définir des log groups et spécifier les flux à placer dans chaque groupe. Le nombre de flux de logs pouvant appartenir à un log group est illimité.



>_Remarque_: Il est une bonne pratique de mettre en place des tags sur les log Groups afin de pouvoir mettre en place un suivi (exemple: facturation), des policies (exemple: ne pas supprimer ce log group si le tag "toto" est présent)....


![aws_logs-cloudwacthLogGroups_exemples](../images/docOfficielle/aws_logs-cloudwacthLogGroups_exemples.png)           



=======================================================================================================================        

### CLOUDWATCH LOG GROUPS - SUBSCRIPTION FILTERS 


_Traitement en temps réel des données du log avec les abonnements (subscriptions)_

Vous pouvez utiliser des abonnements pour accéder à un flux en temps réel des événements du log depuis `CloudWatch Logs` et le transmettre à d'autres services tels qu'un flux `Amazon Kinesis`, un flux `Amazon Data Firehose`, ou `AWS Lambda` pour un traitement, une analyse ou un chargement personnalisés sur d'autres systèmes. Lorsque les événements du log sont envoyés au service récepteur, ils sont `codés en base64 et compressés au format gzip`.

Pour commencer à s'abonner aux événements du log, créez la ressource de réception, telle qu'un flux `Kinesis Data Streams`, où les événements seront transférés. Un` filtre d'abonnement (subscription filter)` définit le modèle de filtre à utiliser pour filtrer les événements du log transmis à votre AWS ressource, ainsi que les informations indiquant à qui envoyer les événements de log correspondants.

Vous pouvez créer des abonnements au niveau du compte et au niveau du `log group`. Chaque compte peut avoir un `subscription filter` au niveau du compte. Chaque `log group` peut avoir jusqu'à 2 `subscription filter` associés


![aws_logs-cloudwacthLogGroup-subscriptionFilter](../images/docOfficielle/aws_logs-cloudwacthLogGroup-subscriptionFilter.png)          


>_Remarque_: Si le service de destination renvoie une **erreur réessayable**, telle qu'une exception de limitation ou une exception de service réessayable (HTTP5xx par exemple), `CloudWatch Logs` continue de réessayer la livraison pendant **24 heures au maximum**.     
>`CloudWatch Logs` n'essaie pas de renvoyer le message s'il s'agit d'une erreur non réessayable, telle que  `AccessDeniedException` ou `ResourceNotFoundException`     
> Dans ces cas, le `subscription filter` est désactivé pendant 10 minutes au maximum, puis `CloudWatch Logs` tente à nouveau d'envoyer les logs à la destination. Pendant cette période de désactivation, les logs sont ignorés.


Les abonnements ne sont pris en charge que pour les groupes de journaux de la classe de logs standard


.......................................................................................................................       

#### CLOUDWATCH LOG GROUPS - SUBSCRIPTION FILTERS - CONCEPTS


Chaque subscription filter est composé des principaux éléments suivants :


- **filter pattern**: Une description symbolique de la façon dont les `CloudWatch Logs` doivent interpréter les données de chaque événement du log, ainsi que des expressions de filtrage qui limitent ce qui est livré à la AWS ressource de destination               

- **destination arn**: Amazon Resource Name (ARN) du flux `Kinesis Data Streams`, du flux `Firehose` ou de `Lambda` que vous souhaitez utiliser comme destination du flux d'abonnement    

- **ARN de rôle**: `IAM Role` qui accorde à `CloudWatch Logs` les autorisations nécessaires pour placer des données dans la destination choisie. Ce rôle n'est pas nécessaire pour les destinations `Lambda` car `CloudWatch Logs` peut obtenir les autorisations nécessaires à partir des paramètres de contrôle d'accès de la fonction Lambda elle-même      

- **distribution**: La méthode utilisée pour distribuer les données du log à la destination, lorsque la destination est un flux dans `Amazon Kinesis Data Streams`. Par défaut, les données des logs sont regroupées par flux de logs. Pour assurer une meilleure répartition de la distribution, vous pouvez regrouper les données des logs de manière aléatoire     

- **log group name**: Le `log group` auquel associer le `subscription filter`. Tous les événements du log téléchargés vers ce `log group` sont soumis au `subscription filter`. Ceux qui correspondent au filtre sont envoyés au service de destination qui reçoit les événements du log correspondant       

- **selection criteria**: Critères utilisés pour sélectionner les `log groups` auxquels le `subscription filter` au niveau du compte est appliqué. Si vous ne le spécifiez pas, le `subscription filter` au niveau du compte est appliqué à tous les `log groups` du compte. Ce champ est utilisé pour empêcher les boucles de log infinie. Selection criteria a une taille limite de 25 KB              



.......................................................................................................................       

#### CLOUDWATCH LOG GROUPS - SUBSCRIPTION FILTERS  & LOG GROUPS 


[Documentation AWS - Subscription filter au niveau des logs groups](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/logs/SubscriptionFilters.html)             



>_Remarque_: Pour les parties suivantes (kinesis data stream, lambda et firehose), il est préféreable d'**utiliser AWS CLI** plutôt que la console si vous souhaiter faire des tests : avec la console -> erreur, avec la cli -> OK
> Il faut aussi avoir créé la cible du filtre avant de mettre en place la configuration dans `cloudwatch` puisqu'elle est demandée

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
##### CLOUDWATCH LOG GROUPS - SUBSCRIPTION FILTERS & LOG GROUPS - KINESIS DATA STREAMS 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/logs/SubscriptionFilters.html#DestinationKinesisExample)            







+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
##### CLOUDWATCH LOG GROUPS - SUBSCRIPTION FILTERS & LOG GROUPS - AWS LAMBDA 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/logs/SubscriptionFilters.html#LambdaFunctionExample)            








+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
##### CLOUDWATCH LOG GROUPS - SUBSCRIPTION FILTERS & LOG GROUPS - AMAZON DATA FIREHOSE 

[Doc AWS](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/logs/SubscriptionFilters.html#FirehoseExample)


 1. Créer un S3 bucket     
 `aws s3api create-bucket --bucket my-bucket --create-bucket-configuration LocationConstraint=region`      

 2. Créez le IAM rôle qui autorise `Amazon Data Firehose` à placer des données dans votre bucket `Amazon S3`      
 ```bash
  aws iam create-role \
  --role-name FirehosetoS3Role \
  --assume-role-policy-document file://~/TrustPolicyForFirehose.json
  #------------------------------------------------------------------------------
  {
      "Role": {
          "AssumeRolePolicyDocument": {
              "Statement": {
                  "Action": "sts:AssumeRole",
                  "Effect": "Allow",
                  "Principal": {
                      "Service": "firehose.amazonaws.com"
                  }
              }
          },
          "RoleId": "AAOIIAH450GAB4HC5F431",
          "CreateDate": "2015-05-29T13:46:29.431Z",
          "RoleName": "FirehosetoS3Role",
          "Path": "/",
          "Arn": "arn:aws:iam::123456789012:role/FirehosetoS3Role"
      }
  }
  #------------------------------------------------------------------------------
 ```

 avec `TrustPolicyForFirehose.json`:          
 ```json
  {
    "Statement": {
      "Effect": "Allow",
      "Principal": { "Service": "firehose.amazonaws.com" },
      "Action": "sts:AssumeRole"
      } 
  }
 ```

 et `PermissionsForFirehose.json`: 
 ```json
  {
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [ 
            "s3:AbortMultipartUpload", 
            "s3:GetBucketLocation", 
            "s3:GetObject", 
            "s3:ListBucket", 
            "s3:ListBucketMultipartUploads", 
            "s3:PutObject" ],
        "Resource": [ 
            "arn:aws:s3:::my-bucket", 
            "arn:aws:s3:::my-bucket/*" ]
      }
    ]
  }
 ```

  3. Associez la politique d'autorisations au rôle à l'aide de la put-role-policy commande suivante       
  `aws iam put-role-policy --role-name FirehosetoS3Role --policy-name Permissions-Policy-For-Firehose --policy-document file://~/PermissionsForFirehose.json`          

  4. Créez un flux de diffusion Firehose de destination comme suit, en remplaçant les valeurs d'espace réservé pour Role et Bucket par le rôle ARN et le bucket ARN que vous avez créés          
  ```bash
  aws firehose create-delivery-stream \
    --delivery-stream-name 'my-delivery-stream' \
    --s3-destination-configuration \
    '{"RoleARN": "arn:aws:iam::123456789012:role/FirehosetoS3Role", "BucketARN": "arn:aws:s3:::my-bucket"}'                    
  ```

  5. Créez le IAM rôle qui autorise CloudWatch Logs à insérer des données dans votre flux de diffusion Firehose    
  ```bash
    aws iam create-role \
      --role-name CWLtoKinesisFirehoseRole \
      --assume-role-policy-document file://~/TrustPolicyForCWL.json
    #------------------------------------------------------------------------------
    {
        "Role": {
            "AssumeRolePolicyDocument": {
                "Statement": {
                    "Action": "sts:AssumeRole",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "logs.amazonaws.com"
                    },
                    "Condition": { 
                        "StringLike": { 
                            "aws:SourceArn": "arn:aws:logs:region:123456789012:*"
                        } 
                    }
                }
            },
            "RoleId": "AAOIIAH450GAB4HC5F431",
            "CreateDate": "2015-05-29T13:46:29.431Z",
            "RoleName": "CWLtoKinesisFirehoseRole",
            "Path": "/",
            "Arn": "arn:aws:iam::123456789012:role/CWLtoKinesisFirehoseRole"
        }
    }
    #------------------------------------------------------------------------------
  ```


  avec `TrustPolicyForCWL.json`: 
  ```json
  {
    "Statement": {
      "Effect": "Allow",
      "Principal": { "Service": "logs.amazonaws.com" },
      "Action": "sts:AssumeRole",
      "Condition": { 
          "StringLike": { 
              "aws:SourceArn": "arn:aws:logs:region:123456789012:*"
          } 
      }
    }
  }
  ```

  et `PermissionsForCWL.json`:            
  ```json
  {
      "Statement":[
        {
          "Effect":"Allow",
          "Action":["firehose:PutRecord"],
          "Resource":[
              "arn:aws:firehose:region:account-id:deliverystream/delivery-stream-name"]
        }
      ]
  }
  ```

  6. Associer la policy         
  `aws iam put-role-policy --role-name CWLtoKinesisFirehoseRole --policy-name Permissions-Policy-For-CWL --policy-document file://~/PermissionsForCWL.json`        

  7. Une fois que le flux de diffusion `Amazon Data Firehose` est actif et que vous avez créé le IAM rôle, vous pouvez créer le subscription filter `CloudWatch Logs`. Le subscription filter lance immédiatement le flux de données de log en temps réel du log group choisi vers votre flux de diffusion `Amazon Data Firehose`        
  ```bash
    aws logs put-subscription-filter \
        --log-group-name "CloudTrail" \
        --filter-name "Destination" \
        --filter-pattern "{$.userIdentity.type = Root}" \
        --destination-arn "arn:aws:firehose:region:123456789012:deliverystream/my-delivery-stream" \
        --role-arn "arn:aws:iam::123456789012:role/CWLtoKinesisFirehoseRole"
  ```

  8. Consulter votre bucket Amazon S3 pour vérifier vos données         
  ```bash
    aws s3api list-objects --bucket 'my-bucket' --prefix 'firehose/'
    #------------------------------------------------------------------------------
    {
        "Contents": [
            {
                "LastModified": "2015-10-29T00:01:25.000Z",
                "ETag": "\"a14589f8897f4089d3264d9e2d1f1610\"",
                "StorageClass": "STANDARD",
                "Key": "firehose/2015/10/29/00/my-delivery-stream-2015-10-29-00-01-21-a188030a-62d2-49e6-b7c2-b11f1a7ba250",
                "Owner": {
                    "DisplayName": "cloudwatch-logs",
                    "ID": "1ec9cf700ef6be062b19584e0b7d84ecc19237f87b5"
                },
                "Size": 593
            },
            {
                "LastModified": "2015-10-29T00:35:41.000Z",
                "ETag": "\"a7035b65872bb2161388ffb63dd1aec5\"",
                "StorageClass": "STANDARD",
                "Key": "firehose/2015/10/29/00/my-delivery-stream-2015-10-29-00-35-40-7cc92023-7e66-49bc-9fd4-fc9819cc8ed3",
                "Owner": {
                    "DisplayName": "cloudwatch-logs",
                    "ID": "1ec9cf700ef6be062b19584e0b7d84ecc19237f87b6"
                },
                "Size": 5752
            }
        ]
    }
    #------------------------------------------------------------------------------
  ```


**RESUME DES ACTIONS**
```bash
1. Créer un s3 
2. Créer IAM role (avec sa policy) qui autorise Firehose à ecrire dans s3 précédemment créé 
3. Créer un flux firehose avec les s3 et IAM role précédement créés    
4. Créer IAM role (avec sa policy) qui autorise cloudwatch Logs à insérer les logs dans le flux firehose 
5. Créer le subscription filter cloudwatch logs pour le firehose créé 
6. Vérifier les objets dans le s3 (pas dans la pipeline)
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

.......................................................................................................................       

=======================================================================================================================        

___________________________________________________________________________________________________      

# FIN