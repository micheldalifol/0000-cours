# DOCUMENTATION OFFICIELLE AWS KINESIS

Collectez, traitez et analysez des data stream et de vidéos en temps réel

___________________________________________________________________________________________________      

## KINESIS  

[Documentation AWS Officielle: Kinesis](https://aws.amazon.com/fr/kinesis/)          


Vous pouvez utiliser `Amazon Kinesis Data Streams` pour **collecter et traiter des flux volumineux d'enregistrements de données en temps réel**.     

Vous pouvez créer des applications de traitement de données appelées applications `Kinesis Data Streams`. Une application `Kinesis Data Streams` standard lit les données à partir d'un data stream sous forme d'enregistrements de données.      
Ces applications peuvent utiliser la bibliothèque cliente Kinesis et s'exécuter sur des instances AmazonEC2.     

Vous pouvez envoyer les enregistrements traités à des tableaux de bord, les utiliser pour générer des alertes et modifier dynamiquement les stratégies tarifaires et publicitaires, ou envoyer des données à différents autres services  AWS .


Les scénarios suivants sont typiques de l'utilisation de `Kinesis Data Streams` :

**Extraction et traitement accélérés des logs et des data stream**          
  Les applications producteur peuvent envoyer leurs données directement dans un flux. Par exemple, envoyez des logs système et d'application ; ils sont alors disponibles pour le traitement en quelques secondes.    
  Cela empêche la perte des données du log en cas de défaillance du serveur frontal ou de l'application.     
  `Kinesis Data Streams` permet l'accélération de la collecte des data stream, car vous n'avez pas besoin de regrouper les données sur les serveurs avant de les soumettre à la collecte.

**Métriques et création de rapports en temps réel**           
  Vous pouvez utiliser les données collectées dans `Kinesis Data Streams` pour une analyse de données simple et une création de rapports en temps réel. Par exemple, votre application de traitement de données peut générer des métriques et des rapports à partir de logs d'applications et d'événements système dès que les données ont été transmises, au lieu d'attendre qu'elles soient envoyées par lots de données.

**Analyse des données en temps réel**           
  Cette analyse combine la puissance du traitement parallèle avec la valeur des données en temps réel. Par exemple, traiter des flux de clics de site Web en temps réel, puis analyser la facilité d'utilisation du site à l'aide de plusieurs applications `Kinesis Data Streams` différentes exécutées en parallèle.

**Traitement des flux complexes**              
  Vous pouvez créer des _graphes acycliques dirigés_ (DAGs) à partir d'applications et de data stream `Kinesis Data Streams`. Cela implique généralement de placer des données issues de plusieurs applications `Kinesis Data Streams` dans un autre flux pour être traitées en aval par une application `Kinesis Data Streams` différente.


===============================================================================================================            

### KINESIS - ARCHITECTURE KINESIS DATA STREAM


Le diagramme suivant illustre l'architecture de haut niveau de `Kinesis Data Streams`. Les applications producteur envoient (push) continuellement des données à `Kinesis Data Streams` et les applications consommateur traitent ces données en temps réel. Les consommateurs (tels qu'une application personnalisée exécutée sur Amazon EC2 ou un flux de diffusion `Amazon Data Firehose`) peuvent stocker leurs résultats à l'aide d'un AWS service tel qu'`Amazon DynamoDB`, `Amazon Redshift` ou `Amazon S3`     

![aws_kinesis_architecture-diagram](../images/docOfficielle/aws_kinesis_architecture-diagram.png)      




===============================================================================================================            

### KINESIS - TERMINOLIGIE KINESIS DATA STREAM

 - Un **flux de données Kinesis** est un ensemble de partitions. Chaque partition comporte une séquence d'enregistrements de données. Chaque enregistrement de données dispose d'un numéro de séquence attribué par `Kinesis Data Streams`


- Un **enregistrement de données** est l'unité de données stockée dans un flux de données Kinesis. Les enregistrements de données sont composés d'un numéro de séquence, d'une clé de partition et d'un blob de données, qui est une séquence immuable d'octets. Une fois que vous avez stocké les données dans l'enregistrement, `Kinesis Data Streams` n'inspecte pas, n'interprète pas ou ne modifie absolument pas le blob. Un blob de données peut atteindre jusqu'à 1 Mo      

- Un **mode de capacité** de flux de données détermine comment la capacité est gérée et comment l'utilisation de votre flux de données vous est facturée. Actuellement, dans `Kinesis Data Streams`, vous pouvez choisir entre un mode à la demande et un mode provisionné pour vos flux de données          

- La **période de conservation** correspond à la durée pendant laquelle les enregistrements de données sont accessibles après avoir été ajoutés au flux. La période de conservation d'un flux a une valeur par défaut de 24 heures après la création. Vous pouvez augmenter la période de rétention jusqu'à 8760 heures (365 jours) en utilisant l'`IncreaseStreamRetentionPeriodopération`, et la réduire à un minimum de 24 heures en utilisant le`DecreaseStreamRetentionPeriodopération`. Des frais supplémentaires s'appliquent pour les flux dont la période de conservation définie est supérieure à 24 heures              

- Les **producteurs** placent des enregistrements dans `Amazon Kinesis Data Streams`. Par exemple, un serveur Web qui envoie des données de log dans un flux est un producteur              

- Les **consommateurs** récupèrent les enregistrements depuis `Amazon Kinesis Data Streams` et les traitent. Ces applications consommateur s'appellent _Application Amazon Kinesis Data Streams_              

- Une **Application Amazon Kinesis Data Streams** utilise un flux qui s'exécute généralement sur un parc d'instances EC2     

- Une **partition** est une séquence d'enregistrements de données appartenant à un flux et identifiée de manière unique. Un flux se compose d'une ou plusieurs partitions, chacune fournissant une unité de capacité fixe. Chaque partition peut prendre en charge jusqu'à 5 transactions par seconde pour les lectures, jusqu'à un taux total de lecture de données maximal de 2 Mo par seconde et jusqu'à 1 000 enregistrements par seconde pour les écritures, jusqu'à un taux d'écriture de données total maximal de 1 Mo par seconde (clés de partition incluses). La capacité de données de votre flux dépend du nombre de partitions que vous spécifiez pour le flux. La capacité totale du flux est la somme des capacités de ses shards             

- Une **clé de partition** sert à grouper les données par partition dans un flux.` Kinesis Data Streams` sépare les enregistrements de données appartenant à un flux en plusieurs partitions. Il utilise la clé de partition associée à chaque enregistrement de données pour déterminer à quelle partition un enregistrement de données spécifique appartient. Les clés de partition sont des chaînes Unicode, avec une longueur maximale de 256 caractères pour chaque clé. Une fonction de MD5 hachage est utilisée pour mapper les clés de partition à des valeurs entières de 128 bits et pour mapper les enregistrements de données associés aux partitions en utilisant les plages de clés de hachage des partitions. Lorsqu'une application place des données dans un flux, elle doit spécifier une clé de partition         

- Chaque enregistrement de données a un **numéro de séquence** qui est unique par clé de partition au sein de son groupe de données. `Kinesis Data Streams` attribue le numéro de séquence une fois que vous avez écrit dans le flux avec `client.putRecords` ou `client.putRecord`. Les numéros de séquence correspondant à une même clé de partition deviennent généralement de plus en plus longs au fil du temps. Plus le délai entre les demandes d'écriture est élevé, plus les numéros de séquence sont longs           

- La **bibliothèque client Kinesis** est compilée dans votre application pour permettre une consommation de données tolérante aux pannes à partir du flux. La bibliothèque client Kinesis garantit la présence, pour chaque partition, d'un processeur d'enregistrements qui s'exécute et traite cette partition. La bibliothèque simplifie également la lecture des données dans le flux. La bibliothèque cliente Kinesis utilise un tableau `Amazon DynamoDB` pour stocker les données de contrôle. Elle crée une seule table par application qui traite des données              




===============================================================================================================            

__________________________________________________________________________________________________________________________          

# FIN
