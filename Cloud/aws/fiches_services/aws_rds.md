# DOCUMENTATION OFFICIELLE AWS RDS

**Configurer, gérer et mettre à l'échelle une base de données relationnelle dans le cloud en quelques clics**    

___________________________________________________________________________________________________      

## RDS: RELATIONAL DATABASE SERVICE  

[Documentation AWS Officielle: RDS](https://docs.aws.amazon.com/rds/)          


Amazon Relational Database Service (Amazon RDS) est un service de base de données relationnelle facile à gérer, optimisé pour le coût total de possession.     
Il est simple à configurer, à utiliser et se met à l’échelle à la demande. Amazon RDS automatise les tâches indifférenciées de gestion des bases de données, telles que l’allocation, la configuration, les sauvegardes et l’application de correctifs.    
Amazon RDS permet aux clients de créer une nouvelle base de données en quelques minutes et offre la flexibilité nécessaire pour personnaliser les bases de données afin de répondre à leurs besoins grâce à 8 moteurs et 2 options de déploiement.    
Les clients peuvent optimiser les performances grâce à des fonctionnalités telles que [Multi-AZ](https://aws.amazon.com/rds/features/multi-az/) avec 2 modes de veille lisibles, des [écritures et des lectures optimisées](https://aws.amazon.com/rds/features/#product-features#rds-features#customizable-performance) et des [instances basées sur AWS Graviton3](https://aws.amazon.com/rds/instance-types/), et choisir parmi [plusieurs options de tarification](https://aws.amazon.com/rds/pricing/) pour gérer efficacement les coûts.    


=============================================================================    

### RDS: AVANTAGES

Amazon RDS est un service de base de données géré. Il est responsable de la plupart des tâches de gestion. En éliminant les processus manuels fastidieux, Amazon RDS vous permet de vous concentrer sur votre application et vos utilisateurs    

Amazon RDS offre les principaux avantages suivants par rapport aux déploiements de bases de données qui ne sont pas entièrement gérés :

 - Vous pouvez utiliser des moteurs de base de données que vous connaissez déjà : IBM Db2, MariaDB, SQL Microsoft Server, MySQL, Oracle Database et Postgre. SQL

 - Amazon RDS gère les sauvegardes, les correctifs logiciels, la détection automatique des défaillances et la restauration.

 - Vous pouvez activer des sauvegardes automatiques ou créer manuellement vos propres snapshots de sauvegarde. Vous pouvez utiliser ces sauvegardes pour restaurer une base de données. Le processus de RDS restauration Amazon fonctionne de manière fiable et efficace.

 - Vous pouvez obtenir une haute disponibilité avec une instance de base de données principale et une instance de base de données secondaire synchrone vers laquelle vous pouvez basculer en cas de problème. Vous pouvez aussi utiliser les réplicas en lecture pour augmenter la mise à l'échelle de la lecture.

 - Outre la sécurité de votre package de base de données, vous pouvez contrôler l'accès en utilisant AWS IAM pour définir les utilisateurs et les autorisations. Vous pouvez également contribuer à protéger vos bases de données en les plaçant dans un cloud privé virtuel (VPC).



=============================================================================    

### RDS: AFFICHER LES METRIQUES RDS DANS LA CONSOLE AWS 

[Documentation AWS](https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/USER_Monitoring.html)      


**Amazon RDS s'intègre à CloudWatch** pour afficher diverses métriques d'instances de base de données du RDS cluster dans la RDS console. 

Pour votre instance de base de données, les catégories de métriques suivantes sont surveillées :

 - **CloudWatch**— Affiche les CloudWatch métriques Amazon pour auxquelles vous pouvez accéder dans la RDS console. Vous pouvez également accéder à ces métriques dans la CloudWatch console. Chaque métrique inclut un graphique affichant la métrique supervisée sur une période donnée. Pour obtenir la liste des CloudWatch indicateurs, consultez CloudWatch Métriques Amazon pour Amazon RDS.

 - **Surveillance améliorée** : affiche un résumé des métriques du système d'exploitation lorsque votre instance de base de données du cluster de base de RDS données a activé la surveillance améliorée. RDS fournit les statistiques issues de la surveillance améliorée à votre compte Amazon CloudWatch Logs. Chaque métrique du système d'exploitation comprend un graphique montrant la métrique surveillée sur un intervalle spécifique. Pour avoir une présentation, consultez Surveillance des métriques du système d'exploitation à l'aide de la Surveillance améliorée. Pour obtenir une liste des métriques de la surveillance améliorée, veuillez consulter Métriques du système d'exploitation dans la surveillance améliorée.

 - **Liste de processus du système d'exploitation** : affiche les détails de chaque processus s'exécutant dans votre instance de base de données.

 - **Performance Insights** — Ouvre le tableau de bord Amazon RDS Performance Insights pour une instance de base de données . Pour une présentation de Performance Insights, veuillez consulter Surveillance de la charge de base de données avec Performance Insights sur RDSAmazon. Pour obtenir une liste des métriques de Performance Insights, veuillez consulter Statistiques CloudWatch Amazon pour Amazon RDS Performance Insights.


Amazon RDS fournit désormais une vue consolidée des CloudWatch statistiques et statistiques de Performance Insights dans le tableau de bord Performance Insights. Performance Insights doit être activé pour que votre instance de base de données puisse utiliser cette vue. Vous pouvez choisir la nouvelle vue de surveillance dans l'onglet Surveillance ou Performance Insights dans le volet de navigation. Pour consulter les instructions relatives au choix de cette vue, consultez Afficher les indicateurs combinés avec le tableau de bord Performance Insights.




=============================================================================    

### RDS: SURVEILLANCE DES LOGS RDS

[Documentation AWS](https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/CHAP_Monitor_Logs_Events.html)          

Lorsque vous surveillez vos bases de données RDS Amazon et vos autres AWS solutions, votre objectif est de maintenir les points suivants :

 - Fiabilité        
 - Disponibilité     
 - Performance     
 - Sécurité    

Surveillance des métriques dans une instance Amazon RDS explique la surveillance de votre instance à l'aide de métriques. Une solution complète doit également surveiller les événements de base de données, les fichiers journaux et les flux d'activité. AWS met à votre disposition les outils de surveillance suivants :

 - **Amazon EventBridge** est un service de bus d'événements serverless qui permet de connecter facilement vos applications à des données provenant de diverses sources. EventBridge fournit un flux de données en temps réel à partir de vos propres applications, applications Software-as-a Service (SAAS) et AWS services. EventBridge achemine ces données vers des cibles telles que AWS Lambda. Cela vous permet de surveiller les événements qui se produisent dans les services et de créer des architectures basées sur les événements. Pour plus d'informations, consultez le guide de EventBridge l'utilisateur Amazon.

 - **Amazon CloudWatch** Logs fournit un moyen de surveiller, de stocker et d'accéder à vos fichiers logs à partir d'instances RDS Amazon et d'autres sources. AWS CloudTrail Amazon CloudWatch Logs peut surveiller les informations contenues dans les fichiers logs et vous avertir lorsque certains seuils sont atteints. Vous pouvez également archiver vos données de logs dans une solution de stockage hautement durable. Pour plus d'informations, consultez le guide de l'utilisateur d'Amazon CloudWatch Logs.

 - **AWS CloudTrail** enregistre les appels API et les événements connexes effectués par ou en votre nom Compte AWS. CloudTrail fournit les fichiers journaux dans un compartiment Amazon S3 que vous spécifiez. Vous pouvez identifier les utilisateurs et les comptes appelés AWS, l'adresse IP source à partir de laquelle les appels ont été effectués et la date des appels. Pour plus d’informations, consultez le AWS CloudTrail Guide de l’utilisateur .

 - **Database Activity Streams** est une fonctionnalité de RDS Amazon qui fournit un flux en temps quasi réel de l'activité de votre instance de de base de données. Amazon RDS redirige les activités vers un flux de données Amazon Kinesis. Le flux Kinesis est créé automatiquement. Kinesis vous permet de configurer des AWS services tels qu'Amazon Data Firehose, de consommer le flux et AWS Lambda de stocker les données.


.......................................................................................................................       

#### RDS: LOGS - METTRE EN PLACE LA SURVEILLANCE


Il est possible de récupérer plusieurs types de logs des database rds:        
 - Audit log      
 - Error log      
 - General log     
 - Slow query log    


Nous pouvons accéder à ces fichiers de 2 manières différentes:    
 - depuis la console rds          
 - depuis cloudwatch           

Dans les 2 cas une configuration spécifique est demandée

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

##### RDS - LOGS - ACTIVATION DES LOGS


**Cas d'un cluster de db ou d'une instance seule**:       


Dans ce cas même si une seule instance est présente dans ce cluster pour pouvoir récupérer les logs il faut mettre en place un **parameter group** de type **DB cluster parameter group** avec les bons parametres pour avoir accès aux logs souhaités

![aws_doc-rds-listOfCluster-instance](../images/docOfficielle/aws_rds-listOfCluster-instance.png)           
Dans l'image ci-dessus on peut voir une liste d'instances et de cluster rds    
En vert les cluster, en rouge les instances             

Dans le cas d'un cluster il faut modifier la configuration de ce cluster avec un parameter group specifique créé comme suiit: 

![aws_rds-paramgroup_dbcluster](../images/docOfficielle/aws_rds-paramgroup_dbcluster.png)                


Les parametres à mettre en place sont les suivants:          

 - `server_audit_logging` avec la valeur **`1`**   (active)              
 - `server_audit_logs_upload` avec la valeur **`1`**   (active)           
 - `server_audit_events` avec la valeur **`CONNECT,QUERY,QUERY_DCL,QUERY_DDL,QUERY_DML,TABLE`**   (active)           


Une fois ce parameter group créé on peut l'ajouter dans le configuration du cluster de db:          

![aws_rds-dbcluster-config](../images/docOfficielle/aws_rds-dbcluster-config.png)        

Il est maintenant possible de récupérer directmement depuis la console rds les logs configurés: 
![aws_rds-instance-logs](../images/docOfficielle/aws_rds-instance-logs.png)                           

Sur cette capture nous pouvons voir que les audit logs sont récupérés     




+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

##### RDS - LOGS - ACTIVATION DES LOGS DANS CLOUDWATCH

Dans ce cas il faut en plus de ce qui a été décrit précédement ajouter la configuration de l'exporter des logs (sélectionner ce que l'on souhaite r&écupérer) dans la partie **cluster** dans le cas de cluster et **instance** dans le cas d'instance seule   


Sélection à faire pour export des logs dans cloudwatch:       
![aws_rds-instance_cluster-config-cloudwactch](../images/docOfficielle/aws_rds-instance_cluster-config-cloudwactch.png)                      



![aws_rds-dbcluster-config-cloudwatch](../images/docOfficielle/aws_rds-dbcluster-config-cloudwatch.png)           
Dans l'exemple ci-dessus seul les audit logs sont sélectionnés

![aws_rds-instance-config-cloudwatch](../images/docOfficielle/aws_rds-instance-config-cloudwatch.png)                   
Dans l'exemple ci-dessus 2 types de logs sont sélectionnés        



Depuis la console cloudwacth (en cliquant sur le lien de la configuration des exports de logs vers cloudwatch de la db rds) on a ceci     

![aws_rds-cloudwacthLogGroup](../images/docOfficielle/aws_rds-cloudwacthLogGroup.png)                 
On peut voir la liste des logs que l'on exporte dans cloudwatch   


Le log group cloudwatch aura pour nom `/aws/rds/<type: cluster | instance>/<nom database>/<type de logs>`





+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

.......................................................................................................................       

=============================================================================    

___________________________________________________________________________________________________  

# FIN   