# AWS CLI


## DOCUMENTATION    


[Documentation AWS - aws cli](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html)              



## EXEMPLES DE CLI


### CONNAITRE LE CURRENT ACCOUNT ID 

```bash
aws sts get-caller-identity --query "Account" --output text
#-------------------------------------------------------------------
111111111111    ## ID du current account
#-------------------------------------------------------------------
```

### CONNAITRE LA CURRENT REGION 

```bash
aws ec2 describe-availability-zones --output text --query 'AvailabilityZones[0].[RegionName]'
#-------------------------------------------------------------------
eu-west-1    ## current region
#-------------------------------------------------------------------
```

