# DOCUMENTATION OFFICIELLE AWS ATHENA

Analyser des pétaoctets de données là où elles se trouvent, de manière simple et flexible

___________________________________________________________________________________________________      

## ATHENA  

[Documentation AWS Officielle: Athena](https://aws.amazon.com/fr/athena/)          
