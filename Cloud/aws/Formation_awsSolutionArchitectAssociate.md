# <span style="color: #CE5D6B"> **AWS SOLUTION ARCHITECT ASSOCIATE** </span>

La formation suivie est celle de A Cloud Guru: https://learn.acloud.guru/course/certified-solutions-architect-associate/overview 


_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **PRESENTATION GENERALE DE LA FORMATION** 
</span>


Dans cette formation sera expliqué les sujets suivants:
- [AWS fundamentals](./Formation_awsSolutionArchitectAssociate.md#aws-fundamentals)    
- [IAM](./Formation_awsSolutionArchitectAssociate.md#iam-identity-and-access-management)        
- [S3](Formation_awsSolutionArchitectAssociate.md#s3-simple-storage-service)           
- [EC2](./Formation_awsSolutionArchitectAssociate.md#ec2-elastic-compute-cloud)    
- [EBS et EFS](./Formation_awsSolutionArchitectAssociate.md#ebs-elastic-block-storage-efs-elastic-file-system)       
- [Databases](./Formation_awsSolutionArchitectAssociate.md#databases-1)    
- [VPC Networking](./Formation_awsSolutionArchitectAssociate.md#vpc-virtual-private-cloud-networking)        
- [Route 53](./Formation_awsSolutionArchitectAssociate.md#route-53)    
- [ELB (Elastic Load Balancing)](./Formation_awsSolutionArchitectAssociate.md#elb-elastic-load-balancing)   
- [Monitoring](./Formation_awsSolutionArchitectAssociate.md#monitoring)
- [High availability and scaling](./Formation_awsSolutionArchitectAssociate.md#high-availability-and-scaling)       
- [Decoupling Workflows](./Formation_awsSolutionArchitectAssociate.md#decoupling-workflows)   
- [Big data](./Formation_awsSolutionArchitectAssociate.md#big-data)    
- [Serverless architecture](./Formation_awsSolutionArchitectAssociate.md#serverless-architecture)  
- [Security](./Formation_awsSolutionArchitectAssociate.md#security)   
- [Automation](./Formation_awsSolutionArchitectAssociate.md#automation)    
- [Caching](./Formation_awsSolutionArchitectAssociate.md#caching)    
- [Gouvernance](./Formation_awsSolutionArchitectAssociate.md#governance)    
- [Migration](./Formation_awsSolutionArchitectAssociate.md#migration)
- [Frontend web and mobile](./Formation_awsSolutionArchitectAssociate.md#frontend-web-and-mobile)   
- [Machine Learning](./Formation_awsSolutionArchitectAssociate.md#machine-learning)   
- [Media](./Formation_awsSolutionArchitectAssociate.md#media)    
- [Exam Preparation](./Formation_awsSolutionArchitectAssociate.md#aws-exam-preparation)    
  

[Resume AWS](./resumeAws.md)     


_______________________________________________________________________________________________________________________    


<span style="color: #CE5D6B">

##  **AWS FUNDAMENTALS** 
</span>


=======================================================================================================================    

<span style="color: #EA9811">

###  **AWS Global Infrastructure** 
</span>

Dans cette partie nous allons voir les différentes zones.
On peut voir sur la page d'accueil la carte du monde avec tous les Data center (Regions) en fonctionnement + ceux à venir.   
https://aws.amazon.com/fr/ 

Chaque **region** peut avoir 2 ou + **availability zones**: https://aws.amazon.com/about-aws/global-infrastructure/?hp=tile&tile=map     
Une region est une localisation physqiue dans le monde.    


Une **availability zone** est 1 ou + data center avec son reseau electrique, reseau internet et dans des entrepots séparés    

**Edge location** sont des endpoints de regions ➡️ il y a beaucoup plus de edge location que de regions.    
Ces **edge location** servent à la livraison de données statiques (ex les DNS sont dans les edge locations): seulement pour du stokage, pas de calculs sont faits ➡️ plus rapide    
Ils mettent en cache des données ➡️ c'est par là que les utilisateurs "web" accèdent à leurs services    
Ils sont les cloud front d'aws    


La console aws: **AWS Management Console**    
Elle permet de controler les différents services.

Il n'est pas nécessaire de connaitre l'ensemble des services fournis par AWS. (il y en a trop).
Par contre pour la certification il faut connaitre les services de ces types là:    
![servicesWeHaveToKnowForCertification](./images/aws_solutionArchitectAssociate/servicesWeHaveToKnowForCertification.png)    


=======================================================================================================================    

<span style="color: #EA9811">

###  **Who owns What is the cloud?** 
</span>

Qui est responsable du contenu du cloud?

Lire cette page pour plus de détails: https://aws.amazon.com/compliance/shared-responsibility-model/    

Les responsabilités sont partagées entre AWS et le client:
- **responsabilités aws**: AWS exécute, gère et contrôle les composants depuis le système d’exploitation hôte jusqu’à la couche de virtualisation en passant par la sécurité physique des installations dans lesquelles le service s’exécute
- **responsabilités client**:  gestion du système d'exploitation invité (et notamment de l'installation des mises à jour et correctifs de sécurité), de tout autre logiciel d'application associé, ainsi que de la configuration du pare-feu du groupe de sécurité fourni par AWS


![aws responsability shared](./images/aws_solutionArchitectAssociate/awsResponsabilityShared.png)    


Peut-on agir sur cela à partir de la console de management aws?    
- Oui: security groups, IAM, mise à jours EC2 OS, database....    
- Non: manage les data centers, cables reseaux, caméras de sécurité, mises à jour RDS, OS....


➡️ Nous: tout ce qui est liés aux services choisis


Une action qui peut être "partagée" est l'encryption ➡️ nous pouvons encrypter nos données    
➡️ cela se fait avec le clic bouton dans la console (encrypted this volume) et aws doit s'assurer que l'encryption se fait correctement



=======================================================================================================================    



<span style="color: #EA9811">

###  **Compute, Storage, Database, Networking** 
</span>

Nous allons parler des différents types de services.    

........................................................................................................................

<span style="color: #11E3EA">

####  **COMPUTE** 
</span>

Il n'est pas possible d'avoir une application sans puissance de calcul: il faut analyser les données    

**Compute** est le moyen avec lequel nous procédons à l'information.    
- **EC2**
- **Lambda**
- **Elastic Beanstalk**


........................................................................................................................

<span style="color: #11E3EA">

####  **STORAGE** 
</span>

Sert à conserver les données: comme un disque géant dans le cloud.   
C'est un emplacement sûr pour conserver les données.    

- **S3**
- **EBS**
- **EFS**
- **FSx**
- **Storage Gateway**



........................................................................................................................

<span style="color: #11E3EA">

####  **DATABASES** 
</span>

Les DB sont d'immenses feuilles de calcul qui permettent de relier différents storages et retrouver les informations.    

- **RDS**
- **DynamoDB**
- **Redshift**



........................................................................................................................

<span style="color: #11E3EA">

####  **NETWORKING** 
</span>


Pour que les 3 types précédents (compute, storage, databases) puissent communiquer et vivre il faut un reseau pour les relier.    

- **VPCs**
- **Direct Connect**
- **Route 53**                  ➡️ DNS
- **API Gateway**
- **AWS Global Accelerator**

........................................................................................................................

![différents sujets de l'exam aws pour les types de services](./images/aws_solutionArchitectAssociate/awsExamTipsDiiferentTypesOfServices.png)    

=======================================================================================================================    

<span style="color: #EA9811">

###  **AWS What is the well-architected framework** 
</span>


Dans cette partie il est question des contenus décrits [dans cette page](https://aws.amazon.com/fr/whitepapers/?whitepapers-main.sort-by=item.additionalFields.sortDate&whitepapers-main.sort-order=desc&awsf.whitepapers-content-type=*all&awsf.whitepapers-global-methodology=*all&awsf.whitepapers-tech-category=*all&awsf.whitepapers-industries=*all&awsf.whitepapers-business-category=*all)  

Le framework permet de connaitre et comprendre les avantages et inconvénients lors de la construction d'infrastructure et surtout d'utiliser les bonnes pratiques.    

Cette page est à lire **imperativement** à la fin du cours: https://docs.aws.amazon.com/wellarchitected/latest/framework/welcome.html?did=wp_card&trk=wp_card    


Les 6 piliers du framework:
- **operational excellence**: running and monitoring systems                ➡️ https://docs.aws.amazon.com/wellarchitected/latest/operational-excellence-pillar/welcome.html?did=wp_card&trk=wp_card    
- **performance efficiency** (efficacité): using IT and computing resources efficiently  ➡️ https://docs.aws.amazon.com/wellarchitected/latest/performance-efficiency-pillar/welcome.html?did=wp_card&trk=wp_card    
- **security**: protecting informations et systems                          ➡️ https://docs.aws.amazon.com/wellarchitected/latest/security-pillar/welcome.html?did=wp_card&trk=wp_card    
- **cost optimization**: avoiding unnecessary costs                         ➡️ https://docs.aws.amazon.com/wellarchitected/latest/cost-optimization-pillar/welcome.html?did=wp_card&trk=wp_card    
- **reliability** (fiabilité): ensuring a workload performs                             ➡️ https://docs.aws.amazon.com/wellarchitected/latest/reliability-pillar/welcome.html?did=wp_card&trk=wp_card    
- **sustainability** (durabilité): minimizing the environmental impacts                  ➡️ https://docs.aws.amazon.com/wellarchitected/latest/sustainability-pillar/sustainability-pillar.html?did=wp_card&trk=wp_card    



=======================================================================================================================    

**QUIZZ CHAPTER 2**: 

![Quizz chapter 2 question 1](./images/aws_solutionArchitectAssociate/quizz_chapter2/quizz201.png)   
![Quizz chapter 2question 2](./images/aws_solutionArchitectAssociate/quizz_chapter2/quizz202.png)   
![Quizz chapter 2question 3](./images/aws_solutionArchitectAssociate/quizz_chapter2/quizz203.png)   
![Quizz chapter 2question 4](./images/aws_solutionArchitectAssociate/quizz_chapter2/quizz204.png)   
![Quizz chapter 2question 5](./images/aws_solutionArchitectAssociate/quizz_chapter2/quizz205.png)   


**QUIZZ REDO**

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/01.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/02.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/03.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/04.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/05.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/06.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/07.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/08.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/09.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/10.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/11.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/12.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/13.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/14.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/15.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/16.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/17.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/18.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/19.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/20.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/21.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/22.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/23.png)             
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/24.png)             




_______________________________________________________________________________________________________________________    


<span style="color: #CE5D6B">

##  **IAM: IDENTITY AND ACCESS MANAGEMENT** 
</span>

[Documentation AWS IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)    
[Documentation formation: determining whether a request is allowed or denied within an account](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_evaluation-logic.html#policy-eval-denyallow)    
[Documentation formation: overview json policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html#access_policies-json)    
[Documentation formation: aws Identity and Access Management](https://aws.amazon.com/iam/faqs/)     
[Documentation formation: ecample IAM identity-based policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_examples.html)    


=======================================================================================================================    

<span style="color: #EA9811">

###  **IAM AWS Secure the root account** 
</span>


.......................................................................................................................    

<span style="color: #11E3EA">

####  **IAM c'est quoi** 
</span>

**IAM**: Instance and Access Management    

- C'est un service web qui vous permet de contrôler l'accès aux ressources AWS.    
- Nous pouvons gérer de manière centralisée les autorisations qui contrôlent les ressources AWS auxquelles les utilisateurs peuvent accéder.    
- Nous pouvons contrôler les personnes qui s'authentifient (sont connectées) et sont autorisées (disposent d'autorisations) à utiliser des ressources.    

A la création d'un compte AWS, nous commençons avec **une seule identité** de connexion disposant d'un **accès complet à tous les Services AWS et ressources du compte**. Cette identité est appelée **root account** du Compte AWS.    
Nous pouvons y accéder en vous connectant à l'aide de l'adresse électronique et du mot de passe que vous avez utilisés pour créer le compte.   
**Il est vivement recommendé de NE PAS UTILISER ce compte pour les actions quotidiennes.**    
Pour connaitre les taches de l'account root: https://docs.aws.amazon.com/accounts/latest/reference/root-user-tasks.html  

![what is IAM](./images/aws_solutionArchitectAssociate/whatIsIAM.png)   


.......................................................................................................................    

<span style="color: #11E3EA">

####  **IAM Comprendre IAM** 
</span>


IAM commence avec le **root account**.



.......................................................................................................................    

<span style="color: #11E3EA">

####  **IAM Qu'est-ce qu'un account root** 
</span>

Le **root account** permet l'administration complete à AWS. Il est donc important de securiser celui-ci.    
Il ne s'agit que de l'email avec lequel on a fait la 1ere connexion lors de la création d'un compte AWS.


.......................................................................................................................    

<span style="color: #11E3EA">

####  **IAM Première connexion à IAM** 
</span>


Une page confluence a été faite pour ce sujet: https://confluence.gemalto.com/pages/viewpage.action?pageId=263693681     
[Connexion à AWS pour la premiere fois](./archives/formation/Cloud/aws_solutionArchitectAssociate/How-to_connect_AWS_for_the_first_time%20.pdf)    

![Exams tips Secure Your AWS Root Account](./images/aws_solutionArchitectAssociate/awsExamTipsSecureYourAwsRootAccount.png)    


.......................................................................................................................    



=======================================================================================================================    

<span style="color: #EA9811">

###  **IAM AWS Controlling Users' Actions with IAM Policy Documents** 
</span>


.......................................................................................................................  

<span style="color: #11E3EA">

####  **IAM Permission with IAM** 
</span>

Ces permissions sont dans un fichier json qui est appliqué.    


Exemple de policy document: 

```json
{
  "Version": "2012-10-17"
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
```

Ce fichier json donne **TOUS LES ACCES** depuis la console aws  ➡️ équivalent droit root     

Ce fichier json peut assigné à un (des) groupe(s), user(s), role(s).    



**Structure d'un fichier json gérant les policies**:

[Documentation aws json policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html#access_policies-json)    
![structure json for policies](./images/aws_solutionArchitectAssociate/jsonPoliciesStructure.png)     

- **version**: spécifier la version de policy à utiliser (la dernière est `2012-10-17`). [Page aws contenant les version de policy](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_version.html)    
- **Statement**: Utiliser cette policy principale comme container de ses éléments. Il est possible d'avoir plusieurs "statement" dans 1 policy     
- **Sid**: _(optionnel)_  inclu un statement ID pour pouvoir différentier les statements
- **Effect**: utiliser `Allow` ou `Deny` pour indiquer si la policy autorise ou bloque les accès    
- **Principal**: (_nécessaire seulement dans quelques circonstances_)  
    Si nous créons une **policy resource-based** il est **OBLIGATOIRE** d'indiquer l'**account, user, role ou federated user** pour qui nous souhaitons **allow** ou **deny** les accès.    
    Si nous créons une **policy IAM permission** pour attacher un user ou un role, nous ne pouvons pas inclure cet élement. Le _principal_ est sous-entendu comme user ou role    
- **Action**: inclu une liste d'actions que la policy va `Allow` ou `Deny`    
- **Resource**: (_nécessaire seulement dans quelques circonstances_)  
    Si nous créons une **policy IAM permissions** il est **OBLIGATOIRE** d'**indiquer une liste de ressources** pour lesquelles ces actions vont s'appliquer.    
    Si nous créons une **policy resource-based** cet élément est optionnel.    
    Si nous n'incluons pas cet élément alors la ressource pour qui l'action s'applique sera la ressource qui est attachée à la policy.     
- **Condition**:  _(optionnel)_  Spécifie les circonstances dépendantes des droits de la policy    

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "FirstStatement",
      "Effect": "Allow",
      "Action": ["iam:ChangePassword"],
      "Resource": "*"
    },
    {
      "Sid": "SecondStatement",
      "Effect": "Allow",
      "Principal": {"AWS": ["arn:aws:iam::account-id:root"]},
      "Action": "s3:ListAllMyBuckets",
      "Resource": [
        "arn:aws:s3:::mybucket",
        "arn:aws:s3:::mybucket/*"
      ]
    },
    {
      "Sid": "ThirdStatement",
      "Effect": "Allow",
      "Action": [
        "s3:List*",
        "s3:Get*"
      ],
      "Resource": [
        "arn:aws:s3:::confidential-data",
        "arn:aws:s3:::confidential-data/*"
      ],
      "Condition": {"Bool": {"aws:MultiFactorAuthPresent": "true"}}
    }
  ]
}
```



Pour tester cela se connecter dans la console aws, service IAM.    
On voit qu'avec notre compte "root account" nous avons comme zone définit "global"     



De plus notre compte n'a pas les accès aux différentes policies.    
![IAM list policies KO](./images/aws_solutionArchitectAssociate/iamListPoliciesKO.png)    


Pour cela il faut faire un switch role avec les role faisant parti de la page confluence: 
https://confluence.gemalto.com/display/COPS/Account+Management+-+Accounts+list#660063124b1620d8e25c945998e7f9f06c0f2842c 

account: **dev01-alpha-thales** (par exemple)                          
role: role choisit en fonction des actions à réaliser (pas les mêmes permissions)

![IAM Switch Role KO](./images/aws_solutionArchitectAssociate/IAMSwitchRoleKO.png)    

➡️ je ne peux pas changer de role dans la console ▶️ mon user ne fait pas partie du bon groupe sur aws: **le groupe qui gère la securité AWS dans l'entreprise** 

Demande au manager de le rajouter dans le groupe pour pouvoir avoir les accès et changer de role.

![Liste des policies accessibles dans aws](./images/aws_solutionArchitectAssociate/IAM_PoliciesList.png)    



.......................................................................................................................  

<span style="color: #11E3EA">

####  **IAM How Policy Documents work** 
</span>




En regardant dans les policies, celle qui se nomme **AdmintratorAccess** dans l'onglet _Permissions_ à le même fichier json que présenté précédement.    

![IAM Policy Administator Access](./images/aws_solutionArchitectAssociate/IamPolicyAdministratorAccess.png)    


Pour la partie certification il sera demandé de lire ou créer des fichiers de policies ➡️ se familiariser avec en en regardant un grand nombre et cherchant à quoi cela correspond.    


.......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **IAM AWS Permament IAM Credentials** 
</span>




.......................................................................................................................  

<span style="color: #11E3EA">

####  **IAM Building blocks of IAM** 
</span>

Il existe 3 types de block dans IAM:
- **Users**: personnes physiques
- **Groups**: fonctions de personnes (ex: dev, admin ....). Un groupe contient des users
- **Roles**: usage interne AWS

![IAM Building Blocks](./images/aws_solutionArchitectAssociate/IamBuildingBlocks.png)    



<span style="color: #965DCE">

#####  **IAM USERS, GROUPS, POLICIES** 
</span>


Les bonnes pratiques sont de faire hériter les persmissions des users depuis les groupes.


Dans les regles de Policies, aws a mis en place des "templates" à partir desquels ont peut se baser pour créer nos propres policies.    
Ces policies sont de types **Job Function**.    

Danc cette liste les policies qui n'ont pas l'icone aws sont des policies personalisées: elles ne sont pas managées par aws    

![IAM Diierent types of policies](./images/aws_solutionArchitectAssociate/IamDifferentTypesOfPolicies.png)  
- <span style="color: #2CD62A">**vert**: template de policies de type Job Function</span>
- <span style="color: #965DCE">**vlolet**: policies personalisées</span>


Par defaut, un user créé n'a aucun accès à aucune ressource: il faut l'associé à un groupe qui lui a des droits définis.    



.......................................................................................................................  

<span style="color: #11E3EA">

####  **IAM The principle of Least Privilege** 
</span>

Donner à un user les droits minimum pour que le job soit fait.    


.......................................................................................................................  

<span style="color: #11E3EA">

####  **IAM Roles: Identity provider, IAM Federation** 
</span>


[documentation aws](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers.html)    


**Identity provider** permet de gérer les users identity en dehors de aws.    

Un fournisseur d'identité (IdP) est un service qui stocke et vérifie l'identité des utilisateurs.    
Les IdP sont généralement des services hébergés dans le cloud qui travaillent souvent avec des fournisseurs d'authentification unique (SSO) pour authentifier les utilisateurs.    

Cela est très utile dans une organisation surtout lorsque'elle a déjà son systeme de gestion des identités.    
Pas besoin de créer une authentification personnalisée en utilsant **IdP** (identity provider), cela est géré automatique en utilisant [OpenId Connect (OIDC)](https://openid.net/connect/).    

Pour une connexion à l'AD il est utilisé le protocole SAML pour une connexion avec IdP.    


.......................................................................................................................  

<span style="color: #11E3EA">

####  **IAM Roles: Identity-based policies, resource-based policies** 
</span>


[Documentation aws ](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_identity-vs-resource.html)    

Une **policy** est un objet aws qui, quand associée à une identité ou resource, définit leurs permissions.    


**AWS gère 6 types de politiques**: https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/access_policies.html     

* **Identity-based policies** (Politiques basées sur l'identité) : attachez des politiques gérées et en ligne à des identités IAM (utilisateurs, groupes auxquels appartiennent des utilisateurs ou rôles). Les politiques basées sur l'identité accordent des autorisations à une identité.

* **Resource-based policies** (Politiques basées sur les ressources) : attachez des politiques en ligne aux ressources. Les exemples les plus courants de politiques basées sur les ressources sont les policies de Amazon S3 bucket et les politiques de confiance de rôle IAM. Les politiques basées sur des ressources accordent des autorisations au principal qui est spécifié dans la politique. Les principaux peuvent être dans le même compte que la ressource ou d'autres comptes.

* **Permissions boundaries** (Limites d'autorisations) : utilisez une politique gérée en tant que limite d'autorisations pour une entité IAM (utilisateur ou rôle). Cette politique définit les autorisations maximales que les politiques basées sur une identité peuvent accorder à une entité, mais n'accorde pas d'autorisations. Les limites d'autorisations ne définissent pas les autorisations maximales qu'une politique basée sur les ressources peut accorder à une entité.

* **Organizations SCPs** (Politiques de contrôle des services Organizations) : utilisez une politique de contrôle des services (SCP) AWS Organizations pour définir les autorisations maximales pour les membres du compte d'une organisation ou d'une unité d'organisation (OU). Les politiques de contrôle des services limitent les autorisations que les politiques basées sur l'identité ou sur une ressource accordent à des entités (utilisateurs ou rôles) au sein du compte, mais n'accordent pas d'autorisations.

* **Access control lists (ACLs)** (Listes de contrôle d'accès [ACL]) : utilisez les listes de contrôle d'accès (ACL) pour contrôler quels principaux dans d'autres comptes peuvent accéder à la ressource à laquelle la liste de contrôle d'accès (ACL) est attachée. Les listes de contrôle d'accès sont semblables aux politiques basées sur les ressources, bien qu'elles soient le seul type de politique qui n'utilise pas la structure d'un document de politique JSON. Les listes de contrôle d'accès sont des politiques d'autorisations entre comptes qui accordent des autorisations pour le principal spécifié. Les listes de contrôle d'accès ne peuvent pas accorder des autorisations aux entités au sein du même compte.

* **Session policies** (Politiques de session) : transmettez des politiques de session avancées lorsque vous utilisez AWS CLI ou l'API AWS pour endosser un rôle ou un utilisateur fédéré. Les politiques de session limitent les autorisations que les politiques basées sur l'identité du rôle ou de l'utilisateur accordent à la session. Les politiques de session limitent les autorisations d'une session créée, mais n'accordent pas d'autorisations. Pour plus d'informations, consultez Politiques de session.



[Documentation aws: liste des policies managées](https://docs.aws.amazon.com/aws-managed-policy/latest/reference/policy-list.html)   
[Documentation aws qui fonctionne avec IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html)     

.......................................................................................................................  


[Documentation aws: exemples de polcies IAM identity-based](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_examples.html)    
[Documentation aws: Creation de policies IAM](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/access_policies_create.html)    
[Documentation aws: creation de policies IAM par API](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/access_policies_create-api.html)    
[Documentation aws: Validation de polices IAM](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/access_policies_policy-validator.html)     
[Documentation aws: Analyser de validation de policies IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/access-analyzer-policy-validation.html)    
[Documentation aws: Syntaxe SCP](https://docs.aws.amazon.com/fr_fr/organizations/latest/userguide/orgs_manage_policies_scps_syntax.html)    


=======================================================================================================================    

![AWS Exam Tips IAM](./images/aws_solutionArchitectAssociate/awsExamTipsIam.png)    
![AWS Exam Tips IAM](./images/aws_solutionArchitectAssociate/awsExamTipsIam02.png)    
![AWS Exam Tips IAM](./images/aws_solutionArchitectAssociate/awsExamTipsIam03.png)    
![AWS Exam Tips IAM](./images/aws_solutionArchitectAssociate/awsExamTipsIam04.png)    




<span style="color: #EA9811">

###  **IAM LAB** 
</span>

![Lab IAM](./images/aws_solutionArchitectAssociate/IamLab.png)    
[Documentation AWS: IAM Roles](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html)    



....................................................................................................................... 

<span style="color: #11E3EA">

#####  **IAM Lab1:create Amazon IAM policies** 
</span>

Au début du lab nous avons une configuration de départ sur aws.     
L'objectif sera de modifier cette configuration en créant une policy IAM.    

![Configuration IAM au début du Lab](./images/aws_solutionArchitectAssociate/labs/lab1_IAM/IamConfigurationAtThebeginingOfTheLab.png)    

Objectifs de fin de lab:
![Configuration IAM à la fin du lab: objectif à atteindre](./images/aws_solutionArchitectAssociate/labs/lab1_IAM/IamConfigurationObjectivesAtTheEndOfTheLab.png)    
  

Account ID : 301236818234    
user_name: cloud_user    
password: &9Flbqxjiwyxeyf    

 [Lien Lab1 IAM](https://740121593245.signin.aws.amazon.com/console?region=us-east-1)


pour les users dev (dans un autre navigateur en incognito)

Account ID : 301236818234     
dev1 password: 3Kk6!AY36^5h1rolJYb@C    
dev2 password: 3Kk6!AY36^5h1rolJYb@C    
dev3 password: 3Kk6!AY36^5h1rolJYb@C    


![Connexion Lab1 user cloud_user](./images/aws_solutionArchitectAssociate/labs/lab1_IAM/connexionCloudUser.png)  
![Connexion Lab1 user dev](./images/aws_solutionArchitectAssociate/labs/lab1_IAM/connexionDevUser.png)  


<span style="color: #CE5D6B"> ⚠ </span> Si on refait le lab: le account ID ne sera pas le même ➡️ le noter pour pouvoir refaire avec les users

....................................................................................................................... 

<span style="color: #11E3EA">

#####  **IAM Lab2** 
</span>


![Lab2 IAM](./images/aws_solutionArchitectAssociate/labs/lab2_IAM/sujetLab2_Iam.png)    

![Sujet lab 2 IAM](./images/aws_solutionArchitectAssociate/labs/lab2_IAM/actionsLab2Iam.png)    

![connexion lab 2 IAM](./images/aws_solutionArchitectAssociate/labs/lab2_IAM/connexionLab2_Iam.png)    

account ID: 071996341712    
username: cloud_user    
password: &8Wvvggljluhfrz    

[lien lab2 IAM](https://071996341712.signin.aws.amazon.com/console?region=us-east-1)    

user-1 password: 3Kk6!AY36^5h1rolJYb@C     
user-2 password: 3Kk6!AY36^5h1rolJYb@C     
user-3 password: 3Kk6!AY36^5h1rolJYb@C     


Ces 2 labs sont à refaire afin de comprendre le fonctionnement des policies et comment les affilier à des groupes ou des ressources PUIS affilier des users à des groupes.


....................................................................................................................... 

<span style="color: #11E3EA">

#####  **IAM Quizz** 
</span>

![Quizz IAM question1](./images/aws_solutionArchitectAssociate/quizz_chapter3-IAM/quizzIam_q1.png)    
![Quizz IAM question2](./images/aws_solutionArchitectAssociate/quizz_chapter3-IAM/quizzIam_q2.png)    
![Quizz IAM question3](./images/aws_solutionArchitectAssociate/quizz_chapter3-IAM/quizzIam_q3.png)    
![Quizz IAM question4](./images/aws_solutionArchitectAssociate/quizz_chapter3-IAM/quizzIam_q4.png)    
![Quizz IAM question5](./images/aws_solutionArchitectAssociate/quizz_chapter3-IAM/quizzIam_q5.png)    





**QUIZZ REDO**        
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/25.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/26.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/27.png)         



....................................................................................................................... 

=======================================================================================================================    

_______________________________________________________________________________________________________________________    




<span style="color: #CE5D6B">

##  **S3: SIMPLE STORAGE SERVICE** 
</span>


Un des plus vieux service de aws.    


[Documentation aws S3](https://docs.aws.amazon.com/s3/)   


.......................................................................................................................   

<span style="color: #11E3EA">

####  **Qu'est-ce que S3?** 
</span>

**S3**: **Simple Service Storage**    
![Définition de S3](./images/aws_solutionArchitectAssociate/s3_definition.png)    


S3 est un **objet-based storage**.    
Il permet de gérer les données en tant qu'objets plutôt qu'en tant que fichiers de systeme ou blocks de données.    

C'est un moyen de conserver nos données (static files):
- **upload un fichier** (photos, videos, code, documents, fichier system, fichiers texte)
- **NE PEUT PAS ETRE UTILISER POUR** run un OS ou une database


 






.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 basics** 
</span>


Le nombre d'objets qui peut être conservés est illimité    
Un objet peut avoir une taille de 0 bytes à 5 terabytes   
Les objets sont conservés dans un bucket (proche d'un répertoire)     




.......................................................................................................................   

<span style="color: #11E3EA">

####  **Travailler avec S3 bucket?** 
</span>


**Universal namespace**: tous les accounts aws partage le S3 namespace. Chaque S3 bucket a un nom unique.    
➡️ exemple d'url S3:

```bash
# syntaxe
https://bucket-name.s3.Region.amazonaws.com/key-name

## exemple
https://acloudguru.s3.us-east-1.amazonaws.com/Ralphie.jpg
```



Lors de l'upload d'un fichier dans S3 bucket, **si l'upload est OK on reçoit un code http 200**


.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Key-value store** 
</span>

![S3 key-value store](./images/aws_solutionArchitectAssociate/s3-keyValueStore.png)    









.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Disponibilité et durabilité** 
</span>


Les données sont partagées à travers de multiples devices    
➡️ disponibilité: 99.99% (construit pour au moins 99.95%)    
➡️ durabibilité: 99.99999999% ➡️ les données ne sont pas supprimées ni perdues







.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Caractéristiques de S3** 
</span>

Standard S3 est S3 "classique" avec ses caractéristiques:

High availability and durability     
Design for frequent access   (accès fréquents aux données)    
Adaptés à la plupart des charges de travail (utilisé aussi pour l'analyse de big data, plateforme de jeux vidéos...)    

S3 peut s'adapter aux uses cases auxquels on le confronte.


.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Sécuriser les données** 
</span>

- **Server-Side encryption**: on peut définir par defaut une encryption dans un bucket de tous les nouveaux objets du bucket    
- **ACL**: un accès en fonction des groupes ou accounts. Ils peuvent être attachés à un objet seul dans 1 bucket    
- **bucket policies**: les policies spécifiques S3 bucket vont spécifier quelles actions sont autorisées ou refusées sur S3

Les buckets policies ressemblent aux IAM policies: fichier json qui donnent des autorisations ou interdiction de la même manière.    



.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Forte cohérence entre lecture et écriture** 
</span>

- après une écriture réussie: cela correspond à un **PUT** ➡️ la prochaine requête de lecture va afficher la dernière version de l'objet    
- forte cohérence: pour la liste des opérations ➡️ après l'écriture il est possible d'obtenir la liste des objets avec les changements effectués    




.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Exam tips** 
</span>


![S3 exam tips 001](./images/aws_solutionArchitectAssociate/awsExamTipsS3-001.png)       
![S3 exam tips 002](./images/aws_solutionArchitectAssociate/awsExamTipsS3-002.png)       
![S3 exam tips 003](./images/aws_solutionArchitectAssociate/awsExamTipsS3-003.png)       
 




.......................................................................................................................   


=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 SECURING YOUR BUCKET WITH S3 BLOCK PUBLIC ACCESS** 
</span>


.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 ACLs vs Bucket Policies** 
</span>

Quelles sont les différences entre les ACLs et les Bucket Policies ?

Les **ACLs (Access Control List)** sont utilisées pour 1 objet SEUL.

Les **Bucket Policies** sont utilisées pour généraliser les regles à tout S3 Bucket.

![ACLs vs Bucket Policies](./images/aws_solutionArchitectAssociate/s3_aclVsBucketPolicies.png)    


Par defaut à la creation d'un bucket privé, les accès public ne sont pas autorisés    
  ➡️ il faut les modifier en ajoutant des ACLs public si nous voulons intervenir sur 1 objet individuel    
  ➡️ il faut modifier les Bucket Policies si on veut mettre la règle sur le bucket entier  

Une fois les fichiers uploadés dans le bucket, un retour HTTP 200 se fait pour confirmation  

![awsExamTipsS3Bucket-011](./images/aws_solutionArchitectAssociate/awsExamTipsS3Bucket-011.png)    




.......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 HOSTING A STATIC WEBSITE USING S3** 
</span>



.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Static website on S3** 
</span>


Il est possible d'héberger un site statique sur S3 (pages html).    
Un site dynamique doit accéder à une database ➡️ PAS POSSIBLE   







.......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Automatic scaling** 
</span>


S3 met son échelle à jour de manière automatique en fonction du nombre de requêtes qu'il reçoit.     



Comment mettre en place un site static?    

1. [récupérer les fichiers pour la démo](./archives/formation/Cloud/aws_solutionArchitectAssociate/S3website/)    

Bucket policies json:    
```json
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "PublicReadGetObject",
			"Effect": "Allow",
			"Principal": "*",
			"Action": [
				"s3:GetObject"
			],
			"Resource": [
				"arn:aws:s3:::BUCKET_NAME/*"      ## mettre le nom de notre bucket sur cette ligne
			]
		}
	]
}
```

Puis les pages html (index.html et error.html)   
```html
<html>
	<head>
		<title>Hello Cloud Gurus</title>
	</head>
	<body>
		<div align="center">
			<h1>Hello Cloud Gurus!</h1>
			<img src="https://s3.amazonaws.com/acloudguruimages/ACG.jpg">
		</div>
	</body>
</html>
```

```html
<html>
	<head>
		<title>Error Cloud Gurus</title>
	</head>
	<body>
		<div align="center">
			<h1>Sorry Cloud Gurus, there has been an error!</h1>
			<img src="https://s3.amazonaws.com/acloudguruimages/acg2.png">
		</div>
	</body>
</html>
```

▶️ donc un site très simple

➡️ se connecter sur aws console, aller dans S3, créer un bucket, 
    aller dans ce bucket, properties, éditer "static website hosting": mettre à enable    
      ajouter les noms des pages index.html et error.html dans les champs pour cela     
      ▶️ cela génère une url externe


➡️ uploader les fichiers html
➡️ ajouter la bucket policy : bucket, permission, edit bucket policy, copié et collé le contenu du fichier et modifiant la ligne 12 (arn) qui se trouve au dessus de la fenêtre pour éditer cette policy    

![s3-editBucketPolicy_arn.png](./images/aws_solutionArchitectAssociate/s3-editBucketPolicy_arn.png)    

▶️ le site est accessible

![awsExamTipsS3Bucket-012.png](./images/aws_solutionArchitectAssociate/awsExamTipsS3Bucket-012.png)    





.......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 VERSIONING OBJECTS IN S3** 
</span>





......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 What is versioning?** 
</span>


Il est possible d'avoir plusieurs versions d'un même objet S3 bucket










......................................................................................................................   

<span style="color: #11E3EA">

####  **S3 Advantages of versioning** 
</span>

![s3-avantageOfVersioning.png](./images/aws_solutionArchitectAssociate/s3-avantageOfVersioning.png)    


Dans le bucket, nous allons dans properties et faisons un edit de bucket versioning    
➡️ le mettre à enable    


en uploadant une mise à jour d'un fichier, la nouvelle version sera pris en compte   
Il est possible de voir les anciennes versions en les sélectionnant    



![awsExamTipsS3Bucket-013](./images/aws_solutionArchitectAssociate/awsExamTipsS3Bucket-013.png)    



......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 STORAGE CLASSES** 
</span>


Les différentes storage classes de S3:
- S3 Standard
- S3 standard-Infrequent Access
- S3 One Zone-Infrequent Access
- S3 Intelligent-Tiering
- S3 Glacier
- Performance accross S3
- Storage Costs

......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 Standard** 
</span>

![s3Standard_caracteristiques](./images/aws_solutionArchitectAssociate/s3Standard_caracteristiques.png)    

Si on pense à stocker une image, video, musique, documents statiques ▶️ penser S3 Standard





......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 standard-Infrequent Access** 
</span>

![s3StandardInfrequentAccess_caracteristique](./images/aws_solutionArchitectAssociate/s3StandardInfrequentAccess_caracteristiques.png)    

Utilisé pour des accès rapides mais peu fréquent ➡️ exemples: archives, Desaster recovery...    
Cette solution est payante.    


......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 OneZone InfrequentAccess** 
</span>

![s3_OneZoneInfrequentAccess](./images/aws_solutionArchitectAssociate/s3_OneZoneInfrequentAccess.png)    

Cela est utilisé pour des données qui sont très peu utilisées et non critiques ➡️ 20% moins cher    




......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 Intelligent-Tiering** 
</span>


Dans le cas où l'utilisation des données sont parfois fréquentes (S3 Standard), parfois non (S3 Standard-Infrequent Access): utiliser S3 Intelligent-Tiercing.    


Cela permet un gain d'argent




......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 Glacier** 
</span>


Lieu d'archives des données sur long terme.   
Il y a 3 options:
- **glacier instant retrieval**: accès aux datas en quelques millisecondes
- **glacier flexible retrieval**: accès aux datas en quelques minutes à 12h (moins cher)
- **glacier deep archive**:  accès aux datas en 12 à 48h (le moins cher) 


![s3_glacier](./images/aws_solutionArchitectAssociate/s3_glacier.png)    




......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 Performance accross S3** 
</span>


![s3_performance](./images/aws_solutionArchitectAssociate/s3_performance.png)    



......................................................................................................................  

<span style="color: #11E3EA">

####  **S3 Storage Costs** 
</span>


![s3_costs](./images/aws_solutionArchitectAssociate/s3_costs.png)    

![s3 costs glacier](./images/aws_solutionArchitectAssociate/s3_costs_glacier.png)    




![s3 exam tips](./images/aws_solutionArchitectAssociate/awsExamTipsS3Bucket-014.png)    



......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 LIFECYCLE MANAGEMENT WITH S3** 
</span>


[Documentation aws: Transitioning objects using Amazon S3 Lifecycle](https://docs.aws.amazon.com/AmazonS3/latest/userguide/lifecycle-transition-general-considerations.html)    
[Documentation aws: Amazon S3 Storage Classes](https://aws.amazon.com/s3/storage-classes/)    


.......................................................................................................................        

<span style="color: #11E3EA">

####  **S3 What is LifeCycle Management ?** 
</span>

![s3-lifecyclaManagement](./images/aws_solutionArchitectAssociate/s3-lifecyclaManagement001.png)    

Ce cycle de vie permet de migrer de manière automatique d'un service S3 à un autre (vers de la solution moins chère dans le temps). Plus on avance dans le temps, moins nous avons besoin de lire les datas régulièrements, donc nous pouvons migrer vers d'autres services moins couteux.    


Au bout de 30 jours sans activités ➡️ migrer vers un autre service  ➡️ au bout de 90 jours sans activités sur ce 2e service ➡️ migre vers S3 glacier




.......................................................................................................................        

<span style="color: #11E3EA">

####  **S3 LifeCycle Management and Versioning** 
</span>


De la même manière ce management peut-être fait en fonction du versioning des datas.
Ainsi seules les releases les plus récentes seront sur des services plus chers.    



.......................................................................................................................     


Pour mettre cela en place, dans la console aws/S3/sélectionner le bucket/properties/edit bucket versioning (mettre à enable)
  ➡️ le versioning est mis en place 

Maintenant aller dans la console aws/S3/sélectionner le bucket/management/create a lifecycle rule/
  - donner un nom à cette regle
  - choix si on applique à tous les objets du buckets ou si on limite son scope (fichier, dossier, ...)
  - lifecycle rule actions: on définit les actions que cette regle va mettre en place
  - transition current versions of objetcs between storage classes: définir une durée à partir de laquelle la regle va s'exécuter depuis la création du fichier (c'est là qu'on définit les temps de rétentions entre les différents services)  

> *Remarque*: lorsqu'on choisit les différents services une aide pour la durée est définie

![exam tips s3-lifeCyclaManagement002-examTips.png](./images/aws_solutionArchitectAssociate/s3-lifeCyclaManagement002-examTips.png)    






.......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 OBJECT LOCK AND GLACIER VAULT LOCK** 
</span>

[Documentation aws: S3 Object Lock](https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-lock.html)    


.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Object Lock** 
</span>


**S3 Object Lock** est utilisé comme magasins d'objets qui ont pour usage: **writing once, read many (WORM)**.    
Cela permet de se prémunir comme l'effacement ou modification d'objets inopinés.    



.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Governance mode** 
</span>


Dans ce mode de gouvernance il n'est pas possible aux users d'effacer ou modifier 1 objet SANS modifier les parametres du **lock** ➡️ il faut avoir les autorisations nécessaires.    

Avec cette gouvernance, nous protégeons les objets contre la plupart des utilisateurs MAIS certains ont des habilités pour modifier des paramètres et/ou effacer les objets.







.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Compliance mode** 
</span>


Dans ce mode AUCUN user ne peut supprimer ou modifier un objet, y compris le root user.    
Quand 1 objet est mis dans ce mode, l'objet et sa rétention ne peut être modifié durant la période définie.



.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Retention Periods** 
</span>

C'est la durée de la période définie pour un objet pour sa rétention "en l'état".    
A la création de cette période un timestamp est généré avec les metadata comprenant la durée de cette rétention.

Après cette durée, l'objet peut être modifié ou supprimé SAUF si une conservation légale est mise en place sur la version.






.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Legal Holds** 
</span>


S3 Object Lock permet aussi de mettre en place une conservation légale sur une version d'objet.    
Cela prévient de la suppression ou modification de l'objet dans cette version.






.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Glacier Vault Lock** 
</span>


**S3 Glacier Vault Lock** permet de déployer et d'appliquer des contrôles de conformité pour un coffre S3 Glacier individuel avec une stratégie de verrouillage de coffre.   

Il s'agit d'un _worm_ modèle pour glacier.   





![exam Tips 01](./images/aws_solutionArchitectAssociate/s3-ObjectLock-examTips01.png)    
![exam Tips 02](./images/aws_solutionArchitectAssociate/s3-ObjectLock-examTips02.png)    
![exam Tips 03](./images/aws_solutionArchitectAssociate/s3-ObjectLock-examTips03.png)    

.......................................................................................................................     

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 ENCRYPTING S3 OBJECTS** 
</span>

[Documentation aws: Encrypting S3 Object](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/UsingEncryption.html)    

>_Remarque_: Depuis le 5 janvier 2023 par defaut tous les nouveaux objets s3 sont encryptés

.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Types of encryption** 
</span>


<span style="color: #11E3EA">

####  **S3 Encryption en transit** 
</span>

Cette encryption se fait via les certificats SSL/TLS et/ou le port 443 avec le protocole HTTPS.    


<span style="color: #11E3EA">

####  **S3 Encryption au repos: Server-Side encryption** 
</span>

- **SSE-S3**: utilise des S3-managed keys avec AES 256-bit 
- **SSE-KMS**: aws Key Management Service-managed keys
- **SSE-C**: Customer-provider keys


<span style="color: #11E3EA">

####  **S3 Encryption au repos: Client-Side encryption** 
</span>

Les fichiers sont encryptés avant d'être uploadés dans S3




.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Enforcing Server-Side Encryption** 
</span>


Il existe 2 méthodes pour le faire: 
- **aws console**: sélectionner les parametres de l'encryption de S3 bucket
- **bucket policy**: forcer l'utilisation d'une bucket policy dur le bucket S3

Le fait de créer une `encryption Server-side` pour les uploads sur S3 correspond à une **requête PUT**.    

Comment cela se passe-t-il?

1. un fichier `x-amz-server-side-encryption` est créé

2. Ce fichier comporte 2 options:
- `x-amz-server-side-encryption` a une encryption AES256 (correspond à SSE-S3)
- `x-amz-server-side-encryption` a une encryption aws:kms (SSE-KMS)

3. Header de la requête PUT: ce fichier est inclus dans le header avec ses parametres, cela dit à S3 d'encrypter l'objet lors de l'upload avec la méthode définie


![exam tips](./images/aws_solutionArchitectAssociate/s3_Encryption-examTips.png)    



.......................................................................................................................    


=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 OPTIMIZING S3 PERFORMANCE** 
</span>



[Documentation aws: difference between prefixes and nested folders](https://repost.aws/knowledge-center/s3-prefix-nested-folders-difference)    
[Documentation aws: request quotas](https://docs.aws.amazon.com/kms/latest/developerguide/requests-per-second.html)    



.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 prefixes** 
</span>


Cela permet d'optimiser les performances aws.    

Qu'est-ce qu'un prefixe?    
A la création d'un bucket on peut avoir des dossiers et sous dossiers avant d'arriver à l'objet qui est présent dans ce bucket.    

_exemples_: 
1. `mybucketname/folder1/subfolder1/myfile.jpg`    
  ▶️ le prefixe correspond à l'arborescence depuis le bucket jusqu'à l'objet: `/folder1/subfolder1`
2. `mybucketname/folder2/subfolder1/myfile.jpg`    
  ▶️ le prefixe correspond à l'arborescence depuis le bucket jusqu'à l'objet: `/folder2/subfolder1`
3. `mybucketname/folder3/myfile.jpg`    
  ▶️ le prefixe correspond à l'arborescence depuis le bucket jusqu'à l'objet: `/folder3`




.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 performance** 
</span>



Comment optimer les performances S3 bucket?    
S3 a des latences faibles: il est possible de récupérer le 1er byte en environ 100 à 200 millisecondes.    

Il est possible d'atteindre **3500 PUT/COPY/POST/DELETE requêtes** ET **5500 GET/HEAD requête par seconde et par préfixe**    

➡️ **plus il y a de prefixes dans notre bucket meilleures seront les performances de notre S3 bucket**. 




Il est possible d'amémiorer la performance en lecture à travers différents prefixes.    
➡️ par exemples en utilsant 2 prefixes on peut doubler le nombre de requêtes par secondes (5500 ➡️ 11000 requetes)    
  


.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 Limitation with KMS** 
</span>



- en utilisant **SSE-KMS** pour encrypter les objets dans S3 il est nécessaire de **garder en tête les limitations KMS**    

[Documentation aws: quotas KMS](https://docs.aws.amazon.com/kms/latest/developerguide/limits.html)    


- lors de l'**upload d'un fichier**, un appel **GenerateDataKey** dans l'**API KMS** se fait 

[Documentation aws: generatedatakey](https://docs.aws.amazon.com/kms/latest/APIReference/API_GenerateDataKey.html)    


- lors d'un **download de fichier**, un appel **Decrypt** de l'**API KMS** se fait

[Documentation aws: decrypt](https://docs.aws.amazon.com/fr_fr/kms/latest/APIReference/API_Decrypt.html)    





Les taux de requêtes KMS:    

- upload/download fichiers sont "bloqués" par les quotas KMS       
- il n'est pas possible d'augmenter un quota KMS    
- ces taux sont dépendants des regions (region-specific): ils varient de 5500, 10000 ou 30000 requêtes par secondes    




.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 performance uploads** 
</span>



Comment optimiser un upload?   

Nous parlons ici de **multipart uploads**: 

- recommendé pour les fichiers de plus de 100Mb    
- obligatoire pour les fichiers de plus de 5Gb     
- il est possible de paralléliser les uploads   

Le principe: 1 gros fichier est "découpé" en plusieurs petits fichiers qui sont uploadés en parallèle puis la "reconstruction" du gros fichier se fait 

![multipart uploads](./images/aws_solutionArchitectAssociate/s3_performanceUpload_MultipartUploads.png)            





.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 performance downloads** 
</span>


De manière similaire au Multpart Uploads, il existe le **S3 Byte-range fetches** pour le download    

- parralélisation des downloads en spécifiant les byte ranges (on dit qu'on ne veut que la partie de 100mb à 180mb du fichier par exemple)    
➡️ en cas d'erreur seulement sur ce range l'erreur se produira 

![S3 Bytes-ranges fetches](./images/aws_solutionArchitectAssociate/s3-S3_byte-range-fetches.png)    


Ce principe peut donc être utilisé pour augmenter la vitesse de download ET/OU pour de faire qu'un download partiel de fichier.    

.......................................................................................................................   

![s3 performance examTips 001](./images/aws_solutionArchitectAssociate/s3_examTips_s3performance001.png)    
![s3 performance examTips 002](./images/aws_solutionArchitectAssociate/s3_examTips_s3performance002.png)    
![s3 performance examTips 003](./images/aws_solutionArchitectAssociate/s3_examTips_s3performance003.png)    





.......................................................................................................................    



=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 BACKING UP DATA WITH S3 REPLICATION** 
</span>


[Documentation aws: replicate existing objects with amazon S3 batch replication](https://aws.amazon.com/blogs/aws/new-replicate-existing-objects-with-amazon-s3-batch-replication/)    
[Documentation aws: replication d'objets](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/replication.html)    
[Documentation aws: certifications FAQs](https://aws.amazon.com/certification/faqs/)    



.......................................................................................................................    

<span style="color: #11E3EA">

####  **S3 replication** 
</span>



Un moyen de replication d'un objet d'un bucket vers un autre bucket    
  ➡️ le versionning est très important dans ce cas 

les objets existant dans 1 bucket ne sont pas répliquer automatiquement    
  ➡️ **il faut déclencher la replication d'un bucket** et à ce moment tous les objets seront repliqués (pas les objets déjà existants si le cas se propose, seulement les nouveaux ou les nouvelles versions)       
  ➡️ il est donc conseillé dans ce cas de **créer 2 buckets**: **1 source bucket** et **1 destination bucket**

Il faut supprimer les marqueurs qui ne seront pas répliquer par défaut    
  ➡️ supprimer les versions individuelles ou les marqueurs qui ne seront pas repliqués   






- Après la creation des 2 buckets (source et destination; pas forcément dans la même region ➡️ suivant son choix), on upload un fichier dans le bucket source.     

- Pour mettre en place la replication aller dans le bucket source/management/replication rule (create a replication rule)    
➡️ il est mentionné ici (si cela n'est pas fait lors de la création du bucket) que le versionning est obligatoire   
➡️ mettre en place le versionning (enable)    

- **donner un nom** "clair" à la regle de replication    
- **source bucket** ➡️ définir une limite de scope ou si on replique tous les objets du bucket    
- **destination** ➡️ definir le bucket destination (là aussi si le versionning n'est pas activé cela sera demandé)    
- **iam role** ➡️ choisir l'option **create new role** ▶️ cela créera un role automatiquement    
- **encryption** ➡️ choisir si nous encryptons les replications   
- **destination storage class** ➡️ à définir si on souhaite changer ou non les storages classes    
- **additional replication options** ➡️ définir si cocher `delete marker replication`    


> *Remarque*: une fois cette creation de regle de replication si nous allons sur le bucket cible nous constatons que le fichier précédemment uploadé n'est pas répliqué  ➡️ il y a donc des actions complémentaires à faire    


Si nous ré uploadons le même fichier sur le bucket source ➡️ dans ce cas la replication va se faire (nouvelle version pour aws)   



> *Remarque*: Lors de la configuration de le replication nous n'avons pas coché cette condition: `delete marker replication`    
  ➡️ cela implique que si nous supprimons un objet dans la bucket source, celui-ci ne sera pas supprimé dans le bucket destination    




.......................................................................................................................    

![s3_examTips_replication](./images/aws_solutionArchitectAssociate/s3_examTips_replication.png)


.......................................................................................................................    

**S3 RESUME INFOS A CONNAITRE SUR S3 POUR LA CERTIFICATION**

![s3_examTips_summary001](./images/aws_solutionArchitectAssociate/s3_examTips_summary001.png)    
![s3_examTips_summary002](./images/aws_solutionArchitectAssociate/s3_examTips_summary002.png)    
![s3_examTips_summary003](./images/aws_solutionArchitectAssociate/s3_examTips_summary003.png)    
![s3_examTips_summary004](./images/aws_solutionArchitectAssociate/s3_examTips_summary004.png)    
![s3_examTips_summary005](./images/aws_solutionArchitectAssociate/s3_examTips_summary005.png)    
![s3_examTips_summary006](./images/aws_solutionArchitectAssociate/s3_examTips_summary006.png)    
![s3_examTips_summary007](./images/aws_solutionArchitectAssociate/s3_examTips_summary007.png)    
![s3_examTips_summary008](./images/aws_solutionArchitectAssociate/s3_examTips_summary008.png)    
![s3_examTips_summary009](./images/aws_solutionArchitectAssociate/s3_examTips_summary001.png)    
![s3_examTips_summary010](./images/aws_solutionArchitectAssociate/s3_examTips_summary010.png)    
![s3_examTips_summary011](./images/aws_solutionArchitectAssociate/s3_examTips_summary011.png)    
![s3_examTips_summary012](./images/aws_solutionArchitectAssociate/s3_examTips_summary012.png)    
![s3_examTips_summary013](./images/aws_solutionArchitectAssociate/s3_examTips_summary013.png)    
![s3_examTips_summary014](./images/aws_solutionArchitectAssociate/s3_examTips_summary014.png)    
![s3_examTips_summary015](./images/aws_solutionArchitectAssociate/s3_examTips_summary015.png)    
![s3_examTips_summary016](./images/aws_solutionArchitectAssociate/s3_examTips_summary016.png) 

....................................................................................................................... 

=======================================================================================================================    

<span style="color: #EA9811">

###  **S3 LABS** 
</span>



....................................................................................................................... 

<span style="color: #11E3EA">

####  **S3 Lab1** 
</span>

![enonce s3 lab1](./images/aws_solutionArchitectAssociate/s3_lab1.png)    

Mise en place d'une replication S3 inter region



aws account:    
username: cloud_user     
password: %6Bukpjbbuhahjs    
lien vers le lab: https://367869946008.signin.aws.amazon.com/console?region=us-east-1    


![s3 lab 1: exercice à faire](./images/aws_solutionArchitectAssociate/labs/s3_lab1/s3_lab1.png)    
 


....................................................................................................................... 

<span style="color: #11E3EA">

####  **S3 Lab2** 
</span>


![enonce s3 lab2](./images/aws_solutionArchitectAssociate/s3_lab2.png)    

Créer un site web static

aws account:    
username: cloud_user     
password: %4Twcwqnybnmqnp         
lien vers le lab: https://856464788233.signin.aws.amazon.com/console?region=us-east-1     

github repository pour les fichiers nécessaires au lab: https://github.com/ACloudGuru-Resources/Course-Certified-Solutions-Architect-Associate/tree/master/labs/creating-a-static-website-using-amazon-s3  


![s3 lab 2: exercice à faire](./images/aws_solutionArchitectAssociate/labs/s3_lab2/s3_lab2.png)    


bucket policy:    

```json
{
    "Version": "2012-10-17",
    "Id": "Policy1645724938586",
    "Statement": [
        {
            "Sid": "Stmt1645724933619",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "<BUCKET_ARN>/*"
        }
    ]
}
```

> *Remarque*: mettre en place l'arn du bucket (qui est founri par aws lorsqu'on clique sur `bucket/permissions/edit bucket policy`) à la place de ce qui est entre chevrons ("<BUCKET_ARN>")    
<span style="color: #CE5D6B"> ⚠ </span> conserver le "/*" à la fin de la ligne pour autoriser sur TOUT le contenu de la ressource l'accès 


....................................................................................................................... 

<span style="color: #11E3EA">

####  **S3 Lab3** 
</span>


![enonce s3 lab3](./images/aws_solutionArchitectAssociate/s3_lab3.png)    

Créer un bucket S3 amanzon, avec gestion d'objets et versionning

aws account:    
username: cloud_user     
password: %#2Beqsgsmbzmjyg         
lien vers le lab: https://734854643950.signin.aws.amazon.com/console?region=us-east-1   

line github pour fichiers nécessaires au lab: https://734854643950.signin.aws.amazon.com/console?region=us-east-1  


![s3 lab 3: exercice à faire](./images/aws_solutionArchitectAssociate/labs/s3_lab3/s3_lab3.png)  



....................................................................................................................... 

=======================================================================================================================  


<span style="color: #11E3EA">

###  **S3 Quizz** 
</span>



![S3 quizz question 1](./images/aws_solutionArchitectAssociate/s3_quizz/s3_quizz001.png)     
![S3 quizz question 2](./images/aws_solutionArchitectAssociate/s3_quizz/s3_quizz002.png)     
![S3 quizz question 3](./images/aws_solutionArchitectAssociate/s3_quizz/s3_quizz003.png)     
![S3 quizz question 4](./images/aws_solutionArchitectAssociate/s3_quizz/s3_quizz004.png)     
![S3 quizz question 5](./images/aws_solutionArchitectAssociate/s3_quizz/s3_quizz005.png)     


**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/28.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/29.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/30.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/31.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/32.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/33.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/34.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/35.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/36.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/37.png)         




=======================================================================================================================    

_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **EC2: ELASTIC COMPUTE CLOUD** 
</span>


[Documentation aws EC2](https://aws.amazon.com/ec2/instance-types/)    


=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 OVERVIEW** 
</span>


Une des ressources les plus fondamentales d'AWS.  

**EC2**: **Elastic Compute Cloud**  

Cette ressource permet de faire des calculs dans le cloud. Elle est sécurisée et resizable.   
Cela correspond à une VMs de notre data center mais dans AWS.    

Elle est créée pour un **web-scale cloud computing**    
Le but est d'avoir la capacité qu'on veut lorsqu'on le veut    


On ne paie que pour ce qu'on utilise

C'est très rapide à créer et provisionner (quelques minutes).     


![ec2_pricingOptions](./images/aws_solutionArchitectAssociate/ec2_pricingOptions.png)    




....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 On-demand instances** 
</span>

[Documentation aws: on demand instances](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/ec2-on-demand-instances.html)    

- **flexible**: couts faibles et flexibles sans mettre en avant un contrat longue durée (sans engagement)    
- **short-term**: utilisé pour des applications courtes-durées, "pointues" (d'un grand niveau de complexité), qui ne peut pas anticiper la charge et ne peut pas interrompues    
- **testing-the-water**: les applications ont été développées et testées sur aws EC2 la 1ere fois    


Avec ce type d'instances on paie la capacité de calcul à la seconde SANS engagement.    
**On maitrise totalement son cycle de vie: quand démarrer, utiliser, arrêter, mettre en pause**    


On ne paie que les secondes pour les instances à l'état **running** avec un minimum de 60 secondes    
Le prix à la seconde est fixe    

[Documentation aws: tarification EC2 on-demand](https://aws.amazon.com/fr/ec2/pricing/on-demand/)    

Le nombre d'instances à la demande en cours d'exécution se compte par compte AWS par Région. Les limites d'instance à la demande sont gérées en termes de nombre d'unités centrales virtuelles (vCPU) que vos instances à la demande en cours d'exécution utilisent, quel que soit le type d'instance:    



....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Reserved instances** 
</span>


Les instances réservées vous permettent de réaliser d'importantes économies sur vos coûts Amazon EC2 en comparaison de la tarification des instances à la demande. Les instances réservées ne sont pas des instances physiques, mais correspondent à une remise de facturation appliquée à l'utilisation d'instances à la demande dans votre compte. Ces instances à la demande doivent correspondre à certains attributs, comme le type et la région de l'instance, afin d'entraîner une remise de facturation.    
 


[Documentation aws: EC2 Reserved instances](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/ec2-reserved-instances.html)    


- **predictable usage**: applications avec un usage prévisible sur le long terme    
- **specific capacity requirements**: applications qui ont besoin d'une capacité réservée    
- **pay up front**: planification sur le long-terme de la tarification (abonnements ➡️ contrats) ▶️ réduire les couts    
- **standard Reserved Instances(RIs)**: jusqu'à 72% de réduction sur le prix "on-demand"     
- **convertible RIs**: jusqu'à 54% de reduction sur le prix "on-demand". Contient une option de changement pour un autre type de RI équivalent ou d'une valeur plus importante   
- **scheduled RIs**: démarre dans les plages horaires définies qui correspond à la réservation des machines (seulement pour une partie de journée, semaine, mois, ...)    


Ces instances sont un engagement sur 1 ou 3 ans.



....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Pricing Options** 
</span>


[Documentation aws: EC2 instance pricing](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/instance-purchasing-options.html)    

La réservation d'instances se fait soit à un niveau régional (region aws) soit à un niveau zonal (zone aws)      
[Documentation aws: EC2 instances reservées regionales ou zonales](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/reserved-instances-scope.html)    
Le prix ne varie pas entre ces 2 choix     


Il est possible d'économiser jusqu'à 72% en comparaison avec instance type ou region    

Il est nécessaire de s'engager 1 ou 3 ans pour bénéficier de ces tarifs    

Avec les instances réservées, vous payez pour toute la durée de l'abonnement et non en fonction de l'utilisation réelle    

Lorsque des instances réservées expirent, le tarif à la demande est facturé pour l'utilisation d'instance EC2    

Cela n'est pas spécifique à EC2, c'est aussi le cas pour des technologies serverless comme Lambda ou Fargate    


Il y a 2 types d'instances réservées:  
[Documentation aws: EC2 Instances Réservées Standard ou Convertibles](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/reserved-instances-types.html)        
- **Instances Réservées Standard**: moins cher que convertible mais peu de changements d'instances ne peut se faire      
- **Instances Réservées Convertibles**: il est possible d'échanger les instances    


....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Instances Spot** 
</span>



Quand utiliser les instances **Spot** (Spot Instances) ?     
Qu'est-ce qu'une instance Spot ?    

[Documentation aws: EC2 spot instance](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/using-spot-instances.html)    


Une **instance spot** est une **instance qui utilise la capacité EC2 de ressources non utilisées** ➡️ elles sont beaucoup moins chères mais ne sont pas disponibles lorsqu'on le souhaite (indépendant de nous)    


L'instance s'exécute à chaque fois que celle-ci est disponible    

Les instances Spot sont recommandées pour les applications flexibles sans état, tolérantes aux pannes    

Spot ne garantit pas que vous pouvez conserver vos instances en cours d'exécution suffisamment longtemps pour terminer vos charges de travail. Spot ne garantit pas que vous pouvez obtenir la disponibilité immédiate des instances que vous recherchez, ou que vous pouvez toujours obtenir la capacité globale que vous avez demandée.    


Le prix peut être diminué jusqu'à 90% par rapport à une instance on-demand    

<span style="color: #965DCE">

#####  **EC2 Instances Spot: Concepts** 
</span>

- **Groupe de capacités Spot** – Un ensemble d'instances EC2 inutilisées avec le même type d'instances (par exemple, m5.large) et la même zone de disponibilité.    
- **Prix Spot** – Prix horaire actuel d'une instance Spot: celui-ci varie régulièrement    
- **Demande d'instance Spot** – Lorsque la capacité est disponible, Amazon EC2 répond à votre demande. Une demande d'instance Spot est soit One-time (Unique) soit Persistent (Persistante). Amazon EC2 soumet à nouveau automatiquement une demande d'Instance Spot persistante après que l'Instance Spot associée à la demande soit interrompue.    
- **Recommandation de rééquilibrage d'instance EC2** – Amazon EC2 émet un signal de recommandation de rééquilibrage d'instances pour vous avertir qu'une instance Spot présente un risque élevé d'interruption. Ce signal vous donne la possibilité de rééquilibrer de manière proactive vos charges de travail entre les instances Spot existantes ou nouvelles sans avoir à attendre l'avis d'interruption d'instance Spot deux minutes avant celle-ci.    
- **Spot Instance interruption (Interruption d'instance Spot)** – Amazon EC2 résilie, arrête ou met en veille prolongée votre instance Spot lorsque Amazon EC2 a besoin de récupérer la capacité. Amazon EC2 communique un avis d'interruption d'instance Spot, qui donne à l'instance un avertissement deux minutes avant qu'elle soit interrompue.




....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Dedicated hosts** 
</span>


[Documentation aws: EC2 Dedicated hosts](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/dedicated-hosts-overview.html)    


Un hôte dédié Amazon EC2 est un serveur physique avec une capacité d'instance EC2 entièrement dédiée à votre utilisation.       


⚠️ L'utilisation d'hôtes dédiées prend en compte des restrictions: voir documentation aws chapitre "Restrictions Hôtes dédiés"➡️ https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/dedicated-hosts-overview.html 


Il existe 4 types d'usage d'hôtes dédiés:
- **compliance**: exigences réglementaires qui peuvent ne pas prendre en charge la virtualisation mutualisée (partage de hardware entre différents utilisateurs aws: une banque ne peut pas partager les cpus ou autre hardware avec un utilisateur "classique" ➡️ risque de piratage )        
- **on-demand**: on peut utiliser un hote dédié à la demande et payer à l'heure     
- **licencing**: super licence qui ne prend pas en charge les déploiements multi-locataires ou cloud (un hôte avec une licence microsoft ne peut pas fonctionner avec des applications linux par exemple)    
- **reserved**: on peut réserver un hôte dédié pour une utilisation ponctuelle (- 70% par rapport à on-demand)    





<span style="color: #965DCE">

#####  **EC2 Dedicated Hosts: Bring Your Own License (BYOL, licence à fournir)** 
</span>


Les Hôtes dédiés vous permettent d'utiliser vos licences logicielles existantes par socket, par cœur ou par machine virtuelle. Lorsque vous utilisez vos propres licences, vous êtes responsable de leur gestion. Toutefois, Amazon EC2 comporte des fonctionnalités qui vous aident à assurer la conformité de vos licences, telle que l'affinité d'instances et le placement ciblé.     



Voici les grandes étapes que vous devez suivre afin d'utiliser votre propre image de machine virtuelle sous licence en volume dans Amazon EC2:

- Assurez-vous que les termes du contrat de licence régissant l'utilisation de vos images de machine permettent l'utilisation dans un environnement cloud virtualisé.    
- Après avoir vérifié que votre image de machine peut être utilisée dans Amazon EC2, importez-la avec VM Import/Export. Pour plus d'informations sur la procédure à suivre pour importer votre image de machine, consultez le Guide de l'utilisateur VM Import/Export.    
- Une fois votre image de machine importée, vous pouvez lancer des instances depuis cette image sur des Hôtes dédiés actifs de votre compte.    
- Lorsque vous exécutez ces instances, en fonction du système d'exploitation, vous pouvez être contraint d'activer ces instances sur votre propre serveur KMS.    

....................................................................................................................... 

![EC2 resume pour certification](./images/aws_solutionArchitectAssociate/ec2_examTips.png)    

....................................................................................................................... 

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 AWS CONSOLE** 
</span>

Une fois dans le module EC2, cliquer sur **lauch instance**    

1. **Choisir une AMI** (image Amazon avec un OS)  ➡️ une liste apparait, choisir celle souhaitée    

2. **Choisir le type d'instance**: par defaut une instance de type **t2** est une instance "classique" de travail. Le type **t2.micro** pour faire un test est préconisé puisque gratuite    

3. **Configurer l'instance**: combien d'instances de ce type, dans quel VPC, sous-reseau... Cela correspond à une VM       

> *Remarque*: Une instance doit être dans un VPC qui correspond à un "mini" data center pour nos instances    


4. **Ajout d'un storage** (espace disque): il s'agit d'un disque virtuel. par defaut 1 est proposé


5. **Ajout de tags**: permet d'avoir des annotations ➡️ utiles pour une recherche rapide (?? utilisé avec les APIs ???)

6. **Configurer les security groups**: il s'agit d'un virtual firewall. On lui donne un nom "identifiable" ainsi que les ports autorisés (par defaut le port 22 pour les accès ssh et 80/443 dans le cas d'un service web)    
On peut ajouter aussi des plages d'IP pour augmenter la sécurité (sinon par defaut toutes les plages d'IP sont en 0.0.0.0/0 donc ouvert à tout)    

7. **review**: un résumé de la configuration mise en place avant de créer l'instance    


En cliquant sur **launch**  il nous est proposé la création de paires de clés ssh. Ces clés seront utilisées pour l'administration de l'instance (en ssh pour linux, permettent la demande d'un password pour les windows)     
On download ces clés et on les conserve en sécurité    




Une fois l'instance créée et à l'état running, on peut s'y connecter en cliquant sur **connect**.    
Une nouvelle fenêtre s'ouvre et présente l'ID de l'instance, le user (userInstance: ec2-user), son IP public(ipAdressPublic: 52.91.168.83) et permet la connexion    
Dans un autre onglet (SSH Client) se trouve les information pour la connexion en ssh   


```bash
#1. Ouvrir un ssh client

#2. Récupérer les clés ssh et utiliser pour la connexion le .pem généré (ex: toto.pem)

#3. Modifier les droits du certificats .pem
chmod 400 toto.pem 

#4. Se connecter à l'instance via le DNS public : ec2-52-91-168-83.compute-1.amazonaws.com
ssh -i "toto.pem" ec2-user@ec2-52-91-168-83.compute-1.amazonaws.com
```

Une fois connecté à l'instance nous nous trouvons dans un terminal linux classique, nous pouvons effectuer les opérations habituelles (en commençant par faire un update)    


Pour arrêter l'instance, au niveau du dashboard ec2 dans la console, aller sur **Instance State** et sélectionnant **terminate instance**.      

Au niveau facturation nous payons à la seconde utilisée puisque nous sommes dans le cas "on-demand"    




=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 AWS COMMANDE LINE** 
</span>


....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 AWS CLI** 
</span>


[Documentation aws: aws CLI](https://docs.aws.amazon.com/cli/index.html)    


[Documentation aws: install aws cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)    

Il est possible d'installer aws cli sur windows, macOs, Linux ou depuis une instance ec2.   
Dans cette partie sera montré depuis une instance ec2    

Pour utiliser la cli aws il faut suivre la syntaxe suivante: 

```bash
# syntaxe
aws <service> <command> <subcommand> <parameter>

# exemple 
aws s3 ls
```


Dans 1 premier il faut configurer aws cli.    
Pour cela il faut:    
- depuis une instance sur aws 
  - se connecter sur l'instance
  - lancer la configuration de aws: `aws configure`   
  - il est demandé de fournir un access key ID, pour cela il faut aller dans la `console aws/ IAM / créer un user /` l'ajouter à un groupe auquel on à les policies souhaitées (pour avoir les droits nécessaires pour les actions à faire)    
  ➡️ là nous avons les access key ID qui sont affichés: les renseigner dans le terminal où on a noté `aws configure`    
  - on peut maintenant exécuter les commandes avec aws cli (en fonction des droits du groupe du user)    


[Documentation aws: commands references aws cli](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html)    




![aws cli exam tips 001](./images/aws_solutionArchitectAssociate/awsCli_examTips001.png)    
![aws cli exam tips 002](./images/aws_solutionArchitectAssociate/awsCli_examTips002.png)    






....................................................................................................................... 

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 USING ROLES** 
</span>



[Documentation aws: IAM Roles](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/id_roles.html)    


....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 What is an IAM role ?** 
</span>


Un **rôle IAM** est une **identité IAM** que vous pouvez **créer dans votre compte** et qui **dispose d'autorisations spécifiques** (qui sont données par les policies auxquelles il est rattaché). Un rôle IAM est similaire à un utilisateur IAM.       

Un rôle n'est pas associé à un user unique, il est conçu pour être endossé par tout utilisateur qui en a besoin    



Utiliser des rôles pour déléguer l'accès à des utilisateurs, des applications ou des services qui n'ont normalement pas accès à vos ressources AWS.    



Les rôles peuvent être utilisés par :    
- Un utilisateur IAM qui appartient au même Compte AWS que le rôle
- Un utilisateur IAM dans un Compte AWS différent de celui du rôle    
- Un service web proposé par AWS, par exemple Amazon Elastic Compute Cloud (Amazon EC2)    
- Un utilisateur externe authentifié par un service de fournisseur d'identité (IdP) externe, compatible avec SAML 2.0 ou OpenID Connect, ou un broker d'identité personnalisé.    


[Documentation aws: Creation et delegation d autorisations à un service](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/id_roles_create_for-service.html)    











....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Roles are temporary** 
</span>



Un rôle ne dispose pas d'informations d'identification standard à long terme (mot de passe ou des clés d'accès associées)    
Il vous fournit des informations d'identification de sécurité temporaires pour votre session de rôle.    











....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 What else can role do ?** 
</span>



Les roles peuvent être assumés par les personnes, architecture aws ou autre comptes techniques   


Ils peuvent autoriser l'accès entre comptes. Cela permet à un compte aws d'accéder et interagir avec des ressources d'un autre compte aws.    





....................................................................................................................... 

![ec2_iamRoles_examTips.png](./images/aws_solutionArchitectAssociate/ec2_iamRoles_examTips.png)    

....................................................................................................................... 


=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 SECURITY GROUPS AND BOOTSTRAP SCRIPTS** 
</span>



[Documentation aws: security groups](https://docs.aws.amazon.com/fr_fr/vpc/latest/userguide/VPC_SecurityGroups.html)    

Un **security group** contrôle le trafic autorisé à atteindre et à quitter les ressources auxquelles il est associé    

Par exemple, après avoir associé un groupe de sécurité à une instance EC2, il contrôle le trafic entrant et sortant pour l'instance    

Lorsque vous créez un **VPC**, celui-ci est **fourni avec un security group par défaut**. Vous pouvez créer des groupes de sécurité supplémentaires pour chaque VPC. Vous pouvez associer un security group uniquement aux ressources du VPC pour lequel il est créé.    

Pour chaque security group, vous **ajoutez des règles qui contrôlent le trafic en fonction des protocoles et des numéros de port**.


L'utilisation de security group n'ajoute aucun frais supplémentaire.    





[Documentation aws: bootstrap scripts](https://docs.aws.amazon.com/fr_fr/emr/latest/ManagementGuide/emr-plan-bootstrap.html)    




![AWS-EC2-Network-Security-Group](./images/aws_solutionArchitectAssociate/AWS-EC2-Network-Security-Group.png)          
 



....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Security groups** 
</span>


Ce sont des firewall virtuels sur les instances EC2 (équivalent iptables)     
Par defaut tout est fermé  ➡️   pour pouvoir communiquer il faut ouvrir les ports souhaités    

Pour tout ouvrir: `0.0.0.0/0`   ▶️ tout est ouvert (sur le port souhaité: il s'agit d'une plage d'IPs qui est renseigné et à qui nous donner les accès sur le port définit)    

Les ports "minima" à ouvrir sont les suivants:
- 22 (ssh pour linux)         ➡️   permet d'administrer l'instance     
- 3389 (rdp pour windows)     ➡️   permet d'administrer l'instance     
- 80 (http)                   ➡️   permet les mises à jour (surtout nécessaire dans le cas d'instance web)     
- 443 (https)                 ➡️   permet les mises à jour (surtout nécessaire dans le cas d'instance web)     





....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Bootstrap scripts** 
</span>


Un **bootstrap script** est un script qui doit être joué à la suite de la création de l'instance     


<div class="resume" style='padding:0.1em; background-color:#B6F0C1; color:#07319A'>
<span>
_Exemple de bootstrap script_:     

```bash
#!/bin/bash

# install apache service
yum install httpd -y

# start the apache service
yum service httpd start
```
Cette instance sert pour un serveur web qui fonctionne sous apache. A la création de l'instance il est donc nécessaire d'installé et de démarrer le service apache     
</span></div>


Cela permet de "finaliser" la création de l'instance de manière automatisée sans à avoir à s'y connecter: gain de temps     


Pour pouvoir utiliser ces scripts il faut lors de la creation (depuis la console aws) aller dans `configure instance/advanced details/ user data` et coller le script "as text" s'il est court ou "as file" s'il est long    




....................................................................................................................... 

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 METADATA AND USER DATA** 
</span>


[Documentation aws: metadata and user data](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/ec2-instance-metadata.html)    


Les **metadata d'instance** sont des **données portant sur votre instance** que vous pouvez utiliser pour configurer ou gérer l'instance en cours d'exécution.     

[Documentation aws: categories de metadata](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/instancedata-data-categories.html)    


Les metadatas sont les données des données de l'instance: IP privée, IP public, Hostname, Security group....


A partir de ces metadata nous pouvons accéder aux **user data** spécifiées au moment de la creation de l'instance    

Un **bootstrap script est** par exemple un **user data**    






....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Retrieving metadata** 
</span>


Pour récupérer ces metadata un curl suffit:

```bash
# se connecter à l'instance ec2 puis exécuter la commande curl suivante
curl http://169.254.169.254/latest/meta-data/

## cela retourne la liste de toutes les metadata de l'instance
## pour connaitre la valeur de la metadata souhaitée "compléter" le curl avec la metadata souhaitée

## pour ne recupérer dans un fichier que l'ip adress
curl http://169.254.169.254/latest/meta-data/local-ipv4 > ipAdress.txt
```


De la même manière nous pouvons récupérer les **user data**:    

```bash
curl http:169.254.169.254/latest/user-data
```

➡️ Si nous avons ajouter lors de la création des "user data" comme un script bootstrap, cela apparaitra



....................................................................................................................... 

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 NETWORKING WITH EC2** 
</span>


[Documentation aws: networking with EC2](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/ec2-networking.html)    


**Amazon VPC** vous permet de lancer des ressources AWS, telles que des instances Amazon EC2, dans un réseau virtuel dédié à votre compte AWS, connu sous le nom de **Virtual Private Cloud (VPC)**.     
Lorsque vous **lancez une instance**, vous pouvez **sélectionner un sous-réseau à partir du VPC**.     
L'instance est configurée avec une interface réseau principale, qui est une carte réseau virtuelle logique.     
L'instance reçoit une adresse IP privée principale de l'adresse IPv4 du sous-réseau et elle est affectée à l'interface réseau principale.    

Vous pouvez contrôler si l'instance reçoit une adresse IP publique du pool d'adresses IP publiques d'Amazon.    
L'adresse IP publique d'une instance est associée à votre instance uniquement jusqu'à ce qu'elle soit arrêtée ou résiliée     

Si vous avez besoin d'une adresse IP publique persistante, vous pouvez allouer une adresse IP Elastic à votre compte AWS et l'associer à une instance ou à une interface réseau    
Une adresse IP Elastic reste associée à votre compte AWS jusqu'à ce que vous la libériez    

Pour augmenter les performances réseau et réduire la latence, vous pouvez lancer des instances dans un **placement group**   
L'explication d'un **placement group** se fera ultérieurement dans le chapitre EC2     

![AWS-EC2-Placement-Group](./images/aws_solutionArchitectAssociate/AWS-EC2-Placement-Group.png)         

....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Different Virtual Networking Options** 
</span>


Il y a 3 différents types de virtual networking cards pour les instances EC2:
- **ENI**: Elastic Network Interface    ➡️  pour le basic: networking day-to-day
- **EN**: Enhanced Networking           ➡️  utilise un root I/O virtualization pour améliorer la haute performance
- **EFA**: Elastic Fabric Adapter       ➡️  accelère le HPC (High Performance Computing) et appli machines learning











....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 ENI: Elastic Network Interfaces** 
</span>


C'est un virtual network card simple qui permet: 
- ipv4 privéés
- ipv4 publiques
- nombreuses ipv6
- MAC adress
- 1 ou + security groups (firewall virtuels)








....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Enhanced Networking** 
</span>


C'est utilisé pour la haute performance reseau entre 10 et 100 Gb par seconde    
Le single root I/O virtualization (SR-IOV) permet la haute performance et une diminution de l'utilisation CPU    

Large bande passante, plus de packets par seconde dans le trafic, diminution de latences inter-instance   










....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 ENA (Elastic Network Adapter) vs VF (Virtual Function)** 
</span>



ENA   ➡️  utilisé sur des instances "récentes" et pour un débit > 100 Gbps      
VF    ➡️  utilisé sur des instances "anciennes" pour un > 10 Gbps    








....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 EFA (Elastic Fabric Adapter)** 
</span>


Permet la très haute performance (HPC).        
Utilisé avec le machine learning       
Diminue consodérablement les latences TCP     


Seulement supporté par linux    




....................................................................................................................... 

![ec2-networking](./images/aws_solutionArchitectAssociate/ec2-networking.png)     

....................................................................................................................... 



=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 OPTIMIZING WITH EC2 PLACEMENT GROUPS** 
</span>


[Documentation aws: Placement groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/placement-groups.html)    


Lorsque vous lancez une nouvelle instance EC2, le service EC2 tente de placer l'instance de façon à ce que toutes vos instances soient réparties sur la matériel sous-jacent pour minimiser les échecs corrélés    

Vous pouvez utiliser des **placement groups** pour influencer le placement d'un groupe d'instances interdépendantes afin de répondre aux besoins de votre charge de travail.


Il existe 3 types de placement groups.

Selon le type de charge de travail, vous pouvez créer un **placement group** à l'aide de l'une des stratégies de placement suivantes:
- **Cluster**: regroupe des instances rapprochées à l'intérieur d'une Zone de disponibilité. Cette stratégie permet aux charges de travail d'atteindre les performances réseau à faible latence nécessaires à une communication de nœud à nœud étroitement couplée, typique des applications ce calcul haute performance (HPC).      
- **Partition**: répartit les instances entre les partitions logiques de façon à ce que des groupes d'instances d'une partition ne partagent pas le matériel sous-jacent avec des groupes d'instances d'autres partitions. Cette stratégie est généralement utilisée par les grandes charges de travail distribuées et répliquées telles que Hadoop, Cassandra, et Kafka    
- **Spread (répartition)**: place strictement un petit groupe d'instances sur un matériel sous-jacent distinct pour réduire les défaillances corrélées    

Il n'y a aucun frais pour la création d'un placement group


Certaines regles concernant les placement groups sont à prendre en compte:

- **Regles générales sur les placement groups**:
  - Vous pouvez **créer un maximum de 500 placement groups par compte dans chaque région**.     
  - Le **nom** que vous spécifiez pour un placement group doit être **unique au sein de votre compte AWS pour la région**.    
  - Vous **ne** pouvez **pas fusionner** des placement groups.    
  - **Une instance peut être lancée dans un seul placement group à la fois** ; elle ne peut pas s'étendre sur plusieurs placement groups.    
  - [Réservation de capacité à la demande](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/ec2-capacity-reservations.html#capacity-reservations-limits) et les [Instances réservées zonales](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/reserved-instances-scope.html) fournissent une réservation de capacité pour les instances EC2 dans une zone de disponibilité spécifique. La réservation de capacité peut être utilisée par les instances d'un placement group. Lorsque vous utilisez un placement group du [cluster avec réserve](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/cr-cpg.html) de capacité, il est recommandé de réserver de la capacité au sein du placement group du cluster.        
  - Les instances réservées zonales fournissent une réserve de capacité pour les instances dans une zone de disponibilité spécifique. La réservation de capacité peut être utilisée par les instances d'un placement group. Toutefois, il n'est pas possible de réserver explicitement de la capacité dans un placement group à l'aide d'une Instance réservée zonale.    
  - **Vous ne pouvez pas lancer des Hôtes dédiés dans les placement groups**. 

Il existe d'autres regles compémentaires sur les placement groups en fonction de leur statégie.    




![ec2_placementGroup cluster](./images/aws_solutionArchitectAssociate/ec2_placementGroup001-clusterPartition.png)    
![ec2_placementGroup spread](./images/aws_solutionArchitectAssociate/ec2_placementGroup002-spread.png)    
![ec2_placementGroup partition](./images/aws_solutionArchitectAssociate/ec2_placementGroup003-Partition.png)    

.......................................................................................................................  

![ec2_placementGroup exam tips](./images/aws_solutionArchitectAssociate/ec2_placementGroup004-examTips.png)    
![ec2_placementGroup exam tips](./images/aws_solutionArchitectAssociate/ec2_placementGroup005-examTips.png)    


....................................................................................................................... 

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 SOLVING LICENCING ISSUES WITH DEDICATED HOSTS** 
</span>



![EC2 Modèles de Prix](./images/aws_solutionArchitectAssociate/ec2_priceModels.png)    


....................................................................................................................... 

![EC2 Deficated hosts exam tips](./images/aws_solutionArchitectAssociate/ec2_dedicatedHost_examTips.png)    

....................................................................................................................... 

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 TIMING WORKLOADS WITH SPOT INSTANCES AND SPOT FLEETS** 
</span>


....................................................................................................................... 

<span style="color: #11E3EA">

####  **EC2 Rappels sur les instances spot** 
</span>

Une instance Spot amazon ec2 permet d'utiliser les capacités "non-utilisées" EC2 de aws.     
Elles sont disponibles jusqu'à 90% moins chère que les instance ec2 on-demand (mais le prix varie en fonction de l'offre et la demande du moment)    

Quand devrions-nous utiliser ces instances Spot ? 
  Pour des applications stateless, tolérantes à la panne ou flexibles    
    ➡️ un webservice ne peut-être utilisé avec ce type d'instances puisqu'l doit être disponible h24   
    ➡️ faire des tests ("non urgent") convient parfaitement à ces instances    

La 1ere chose à décider pour une utilisation d'instances spot est le prix maximum souhaité: les instances seront fournies tant que le prix est SOUS la limite définie. Le prix de cette instance est définit à l'heure        
Si le prix passe au-dessus de la limite décidée: nous avons 2 minutes pour décider si on stoppe ou si on continu l'utilisation de cette instance    


Il est possible de mettre en place un **spot block** pour arrêter mais ne pas supprimer nos instances spot en cas de prix suppérieur à notre limite. (si un workload doit absolument se terminer avant d'arrêter l'instance)   
Nous pouvons définir un spot block entre 1 à 6 heures   

Le prix dépend aussi de la region aws dans laquelle vous l'utilisez    


Les plus grandes utilisations de ce genre d'instances sont: 
- **big data and analytics**    
- **containerized workloads**    
- **ci/cd and testing**    
- **image and media endering**    
- **high performance computing**    


Les cas pour lesquels il NE FAUT PAS utiliser cette instance: 
- **persistent workload** (comme un site web)
- **critical jobs** (on ne peut pas stopper un job critique avant sa fin)
- **databases** 


[Documentation aws: spot request](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/spot-requests.html)    

![ec2_spotRequest](./images/aws_solutionArchitectAssociate/ec2_spotRequest.png)    








.......................................................................................................................

<span style="color: #11E3EA">

####  **EC2 Spot fleets ?** 
</span>


[Documentation aws: spot fleet](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/spot-fleet.html)    


Une **spot fleet** est une collection d'instances spots et d'instances on-demand.    
La **spot fleet** tente de se lancer quand le nombre de spot instances et on-demand instances a atteint la capacité cible dans la requête spot fleet    
La requête est atteinte lorsque les conditions de la capacité d'instances ET le prix maximum spécifié sont remplis.    
La **spot fleet** aura pour but de ré atteindre sa capcité cible si des instances spots sont intérrompues    



Pour définir correctement ces conditions nous utilisons des stratégies.    
Le but de spot fleet est d'atteindre les contraintes de la stratégie à laquelle il est affilié    


Quelles sont les différentes stratégies?    
[Documentation aws: strategies spot fleet](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/how-spot-fleet-works.html)    

- **cacacityOptimized**: les instances spot viennent d'un pool avec la capacité optimale pour le nombre d'instances spot démarrées     
- **diversified**: les instances spot sont disponibles à travers plusieurs pools    
- **lowestprice**: les instances spot se trouvent dans le pool le moins cher (stratégie par defaut)    
- **instancePoolsToUseCount**: les instances spot sont disponibles à travers un nombre défini de pool d'instances spot. Le parametre est valide seulement si est associé à `lowestPrice`    




.......................................................................................................................


![ec2-spotExamTips](./images/aws_solutionArchitectAssociate/ec2-spotExamTips.png)

.......................................................................................................................

=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 DEPLOYING VCENTER IN AWS WITH VMWARE CLOUD ON AWS** 
</span>



Pourquoi utiliser VMWare sur AWS ?    

VMWare est (était) utilisé par de nombreuses sociétés avant l'arrivée du cloud.     
Ces sociétés ont encore pour beaucoup leurs infrastructures privées et donc sont passées à du cloud hybride    



Dans quel cas utiliser VMWare ?

**Hybrid cloud**: on se connecte sur notre on-premise cloud vers le cloud aws pour gérer les workloads hybrides    
  ➡️ dans ce cas nous ne savons pas exactement combien d'instances EC2 vont être nécessaire pour le fonctionnement du cloud hybride    

**cloud migration**: la migration de on-premise vers le cloud aws se fait via les VMWare built-in tools    

**disaster recovery**: VMWare est célèbre dans le disaster recovery ➡️ cela permet de faire un DR "gratuit" à partir de l'environnement aws    

**leverage aws**: aws a plus de 200 services pour update des applications ou la creation d'une nouvelle   




Comment VMWare est-il déployé sur aws ?   

Il fonctionne sur un hote dédié et utilise un SEUL aws account    
Chaque hôte a 2 sockets avec 18 cores par socket, 512 Gb RMA, 15.2 Tb ssd storage    
Chaque hôte est capable de faire fonctionner une multitude d'instances VMWare (+ de 100)    
Les clusters peuvent démarrer avec 2 à 16 hotes hébergés par cluster    
  ➡️ cela sera le VCenter sur aws




▶️ Il est possible de deployer un VCenter on sur cloud aws en utilisant VMWare    
      ➡️ solution parfaite pour un cloud privé vmware dans le cloud publique aws    





=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 EXTENDING AWS BEYOND THE CLOUD WITH AWS OUTPOSTS** 
</span>



Il est possible d'étendre le cloud aws avec **aws outpost**     

[Documentation aws: aws outpost](https://docs.aws.amazon.com/fr_fr/outposts/latest/userguide/what-is-outposts.html)    


**AWS Outposts est un service entièrement géré qui étend l'infrastructure AWS, les services, les API et les outils aux sites du client**    

En fournissant un accès local à l'infrastructure gérée par AWS, AWS Outposts permet aux clients de créer et d'exécuter des applications sur site à l'aide des mêmes interfaces de programmation que dans les régions AWS, tout en utilisant les ressources de calcul et de stockage locales pour réduire la latence et les besoins de traitement des données locales    

➡️ on amène le cloud aws directement on-premise dans notre data center    


Un **outpost** est un **pool de capacités de calcul et de stockage AWS déployées sur un site client**.    

AWS exploite, surveille et gère cette capacité dans le cadre d'une région AWS.    
Vous pouvez créer des sous-réseaux sur votre outpost et les spécifier lorsque vous créez des ressources AWS telles que des instances EC2, des volumes EBS, des clusters ECS et des instances RDS. Les instances se trouvant dans des sous-réseaux outpost communiquent avec d'autres instances de la région AWS à l'aide d'adresses IP privées se trouvant toutes dans le même VPC    

[Page aws: produits de aws outpost](https://aws.amazon.com/outposts/)    


.......................................................................................................................

<span style="color: #11E3EA">

####  **EC2 aws outposts: concepts clé** 
</span>



- **Site d'Outpost**: Les bâtiments physiques gérés par le client où AWS installera votre avant-poste (outpost). Un site doit répondre aux exigences d'installation, de réseau et d'alimentation de votre outpost.    
- **Configurations d'Outpost**: Configurations de la capacité de calcul Amazon EC2, de la capacité de stockage Amazon EBS et de la prise en charge réseau. Chaque configuration a des exigences uniques en matière d'alimentation, de refroidissement et de soutien du poids.    
- **Capacité d'Outpost**: Ressources de calcul et de stockage disponibles sur l'outpost. Vous pouvez afficher et gérer la capacité de votre outpost à partir d'`AWS Outpostsconsole`    
- **Équipement d'Outpost**: Matériel physique qui permet d'accéder au `AWS Outpostsservice`. Le matériel comprend des racks, des serveurs, des commutateurs et des câbles détenus et gérés par AWS.    
- **Supports d'Outpost**: Facteur de forme Outpost qui est un rack 42U standard. Les racks d'outpost comprennent des serveurs montables en rack, des commutateurs, un panneau de brassage réseau, une étagère électrique et des panneaux vierges.    
- **Serveurs Outpost**: Facteur de forme Outpost qui est un serveur 1U ou 2U standard, qui peut être installé dans un rack 4 montants conforme à la norme EIA-310D 19. Les serveurs d'outpost fournissent des services locaux de calcul et de mise en réseau aux sites dont l'espace est limité ou dont la capacité est réduite.     
- **Lien service**: Route réseau permettant la communication entre votre outpost et sa Region AWS associée. Chaque outpost est une extension d'une zone de disponibilité et de la région associée.     
- **Passerelle locale**: Un routeur virtuel d'interconnexion logique qui permet la communication entre un rack Outpost et votre réseau local.     
- **interface réseau locale**: Interface réseau qui permet la communication à partir d'un serveur Outpost et de votre réseau local.   




Permet d'avoir un cloud hybrid, est complètement managé par aws dans votre infrastructure, permet d'utiliser tous les service aws    


Il existe 2 sortent d'outposts disponibles: 
- **outposts rack**: 
  - *harware*: disponible au démarrage avec un rack 42U seul (possibilité d'upgrade à 96U) 
  - *services*: permet le compute aws, storage, database.... services en local
  - *results*: donne la même infra qu'aws, mêmes services et APIs dans VOTRE data center
- **outposts servers**: 
  - *harware*: serveur individuel 1U ou 2U   
  - *use cases*: petite place nécessaire (magasin de vente, bureaux secondaires...)
  - *results*: permet un compute local et les services network    


Comment mettre en place un outpost aws ?   

1. Commander le outpost depuis la console aws
2. Une équipe aws vient faire l'installation "complete" (install, hardware, power, network, conectivity) sur site    
3. Se connecter à la console aws et lancer le outpost 
4. Construire notre propre environnement aws    






.......................................................................................................................

![ec2_outposts_examTips](./images/aws_solutionArchitectAssociate/ec2_outposts_examTips.png)    


.......................................................................................................................


=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 RAPPELS EXAM TIPS** 
</span>


![ec2_examTips001](./images/aws_solutionArchitectAssociate/ec2_examTips001.png)     
![ec2_examTips002](./images/aws_solutionArchitectAssociate/ec2_examTips002.png)     
![ec2_examTips003](./images/aws_solutionArchitectAssociate/ec2_examTips003.png)     
![ec2_examTips004](./images/aws_solutionArchitectAssociate/ec2_examTips004.png)     
![ec2_examTips005](./images/aws_solutionArchitectAssociate/ec2_examTips005.png)     
![ec2_examTips006](./images/aws_solutionArchitectAssociate/ec2_examTips006.png)     
![ec2_examTips007](./images/aws_solutionArchitectAssociate/ec2_examTips007.png)     
![ec2_examTips008](./images/aws_solutionArchitectAssociate/ec2_examTips008.png)     
![ec2_examTips009](./images/aws_solutionArchitectAssociate/ec2_examTips009.png)     
![ec2_examTips010](./images/aws_solutionArchitectAssociate/ec2_examTips010.png)     
![ec2_examTips011](./images/aws_solutionArchitectAssociate/ec2_examTips011.png)     
![ec2_examTips012](./images/aws_solutionArchitectAssociate/ec2_examTips012.png)     
![ec2_examTips013](./images/aws_solutionArchitectAssociate/ec2_examTips013.png)     
![ec2_examTips014](./images/aws_solutionArchitectAssociate/ec2_examTips014.png)     
![ec2_examTips015](./images/aws_solutionArchitectAssociate/ec2_examTips015.png)     




=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 LAB1: EC2 Instance bootstrapping** 
</span>


![ec2 lab1 enoncé](./images/aws_solutionArchitectAssociate/labs/ec2_lab1/ec2_lab1_enonce.png)    



lien lab1 ec2: https://816882681666.signin.aws.amazon.com/console?region=us-east-1    
username: cloud_user    
password: #4Teklurhzcdwci     

cloud server webserver01       
lien terminal: https://ssh.instantterminal.acloud.guru/?_ga=2.74535638.535175582.1680677002-1577482731.1680677002    
username: cloud_user     
password: DBJis!8(      
public IP: 54.198.202.36     

![ec2 lab1 projet](./images/aws_solutionArchitectAssociate/labs/ec2_lab1/ec2_lab1_projet.png)    


Lors de la création de l'instance, comme nous voulons que ce soit un serveur web (accessible depuis internet), dans la partie **Network Settings** on clique sur `Edit` et on met à **enable** la partie **Auto-assign public IP**    

> *Remarque*: dans ce lab des security group ont été créés ➡️ sélectionné celui qui autorise les accès internet  
![ec2_lab1_securityGroup](./images/aws_solutionArchitectAssociate/labs/ec2_lab1/ec2_lab1_securityGroup.png)    





[fichier commandes ec2 instance lab1](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab1/commandes.txt)    

Pour la partie de création des instances webserver-02 et webserver-03 on utilise le `bootstrap.sh` qui est [ici](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab1/bootstrap.sh)    

Pour voir ce qu'il s'est passé sur l'installation du webserver-02 avec `bootstrap.sh` [voir les logs](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab1/log_installEc2WithBootsrap.log)    


> *Remarque*: Si nous avions eu une erreur avec le bootsrap.sh lors de l'exécution de la commande `curl http://169.254.169.254/latest/user-data` une erreur serait remontée    


![resultat page à partir IP public du webserver-03](./images/aws_solutionArchitectAssociate/labs/ec2_lab1/ec2_lab1_result-webserver-03.png)    


=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 LAB2: Using EC2 Roles and Instance Profiles in AWS** 
</span>

![ec2_lab2_enonce](./images/aws_solutionArchitectAssociate/labs/ec2_lab2/ec2_lab2_enonce.png)    


lien lab: https://541160063606.signin.aws.amazon.com/console?region=us-east-1     
user_name: cloud_user        
password: $0Vxgkqhyyueqsq       

cloudServer Bastion Server :      
username: cloud_user          
password: [y^w8!pF              

public adress of bastion Server: 54.166.254.25           
public adress of web Server: 34.207.230.152              
lien instant terminal: https://ssh.instantterminal.acloud.guru/?_ga=2.207039893.760027901.1680787596-137901257.1680787596      


![ec2_lab2_schema](./images/aws_solutionArchitectAssociate/labs/ec2_lab2/ec2_lab2_schema.png)     


1. se connecter à AWS    


2. Aller dans le service s3 bucket / le bucket "...lookupfiles...." /download le fichier `labreferences.txt`      
[labreferences.txt](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab2/labreferences.txt)     

Ce fichier contient le mot des buckets S3 avec le secret pour s'y connecter    
  ➡️ sera utilisé dans le lab    


3. on va faire une connexion ssh à une instance     
➡️ aller dans le `service EC2 / chercher dans les instances l'instance Bastion / récupérer son ip public` : 54.166.254.25    
    on lance la connexion ssh sur cette ip:    
```bash
ssh cloud_user@54.166.254.25
## le pass est renseigné dans les infos du lab
```

4. configure aws cli: `aws configure`   ➡️ pas besoin pour le lab de renseigner access key ni le secret access key, seulement la region et le format des ouputs 


5. creation d'une trusted policy     
➡️ il s'agit d'une policy qui va autoriser le service ec2 à assumer le role de creation    
 on crée le fichier `trust_policy_ec2.json`    
[trust_policy_ec2.json](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab2/trust_policy_ec2.json)    


6. creation d'un role     
➡️ exécuter la commande `aws iam create-role --role-name DEV_ROLE --assume-role-policy-document file://trust_policy_ec2.json`



7. Autoriser ce role en read-access sur un autre S3 bucket    
 nous créons une nouvelle policy IAM pour definir ces permissions ➡️ `dev_s3_read_access.json`  
 [dev_s3_read_access.json](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab2/dev_s3_read_access.json)    

On récupère dans le fichier `labreferences.txt` la valeur de "<DEV_S3_BUCKET_NAME>" à remplacer dans le fichier    

8. creation d'une policy IAM attchée à cette policy    
`aws iam create-policy --policy-name DevS3ReadAccess --policy-document file://dev_s3_read_access.json`    

Dans le resultat copier la ligne "Arn" puisque nous allons en avoir besoin plus tard (`arn:aws:iam:xxxxxxx:policy/DevS3ReadAccess`)    



9. Nous attachons la policy au role: 
`aws iam attach-role-policy --role-name DEV_ROLE --policy-arn "<DevS3ReadAccess_POLICY_ARN>"` avec comme valeur de "<DevS3ReadAccess_POLICY_ARN>" la valeur copiée précédement 


10. Nous pouvons lister la policy attachée au role    
`aws iam list-attached-role-policies --role-name DEV_ROLE`    


11. creation de l'instance profile     
`aws iam create-instance-profile --instance-profile-name DEV_PROFILE`     


12. Ajout d'un role sur cette instance profile     
`aws iam add-role-to-instance-profile --instance-profile-name DEV_PROFILE --role-name DEV_ROLE`    


13. Vérification que tout est ok     
`aws iam get-instance-profile --instance-profile-name DEV_PROFILE`    


14. Association de IAM instance profile avec l'EC2 instance ID    
`aws ec2 associate-iam-instance-profile --instance-id <LAB_WEB_SERVER_INSTANCE_ID> --iam-instance-profile Name="DEV_PROFILE"`    
avec "<LAB_WEB_SERVER_INSTANCE_ID>" l'ID de l'instance web server (récupéré sur la console aws ec2/instance)    


15. verification     
`aws ec2 describe-instances --instance-ids <LAB_WEB_SERVER_INSTANCE_ID>`    
L'instance profile IAM est bien associée à l'instance EC2     

Pour valider on va faire une connexion ssh sur le serveur web


16. connexion en ssh sur le serveur web puis vérification des infos    
`aws sts get-caller-identity`     


17. liste des s3 buckets    
`aws s3 ls`     
on copie le nom qui commence par "cfst" du bucket avec "s3bucketdev"    


18.  Affiche le contenu d'un bucket       
`aws s3 ls s3://<s3bucketdev-123>`    avec "<s3bucketdev-123>" le nom du bucket     
s'affiche une liste des fichiers dans le bucket 


19. Partie PROD    
On refait de m^me mais avec la policy de prod

[fichier log](./archives/formation/cloud/aws_solutionArchitectAssociate/ec2_lab2/log_ec2_lab2.log)    





=======================================================================================================================  

<span style="color: #EA9811">

###  **EC2 QUIZZ** 
</span>

![ec2 quizz question 01](./images/aws_solutionArchitectAssociate/ec2_quizz001.png)    
![ec2 quizz question 02](./images/aws_solutionArchitectAssociate/ec2_quizz002.png)    
![ec2 quizz question 03](./images/aws_solutionArchitectAssociate/ec2_quizz003.png)    
![ec2 quizz question 04](./images/aws_solutionArchitectAssociate/ec2_quizz004.png)    
![ec2 quizz question 05](./images/aws_solutionArchitectAssociate/ec2_quizz005.png)    



**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/38.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/39.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/40.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/41.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/42.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/43.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/44.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/45.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/46.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/47.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/48.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/49.png)         




=======================================================================================================================  

_______________________________________________________________________________________________________________________    




<span style="color: #CE5D6B">

##  **EBS: ELASTIC BLOCK STORAGE / EFS: ELASTIC FILE SYSTEM** 
</span>


======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS OVERVIEW** 
</span>


[Documentation aws - ebs faqs](https://aws.amazon.com/ebs/faqs/)     
[Documentation aws - attach a volume to multiple instances with amazon EVS Multi-Attach](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volumes-multi.html)    



EBS c'est quoi?      
Ce sont des volumes de différents types que l'on attache aux instances EC2         
C'est une sorte de disque dur virtuel sur le cloud.        
On peut par exemple avoir 1 volume pour l'OS, 1 volume pour l'application (apache), 1 volume pour les datas....    

![ebs-architectureEbs](./images/aws_solutionArchitectAssociate/ebs-architectureEbs.png)  

Ils ont été créés pour:   
- les charges de travail en production (charges de travail critiques)     
- grande disponibilité: repliqué automatiquement dans 1 unique Availability zone pour protéger contre les plantages hardware   
-  scalable: peut augmenter dynamiquement sa capacité et change le type de volume en fonction des performances qui impactent le system    



**IOPS**: entrées / sorties par seconde

**Amazon EBS Multi-Attach** vous permet d'**attacher un volume SSD IOPS** provisionnés (io1 ou io2) **à plusieurs instances situées dans la même zone de disponibilité**    
Il est possible d'attacher plusieurs volumes activés pour Multi-Attach à une instance ou à un ensemble d'instances    
Chaque instance à laquelle le volume est attaché dispose d'une autorisation complète en lecture et en écriture sur le volume partagé    
Multi-Attach permet de bénéficier d'une disponibilité d'application plus importante dans les applications Linux en cluster qui gèrent des opérations d'écriture simultanée    


.......................................................................................................................    

<span style="color: #11E3EA">

####  **EBS Considerations et limitations** 
</span>

* Les volumes Multi-Attach peuvent être attachés à un maximum de 16 instances Linux construites sur le Système Nitro qui se trouvent dans la même zone de disponibilité    
Multi-Attach est activé pour s'attacher à des instances Windows, mais le système d'exploitation ne reconnaît pas les données sur le volume partagé entre les instances     

> _Remarque_: Le Système Nitro est un ensemble de composants matériels et logiciels élaborés par AWS qui garantissent des performances élevées, une haute disponibilité et un niveau de sécurité élevée. [Documentation aws nitro](http://aws.amazon.com/ec2/nitro/)    

* Multi-Attach est pris en charge exclusivement sur [Volumes SSD IOPS provisionnés (io1 et io2)](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/provisioned-iops.html#EBSVolumeTypes_piops)    
  * les volumes `io1` sont disponibles uniquement dans les régions suivantes : USA Est (Virginie du Nord), USA Ouest (Californie du Nord), USA Ouest (Oregon) et Asie-Pacifique (Séoul).    
  * les volumes `io2` sont disponibles dans toutes les régions prenant en charge ces types de volumes.    

> ⚠️Les volumes `io2` sont répartis en 2 sortes de volumes: les volumes `io2` et les [Volumes Block Express `io2`](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/provisioned-iops.html#io2-block-express)⚠️    

**Les 2 type de `io2` ne peuvent pas être pris en charge par Multi-Attach en même temps**    

* Les systèmes de fichiers standards, tels que XFS et EXT4, ne sont pas conçus pour être accessibles simultanément par plusieurs serveurs    

* Les volumes activés pour Multi-Attach ne prennent pas en charge l'isolation d'I/O    
Les protocoles d'isolation d'I/O contrôlent l'accès en écriture dans un environnement de stockage partagé afin de maintenir la cohérence des données    
Vos applications doivent fournir un ordre d'écriture pour les instances attachées afin de maintenir la cohérence des données.    

* Les volumes activés pour Multi-Attach ne peuvent pas être créés en tant que boot volumes       

* Les volumes activés pour Multi-Attach peuvent être attachés à un mappage de périphérique de stockage en mode bloc par instance    

* Multi-Attach ne peut pas être activé lors du lancement de l'instance via la console Amazon EC2 ou l'API Runinstances    

* Les volumes activés pour Multi-Attach présentant un problème au niveau de la couche d'infrastructure Amazon EBS sont indisponibles pour toutes les instances attachées    



.......................................................................................................................    

<span style="color: #11E3EA">

####  **EBS Performances** 
</span>

Chaque instance attachée est capable de piloter ses performances IOPS maximales jusqu'aux performances provisionnées maximales du volume    
Les performances agrégées de toutes les instances attachées ne peuvent pas dépasser les performances provisionnées maximales du volume    


.......................................................................................................................    

<span style="color: #11E3EA">

####  **EBS Utiliser Multi-Attach** 
</span>

Les volumes activés pour Multi-Attach peuvent être gérés de la même manière que n'importe quel autre volume Amazon EBS. Toutefois, pour utiliser la fonctionnalité Multi-Attach, vous devez l'activer pour le volume    




<span style="color: #965DCE">

#####  **EBS activer Multi-Attach** 
</span>

1. Ouvrez la **console Amazon EC2** à l'adresse https://console.aws.amazon.com/ec2/.

2. Dans le panneau de navigation, choisissez **Volumes**.

3. Choisissez **Créer un volume**.

4. Pour **Volume Type** (Type de volume), sélectionnez **Provisioned IOPS SSD (io1)** (SSD à IOPS provisionnés (io1)) **ou Provisioned IOPS SSD (io2)** (SSD à IOPS provisionnés (io2)).

5. Pour **Size** (Taille) et IOPS, choisissez la **taille de volume requise et le nombre d'I/O par seconde à provisionner**.

6. Pour **Availability Zone** (Zone de disponibilité), choisissez la **même zone de disponibilité que celle dans laquelle se trouvent les instances**.

7. Pour **Amazon EBS Multi-Attach**, choisissez **Enable Multi-Attach** (Activer Multi-Attach).

8. (Facultatif) Pour Snapshot ID (ID d'instantané), choisissez l'snapshot à partir duquel créer le volume.

9. Définissez l'**état du chiffrement du volume**.

  **Si l'snapshot sélectionné est chiffré**, ou si votre compte est activé pour le chiffrement par défaut, le chiffrement est activé automatiquement et vous ne pouvez pas le désactiver. Vous pouvez choisir la clé KMS à utiliser pour chiffrer le volume.

  **Si l'snapshot sélectionné n'est pas chiffré** et que le chiffrement par défaut n'est pas activé pour votre compte, le chiffrement est facultatif. Pour chiffrer le volume, pour Encryption (Chiffrement), choisissez Encrypt this volume (Chiffrer ce volume), puis sélectionnez la clé KMS à utiliser pour chiffrer le volume.


10. (Facultatif) Pour attribuer des identifications personnalisées au volume, dans la section Tags (Identifications), choisissez Add tag (Ajouter une identification), puis saisissez une paire clé-valeur d'identification. Pour plus d'informations, consultez Baliser vos ressources Amazon EC2.

11. Choisissez **Créer un volume**.



.......................................................................................................................    

<span style="color: #11E3EA">

####  **EBS Differents types de volumes** 
</span>   


- General Purpose SSD  ➡️ en général utilisé dans les boot volumes (OS par exemple)
  - **gp2**: 
    - 3 IOPS par Gb ➡️ max de 16 000 IOPS par volume
    - pour les volumes < 1Tb ➡️ peut aller jusqu'à 3000 IOPS
    - bon choix pour les boot volumes ou les developpement/test d'applications 

  - **gp3**: 
    - une baseline de 3000 IOPS et 125Mb/s sans tenir compte de la taille du volume     
    - ideal pour les applications qui nécessitent de hautes performances et un faible cout (MySQL, Cassandra, virtual desktop, hadoop)   
    - utilisateurs qui recherchent à pouvoir augmenter leur performance à 16000 IOPS at 1000 Mb/s additionnel    
    - les performances de gp3 sont 4 fois supérieures à celles de gp2    


- Provisioned IOPS SSD (io1/io2)
  - **io1**: 64000 IOPS par volume ➡️ 50 IOPS par Gb    
      N'est utilisé que dans le cas où il est nécessaire d'avoir plus de 16000 IOPS (beaucoup plus cher que gp)    

  - **io2**: la dernière génération ➡️  meilleure performance dans la durée que io1 et même tarifs     


- Thoughput Optimized HDD 
  - **st1**: c'est le volume le moins cher. C'est un "vieux" volume (magnetique)
    - 40 Mb/s par Tb    
    - peut aller jusqu'à 250 Mb/s par Tb    
    - max de 500Mb/s par volume    
    - fréquement utilisés via de charges de travail intensives
    - big data, data warehouses, ETL, log processing 
    - ne peut pas être un boot volume    

  - **sc1**:  cold hdd     
    - le volume le moins cher    
    - 12Mb/s par Tb    
    - peut aller jusqu'à 80 Mb/s par Tb
    - max de 250 Mb/s par volume
    - bon choix pour les données peu utilisées et requêtées    
    - ne peut pas être un boot volume    


![EBS IOPS vs Throughput](./images/aws_solutionArchitectAssociate/ebs_IopsVsThroughput.png)    

.......................................................................................................................  

[ebs_overview_examTips01.png](./images/aws_solutionArchitectAssociate/ebs_overview_examTips01.png)    
[ebs_overview_examTips02.png](./images/aws_solutionArchitectAssociate/ebs_overview_examTips02.png)    

.......................................................................................................................  

======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS VOLUMES AND SNAPSHOTS** 
</span>



Un volume c'est quoi ?    
Les volumes existent sur EBS: c'est un disque dur virtuel    
Chaque instance EC2 doit avoir au moins 1 volume ➡️ il est appelé **root device volume**    
C'est là qu'est installé l'OS    

Un snapshot c'est quoi ?     
Les snapshots existent sur S3    
C'est une "photo" à un instant t d'un volume    
Ils sont incrementals ➡️ le 1er prendra du temps et de la place       

Il existe 3 sortes de snapshots:
- **consistent snapshots**: il faut arrêter l'instance pour prendre ce type de snapshot    
- **encrypted snapshots**: un volume EBS encrypté donnera un snapshot encrypté automatiquement    
- **sharing snapshots**: il est possible de partager un snapshot SEULEMENT dans la region dans laquelle il se trouve. Pour le partager avec une autre region, il fafut d'abord faire une copie dans l'autre région de ce snapshot    


Que sait-on sur les volumes EBS?   

![EBS Volumes infos](./images/aws_solutionArchitectAssociate/ebs_volumesInfos.png)      



Pour supprimer un volume cela se fait en 2 étapes (dans la partie `ec2/volumes/actions`):    
- détacher le volume (attendre que cette action soit terminée)    
- supprimer le volume     



Pour créer un snapshot, dans `ec2/volumes/actions/create a snapshot`, donner un nom clair puis exécuter   

Pour copier le snapshot dans 1 autre region il faut faire: `ec2/volumes/actions/copy`, choisir la region dans laquelle nous souhaitons copier ce snapshot    

Il pourra être utiliser dans cette region après cela ➡️ créer un clone d'instance par exemple   


![ebs-volumesSanpshots_examTips](./images/aws_solutionArchitectAssociate/ebs-volumesSanpshots_examTips.png)    



======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS PROTECTION EBS VOLUMES WITH ENCRYPTION** 
</span>



[Documentation aws: encrypt and unencrypt ebs volume in linux](https://repost.aws/knowledge-center/create-unencrypted-volume-kms-key)    
[Documentation aws: amazon ebs encryption](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html)    


EBS Encryption c'est quoi ?    
On utilise **aws kms** (Key Management Service) avec une **cmk** (Customer Master Key) lors de la creation et encryption des volumes et snapshots    


Que se passe-t-il lors de l'encryption ?   
Les datas sont encryptées à l'interieur du volume    
Toutes les data en transition entre l'instance et le volume sont encryptées    
Tous les snapshots sont encryptés   


- L'encryption se gère seule (pas d'action de notre part)    
- La latence durant l'opération est très faible    
- La copie d'un snapshot autorise l'encryption  
- Un snapshot d'un volume encrypté est lui aussi encrypté     
- il est possible d'encrypter le boot volume (appelé aussi root volume) lors de a création   


Pour avoir une instance encryptée à partir d'une AMI encryptée il faut suivre les 4 étapes:      
- créer un snapshot d'un volume root non encrypté    
- créer une copie d'un snapshot et ajouter des options d'encryption    
- créer une AMI depuis un snapshot encrypté    
- utiliser cette AMI pour lancer une nouvelle instance encryptée    


![ebs_encrypted-examTips](./images/aws_solutionArchitectAssociate/ebs_encrypted-examTips.png)    
![ebs_encrypted-examTips02](./images/aws_solutionArchitectAssociate/ebs_encrypted-examTips02.png)    







======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS EC2 HIBERNATION** 
</span>


EC2 hibernation c'est quoi ?    
Lors d'une hibernation, les données de l'instance qui sont en mémoire s'écrivent sur le root volume ebs (ou autre s'il y a plusieurs volumes et en fonction de la data) pour pouvoir être persistantes     

Lors de la sortie d'hibernation, ces datas seront reportées en mémoire     
  ➡️ cela veut dire que ces instances sont très rapides à sortir d'hibernation puisque pas besoin de reboot pour être disponibles    

![ebs_hibernation_examTips](./images/aws_solutionArchitectAssociate/ebs_hibernation_examTips.png)    






======================================================================================================================= 

<span style="color: #EA9811">

###  **EFS OVERVIEW** 
</span>


EFS c'est quoi ?     
**EFS**: **Elastic File System**    
Il s'agit d'un montage NFS qui peut être monté sur plusieurs instances EC2     
EFS peut fonctionner avec plusieurs instances EC2 dans plusieurs availability zones    
EFS est à haute disponibilité et scalabilité    
EFS est cher      


![EFS architecture](./images/aws_solutionArchitectAssociate/efs_architecture.png)    


Cas d'utilisations:    
- **Content management**: très bien adapté aux contenus de systeme de management, facilité de partage de contenu entre instances EC2    

- **Web Servers**: très bonne adaptaion pour les web servers ➡️ il est nécessaire d'avoir un seul dossier structure du site   



Le protocole utilisé est **NFSv4**    
Il est compatible avec une AMI Linux (pas windows)    
Pour l'encryption KMS est utilisé    
Ses FS se scalent automatiquement, pas besoin de planning de capacité à définir    
On paie à l'utilisation   



Ses performances sont:    
![efs_performance](./images/aws_solutionArchitectAssociate/efs_performance.png)    
![efs_controlling performance](./images/aws_solutionArchitectAssociate/efs_performance02.png)    


EFS est fourni avec un storage tier qui vous permet de déplacer les datas depuis 1 tier vers 1 autre après X jours (en fonction de ce qui est défini dans le cycle de vie)   


![efs_examTips](./images/aws_solutionArchitectAssociate/efs_examTips.png)     





======================================================================================================================= 

<span style="color: #EA9811">

###  **FSx OVERVIEW** 
</span>


[Documentation aws: EFS faqs](https://aws.amazon.com/efs/faq/)    
[Documentation aws: FSx for windows file server faqs](https://aws.amazon.com/fsx/windows/faqs/)    
[Documentation aws: FSx for lustre faqs](https://aws.amazon.com/fsx/lustre/faqs/)    
[Documentation aws: amazon FSx](https://aws.amazon.com/fsx/)    


FSx pour windows      
  Il fournit le native Microsoft file system complet et peut le bouger facilement 
  



FSx pour windows vs EFS    
  ![ebs_diffEfs8FsxWindows.png](./images/aws_solutionArchitectAssociate/ebs_diffEfs8FsxWindows.png)     




FSx pour Lustre      
  Un file system complètement managé et optimisé pour les charges de travail de calcul instensif    
    - haute performance de calcul    
    - machine learning   
    - media data processing workflows    
    - electronic design automation (Intelligence Artificielle)      


![ebs_fsx-examTips](./images/aws_solutionArchitectAssociate/ebs_fsx-examTips.png)    






======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS AMAZON MACHINE IMAGES:EBS vs INSTANCE STORE** 
</span>


[Documentation aws: Prebacking vs Bootstraping AMIs](https://docs.aws.amazon.com/whitepapers/latest/overview-deployment-options/prebaking-vs.-bootstrapping-amis.html)     
[Documentation aws: difference between instane store and EBS volume ?](https://repost.aws/knowledge-center/instance-store-vs-ebs)     


[Documentation aws: Amazon Machine Image AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html)    

.......................................................................................................................    

<span style="color: #11E3EA">

####  **PREBACKING vs BOOTSTRAPPING AMIs** 
</span>

Ces 2 pratiques font partie des stratégies de déploiements.    

Si votre application s'appuie fortement sur la personnalisation ou le déploiement d'applications sur des instances Amazon EC2, vous pouvez optimiser vos déploiements grâce à des pratiques bootstrapping (amorçage) et de prebacking (en avance de phase)     

* **bootstrapping**: installer vos applications, dépendances ou customisations pendant que EC2 se lance       

* **AMI**: une AMI fournit les informations nécessaires au lancement d'une instance (OS, volumes de stockage, permissions, packages logiciels, etc.). Vous pouvez lancer plusieurs instances identiques à partir d'une seule AMI. Chaque fois qu'une instance EC2 est lancée, vous sélectionnez l'AMI qui doit être utilisée comme modèle. 

* **prebacking**: Le prebacking est le processus d'intégration d'une partie importante de vos artefacts d'application dans une AMI.     
Le prebacking des composants d'application dans une AMI peut accélérer le lancement et l'opérationnalisation d'une instance Amazon EC2. 


Les pratiques de prebacking et bootstrapping peuvent être combinées au cours du processus de déploiement pour créer rapidement de nouvelles instances personnalisées en fonction de l'environnement actuel.




.......................................................................................................................    

<span style="color: #11E3EA">

####  **DIFFERENCE BETWEEN INSTANCE STORE AND EBS VOLUMES** 
</span>


Certains types d'instances EC2 sont fournies avec une forme de stockage de périphérique de bloc directement attaché appelé magasin d'instance (instance store). Utilisez cet instance store pour le stockage temporaire. Les données stockées dans les volumes de stockage d'instance ne sont pas persistantes lors des arrêts d'instance, des résiliations ou des pannes matérielles.     


Pour les données que vous souhaitez conserver plus longtemps, ou si vous souhaitez chiffrer les données, utilisez plutôt les volumes Amazon EBS.    
Fonctionalités EBS Volumes:    
- Les volumes EBS conservent leurs données lors des arrêts et des résiliations d'instances.    
- Vous pouvez sauvegarder des volumes EBS avec des snapshots EBS.    
- Vous pouvez supprimer des volumes EBS d'une instance et les rattacher à une autre.    
- Les volumes EBS prennent en charge le chiffrement de volume complet.    


.......................................................................................................................    


Une AMI c'est quoi?  
**AMI**: **Amazon Machine Image**   
Elle contient les informations nécessaires pour exécuter une instance    

Quand on parle AMI on pense à 5 choses:    
- region
- OS
- architecture (32 ou 64-bit)
- permissions d'exécution
- stockage pour le root volume   

Ces AMIs sont soit des:    
- **Amazon EBS**: Le root volume d'une instance exécutée depuis une AMI EBS volume sera créée à partir d'un snapshot EBS    
- **Instance Store**: Le root volume d'une instance exécutée depuis une AMI instance store sera créée à partir d'un template conservé sur S3    


Un **instance store** est éphémère. Il NE PEUT PAS être arrêté sous risque de perdre ses datas    
Un **EBS volume** est long-terme. Il PEUT être arrêté    


![ebs_ami_examTips](./images/aws_solutionArchitectAssociate/ebs_ami_examTips.png)    









======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS AWS BACKUP** 
</span>


Qu'est-ce qu'un backup aws ?
AWS backup vous permet de consolider les backups des services (EC2, EBS, EFS...). Cela inclus aussi les techno database comme RDS....    


AWS Backup peut être utilisé avec AWS Organization pour sauvegarder de multiple accounts dans votre organisation    


Les avantages de AWS Backup:
- ils sont centralisés dans 1 seule console quelque soit le nombre d'accounts de backup utilisés       
- Ils peuvent être automatisés, créer des cycles de vie de policies, déterminer la durée des ces backups    
- Ils améliorent la conformité; des backups de policies peuvent être imposés lors de backups (encryptés). Les audit se font plus facilement de cette manière puisque "complets"    



![aws_backup_examTips](./images/aws_solutionArchitectAssociate/aws_backup_examTips.png)    







======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS EXAM TIPS** 
</span>


![ebs_examTips001](./images/aws_solutionArchitectAssociate/ebs_examTips001.png)    
![ebs_examTips002](./images/aws_solutionArchitectAssociate/ebs_examTips002.png)    
![ebs_examTips003](./images/aws_solutionArchitectAssociate/ebs_examTips003.png)    
![ebs_examTips004](./images/aws_solutionArchitectAssociate/ebs_examTips004.png)    
![ebs_examTips005](./images/aws_solutionArchitectAssociate/ebs_examTips005.png)    
![ebs_examTips006](./images/aws_solutionArchitectAssociate/ebs_examTips006.png)    
![ebs_examTips007](./images/aws_solutionArchitectAssociate/ebs_examTips007.png)    
![ebs_examTips008](./images/aws_solutionArchitectAssociate/ebs_examTips008.png)    
![ebs_examTips009](./images/aws_solutionArchitectAssociate/ebs_examTips009.png)    
![ebs_examTips010](./images/aws_solutionArchitectAssociate/ebs_examTips010.png)   
![ebs_examTips011](./images/aws_solutionArchitectAssociate/ebs_examTips011.png)    
![ebs_examTips012](./images/aws_solutionArchitectAssociate/ebs_examTips012.png)       









======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS LAB: Reduce storage costs with EFS** 
</span>


![ebs_lab_sujet](./images/aws_solutionArchitectAssociate/labs/ebs_lab/ebs_lab_sujet.png)     



username: cloud_user      
password: $1Htkqlkvbgrabr     
lien: https://317432833221.signin.aws.amazon.com/console?region=us-east-1    


cloud server webserver01    
username: cloud_user      
password: H)l#]-3+    
webserver-01 pubic IP: 54.225.43.92      
terminal: https://ssh.instantterminal.acloud.guru/?_ga=2.181965735.1019105499.1681456116-1058941318.1681456116    



Architecture au debut du lab:    
![ebs_lab_beginingArchitecture](./images/aws_solutionArchitectAssociate/labs/ebs_lab/ebs_lab_beginingArchitecture.png)     


Architecture cible en fin du lab:    
![ebs_lab_ArchitectureCibleFinDeLab](./images/aws_solutionArchitectAssociate/labs/ebs_lab/ebs_lab_ArchitectureCibleFinDeLab.png)         



Se connecter à la console EC2 ➡️ dans les instances déjà créées (webserver-01 à 03), on les sélectionne (1 par 1), dans l'onglet storage on voit 1 2e volume (le 1er est le root volume) de 10 Go sur chaque.   
C'est ce volume que nous allons supprimer pour un nouveau volume EFS partagé (donc ce sera moins cher ➡️ but du lab)   


Dans la partie de recherche de service da la console on ecrit **EFS** et on ouvre dans 1 nouvel onglet    
Dans cet onglet on va créer un nouveau volume (**sharedweb**) dans 1 seule region (customize/select la region; us-east-1)    
Une fois le volume créé, on va dans `view file system/networks` ➡️ on voit que le reseau pour la region choisit est en cours de création (on attend que la création soit terminée)    

Une fois la creation du network terminé cliquer sur `manage`    
Dans cette page supprimer le security group présent et sélectionner celui dont le nom contient **EC2SecurityGroup**    



On retourne maintenant dans l'onglet EC2 et on va choisir dans la gauche de la fenêtre **security groups**    

On selectionne le security group choisit précédemment (ontient **EC2SecurityGroup**), on va dans l'onglet **inboud rules** et on va editer ces regles    

On ajoute un regle pour le NFS sur toutes les plages d'IPs (0.0.0.0/0)    


On retourne dans le dashboard EC2 et on se connecte à l'instance webserver-01 





```bash
# une fois connecté à la console webserver-01

## voir les differents fs
lsblk
### on voit que le /data est le fs de 10 Go que nous voulons changer

ls /data    ## affiche 10 fichiers 

## on crée un fs /efs qui sera notre montage partagé plus tard
sudo mkdir /efs
```

Dans l'onglet efs, on cliqc sur le bouton **attach**, on sélectionne `mount via ip` et on copie la commande proposée pour monter le volume
`sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport 10.0.0.206:/ efs`

```bash
# on colle et modifie la commande comme ceci (fin de commande ➡️ /efs et pas efs seul)
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport 10.0.0.206:/ /efs

## on verifie que le volume soit monté 
ls /efs   # rien n'apparait: normal  
## idem avec lsblk

## on vérifie que le mount c'est fait
mount
#------------------------------------------------------
...
10.0.0.206:/ on /efs type nfs4 (rw,relatime,vers=4.1,rsize=1048576,wsize=1048576,namlen=255,hard,noresvport,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.0.0.100,local_lock=none,addr=10.0.0.236)
#------------------------------------------------------
### le mount s'est fait correctement 

df -h 
#------------------------------------------------------
...
10.0.0.206:/    8.0E     0  8.0E   0% /efs
#------------------------------------------------------
```

Déplaçons maintenant les fichiers de `/data` vers `/efs`   
```bash
sudo rsync -rav /data/* /efs

ls /efs   # les fichiers sont présents
```

Il faut maintenant supprimer les "vieilles" données et supprimer le volume de 10 Go  

```bash
# on commence par demonter le volume data
sudo umount /data

# on modifie le fichier fstab pour ne pas qu'il el remonte
sudo vi /etc/fstab
## on supprime la lgne qui commence par UUID (l'autre ligne est pour le /boot)
```


On doit maintenant monter le nouveau volume efs (sharedweb), pour cela (avant de fermer le fichier `/etc/fstab`), on va dans l'onglet efs    

On copie l'IP qui est présente dans la commande (`10.0.0.206:/`), on retourne dans le terminal et on colle cette valeur sur la ligne "vierge"
On rajoute comme la ligne du dessus `/` et `nfs4` et à partir de l'onglet EFS `nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport`  puis on finir par "  0  0"   


```bash
sudo vi /etc/fstab
#-------------------------------------------
LABEL=cloudimg-rootfs   /        ext4   defaults,discard        0 0
10.0.0.206:/            /data    nfs4   nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport     0 0
#-------------------------------------------
```

Démonter le `/efs` pour s'assurer que tout fonctionne

```bash
sudo umount /efs 
df -h     # /efs ou /data n'apparait pas 

sudo mount -a
df -h     # /data apparait  

ls /data  # les 10 fichiers apparaissent  
```

Maintenant que nous n'avons plus besoin du volume de 10 Go, on retourne sur l'onglet EC2 pour supprimer ce volume  
`EC2 Dashboard/Volumes`  ➡️ 6 volumes apparaissent   


On sélectionne le volume qui est attaché à l'instance (voir la colonne `Attached Instances`, sélectionner le volume qui est attaché au webserver-01), cliquer sur `actions/detach volume` et attendre que le volume soit détaché    
Une fois détaché, sélectionner à nouveau le volume puis `actions/delete volume`  



Faire de même pour les 2 autres serveurs (la partie la plus importante étant de mettre les mêmes infos dans les fichiers `/etc/fstab`)


```bash
sudo umount /data
sudo vi /etc/fstab    ## on colle les mêmes valeurs que sur le serveur-01
sudo mount -a
df -h                 ## /data apparait 
ls /data              ## les 10 fichiers apparaissent
```


On retourne dans l'onglet EC2 et de la même manière on supprime les volumes devenus inutiles


======================================================================================================================= 

<span style="color: #EA9811">

###  **EBS QUIZZ** 
</span>




![ebs_quizz01](./images/aws_solutionArchitectAssociate/ebs_quizz01.png)     
![ebs_quizz02](./images/aws_solutionArchitectAssociate/ebs_quizz02.png)     
![ebs_quizz03](./images/aws_solutionArchitectAssociate/ebs_quizz03.png)     
![ebs_quizz04](./images/aws_solutionArchitectAssociate/ebs_quizz04.png)     
![ebs_quizz05](./images/aws_solutionArchitectAssociate/ebs_quizz05.png)     




======================================================================================================================= 

_______________________________________________________________________________________________________________________    


<span style="color: #CE5D6B">

##  **DATABASES** 
</span>


======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - RDS: RELATIONAL DATABASE SERVICE  OVERVIEW** 
</span>


.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - RDS - Relational databases** 
</span>


Les databases relationnelles existent depuis de nombreuses années et sont utilisées par de nombreuses applications.   

Sa structure est composée de:    
- **tables**: les données sont organisées dans des tables
- **lignes**: l'item de la donnée
- **colonnes**: le champ dans la database 

![db_rds_exempleDbClassic](./images/aws_solutionArchitectAssociate/db_rds_exempleDbClassic.png)    
  ➡️ montre dans la table "Customer" comment les données sont accessibles avec les lignes et les colonnes    

Il existe différents fournisseurs de Database Relationnelles comme:     
![db_rds_exempleFournisseursDbRelationnelles](./images/aws_solutionArchitectAssociate/db_rds_exempleFournisseursDbRelationnelles.png)    
 ➡️ **ces 6 fournisseurs sont disponibles chez AWS**




.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - RDS - RDS avantages** 
</span>


Ces databases sont accessibles après création en quelques minutes seulement.        
Elles sont construites sur des instances EC2.     
Nous n'avons pas accès dans ce cas à l'OS, seulement à la database    

Il est possible d'en avoir dans de nombreuses Availibilty zones.            
Cela permet de prémunir de failover (basculement d'une zone à une autre) capability. Un failover built-in est généré par defaut   
Un backup automatisé est mis en place               

En comparaison une installation "manuelle classique" prendrait dans votre data center 8 jours ou plus    






.......................................................................................................................    


<span style="color: #EA9811">

###  **DB - RDS - OLTP (OnLine Transaction Process) and OLAP (OnLine Analytical Processing)**         
</span>


Ces db **RDS** sont généralement **utilisées pour des workloads OLTP**                  

Différence entre OLTP et OLAP:            
![db_rds_diffEntreOltpEtOlap](./images/aws_solutionArchitectAssociate/db_rds_diffEntreOltpEtOlap.png)              
  ➡️ OLTP : pour des **transactions**   ▶️ rapide             
  ➡️ OLAP : avec des **analyses**       ▶️ lent            

![db_rds_exempleOLAP](./images/aws_solutionArchitectAssociate/db_rds_exempleOLAP.png)    
  ➡️ une db RDS n'est pas faite pour de l'analyse   ▶️ cela concerne le big data: utiliser une autre forme de database pour ce type de service (ex: datawarehouse comme Redshift)   

   
**RDS**       == **OLTP**        
**Redshift**  == **OLAP**        




.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - RDS - Multi-AZ** 
</span>


![db_rds_multiAz](./images/aws_solutionArchitectAssociate/db_rds_multiAz.png)     
  ➡️ la replication dans 1 autre zone se fait automatiquement  
  ➡️ AWS s'occupe "tout seul" de cette replication   

Quels types de RDS peuvent être configurées an tant que multi-AZ ?    
- **SQL Server**       
- **MySQL**    
- **MariaDb**    
- **Oracle**    
- **Postgresql**    

> _Remarque_: Tous ces types d'instances peuvent être aussi en standalone, pas forcement repliquées en multi-AZ





.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - RDS - Unplanned Failures and Maintenance** 
</span>



Dans le schéma précédent, on voit un load balancer ➡️ 3 instances EC2 ➡️ RDS 1 (dans AZ1)  ↔️ RDS 2 (dans AZ2)


Le lien entre les instances EC2 et le RDS 1 se fait de 2 manières: 
- IP ➡️ login en user/password 
- url (dans ce cas un DNS intervient) ➡️ login en user/password 

Le DNS peut tomber ➡️ le fqdn ne sera pas reconnu pour se connecter au RDS 1: on est dans le cas où RDS 1 n'est plus accessible (idem si RDS 1 tombe)     
  ▶️ dans ce cas, AWS bascule automatiquement vers le RDS 2 (sans coupure de service)

![db_rds_multiAzDisasterRecovery](./images/aws_solutionArchitectAssociate/db_rds_multiAzDisasterRecovery.png)     
  ➡️ Multi-AZ n'est pas fait pour le disaster recovery mais pour pour améliorer les performances   
  ➡️ on ne peut pas connecter "directement" une instance vers le RDS 2 pour améliorer la performance ➡️ IMPOSSIBLE   



Pour ameliorer les performances il faut ajouter des replicas (sera vu plus tard)    



....................................................................................................................... 


<span style="color: #EA9811">

###  **DB - RDS - Console demo** 
</span>


Dans la console AWS `RDS (dans la section Databse)/ créer une database / creation standard / <choix DB> MySQL / <choix version> / templates free-tiers pour le test / donner un nom à la db (mettre un nom qui decrit) /username (admin par defaut mais on peut le changer ➡️ toto) / mot de passe (totototo) / db instance class (db.t2.micro ➡️ la plus petite) / storage (ssd gp2 20 Go) / ....`

> _Remarque_: Si on choisit **production** dans les templates, l'option **multi-AZ** sera proposée sinon non  

Le endpoint qui est défini dans l'instance de la DB Connectivity and security est l'url utilisé par le DNS   

![db_rds_endpoint](./images/aws_solutionArchitectAssociate/db_rds_endpoint.png)    




.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - RDS - Exam Tips** 
</span>


![db_red_examTips](./images/aws_solutionArchitectAssociate/db_rds_examTips.png)    




.......................................................................................................................    

======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - INCREASING READ PERFOMANCE WITH READ REPLICAS** 
</span>


[Documentation aws: amazon RDS faqs](https://aws.amazon.com/rds/faqs/#Read_Replicas)    
[Documentation aws: read replicas, Multi AZ and Multi regions deployements](https://aws.amazon.com/rds/features/read-replicas/#Read_replicas.2C_Multi-AZ_deployments.2C_and_multi-region_deployments)    


**What is a read replica ?**

C'est une copie **read-only** de notre db primaire.   
**Un read replica permet d'améliorer les performances de la db**     

![db_readreplicasFonctionnement](./images/aws_solutionArchitectAssociate/db_readreplicasFonctionnement.png)    

Un read replica peut être dans plusieurs AZ mais aussi plusieurs regions      

Chaque read replica a son propre DNS endpoint (url mentionnée dans le DNS pour la connexion)    

![db_readreplicasDNS](./images/aws_solutionArchitectAssociate/db_readreplicasDNS.png)    

Un read replica peut devenir une db à part entière si on la dédit à des requêtes spécifiques (en général pour les  "grosses requêtes" pour ne pas "embouteiller" l'accès à la DB primaire)    
![db_readreplicasSwitchPrimaryForBigQueries](./images/aws_solutionArchitectAssociate/db_readreplicasSwitchPrimaryForBigQueries.png)    



Read replica est là pour améliorer les performances; il n'est pas utiliser pour les Disaster Recovery      
Il est nécessaire d'avor un backup automatique pour pouvoir deployer un read replica    
Plusieurs read replicas peuvent co exister. MySQL, MariaDb, PostgreSQL, Oracle et SQL Server autorise à avoir jusqu'à 5 read replcas par db instance   


Pour cela il faut aller dans la console, `selectionner la db/actions/create a read replica/ ...`
 ➡️ on choisit les configuration en fonction de notre besoin    


![db_readreplica-examTips](./images/aws_solutionArchitectAssociate/db_readreplica-examTips.png)    







======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - WHAT IS AMAZON AURORA ?** 
</span>


.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - What is Aurora ?** 
</span>


**Aurora** est une database propriétaire Amazon.    
C'est un moteur de base relationnelles compatibles MySQL et PostGreSQL qui combine la vitesse, la capacité haut de gamme avec la simplicité et les coûts bas de l'opensource    






.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - Aurora Performance** 
</span>


Elle est 5 fois plus performante que MySQL et 3 fois que PostgreSQL.    
Tout ça avec un prix plus bas pour des performances et capacités équivalentes    



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - Basics** 
</span>


Elle est pris en charge par l'autoscaling.   
Au minimum 10Gb et augmente jusqu'à 128 Tb    

Elle peut avoir des ressources compute jusqu'à 96vCPUs et 768GGb de RAM   

Elle a 2 copies dans chaque AZ avec au minimum 3 AZ (soit 6 copies mini)       


.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - Scaling** 
</span>



Aurora est concu pour pouvoir perdre 2 copies des datas sans affecter la capacité d'écriture ni affecter la capcité de lectures des 3 copies    

Aurora se scanne et se répare seul    



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - types of Aurora Replicas** 
</span>



Il existe 3 différents types de replica Aurora: 
- **Aurora Replicas**: jusqu'à 15 read replicas    
- **MySQL Replicas**: jusqu'à 5 read replicas    
- **PostGreSQL**: jusqu'à 5 read replicas     

![db_aurora_typesOfReplicas](./images/aws_solutionArchitectAssociate/db_aurora_typesOfReplicas.png)    


.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - Backups** 
</span>


Les backups sont automatiques avec les db Aurora et n'impactent pas les performances de la database    

Il est possible de prendre des snapshosts avec Aurora sans impact sur la performance    

Nous pouvons partager les snapshots avec d'autres AWS accounts   



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - Serverless** 
</span>


C'est à la demande, une configuration auto-scaling pour MySQL et PostgreSQL compatible editions d'Aurora    
Un cluster de db aurora serverless démarre, s'éteint, s'agrandit ou se rétrcit automatiquement en fonction des besoin de l'application   



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - AURORA - Exam Tips** 
</span>


![db_aurora_examTips](./images/aws_solutionArchitectAssociate/db_aurora_examTips.png)    




.......................................................................................................................    

======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - DYNAMODB OVERVIEW** 
</span>


[Documentation aws: DynamoDB faqs](https://aws.amazon.com/dynamodb/faqs/)    
[Documentation aws: Amazon DynamoDb](https://aws.amazon.com/about-aws/whats-new/2021/12/amazon-dynamodb-standard-infrequent-access-table-class/)    


.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - What is DynamoDB ?** 
</span>


C'est une database propriétaire Amazon mais n'est pas relationnelle.   
C'est une database rapide et flexible NoSQL    

Elle est utilisée en tant que service pour les applications qui ont des besoins consequents avec une latence de moins de 10 ms 

Elle est complètement managée et supporte les documents ou les modèles clé-valeurs   

C'est un modele de données flexible avec des performances fiables qui sont parfaites pour les mobiles, web jeux, ....



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - High Level DynamoDb** 
</span>

Elles sont stockées sur du SSD, sont dans des datacenters dans 3 zones géographiquement distinctes, a une forte cohérence de lecture    




.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - Read Consistency** 
</span>


Qu'est qu'une **eventually consistent read** (consistence de lecture éventuelle) ?    
Une requête DOIT retourner le resultat en moins d'une seconde à travers les copies.    
Une nouvelle requête identique après un court moment doit retourner immédiatement la data updatée  


Qu'est-ce qu'une **strong consistent read** ?    
Une requête retourne comme resultat toutes les écritures qui ont eu une réponse de succès avant la lecture   
 ➡️ nous n'avons pas à attendre une seconde dans ce cas    




.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - DynamoDB Accelerator - DAX** 
</span>


Il est complètement managé, de haute capicité en mémoire cache.    
Cela augmente 10 fois les performances de dynamoDB   
Reduit le temps des requêtes de la ms à la microseconde meme en charge    
N'a pas besoin de dev pour manager le cache   
C'est compatible avec des appels API DynamoDB   


![db_dynamaDB_DAX](./images/aws_solutionArchitectAssociate/db_dynamaDB_DAX.png)    



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - On-demand capacity** 
</span>


Son prix est dépendant du nombre de requêtes    
Equilibre entre le cout et la performance    
Pas besoin de capacity minimale   

On paie plus par requêtes que par capicité provisionnée   
Utilisé pour les lancements de nouveaux produits   




.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - Security** 
</span>


L'encryption au repos utilise KMS       
Utilise le VPN pour des connexions site à site     
Utilise Direct Connect (DX)    
Utilise les IAM policies et roles pour les accès   ➡️ pour définir finement qui a les accès    
Est intégré avec CloudWatch et CloudTrail          ➡️ monitoring    
Utilise les endpoints VPC   



.......................................................................................................................    

<span style="color: #EA9811">

###  **DB - DynamoDB - Exam Tips** 
</span>


![db-dynamoDB_examTips01](./images/aws_solutionArchitectAssociate/db-dynamoDB_examTips01.png)    
![db-dynamoDB_examTips02](./images/aws_solutionArchitectAssociate/db-dynamoDB_examTips02.png)    

.......................................................................................................................  

======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - WHEN DO WE USE DYNAMODB TRANSACTIONS ?** 
</span>


Diagramme **ACID**    
![db_dynamoDb_AcidDiagram](./images/aws_solutionArchitectAssociate/db_dynamoDb_AcidDiagram.png)    


Les transactions DynamoDB autorise les dev pour l'**atomaticy**, **consistency**, **isoliation** aet **durability** (ACID) à travers 1 ou  plusieurs tables avec 1 seul AWS account dans 1 region    

 ➡️ Il est possible d'utiliser les transactions pour créer des applications qui nécessitent des insertions, suppressions, modifications coordonnées pour de multiples items dans 1 seule opération logique   


**ACID** veut dire "basiquement" TOUT OU RIEN    
  ➡️ ALL: les transactions sont réussiées dans 1 ou  plusieurs tables    
  ➡️ NOTHING: toutes les transactions sont failed    


Les transactions dynamoDB sont utilisées dans les cas suivants:    
- effectuer des transactions financières   
- remplir et gérer des commandes    
- construire des moteurs de jeux multiplayer    
- coordonnées des actions à travers des composants et services distribués    


![db_dynamoDb_transactions](./images/aws_solutionArchitectAssociate/db_dynamoDb_transactions.png)     


![db_dynamoDb_transactions-examTips](./images/aws_solutionArchitectAssociate/db_dynamoDb_transactions-examTips.png)     





======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - SAVING YOUR DATA WITH DYNAMODB BACKUPS** 
</span>


DynamoDB a une solution de backup et restore on-demand       
  - full backups n'importe quand
  - zero impact sur les performance et disponibilité
  - consistent en quelques secondes et conservé jusqu'à suppression
  - se trouve dans la même region que la source   


**PITR**: Point In Time Recovery:    
- protège contre les écritures ou suppressions accidentelles   
- restore n'importe quel point des 35 derniers jours    
- possède des backups incrementiels
- pas activé par defaut 
- le point le plus recent restorable est il y a 5 minutes      





======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - TAKING YOUR DATA GLOBAL WITH DYNAMODB STREAMS AND GLOBAL TABLES** 
</span>

**Quels sont les flux (streams) dans dynamoDB ?**    
Les flux sont du style FIFO (first in first out).    
Chaque entrée est séquencée avec un ID et ces séquences sont conservées dans dynamoDB pendant 24 heures.  
Chaque séquence est conservée en tant que shard (fragment)         


![db_dynamoDB_streams-shard](./images/aws_solutionArchitectAssociate/db_dynamoDB_streams-shard.png)    


⚠️     
**Global tables**    
Le principe des global tables est d'avoir un gestion multi-masters et des replications multi-regions     
Cela consiste à avoir des applications à l'échelle mondiale    
Ces tables sont basées sur les flux dynamoDB    
Il y a des redondances multi-regions pour les disaster recovery et/ou pour de la haute disponibilité    
Aucune application n'est ré écrite   
Le temps de replication est de moins d'une seconde     
⚠️


Dans la console aws:     
`Service DynamoDB/Create table/ Table name: mydynamodb-tb1/partition key: user-di (string)` puis créer la table  

Une fois la table créée et qu'elle est active, clique sur la table (ouvre dans la fenêtre une nouvelle fenêtre avec les détails de cette table), en cliquant sur "explore table" on peut aller créer un item dans la table par exemple avec ses données    

La création d'un replica pour les global tables prend environ 5 minutes    

Dans le cas de suppression de replicas ➡️ supprimer les replicas PUIS supprimer le "master" (la 1ere table créée ➡️ dans sa region)    




======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - OPERATING MONGODB-COMPATIBLE DATABASES IN AMAZON DOCUMENTDB** 
</span>

[Documentation aws: DpcumentDB](https://docs.aws.amazon.com/documentdb/latest/developerguide/what-is.html)    

**What is mongoDB ?**    
**MongDB** est une **document database** qui permet la scalabilité et la flexibilité des datas ainsi que des fonctionnalités d'interrogation et d'indexation robustes   

_exemple de document database de mongodb_: 
```js
export async function insertDocuments (db) {
  //Get the documents collection
  const result = await collection.insertMany([
    {
      name: 'Sun Bakery Tratoria',
      stars: 4,
      categories: [
        'Pizza', 'Pasta', 'Italian', 'Coffee', 'Sandwiches'
      ]
    }, 
    {
      name: 'Blue bagels Grill';
      stars: 3,
      categories: [
        'bagels', 'Cookies', 'Sandwcihes'
      ]
    }
  ])
}
```


**What is Amazon DocumentDB ?**    
**Cela permet d'exécuter mongoDB sur le cloud AWS**    
Amazon DocumentDB (compatible avec MongoDB) est un service de base de données de documents rapide, scalable, hautement disponible et entièrement géré qui prend en charge les charges de travail MongoDB.       


**Pourquoi utiliser Amazon DocumentDB ?**    
Il n'est plus nécessaire de s'inquiéter des taches manuelles en rapport avec les workloads mongoDB, comme une gestion de software de cluster, la configuration des backups, le monitoring des workloads de production         
  ➡️ on peut se séparer les frais généraux de la production    


Amazon DocumentDB est conçu entièrement pour offrir les performances, la scalabilité et la disponibilité dont vous avez besoin lorsque vous exploitez des charges de travail MongoDB stratégiques à grande échelle. Dans Amazon DocumentDB, le stockage et le calcul sont découplés, ce qui permet de les mettre à l'échelle de façon indépendante. De plus, les développeurs peuvent augmenter la capacité en lecture à des millions de demandes par seconde en ajoutant jusqu'à 15 réplicas en lecture à faible latence en quelques minutes, quelle que soit la taille de vos données. Amazon DocumentDB est conçu pour offrir une disponibilité de 99,99 % et réplique six copies de vos données dans trois zones de disponibilité AWS.    


![db_mongoDB_-examTips](./images/aws_solutionArchitectAssociate/db_mongoDB_-examTips.png)    





======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - RUNNING APACHE CASSANDRA WORKLOADS WITH AMAZON KEYSPACES** 
</span>

[Documentation aws: Amazon Keyspaces](https://docs.aws.amazon.com/keyspaces/)    

**Amazon Keyspaces**: Service de base de données scalable, hautement disponible et géré compatible Apache Cassandra.    

**Amazon Keyspaces** (pour Apache Cassandra) est un service de base de données scalable, hautement disponible et géré compatible avec Apache Cassandra. Avec Amazon Keyspaces, vous pouvez exécuter vos wokloads Cassandra sur AWS en utilisant le même code d'application Cassandra et les mêmes outils de développement que ceux que vous utilisez actuellement. Vous n'avez pas à mettre en service, corriger ou gérer des serveurs, ni à installer, gérer ou exécuter des logiciels. Amazon Keyspaces étant serverless, vous ne payez donc que les ressources que vous utilisez. Le service dispose d'un mode de capacité à la demande qui peut traiter des milliers de requêtes par seconde avec une latence de quelques millisecondes sur un débit et un stockage pratiquement illimités.    



![db_amazonKeyspace-examTips](./images/aws_solutionArchitectAssociate/db_amazonKeyspace-examTips.png)    







======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - IMPLEMENTING GRAPH DATABASES USING AMAZON NEPTUNE** 
</span>


[Documentation aws: Neptune](https://docs.aws.amazon.com/neptune/latest/userguide/intro.html)    

**Amazon Neptune** est un service de bases de données orientées graphe rapide et fiable qui facilite la création et l'exécution d'applications fonctionnant avec des ensembles de données hautement connectés.


**Un graphe database c'est quoi ?**    
![db_graphDatabase](./images/aws_solutionArchitectAssociate/db_graphDatabase.png)    
  ➡️ c'est un graphe avec des relations entre chaque entrées (utilisé dans les reseaux sociaux) plutôt que des tables ou des documents      

Neptune stocke et navigue efficacement dans les données hautement connectées. Son moteur de traitement de requêtes est optimisé pour les principaux langages de requête de graphe, Apache TinkerPop Gremlin et RDF SPARQL du W3C. Neptune offre des performances élevées grâce aux API ouvertes et standard de ces cadres de graphe. De plus, Neptune est entièrement géré, vous n'avez pas à vous soucier des tâches de gestion de bases de données comme l'application de correctifs logiciels, l'installation, la configuration ou les sauvegardes. 

Amazon Neptune est optimisé pour les applications à faible latence et à débit élevé.       
Neptune est capable de traiter des requêtes de graphe sur des milliards de relations en quelques millisecondes.    
Il prend en charge jusqu'à 15 copies en lecture pour dimensionner le débit des requêtes à 100 000 requêtes par seconde.


![db_neptune-usecases](./images/aws_solutionArchitectAssociate/db_neptune-usecases.png)    


Vous pouvez créer une instance avec jusqu'à 64 To de stockage à dimensionnement automatique, une réplication à 6 nœuds dans toutes les zones de disponibilité et la prise en charge de 15 copies en lecture. 

Vous payez uniquement pour les ressources que vous utilisez.     


![db_neptune-examTips](./images/aws_solutionArchitectAssociate/db_neptune-examTips.png)    




======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - LEVERAGING AMAZON QUANTUM LEDGER DATABASE (AMAZON QLDB) FOR LEDGER DATABASES** 
</span>


[Documentation aws: Amazon QLDB](https://docs.aws.amazon.com/qldb/latest/developerguide/what-is.html)   


**AMAZON QLDB**: Base de données de registres avec un log des transactions transparent, immuable et vérifiable cryptographiquement (chiffrement détenu par une autorité centrale de confiance)    

Il est IMPOSSIBLE d'aller modifier directement une donnée dans la database  ➡️ **immutable**

Les utilisations principales sont la crypto monnaie, les compagnies de transport (traquer les containers, paquets...) et les compagnies pharmaceutiques (traquer la creation et distribution de médicaments et s'assurer qu'il n'y ait pas de contre façons qui soient produite)

![db_qldb-usecases](./images/aws_solutionArchitectAssociate/db_qldb-usecases.png)    



Amazon QLDB suit chaque modification des données d'application et conserve un historique complet et vérifiable des modifications au fil du temps

Vous ne payez que ce que vous utilisez, sans frais minimum ni utilisation obligatoire du service    


![db_qldb-examTips](./images/aws_solutionArchitectAssociate/db_qldb-examTips.png)    





======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - ANALYSING TIME-SERIES DATA WITH AMAZON TIMESTREAM** 
</span>


[Documentation aws: Amazon Timestream](https://docs.aws.amazon.com/timestream/)    


**Amazon Timestream** est une Base de données chronologique rapide, évolutive et serverless            

Vous pouvez facilement stocker et analyser les données de capteur pour les applications IoT, les métriques pour les cas d'utilisation DevOps et la télémétrie pour les scénarios de surveillance d'application tels que l'analyse de données de parcours de clics.


Amazon Timestream offre des performances de requête jusqu'à 1 000 fois plus rapides pour un dixième du coût des bases de données relationnelles.    
Il fournit une ingestion à haut débit, des requêtes instantanées rapides grâce à son stockage de mémoire et des requêtes analytiques rapides grâce à son stockage magnétique à coût optimisé    
  

Amazon Timestream vous permet de traiter des milliards d'événements et des millions de requêtes par jour. À mesure que les besoins de votre application évoluent, il s'adapte automatiquement pour ajuster la capacité    


Amazon Timestream est utilisé dans l'IoT, l'analyse, devops applications    


Amazon Timestream garantit que vos données de séries chronologiques sont toujours chiffrées, qu'elles soient au repos ou en transit    

Vous ne payez que pour les données que vous ingérez, stockez et interrogez


![db_timeseries-examTips](./images/aws_solutionArchitectAssociate/db_timeseries-examTips.png)    





======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - EXAM TIPS** 
</span>


![db_examTips01](./images/aws_solutionArchitectAssociate/db_examTips01.png)      
![db_examTips02](./images/aws_solutionArchitectAssociate/db_examTips02.png)      
![db_examTips03](./images/aws_solutionArchitectAssociate/db_examTips03.png)      
![db_examTips04](./images/aws_solutionArchitectAssociate/db_examTips04.png)      
![db_examTips05](./images/aws_solutionArchitectAssociate/db_examTips05.png)      
![db_examTips06](./images/aws_solutionArchitectAssociate/db_examTips06.png)      
![db_examTips07](./images/aws_solutionArchitectAssociate/db_examTips07.png)      
![db_examTips08](./images/aws_solutionArchitectAssociate/db_examTips08.png)      
![db_examTips09](./images/aws_solutionArchitectAssociate/db_examTips09.png)      
![db_examTips10](./images/aws_solutionArchitectAssociate/db_examTips10.png)      
![db_examTips11](./images/aws_solutionArchitectAssociate/db_examTips11.png)      
![db_examTips12](./images/aws_solutionArchitectAssociate/db_examTips12.png)      
![db_examTips13](./images/aws_solutionArchitectAssociate/db_examTips13.png)      
![db_examTips14](./images/aws_solutionArchitectAssociate/db_examTips14.png)      
![db_examTips15](./images/aws_solutionArchitectAssociate/db_examTips15.png)      
![db_examTips16](./images/aws_solutionArchitectAssociate/db_examTips16.png)      
![db_examTips17](./images/aws_solutionArchitectAssociate/db_examTips17.png)      
![db_examTips18](./images/aws_solutionArchitectAssociate/db_examTips18.png)      


















======================================================================================================================= 


<span style="color: #EA9811">

###  **DATABASE - LAB: SET UP A WORDPRESS SITE USING EC2 AND RDS** 
</span>



![db_lab-enonce](./images/aws_solutionArchitectAssociate/labs/db_lab/db_lab-enonce.png)    


aws account: cloud_user      
password: #0Snrhjjgqnkxik      
link: https://616224327405.signin.aws.amazon.com/console?region=us-east-1     


cloud server webserver01    
username: cloud_user     
password: SrJtA0]Y    
webserver01 public IP: 34.204.90.90    
link: https://ssh.instantterminal.acloud.guru/?_ga=2.230298590.397583483.1682672693-948713459.1682672693    


![db_lab-objectives](./images/aws_solutionArchitectAssociate/labs/db_lab/db_lab-objectives.png) 



**Solution**:    

1. Se logguer à la console et au serveur via le terminal (lien fourni)

**Create RDS Dtatabase**    

1. Creér une db RDS ➡️ aws console/service RDS/create Database / Standard create / MySQL / MySQL Engine version: 8.0.32 (celle proposée) / Templates ➡️ Free tier / DB instance identifier ➡️ wordpress / credentials settings: master username ➡️ wordpress / master password ➡️ wordpress / confirm password ➡️ wordpress / instance configuration ➡️ db.t2.micro / Storage (touche à rien) / Connectivity ➡️ don't onnect to an EC2 compute resource / IPV4 / choisir le vpc définit / Availibilty zone ➡️ us-east-1 / Monitoring (touche à rien) / Additional Configuration (ouvre sur la flèche) : initaila db name ➡️ wordpress / Backups ➡️ décoche le enable / create database  



2. Pendant la création de la db ➡️ aller sur le service EC2 dans la console AWS 

3. Dupliquer l'onglet et aller sur les instances running  ➡️ webserver-01 est présent   

4. se connecter à cette instance 


**Install Apache et ses dépendances**  

1. installer apache, php, php mysql et ses dépendances depuis le terminal   
`sudo apt install apache2 libapache2-mod-php php-mysql -y`

2. Aller dans le répertoire d'Apache    
```bash
cd /var/www
ls      # affiche le dossier html
``` 

3. Déplacer le contenu de `/wordpress`  (qui avait été installé pour le lab)  ici   
```bash
sudo mv /wordpress .
cd wordpress
```

4. Déplacer la config Apache dans `/etc/apache2/sites-enabled` pour activer le site wordpress depuis `/var/www/wordpress`     
```bash
sudo mv 000-default.conf /etc/apache2/sites-enabled/
```

5. Restart service apache    
`sudo apache2ctl restart`


6. Ouvrir le fichier de config php de wordpress   
```bash
vi wp-config.php
```
  ➡️ Ne PAS toucher au fichier pour le moment

7. Aller dans l'autre onglet et aller sur RDS, sélectionner la base wordpress

8. Dansl'onglet **Connectivity & security**, copier la valeur du endpoint 

9. Retour dans le terminal, changer la ligne `define('DB_HOST', 'localhost');` pour avoir `define('DB_HOST', '<INSERT ENDPOINT HERE>');`     


**Modification des security groups** : 

1. Dans l'onglet de la console EC2, aller dans l'onglet **Security** cliquer sur **Security Groups**   

2. Aller dans les **Inboud Rules** et ajouter une nouvelle regle depuis le menu **MYSQL/Aurora** et ajouter la regle  en **sélectionnant dans les sources le security group** précédemment choisit


**Complete Wordpress installation and test**: 

1. Retourner dans le terminal  

2. En bas de page, dans le bandeau copier l'ip publique et la mettre dans 1 nouvel onglet 

3. On the WordPress installation page, enter in the following information for each field:    
Site Title: "A Cloud Guru"    
Username: "guru"     
Password: Select a strong password to use here, and make sure to copy it in your clipboard for later. ➡️ TQLghwV!nIz0w1IlpW           
Your Email: "test@test.com"     


Click Install WordPress.      

Click Log in.

Enter "guru" for the Username or email and paste in the password that you copied earlier.

To view the website you just created, click A Cloud Guru in the top right corner of the page..

Click Visit Site to visit your newly created WordPress site.


[fichiers de logs](./archives/formation/cloud/aws_solutionArchitectAssociate/db_lab/db_lab.log)    







======================================================================================================================= 

<span style="color: #EA9811">

###  **DATABASE - QUIZZ** 
</span>



![db_quizz01](./images/aws_solutionArchitectAssociate/db_quizz01.png)    
![db_quizz02](./images/aws_solutionArchitectAssociate/db_quizz02.png)    
![db_quizz03](./images/aws_solutionArchitectAssociate/db_quizz03.png)    
![db_quizz04](./images/aws_solutionArchitectAssociate/db_quizz04.png)    
![db_quizz05](./images/aws_solutionArchitectAssociate/db_quizz05.png)    





======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **VPC: VIRTUAL PRIVATE CLOUD NETWORKING** 
</span>


[Documentation aws: VPC](https://docs.aws.amazon.com/vpc/index.html)    


Avec amazon **VPC (Virtual Private Cloud)** il est possible de lancer des ressources amazon dans un reseau logique virtuel isolé définit.    



Un **VPC** ressemble à un reseau classqiue dans lequel nous pouvons gérer notre propre data center, avec les benefices de scalabilité d'AWS.    
Un VPC s'étend dans toutes les Availability Zones d'une region.    
Après la création d'un VPC, nous pouvons ajouter 1 ou plusieurs subnet dans chaque AZ     
A la création d'un VPC on planifie les plages d'IPs ipv4 et ipv6 (**CIDR**)     
On peut avoir jusqu'à 5 CIDR par VPC pour les ipv4 ET les ipv6 (ceci est ajustable si besoin)    




Un **subnet** est une plage d'adresses IP dans le VPC.      
Un subnet appartient à 1 seule AZ.       
Après avoir créé les subnets, nous pouvons créer les instances    



L'**IP addressing** : nous pouvons assigner les IPv4 et IPv6 dans les VPCs et subnets.    


Le **routing** utilise les **route tables** pour déterminer où le trafic réseau depuis le subnet ou la gateway est dirigé    


Les **gateways et endpoints** : Une **gateway** connecte le VPC à un autre reseau.    
Par exemple, on utilise une **internet gateway** pour la connexion du VPC à internet     
On utilise un **VPC endpoint** pour connecter le VPC à des services AWS de manière privée, sans utiliser d'internet gateway ou de NAT device     


Les **peering connections** sont utilisés pour rediriger le trafic entre les ressources de 2 VPCs 


Le **traffic mirroring** copie le trafic reseau depuis les networks interfaces et les envoie aux appliances de sécurité et de surveillance pour une inspection approfondie des paquets      


Le **transit gateways**, qui est comme un hub central, re route le trafic entre vos VPCs, VPN connections et AWS Direct Connect connections      


**AWS Direct Connect** : Le service cloud AWS Direct Connect représente le chemin le plus court vers vos ressources AWS. Lors de son acheminement, votre trafic réseau reste sur le réseau mondial AWS et ne passe jamais sur l'Internet public.    

Le **VPC Flow Logs** est un flow de logs d'informations capturées depuis le trafic IP entre les interfaces reseaux de votre VPC       


Les **VPN connections** permettent de connecter vos VPCs à vos reseaux on-premise utilisant AWS Virtual Private Network    


======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Overview** 
</span>


**What is a VPC ?**      
Penser à un data center virtuel dans le cloud lorsqu'on pense VPC    


C'est une partie d'AWS Cloud isolée logiquement où on peut définir notre propre reseau    

Nous y avons un controle complet des reseaux virtuels incluant les range d'IPs, sous-reseaux, table de routage, et network gateways     

Un VPC est un network completement customisable. On peut tirer parti d'une multiple couches de sécurité: security group, network ACL (access control list) pour aider les accès amazon EC2 instance dans chaque subnet    


![vpc_customNetwork](./images/aws_solutionArchitectAssociate/vpc_customNetwork.png)     



Il est possible de créer son propre VPN (hardware Virtual Private Network) pour communiquer avec nos propres data center et nos propres VPCs    

> _Remarque_: Le site https://cidr.xyz/ permet de calculer les plages d'adresses IPs facilement pour la création des VPCs     
> ➡️ ex:  10.0.0.0/16 permet d'avoir 65536 IPs dans ce réseau    (max IPs qu'on puisse avoir)
> ➡️      10.0.0.0/28 permet d'avoir 16 IPs dans ce reseau       (min IPs qu'on puisse avoir)



**VPC Diagram**      
![vpc_diagram](./images/aws_solutionArchitectAssociate/vpc_diagram.png)   


**Que pouvons nous faire avec un VPC ?**     
![vpc_whatCanDoWithIt](./images/aws_solutionArchitectAssociate/vpc_whatCanDoWithIt.png)   



**Quelle difference entre le VPC par defaut et un VPC personnalisé ?**      
![vpc_difference](./images/aws_solutionArchitectAssociate/vpc_difference.png)  





![vpc_overview_tips](./images/aws_solutionArchitectAssociate/vpc_overview_tips.png)  




======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Provisionning a VPC** 
</span>


[Documentation aws: VPC](https://aws.amazon.com/documentation/vpc/?&trk=ta_a134p000003yjzcAAA&trkCampaign=pac-edm-2020-vpc-documentation&sc_channel=ta&sc_campaign=pac-edm-2020-vpc-console-ad&sc_outcome=Product_Adoption_Campaigns)        
[Documentation aws: les ressources d'un VPC](https://aws.amazon.com/vpc/?&trk=ta_a134p000003yjzhAAA&trkCampaign=pac-edm-2020-vpc-pdp&sc_channel=ta&sc_campaign=pac-edm-2020-vpc_pdp-console-ad&sc_outcome=Product_Adoption_Campaigns)     
[Documentation aws: VPC subnets](https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html)     


Il existe pour 1 VPC 1 subnet par Availability zone    


La création d'un VPC "simple" va exécuter les création de 3 choses:                       
- security group     
- route table      
- network ACL   



En cliquant sur la route table générée, on peut voir qu'aucun subnet n'a été généré (dans l'onglet _subnets association_ de la route table)  ▶️ il faut en créer 1 (par defaut il en existe le nombre d'avalibity zone qui existe dans la region)  
Un subnet peut avoir 2 caractéristiques:     
- accès internet: public         
- accès interne (MAIS pas internet): private    


**Un subnet est lié à un VPC**           
Par defaut **5 IPs sont réservées à la création d'un subnet**:    
- `10.0.0.0` : network address
- `10.0.0.1` : VPC router (réservé par AWS)
- `10.0.0.2` : DNS (réservé par AWS)
- `10.0.0.3` : en cas d'utilisation future (réservé par AWS)
- `10.0.0.255` : network broadcast address

![vpc_demo_tips01](./images/aws_solutionArchitectAssociate/vpc_demo_tips01.png)  




Lors de la création d'un subnet (exemple: cas de creation de 2 subnets: 1 privé / 1 public).
Pour rendre les IPs publiques sur le subnet public il faut sélectionner ce subnet dans la liste, éditer ce subnet, enable les IPs publiques    


Maintenant il faut rendre ce subnet accessible depuis internet ➡️ on utilise une **internet gateway**    


A la création d'une internet gateway par défaut elle est dans l'état **detached**.     
Il faut donc l'attachée au VPC souhaité.     

⚠️    
Une seule internet gateway par VPC est possible     
⚠️    

Maintenant que la gateway est créée il nous faut une **route table** pour les sorties vers internet    

Par défaut la **route table** de notre VPC a été créée lors de la création du VPC    
En la sélectionnant nous voyons ces caractéristiques (subnet association) qu'il y a 2 parties:    
- explicit subnet associations            ➡️ subnet associé à cette route table
- subnets without explicit associations   ➡️ subnet NON associé à cette route table 

Par defaut tous les subnets seront associées à cette **main route table** qui a été créée lors de la création du VPC.    
Or nous souhaitons pouvoir filtrer  ➡️ il faut créer une nouvelle route table    


Ensuite nous éditons cette nouvelle route table en choisissant l'internet gateway que nous avons créé qui va nous permettre l'accès de nos publiques depuis l'extérieur     

Il est maintenant possible d'associé notre subnet à notre route table en editant la liste des **explicit subnet associations**  ➡️ on ajoute le subnet public pour avoir les sorties vers internet    



Nous allons maintenant créer 2 instances EC2 que nous allons associée à ce VPC (1 publique et 1 privée)    

Lors de cette création nous créons aussi un security group (on donne les accès http et https)  
Idem pour la seconde sans les accès HTTP et HTTPS    

Les 2 instances ont les mêmes caractéristiques SAUF son subnet                  
➡️ publique: tout accès vers internet (et update) possible                
➡️ privée: les accès vers internet sont bloqué cas dans le subnet il n'y a pas de regles de sorties vers internet (update impossible)



======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Using NAT Gateways for Internet Access** 
</span>


[Documentation aws: NAT Gateways](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html)    


Nous avons maintenant 2 instances sur le même VPC avec 2 subnets differents (1 privé qui n'autorise pas les connexions internet et 1 publique qui les autorise) avec 1 security group, 1 route table, 1 internet gateway    
Or l'instance qui est "privée" ne peut pas faire ses updates systemes puisqu'il n'y a pas de sortie vers internet autorisée     
Le but va être de pouvoir faire ces mises à jour sans casser la securité     





**NAT Gateway**: nous pouvons utiliser un **Network Adress Translation (NAT) Gateway** pour permettre les accès d'instances dans un private subnet vers internet ou d'autres services AWS tout en sécurisant les connexions de ces instances    


Une instance d'un public subnet va passer par le Netwwork ACL puis la route table (qui a une sortie vers internet de définit), puis le router, puis l'Internet gateway pour aller vers Internet   

![vpv_natgateway_publicSubentRouteToInternet](./images/aws_solutionArchitectAssociate/vpv_natgateway_publicSubentRouteToInternet.png)    



Une instance d'un private subnet va passer dans le Network ACL et va être bloqué par la route table (pas de route out vers internet de définit)    

![vpv_natgateway_privateSubentRouteToInternet](./images/aws_solutionArchitectAssociate/vpv_natgateway_privateSubentRouteToInternet.png)    



La solution est de créer un NAT Gateway dans le public subnet.

⚠️ Un NAT Gateway ne peut pas agir à travers plusieurs Avalibility Zones ➡️ il faut donc le créer dans la même AZ que le private subnet pour lequel nous voulons sortir vers internet ⚠️    

![vpv_natgateway_privateSubentRouteToInternetSolution](./images/aws_solutionArchitectAssociate/vpv_natgateway_privateSubentRouteToInternetSolution.png) 


**5 faits importants à se rappeler sur les NAT Gateway**:    
- Ils sont redondant dans une même Availability Zone    
- Ils commencent à 5Gbps et vont jusqu'à 45 Gbps     
- Pas besoin de patch    
- Ils ne sont pas associés à un security group    
- Ils sont associés automatiquement à une adresse publique    


Pour mettre en place une NAT Gateway, aller dans le service VPC/Nat Gateway/Create a NAT Gateway/Mettre son nom/Définir un subnet publique de la même AZ que celle du private subnet que l'on souhaite/Noté connectivity public (nous souhaitons nous connecter à internet)/Définir un ID d'elastic IP    

Cela prend environ 10 minutes de créer une NAT GAteway dans AWS    

Une fois créé cette NAT Gateway est définit par:
- nom (que nous avons définit lors de la création)    
- ID
- Son état (available) 
- Elastic IP Address (adress publique)   
- Private IP addess (lié au subnet définit lors de la création)   
- ID Network Interface 
- VPC  
- subnet (celui définit lors de la création)    


Il faut maintenant aller dans l'onglet **Route Table** pour créer une route qui va aller vers internet 
Si une table pour une sortie vers internet existe déjà, éditer celle-ci pour ajouter une route qui passe vers la NAT GAteway créée    





======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Protecting your resources with security groups** 
</span>


Les serveurs communiquent à travers différents protocoles et ces protocoles utilisent les ports:     
- Linux       ssh           ➡️ **port 22**     
- Windows     rdp           ➡️ **port 3389**    
- HTTP        web           ➡️ **port 80**    
- HTTPS       web secure    ➡️ **port 443**    


![vpc_networkDiagram](./images/aws_solutionArchitectAssociate/vpc_networkDiagram.png)    

Les **security groups** sont des firewalls virtuels pour les instances EC2. Par défaut TOUT EST BLOQUE    


![vpc_securityGroup_tips](./images/aws_solutionArchitectAssociate/vpc_securityGroup_tips.png)    




======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Controlling subnet traffic with Network ACLs** 
</span>


[Documentation aws: ephemeral ports](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#nacl-ephemeral-ports)    
[what are ephemeral ports](https://en.wikipedia.org/wiki/Ephemeral_port)    


.....................................................................................................      

<span style="color: #11E3EA">

####  **VPC - Network ACL** 
</span>

**C'est la 1ere ligne de défense**.    

Un **Network Access Control List (ACL)** est une couche optionnelle de sécurité pour votre VPC qui agit comme un firewall pour controler le trafic entrant et sortant d'un ou plusieurs subnets    

Il FAUT configurer le network ACL avec des règles similaires à votre security group dans le but d'ajouter une nouvelle couche de sécurité à votre VPC    



.....................................................................................................      

<span style="color: #11E3EA">

####  **VPC - Network diagram** 
</span>



![vpc_networkAclDiagram](./images/aws_solutionArchitectAssociate/vpc_networkAclDiagram.png)    



.....................................................................................................      

<span style="color: #11E3EA">

####  **VPC - Network ACL overview** 
</span>


- **default Network ACLs**: Par defaut à la création d'un VPC, un network ACL est créé par defaut avec toutes les autorisations entrantes et sortantes    

- **Custom Network ACLs**: Il est possible de créer de nouveaux network ACLs personalisés. Par defaut à leur création tout est fermé ➡️ il faut donc ouvrir les regles souhaitées 


- **subnet associations**: chaque subnet dans votre VPC DOIT ETRE associé à un network ACL. Si cela n'est pas fait, le subnet sera associé par defaut au default network ACL    


- **Block IP Addresses**: ⚠️ Permet de bloquer des IPs en utilisant les network ACLs et pas les security groups ⚠️  




.....................................................................................................      

<span style="color: #11E3EA">

####  **VPC - Network ACL Tips** 
</span>



![vpc_networkAcl-tips](./images/aws_solutionArchitectAssociate/vpc_networkAcl-tips.png)





.....................................................................................................      

<span style="color: #11E3EA">

####  **VPC - Network ACL exam Tips** 
</span>



![vpc_networkAcl_examTips01](./images/aws_solutionArchitectAssociate/vpc_networkAcl_examTips01.png)    
![vpc_networkAcl_examTips02](./images/aws_solutionArchitectAssociate/vpc_networkAcl_examTips02.png)    





.....................................................................................................     


======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Private communication using VPC endpoints** 
</span>


[Documentation aws: VPC endpoints](https://docs.aws.amazon.com/whitepapers/latest/aws-privatelink/what-are-vpc-endpoints.html)    



Un **VPC endpoint** c'est quoi ?   

Un **VPC endpoint** permet aux clients de se connecter en privé aux services AWS pris en charge et aux services VPC endpoint alimentés par AWS PrivateLink sans à avoir recours à une internet gateway, NAT device, VPN ou AWS Direct Connect   

**Les instances Amazon VPC ne nécessitent pas d'adresses IP publiques pour communiquer avec les ressources du service. Le trafic entre un Amazon VPC et un service ne quitte pas le réseau Amazon.**


![vpc_endpoint-tips](./images/aws_solutionArchitectAssociate/vpc_endpoint-tips.png)        


![vpc_endpoint_types](./images/aws_solutionArchitectAssociate/vpc_endpoint_types.png)    


CONNEXION A UN S3 BUCKET SANS VPC ENDPOINT:      
![vpc_networkDiagramWithoutEndpointForS3connection](./images/aws_solutionArchitectAssociate/vpc_networkDiagramWithoutEndpointForS3connection.png)   

CONNEXION A UN S3 BUCKET AVEC VPC ENDPOINT:       
![vpc_networkDiagramWithEndpointForS3connection](./images/aws_solutionArchitectAssociate/vpc_networkDiagramWithEndpointForS3connection.png)  




![vpc_endpoint-examTips](./images/aws_solutionArchitectAssociate/vpc_endpoint-examTips.png)





======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Building solutions across VPCs with peering** 
</span>


[Documentation aws: update your route table](https://docs.aws.amazon.com/vpc/latest/peering/vpc-peering-routing.html)    


**Multiple VPCs**:    
Il arrive d'avoir besoin d'avoir plusieurs VPCs pour différents environnements et que ces VPCs communiquent entre eux.

_exemple_: 1 VPC pour la production + 1 VPC pour nos contenus + 1 VPC pour l'intranet  

Et ces VPCs sont amenés à communiquer entre eux  ➡️ il s'agit du **vpc peering**   

Pour que cela fonctionne il faut:           
- être autorisé à connecter 1 VPC avec 1 autre via un direct network route qu iutilise des IP privées    
- les instances doivent se comporter comme si elles étaient dans le même private network    
- vous pouvez appairer des VPC avec des comptes AWS ainsi qu'avec d'autres VPC dans le même compte    
- **peering** (appairage) est une configuration en étoile ➡️ 1 VPC central avec 4 autres
- On peut appairer entre les regions    


![vpc_transitivePeeringStarConfiguration](./images/aws_solutionArchitectAssociate/vpc_transitivePeeringStarConfiguration.png)    

![vpc_transitivePeeringStarConfigurationNewPeering](./images/aws_solutionArchitectAssociate/vpc_transitivePeeringStarConfigurationNewPeering.png)   


>_Remarque_: les CIDR DOIVENT être différents pour le peering  ➡️ le overlapping ne se fait pas    
>_exemple_: 
> - 2 VPCs (VPC1 ➡️ CIDR: 10.0.0.0/16  et VPC2 ➡️ CIDR: 172.3.4.0/16)  ➡️ peering OK
> - 2 VPCs (VPC1 ➡️ CIDR: 10.0.1.0/16  et VPC2 ➡️ CIDR: 10.0.1.0/16)  ➡️ peering KO  ➡️ pas d'overlapping




![vpc_peering-examTips](./images/aws_solutionArchitectAssociate/vpc_peering-examTips.png)    





======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Network privacy with AWS PrivateLink** 
</span>



Il existe 2 moyens de faire communiquer des VPCs entre eux:    
- via internet        ➡️ vérifier la securité; tout ce qui est dans le public subnet sera publique; beaucoup d'admin du système à faire    
- grace au peering    ➡️   créer et manager différentes relations peering; l'ensemble du reseau sera accessible ➡️ mauvaise solution dans le cas de multiple applications   


Le meilleur moyen de "partager" de nombreux VPCs à de nombreux utilisateurs et d'utiliser **PrivateLink**  

![vpc_privateLink](./images/aws_solutionArchitectAssociate/vpc_privateLink.png)    

 
![vpc_privateLink-examTips](./images/aws_solutionArchitectAssociate/vpc_privateLink-examTips.png)    







======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Securig your network with VPN CloudHub** 
</span>



**AWS VPN CloudHub** est utile dans le cas où l'on a une multitudes de sites, chacun avec sa propre connexion VPN    
  ➡️ dans ce cas utiliser AWS VPN CloudHub pour connecter tous ces sites entre eux est utile  


Il utilse un **hub-and-spoke model** (modèle en étoile)    
N'est pas cher et facile à gérer    
Il fonctionne dans l'internet publique MAIS tout le trafic entre l'utilisateur gateway et AWS VPN CloudHub est encrypté    


![vpc_vpnClouHub-diagram](./images/aws_solutionArchitectAssociate/vpc_vpnClouHub-diagram.png)    

![vpc_vpnClouHub-examTips](./images/aws_solutionArchitectAssociate/vpc_vpnClouHub-examTips.png)    





======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Connecting On-Premises with DirectConnect** 
</span>



**Direct Connect c'est quoi ?**    

**AWS Direct Connect** est une solution de service cloud qui permet d'établir facilement une connection dédiée depuis notre on-premises vers AWS    


Il est possible d'avoir une connexion privée entre AWS et notre data center     
Cela permet souvent de réduire considérablement les couts de notre reseau, augmenter le débit de bande passante et offrir une expérience réseau plus cohérente que les connexions basées sur Internet   


Il existe 2 types de Direct Connect Connexion:    
- **dedicated connection**: connexion ethernet physique associée à un client unique. Les clients peuvent effectuer des requêtes à travers la console aws direct connect, la cli ou les APis     
- **hosted connection**: connexion Ethernet physique qu'un partenaire aws direct connect fournit au nom d'un client. Les clients peuvent faire des requêtes en contactant le partenaire qui fourni la connexion dans aws direct connect partner program   


![vpc_directConnect-diagram](./images/aws_solutionArchitectAssociate/vpc_directConnect-diagram.png)    


**Différences entre VPN et DirectConnect ?**    
- Le VPN autorise les communication privée mais dépend du flux du reseau internet publique (peut-être lent en fonction des datas à faire transiter)    
- Direct Connect est rapide (lien direct), securisé, fiable et à son propre flux

>_Remarque_: Il est possible d'ajouter un VPN à une connexion direct connect


![vpc_directConnect-examTips](./images/aws_solutionArchitectAssociate/vpc_directConnect-examTips.png)    






======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Simplifying networks with Transit Gateway** 
</span>




Une architecture aws peut vite devenir compliquée si on a plusieurs VPCs (il faut monter des peering entre eux pour qu'ils communiquent), + si on a du on-premise monter du directconnect (voir en plus du VPN) ....

Dans ce cas **transit gateway** est là pour simplifier.    

AWS Transit gateway connecte les VPCs et les reseaux on-premises à travers un hub central.    
Cela simplifit le reseau et permet de ne pas à avoir à ajouter des relations complex de peering.    
Il agit comme un router cloud, chaque nouvelle connexion n'est faite qu'une seule fois    

![vpc_transitGateway-diagram](./images/aws_solutionArchitectAssociate/vpc_transitGateway-diagram.png)    

![vpc_transitGateway-facts](./images/aws_solutionArchitectAssociate/vpc_transitGateway-facts.png)


![vpc_transitGateway-examTips](./images/aws_solutionArchitectAssociate/vpc_transitGateway-examTips.png)






======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - 5G networking with AWS Wavelenght** 
</span>



**Les reseaux 5G**:     
Ces réeaux permettent aux mobiles d'avoir une plus grande rapidité et capacité que le réseau 4G    

Un nouveau service AWS a été conçu pour embarquer les services compute et storage dans le reseau 5G.    
Ce service est **AWS Wavelength**    


![vpc_Wavelength-examTips](./images/aws_solutionArchitectAssociate/vpc_Wavelength-examTips.png)   





======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - VPC Flow Logs** 
</span>


Les flow logs peuvent vous aider pour de nombreuses tâches, par exemple :          

 - Diagnostiquer les règles de groupe de sécurité trop restrictives          
 - Surveiller le trafic qui accède à votre instance         
 - Déterminer la direction du trafic vers et depuis les interfaces réseau   

Les données du flow logs sont collectées en dehors du chemin d'accès de votre trafic réseau et n'affectent donc pas le débit réseau ou la latence. Vous pouvez créer ou supprimer des flow logs sans risque d'impact sur les performances du réseau. 


Un flow log capture tout le trafic d'un sous-réseau et publie les enregistrements du flow log sur Amazon CloudWatch Logs, S3, Kinesis firehose. Le flow log capture le trafic pour toutes les interfaces réseau du sous-réseau



======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - Exam Tips** 
</span>





![vpc-examTips01](./images/aws_solutionArchitectAssociate/vpc-examTips01.png)        
![vpc-examTips02](./images/aws_solutionArchitectAssociate/vpc-examTips02.png)        
![vpc-examTips03](./images/aws_solutionArchitectAssociate/vpc-examTips03.png)        
![vpc-examTips04](./images/aws_solutionArchitectAssociate/vpc-examTips04.png)        
![vpc-examTips05](./images/aws_solutionArchitectAssociate/vpc-examTips05.png)        
![vpc-examTips06](./images/aws_solutionArchitectAssociate/vpc-examTips06.png)        
![vpc-examTips07](./images/aws_solutionArchitectAssociate/vpc-examTips07.png)        
![vpc-examTips08](./images/aws_solutionArchitectAssociate/vpc-examTips08.png)        
![vpc-examTips09](./images/aws_solutionArchitectAssociate/vpc-examTips09.png)        
![vpc-examTips10](./images/aws_solutionArchitectAssociate/vpc-examTips10.png)        
![vpc-examTips11](./images/aws_solutionArchitectAssociate/vpc-examTips11.png)        
![vpc-examTips12](./images/aws_solutionArchitectAssociate/vpc-examTips12.png)        
![vpc-examTips13](./images/aws_solutionArchitectAssociate/vpc-examTips13.png)        
![vpc-examTips14](./images/aws_solutionArchitectAssociate/vpc-examTips14.png)        






======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - LAB - Build solution accross VPCs with peering** 
</span>



![vpc_lab-enonce](./images/aws_solutionArchitectAssociate/labs/vpc_lab/vpc_lab-enonce.png)    


![vpc_lab-objectif](./images/aws_solutionArchitectAssociate/labs/vpc_lab/vpc_lab-objectif.png)    


username: cloud_user          
password: &4Yklzzfgpbmkgn         
link: https://440527276625.signin.aws.amazon.com/console?region=us-east-1         


**Solution**  

Log in to the AWS Management Console using the credentials provided on the lab instructions page. Make sure you're in the N. Virginia (us-east-1) Region throughout the lab.


**Create Web_VPC Subnets and Attach a New Internet Gateway**  

**Create a VPC**  

1. Use the top search bar to look for and navigate to VPC.

2. Under Resources by Region, click VPCs.

3. Use the top search bar to look for and navigate to RDS in a new tab.

4. Click DB Instances, and observe the instance created for this lab.   
> Note: Keep this tab open for use later on in the lab.

5. Go back to your VPC tab, and click Create VPC.

6. Ensure the VPC only option is selected.

7. Set the following values:

  - Name tag: Enter Web_VPC.
  - IPv4 CIDR block: Enter 192.168.0.0/16.

8. Leave the rest of the settings as their defaults, and click Create VPC.


**Create a Subnet**

1. On the left menu under VIRTUAL PRIVATE CLOUD, select Subnets.

2. Click Create subnet.

3. For VPC ID, select the newly created Web_VPC.

4. Under Subnet settings, set the following values:

  - Subnet name: Enter WebPublic.
  - Availability Zone: Select us-east-1a.
  - IPv4 CIDR block: Enter 192.168.0.0/24.

5. Click Create subnet.


**Create an Internet Gateway**

1. On the left menu, select Internet Gateways.

2. Click Create internet gateway.

3. For Name tag, enter WebIG.

4. Click Create internet gateway.

5. In the green notification at the top of the page, click Attach to a VPC.

6. In Available VPCs, select the Web_VPC and click Attach internet gateway.

7. On the left menu, select Route Tables.

8. Select the checkbox for the Web_VPC.

9. Underneath, select the Routes tab and click Edit routes.

10. Click Add route.

11. Set the following values:

  - Destination: Enter 0.0.0.0/0.
  - Target: Select Internet Gateway, and select the internet gateway that appears in the list.

12. Click Save changes.


**Create a Peering Connection**

1. On the left menu, select Peering Connections.

2. Click Create peering connection.
3. Set the following values:
  - Name: Enter DBtoWeb.
  - VPC (Requester): Select the DB_VPC.
  - VPC (Accepter): Select the Web_VPC.

4. Click Create peering connection.

5. At the top of the page, click Actions > Accept request.

6. Click Accept request.

7. On the left menu, select Route Tables.

8. Select the checkbox for the Web_VPC.

9. Underneath, select the Routes tab, and click Edit routes.

10. Click Add route.

11. Set the following values:

  - Destination: Enter 10.0.0.0/16.
  - Target: Select Peering Connection, and select the peering connection that appears in the list.

12. Click Save changes.

13. Go back to Route Tables, and select the checkbox for the DB_VPC instance with a Main column value of Yes.

14. Underneath, select the Routes tab, and click Edit routes.

15. Click Add route.

16. Set the following values:

  - Destination: Enter 192.168.0.0/16.
  - Target: Select Peering Connection, and select the peering connection that appears in the list.

17. Click Save changes.


**Create an EC2 Instance and Configure WordPress**

1. In a new browser tab, navigate to EC2.

2. Click Launch instance > Launch instance.

3. Scroll down and under Quick Start, select the Ubuntu image box. (You can skip the Name field before this.)

4. Under Amazon Machine Image (AMI), click the dropdown and select Ubuntu Server 20.04 LTS.

5. Under Instance type, click the dropdown and select t3.micro.

6. For Key pair, click the dropdown and select Proceed without a key pair.

7. In the Network settings section, click the Edit button.

8. Set the following values:

  - VPC: Select the Web_VPC.
  - Subnet: Ensure the WebPublic subnet is selected.
  - Auto-assign public IP: Select Enable.

9. Under Firewall (security groups), ensure Create security group is selected (the default value).

10. Scroll down and click Add security group rule.

11. Set the following values for the new rule (i.e., Security group rule 2):

  - Type: Select HTTP.
  - Source: Select 0.0.0.0/0.

12. Scroll to the bottom, and expand Advanced details.

13. At the bottom, under User data, copy and paste the following bootstrap script:

```bash
#!/bin/bash
sudo apt update -y
sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip perl mysql-server apache2 libapache2-mod-php php-mysql -y
wget https://github.com/ACloudGuru-Resources/course-aws-certified-solutions-architect-associate/raw/main/lab/5/wordpress.tar.gz
tar zxvf wordpress.tar.gz
cd wordpress
wget https://raw.githubusercontent.com/ACloudGuru-Resources/course-aws-certified-solutions-architect-associate/main/lab/5/000-default.conf
cp wp-config-sample.php wp-config.php
perl -pi -e "s/database_name_here/wordpress/g" wp-config.php
perl -pi -e "s/username_here/wordpress/g" wp-config.php
perl -pi -e "s/password_here/wordpress/g" wp-config.php
perl -i -pe'
  BEGIN {
    @chars = ("a" .. "z", "A" .. "Z", 0 .. 9);
    push @chars, split //, "!@#$%^&*()-_ []{}<>~\`+=,.;:/?|";
    sub salt { join "", map $chars[ rand @chars ], 1 .. 64 }
  }
  s/put your unique phrase here/salt()/ge
' wp-config.php
mkdir wp-content/uploads
chmod 775 wp-content/uploads
mv 000-default.conf /etc/apache2/sites-enabled/
mv /wordpress /var/www/
apache2ctl restart
```

14. At the bottom, click Launch Instance.

> Note: It may take a few minutes for the new instance to launch.

15. From the green box that appears after the instance launches, open the link for the instance in a new browser tab.

16. Observe the Instance state column, and check to ensure it is Running before you proceed.

17. Select the checkbox for the new instance and click Connect.

18. Click Connect.

> Note: The startup script for the instance may take a few minutes to complete and you may need to wait for it to complete before proceeding with the next step.

19. To confirm WordPress installed correctly, view the configuration files:

```bash
cd /var/www/wordpress
ls
```

20. To configure WordPress, open wp-config.php:

`sudo vim wp-config.php`

21. Go back to your browser tab with RDS.

22. Click the link to open the provisioned RDS instance.

23. Under Connectivity & security, copy the RDS Endpoint.

24. Go back to the tab with the terminal, and scroll down to /** MySQL hostname */.

25. Press i to enter Insert mode.

26. Replace localhost with the RDS endpoint you just copied. Ensure it remains wrapped in single quotes.

27. Press ESC followed by :wq, and press Enter. Leave this tab open.


**Modify the RDS Security Groups to Allow Connections from the Web_VPC VPC**

1. Go back to your RDS browser tab.

2. In Connectivity & security, click the active link under VPC security groups.

3. At the bottom, select the Inbound rules tab.

4. Click Edit inbound rules.

5. Click Add rule.

6. Under Type, search for and select MYSQL/Aurora.

7. Under Source, search for and select 192.168.0.0/16.

8. Click Save rules.

9. Return to the terminal page.

10. Below the terminal window, copy the public IP address of your server.

11. Open a new browser tab and paste the public IP address in the address bar. You should now see the WordPress installation page.

12. Set the the following values:

  - Site Title: Enter A Blog Guru.
  - Username: Enter guru.
  - Your Email: Enter test@test.com.

13. Click Install WordPress.

14. Reload the public IP address in the address bar to view your newly created WordPress blog.



**Troubleshooting**

If the website isn't loading the way you expected at the end of this lab, here are some tips to help troubleshoot:

- Check the status of the lab objectives - are any not yet completed?
- Is everything you set up ready to use? Check things like the VPC peering connection, which requires you to specifically accept the connection request.
- Does the database error page load after a minute or so of waiting, or does no page load at all? This gives a hint on whether the issue may be with the peering or the security groups.












======================================================================================================================= 


<span style="color: #EA9811">

###  **VPC - QUIZZ** 
</span>


![vpc_quizz01](./images/aws_solutionArchitectAssociate/vpc_quizz01.png)     
![vpc_quizz02](./images/aws_solutionArchitectAssociate/vpc_quizz02.png)     
![vpc_quizz03](./images/aws_solutionArchitectAssociate/vpc_quizz03.png)     
![vpc_quizz04](./images/aws_solutionArchitectAssociate/vpc_quizz04.png)     
![vpc_quizz05](./images/aws_solutionArchitectAssociate/vpc_quizz05.png)     



**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/50png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/51.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/52.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/53.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/54.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/55.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/56.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/57.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/58.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/69.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/61.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/62.png)         







======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **ROUTE 53** 
</span>

[Documentation aws: Route 53](https://docs.aws.amazon.com/route53/index.html)    


**Amazon Route 53 est un service Web DNS (Domain Name System) hautement disponible et évolutif.**    



======================================================================================================================= 


<span style="color: #EA9811">

###  **ROUTE 53 - OVERVIEW** 
</span>


[Documentation aws: Working with private hosted zones](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-private.html)    
[Documentation aws: Working with public hosted zones](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/AboutHZWorkingWith.html)    
[Documentation aws: Amazon Route 53 FAQs](https://aws.amazon.com/route53/faqs/)    
[Documentation aws: route 53 quotas](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/DNSLimitations.html)    
[Documentaion: List of Internet top-level domains](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains)    
[Documentation: Second-level domain](https://en.wikipedia.org/wiki/Second-level_domain)    




Les IPv4 ont 32-bit  ➡️ 4 294 967 296 adresses possibles    
Les IPV6 ont 128-bit ➡️ 340 undecillion (340 milliards de milliards de milliards) d'adresses disponibles


**Top level domain**:    

![route53_topLevelDomain-examTips](./images/aws_solutionArchitectAssociate/route53_topLevelDomain-examTips.png)    

Les 4 top level domaines names sont: 
- **.gov**    
- **.gov.uk**    
- **.com.au**   
- **.edu**   


Ces top level domain names sont controllés par l'**IANA** (**Internet Assignated Numbers Authority**) dans 1 root zone database qui est une db de tous les top-level domains disponibles: https://www.iana.org/domains/root/db 

Chaque nom de domaine doit être unique voilà pourquoi ils sont enregistré ici   

Un **registrar** est une authorité qui permet d'assigner un domain name directement sous un top-level domain    

Ces domaines sont enregistrer avec InterNIC, un service de ICANN qui va vérifier que chaque nom est unqieu à travers tout internet    


Les 5 registrar populaires:    
- domain.com   
- GoDaddy
- Hover
- AWS    
- Namecheap



.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DNS c'est quoi ?** 
</span>



Un enregistrement DNS SOA (Start Of Authority) stocke les informations importantes sur un domaine ou une zone telle que le transfert de zone, l'administrateur de la zone, la version des fichiers de données, le nombre de secondes du TTL, le nom du serveur qui fournit les datas pour la zone        
Un SOA est une section importante de la définition de la zone dans le systeme DNS  


**Qu'est-ce qu'un TTL ?**:       
**TTL**: **Time To Live**    
C'est le temps qu'un enregistrement restera en cache sur un pc (en secondes)    
Plus il est petit, plus rapide sera le changement d'enregistrement DNS pour la propagation à travers internet  


**Les enregistrements DNS: enregistrement A**: 
Un **A record** est l'enregristement fondamental du DNS ➡️ A pour Adress    
Le **A record** est utilisé par le pc pour traduire le nom de domaine en IP    



**Qu'est-ce qu'un CNAME ?**:       
Un **CNAME** (canocical name) peut être utilisé pour résoudre un domaine pour un autre    


**Qu'est-ce qu'un Alias ?**:       
Les **Alias** sont utilisés pour mapper les enregistrement de ressources configurés dans les hosted zones    
Ils fonctionnent comme les CNAME dans le sens qu'on peut mapper un nom DNS à une autre cible nom DNS    


![route53_dns-aliasRecord](./images/aws_solutionArchitectAssociate/route53_dns-aliasRecord.png)    



.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - Route53 c'est quoi ?** 
</span>


**Route53** est le service DNS Amazon    
Il permet d'enregistrer le domain names, créer les hosted zones, manager et créer les enregistrements DNS   
Il s'appelle route53 parce qu'il utilise le **port 53**     


Route53 possède 7 route policies: 
![route53-les7RoutesPolicies](./images/aws_solutionArchitectAssociate/route53-les7RoutesPolicies.png)        

Chaque policy va être vue dans la partie DEMO   


![route53-overview-examTips01](./images/aws_solutionArchitectAssociate/route53-overview-examTips01.png)     
![route53-overview-examTips02](./images/aws_solutionArchitectAssociate/route53-overview-examTips02.png)     





.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - Private Hosted Zones** 
</span>



Une **private hosted zone** (zone hébergée privée) est un conteneur qui contient des informations sur la façon dont vous souhaitez qu'Amazon Route 53 réponde aux requêtes DNS pour un domaine et ses sous-domaines dans un ou plusieurs VPC que vous créez avec le service Amazon VPC.


**Fonctionnement d'une private hosted zone**:    

- creation d'une private zone

- enregistrement des hosted zones qui vont déterminer comment route 53 va répondre aux requêtes du DNS pour les domaines et sous-domaines avec les VPCs    

- lors d'une requête DNS il doit être retourné l'IP correspondante. Pour avoir une réponse depuis une private hosted zone, il faut aussi avoir une instance EC2 en marche associé avec les VPCs associés. Toutes requêtes d'une private hosted zone en dehors des VPCs sera résolu via internet    

- l'application qui utilise l'IP address fournit par Route 53 étabblit une connexion avec le server db   




.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - Public Hosted Zones** 
</span>


Une **public hosted zone** (zone hébergée publique) est un conteneur qui contient des informations sur la manière dont vous souhaitez acheminer le trafic sur Internet pour un domaine spécifique, tel que example.com, et ses sous-domaines (acme.example.com, zenith.example.com)


- lorsque nous enregistrons un domaine avec Route 53, nous créons une hosted zone automatiquement (aws le fait)    

- lors d'un transfer de zone de DNS avec Route 53, nous créons une hosted zone pour le domaine    



.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - Quotas** 
</span>



Les requêtes et les entités de l'API Amazon Route 53 sont soumises à des quotas: [Documentation aws: service quotas](https://docs.aws.amazon.com/servicequotas/latest/userguide/intro.html)    




.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - Health checks** 
</span>


**Amazon Route 53 health checks** surveillent l'état et les performances de vos applications Web, serveurs Web et autres ressources

On peut les créer et les monitorer comme ceux qui suit:     
- health d'une ressource spécifique (ex: serveur web)   
- les status des health checks    
- le status de Amazon CloudWatch alarm   
- .....

Il est possible de configurer les health checks sur l'état des ensembles d'enregistrements (record sets) individuels    
Si un record set échoue un health check il sera sera supprimé de Route53 jusqu'à ce qu'il réussisse le health check    
On peut envoyer des SNS notification (service amazon de SMS) pour alerter à sujet de health checks failed



Pour mettre en place un healthcheck il faut aller dans la console aws/route53/configure health check / 


![route53-setHealthCheck](./images/aws_solutionArchitectAssociate/route53-setHealthCheck.png)    




.....................................................................................................   

======================================================================================================================= 


<span style="color: #EA9811">

###  **ROUTE 53 - REGISTER A DOMAIN NAME** 
</span>

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         

**bootstrap script**    
```bash
#!/bin/bash
yum update -y
yum install httpd -y
service httpd start
cd /var/www/html
echo "<hmtl><body><h1>Hello Cloud Gurus</h1></body></html>" > index.html
```

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         


Il est possible de faire des enregistrement DNS directement depuis AWS.    
Cela veut dire qu'on peut acheter les domains names depuis AWS     
Cela prend 3 jours environ pour s'inscrire    


> _Remarque_: **Route53** est au niveau **Global** dans AWS, pas besoin de spécifier une region    


[Documentation aws: What is Route53 ?](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)    


Aller dans la console aws dans le service route53 et vérifier un domain name ➡️ envoie vers une page qui indique si le nom de domaine indiqué existe ou s'il peut être acheté, continuer dans les pages suivantes  ➡️ le nom de domaine sera enregistré au bout d'un certain temps (dépendant de plusieurs paramètres)   

En allant ensuite dans **hosted zones** nous pouvons voir notre domain name qui apparait   


> _Remarque_: Il est nécessaire d'avoir des instances EC2 qui fonctionnent dans les regions souhaitées  pour pouvoir ensuite mettre en place le DNS avec Route53     





======================================================================================================================= 


<span style="color: #EA9811">

###  **ROUTE 53 - DEMO** 
</span>



[Documentation aws: Values that you specify when you create or update health checks](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/health-checks-creating-values.html)    


.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Simple Routing Policy** 
</span>



**Simple Routing Policy**:     
Si nous choisissons cette policy cela veut dire qu'il ne peut y avoir qu'un seul enregistrement de domaine qui peut contenir plusieurs IP addresses 
Si l'on spécifie plusieurs valeurs (IP) dans 1 enregistrement, route53 va toutes les renvoyer à l'utilisateur de manière aléatoire      




.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Weighted Routing Policy** 
</span>


**Weighted Routing Policy c'est quoi ?**      
Une **Weighted Routing Policy** permet de splitter notre trafic en se basant sur différents poids    

_exemple_: 10% sur la region us-east-1 et 90% sur la region eu-west-1    



De la même manière lors de la création d'une hosted zone, il faut choisir cette fois la policy **weighted** et à ce moment nous pouvons dire quel point du trafic nous souhaitons mettre sur cette zone    

> _Remarque_: Dans ce cas nous ne pouvons pas mettre plusieurs IPs dans la même regle, il faut donc créer une regle par IP    




![route53-weigthedPolicy-examTips](./images/aws_solutionArchitectAssociate/route53-weigthedPolicy-examTips.png)    



.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Failover Routing Policy** 
</span>


**Failover Routing Policy c'est quoi ?**      
**Failover Routing Policy** est utilisé lorsqu'on souhaite créer une configuration active/passive    


_exemple_: on souhaite avoir un site primaire (eu-west-1) et un site seccondaire (pour un Disaste Recovery en ap-southesat-2)   


Route53 va monitorer la health du site primaire et pouvoir faire une bascule si nécessaire   

Comme précédemment, lors de la creation de la hosted zone, nous sélectionnant cette fois la **failover** policy     
  ➡️ comme précédemment 1 seule IP peut être configurée pour cette regle   
  ➡️ il faut donc 1 regle pour le primary et 1 regle pour le secondary    





.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Geolocation Routing Policy** 
</span>


**Geolocation Routing Policy c'est quoi ?**      
La **Geolocation Routing Policy** permet de choisir où le trafic se fera en fonction de la geolocalisation des users    


![route53-geolocationPolicy](./images/aws_solutionArchitectAssociate/route53-geolocationPolicy.png)    

Comme précédemment il faut 1 regle par IP (dans le cas où nous n'avons que 2 IPs)  ➡️ pour 2 IPs il faut 2 regles  





.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Geoproximity Routing Policy** 
</span>


**Geoproximity Routing Policy c'est quoi ?**       
Nous pouvons utiliser **Route53 Traffic Flow** pour construire un système de routage qui utilise une combinaison de:    
- localisation geographique      
- latence     
- disponibilité du trafic      

Pour les users de votre cloud ou on-premises endpoints       
Il est possible de créer les policies à partir de 0 ou de récupérer un template depuis une bibliothèque et de le personnaliser    

Il est possible de choisir de router plus ou moins de trafic en donnant une valeur specifique connu en tant que **bias**    
Un **bias** permet d'aggrandir ou diminuer la region geographique pour le routage de trafic    


[Documentation aws: creating a traffic policy](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/traffic-policies.html#traffic-policies-creating)    



Dans la console aws / route53/traffic Flow / traffic policies / create a traffic policy


Il s'agit d'un melange de toutes les regles DNS que route53 peut fournir. Cela peut donc devenir très complexe.
A configurer en fonction de nos besoins   


![route53-traficFlowPolicy](./images/aws_solutionArchitectAssociate/route53-traficFlowPolicy.png)    


.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Latency Routing Policy** 
</span>


**Latency Routing Policy c'est quoi ?**      
**Latency Routing Policy** permet de router le trafic vers le qui a la plus faible latence par rapport à user final    

Cela implique qu'il faut créer plusieurs instances dans plusieurs region pour que le user soit dans une même region que l'instance pour que la latence soit la plus faible     
  ➡️ lorsque route53 reçoit une requête pour le site, il sélecte la latence la plus faible (la region) et renvoie la requête vers cette instance



⚠️ La region qui a la latence la plus faible n'est pas forcément la region dans laquelle se trouve le user final   ⚠️     


![route53-latencePolicy](./images/aws_solutionArchitectAssociate/route53-latencePolicy.png)    



.....................................................................................................      

<span style="color: #11E3EA">

####  **Route 53 - DEMO - Using a Multivalue Answer Routing Policy** 
</span>


**Multivalue Answer Routing Policy c'est quoi ?**        
**Multivalue Answer Routing Policy** nous laisse configurer Amazon Route53 pour renvoyer plusieurs values comme les IP adresses des web servers en réponse aux requêtes DNS    


![route53-multipleValuesPolicy](./images/aws_solutionArchitectAssociate/route53-multipleValuesPolicy.png)     

Cela fonctionne avec le health checks ➡️ il doit donc être mis en place   





.....................................................................................................   

======================================================================================================================= 


<span style="color: #EA9811">

###  **ROUTE 53 - EXAM TIPS** 
</span>


![route53-examTips01](./images/aws_solutionArchitectAssociate/route53-examTips01.png)       
![route53-examTips02](./images/aws_solutionArchitectAssociate/route53-examTips02.png)       
![route53-examTips03](./images/aws_solutionArchitectAssociate/route53-examTips03.png)       
![route53-examTips04](./images/aws_solutionArchitectAssociate/route53-examTips04.png)       
![route53-examTips05](./images/aws_solutionArchitectAssociate/route53-examTips05.png)       
![route53-examTips06](./images/aws_solutionArchitectAssociate/route53-examTips06.png)       
![route53-examTips07](./images/aws_solutionArchitectAssociate/route53-examTips07.png)       
![route53-examTips08](./images/aws_solutionArchitectAssociate/route53-examTips08.png)       
![route53-examTips09](./images/aws_solutionArchitectAssociate/route53-examTips09.png)       
![route53-examTips10](./images/aws_solutionArchitectAssociate/route53-examTips10.png)       
![route53-examTips11](./images/aws_solutionArchitectAssociate/route53-examTips11.png)       
![route53-examTips12](./images/aws_solutionArchitectAssociate/route53-examTips12.png)       





======================================================================================================================= 


<span style="color: #EA9811">

###  **ROUTE 53 - QUIZZ** 
</span>



![route53-quizz01](./images/aws_solutionArchitectAssociate/route53-quizz01.png)     
![route53-quizz02](./images/aws_solutionArchitectAssociate/route53-quizz02.png)     
![route53-quizz03](./images/aws_solutionArchitectAssociate/route53-quizz03.png)     
![route53-quizz04](./images/aws_solutionArchitectAssociate/route53-quizz04.png)     
![route53-quizz05](./images/aws_solutionArchitectAssociate/route53-quizz05.png)


**QUIZZ REDO**      


![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/63.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/64.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/65.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/66.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/67.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/68.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/69.png)         
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/70.png)         



======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **ELB: ELASTIC LOAD BALANCING** 
</span>


[Documentation aws: ELB Elastic Load Balancer](https://aws.amazon.com/elasticloadbalancing/?nc1=h_ls)     
[Documentation aws: ELB Get Started](https://aws.amazon.com/elasticloadbalancing/?nc1=h_ls)    



**ELB** (**Elacting Load Balancing**) distribut automatiquement les flux entrants des applications à travers de multiple cibles et appareils virtuels dans 1 ou plusieurs Avalabilty Zones (AZs)    


![elb_schema_ApplicationLoadBalancer](./images/aws_solutionArchitectAssociate/elb_schema_ApplicationLoadBalancer.png)     
![elb_schema_GatewayLoadBalancer](./images/aws_solutionArchitectAssociate/elb_schema_GatewayLoadBalancer.png)     
![elb_schema_NetworkLoadBalancer](./images/aws_solutionArchitectAssociate/elb_schema_NetworkLoadBalancer.png)     






======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - OVERVIEW** 
</span>


[Documentation aws: ELB FAQs](https://aws.amazon.com/elasticloadbalancing/faqs/)         
[Documentation aws: ELB freatures](https://aws.amazon.com/elasticloadbalancing/features/)        
[Documentation aws: ELB Health checks](https://docs.aws.amazon.com/elasticloadbalancing/latest/network/target-group-health-checks.html)     


Il existe historiquement 3 types de load balancer: 
- **Application Load Balancer**: le mieux adapté pour le load balancing de trafic HTTP et HTTPS. Il opère sur la couche 7 (modele OSI ➡️ couche Applicative) et est compatible avec les application  ➡️ **Intelligent Load balancer**    
- **Network Load Balancer**: Opère au niveau de la couche de connexion (couche 4 modele OSI ➡️ Transport). Ils sont capable de prendre en charge des millions de requêtes par seconde tout en maintenant une très faible latence  ➡️ **Performance Load Balancer**    
- **Classic Load Balancer**: Les plus ancients historiquement.Il prennent en charge le load balancing HTTP/HTTPS applicatives et ustilise des fonctionnalités spécifiques de la couche 7 comme le _X-forwarded_ et les _sticky sessions_  ➡️ **classic/test/Dev Load balancer**    



![modeleOSI](./images/aws_solutionArchitectAssociate/modeleOSI.png)    




Tous les load balancers AWS peuvent être configurés avec le **health check**    
Chaque instance "derrière" un load balancer va être requêté régulièrement et envoie un status.    
Si celui-ci est **up** (en service) ➡️ les requêtes pourront lui être distribuées sinon non    
  


![elb-overview-examTips01](./images/aws_solutionArchitectAssociate/elb-overview-examTips01.png)    
![elb-overview-examTips02](./images/aws_solutionArchitectAssociate/elb-overview-examTips02.png)    



======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - USING APPLICATION LOAD BALANCER** 
</span>


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++     
```bash
#!/bin/bash
yum update -y
yum install httpd -y
systemctl start httpd
systemctl enable httpd
cd /var/www/html
echo '<html><h1>This is Web Server 1</h1></html>' > index.html
```
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++     



**Un load balancer d'application (couche 7) c'est quoi ?**   
Après que le load balancer ait reçu une requête, il évalue la regle d'écoute (listeners rules) dans l'ordre de priorité pour déterminer quelle regle appliquer et sélectionner la cible parmi un groupe de cible


**Un listener c'est quoi ?**    
Un **listener** verifie les requêtes pour les connexions clients en utilisant le protocole et port configuré    
On définit les regles qui déterminent comment le load balancer route les requêtes vers ses cibles enregistrées   

Chaque regle correspond à 1 priorité, 1 ou plusieurs actions et 1 ou plusieurs conditions    



**Que sont les regles (rules) ?**    
Lorsque les conditions d'une regle sont atteintes alors les actions sont réalisées    
Il faut définir une regle par defaut pour chaque listener puis il est possible d'ajouter d'autres regles   


**Qu'est-ce que le target group ?**    
Chaque **target group** redirige les requêtes vers 1 ou plusieurs cibles enregistrées comme une instance EC2, utilisant le port et le protocole définit   


![elb-diagramApplicationLoadBalancer](./images/aws_solutionArchitectAssociate/elb-diagramApplicationLoadBalancer.png)    




.......................................................................................................................  


<span style="color: #11E3EA">

####  **Path-based Routing - Common Exam Scenario** 
</span>


![elb_commonExam](./images/aws_solutionArchitectAssociate/elb_commonExam.png)     


.......................................................................................................................



>_Remarque_: **Un load balancer d'application NE SUPPORTE QUE le HTTP et le HTTPS**




.......................................................................................................................  


<span style="color: #11E3EA">

####  **ELB - HTTPS Load Balancing** 
</span>

Pour utiliser un listener HTTPS, il est OBLIGATOIRE de déployer au moins 1 certificat serveeur SSL/TLS sur le load balancer.    
Le load balancer utilise un certificat serveur pour mettre fin à la connexion frontend et décrypter les requêtes des clients avant de les envoyer vers les cibles ➡️ la décryption est réalisée sur le load balancer   


.......................................................................................................................  


<span style="color: #11E3EA">

####  **ELB - Mise en place de 3 serveurs web dans 3 AZ différentes avec un load balancer** 
</span>

Pour un soucis de simplicité ce lab est fait sur le port 80 (HTTP) pour la pas à avoir à mettre en place de certificat pour le HTTPS (mais cela peut être fait pour valider)

1. On crée 3 serveurs web dans 3 AZ différentes en utilisant le bootstrap script sur ces serveurs (en pensant à différencier la valeur "WebServer1/2/3" dans le script en fonction du serveur)    

2. On vérifie que chaque IP publique est accessible et retourne bien ce qui est mentionné dans le bootstrap script    

3. Aller dans le service EC2 dans la partie (à gauche de l'écran) **Load Balancing**    

4. Créer un Application Load Balancer   
    ➡️ penser à mapper les 3 AZ à ce load balancer
    ➡️ ajouter le même security group que pour les instances
    ➡️ ajouter un listener: définir le port utilisé
                            créer un target group (le nommer ...)
                              definir les health checks
                              cliquer sur next
                                ajouter les instances
                            ajouter le nom du target group

5. Une fois le ALB (Application Load Balancer) actif, copier son nom DNS pour tester et valider dans le navigateur que cela fonctionne (doit afficher au refresh les 3 serveurs 1 après l'autre)



.......................................................................................................................



![elb-alb_examTips01](./images/aws_solutionArchitectAssociate/elb-alb_examTips01.png)   
![elb-alb_examTips02](./images/aws_solutionArchitectAssociate/elb-alb_examTips02.png)    






======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - EXTREME PERFORMANCE WITH NETWORK LOAD BALANCERS** 
</span>



Ce type de load balancer est utilisé lorsqu'on souhaite gérer "énormément" de connexions en cible    

Il agit sur la couche 4 du modèle OSI (transport). 

Il peut gérer plusieurs millions de requêtes par seconde   


Une fois qu'il a reçu 1 requête de connexion, il sélectionne une cible dans son target group    
Cela va ouvrir une connexion TCP vers la cible choisie          


Un listener pour un network load balancer renvoie les requêtes vers le target group.     
Il n'y a pas de regles contrairement à l'Application Load Balancer


Les target groups fonctionnent de la même manière que ceux de l'ALB     

Les ports et protocoles supportés sont:    
- **protocoles**: TCP, TLS, UDP, TCP_UDP      
- **ports**! 1 ➡️ 65535    


Il est possible d'utiliser un TLS listener pour décharger l'encryption/décryption des load balancers ainsi les applications peuvent se concentrer sur leur logique d'affaire   


>_Remarque_: **Si unlistener utilise le protocole TLS, il FAUT DEPLOYER EXACTEMENT 1 SSL certificate sur le listener**   


![elb-nlb-examTips01](./images/aws_solutionArchitectAssociate/elb-nlb-examTips01.png)     
![elb-nlb-examTips02](./images/aws_solutionArchitectAssociate/elb-nlb-examTips02.png)    




======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - USING THE CLASSIC LOAD BALANCER** 
</span>

**Un classic Load Balancer c'est quoi ?**:    
Ce sont les LB "historiques" (legacy).     
Il est possible de load balancer HTTP/HTTPS sur la couche 7 en utilisant des fonctionalités spécifiques (x-forwarded, sticky sessions).    
Il est aussi possible d'utiliser la couche 4 pour le load balancing reseau sur le protocole TCP    




**X-Forwarded-For**:    
![elb_X-Forwarded](./images/aws_solutionArchitectAssociate/elb_X-Forwarded.png)    
Lorsque le trafic est envoyé depuis un LB, les logs d'accès du serveurs contiennent l'IP du proxy ou du LB.    
Pour connaitre l'IP source "originale" du client le **X-Forwarded-For** dans le requête du header est utilisé    



**Gateway Timeouts**:    
Si l'application ne répond pasld LB classic renvoie un **504**     
Cela veut dire que l'application a eu un problème.    
Cela peut venir du webserver ou de la db server   



![elb_calssicLoadBalancer-examTips](./images/aws_solutionArchitectAssociate/elb_calssicLoadBalancer-examTips.png)    



======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - GETTING STUCK WITH STICKY SESSIONS** 
</span>


Etre coincé avec les sessions collantes     

Ceci est une partie qui reviens souvent lors de la certification    

**Sticky sessions c'est quoi ?**    
**Classic Load Balancer** route chaque requête indépendamment vers l'instance EC2 enregistrée (en fonction de son algorithme)    
**Une Sticky Session** autorise le lien entre un "user session" et une instance EC2 définie    

Si cela est mis en place le problème potentiel est que l'instance "tombe", elle ne serait plus accessible MAIS le LB continu d'envoyer les requêtes vers celle-ci ➡️ KO   

Comment faire pour résoudre le problème ?    ➡️ disable sticky session     


![elb_stickySessions-examTips01](./images/aws_solutionArchitectAssociate/elb_stickySessions-examTips01.png)    
![elb_stickySessions-examTips02](./images/aws_solutionArchitectAssociate/elb_stickySessions-examTips02.png)    








======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - LEAVING THE LOAD BALANCER WITH DEREGISTRATION DELAY** 
</span>


Le **deregistration delay** (ou **connection draining**) permet aux LB de conserver une connexion ouverte si les instances EC2 sont "dé enregistrées" puisque le healthcheck est unhealthy    

Cela permet aux LB de terminer les requêtes en cours vers les instances qui ont été dé enregistrées    

Il est possible disable cela si on ne souhaite pas garder d'instances unhealthy    


![elb_deregistraionDelay-examTips](./images/aws_solutionArchitectAssociate/elb_deregistraionDelay-examTips.png)    






======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - EXAM TIPS** 
</span>


[Documentation aws: Application Load Balancer Support for End-to-End HTTP/2 and gRPC](https://aws.amazon.com/blogs/aws/new-application-load-balancer-support-for-end-to-end-http-2-and-grpc/)    



![elb-examTips01](./images/aws_solutionArchitectAssociate/elb-examTips01.png)    
![elb-examTips02](./images/aws_solutionArchitectAssociate/elb-examTips02.png)    
![elb-examTips03](./images/aws_solutionArchitectAssociate/elb-examTips03.png)    
![elb-examTips04](./images/aws_solutionArchitectAssociate/elb-examTips04.png)    
![elb-examTips05](./images/aws_solutionArchitectAssociate/elb-examTips05.png)    
![elb-examTips06](./images/aws_solutionArchitectAssociate/elb-examTips06.png)    
![elb-examTips07](./images/aws_solutionArchitectAssociate/elb-examTips07.png)    
![elb-examTips08](./images/aws_solutionArchitectAssociate/elb-examTips08.png)    
![elb-examTips09](./images/aws_solutionArchitectAssociate/elb-examTips09.png)    
![elb-examTips10](./images/aws_solutionArchitectAssociate/elb-examTips10.png)    




======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - LAB - USE APPLICATION LOAD BALANCERS FOR WEB SERVERS** 
</span>



![elb_lab-enonce](./images/aws_solutionArchitectAssociate/labs/elb_lab/elb_lab-enonce.png)    

AWS Account:     
username: cloud_user       
password: &8Psttavwycwqht      
link: https://264357452481.signin.aws.amazon.com/console?region=us-east-1      

Cloud Server of webserver-01    
username: cloud_user    
password: &W[|cbW8      
Public IP of webserver-01: 54.237.209.51    

![elb_lab-enonce-diagram](./images/aws_solutionArchitectAssociate/labs/elb_lab/elb_lab-enonce-diagram.png)  




**Solution**

Log in to the live AWS environment using the credentials provided. Use an incognito or private browser window to ensure you're using the lab account rather than your own.

Make sure you're in the N. Virginia (us-east-1) region throughout the lab.


**Observe the Provided EC2 Website and Create a Second Server**  

1. Navigate to EC2.

2. Click Instances (running).

3. Select the box next to webserver-01.

4. Copy its Public IPv4 address.

5. In a new browser tab, paste in the public IP address you just copied. You should see the load balancer demo page.

6. Back in the EC2 console, at the top, click Launch instances.

7. Under Name and Tags, enter "webserver2".

8. Under Application and OS Images (Amazon Machine Image), select Ubuntu and Ubuntu Server 22.04 LTS.

9. Under Instance Type, select t3.micro.

10. Under Key pair (login), in the dropdown, select Proceed without a key pair.

11. Under Network settings, click Edit and set Auto-assign Public IP to Enable.

12. Under Network settings > Firewall (security groups), click Select existing security group and select the one with EC2SecurityGroup in its name (not the default security group).

13. Under Advanced Details, in the User Data box, enter the following bootstrap script:

```bash
#!/bin/bash
sudo apt-get update -y
sudo apt-get install apache2 unzip -y
echo '<html><center><body bgcolor="black" text="#39ff14" style="font-family: Arial"><h1>Load Balancer Demo</h1><h3>Availability Zone: ' > /var/www/html/index.html
curl http://169.254.169.254/latest/meta-data/placement/availability-zone >> /var/www/html/index.html
echo '</h3> <h3>Instance Id: ' >> /var/www/html/index.html
curl http://169.254.169.254/latest/meta-data/instance-id >> /var/www/html/index.html
echo '</h3> <h3>Public IP: ' >> /var/www/html/index.html
curl http://169.254.169.254/latest/meta-data/public-ipv4 >> /var/www/html/index.html
echo '</h3> <h3>Local IP: ' >> /var/www/html/index.html
curl http://169.254.169.254/latest/meta-data/local-ipv4 >> /var/www/html/index.html
echo '</h3></html> ' >> /var/www/html/index.html
```


14. Click Launch Instance.

15. Click the Instance ID (This will start with i-).

16. Once it's in the Running state, copy the Public IPv4 address.

17. In a new browser tab, paste in the public IP address you just copied. You should see the load balancer demo page again, which means the legacy clone is successfully running. This time, though, it will have a different instance ID, public IP, and local IP listed.


**Create an Application Load Balancer**


1. Back in the EC2 console, click Load Balancers in the left-hand menu.

2. Click Create Load Balancer.

3. From the Application Load Balancer card, click Create.

4. For Load balancer name, enter LegacyALB.

5. Under Network mapping, click the VPC dropdown, and select the listed VPC.

6. When the Availability Zones list pops up, select each one (us-east-1a, us-east-1b, and us-east-1c).

7. Under Security groups, deselect the default security group listed, and select the one from the dropdown with EC2SecurityGroup in its name.

8. Under Listeners and routing, ensure that the Protocol is set to HTTP and the Port is 80. Then, under Default action, click Create target group This will open a new tab. Keep this first tab open to complete later.

9. For Target group name, enter TargetGroup.

10. Click Next.

11. Under Available instances, select both targets that are listed.

12. Click Include as pending below.

13. Click Create target group.

14. Back in the first tab, under Default action, click the refresh button (looks like a circular arrow), and in the dropdown, select the TargetGroup you just created.

15. Click Create load balancer.

16. On the next screen, click View load balancer.

17. Wait a few minutes for the load balancer to finish provisioning and enter an active state.

18. Copy its DNS name, and paste it into a new browser tab. You should see the load balancer demo page again. The local IP lets you know which instance you were sent (or "load balanced") to.

19. Refresh the page a few times. You should see the other instance's local IP listed, meaning it's successfully load balancing between the two EC2 instances.


**Enable Sticky Sessions**

1. Back on the EC2 > Load Balancers page, select the Listeners tab.

2. Click the TargetGroup link in the Rules column, which opens the target group.

3. Click the only link to open TargetGroup.

4. Select the Attributes tab.

5. Click Edit.

6. Check the box next to Stickiness to enable it.

7. Leave Stickiness type set to Load balancer generated cookie.

8. Leave Stickiness duration set to 1 days.

9. Click Save changes.

10. Refresh the tab where you navigated to the load balancer's public IP. This time, no matter how many times you refresh, it will stay on the same instance (noted by the local IP).






======================================================================================================================= 


<span style="color: #EA9811">

###  **ELB - QUIZZ** 
</span>





![elb-quizz01](./images/aws_solutionArchitectAssociate/elb-quizz01.png)    
![elb-quizz02](./images/aws_solutionArchitectAssociate/elb-quizz02.png)    
![elb-quizz03](./images/aws_solutionArchitectAssociate/elb-quizz03.png)    
![elb-quizz04](./images/aws_solutionArchitectAssociate/elb-quizz04.png)    
![elb-quizz05](./images/aws_solutionArchitectAssociate/elb-quizz05.png)    


**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/71.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/72.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/73.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/74.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/75.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/76.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/77.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/78.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/79.png)               



======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **MONITORING** 
</span>



[Documentation aws: cloudwatch monitoring](https://docs.aws.amazon.com/cloudwatch/index.html)     
[Documentation aws: monitoring best practices for AWS Outposts](https://aws.amazon.com/blogs/mt/monitoring-best-practices-for-aws-outposts/)    



**Amazon CloudWatch** fournit une solution de surveillance fiable, évolutive et flexible que vous pouvez commencer à utiliser en quelques minutes. Vous n'avez plus besoin de configurer, de gérer et de faire évoluer vos propres systèmes et infrastructure de surveillance.


On peut accéder au service via appels API, cli, aws sdk, la console aws    

Cloudwatch reçoit et fournit les metriques de toutes les instances EC2 quelque soit l'OS tant qu'il est supporté par aws    

Cloudwatch fonctionne avec les accès IAM    

Les logs cloudwatch permettent de monitorer en "presque" temps réel



======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - CLOUDWATCH OVERVIEW** 
</span>


[Documentation aws: cloudwatch concepts](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html)     
[Documentation aws: cloudwatch FAQs](https://aws.amazon.com/cloudwatch/faqs/)    


**Qu'est-ce qur CloudWatch ??**        
**CloudWatch** est une platform de monitoring conçue par AWS pour visualiser notre architecture    
Elle permet de monitorer à plusieurs niveaux nos applications et identifier de potentiels problèmes    

Cloudwatch      
- collecte les metriques systeme    
- collecte les metrqiues applicatives (des instances EC2)     
- crée des alarmes



Il existe 2 types de metriques: 
- default   ➡️ fournit par defaut et besoin d'aucune configuration supplémetaire
- custom    ➡️ l'agent cloudwatch DOIT être installé sur le host

![monitoring_cloudwatch-metrics](./images/aws_solutionArchitectAssociate/monitoring_cloudwatch-metrics.png)     


![monitoring_cloudwatch-overview-examTips01](./images/aws_solutionArchitectAssociate/monitoring_cloudwatch-overview-examTips01.png)     
![monitoring_cloudwatch-overview-examTips02](./images/aws_solutionArchitectAssociate/monitoring_cloudwatch-overview-examTips02.png)     




======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - APPLICATION MONITORING WITH CLOUDWATCH LOGS** 
</span>


[Documentation aws: scaling based on amazon SQS](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-using-sqs-queue.html)     


**CloudWatch logs** est un outil qui permet de monitorer, conserver et accéder aux logs de différentes sources.    
Cela permet de requêter nos logs pour chercher un potentiel problème ou données qui sera pertinent    


Il y a 3 types de logs cloudwatch:     
- **log event**: c'est l'enregistrement de ce qu'il s'est passé. Il contient le timestamp et la data     
- **log stream**: c'est une collection de _log event_ depuis 1 unique source (instance)      
- **log group**:  c'est une collection de _log stream_ depuis 1 seule localisation (on ne parle plus d'instance)    

![monitoring_cloudwtach-logsTpyesDeFeature](./images/aws_solutionArchitectAssociate/monitoring_cloudwtach-logsTpyesDeFeature.png)    



**Comment installer l'agent cloudwatch sur une instance ?**     

```bash
## installation de l'agent
sudo yum install amazon-cloudwatch-agent -y

## génération d'un fichier de config custom pour permettre la customization de la supervision
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-config-wizard
### un formulaire est demandé lors de l'exécution de cette commande pour connaitre les infos à renseigner (ex: type d'OS...)

## load et run l'agent à partir du fichier "config.json" que nous venons de créer
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:sudo /opt/aws/amazon-cloudwatch-agent/bin/config.json
```



![monitoring_cloudwatch-logs-examTips01](./images/aws_solutionArchitectAssociate/monitoring_cloudwatch-logs-examTips01.png)     
![monitoring_cloudwatch-logs-examTips02](./images/aws_solutionArchitectAssociate/monitoring_cloudwatch-logs-examTips02.png)     


======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - MONITORING WITH AMAZON MANAGED SERVICE FOR PROMETHEUS AND AMAZON MANAGED GRAFANA** 
</span>


**Qu'est-ce qu'un service amazon managed grafana ?**     
Amazon Managed Grafana est un service Grafana entièrement géré qui est évolutif, sécurisé et hautement disponible. Avec Amazon Managed Grafana, vous pouvez analyser et surveiller les métriques, journaux et traces sur plusieurs sources de données, ainsi que générer des alarmes.        

[Documentation aws: amazon service grafana](https://eu-west-1.console.aws.amazon.com/grafana/home?region=eu-west-1#)    


![monitoring-grafana_fonctionnement](./images/aws_solutionArchitectAssociate/monitoring-grafana_fonctionnement.png)      


Grafana est facile à déployer, utiliser avec le compte aws    
Il est possible d'avoir des workspaces (serveurs logiques grafana) qui permettent une séparation des données et des requêtes    
Ce service est managé par AWS: installation, configuration , MCO, MCS fait par AWS   
Il contient des fonctionalités de securité    
Son prix est définit par nombre d'utilisateurs actifs dans 1 workspace     
Le service grafana dans amazon est directement intégré avec les data sources cloudwatch, prometheus, opensearch, timestream d'amazon    

**Quels sont les use cases grafana ?**     
- **container metric visualizations**: connecté aux data source comme prometheus pour la visualisation EKS, ECS ou connaitre les metrqiues de votre propre cluster kubernetes    
- **IoT**:  grande possibilité de plugins pour une connexion permettant le monitoring IoT et du device
- **troubleshooting**: la centralisation de dashboards permet une plus grande efficacité dans le recherche et la résolution de problème   




**Qu'est-ce que le service managé prometheus ?**    
Service de surveillance entièrement géré, compatible avec Prometheus, qui facilite la surveillance sécurisée et à grande échelle des applications conteneurisées.  

[Documentation aws: Amazon Managed Service for Prometheus](https://eu-west-1.console.aws.amazon.com/prometheus/home?region=eu-west-1#/)    


Les API compatibles avec Prometheus vous donnent accès à des métriques d’écriture à distance depuis le serveur Prometheus existant et aux métriques de requête à l’aide de PromQL.    

Il  évolue automatiquement au fur et à mesure que vos besoins en matière d'ingestion et de requête augmentent, en gérant des millions de métriques de séries chronologiques uniques à l'aide de déploiements multi-AZ    

Il s'intègre à AWS Identity and Access Management (IAM) pour l'authentification et les autorisations précises pour les utilisateurs et les groupes. VPC PrivateLink fournit un accès facile et sécurisé aux services hébergés sur AWS, en maintenant votre trafic réseau au sein du réseau AWS.

Les données sont sauvegardées pendant 150 jours


![monitoring-grafana-examTips](./images/aws_solutionArchitectAssociate/monitoring-grafana-examTips.png)       



======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - EXAM TIPS** 
</span>



![monitoring-examTips01](./images/aws_solutionArchitectAssociate/monitoring-examTips01.png)     
![monitoring-examTips02](./images/aws_solutionArchitectAssociate/monitoring-examTips02.png)     
![monitoring-examTips03](./images/aws_solutionArchitectAssociate/monitoring-examTips03.png)     
![monitoring-examTips04](./images/aws_solutionArchitectAssociate/monitoring-examTips04.png)     





======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - LAB1 - IMPLEMENT ADVANCED CLOUDWATCH MONITORING FOR A WEB SERVER** 
</span>


![monitoringLab01_enonce](./images/aws_solutionArchitectAssociate/labs/monitoringLab01/monitoringLab01_enonce.png)       
![monitoringLab01_objectif](./images/aws_solutionArchitectAssociate/labs/monitoringLab01/monitoringLab01_objectif.png)      




aws account:     
Username: cloud_user         
Password: #9Xajcdarngppom           
link: https://134071960526.signin.aws.amazon.com/console?region=us-east-1        

Cloud Server webserver-01:         
Username: cloud_user           
Password: -#6tOqGo          
webserver-01 Public IP: 52.23.182.211            


**Solution**    

Log in to the AWS Management Console using the credentials provided on the lab instructions page. Make sure you're using the us-east-1 Region.


**Download and Run the CloudWatch Logs Installer**    

1. From the AWS Management Console, navigate to EC2.

2. Select Instances (running).

3. Select webserver-01.

4. At the top, click Connect.

5. At the bottom of the page, click Connect to access the CLI.

6. Run the following commands to install the CloudWatch Logs agent:

```bash
wget -O awslogs-agent-setup.py https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py 
sudo python ./awslogs-agent-setup.py --region us-east-1
```


**Configure CloudWatch Logs**    

1. Go back to the AWS Management Console, and open IAM in a separate tab.

2. Click Users.

3. Click cloud_user.

4. Select the Security credentials tab.

5. Click Create access key.

6. Select Command Line Interface (CLI).

7. Check the acknowledgment checkbox at the bottom of the page and click Next.

8. Click Create access key.

9. In case you need to reference them again, click Download .csv file to download the credentials file to your browser's downloads folder. You can open this file in a plain text editor if needed.

10. From the Access key field on the left, copy the Access key ID.

11. Go back to the CLI (keep IAM open in the other tab), paste the Access key ID, and press Enter.

12. Go back to IAM and, in the Secret access key on the right, click Show. Then, copy the Secret access key.

13. Go back to the CLI, paste the Secret access key, and press Enter.

14. Press Enter twice to accept the default region name and output format.

15. Copy and paste the below path for the log file to upload:

`/var/log/apache2/error.log`  

16. Press Enter to keep the current Destination Log Group name.

17. Press Enter to accept the default Log Stream name.

18. Press Enter to keep the default Log Event timestamp format.

19. Press Enter to select From start of file as the initial position of upload.

20. Type N to complete the configuration.


**Log in to the CloudWatch Logs Website**   

1. Copy the link provided in the CLI output and open it in a new browser tab to begin accessing new log events. Make sure to include the ending :.

2. Click `/var/log/apache2/error.log.`

3. Under Log streams, click the link for the instance identifier. You will see the contents of your error log with two events logged.

4. You can also view the contents of the error log in the CLI by running the following command:

`sudo cat /var/log/apache2/error.log`






======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - LAB2 - WORK WITH AWS VPC FLOW LOGS FOR NETWORK MONITORING** 
</span>


![monitoringLab02_enonce](./images/aws_solutionArchitectAssociate/labs/monitoringLab02/monitoringLab02_enonce.png)    


![monitoringLab02_objectif](./images/aws_solutionArchitectAssociate/labs/monitoringLab02/monitoringLab02_objectif.png)    


AWS Account:    

Username: cloud_user       
Password: #7Ptxpffqtejeak                     
link: https://739458833322.signin.aws.amazon.com/console?region=us-east-1        


Username: cloud_user       
Password: UTQoB4^]                          
Public IP address of EC2 instance: 18.212.100.24     




cloudwatch log metric filter pattern:     
```bash
[version, account, eni, source, destination, srcport, destport="22", protocol="6", packets, bytes, windowstart, windowend, action="REJECT", flowlogstatus]
```


Custom Log Data to Test:     
```bash
2 086112738802 eni-0d5d75b41f9befe9e 61.177.172.128 172.31.83.158 39611 22 6 1 40 1563108188 1563108227 REJECT OK
2 086112738802 eni-0d5d75b41f9befe9e 182.68.238.8 172.31.83.158 42227 22 6 1 44 1563109030 1563109067 REJECT OK
2 086112738802 eni-0d5d75b41f9befe9e 42.171.23.181 172.31.83.158 52417 22 6 24 4065 1563191069 1563191121 ACCEPT OK
2 086112738802 eni-0d5d75b41f9befe9e 61.177.172.128 172.31.83.158 39611 80 6 1 40 1563108188 1563108227 REJECT OK
```


Create Athena Table:      
```bash
CREATE EXTERNAL TABLE IF NOT EXISTS default.vpc_flow_logs (
  version int,
  account string,
  interfaceid string,
  sourceaddress string,
  destinationaddress string,
  sourceport int,
  destinationport int,
  protocol int,
  numpackets int,
  numbytes bigint,
  starttime int,
  endtime int,
  action string,
  logstatus string
) PARTITIONED BY (
  dt string
) ROW FORMAT DELIMITED FIELDS TERMINATED BY ' ' LOCATION 's3://{your_log_bucket}/AWSLogs/{account_id}/vpcflowlogs/us-east-1/' TBLPROPERTIES ("skip.header.line.count"="1");
```


Create Partitions:            
```bash
ALTER TABLE default.vpc_flow_logs
ADD PARTITION (dt='{Year}-{Month}-{Day}') location 's3://{your_log_bucket}/AWSLogs/{account_id}/vpcflowlogs/us-east-1/{Year}/{Month}/{Day}';
```


Analyze Data:             
```bash
SELECT day_of_week(from_iso8601_timestamp(dt)) AS
  day,
  dt,
  interfaceid,
  sourceaddress,
  destinationport,
  action,
  protocol
FROM vpc_flow_logs
WHERE action = 'REJECT' AND protocol = 6
order by sourceaddress
LIMIT 100;
```










**Solution**      

Log in to the live AWS environment using the credentials provided. Make sure you are using us-east-1 (N. Virginia) as the selected Region.

**Create a CloudWatch Log Group and VPC Flow Logs to CloudWatch**    

**Create a VPC Flow Log to S3**    

1. Navigate to VPC.

2. In the VPC dashboard, select the VPCs card.

You should see an A Cloud Guru VPC pre-provisioned for the lab.

3. Check the checkbox next to the A Cloud Guru VPC.

4. Toward the bottom of the screen, select the Flow logs tab.

5. On the right, click Create flow log.

6. Fill in the flow log details:

- Name: You can leave this field blank.
- Filter: Ensure that All is selected.
- Maximum aggregation interval: Select 1 minute.
- Destination: Select Send to an Amazon S3 bucket.

7. Get the S3 bucket ARN:

- In a new browser tab, navigate to S3.
- Select the radio button next to the provided bucket.
- Click Copy ARN.

8. Navigate back to the VPC Management Console tab and fill in the rest of the flow log details:

- S3 bucket ARN: In the text field, paste your copied S3 bucket ARN.
- Log record format: Ensure that AWS default format is selected.

9. Leave the other fields as the default settings and click Create flow log.

Your flow log is created.

10. From the Your VPCs page, select the Flow logs tab.

11. Review the flow log details and verify that it shows an Active status.

12. Navigate back to the S3 Management Console tab.

13. Select your bucket name, and then select the Permissions tab.

14. Review the bucket policy and note that it is modified automatically by AWS when you create flow logs so that the flow logs can write to the bucket.

> Note: It can take between 5–15 minutes for flow logs to appear. You can continue working through the other lab objectives while you wait for the flow logs to populate.


**Create the CloudWatch Log Group and VPC Flow Log**  

1. In a new browser tab, navigate to CloudWatch.

2. In the CloudWatch sidebar menu, navigate to Logs and select Log groups.

3. Click Create log group.

4. In the Log group name field, enter VPCFlowLogs.

5. Click Create.

6. Navigate back to the VPC Management Console tab and ensure the Flow logs tab is still selected.

7. On the right, click Create flow log.

8. Fill in the flow log details:

- Name: You can leave this field blank.
- Filter: Ensure that All is selected.
- Maximum aggregation interval: Select 1 minute.
- Destination: Ensure that Send to CloudWatch Logs is selected.
- Destination log group: Click into the field and select your VPCFlowLogs log group.
- IAM role: Use the dropdown to select the DeliverVPCFlowLogsRole role.
- Log record format: Ensure that AWS default format is selected.

9. Click Create flow log.

Your flow log is created.

10. From the Your VPCs page, ensure the Flow logs tab is selected.

11. Review the flow log details and verify that the new flow log shows an Active state.

12. Navigate back to the CloudWatch Management Console tab.

13. Select the VPCFlowLogs log group name.

You should see there are currently no log streams. Remember, it may take some time before the flow logs start populating data.


**Generate Network Traffic**    

1. In a new browser tab, navigate to EC2.

2. In the Resources section of the EC2 dashboard, select Instances (running).

You should see a Web Server instance that was pre-provisioned for the lab.

3. Check the checkbox next to the Web Server instance.

4. In the instance's Details tab, copy the Public IPv4 address.

5. Open a terminal session and log in to the EC2 instance using the credentials provided for the lab:

`ssh cloud_user@<PUBLIC-IP-ADDRESS>`     
Now that you have connected to the terminal successfully, the VPC flow logs will record for this connection.

6. Exit the terminal:

`logout`    

7. Navigate back to the EC2 Management Console tab.

8. Update the EC2 instance security group:

- Check the checkbox next to the Web Server instance, and then use the Actions dropdown to select Security > Change security groups.
- In the Associated security groups section, click Remove to the right of the security group details to remove the SecurityGroupHTTPAndSSH group.
- Use the search bar in the Associated security groups section to select the SecurityGroupHTTPOnly security group.
- Click Add security group, and then click Save.

9. Navigate back to your terminal session and reconnect to the EC2 instance using the credentials provided for the lab:

`cloud_user@<PUBLIC-IP-ADDRESS>`    

This time, your connection should time out because you removed SSH access with the security group change. This will be recorded in VPC Flow Logs as a reject record.

10. Press Ctrl+C to cancel the SSH command.

11. Navigate back to the EC2 Management Console tab.

12. Revert the EC2 instance security group back to SecurityGroupHTTPAndSSH:

- Ensure the Web Server instance is selected, and then use the Actions dropdown to select Security > Change security groups.
- In the Associated security groups section, click Remove to the right of the security group details to remove the SecurityGroupHTTPOnly group.
- Use the search bar in the Associated security groups section to select the SecurityGroupHTTPAndSSH security group.
- Click Add security group, and then click Save.

13. Navigate back to your terminal session and reconnect to the EC2 instance using the credentials provided for the lab:

`ssh cloud_user@<PUBLIC-IP-ADDRESS>`   
This time, the connection should be accepted.



**Create CloudWatch Filters and Alerts**    


**Create a CloudWatch Log Metric Filter**   

1. Navigate back to the CloudWatch Management Console tab.

2. In the CloudWatch sidebar menu, navigate to Logs and select Log groups.

3. Select the VPCFlowLogs log group name.

You should now see a log stream. If you don't see a log stream listed yet, wait a few more minutes and refresh the page until the data appears.

4. Select the listed log stream name and review the data.

5. Use the breadcrumb along the top of the page to select VPCFlowLogs.

6. Select the Metric filters tab and then click Create metric filter.

7. In the Filter pattern field, enter the following pattern to track failed SSH attempts on port 22:

```bash
[version, account, eni, source, destination, srcport, destport="22", protocol="6", packets, bytes, windowstart, windowend, action="REJECT", flowlogstatus]
```

8. Use the Select log data to test dropdown to select Custom log data.

9. In the Log event messages field, replace the existing log data with the following:

```bash
2 086112738802 eni-0d5d75b41f9befe9e 61.177.172.128 172.31.83.158 39611 22 6 1 40 1563108188 1563108227 REJECT OK
2 086112738802 eni-0d5d75b41f9befe9e 182.68.238.8 172.31.83.158 42227 22 6 1 44 1563109030 1563109067 REJECT OK
2 086112738802 eni-0d5d75b41f9befe9e 42.171.23.181 172.31.83.158 52417 22 6 24 4065 1563191069 1563191121 ACCEPT OK
2 086112738802 eni-0d5d75b41f9befe9e 61.177.172.128 172.31.83.158 39611 80 6 1 40 1563108188 1563108227 REJECT OK
```

10. Click Test pattern and then review the results.

11. Click Next.

12. Fill in the metric details:

- Filter name: In the text field, enter dest-port-22-rejects.
- Metric namespace: In the text field, enter a name (e.g., vpcflowlogs).
- Metric name: In the text field, enter SSH Rejects.
- Metric value: In the text field, enter 1.

13. Leave the other fields blank and click Next.

14. Review the metric details and then click Create metric filter.


**Create an Alarm Based on the Metric Filter**    

1. After the metric filter is created, ensure that the Metric filters tab is selected.

2. In the Metric filter details, check the checkbox to the right of the dest-port-22-reject filter.

3. On the right, click Create alarm.

The Alarms page opens in a new browser tab automatically.

4. Specify the metric conditions:

- Period: Use the dropdown to select 1 minute.
- Threshold type: Ensure that Static is selected.
- Whenever SSH Rejects is...: Select Greater/Equal.
- than...: In the text field, enter 1.

The metric will trigger an alarm whenever there is one or more reject messages within a one-minute period.

5. Click Next.

6. Configure the alarm actions:

- Alarm state trigger: Ensure that In alarm is selected.
- Send a notification to the following SNS topic: Select Create a new topic.
- Create a new topic...: Leave the default topic name.
- Email endpoints that will receive the notification...: In the text field, enter an email address (this can be your real email address or a sample address like user@example.com), and then click Create topic.

> Note: If you entered your real email address, open your email inbox and click the Confirm Subscription link you received in the SNS email.

7. Click Next.

8. In the Alarm name field, enter SSH rejects.

9. Click Next.

10. Review the alarm details and then click Create alarm.

The alarm is created but will take some time to start populating data.



**Generate Traffic for Alerts**    


1. Navigate back to the terminal session and reconnect to the EC2 instance using the credentials provided for the lab:

`ssh cloud_user@<PUBLIC-IP-ADDRESS>`    

2. Exit the terminal:

`logout`    

3. Navigate back to the EC2 Management Console tab.

4. Update the EC2 instance security group:

- Check the checkbox next to the Web Server instance, and then use the Actions dropdown to select Security > Change security groups.
- In the Associated security groups section, click Remove to the right of the security group details to remove the SecurityGroupHTTPAndSSH group.
- Use the search bar in the Associated security groups section to select the SecurityGroupHTTPOnly security group.
- Click Add security group, and then click Save.

5. Navigate back to your terminal session and reconnect to the EC2 instance using the credentials provided for the lab:

`ssh cloud_user@<PUBLIC-IP-ADDRESS>`    

Again, this will be recorded as a reject record, since you no longer have SSH access.

6. Press Ctrl+C to cancel the SSH command.

7. Navigate back to the EC2 Management Console tab.

8. Revert the EC2 instance security group back to SecurityGroupHTTPAndSSH:

- Ensure the Web Server instance is selected, and then use the Actions dropdown to select Security > Change security groups.
- In the Associated security groups section, click Remove to the right of the security group details to remove the SecurityGroupHTTPOnly group.
- Use the search bar in the Associated security groups section to select the SecurityGroupHTTPAndSSH security group.
- Click Add security group, and then click Save.

9. Navigate back to the CloudWatch Alarms tab and refresh the alarms details.

You should see that the alarm state is now In alarm. If you attached the alarm to your email address, you should receive a notification about this alarm.

> Note: If the alarm state still shows Insufficient data, wait another moment or two and then refresh the alarms details again.


**Use CloudWatch Logs Insights**    

1. In the CloudWatch sidebar menu, navigate to Logs and select Logs Insights.

2. Use the Select log group(s) search bar to select VPCFlowLogs.

3. In the right-hand pane, select Queries.

4. In the Sample queries section, expand VPC Flow Logs and then expand Top 20 source IP addresses with highest number of rejected requests.

5. Click Apply and note the changes applied in the query editor.

6. Click Run query.

After a few moments, you'll see some data start to populate.


**Analyze VPC Flow Logs Data in Athena**    


**Create the Athena Table**    

1. Navigate back to the S3 browser tab and then navigate to your Buckets.

2. Select the provisioned bucket name to open it.

3. Select the AWSLogs/ folder, and then continue opening the subfolders until you reach the <DAY> folder containing the logs.

4. In the top right, click Copy S3 URI.

5. Paste the URI into a text file, as you'll need it shortly.

6. In a new browser tab, navigate to Athena.

7. On the right, click Launch query editor.

8. Select the Settings tab and then click Manage.

9. In the Location of query result field, paste your copied S3 URI.

10. Click Save.


**Create Partitions and Analyze the Data**    

1. Select the query editor's Editor tab.

2. In the Query 1 editor, paste the following query, replacing {your_log_bucket} and {account_id} with your log bucket and account ID details (you can pull these from the S3 URI path you copied):

```bash
CREATE EXTERNAL TABLE IF NOT EXISTS default.vpc_flow_logs (
  version int,
  account string,
  interfaceid string,
  sourceaddress string,
  destinationaddress string,
  sourceport int,
  destinationport int,
  protocol int,
  numpackets int,
  numbytes bigint,
  starttime int,
  endtime int,
  action string,
  logstatus string
)
PARTITIONED BY (dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ' '
LOCATION 's3://{your_log_bucket}/AWSLogs/{account_id}/vpcflowlogs/us-east-1/'
TBLPROPERTIES ("skip.header.line.count"="1");
```

3. Click Run.

You should see a message indicating that the query was successful.

4. On the right, click the + icon to open a new query editor.

5. In the editor, paste the following query, replacing {Year}-{Month}-{Day} with the current date, and replacing the existing location with your copied S3 URI:

```bash
ALTER TABLE default.vpc_flow_logs
    ADD PARTITION (dt='{Year}-{Month}-{Day}')
    location 's3://{your_log_bucket}/AWSLogs/{account_id}/vpcflowlogs/us-east-1/{Year}/{Month}/{Day}/';
```

6. Click Run.

You should see a message indicating that the query was successful.

7. On the right, click the + icon to open a new query editor.

8. In the editor, paste the following query:

```bash
SELECT day_of_week(from_iso8601_timestamp(dt)) AS
     day,
     dt,
     interfaceid,
     sourceaddress,
     destinationport,
     action,
     protocol
   FROM vpc_flow_logs
   WHERE action = 'REJECT' AND protocol = 6
   order by sourceaddress
   LIMIT 100;
```

9. Click Run.

Your partitioned data should display in the query results.









======================================================================================================================= 


<span style="color: #EA9811">

###  **MONITORING - QUIZZ** 
</span>

![monitoring_quizz01](./images/aws_solutionArchitectAssociate/monitoring_quizz01.png)      
![monitoring_quizz02](./images/aws_solutionArchitectAssociate/monitoring_quizz02.png)      
![monitoring_quizz0"](./images/aws_solutionArchitectAssociate/monitoring_quizz03.png)      
![monitoring_quizz04](./images/aws_solutionArchitectAssociate/monitoring_quizz04.png)      



**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/80.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/81.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/82.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/83.png)               






======================================================================================================================= 


_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **HIGH Availability AND SCALING** 
</span>


[Documentation aws: High availability and scalability on aws](https://docs.aws.amazon.com/whitepapers/latest/real-time-communication-on-aws/high-availability-and-scalability-on-aws.html)    


La plupart des fournisseurs (providers) de communication en temps réel s'alignent sur des niveaux de service qui offrent une disponibilité de 99,9 % à 99,999 %. Selon le degré de haute disponibilité (HA) que vous souhaitez, vous devez prendre des mesures de plus en plus sophistiquées tout au long du cycle de vie de l'application    

AWS recommande de suivre ces directives pour atteindre un degré robuste de haute disponibilité:    

- Concevez (Design) le système de sorte qu'il n'y ait aucun point de défaillance unique. Utilisez des mécanismes automatisés de surveillance, de détection des pannes et de basculement pour les composants sans état et avec état
  - Les points de défaillance uniques (**SPOF: Single Spots Of Failure**) sont généralement éliminés avec une configuration de redondance N + 1 ou 2N, où N + 1 est atteint via l'équilibrage de charge entre les nœuds actifs-actifs, et 2N est atteint par une paire de nœuds en configuration active-veille (active-standby pair)    
  - AWS propose plusieurs méthodes pour atteindre la haute disponibilité via les 2 approches, par exemple via un cluster évolutif (scalabla) à charge équilibrée (load balanced) ou en supposant une paire active-en veille    

- Disponibilité correcte des instruments et du système de test    

- Prepare operating procedures for manual mechanisms to respond to, mitigate, and recover from the failure    




======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - HORIZONTAL VS VERTICAL SCALING** 
</span>


**Vertical scaling c'est quoi ?**     
C'est la scaling "historique"     
Cette augmentation a un point où elle ne peut plus se faire (point max de capacité)     
Augmentation cpu, ram, .....




**Horizontal scaling c'est quoi ?**      
On augmente le nombre d'instances plutôt que d'augmenter ses capacités (pas de limites)     
Cela permet aussi d'augmenter la disponibilité (en ajoutant des instances dans différentes AZs)



**3 questions importantes du scaling: les 3 _W_**     
- What do we scale ?        
  ➡️ Il faut décider quelle sorte de ressources va être augmentée      
  ➡️ Comment définir/utiliser un template ?        
- Where do we scale ?     
  ➡️ quand et où appliquer un modèle ?     
  ➡️ devons-nous faire évlouer les database ? les webservers ? ...     
- When do we scale ?        
  ➡️ Comment savoir que nous avons besoin de plus de ressources ?    
  ➡️ CloudWatch (les alarmes) peuvent nous informer qu'il est temps d'ajouter/supprimer des ressources    


Lors de la certification, ce sera la plupart du temps CloudWatch qui nous informe   







======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - WHAT ARE LAUNCH TEMPLATES AND LAUNCH CONFIGURATIONS ?** 
</span>



Il faut savoir que le différences entre les 2 sujets sont définits dans lé 1er W: **What are we scaling ?**


**What are we scaling ?**     
La 1ere chose à faire est de répondre à cette question    
Pour y répondre il faut savoir ce qu'est un **Launch Template**    



**Lauch templates c'est quoi ?**     
[Documentation aws: launch template](https://docs.aws.amazon.com/autoscaling/ec2/userguide/launch-templates.html)    

Un **Launch Template** défini tous les besoins de paramétrage lors de la construction d'une instance    
Cette collection de paramètres permettent la configuration des instances EC2




**Launch configurations c'est quoi ?**
[Documentation aws: launch configuration](https://docs.aws.amazon.com/autoscaling/ec2/userguide/launch-configurations.html)    
C'est une pratique "historique". Ce n'est pas mauvais MAIS utilisé le template est fortement précaunisé





![HA_launchTemplateVsLaucnConfiguration](./images/aws_solutionArchitectAssociate/HA_launchTemplateVsLaucnConfiguration.png)    


>_Remarque_: 
> - Lors de la création d'un Launch Template NE PAS SPECIFIER de VPC ➡️ cela est dépendant de l'instance que nous souhaiterons créer    
> - la partie **user-data** correspond à la partie où nous pouvons mettre un place le bootstrap script d'une instance    



Lorsqu'on crée une instance à partir d'un template ➡️ nous déinissons le VPC    


![HA_launchTemplateAndLaucnConfigurationExamTips01](./images/aws_solutionArchitectAssociate/HA_launchTemplateAndLaucnConfigurationExamTips01.png)     
![HA_launchTemplateAndLaucnConfigurationExamTips02](./images/aws_solutionArchitectAssociate/HA_launchTemplateAndLaucnConfigurationExamTips02.png)    




======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - SCALING EC2 INSTANCES WITH AUTO SCALING** 
</span>

[Documentation aws: EC2 auto scaling FAQs](https://aws.amazon.com/ec2/autoscaling/faqs/)     
[Documentation aws: On-demand instance quotas](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-on-demand-instances.html#ec2-on-demand-instances-limits)    



Dans cette partie nous allons répondre au 2e W: **Where do we scale ?**

**Auto Scalling Groups** répondent à cette question     
Un auto scling group contient une collection d'EC2 instances qui sont considérées un un group collectif dans le but du scaling et du management    

C'est avec l'auto scaling group que le network va être défini ➡️ c'est le **où** 


![HA_autoScalinGroup-etapes](./images/aws_solutionArchitectAssociate/HA_autoScalinGroup-etapes.png)    

![HA_autoScalinGroup-restrictions](./images/aws_solutionArchitectAssociate/HA_autoScalinGroup-restrictions.png)    


>_Remarque_: Il est possible d'utiliser des spots instances avec un auto scaling group ce qui permet d'économiser 


![HA_autoScalinGroup-examTips01](./images/aws_solutionArchitectAssociate/HA_autoScalinGroup-examTips01.png)    
![HA_autoScalinGroup-examTips02](./images/aws_solutionArchitectAssociate/HA_autoScalinGroup-examTips02.png)    










======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - DIVING DEEPER INTO AUTO SCALING POLICIES** 
</span>


Cette partie répond au 3e W: **Quand ?**    




![ha_policies-stepScaling](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling.png)    
![ha_policies-stepScaling02](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling02.png)    
![ha_policies-stepScaling03](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling03.png)    
![ha_policies-stepScaling04](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling04.png)    
![ha_policies-stepScaling05](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling05.png)    
![ha_policies-stepScaling06](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling06.png)    
![ha_policies-stepScaling07](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling07.png)    
![ha_policies-stepScaling08](./images/aws_solutionArchitectAssociate/ha_policies-stepScaling08.png)    


La **WARM-UP** period est une période "longue" le temps que toutes les instances soit complètement démarées et accessible. Le health check et le load balancing sur celles-ci durant cette période n'est pas activé     

La **COOLDOWN** période est elle instantannée.      
Elle met en pause l'Auto Scaling pendant 1 durée définie (par defaut 5 minutes)     
Elle aide à éviter les scaling incontrolables    


Ces 2 périodes permettent d'éviter le **trashing**     
On souhaite avoir des instances rapidement et les éteindre lentement


Récap des 3 W:    
- **What**  ➡️  **Launch template**    
- **Where** ➡️  **Auto Scaling**
- **When**  ➡️  **Scaling Policies**    




![ha_policies-typesScaling](./images/aws_solutionArchitectAssociate/ha_policies-typesScaling.png)    



⚠️    
Nous pouvons nous trouver dans le cas où nous devons 1 SEULE et UNIQUE instance: ex ➡️ cas d'un "vieux" service legacy
Dans ce cas nous créons 1 instance avec le min à 1 instance, le max à 1 instance, le desired à 1 instance    
Cela avec un group AutoScaling qui a plusieurs AZs ➡️ comme cela si 1 AZ est innacessible le service sera toujours rendu (le temps de redémarrage de l'instance)    
⚠️ 



![ha_policies-examTips02](./images/aws_solutionArchitectAssociate/ha_policies-examTips02.png)    
![ha_policies-examTips](./images/aws_solutionArchitectAssociate/ha_policies-examTips.png)    


======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - SCALING RELATIONNAL DATABASES** 
</span>



![ha_rds](./images/aws_solutionArchitectAssociate/ha_rds.png)    


>_Remarque_: à vérifier si le nombre de read replicas est 15 ou 50 .....


![ha_rds-examTips](./images/aws_solutionArchitectAssociate/ha_rds-examTips.png)    
![ha_rds-examTips02](./images/aws_solutionArchitectAssociate/ha_rds-examTips02.png)    



======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - SCALING NON-RELATIONNAL DATABASES** 
</span>


On parle ici de DynamoDB    

![ha_dynamoDb_scalingOptions](./images/aws_solutionArchitectAssociate/ha_dynamoDb_scalingOptions.png)    


![ha_dynamoDb_examTips01](./images/aws_solutionArchitectAssociate/ha_dynamoDb_examTips01.png)    
![ha_dynamoDb_examTips02](./images/aws_solutionArchitectAssociate/ha_dynamoDb_examTips02.png)   




======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - HIGH AVAILABILITY AND SCALING EXAM TIPS** 
</span>


[Documentation aws: Disaster Recovery options in the cloud](https://docs.aws.amazon.com/whitepapers/latest/disaster-recovery-workloads-on-aws/disaster-recovery-options-in-the-cloud.html)    


![ha_dynamoDb_examTips03](./images/aws_solutionArchitectAssociate/ha_dynamoDb_examTips03.png)    
![ha_dynamoDb_examTips04](./images/aws_solutionArchitectAssociate/ha_dynamoDb_examTips04.png)    
![ha_dynamoDb_examTips05](./images/aws_solutionArchitectAssociate/ha_dynamoDb_examTips05.png)    
![ha_dynamoDb_examTips06](./images/aws_solutionArchitectAssociate/ha_dynamoDb_examTips06.png)    






======================================================================================================================= 


<span style="color: #EA9811">

###  **HA - HIGH AVAILABILITY AND SCALING QUIZZ** 
</span>



![ha_quizz01](./images/aws_solutionArchitectAssociate/ha_quizz01.png)     
![ha_quizz02](./images/aws_solutionArchitectAssociate/ha_quizz02.png) 
![ha_quizz03](./images/aws_solutionArchitectAssociate/ha_quizz03.png) 
![ha_quizz04](./images/aws_solutionArchitectAssociate/ha_quizz04.png) 
![ha_quizz05](./images/aws_solutionArchitectAssociate/ha_quizz05.png) 




**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/84.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/85.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/86.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/87.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/88.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/89.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/90.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/91.png)               
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/92.png)               




======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **DECOUPLING WORKFLOWS** 
</span>


[Documentation aws: applications decoupling patterns](https://docs.aws.amazon.com/fr_fr/prescriptive-guidance/latest/modernization-mainframe-decoupling-patterns/shared.html)    







======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - DECOUPLING WORKFLOW OVERVIEW** 
</span>


Le but de ce chapitre est de découpler convenablement les flux entre les users / frontend / backend pour toujours avoir une disponibilité même dans le cas de perte de ressources  ➡️ on parle de **load balancers (ELB)**     

Des outils sont arrivés avec aws pour aider à la mise en place de cette architecture:    
- **SQS (Simple Queue Service)**: c'est un service de file d'attente de messages entièrement gérés qui vous permet de découpler et de mettre à l'échelle les microservices, les systèmes distribués et les applications sans serveur (serverless applications)    
- **SNS (Simple Notification Service)**: c'est un service de file d'attente de messages entièrement gérés pour les communications d'applications vers applications et d'applications vers les personnes (sms ou email)      
- **API Gateway**: c'est un service entièrement géré qui facilite pour les developpeurs la creation, publication, maintenance, monitoring et securisation des APIs quelquesoit leur échelle    


![workflow_overview-examTips01](./images/aws_solutionArchitectAssociate/workflow_overview-examTips01.png)      
![workflow_overview-examTips02](./images/aws_solutionArchitectAssociate/workflow_overview-examTips02.png)      





======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - MESSAGING WITH SQS** 
</span>


[Documentation aws: sqs encryption keys (SSE-SQS)](https://aws.amazon.com/blogs/compute/announcing-server-side-encryption-with-amazon-simple-queue-service-managed-encryption-keys-sse-sqs-by-default/)    


[Documentation aws: SQS](https://docs.aws.amazon.com/fr_fr/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html)    

Amazon Simple Queue Service (Amazon SQS) offre une file d'attente hébergée sécurisée, durable et disponible qui vous permet d'intégrer et de découpler les systèmes et les composants de logiciels distribués.     
Amazon SQS propose des concepts courants tels que des files d'attente contenant des lettres mortes et des balises de répartition des coûts.     
Il fournit une API de services Web générique à laquelle vous pouvez accéder à l'aide de n'importe quel langage de programmation pris en charge par le AWS SDK.    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SQS : POLL-BASED MESSAGING** 
</span>


[Documentation AWS: SQS short and long polling](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-short-and-long-polling.html)     


_comparaison_:     
> J'écris une lettre à un ami, je la mets dans 1 enveloppe que je timbre, je la poste, le facteur récupère la lettre l'emmène jusq'à la boite aux lettres de mon ami, mon ami récupère la lettre, ouvre l'enveloppe, lit la lettre    
> **Ceci correspond à un poll-based messaging**     
> Une instance crée et envoie un message à une autre instance qui réceptionne ce message et le traite          
> **SQS** est l'équivalent du facteur    






........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SQS : WHAT IS SQS ?** 
</span>


**SQS**: **Simple Queue Service** ➡️ file d'attente de messagerie qui permet le traitement asynchrone    





........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SQS : IMPORTANT SETTINGS** 
</span>


Pour que le service fonctionne correctement il y a une configuration à faire    
1. **Delivery delay** ➡️ par default 0 ➡️ peut être jusqu'à 15 minutes      
2. **Message size**  ➡️ peut aller jusqu'à que 256 Kb dans tous les formats    
3. **Encryption**    ➡️ les messages sont encryptés par defaut lors du transist     

![workflow_sqs-settings](./images/aws_solutionArchitectAssociate/workflow_sqs-settings.png)     



4. **Message Retention** ➡️ default: 4 jours  ➡️ entre 1 minute et 14 jours    
5. **Long vs Short** ➡️ les _polling_ (demande si messages dans la _queue_) peuvent long ou court  ➡️ il est conseillé de faire des longs: les courts vont se connecter, faire un appel API, si rien se déconnecter et recommencer régulièrement ➡️ cela prend de la ressource CPU ET le nombre d'appels API est payant.....   
Un **long polling** va faire un appel API est va attendre qu'un appel arrive  ➡️ ce n'est pas par defaut donc conseiller de le mettre en place     


6. **Queue depht** ➡️ cela peut être autoscaler par trigger      


>_Remarque_: les questions sur SQS arrivent très souvent lors de la certification ➡️ **IMPORTANT**     






........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SQS : VISIBILITY TIMEOUT** 
</span>


![workflow_sqs-timeout](./images/aws_solutionArchitectAssociate/workflow_sqs-timeout.png)     




........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SQS : EXAM TIPS** 
</span>


![workflow_sqs-examTips01](./images/aws_solutionArchitectAssociate/workflow_sqs-examTips01.png)     
![workflow_sqs-examTips02](./images/aws_solutionArchitectAssociate/workflow_sqs-examTips02.png)    




........................................................................................................................


======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - SIDELING MESSAGES WITH DEAD-LETTER QUEUES** 
</span>

[Documentation aws: Sead Letter queue](https://docs.aws.amazon.com/fr_fr/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-dead-letter-queues.html)     


Amazon SQS prend en charge les files d'attente de lettres mortes (DLQ), que d'autres files d'attente (files d'attente sources) peuvent cibler pour les messages qui ne peuvent pas être traités (consommés) correctement.     
Les files d'attente de lettres mortes sont utiles pour déboguer votre application ou votre système de messagerie, car elles vous permettent d'isoler les messages non utilisés afin de déterminer pourquoi leur traitement échoue.    




Un **dead-letter queues** est un message "mal renseigné" avec des informations non vérifiées lors de la construction du message.    
Ce message sera alors récupérer par l'instance du backend qui tombera en failed sur le message     
Ce message sera donc remis en place dans le queue    
Il sera à nouveau traité en erreur ➡️ remis dans la queue........  
➡️ cela peut aller jusqu'à perdre la donnée qui est dans le message     


_exemple de mauvais renseignements_: dans 1 champ d'adresse le num de tel est renseigné     


Comment ne pas perdre la donnée du message ?      

![workflow_sqs-dql](./images/aws_solutionArchitectAssociate/workflow_sqs-dql.png)    


Pour pouvoir créer une **Dead Letter Queue** il faut avoir créer une **queue** précédement

Dans la `console AWS / Amazon SQS / Create queue`       
Ensuite on peut créer une nouvelle queue et la definir en tant que **dead letter queue** (coche _enable_)      
  ➡️ on doit indiquer l'arn de la queue précédement créée (si on veut la créer pour ses messages en erreur)     
  ➡️ ET définir au bout de combien d'erreurs le message part dans cette DLQ (nombre de fois où le message va être traité et retombe en failed)   ➡️ **`Maximum receives`** (par defaut à 10)     

>_Remarque_: la 1ere queue créée sera celle qui fera office de DLQ puisque nous indiquons sont arn dans le 2e ....
> La 2e fera donc office de queue "princiaple".......      


![workflow_sqs-dql-examTips01](./images/aws_solutionArchitectAssociate/workflow_sqs-dql-examTips01.png)    
![workflow_sqs-dql-examTips02](./images/aws_solutionArchitectAssociate/workflow_sqs-dql-examTips02.png)     







======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - ORDERED MESSAGES WITH SQS FIFO** 
</span>

[Documentation aws: SQS FIFO](https://docs.aws.amazon.com/fr_fr/AWSSimpleQueueService/latest/SQSDeveloperGuide/FIFO-queues.html)    

Les files d'attente FIFO (First-In-First-Out) possèdent toutes les fonctionnalités des files d'attente standard, mais elles sont conçues pour améliorer la messagerie entre les applications lorsque l'ordre des opérations et des événements est critique ou lorsque les doublons ne sont pas tolérés.


![workflow_sqs-fifo](./images/aws_solutionArchitectAssociate/workflow_sqs-fifo.png)    

![workflow_sqs-fifoDifference](./images/aws_solutionArchitectAssociate/workflow_sqs-fifoDifference.png)             



![workflow_sqs-fifo-examTips01](./images/aws_solutionArchitectAssociate/workflow_sqs-fifo-examTips01.png)    
![workflow_sqs-fifo-examTips02](./images/aws_solutionArchitectAssociate/workflow_sqs-fifo-examTips02.png)      

======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - DELIVRING MESSAGES WITH SNS** 
</span>


[Documentation aws: sns](https://docs.aws.amazon.com/sns/latest/dg/sns-message-delivery-retries.html)    
[Documentation aws: sns FAQs](https://aws.amazon.com/sns/faqs/)    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SNS : PUSH-BASED MESSAGING** 
</span>


Pour différencier avec le **poll-based messaging**, le **pull-based messaging** fera dans la même comparaison que le facteur récupère la lettre, vient sonner à la porte, rentre dans la maison et ouvre la lettre pour le destinataire   ➡️ **le destinataire DOIT être prêt en permanence à recevoir un message** ➡️ on ne choisit pas quand réceptionner les messages 






........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SNS : WHAT IS SNS ?** 
</span>

Amazon **Simple Notification Service (SNS)** envoie des notifications de deux manières : A2A et A2P. A2A fournit une messagerie à haut débit, basée sur un système « push », entre des systèmes distribués, des microservices et des applications sans serveur pilotées par les événements



**SNS est un push-based messaging service**.     
Les messages sont délivrés proactivement à des **endpoints** auxquels on a souscrit     
Cela peut être utilisé dans le cas d'alertes (systeme ou personne)     




........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SNS : IMPORTANT SETTINGS** 
</span>


![workflow-sns-settings](./images/aws_solutionArchitectAssociate/workflow-sns-settings.png)    








........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - SNS : EXAM TIPS** 
</span>




![workflow-sns-examTips01](./images/aws_solutionArchitectAssociate/workflow-sns-examTips01.png)      
![workflow-sns-examTips02](./images/aws_solutionArchitectAssociate/workflow-sns-examTips02.png)      



........................................................................................................................

======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - FRONTING APPLICATIONS WITH API GATEWAY** 
</span>


........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - APiGW** 
</span>


[Documentation aws: API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html)    

Amazon API Gateway est un service AWS qui permet de créer, de publier, de gérer, de surveiller et de sécuriser les API REST, HTTP et WebSocket à n'importe quelle échelle     

Les développeurs d'API peuvent créer des API qui accèdent à AWS ou à d'autres services web, ainsi qu'aux données stockées dans le [cloud AWS](http://aws.amazon.com/what-is-cloud-computing/)     

API Gateway crée des API RESTful qui :    
- reposent sur le protocole HTTP ;    
- permettent la communication client-serveur sans état ;   
- mettent en œuvre les méthodes HTTP standard, telles que GET, POST, PUT, PATCH et DELETE.   

API Gateway crée des API WebSocket qui :     
- respectent le [protocole WebSocket](https://tools.ietf.org/html/rfc6455), ce qui permet une communication avec état en duplex intégral entre le client et le serveur ;    
- acheminent les messages entrants reposant sur le contenu du message    


![apigw_architecture](./images/aws_solutionArchitectAssociate/apigw_architecture.png)     



![apigw_fonctionnementHautNiveau](./images/aws_solutionArchitectAssociate/apigw_fonctionnementHautNiveau.png)    


![apigw_videoFormationFonctionnement](./images/aws_solutionArchitectAssociate/apigw_videoFormationFonctionnement.png)    






![apigw_examTips01](./images/aws_solutionArchitectAssociate/apigw_examTips01.png)    
![apigw_examTips02](./images/aws_solutionArchitectAssociate/apigw_examTips02.png)    



........................................................................................................................


======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - EXECUTING BATCH WORKLOADS USING AWS BATCH** 
</span>


[Documentation aws: quand utiliser Fargate](https://docs.aws.amazon.com/batch/latest/userguide/fargate.html#when-to-use-fargate)     
[Documentation aws: batchs FAQs](https://aws.amazon.com/batch/faqs/)
[Documentation aws: aws fargate](https://aws.amazon.com/fr/fargate/)    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS Batch** 
</span>


1. Un **batch aws** est un service qui permet d'exécuter des **batch computing workloads** dans les environnements AWS (sur EC2, ECS/Fargate)   

  [**aws ECS**: **Elastic Conatiner Service**](https://aws.amazon.com/fr/ecs/)     
  Exécutez des conteneurs hautement sécurisés, fiables et évolutifs        


  [**aws Fargate**](https://aws.amazon.com/fr/fargate/)     
  Calcul sans serveur pour conteneurs     


2. Le batch facilite la vie ➡️ il **supprime les configurations et management lourds des infra** nécessaire pour la capacité de calculs      


3. Permet un **scale automatique** en fonction du nombre de jobs en cours (plus précis dans la définition des ressources) ➡️ permet une optimisation de la distribution des workloads    
  ➡️ gain d'argent car si non nécessaire : non utilisé


4. **Ne nécessite aucune installation**  ➡️ permet d'éviter l'installation et la maintenance du batch computing software ➡️ permet un focus sur l'obtention et l'analyse des résultats    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS Batch - Elements importants** 
</span>



Il y en a 4 :     
![workflow-awsBatch-Components](./images/aws_solutionArchitectAssociate/workflow-awsBatch-Components.png)     

........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS Batch - Fargate ou EC2 environments** 
</span>



![workflow-awsBatch-Ec2VsFargate](./images/aws_solutionArchitectAssociate/workflow-awsBatch-Ec2VsFargate.png)    




........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS Batch - AWS Batch ou AWS Lambda** 
</span>


[Documentation aws: AWS Lambda](https://aws.amazon.com/fr/lambda/)    
Exécuter du code sans vous soucier des serveurs ou des clusters      

[Documentation aws: Lambda quotas](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/gettingstarted-limits.html)    


Choisir entre Aws Batch et AWs Lambda:    

1. **Limite de temps**:  **AWS Lambda** a une exécution qui **ne peut pas dépasser les 15 minutes**      


2. **Espace disque**: 
 - **AWS Lambda** a une limite ➡️ 75 Go   
 - pour une extension d'espace disque **EFS** nécessite des fonctions dans 1 VPC qui sont difficiles à manager    


3. Limitation du temps d'exécution: Lambda est **serverless** mais a des limites de temps d'exécution


4. AWS Batch runtimes: batch utilise docker ➡️ n'importe quel runtime peut être utilisé    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS Batch - MANAGED VS UNMANAGED COMPUTE ENVIRONMENTS** 
</span>



![workflow-awsBatch-manageVsUnmanagedComputeEnv](./images/aws_solutionArchitectAssociate/workflow-awsBatch-manageVsUnmanagedComputeEnv.png)    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS Batch - EXAM TIPS** 
</span>



![workflow-awsBatch-examTips](./images/aws_solutionArchitectAssociate/workflow-awsBatch-examTips.png)    




........................................................................................................................


======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - BROKERING MESSAGES WITH AMAZON MQ** 
</span>


[Documentation aws: amazon MQ FAQs](https://aws.amazon.com/amazon-mq/faqs/)    
[Documentation aws: amazon MQ features](https://aws.amazon.com/amazon-mq/features/)    
[Documentation aws: activeMQ](https://aws.amazon.com/amazon-mq/features/#Industry-standard_APIs_and_Protocols)    






........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS MQ - OVERVIEW** 
</span>


[Documentation aws - Amazon MQ](https://aws.amazon.com/fr/amazon-mq/)     
Service entièrement géré pour agents de messages en open source      
- Migrez vos applications plus facilement grâce à des endpoints mis à jour et à des protocoles et API conformes aux normes de l'industrie    
- Déchargez-vous des tâches fastidieuses telles que l'administration, les opérations de maintenance et la gestion de la sécurité pour les agents de messages    
- Assurez la haute disponibilité de vos applications et la durabilité des messages à travers les zones de disponibilité AWS    


Les agents de messages permettent aux systèmes logiciels, qui utilisent souvent différents langages de programmation sur diverses plateformes, de communiquer et d'échanger des informations.     
Amazon MQ est un service d'agent de messages géré pour Apache ActiveMQ et RabbitMQ qui simplifie la configuration, l'exploitation et la gestion des agents de messages sur AWS.     
En quelques étapes, Amazon MQ peut permettre à votre agent de messages de prendre en charge les mises à jour de versions logicielle     


![workflow-mq_schema](./images/aws_solutionArchitectAssociate/workflow-mq_schema.png)    




Les agents de messages (ou message brokers en anglais) sont des modules intermédiaires qui permettent la communication entre différents éléments informatiques (applications, systèmes, services, ...).      
Un agent de messages peut valider, stocker, acheminer et délivrer des messages aux destinataires appropriés     


![workflow-mq_overview](./images/aws_solutionArchitectAssociate/workflow-mq_overview.png)    




Le message broker assure la gestion de l'état et le suivi des clients, de sorte que les applications individuelles n'ont pas à assumer cette responsabilité et que la complexité de la livraison des messages est intégrée au message broker lui-même




........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS MQ - SNS/SQS vs MQ** 
</span>



![workflow-mq_snsVsMq](./images/aws_solutionArchitectAssociate/workflow-mq_snsVsMq.png)    







........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS MQ - BROKERS** 
</span>




![workflow-mq_brokers](./images/aws_solutionArchitectAssociate/workflow-mq_brokers.png)    





........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - AWS MQ - EXAM TIPS** 
</span>




![workflow-mq_examTips](./images/aws_solutionArchitectAssociate/workflow-mq_examTips.png)    





........................................................................................................................

======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - COORDINATING DISTRIBUTED APPS WITH AWS STEP FUNCTIONS** 
</span>


[Documentation aws - AWS Step Functions](https://aws.amazon.com/fr/step-functions/?nc1=h_ls)     
Flux de travail visuels pour des applications distribuées     


AWS Step Functions est un service de flux visuels qui permet aux développeurs qui utilisent les services AWS de créer des applications distribuées, d'automatiser les processus, d'orchestrer des microservices et de créer des pipelines de données et de machine learning (ML).     


![workflow-StepFunctions-schema](./images/aws_solutionArchitectAssociate/workflow-StepFunctions-schema.png)     






........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - OVERVIEW** 
</span>



[Documentation aws - aws state machine](https://docs.aws.amazon.com/fr_fr/step-functions/latest/dg/amazon-states-language-state-machine-structure.html)    


Une state machine est définie dans un json   
_exemple_:    

```json
{
  "Comment": "A Hello World example of the Amazon States Language using a Pass state",    ## optionnel - description lisible de l'état de la machine
  "StartAt": "HelloWorld",        ## obligatoire - Une chaîne qui doit correspondre exactement (sensible à la casse) au nom de l'un des objets d'état    
  "States": {                     ## obligatoire - Objet contenant un ensemble d'états séparés par des virgules
    "HelloWorld": {
      "Type": "Pass",
      "Result": "Hello World!",
      "End": true
    }
  }
}
```

Lorsqu'une exécution de cette machine d'état est lancée, le système commence par l'état référencé dans le champ `StartAt`     
Si cet état a un champ `"End": true` ➡️ l'exécution s'arrête et renvoie un résultat     
Dans le cas contraire, le système recherche un champ `"Next":` et continue avec cet état      
Ce processus se répète jusqu'à ce que le système atteigne un état terminal (un état avec `"Type": "Succeed"`, `"Type": "Fail"` ou `"End": true`) ou jusqu'à ce qu'une erreur d'exécution se produise.      



![workflow-StepFunctions-overview](./images/aws_solutionArchitectAssociate/workflow-StepFunctions-overview.png)    



........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - EXECUTIONS** 
</span>


Il existe 2 types de workflow pour AWS Step Functions:    
- Standard     
- Express    





........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - WORKFLOWS** 
</span>


![workflow-StepFunctions-workflow](./images/aws_solutionArchitectAssociate/workflow-StepFunctions-workflow.png)     




........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - STATES AND STATES MACHINES** 
</span>



![workflow-StepFunctions-state](./images/aws_solutionArchitectAssociate/workflow-StepFunctions-state.png)    







........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - INTEGRATED AWS SERVICES** 
</span>



Enormément de services AWS peuvent être intégrés ➡️ les services intégrés seront donc vraiment dépendant de nos besoins    







........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - DIFFERENTS STATES** 
</span>



![workflow-StepFunctions-differentsStates](./images/aws_solutionArchitectAssociate/workflow-StepFunctions-differentsStates.png)    


[Documentation aws - Differents states](C:\Users\t0284237\Downloads\images\workflow-StepFunctions-differentsStates.png)    





........................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - STEP FUNCTIONS - EXAM TIPS** 
</span>


![workflow-StepFunctions-examTips](./images/aws_solutionArchitectAssociate/workflow-StepFunctions-examTips.png)    




........................................................................................................................

======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - INGESTING DATA FROM SAAS APPLICATIONS TO AWS WITH AMAZON APPFLOW** 
</span>


[Documentation aws - Amazon AppFlow](https://aws.amazon.com/fr/appflow/)     
Les données automatisées circulent entre les logiciels en tant que service (SaaS) et les services AWS    

- Service d'intégration entièrement géré pour transférer des données entre les services tels que Salesforce, SAP, Google Analytics et Amazon Redshift    
- Transférer des données à échelle sans avoir besoin de provisionner les ressources système     
- Automatiser le catalogage de vos données pour découvrir et partager à travers les analyses AWS et les services de machine learning, tels que SageMaker Data Wrangler      
- Simplifier et automatiser la préparation de données avec des transformations, des partitions et des agrégations    


Automatisez les flux de données bidirectionnels entre les applications SaaS et les services AWS en seulement quelques clics avec Amazon AppFlow.     
Exécutez les flux de données à presque n'importe quelle échelle et à la fréquence que vous choisissez, selon un calendrier, en réponse à un événement professionnel ou à la demande.     
Simplifiez la préparation de données avec des transformations, des partitions et des agrégations.     
Automatisez la préparation et le comptage de votre schéma avec le catalogue de données AWS Glue, afin de découvrir et de partager des données avec les services d'analyse et de machine learning d'AWS     



![worlflow_appflow-schema](./images/aws_solutionArchitectAssociate/worlflow_appflow-schema.png)    



.......................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - APPFLOW - OVERVIEW** 
</span>


![worlflow_appflow-overview](./images/aws_solutionArchitectAssociate/worlflow_appflow-overview.png)    




.......................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - APPFLOW - TERMS AND CONCEPTS** 
</span>


![worlflow_appflow-terms](./images/aws_solutionArchitectAssociate/worlflow_appflow-terms.png)    





.......................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - APPFLOW - USES CASES** 
</span>


![worlflow_appflow-schemaUseCase](./images/aws_solutionArchitectAssociate/worlflow_appflow-schemaUseCase.png)     


.......................................................................................................................

<span style="color: #11E3EA">

####  **WORKFLOW - APPFLOW - EXAM TIPS** 
</span>



![worlflow_appflow-examTips](./images/aws_solutionArchitectAssociate/worlflow_appflow-examTips.png) 





======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - DECOUPLING WORKFLOWS EXAM TIPS** 
</span>


[Documentation aws: messages asynchrones](https://docs.aws.amazon.com/whitepapers/latest/microservices-on-aws/asynchronous-messaging-and-event-passing.html)    



![workflow-examTips01](./images/aws_solutionArchitectAssociate/workflow-examTips01.png)    
![workflow-examTips02](./images/aws_solutionArchitectAssociate/workflow-examTips02.png)    
![workflow-examTips03](./images/aws_solutionArchitectAssociate/workflow-examTips03.png)    
![workflow-examTips04](./images/aws_solutionArchitectAssociate/workflow-examTips04.png)    
![workflow-examTips05](./images/aws_solutionArchitectAssociate/workflow-examTips05.png)    
![workflow-examTips06](./images/aws_solutionArchitectAssociate/workflow-examTips06.png)    
![workflow-examTips07](./images/aws_solutionArchitectAssociate/workflow-examTips07.png)    






======================================================================================================================= 


<span style="color: #EA9811">

###  **WORKFLOW - QUIZZ** 
</span>



![workflow_quizz01](./images/aws_solutionArchitectAssociate/workflow_quizz01.png)    
![workflow_quizz02](./images/aws_solutionArchitectAssociate/workflow_quizz02.png)    
![workflow_quizz03](./images/aws_solutionArchitectAssociate/workflow_quizz03.png)    
![workflow_quizz04](./images/aws_solutionArchitectAssociate/workflow_quizz04.png)    
![workflow_quizz05](./images/aws_solutionArchitectAssociate/workflow_quizz05.png)    




**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/93.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/94.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/95.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/96.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/97.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/98.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/99.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/100.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/101.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/102.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/103.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/104.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/105.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/106.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/107.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/108.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/109.png)           
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/110.png)           









======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **BIG DATA** 
</span>




======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - EXPLORING LARGE REDSHIFT DATABASES** 
</span>


[Documentation aws - Amazon redshift](https://aws.amazon.com/redshift/)        
[Documentation aws - Amazon redshift FAQs](https://aws.amazon.com/redshift/faqs/)        



.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - THE 3 V's OF BIG DATA** 
</span>


- **Volume**     
    ➡️ terrabytes ou petabytes       
    ➡️ énormément de data sont stockées et conservées         

- **Variety**     
    ➡️ toutes les datas viennent de différentes sources et dans différents formats    


- **Velocity**             
    ➡️ ce travail nécessite une grande vitesse    
    ➡️ les data doivent être collectées, stockées, traitées et analysées en 1 très courte période     



.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - REDSHIFT** 
</span>


Meilleur rapport qualité-prix pour l'entreposage de données dans le cloud    

Amazon Redshift utilise SQL pour analyser les données structurées et semi-structurées à travers les entrepôts des données, les bases de données opérationnelles et les lacs de données         
➡️ la solution utilise un matériel conçu par AWS et le machine learning pour offrir le meilleur rendement réel, quelle que soit l'échelle    


![bigData-redshift-schema](./images/aws_solutionArchitectAssociate/bigData-redshift-schema.png)    


Redshith est un service complètement managé data warehouse petabyte scale     
C'est un Db relationnelle très grande  ➡️ RDS énorme        





.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - REDSHIFT USES** 
</span>



![bigdata-redshith_uses](./images/aws_solutionArchitectAssociate/bigdata-redshith_uses.png)     



Redshift est un cluster ➡️ lors de sa création il faut définir le nombre de noeuds que ce cluster va avoir   
Redshift n'est que dans 1 seule AZ ➡️ ce n'est pas un service High Availibilty    
  ➡️ pour avoir plusieurs AZ ➡️ il faut créer de multiples copies du cluster    

Depuis la console se trouve une zone de requêtes SQL d'où on peut les exécuter directement    





.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - REDSHIFT - EXAM TIPS** 
</span>



![bigdata-redshith_examTips01](./images/aws_solutionArchitectAssociate/bigdata-redshith_examTips01.png)    
![bigdata-redshith_examTips02](./images/aws_solutionArchitectAssociate/bigdata-redshith_examTips02.png)    




.......................................................................................................................    


======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - PROCESSING DATA WITH EMR (ELASTIC MAP REDUCE)** 
</span>


[Documentation aws - EMR](https://aws.amazon.com/fr/emr/)      
Exécutez et mettez à l'échelle facilement Apache Spark, Hive, Presto et d'autres applications Big Data    




.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - EMR - ETL** 
</span>


**ETL**: **Extract-Transform-Load** est une technologie informatique intergicielle permettant d'effectuer des synchronisations massives d'informations d'une source de données vers une autre      

![bigData-emr-etl](./images/aws_solutionArchitectAssociate/bigData-emr-etl.png)     




.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - EMR** 
</span>


Amazon EMR est la meilleure solution de big data cloud dans le secteur du traitement des données à l'échelle pétaoctet, l'analyse interactive et le machine learning à l'aide de cadres open source, tels que [Apache Spark](https://aws.amazon.com/fr/emr/features/spark/), [Apache Hive](https://aws.amazon.com/fr/emr/features/hive/) et [Presto](https://aws.amazon.com/fr/emr/features/presto/).       




![bigData-emr-schema](./images/aws_solutionArchitectAssociate/bigData-emr-schema.png)    


➡️ c'est l'ETL de AWS    


![bigData-emr-architecture](./images/aws_solutionArchitectAssociate/bigData-emr-architecture.png)    


EMR est un cluster ➡️ ici aussi on définit le nombre de noeuds à sa création    

⚠️ Ces noeuds du cluster correspondent à des instances EC2 ➡️ si dans la console AWS on va dans la partie instance EC2 on verra les instances de ce cluster comme si elles sont des instances "séparées" ➡️ on ne sait qu'elles sont celles du cluster....   
  ➡️ ne pas les supprimer parce qu'on ne sait pas ce qu'elles sont      
  ➡️ lors de la facturation ➡️ ce cluster sera facturé comme des instances "uniques"   ➡️ pas de lien   










.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - EMR - EXAM TIPS** 
</span>




![bigData-emr-examTips01](./images/aws_solutionArchitectAssociate/bigData-emr-examTips01.png)    
![bigData-emr-examTips02](./images/aws_solutionArchitectAssociate/bigData-emr-examTips02.png)    








.......................................................................................................................     




======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - STREAMING DATA WITH KINESIS** 
</span>


[Documentation aws - kinesis](https://aws.amazon.com/fr/kinesis/)     
Collectez, traitez et analysez facilement les flux vidéo et de données en temps réel    

Amazon Kinesis facilite la collecte, le traitement et l'analyse de données en streaming en temps réel, afin d'obtenir rapidement des informations stratégiques et de réagir rapidement.      
Amazon Kinesis offre des capacités cruciales pour traiter de façon économique des données en streaming à n'importe quelle échelle, ainsi que la possibilité de choisir les outils les mieux adaptés aux besoins de votre application     
Avec Amazon Kinesis, vous pouvez intégrer des données en temps réel, comme de la vidéo, de l'audio, des journaux d'applications, des flux de clics sur le site web et des données de télémétrie IoT pour l'apprentissage automatique, l'analyse et d'autres applications      
Amazon Kinesis permet de traiter et d'analyser des données à mesure de leur réception et de réagir instantanément au lieu d'attendre que toutes les données soient collectées pour démarrer leur traitement     




![bigData-kinesis-versions](./images/aws_solutionArchitectAssociate/bigData-kinesis-versions.png)    
  


.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - KINESIS - DATA STREAMS** 
</span>


![bigData-kinesis-archi](./images/aws_solutionArchitectAssociate/bigData-kinesis-archi.png)    







.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - KINESIS - DATA FIREHOSE** 
</span>



![bigData-kinesis-firehose](./images/aws_solutionArchitectAssociate/bigData-kinesis-firehose.png)    






.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - KINESIS - DATA ANLYTICS AND SQL** 
</span>



![bigData-kinesis-analysis](./images/aws_solutionArchitectAssociate/bigData-kinesis-analysis.png)    





.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - KINESIS - KINESIS VS SQS** 
</span>


![bigData-kinesis-vsSqs](./images/aws_solutionArchitectAssociate/bigData-kinesis-vsSqs.png)     








.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - KINESIS - EXAM TIPS** 
</span>



![bigData-kinesis-examTips01](./images/aws_solutionArchitectAssociate/bigData-kinesis-examTips01.png)    
![bigData-kinesis-examTips02](./images/aws_solutionArchitectAssociate/bigData-kinesis-examTips02.png)    






.......................................................................................................................    





======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - AMAZON ATHENA AND AWS GLUE** 
</span>


[Documentation aws - Athena](https://aws.amazon.com/fr/athena/)     
[Documentation aws - AWS Glue](https://aws.amazon.com/fr/glue/)     
[Documentation aws - Amazon quickSight](https://aws.amazon.com/fr/quicksight/features/)    






.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - ATHENA** 
</span>

   
Analyser des pétaoctets de données là où elles se trouvent, de manière simple et flexible     

Amazon Athena est un service d'analytique sans serveur et interactif basé sur des cadres open source, prenant en charge les formats de table et de fichier ouverts     
Analysez des données ou créez des applications à partir d'un lac de données Amazon Simple Storage Service (S3) et plus de 25 sources de données, dont des sources de données sur site ou d'autres systèmes cloud à l'aide de SQL ou de Python    
Athena est un service conçu sur les moteurs open source [Trino](https://trino.io/) et Presto et sur les cadres Apache Spark. Il ne nécessite pas d'approvisionnement ni de configuration


![bigData-athena-schema](./images/aws_solutionArchitectAssociate/bigData-athena-schema.png)    










.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - GLUE** 
</span>

    
Découvrez, préparez et intégrez toutes vos données à n'importe quelle échelle    

AWS Glue est un service d'intégration des données sans serveur qui facilite la découverte, la préparation, le déplacement et l'intégration des données depuis des sources multiples pour l'analytique, le machine learning (ML) et le développement des applications.    






**ELT**: **Extract, Load, Transform**    
  ➡️ extrait des données à partir d’une ou plusieurs sources distantes, mais les charge ensuite dans le datawarehous cible sans changement de format. Dans un processus ELT, la transformation des données s’effectue au sein de la base de données cible. L’ELT nécessite moins de sources distantes, uniquement leurs données brutes et non préparées.









.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - ATHENA/GLUE - USING TOGETHER** 
</span>



![bigData-athenaGlue-archi](./images/aws_solutionArchitectAssociate/bigData-athenaGlue-archi.png)     





**Amazon QuickSight**:    
Service permettant de créer et partager des dashboards qui sont scale pour des centaines de milliers d'utilisateurs depuis n'importe quel device     





.......................................................................................................................

<span style="color: #11E3EA">

####  **BIGDATA - ATHENA/GLUE - EXAM TIPS** 
</span>



![bigData-athenaGlue-examTips01](./images/aws_solutionArchitectAssociate/bigData-athenaGlue-examTips01.png)    
![bigData-athenaGlue-examTips02](./images/aws_solutionArchitectAssociate/bigData-athenaGlue-examTips02.png)    



.......................................................................................................................



======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - VISUALIZING DATA WITH QUICKSIGHT** 
</span>

[Documentation aws - quickSight](https://aws.amazon.com/fr/quicksight/)    
Amazon QuickSight permet à tous les membres de votre organisation de comprendre vos données en posant des questions en langage naturel, en explorant via des tableaux de bord interactif, ou en recherchant automatiquement des modèles et des anomalies alimentés par le machine learning     


Amazon QuickSight dispose d'une architecture serverless qui s'adapte automatiquement à des centaines de milliers d'utilisateurs sans qu'il soit nécessaire d'installer, de configurer ou de gérer vos propres serveurs     
Cela garantit également que vos utilisateurs n'ont pas à gérer des tableaux de bord lents pendant les heures de pointe, lorsque plusieurs utilisateurs de Business Intelligence (BI) accèdent aux mêmes tableaux de bord ou ensembles de données.     


![bigData-quickSight-examTips01](./images/aws_solutionArchitectAssociate/bigData-quickSight-examTips01.png)     
![bigData-quickSight-examTips02](./images/aws_solutionArchitectAssociate/bigData-quickSight-examTips02.png)     



======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - MOVING TRANSFORMED DATA USING AWS DATA PIPELINE** 
</span>

[Documentation aws - data pipeline](https://aws.amazon.com/fr/datapipeline/)     
Automatisez facilement le transfert et la transformation de données     

AWS Data Pipeline est un service Web qui vous permet de traiter et de transférer des données de manière fiable entre différents services AWS de stockage et de calcul et vos sources de données sur site, selon des intervalles définis  ➡️ c'est un ETL        
AWS Data Pipeline est utilisé pour le deplacement et transformation automatique des datas  


![bigData-pipeline-components](./images/aws_solutionArchitectAssociate/bigData-pipeline-components.png)     


![bigData-pipeline-componentsToKnow](./images/aws_solutionArchitectAssociate/bigData-pipeline-componentsToKnow.png)    



Cas d'usages:    
 - traitement des données dans EMR à l'aide du streaming Hadoop     
 - import/export data d'une db dynamoDb    
 - copie de fichiers csv ou date entre des S3 buckets    
 - export data RDS vers S3    
 - copie des data vers Redshift   



![bigData-pipeline-schema](./images/aws_solutionArchitectAssociate/bigData-pipeline-schema.png)    


![bigData-pipeline-examTips](./images/aws_solutionArchitectAssociate/bigData-pipeline-examTips.png)     





======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - IMPLEMENTING AMAZON MANAGED STREAMING FOR APACHE KAFKA (AMAZON MSK)** 
</span>


[Documentation aws - MSK: Managed Streming for Apache Kafka](https://aws.amazon.com/fr/msk/)    
Diffuser vos données en toute sécurité grâce à un service Apache Kafka entièrement géré et hautement disponible    

Amazon MSK facilite l'ingestion et le traitement des données de streaming en temps réel avec Apache Kafka entièrement géré    

![bigData-kafka-schema](./images/aws_solutionArchitectAssociate/bigData-kafka-schema.png)    



![bigData-msk-overview](./images/aws_solutionArchitectAssociate/bigData-msk-overview.png)     
![bigData-msk-concept](./images/aws_solutionArchitectAssociate/bigData-msk-concept.png)       
![bigData-msk-resiliencyt](./images/aws_solutionArchitectAssociate/bigData-msk-resiliencyt.png)    
![bigData-msk-plus](./images/aws_solutionArchitectAssociate/bigData-msk-plus.png)       
![bigData-msk-sec](./images/aws_solutionArchitectAssociate/bigData-msk-sec.png)       

![bigData-msk-examTips](./images/aws_solutionArchitectAssociate/bigData-msk-examTips.png)       




======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - ANALYSING DATA WITH AMAZON OPENSEARCH SERVICE** 
</span>


[Documentation aws - openSearch](https://aws.amazon.com/fr/opensearch-service/)    
[Documentation aws - introdution OpenSearch](https://aws.amazon.com/fr/blogs/opensource/introducing-opensearch/)    

**OpenSearch**     
Débridez la recherche temps réel, le monitoring et l'analyse de données métier et opérationnelles, de manière sécurisée.    

Amazon OpenSearch Service vous permet d'effectuer facilement des analytiques interactives de journaux , de surveiller des applications en temps réel, de rechercher du contenu sur site web, etc. OpenSearch est une suite de recherche et d'analytique distribuée, open source, dérivée d'Elasticsearch. Amazon OpenSearch Service offre les dernières versions d'OpenSearch, la prise en charge de 19 versions d'Elasticsearch (versions 1.5 à 7.10) ainsi que des fonctionnalités de visualisation à technologie OpenSearch Dashboards et Kibana (versions 1.5 à 7.10)    


![bigData-opensearch-schema](./images/aws_solutionArchitectAssociate/bigData-opensearch-schema.png)        



![bigData-opensearch-services](./images/aws_solutionArchitectAssociate/bigData-opensearch-services.png)     
![bigData-opensearch-diagram](./images/aws_solutionArchitectAssociate/bigData-opensearch-diagram.png)     
![bigData-opensearch-examTip](./images/aws_solutionArchitectAssociate/bigData-opensearch-examTip.png)     






======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - EXAM TIPS** 
</span>


![bigData-examTip01](./images/aws_solutionArchitectAssociate/bigData-examTip01.png)      
![bigData-examTip02](./images/aws_solutionArchitectAssociate/bigData-examTip02.png)      
![bigData-examTip03](./images/aws_solutionArchitectAssociate/bigData-examTip03.png)      
![bigData-examTip04](./images/aws_solutionArchitectAssociate/bigData-examTip04.png)      
![bigData-examTip05](./images/aws_solutionArchitectAssociate/bigData-examTip05.png)      
![bigData-examTip06](./images/aws_solutionArchitectAssociate/bigData-examTip06.png)      






======================================================================================================================= 


<span style="color: #EA9811">

###  **BIG DATA - QUIZZ** 
</span>



![bigData-quizz01](./images/aws_solutionArchitectAssociate/bigData-quizz01.png)     
![bigData-quizz02](./images/aws_solutionArchitectAssociate/bigData-quizz02.png)     
![bigData-quizz03](./images/aws_solutionArchitectAssociate/bigData-quizz03.png)     
![bigData-quizz04](./images/aws_solutionArchitectAssociate/bigData-quizz04.png)     
![bigData-quizz05](./images/aws_solutionArchitectAssociate/bigData-quizz05.png)     



**QUIZZ REDO**      

![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/111.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/112.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/113.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/114.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/115.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/116.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/117.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/118.png)          
![quizz redo](./images/aws_solutionArchitectAssociate/quizz-redo/119.png)           



======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **SERVERLESS ARCHITECTURE** 
</span>



[Documentation aws - serverless](https://aws.amazon.com/fr/serverless/)     
Créer et exécuter des applications sans se soucier des serveurs     


AWS propose des technologies qui permettent d'exécuter le code, de gérer les données et d'intégrer les applications, le tout sans avoir à gérer les serveurs.     
Les technologies sans serveur offrent la scalabilité automatique, une haute disponibilité intégrée et un modèle de facturation à l'utilisation qui permettent d'augmenter l'agilité et d'optimiser les coûts.     
Ces technologies éliminent également les tâches de gestion de l'infrastructure, telles que l'approvisionnement en capacité et l'application de correctifs, ce qui vous permet de vous concentrer uniquement sur l'écriture du code qui sert vos clients.     
Les applications sans serveur commencent avec AWS Lambda, un service de calcul guidé par les évènements intégré en natif avec plus de 200 services AWS services et applications SaaS (software as a service).


[Documentation aws - Lambda](https://aws.amazon.com/fr/lambda/)    
Exécuter du code sans vous soucier des serveurs ou des clusters    

AWS Lambda est un service de calcul d'événement sans serveur qui vous permet d'exécuter du code pour presque tout type d'application ou de service de backend, sans vous soucier de l'allocation ou de la gestion des serveurs.   
  
Vous pouvez activer Lambda pour plus de 200 services AWS et applications SaaS (logiciel en tant que service). En plus, vous ne payez que pour ce que vous utilisez.



[Documentation aws - Fargate](https://aws.amazon.com/fr/fargate/)    
Calcul sans serveur pour conteneurs     

AWS Fargate est un moteur de calcul sans serveur, de paiement à l'utilisation qui vous permet de vous concentrer sur la création d'applications sans avoir à gérer les serveurs.      
AWS Fargate est compatible avec Amazon Elastic Container Service (ECS) et Amazon Elastic Kubernetes Service (EKS).     



![serverles-fargate-schema](./images/aws_solutionArchitectAssociate/serverles-fargate-schema.png)    



[Explication Lambda vs Fargate](https://www.cloudzero.com/blog/fargate-vs-lambda)    


======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - OVERVIEW** 
</span>


![serverles-overview-historique](./images/aws_solutionArchitectAssociate/serverles-overview-historique.png)     


**Pourquoi passer au serverless ?**      

Parce que cela demande beaucoup moins de travail   
 ➡️ AWS gère toute la partie harware, network, electricité...
 ➡️ nous ne gérons que le code à écrire, importer et faire évoluer   
 ➡️ nous n'utilisons que les ressources dont nous avons besoin au moment où nous en avaons besoin ➡️ plus besoin d'anticper des grosses affluences et d'ajouter des serveurs, de modifier les LB....


**Lambda** et **Fargate** sont les 2 services serverless dont nous allons parler dans le suite de cette formation    


**Lambda**: ecrire le code, construite les fonctions ➡️ c'est tout !        
**Fargate**: démarrer un container dans fargate ➡️ pas de server, pas d'update !     


![serverles-overview-examTip](./images/aws_solutionArchitectAssociate/serverles-overview-examTip.png)     





======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - COMPUTING WITH LAMBDA** 
</span>


Exemple script lambda function :    
```bash
#!/usr/bin/python
import boto3
import logging

#setup simple logging for INFO
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#define the connection
ec2 = boto3.resource('ec2')

def lambda_handler(event, context):
    # Use the filter() method of the instances collection to retrieve
    # all running EC2 instances.
    filters = [{
            'Name': 'tag:Name',
            'Values': ['']
        },
        {
            'Name': 'instance-state-name',
            'Values': ['running']
        }
    ]

    #filter the instances
    instances = ec2.instances.filter(Filters=filters)

    #locate all running instances
    RunningInstances = [instance.id for instance in instances]

    #print the instances for logging purposes
    #print RunningInstances

    #make sure there are actually instances to shut down.
    if len(RunningInstances) > 0:
        #perform the shutdown
        shuttingDown = ec2.instances.filter(InstanceIds=RunningInstances).stop()
        print shuttingDown
    else:
        print "Nothing to see here"
```


.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - LAMBDA - WHAT IS LAMBDA ?** 
</span>


C'est un service serverless de calcul.    
Pas besoin de gérer les serveurs, seulement le code applicatif et l'exécuter sans se soucier des serveurs        

  ➡️ on écrit le code ➡️ on exécute le code ➡️ on stoppe le code quand ce n'est plus nécessaire    
      ➡️ on met à jour le code    



Lambda est un service de calcul idéal pour les scénarios d'application qui doivent augmenter la capacité rapidement, et la réduire à zéro lorsqu'elle n'est pas demandée   


**Cas d'utilisation**         
- **Traitement de fichiers** : utilisez Amazon Simple Storage Service (Amazon S3) pour déclencher le traitement des données Lambda en temps réel après un chargement.

- **Traitement des flux** : utilisez Lambda et Amazon Kinesis pour traiter des données de flux en temps réel pour le suivi de l'activité des applications, le traitement des ordres de transaction, l'analyse du flux de clics, le nettoyage des données, le filtrage des journaux, l'indexation, l'analyse des réseaux sociaux, la télémétrie des données des appareils de l'Internet des objets (IoT) et les métriques.

- **Applications Web** : associez Lambda à d'autres services AWS pour créer de puissantes applications Web qui se mettent automatiquement à l'échelle et s'exécutent dans une configuration hautement disponible dans plusieurs centres de données.

- **Backends IoT** : créez des backends sans serveur à l'aide de Lambda pour gérer les demandes d'API Web, mobiles, IoT et tierces.

- **Backends mobiles** : créez des backends à l'aide de Lambda et d'Amazon API Gateway pour authentifier et traiter les demandes d'API. Utilisez AWS Amplify pour intégrer facilement à vos frontends iOS, Android, Web et React Native.






.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - LAMBDA - BUILDING A FUNCTION** 
</span>


![serverless-lambda-function](./images/aws_solutionArchitectAssociate/serverless-lambda-function.png)     

[Documentation aws - lambda - guide du développeur](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/welcome.html)    

Lambda exécute le code sur une infrastructure informatique à haute disponibilité et effectue toute l'administration des ressources informatiques, y compris la maintenance des serveurs et du système d'exploitation, l'allocation et la mise à l'échelle automatique des capacités, ainsi que la mise à l'échelle automatique et la journalisation.  
    
Avec Lambda, il vous suffit de fournir votre code dans l'une des exécutions de langage prises en charge par Lambda.     

Vous organisez votre code en fonctions Lambda.      
Le service Lambda n'exécute votre fonction qu'en cas de besoin et se met à l'échelle automatiquement.     
Vous payez uniquement le temps de calcul que vous utilisez.     
Vous n'exposez aucuns frais quand votre code n'est pas exécuté    


  

[Documentation aws - Serverless guide](https://docs.aws.amazon.com/fr_fr/serverless/latest/devguide/welcome.html)     

![serverless-lambda-guide](./images/aws_solutionArchitectAssociate/serverless-lambda-guide.png)     





.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - LAMBDA - EXAM TIPS** 
</span>


[Documentation aws - Lambda quotas](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/gettingstarted-limits.html)    



![serverless-lambda-examTips01](./images/aws_solutionArchitectAssociate/serverless-lambda-examTips01.png)    
![serverless-lambda-examTips02](./images/aws_solutionArchitectAssociate/serverless-lambda-examTips02.png)    










.......................................................................................................................


======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - LEVERAGING THE AWS SERVERLESS APPLICATION REPOSITORY** 
</span>


Tirer parti du référentiels d'applications serverless     

[Documentation aws - Serverless Application Repository](https://aws.amazon.com/fr/serverless/serverlessrepo/)     
Découverte, déploiement et publication d'applications serverless     

AWS Serverless Application Repository est un référentiel géré conçu pour les applications serverless.      
Il permet aux équipes, organisations et développeurs individuels de stocker et de partager des applications réutilisables, ainsi que de facilement assembler et déployer des architectures serverless de manière innovante et optimisée.      
Avec Serverless Application Repository, vous n'avez pas besoin de cloner, créer, compiler ou publier du code source sur AWS avant de le déployer.       
Chacune des applications de ce repository est intégrées dans Lambda ➡️ pas besoin d'installation pour l'utiliser    


C'est l'équivalent docker-hub pour aws serverless    


![serverless-appRepo-schema](./images/aws_solutionArchitectAssociate/serverless-appRepo-schema.png)     


![serverless-appRepo-deploy](./images/aws_solutionArchitectAssociate/serverless-appRepo-deploy.png)      


![serverless-appRepo-examTip](./images/aws_solutionArchitectAssociate/serverless-appRepo-examTip.png)     







======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - CONTAINER OVERVIEW** 
</span>



**Un container c'est quoi ?**      
C'est une unité standard de logiciels qui regroupe le code et toutes ses dépendances      
Cela permet aux applications d'être exécutées rapidement et avec fiabilité depuis un environnement de calcul ou un autre    


Un container utilise un dockerfile     
Un dockerfile génère une image     
Une image peut-être exportée/importée depuis un repo (docker-hub)     


![serverless-container-examTips01](./images/aws_solutionArchitectAssociate/serverless-container-examTips01.png)    
![serverless-container-examTips02](./images/aws_solutionArchitectAssociate/serverless-container-examTips02.png)    






======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - RUNNING CONTAINERS IN ECS OR EKS** 
</span>


[Documentation aws- Amazon Elastic Container Service Anywhere in now generally available](https://aws.amazon.com/fr/about-aws/whats-new/2021/05/amazon-elastic-container-service-anywhere-is-now-generally-available/)   



La question est: il est facile de manager 1 ou 2 container sur une instance MAIS comment faire lorsqu'il y a 1000 containers sur une fleet d'instances ?    
➡️ c'est ici que ECS intervient    




.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - ECS: Elastic Container Service** 
</span>



[Documentation aws - ECS](https://aws.amazon.com/fr/ecs/)     
[Documentation aws - Fonctionnalités ECS](https://aws.amazon.com/fr/ecs/features/)       

Exécutez des conteneurs hautement sécurisés, fiables et évolutifs    


Amazon **Elastic Container Service (Amazon ECS)** est un service d'orchestration de conteneurs entièrement géré qui vous permet de déployer, gérer et mettre à l'échelle des applications conteneurisées en toute simplicité

Décrivez simplement votre application et les ressources requises, et Amazon ECS lancera, surveillera et mettra à l'échelle votre application grâce à des options de calcul flexibles avec des intégrations automatiques à d'autres services AWS de support dont votre application a besoin      

Effectuez des opérations système telles que la création de règles de mise à l'échelle et de capacité personnalisées, et observez et interrogez des données à partir des journaux d'applications et de la télémétrie     


![serverless-ec2-schema](./images/aws_solutionArchitectAssociate/serverless-ec2-schema.png)    



![serverless-ec2-why](./images/aws_solutionArchitectAssociate/serverless-ec2-why.png)     


Le problème est que EC2 ne fonctionne que dans AWS ➡️quoi faire si nous avons des containers qui ne sont pas dans AWS ?   
 ➡️ c'est là que kubernetes intervient ➡️ EKS     


.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - EKS: Elastic Kubernetes Service** 
</span>




[Documentation aws - EKS](https://aws.amazon.com/fr/eks/)     
Le moyen le plus sûr de démarrer, d'exécuter et de mettre à l'échelle Kubernetes     

Amazon EKS est un service Kubernetes géré qui exécute des Kubernetes dans le cloud AWS et dans les centres de données sur site     

Dans le cloud, Amazon EKS gère automatiquement la disponibilité et la capacité de mise à l'échelle des nœuds de plan de contrôle de Kubernetes, qui sont responsables de la planification des conteneurs, de la gestion de la disponibilité des applications, du stockage des données des clusters et d'autres tâches clés    
Avec Amazon EKS, vous pouvez profiter de toutes les performances, de l'évolutivité, de la fiabilité et de la disponibilité de l’infrastructure AWS    
D'autres avantages incluent les intégrations avec les services de mise en réseau et de sécurité d’AWS     
Sur site, EKS offre une solution cohérente, entièrement prise en charge par Kubernetes, avec des outils intégrés et un déploiement facile dans AWS Outposts, les machines virtuelles ou les serveurs sur matériel nu     











.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - ECS vs EKS** 
</span>


![serverless-ec2VsEks](./images/aws_solutionArchitectAssociate/serverless-ec2VsEks.png)     








.......................................................................................................................

<span style="color: #11E3EA">

####  **SERVERLESS - ECS / EKS - EXAM TIPS** 
</span>


![serverless-ec2-examTips01](./images/aws_solutionArchitectAssociate/serverless-ec2-examTips01.png)    
![serverless-ec2-examTips02](./images/aws_solutionArchitectAssociate/serverless-ec2-examTips02.png)    





.......................................................................................................................


======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - REMOVING SERVERS WITH FARGATE** 
</span>



**Pourquoi utiliser Fargate ?**      
Lorsque nous avons beaucoup de containers qui tournent et qu'il les faut mettre à jour on ne va pas les faire 1 à 1  ➡️ fargate est parfait pour cette tache     


Fargate foncionne très bien avec ECS et EKS      




![serverless-fagateVsEc2](./images/aws_solutionArchitectAssociate/serverles-fagateVsEc2.png)     
![serverless-fagateVslambda](./images/aws_solutionArchitectAssociate/serverles-fagateVsLambda.png)     



>_Remarque_: Pour fargate il faut se rappeler que la facturation sera basées sur le nombre de ressource et la durée d'utilisation de ces ressources ➡️ lors de la création de ce service il est important de bien définir ces ressources pour payer moins cher




![serverless-fagate-examTips01](./images/aws_solutionArchitectAssociate/serverles-fagate-examTips01.png)      
![serverless-fagate-examTips02](./images/aws_solutionArchitectAssociate/serverles-fagate-examTips02.png)      



======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - AMAZON EVENTBRIDGE (CLOUDWATCH EVENTS)** 
</span>



[Documentation aws - CloudWatch events](https://docs.aws.amazon.com/fr_fr/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html)      

Amazon CloudWatch Events fournit un flux d'événements système en quasi temps réel qui décrit les modifications apportées aux ressources Amazon Web Services (AWS)     
A l'aide de règles simples et rapidement configurées, vous pouvez faire correspondre des événements et les acheminer vers un ou plusieurs flux ou une ou plusieurs fonctions cibles       
CloudWatch Events prend connaissance des changements opérationnels à mesure qu'ils se produisent       
CloudWatch Events répond à ces changements opérationnels et, le cas échéant, prend des mesures correctives en envoyant des messages pour répondre à l'environnement, en activant des fonctions, en procédant à des modifications et en capturant des informations de statut        
Vous pouvez également utiliser CloudWatch Events pour planifier des actions automatisées qui se déclenchent automatiquement à certaines heures à l'aide d'expressions cron ou de fréquence     

Vous pouvez configurer les différents services AWS en tant que cibles pour CloudWatch Events :     
- Instances Amazon EC2      
- AWS LambdaFonctions    
- Flux dans Amazon Kinesis Data Streams       
- Flux de diffusion dans Amazon Kinesis Data Firehose      
- Groupes de journaux dans Amazon CloudWatch Logs      
- Tâches Amazon ECS      
- Run Command de Systems Manager      
- Systems Manager Automation      
- AWS Batch tâches         
- Machines d'état Step Functions      
- Pipeline dans CodePipeline       
- Projets CodeBuild       
- Modèles d'évaluation Amazon Inspector       
- Rubriques Amazon SNS       
- Files d'attente Amazon SQS       
- Cibles intégrées : `EC2 CreateSnapshot API call`, `EC2 RebootInstances API call`, `EC2 StopInstances API call` et `EC2 TerminateInstances API call`.      
- Le bus d'événement par défaut d'un autre compte AWS        


**Concepts à comprendre**      
- **events** :  les événements indiquent un changement dans votre environnement AWS       

- **regles** :  les règles correspondent à des événements entrants et les acheminent vers des cibles pour être traités. Une seule règle peut aboutir à plusieurs cibles, qui sont toutes traitées en parallèle                 

- **cibles** :   les cibles sont chargées de traiter le événements. Les cibles d'une règle doivent se trouver dans la même région que la règle            



.......................................................................................................................    

<span style="color: #11E3EA">

####  **SERVERLESS - CLOUDWATCH EVENT - EVENTBRIDGE** 
</span>


[Documentation aws - eventbridge](https://aws.amazon.com/fr/eventbridge/)     
Créer des applications événementielles à grande échelle dans AWS, des systèmes existants ou des applications SaaS    

**EventBrige** (**CloudWatch events**) est un bus d'évènement serverless qu iautorise à passer des évènements d'une source vers une cible. 






.......................................................................................................................    

<span style="color: #11E3EA">

####  **SERVERLESS - CLOUDWATCH EVENT - RULE** 
</span>


![serverless-event-rule](./images/aws_solutionArchitectAssociate/serverless-event-rule.png)      





.......................................................................................................................    

<span style="color: #11E3EA">

####  **SERVERLESS - CLOUDWATCH EVENT - EXAM TIPS** 
</span>



![serverless-event-examTips01](./images/aws_solutionArchitectAssociate/serverless-event-examTips01.png)     
![serverless-event-examTips02](./images/aws_solutionArchitectAssociate/serverless-event-examTips02.png)     


.......................................................................................................................    


======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - STORING CUSTOM DOKER IMAGES IN AMAZON ELASTIC CONTAINER REGISTRY (AMAZON ECR)** 
</span>



Amazon ECR est:    
- **registry**        ➡️   registry managé par AWS ➡️ securisé, scalable, possède une infra iable     
- **privé**           ➡️   ce registry est privé et contient nos images de containers. L'accès à ces images se fait avec les **resources-based permissions** via IAM      
- **multi formats**   ➡️   il supporte comme format d'images de containers **OCI (Open Container Initiative)**, **Docker images** et **OCI artifacts**



- Un **registry privé** est accessible à chaque account AWS qui en a les droits     
- Pour se connecter un **token d'authentification** est nécessaire (pour pull, push images)      
- Un **repository** contient les images    
- Les **repositoris policies** controlent les accès aux repos et aux images     
- Un **image** de container peut être push ou pull d'un repository    



**Les policies de cycle de vie**     
- Elles aident à manager les images dans les repositories     
- Elles définissent des regles de nettoyage des images inutilisées     
- Elles ont la capacité de tester ces regles avant de les appliquer  ➡️ permet de voir quelles images va être supprimées avant de lancer la suppression       


**Image scanning**       
- Aide à identifier les vulnérabilités software dans les containers d'images      
- Peut être configuré pour être scanné sur un push    
- permet d'avoir accès aux resultats des scans de chaque image    



**Partage**    
- ECR a un support inter region     
- ECR support inter account       
- ECR est configuré par repository et par region


**Cache rules**     
- fait le pull à travers les regles de cache si elles autorisent le cache publique de repo privés    
- ECR va vérifier le status du cache courant      


**Tag mutability** (mutualisation de tags)           
- ils préviennent les tags des images d'être overwritten     
- ils sont configurés par repo    


**Integrations**      
- on peut integrer notre propre infrastructure container dans les images contenus dans ECR     
- on peut integrer directement ECS qui contient les definitions des containers (mieux)     
- idem pour EKS      
- on peut aussi intégrer les containers locaux amazon linux qui sont utilisés pour le dev     



![serverless-ecr-examTips](./images/aws_solutionArchitectAssociate/serverless-ecr-examTips.png)       





======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - USING OPEN-SOURCE KUBERNETES IN AMAZON EKS DISTRO** 
</span>



**EKS Distro** ➡️ EKS-D ➡️ distribution Kubernetes de AWS      
EKS-D a les mêmes versions et dependances que EKS       
La différence est que **EKS est managé par AWS** ALORS QUE **EKS-D est managé par vous**      

EKS-D fonctionne partout (aws, autre provider, on-premise)      
EKS-D est sous votre responsabilité ➡️ upgrade/manage     


![serverless-eks-examTip](./images/aws_solutionArchitectAssociate/serverless-eks-examTip.png)      





======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - ORCHESTRATING CONTAINERS OUTSIDE AWS USING AMAZON ECS ANYWHERE AND AMAZON EKS ANYWHERE** 
</span>



[Documentation aws - EKS Anywhere](https://aws.amazon.com/fr/eks/eks-anywhere/)     
Créer et exploiter des clusters Kubernetes sur votre propre infrastructure      

Amazon EKS Anywhere vous permet de créer et d'exploiter des clusters Kubernetes sur votre propre infrastructure    
Amazon EKS Anywhere crée sur les forces d'Amazon EKS Distro et fournit un logiciel open source qui est à jour et corrigé afin que vous puissiez avoir un environnement Kubernetes sur site qui est plus fiable qu'une offre Kubernetes autogérée    


![serverless-eks-anywhere-schema](./images/aws_solutionArchitectAssociate/serverless-eks-anywhere-schema.png)    


![serverless-eks-anywhere-overview](./images/aws_solutionArchitectAssociate/serverless-eks-anywhere-overview.png)     
![serverless-eks-anywhere-concepts](./images/aws_solutionArchitectAssociate/serverless-eks-anywhere-concepts.png)     

[Documentation aws - EKS Flux](https://aws.amazon.com/fr/blogs/containers/part-1-build-multi-cluster-gitops-using-amazon-eks-flux-cd-and-crossplane/)     
**Flux** est un outil GitOps qui permet le management et le déploiement d'infrastucture    

[Lien officiel Flux](https://fluxcd.io/)     



[Documentation aws - ECS anywhere](https://aws.amazon.com/fr/ecs/anywhere/)     
Exécuter des conteneurs sur votre propre infrastructure sur site      

Amazon Elastic Container Service (ECS) Anywhere est une fonctionnalité d'Amazon ECS qui vous permet d'exécuter et de gérer des charges de travail de conteneur sur votre infrastructure      
Cette fonctionnalité vous aide à répondre aux exigences de conformité et à faire évoluer votre entreprise sans sacrifier vos investissements sur site      


![serverless-ecs-anywhere-schema](./images/aws_solutionArchitectAssociate/serverless-ecs-anywhere-schema.png)   


![serverless-ecs-anywhere-overview](./images/aws_solutionArchitectAssociate/serverless-ecs-anywhere-overview.png)  


**Pré-requis à l'utilisation de ECS Anywhere**:    
- OBLIGATOIRE d'avoir **SSM Agent**, **ECS Agent** et **Docker** installés    
- OBLIGATOIE d'enregistrer en 1er les instances externes comme **SSM Managed Instances**     
- Ces 2 actions peuvent se faire facilement avec un script d'installation dans la console ECS     
- Ce script doit contenir les **SSM activation keys** et les **commandes** des logiciels nécessaires    
- Ce script peut être exécuté n'importe où (VMs on-premise, serveur bare-metal)     
- Une fois toutes les étapes précédentes faites, nous pouvons déployer les containers en utilisant le **launch type EXTERNAL**      






[Documentation aws - SSM Agent](https://docs.aws.amazon.com/fr_fr/systems-manager/latest/userguide/ssm-agent.html)     
AWS Systems Manager Agent (SSM Agent) est un logiciel Amazon qui s'exécute sur les instances Amazon Elastic Compute Cloud (Amazon EC2), les appareils de périphérie, ainsi que les serveurs sur site et les machines virtuelles (VM). 
    
SSM Agent permet à Systems Manager de mettre à jour, de gérer et de configurer ces ressources     
L'agent traite les demandes du service Systems Manager dans le AWS Cloud et les exécute ensuite comme spécifié dans la demande      

Puis, SSM Agent retourne les informations de statut et d'exécution au service Systems Manager à l'aide du service de Amazon Message Delivery Service (préfixe de service : `ec2messages`).



[Documentation aws - ECS Agent](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_agent.html)    
L'agent de conteneur Amazon ECS permet aux instances de conteneur de se connecter à votre cluster       

L'agent de conteneur Amazon ECS est inclus dans les AMI optimisées pour Amazon ECS, mais vous pouvez également l'installer sur n'importe quelle instance Amazon EC2 prenant en charge la spécification Amazon ECS.      

L'agent de conteneur Amazon ECS est pris en charge sur les instances Amazon EC2 et les instances externes (serveur sur site ou machine virtuelle)    



[Documentation Docker](https://docs.docker.com/engine/install/)     


[Documentation aws - SSM Managed Instances](https://docs.aws.amazon.com/systems-manager/latest/userguide/managed_instances.html)     


[Documentation aws - Set up to use Amazon ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/get-set-up-for-amazon-ecs.html)      




![serverless-ecs-anywhere-diagram](./images/aws_solutionArchitectAssociate/serverless-ecs-anywhere-diagram.png)  







![serverless-eksEcs-anywhere-examTips01](./images/aws_solutionArchitectAssociate/serverless-eksEcs-anywhere-examTips01.png)      
![serverless-eksEcs-anywhere-examTips02](./images/aws_solutionArchitectAssociate/serverless-eksEcs-anywhere-examTips02.png)      




======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - AUTO SCALING DATABASES ON DEMAND WITH AMAZON AURORA SERVERLESS** 
</span>


[Documentation aws - Aurora](https://aws.amazon.com/fr/rds/aurora/)     


[Documentation aws - Aurora Serverless](https://aws.amazon.com/fr/rds/aurora/serverless/)       
Amazon Aurora Serverless est une configuration à la demande à scalabilité automatique pour Amazon Aurora.    

Elle démarre, arrête, augmente ou réduit la capacité en fonction des besoins de votre application, le tout de manière automatique    

Exécutez votre base de données dans le cloud sans gérer d'instances de base de données.     

Vous pouvez également utiliser des instances Aurora sans serveur v2 ainsi que des instances provisionnées dans vos clusters de bases de données existants ou nouveaux.    

Avec Aurora sans serveur, vous créez une base de données, spécifiez sa plage de capacité en fonction de vos besoins et connectez vos applications     

Vous payez à la seconde en fonction de la capacité de la base de données que vous utilisez lorsque cette dernière est active     

Vous pouvez aussi passer d'une configuration standard à une configuration sans serveur en quelques étapes dans la console Amazon Relational Database Service (Amazon RDS)     


![serverless-aurora-overview](./images/aws_solutionArchitectAssociate/serverless-aurora-overview.png)        
![serverless-aurora-concepts](./images/aws_solutionArchitectAssociate/serverless-aurora-concepts.png)         



- **Évolutivité élevée**: Mettez instantanément à l'échelle, en une fraction de seconde, des centaines de milliers de transactions     
- **Disponibilité élevée**: Optimisez vos charges de travail critiques avec l'étendue complète des fonctions d'Aurora, y compris le clonage, la base de données globale, Multi-AZ, et les réplicas en lecture    
- **Rentabilité**: Montez en puissance par incréments précis pour fournir la quantité exacte de ressources et ne payer que la capacité consommée     
- **Simplicité**: Élimine la complexité inhérente à l'approvisionnement et à la gestion de la capacité de la base de données     
- **Transparence**: Mettez instantanément à l'échelle la capacité de la base de données, sans perturber les demandes entrantes des applications    
- **Durabilité**: Protège contre la perte de données grâce au stockage Aurora distribué, tolérant aux pannes, à réparation spontanée, qui réplique les données sur six nœuds    


![serverless-aurora-useCases](./images/aws_solutionArchitectAssociate/serverless-aurora-useCases.png)       

![serverless-aurora-examtip](./images/aws_solutionArchitectAssociate/serverless-aurora-examtip.png)       






======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - USING AWS X-RAY FOR APPLICATION INSIGHTS** 
</span>



[Documentation aws - X-Ray](https://aws.amazon.com/fr/xray/)     
Analysez et déboguez la production et les applications distribuées     

AWS X-Ray fournit une vue complète des requêtes à mesure qu'elles transitent dans votre application et filtre les données visuelles sur les charges utiles, les fonctions, les traces, les services, les API, et plus encore, avec des motions sans code et à faible code.


![serverless-xRay-schema](./images/aws_solutionArchitectAssociate/serverless-xRay-schema.png)     

C'est un APM aws comme Dynatrace ou AppDynamics


**X-Ray concepts**:      
- **segments**: data contenant resource neames, request details.....       
- **subsegments**: segments qui fourni plus de détails dans les datas              
- **service graph**: diagramme qui permet une cartographie des services et des requêtes qui interagissent      
- **traces**: traces avec un ID le chemin de la requete et des infos récupérées sur cette requete dans chaque segment      
- **tracing header**: header HTTP supplementaire contenant des échantillons de décisions et le trace ID          
- ce tracing header s'appelle `X-Amzn-Trace-Id`        




[Documentation aws - X-Ray daemon](https://docs.aws.amazon.com/xray/latest/devguide/xray-daemon.html)       

Le daemon AWS X-Ray est une application logicielle qui écoute le trafic sur le port UDP 2000, rassemble les données de segment brutes et les relaie à l'API AWS X-Ray        

Le daemon fonctionne conjointement avec les kits SDK AWS X-Ray et doit être en cours d'exécution pour que les données envoyées par les kits SDK puissent atteindre le service X-Ray        

  ➡️ Il permet de collecter plus facilement les infos et de les envoyer vers les workers


![serverless-xRay-integrations](./images/aws_solutionArchitectAssociate/serverless-xRay-integrations.png)     

![serverless-xRay-examTip](./images/aws_solutionArchitectAssociate/serverless-xRay-examTippng.png)     




======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - DEPLOYING GRAPHQL INTERFACES IN AWS APPSYNC** 
</span>


[Documentation aws - GraphQL / AppSync](https://aws.amazon.com/fr/appsync/)        
Accélérez le développement des applications avec les API GraphQL et Pub/Sub sans serveur.       


[Documentation GraphQl](https://graphql.org/)       



AWS AppSync crée des API GraphQL et Pub/Sub sans serveur qui simplifient le développement d'applications grâce à un endpoint unique pour interroger, mettre à jour ou publier des données en toute sécurité. 

Les API GraphQL conçues avec AWS AppSync offrent aux développeurs frontend la possibilité d'interroger plusieurs bases de données, microservices et API à partir d’un endpoint GraphQL unique

![serverless-graphQl-schema](./images/aws_solutionArchitectAssociate/serverless-graphQl-schema.png)     


- **Accès unifié aux données**:                
  ➡️ **graphQL**: AWS AppSync utilise GraphQL, un langage de données qui permet aux applications clientes de récupérer, de modifier et de s'abonner aux données des serveurs     

  ➡️ **Mise en cache**: Les capacités de mise en cache des données côté serveur d’AWS AppSync permettent de réduire le besoin d'accéder directement aux sources de données en rendant les données disponibles dans des caches haute vitesse gérés en mémoire, ce qui permet de fournir des données à faible latence     

  ➡️ **Synchronisation des données hors ligne**: Amplify DataStore dispose d'un magasin de données interrogeable sur appareil DataStore pour les développeurs web, mobiles et IoT qui offre un modèle de programmation local et familier pour interagir en permanence avec les données, que vous soyez en ligne ou hors ligne   

  ➡️ **API fusionnées**: Une API fusionnée compose plusieurs schémas GraphQL, importe des sources de données et des résolveurs AppSync à partir de plusieurs API, et consolide toutes les ressources, en combinant toutes les API en un seul endpoint d'API fusionné qui peut être exposé aux clients    


- **Expérience en temps réel**:       
  ➡️ **Mises à jour et accès aux données en temps réel**: AWS AppSync vous permet de spécifier quelles portions des données doivent être accessibles en temps réel à l'aide des abonnements GraphQL     

  ➡️ **Filtres d'abonnement**: Les fonctions de filtrage d'abonnement disponibles avec AWS AppSync vous permettent de créer un large éventail d'expériences en temps réel dans vos applications en tirant parti des opérateurs de filtre, du filtrage côté serveur et de la possibilité de déclencher des désapprobations d'abonnement     

  ➡️ **API Pub/Sub simples**: Si vous voulez uniquement une API éphémère qui reçoive des messages émis vers un canal et envoie ces messages en temps réel à des clients abonnés, vous pouvez configurer votre API sans avoir de connaissances ou d'expertise de GraphQL à l'aide de l'assistant d'API PubSub AWS AppSync.        


- **Operations simplifiées**:              
  ➡️ **Contrôle précis des accès**: AWS AppSync offre plusieurs niveaux d'autorisation et d'accès aux données, en fonction des besoins des applications   

  ➡️ **Noms de domaine personnalisés**: AWS AppSync permet aux clients d'utiliser des noms de domaine personnalisés avec leur API AWS AppSync pour accéder à leur endpoint GraphQl et à leur point de terminaison en temps réel.    

  ➡️ **Observabilité innovante**: Avec AWS AppSync, vous pouvez facilement configurer AWS CloudWatch et AWS X-Ray pour une journalisation et un traçage complets pour votre API GraphQL       

  ➡️ **Sécurité d'entreprise**: AWS AppSync prend en charge les API privées qui peuvent être utilisées pour limiter l'exposition aux API uniquement au sein du VPC d'un client    


**GraphQL c'est quoi ?**      
- interface robuste, scalable GraphQl for les dev d'app        
- permet de combiner les datas depuis plusieurs sources (DynamoDb, Lambda)            
- permet des interactions sur les datas pour les dev via GraphQl       
- langage qui permet l'activation des dates d'applications depuis les serveurs      
- permet une integration avec des outils comme React, IOs, Android...       


![serverless-graphQl-examTip](./images/aws_solutionArchitectAssociate/serverless-graphQl-examTip.png)                 




======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - EXAM TIPS** 
</span>




![serverless-examTips01](./images/aws_solutionArchitectAssociate/serverless-examTips01.png)        
![serverless-examTips02](./images/aws_solutionArchitectAssociate/serverless-examTips02.png)        
![serverless-examTips03](./images/aws_solutionArchitectAssociate/serverless-examTips03.png)        
![serverless-examTips04](./images/aws_solutionArchitectAssociate/serverless-examTips04.png)        
![serverless-examTips05](./images/aws_solutionArchitectAssociate/serverless-examTips05.png)        
![serverless-examTips06](./images/aws_solutionArchitectAssociate/serverless-examTips06.png)        




======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - LAB: TRIGERING AWS LAMBDA FROM AMAZON SQS** 
</span>


![serverless-enonce](./images/aws_solutionArchitectAssociate/labs/serverless-lab1/serverless-enonce.png)       


aws account       
username: cloud_user                 
password: q!2Vf&LFZ=Dv6q8(4Zcc                 
link: https://147065513498.signin.aws.amazon.com/console?region=us-east-1          

cloud EC2 Instance               
username: cloud_user                 
password: ss=zWnK7                       
publicIP: 44.211.86.252                       


![serverless-objectif](./images/aws_solutionArchitectAssociate/labs/serverless-lab1/serverless-objectif.png)    




Log in to the AWS Management Console using the credentials provided on the lab instructions page. Make sure you're using the us-east-1 (N. Virginia) region.

**Create the Lambda Function**             

1. In the search bar on top, enter lambda.    

2. From the search results, select Lambda.    

3. Click the Create function button.     

4. On the Create function page, select Author from scratch.     

5. Under Basic Information, set the following parameters for each field:     

- Function name: Enter _SQSDynamoDB_.      
- Runtime: Select Python 3.9 from the dropdown menu.         
- Architecture: Select x86_64.             

6. Under Permissions, expand Change default execution role.      

7. Select Use an existing role.        

8. Under Existing role, select lambda-execution-role from the dropdown menu.        

9. Click the Create function button.       


**Create the SQS Trigger**       

1. Click the + Add trigger button.        

2. Under Trigger configuration, click the Select a source dropdown menu.        

3. From the menu, select SQS.     

4. Under SQS queue, click the search bar and select Messages.     

5. Ensure that the checkbox next to Activate trigger is checked.    

6. Click Add.    


**Copy the Source Code into the Lambda Function**     

1. Under the + Add trigger button, click the Code tab.     

2. On the left side, double-click on lambda_function.py.     

3. Delete the contents of the function.      

4. Navigate to the link below to the source code for lambda_function.py:    
https://raw.githubusercontent.com/ACloudGuru-Resources/SQSLambdaTriggers/master/lambda_function.py. 

5. Copy the code. 

6. Return to the AWS console and paste the code into the lambda_function.py code box. 

7. Click the Deploy button.


[fichier de log de la console EC2](./archives/formation/cloud/aws_solutionArchitectAssociate/serverless_lab/console.log)        

**Log In to the EC2 Instance and Test the Script**    

1. In the search bar on top of the AWS console, enter sqs.

2. From the search results, select Simple Queue Service.

3. Click Messages.

4. Click the Monitoring tab to monitor our SQS messages.

5. In the search bar on top, enter ec2.

6. From the search results, select EC2 and open it in a new browser tab or window.

7. Under Resources, click Instances (running).

8. In the existing instance available, click the checkbox next to its name.

9. Click the Connect button at the top.

10. Click Connect at the bottom to open a shell and access the command line.

11. In the shell, become the `cloud_user` role:                    
`su - cloud_user`

12. View a list of files available to you:           
`ls`         

13. View the contents of the `send_message.py` file:                 
`cat send_message.py`                  

14. Start sending messages to our DynamoDB table from our Messages SQS queue with an interval of 0.1 seconds:    
`./send_message.py -q Messages -i 0.1          
`
15. After a few seconds, hit Control + C to stop the command from continuing to run.


**Confirm Messages Were Inserted into the DynamoDB Table**          

1. Return to the browser tab or window with the Messages queue in Amazon SQS open. You may have to wait a few minutes to see results showing up in the tables, but you should soon see a spike in the table Number of Messages Received.

2. In the search bar on top, enter dynamodb.
3. From the search results, select DynamoDB.

4. In the left-hand navigation menu, select Tables.

5. Select the Message table.

6. In the top-right corner of the page, click Explore table items and review the list of items that were inserted from our script, sent to SQS, triggered Lambda, and inserted into the DynamoDB database.




======================================================================================================================= 


<span style="color: #EA9811">

###  **SERVERLESS - QUIZZ** 
</span>




![serverless-quizz01](./images/aws_solutionArchitectAssociate/serverless-quizz01.png)    
![serverless-quizz02](./images/aws_solutionArchitectAssociate/serverless-quizz02.png)    
![serverless-quizz03](./images/aws_solutionArchitectAssociate/serverless-quizz03.png)    
![serverless-quizz04](./images/aws_solutionArchitectAssociate/serverless-quizz04.png)    
![serverless-quizz05](./images/aws_solutionArchitectAssociate/serverless-quizz05.png)    



**QUIZZ REDO**      





======================================================================================================================= 


_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **SECURITY** 
</span>


[Documentation aws - AWS Security](https://aws.amazon.com/fr/security/)       
Infrastructure et services pour renforcer votre sécurité dans le cloud      

Renforcer la capacité à répondre aux principales exigences de conformité et de sécurité, telles que la confidentialité, la protection et l'emplacement des données, grâce à l'étendue des fonctionnalités et services AWS    
Automatiser les taches de sécurité manuelles pour se concentrer sur l'innovation et le dimensionnement    
Facturation seulement pour les services utilisés      
Mise en place des normes de sécurité     





[Documentation aws - Sécurité, Identité et conformatité avec AWS](https://aws.amazon.com/fr/products/security/)     
Sécuriser vos charges de travail et vos applications dans le cloud      

- **Gestion des identités et des accès** : [**AWS Identity Services** vous aide à gérer en toute sécurité les identités, les ressources et les autorisations à grande échelle](https://aws.amazon.com/fr/identity/)       
- **Détection** : [Les services de détection de réponse d’AWS vous aident à identifier les éventuelles erreurs de configuration de la sécurité, les menaces ou les comportements inattendus](https://docs.aws.amazon.com/wellarchitected/latest/security-pillar/detection.html)      
- **Protection du reseau et des applications** : [Les services de protection du réseau et des applications vous aident à renforcer les stratégies de sécurité très fines aux points de contrôle du réseau de l'ensemble de votre organisation](https://aws.amazon.com/fr/products/security/network-application-protection/)         
- **Protection des données** : [AWS propose des services qui vous aident à protéger vos données, vos comptes et vos charges de travail des accès non autorisés](https://aws.amazon.com/fr/compliance/data-protection/)          
- **Conformité** : [AWS vous offre une visibilité complète de votre statut de conformité et contrôle en permanence votre environnement](https://aws.amazon.com/fr/cloudops/compliance-and-auditing/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc&blog-posts-cards.sort-by=item.additionalFields.createdDate&blog-posts-cards.sort-order=desc)          



[Documentation aws - AWS CloudFront](https://aws.amazon.com/fr/cloudfront/)       
Diffuser du contenu en toute sécurité avec une faible latence et des vitesses de transfert élevées     

Amazon CloudFront est un **réseau de diffusion de contenu (CDN: Content Delivery Network)** conçu pour des performances élevées, pour la sécurité et pour la simplicité de développement     

![sec-CloudFront-schema](./images/aws_solutionArchitectAssociate/sec-CloudFront-schema.png)       


======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - DDoS OVERVIEW** 
</span>

[Documentation aws - DDoS Attaque](https://aws.amazon.com/fr/shield/ddos-attack-protection/)           
         

**DDoS**: **Distributed Denial of Service**  ➡️ attaque qui rend le site web ou l'application innaccessible par les users      
Cela se fait par de multiples méchanisme comme des envois massif de paquets et utilisant des botnets qui sature les services    

**Attaque DDoS sur la couche 4**     
![modeleOsi](./images/aws_solutionArchitectAssociate/modeleOSI.png)

Cette attaque fait référence à du **SYN Flood**      
Une attaque SYN flood (attaque semi-ouverte) est un type d’attaque par déni de service (DDoS) qui vise à rendre un serveur indisponible pour le trafic légitime en consommant toutes les ressources serveur disponibles. En envoyant à plusieurs reprises des paquets de demande de connexion initiale (SYN), le pirate est en mesure de submerger tous les ports disponibles sur une machine serveur ciblée, ce qui oblige l’appareil ciblé à répondre lentement au trafic légitime, ou l’empêche totalement de répondre        

![sec-DDos-SynFloodAttack](./images/aws_solutionArchitectAssociate/sec-DDos-SynFloodAttack.png)          

La connexion classique se fait en 3 étapes:    
- SYN    
- SYN + ACK    
- ACK    
![sec-DDos-TcpConnexion](./images/aws_solutionArchitectAssociate/sec-DDos-TcpConnexion.png)     
  ➡️ dans ce genre d'attaque le serveur n'a pas la réponse ACK par le user      

En temps normal après la réponse ACK, la connexion est établie et les applications commencent à envoyer leurs datas via HTTP par exemple (couche 7)


**Amplification attack**      
Une Amplification/Reflection attack peut inclure des NTP, SSDP, DNS, CharGen, SNMP attacks.....    
C'est lorsqu'un attaquant peut envoyer à un serveur tiers (tel qu'un serveur NTP) une requête en utilisant une adresse IP usurpée     
UDP est vulnerable mais pas TCP             
Le trafic reçu par la victime peut etre enorme en raison de l'amplification. Contrairement a certains protocoles comme ICMP ”echo” (celui utilise par ping), les protocoles comme le DNS ou comme SNMP amplifient l’attaque lors de la reflexion : la reponse est plus grosse que la question. Avec le DNS, on atteint des facteurs d’amplification de 40 ou 50





[NTP attack](https://www.cloudflare.com/fr-fr/learning/ddos/ntp-amplification-ddos-attack/)      
[DNS attack](https://www.cloudflare.com/fr-fr/learning/ddos/dns-amplification-ddos-attack/)     
[SSDP attack](https://www.cloudflare.com/fr-fr/learning/ddos/ssdp-ddos-attack/)      
[CharGen attack](https://www.link11.com/en/blog/threat-landscape/chargen-flood-attacks-explained/)     
[SNMP attack](https://ddos-guard.net/en/terminology/attack_type/snmp-reflection)     

![sec-DDos-AmplificationAttack](./images/aws_solutionArchitectAssociate/sec-DDos-AmplificationAttack.png)               


**Attaque couche 7**    
Attaque qui envoie à l'application (couche 7) une quantité massive de requêtes GET ou POST (à l'aide de botnets) qui submergent les ressources du serveur qui gère l'application

![sec-DDos-Layer7Attack](./images/aws_solutionArchitectAssociate/sec-DDos-Layer7Attack.png)      



![sec-DDos-examTip](./images/aws_solutionArchitectAssociate/sec-DDos-examTip.png)      





======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - LOGGING API CALLS WITH CLOUDTRAIL** 
</span>


[Documentation aws - CloudTrail](https://aws.amazon.com/fr/cloudtrail/)      
Suivez l'activité des utilisateurs et l'utilisation des API sur AWS et dans des environnements hybrides et multicloud     

AWS CloudTrail contrôle et enregistre l'activité du compte sur l'ensemble de votre infrastructure AWS afin de vous donner le contrôle sur le stockage, l'analyse et les actions correctives     

![sec-CloudTrail-schema](./images/aws_solutionArchitectAssociate/sec-CloudTrail-schema.png)     





[Documentation aws - CloudTrail FAQs](https://aws.amazon.com/fr/cloudtrail/faqs/)           
[Documentation aws - validating cloudTrail log file integrity](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-log-file-validation-intro.html) 


CloudTrail permet l'audit, la surveillance de la sécurité et le dépannage opérationnel en suivant l'activité des utilisateurs et l'utilisation des API.      
CloudTrail enregistre, contrôle en permanence et conserve l'activité du compte liée aux actions effectuées sur votre infrastructure AWS, ce qui vous permet de contrôler le stockage, l'analyse et les actions de remédiation    

CloudTrail enregistre l'activité relative au compte et les événements de service depuis la plupart des services AWS.      
Pour obtenir la liste des services pris en charge, consultez la section [Intégrations et services pris en charge par CloudTrail](http://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-supported-services.html) du guide de l'utilisateur CloudTrail        



![sec-CloudTrail-fonctionnement](./images/aws_solutionArchitectAssociate/sec-CloudTrail-fonctionnement.png)     

**Qu'est qui est conservé dans les logs CloudTrail ?**      
- metadata des appels API     
- identité du caller des appels API     
- l'horaire de l'appel API     
- IP source du caller de l'appel API   
- les parametres de la requête    
- la réponse envoyée avec ses éléments        


**Quand utiliser CloudTrail ?**            
- pour le debug ➡️ après un incident     
- après (ou presque pendant assez rapide à remonter) une détection d'intrusion    
- pour la conformité de l'industrie et sa réglementation      



![sec-CloudTrail-examTip](./images/aws_solutionArchitectAssociate/sec-CloudTrail-examTip.png)     





======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - PROTECTING APPLICATIONS WITH SHIELD** 
</span>

[Documentation aws - AWS Shield](https://aws.amazon.com/fr/shield/)      
Optimisez la disponibilité et la réactivité des applications grâce à une protection DDoS gérée    

AWS Shield est un service géré pour la protection contre les attaques DDoS et qui protège les applications exécutées sur AWS.     

![sec-Shield-schema](./images/aws_solutionArchitectAssociate/sec-Shield-schema.png)      


[Documentation aws - Shield FAQs](https://aws.amazon.com/fr/shield/faqs/)      
AWS Shield Standard est activé automatiquement pour tous les clients AWS sans frais supplémentaires.   
AWS Shield Standard fonctionne sur Amazon Elastic Compute Cloud (Amazon EC2), Elastic Load Balancing (ELB), Amazon CloudFront, AWS Global Accelerator et Route 53      
AWS Shield Standard protège contre les attaques SYN/UDP flood, reflection attacks et d'autres sur les couches 3 et 4     

AWS Shield Advanced est un service payant optionnel ➡️ 3000 $ par mois         
AWS Shield Advanced offre des protections supplémentaires contre les attaques de plus grande ampleur et plus sophistiquées pour vos applications          
AWS Shield Advanced offre une surveillance permanente et basée sur les flux du trafic réseau et une surveillance active des applications pour fournir des notifications en temps quasi réel des attaques DDoS      
AWS Shield Advanced donnent un accès 24/7 au **DDoS ResponseTeam (DRT)** pour aider à la gestion dans la acs d'attaque DDoS     
AWS Shield Advanced protège la facturation suite aux attaques sur les ELB, cloudFront, Route53      



[Documentation aws -Shield specalized support](https://aws.amazon.com/fr/shield/features/#Specialized_support)    

![sec-Shield-examTip](./images/aws_solutionArchitectAssociate/sec-Shield-examTip.png)      







======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - FILTERING TRAFFIC WITH AWS WAF** 
</span>


[Documentation aws - AWS WAF](https://aws.amazon.com/fr/waf/)       
Protégez vos applications web contre les codes malveillants les plus répandus    

AWS WAF vous aide à vous protéger contre les codes et bots web malveillants les plus répandus qui peuvent affecter la disponibilité, compromettre la sécurité ou consommer des ressources excessives     

![sec-WAF-schema](./images/aws_solutionArchitectAssociate/sec-WAF-schema.png)     


**WAF** : **Web Application Firewall**     

Application qui permet de vous laisser monitorer les requêtes HTTP et HTTPS provenant de CloudFront ou d'un ELB  
AWS WAF agit sur la couche 7        
AWS WAF permet de controler les accès vers vos contenus     
  ➡️ autorise certaines IPs à faire des requêtes     
  ➡️ définit quels parametres de requêtes doivent être présent pour que la requête soit autorisée    
  ➡️ si non autorisé ➡️ HTTP 403    


AWS WAF a 3 comportements     
  ➡️ autorise TOUTES les requêtes SAUF celles définies     
  ➡️ bloque TOUTES les requêtes SAUF celles définies      
  ➡️ compte les requêtes qui match avec les propriétés définies     


Conditions à définir pour AWS WAF:      
  ➡️ IP ➡️ localisation source des requêtes        
  ➡️ region ➡️ localisation source des requêtes
  ➡️ parametres dans les headers des requêtes     
  ➡️ présence de code SQL ➡️ SQL injection     
  ➡️ presence de script ➡️ X-cross injection    
  ➡️ string dans la requête soit correspondre aux regex définies     

![sec-WAF-examTip](./images/aws_solutionArchitectAssociate/sec-WAF-examTip.png)     








======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - GUARDING YOUR NETWORK WITH GUARDUTY** 
</span>


[Documentation aws - AWS GuardDuty](https://aws.amazon.com/fr/guardduty/)      
Protéger vos comptes AWS avec la détection intelligente des menaces     

Amazon GuardDuty est un service de détection des menaces qui surveille en permanence vos charges de travail et vos comptes AWS pour détecter les activités malveillantes et fournir des résultats de sécurité détaillés pour la visibilité et la correction     

![sec-GuardDuty-schema](./images/aws_solutionArchitectAssociate/sec-GuardDuty-schema.png)      


GuardDuty fonctionne avec      
  - appels API inhabituels ou appels d'IP malicicous connues    
  - tente de désactiver les log cloudTrail    
  - deploiements non authorisés     
  - des instances compromises     
  - reconnaissance par des attaquants potentiels
  - scan des ports et des logins failed     


Fonctionnalité de GuardDuty:    
  - alertes dans GuardDuty console et dans les events CloudWatch    
  - reçoit des flux de tiers parties (proofpoint/crowdstrike) comme ceux de AWS Security sur des domaines ou IP suspects .....      
  - monitore les logs cloudTrail, les logs de flux VPC et les logs DNS      
  - centralise toutes les détections de menace des multiples accounts AWS     
  - envoie une réponse automatique via cloudWatch event ou Lambda      
  - fait du machine learning pour les s d'anomalies       


La baseline de détection du machine learning se base sur 7 à 14 jours de datas    
Une fois activée on retrouve ces comportements dans la console guardDuty et dans les events cloudWatch    

Les 30 premiers jours guardDuty est gratuit    
Son prix est ensuite calculé en fonction de la quantité de cloudTrail events et du volume de données du DNS et du flux VPC    


![sec-GuardDuty-examTip](./images/aws_solutionArchitectAssociate/sec-GuardDuty-examTip.png)     






[Differences entre CloudWatch et CloudTrail](https://www.geeksforgeeks.org/difference-between-aws-cloudwatch-and-aws-cloudtrail/)      


CloudWatch Logs génère des rapports sur les logs d'application, tandis que CloudTrail Logs vous fournit des informations spécifiques sur ce qui s'est passé dans votre compte AWS.     
CloudWatch Events est un flux d'événements système en temps quasi réel décrivant les modifications apportées à vos ressources AWS. CloudTrail se concentre davantage sur les appels d'API AWS effectués dans votre compte AWS.     






======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - CENTRALIZING WAF MANAGEMENT VIA AWS FIREWALL MANAGER** 
</span>

[Documentation aws - AWS Firewall Manager](https://aws.amazon.com/fr/firewall-manager/)       
Configurer et gérer de manière centralisée les règles de pare-feu sur vos comptes     

AWS Firewall Manager est un service de gestion de la sécurité qui vous permet de configurer et de gérer de manière centralisée les règles de pare-feu dans vos comptes et vos applications dans **AWS Organizations**       
Lorsque de nouvelles applications sont créées, Firewall Manager facilite également la mise en conformité des nouvelles applications et ressources en appliquant un ensemble commun de règles de sécurité    

![sec-FirewallManager-schema](./images/aws_solutionArchitectAssociate/sec-FirewallManager-schema.png)     


[Documentation aws - AWS Firewall Manager Developper guide](https://docs.aws.amazon.com/fr_fr/waf/latest/developerguide/fms-chapter.html)      


Firewall Manager permet de créer de nouvelles regles AWS WAF pour les applications LB, API gatways et amazon CloudFront.       


![sec-FirewallManager-examTip](./images/aws_solutionArchitectAssociate/sec-FirewallManager-examTip.png)    


======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - MONITORING S3 BUCKETS WITH MACIE** 
</span>


[Documentation aws - AWS Macie](https://aws.amazon.com/fr/macie/)      
Identifier et protéger vos données sensibles à grande échelle     

Amazon Macie est un service de sécurité et de confidentialité des données qui utilise le machine learning (ML) et la correspondance des modèles pour découvrir et protéger vos données sensibles      

![sec-Macie-schema](./images/aws_solutionArchitectAssociate/sec-Macie-schema.png)       


Macie est un service qui:      
- avec l'aide du machine learning va chercher si des données sensibles (données perso: santé, adresse, email, compte en banque....) sont stockées dans S3          
- alerte lors de buckets non ecryptés et des buckets publiques     
- alerte lorsque les buckets sont partagés avec des account en dehors de ceux définis dans AWS Organizations    


Les alertes de Macie sont:     
 - dans la console AWS     
 - dans eventBridge et peuvent être ajouter au SIEM perso     
 - peuvent être intégrées dans AWS Security Hub pour de l'analyse    
 - peuvent être intégrées dans d'autres services AWS pour automatiser des actions     





======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - SECURING OPERATING SYSTEMS WITH INSPECTOR** 
</span>


[Documentation aws - AWS Inspector](https://aws.amazon.com/fr/inspector/)     
Une gestion des vulnérabilités automatisée et continue à grande échelle      

Amazon Inspector est un service de gestion automatisée des vulnérabilités qui recherche en permanence les vulnérabilités logicielles et les expositions réseau involontaires dans les charges de travail AWS     

![sec-Inspector-schema](./images/aws_solutionArchitectAssociate/sec-Inspector-schema.png)     



AWS Inspector va analyser votre reseau et vos instances EC2 pour la recherche de vulnérabilités sur les applications ou de deviations des best practices         

Ce que va trouver AWS Inspector sera disponible dans un report depuis la console AWS Inspector ou depuis les APIs    

Pour les analyses reseaux ➡️ recherche des ports accessibles ddepuis et dehors du VPC    
  ➡️ pas besoin d'agent inspector installé     
Pour les analyses hosts ➡️ recherche de CVE, CIS benchmark et security best practices    
  ➡️ besoin d'installation agent inspector     




======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - MANAGING ENCRYPTION KEYS WITH KMS AND CLOUDHSM** 
</span>

[Documentation aws - Key Management Service (KMS)](https://aws.amazon.com/fr/kms/)      
Créez et contrôlez les clés utilisées pour chiffrer ou signer numériquement vos données     

**AWS Key Management Service (AWS KMS)** vous permet de créer, de gérer et de contrôler des clés cryptographiques dans vos applications et vos services AWS.     

![sec-KMS-schema](./images/aws_solutionArchitectAssociate/sec-KMS-schema.png)       


[Documentation aws - CloudHSM](https://aws.amazon.com/fr/cloudhsm/)     
Gérer les modules de sécurité matériels (HSM) à locataire unique sur AWS      

AWS CloudHSM vous aide à répondre aux exigences de conformité des entreprises, des contrats et des réglementations en matière de sécurité des données     

![sec-HSM-schema](./images/aws_solutionArchitectAssociate/sec-HSM-schema.png)     



[Documentation aws - KMS FAQs](https://aws.amazon.com/fr/kms/faqs/)      
AWS KMS est un service géré qui vous aide à créer et à contrôler plus facilement les clés utilisées pour les opérations cryptographiques     
Le service offre une solution hautement disponible de génération, de stockage, de gestion et d'audit de clés pour le chiffrement ou la signature numérique de vos données dans vos propres applications ou le contrôle du chiffrement des données dans les services AWS     


[Documentation aws - deleting KMS Keys](https://docs.aws.amazon.com/kms/latest/developerguide/deleting-keys.html)    


KMS est déjà intégré dans les services EBS, S3, RDS      
➡️ cela rend facile l'encryption des datas sur ces services à partir de vos clés d'encrytion    

KMS fournit un controle centralisé du cycle de vie et des permissions des clés     


**HSM** : **Hardware Security Module**    
Serveur physique qui conserve de manière sécurisée ces clés et les manage    
C'est lui qui crée l'encryption des datas à partir de la clé        
Le service AWS est cloudHSM   




**CMK**: **Customer Manage Key** ➡️ clé à partir de laquelle les clés d'encryption vont être générées    

3 manières de créer une CMK:     
 - aws s'en occupe à partir d'un HSM managé par AWS    
 - import de la clé material depuis votre propre clé de management d'infrastructure et vous l'associez en tant que CMK         
 - clé material générées et utilisées par aws cloudHsm dans un keystore aws kms    


Si c'est le choix 1 qui est pris ➡️ la rotation des clés est gérée par AWS   
Pour les autres choix ➡️ actions manuelles    

L'accès aux CMKs est géré via les policies      
Ces policies attachées à une identité IAM sont des **identity-based policies**    
Celles attachées à une ressources sont des **resource-based policies**
**Key policies**: il est OBLIGATOIRE d'attacher une resource-based policy à votre CMK     
  ➡️ chaque CMK a 1 key policy      
  

3 manières de controler les permissions:     
 - utilisation de la key policy ➡️ les accès sont définis dans la policy de la CMK     
 - utilisation d'une policy IAM + key policy ➡️ tous les accès peuvent être configurés dans la policy IAM    
 - utilisation de grant avec combinaison de key policy ➡️ si les accès sont autorisés dans le key policy une delagation doit autoriser les users à donner les accès à d'autres users    





======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - STORING YOUR SECRETS IN SECRETS MANAGER** 
</span>


[Documentation aws - secret manager](https://aws.amazon.com/fr/secrets-manager/)     
Gérer de manière centralisée le cycle de vie des secrets     

AWS Secrets Manager vous permet de gérer, récupérer et faire alterner les informations d'identification de base de données, des clés d'API et d'autres secrets tout au long de leur cycle de vie     
  ➡️ l'encryption se fait lors du transit     
  ➡️ les credentials ont une rotation automatique    



![sec-SecretManager-schema](./images/aws_solutionArchitectAssociate/sec-SecretManager-schema.png)     


[Documentation aws - secret manager user guide](https://docs.aws.amazon.com/fr_fr/secretsmanager/latest/userguide/intro.html)       
AWS Secrets Manager vous aide à gérer, récupérer et faire pivoter les informations d'identification des bases de données, les informations d'identification des applications, les jetons OAuth, les clés d'API et d'autres secrets tout au long de leur cycle de vie     



Quelles données peuvent être conservées ?     
 - RDS credentials    
 - credentials des databases autre que RDS    
 - tout type de secret qui peut être conservé en tant que clé/valeur ➡️ ssh key, api key...


⚠️⚠️      
Si la rotation de secrets est autorisé, à cette rotation se fait (tout de suite) un test pour valider la configuration ➡️ permet de valider que rien n'est codé en dur         
⚠️⚠️      





======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - STORING YOUR SECRETS IN PARAMETER STORE** 
</span>


[Documentation aws - AWS System Manager](https://aws.amazon.com/fr/systems-manager/)      
Gérez vos ressources sur AWS et dans des environnements multicloud et hybrides     

AWS Systems Manager est une solution de gestion sécurisée de bout en bout pour les ressources sur AWS et dans des environnements multicloud et hybrides    

![sec-SystemManager-schema](./images/aws_solutionArchitectAssociate/sec-SystemManager-schema.png)     


[Documentation aws - parameter store](https://docs.aws.amazon.com/fr_fr/systems-manager/latest/userguide/systems-manager-parameter-store.html)      
Parameter Store, une des fonctionnalités de AWS Systems Manager, offre un stockage sécurisé et hiérarchique des données pour la gestion des données de configuration et des codes secrets     
  ➡️ on peut conserver des datas comme password, AMI IDs, licence code.....

Parameter store est gratuit      


[Documentation aws - comment parameter store utilise KMS](https://docs.aws.amazon.com/fr_fr/kms/latest/developerguide/services-parameter-store.html)     



2 limitations pur Parameter Store:     
 - limitation à 10000 parametres     
 - pas de rotation de clés     


![sec-ParamStore-examTip](./images/aws_solutionArchitectAssociate/sec-ParamStore-examTip.png)      




======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - TEMPORARILY SHARING S3 OBJECTS USING PRESIGNED URLS OR COOKIES** 
</span>


[Documentation aws - Sharing objects with presigned URLs](https://docs.aws.amazon.com/AmazonS3/latest/userguide/ShareObjectPreSignedURL.html)     


[Documentation aws - Signed cookies](https://docs.aws.amazon.com/fr_fr/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-cookies.html)     
Les cookies signés CloudFront vous permettent de contrôler les personnes autorisées à accéder à votre contenu quand vous ne voulez pas modifier vos URL actives ou que vous ne voulez pas fournir l’accès à plusieurs fichiers restreints (par exemple, tous les fichiers de la section des abonnés d’un site web)     


[Documentation aws - mise en cache de constenu basé sur des cookies](https://docs.aws.amazon.com/fr_fr/AmazonCloudFront/latest/DeveloperGuide/Cookies.html)       


Tous les objets dans S3 sont privés par defaut.    
Le owner de chaque objet doit donner des permissions pour qu'on puisse accéder à ceux-ci    
Cependant, un owner d'objet peut partager cet objet avec d'autres accounts avec la creation d'URLs presigned qui utilisent leur propre security credentials pour donner accès (un temps limité) à deownload l'objet         

La creation d'une presigned url nécessite:         
 ➡️ fournir (par le owner de l'objet) ses security credentials     
 ➡️ spécifier le nom du bucket et la clé de l'objet      
 ➡️ indiquer la methode HTTP (GET pour download)     
 ➡️ indiquer une date et heure d'expiration     



Tous les accounts qui reçoivent cette URL presigned peuvent avoir accès à l'objet     

Pour créer une url presigned il faut:    
 - avec un S3 bucket (nom: totobucket)    
 - avec un fichier qu'on souhaite partéger dans ce bucket (nom: imageToto.jpg)    
    - si on tente de se connecter à l'url de l'image ➡️ a ccess denied car le bucket est privé    
 - se connecter depuis une instance EC2 (par forcément la même region que celle où le bucket se trouve) qui a un role qui permet la connexion au S3Bucket     
 - se connecter en ssh à cette instance  

```bash
## lister les buckets dont je suis owner
aws s3 ls 
#--------------------------
totobucket
#--------------------------

## connexion au bucket
aws s3 ls s3://totobucket
#--------------------------
imageToto.jpg
#--------------------------

## creation d'un url presigned 
aws s3 presign s3://totobucket/imageToto.jpg --expires-in 3600    ## 3600 secondes 
### génère une url
#--------------------------
https://totobucket.s3.amazonaws.com/imageToto.jpg?AWSAccessKeyId=<nomAccessKeyDeOwnerDuBucket>&Expires=<timestampDefini>-amz-security-token=<tokenGénéré>
#--------------------------
```
  - on copie cette url complete, on la colle dans un navigateur ➡️ on a le fichier qui s'affiche et on peut le downloader    



⚠️⚠️ Question souvent posée dans les certif: Comment partagé un fichier qui fait parti d'un private bucket ?      
   ➡️ avec une URL presigned     ⚠️⚠️       



Les presigned cookies sont utiles lorsqu'on souhaite partager une multitude de fichiers dans un bucket privé     

Le cookie sera donwloadé sur le PC du user et le user sera capable de naviguer dans la totalité des contenus     










======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - ADVANCED IAM POLICY DOCUMENTS** 
</span>



[Documentation aws - IAM policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html)     


.......................................................................................................................  

<span style="color: #11E3EA">

####  **SECURITY - IAM POLICIES - ARNS: AMAZON RESOURCE NAMES** 
</span>


[Documentation aws - ARN](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/reference-arns.html)     
Les noms ARN identifient de façon unique les ressources AWS      
L'ARN permet de spécifier une ressource sans aucune ambiguïté sur l'ensemble d'AWS, comme par exemple dans les politiques IAM, les balises Amazon Relational Database Service (Amazon RDS) et les appels d'API.      



**Format ARN**:          

```bash
arn:<partition>:<service>:<region>:<account-id>:<resource-id>
arn:<partition>:<service>:<region>:<account-id>:<resource-type>/<resource-id>
arn:<partition>:<service>:<region>:<account-id>:<resource-type>:<resource-id>
```
  ➡️ **partitiion**: Partition dans laquelle se trouve la ressource: groupe de region AWS    
    - `aws`         ➡️ aws regions
    - `aws-cn`      ➡️ regions chinoises      
    - `aws-us-gov`  ➡️ govcloud US


  ➡️ **service**: Espace de noms du service qui identifie le produit AWS              


  ➡️ **region**: Code de région ➡️ [liste endpoints region](https://docs.aws.amazon.com/general/latest/gr/rande.html#regional-endpoints)             


  ➡️ **account-id**: L'ID du compte AWS qui possède la ressource, sans les traits d'union            


  ➡️ **resource-type**: Type de ressource          


  ➡️ **resource-id**: Identificateur de la ressource ➡️  Il s'agit du nom de la ressource, de l'ID de la ressource ou d'un [chemin de ressource](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/reference-arns.html#arns-paths)         


> _Exemples_
> - Utilisateur IAM    `arn:aws:iam::123456789012:user/johndoe`
> - Rubrique SNS       `arn:aws:sns:us-east-1:123456789012:example-sns-topic-name`
> - VPC                `arn:aws:ec2:us-east-1:123456789012:vpc/vpc-0e9801d129EXAMPLE`





[Documentation aws - ARN: Identifiant IAM](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/reference_identifiers.html#identifiers-arns)     


_Syntaxe_:     

```bash
arn:aws:iam::account:root  
arn:aws:iam::account:user/user-name-with-path
arn:aws:iam::account:group/group-name-with-path
arn:aws:iam::account:role/role-name-with-path
arn:aws:iam::account:policy/policy-name-with-path
arn:aws:iam::account:instance-profile/instance-profile-name-with-path
arn:aws:sts::account:federated-user/user-name
arn:aws:sts::account:assumed-role/role-name/role-session-name
arn:aws:iam::account:mfa/virtual-device-name-with-path
arn:aws:iam::account:u2f/u2f-token-id
arn:aws:iam::account:server-certificate/certificate-name-with-path
arn:aws:iam::account:saml-provider/provider-name
arn:aws:iam::account:oidc-provider/provider-name
```

>_Exemples_:    
```bash
arn:aws:iam::123456789012:root
arn:aws:iam::123456789012:user/JohnDoe    ## utilisateur IAM dans le compte
arn:aws:iam::123456789012:user/division_abc/subdivision_xyz/JaneDoe   ## utilisateur avec un chemin reflétant un organigramme
arn:aws:iam::123456789012:group/Developers    ## groupe d'utilisateurs IAM
arn:aws:iam::123456789012:group/division_abc/subdivision_xyz/product_A/Developers   ## groupe d'utilisateurs IAM avec un chemin
arn:aws:iam::123456789012:role/S3Access   ## rôle IAM
arn:aws:iam::123456789012:role/application_abc/component_xyz/RDSAccess   
arn:aws:iam::123456789012:role/aws-service-role/access-analyzer.amazonaws.com/AWSServiceRoleForAccessAnalyzer ## rôle lié à un service 
arn:aws:iam::123456789012:role/service-role/QuickSightAction    # rôle de service#
arn:aws:iam::123456789012:policy/UsersManageOwnCredentials
arn:aws:iam::123456789012:policy/division_abc/subdivision_xyz/UsersManageOwnCredentials
arn:aws:iam::123456789012:instance-profile/Webserver
arn:aws:sts::123456789012:federated-user/JohnDoe
arn:aws:sts::123456789012:assumed-role/Accounting-Role/JaneDoe
arn:aws:iam::123456789012:mfa/JaneDoeMFA
arn:aws:iam::123456789012:u2f/user/JohnDoe/default (U2F security key)
arn:aws:iam::123456789012:server-certificate/ProdServerCert   ## certificat de serveur
arn:aws:iam::123456789012:server-certificate/division_abc/subdivision_xyz/ProdServerCert
arn:aws:iam::123456789012:saml-provider/ADFSProvider    ## Fournisseurs d'identité (SAML et OIDC)
arn:aws:iam::123456789012:oidc-provider/GoogleProvider    ## Fournisseurs d'identité (SAML et OIDC)
```



![sec-iam-arn](./images/aws_solutionArchitectAssociate/sec-iam-arn.png)       

.......................................................................................................................  

<span style="color: #11E3EA">

####  **SECURITY - IAM POLICIES - IAM POLICIES** 
</span>

![sec-iam-policyType](./images/aws_solutionArchitectAssociate/sec-iam-policyType.png)      

[Documentation aws - IAM policy syntax](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/reference_policies_grammar.html)      
[Documentation aws - Policy checker](https://docs.aws.amazon.com/IAM/latest/UserGuide/access-analyzer-reference-policy-checks.html)     


Un document policy est une liste de Statements (dans 1 fichier json).
Chaque statement correspond à 1 AWS API request (ex: créer une instance EC2 / ajouter un user / créer un VPC / ...)      





.......................................................................................................................  

<span style="color: #11E3EA">

####  **SECURITY - IAM POLICIES - PERMISSIONS BOUNDARIES** 
</span>


[Documentation aws - permissions boundaries](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_boundaries.html)     


AWS prend en charge les limites d'autorisations (permissions boundaries) pour les entités IAM (utilisateurs ou rôles).    
 
Une limite d'autorisations est une fonctionnalité avancée permettant d'utiliser une politique gérée pour définir les autorisations maximales qu'une politique basée sur l'identité peut accorder à une entité IAM.      
 
La limite d'autorisations d'une entité lui permet d'effectuer uniquement les actions autorisées à la fois par ses politiques basées sur l'identité et ses limites d'autorisations.     


![sec-iam-permBoundaries](./images/aws_solutionArchitectAssociate/sec-iam-permBoundaries.png)     






.......................................................................................................................  

<span style="color: #11E3EA">

####  **SECURITY - IAM POLICIES - EXAM TIPS** 
</span>


![sec-iam-examTip](./images/aws_solutionArchitectAssociate/sec-iam-examTip.png)      




.......................................................................................................................  

======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - AWS CERTIFICATE MANAGER** 
</span>


[Documentation aws - certificate manager](https://aws.amazon.com/fr/certificate-manager/)      
Provisionner et gérer les certificats SSL/TLS avec les services AWS et les ressources connectées    

Utilisez AWS Certificate Manager (ACM) pour mettre en service, gérer et déployer des certificats SSL/TLS publics et privés à utiliser avec les services AWS et avec vos ressources internes connectée    
ACM supprime le processus manuel chronophage d'achat, de chargement et de renouvellement des certificats SSL/TLS     
Le renouvellement de ces certificats est automatisé      
La mise en place de ces certificats est très facile: plus besoin de générer des paires de clés, de CSR    
  ➡️ la création du certificat SSL se fait en quelques clics depuis la console AWS      

![sec-ACM-schema](./images/aws_solutionArchitectAssociate/sec-ACM-schema.png)      



En utilisant ce service il n'est plus nécessaire de payer les certificats SSL      
  ➡️ on paie pour le service qui utilise les certifcats (ELB par ex)      


![sec-cert-examTip](./images/aws_solutionArchitectAssociate/sec-cert-examTip.png)     




======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - AUDITING CONTINUOUSLY WITH AWS AUDIT MANAGER** 
</span>



[Documentation aws - audit manager](https://aws.amazon.com/fr/audit-manager/)     
Auditez en permanence votre utilisation d'AWS pour simplifier l'évaluation des risques et de la conformité    

Utilisez AWS Audit Manager pour faire correspondre vos exigences de conformité aux données d'utilisation d'AWS grâce à des cadres préconçus et personnalisés et à la collecte automatisée de preuves     

![sec-auditManager-schema](./images/aws_solutionArchitectAssociate/sec-auditManager-schema.png)     


![sec-audit-useCase](./images/aws_solutionArchitectAssociate/sec-audit-useCase.png)      
![sec-audit-examTip](./images/aws_solutionArchitectAssociate/sec-audit-examTip.png)      







======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - DOWNLOAD COMPLIANCE DOCUMENTS FROM AWS ARTIFACT** 
</span>


[Documentation aws - AWS Artifact](https://aws.amazon.com/fr/artifact/)     
Accéder aux rapports de sécurité et de conformité d'AWS et des ISV (prévisualisation)     

AWS Artifact est votre ressource centrale à laquelle vous pouvez vous référer pour obtenir des informations importantes sur la conformité    
Le service fournit un accès à la demande aux rapports de sécurité et de conformité d'AWS et des ISV qui vendent leurs produits sur AWS Marketplace.    

![sec-Artifact-schema](./images/aws_solutionArchitectAssociate/sec-Artifact-schema.png)     




![sec-artifact-examTip](./images/aws_solutionArchitectAssociate/sec-artifact-examTip.png)     






======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - AUTHENTICATING ACCESS WITH AMAZON COGNITO** 
</span>


[Documentation aws - Cognito](https://aws.amazon.com/fr/cognito/)      
Mettez en œuvre une gestion des identités et des accès des clients sécurisée et transparente qui évolue    

Avec Amazon Cognito, vous pouvez ajouter des fonctions d'inscription et d'identification des utilisateurs et contrôler l'accès à vos applications web et mobiles     
Amazon Cognito fournit un magasin d'identités qui s'adapte à des millions d'utilisateurs, prend en charge la fédération d'identité sociale et d'entreprise, et offre des fonctions de sécurité avancées pour protéger vos consommateurs et votre entreprise     
Construit sur des normes d'identités ouvertes, Amazon Cognito prend en charge diverses réglementations de conformité et s'intègre aux ressources de développement frontales et dorsales    

![sec-Cognito-schema](./images/aws_solutionArchitectAssociate/sec-Cognito-schema.png)     


[Documentation aws - Cognito dev guide](https://docs.aws.amazon.com/fr_fr/cognito/latest/developerguide/what-is-amazon-cognito.html)          
Amazon Cognito est une plateforme d'identité pour les applications Web et mobiles.     
Il s'agit d'un annuaire d'utilisateurs, d'un serveur d'authentification et d'un service d'autorisation pour les jetons d'accès OAuth 2.0 et les informations d'identification AWS


![sec-cognito-features](./images/aws_solutionArchitectAssociate/sec-cognito-features.png)      
![sec-cognito-useCases](./images/aws_solutionArchitectAssociate/sec-cognito-useCases.png)      
![sec-cognito-pools](./images/aws_solutionArchitectAssociate/sec-cognito-pools.png)      
![sec-cognito-fonctionnement](./images/aws_solutionArchitectAssociate/sec-cognito-fonctionnement.png)      
![sec-cognito-sequence](./images/aws_solutionArchitectAssociate/sec-cognito-sequence.png)      
![sec-cognito-examTip](./images/aws_solutionArchitectAssociate/sec-cognito-examTip.png)      




======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - ANALYZING ROOT CAUSE USING AMAZON DETECTIVE** 
</span>


[Documentation aws - Amazon Detective](https://aws.amazon.com/fr/detective/)      
Analyser et visualiser les données de sécurité pour enquêter sur les problèmes de sécurité potentiels     

Amazon Detective simplifie le processus d'enquêtes et aide les équipes de sécurité à mener des enquêtes plus rapides et plus efficaces      
Avec les agrégations de données, les résumés et le contexte prédéfinis d'Amazon Detective, vous pouvez rapidement analyser et déterminer la nature et l'étendue des problèmes de sécurité possibles     


![sec-Detective-schema](./images/aws_solutionArchitectAssociate/sec-Detective-schema.png)     

![sec-detective-source](./images/aws_solutionArchitectAssociate/sec-detective-source.png)     
![sec-detective-useCase](./images/aws_solutionArchitectAssociate/sec-detective-useCase.png)     
![sec-Detective-schema](./images/aws_solutionArchitectAssociate/sec-detective-examTip.png)     





======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - PROTECTING VPCS WITH AWS NETWORK FIREWALL** 
</span>


[Documentation aws - network firewall](https://aws.amazon.com/fr/network-firewall/)         
Déployer la sécurité du pare-feu réseau sur vos VPCs      

Avec AWS Network Firewall, vous pouvez définir des règles de pare-feu qui permettent un contrôle précis du trafic réseau     
Network Firewall s'associe à AWS Firewall Manager, afin que vous puissiez concevoir des stratégies basées sur des règles AWS Network Firewall, puis les appliquer de manière centralisée sur vos clouds privés virtuels (VPC) et vos comptes    

![sec-NetworkFirewall-schema](./images/aws_solutionArchitectAssociate/sec-NetworkFirewall-schema.png)     



Lors de la certification si dans 1 question est demandée de déployer un firewall physique sur les VPCs ➡️ network firewall est la réponse         

Il comprend un moteur de regles firewall qui vous donne un controle complet sur votre trafic reseau, autorise des actions telles que les outbound server message block (SMB) requests à arrêterla propagation d'activité malicieuse    

Network Firewall a une infrastructure physique dans les data centers AWS ➡️ managé par AWS     

Il fournit une Intrusion Prevention System (IPS) qui vous donne une inspection de l'activité trafic     
  ➡️ si question à propos d'IPS ➡️ penser à network firewall     



![sec-networkFirewall-useCases](./images/aws_solutionArchitectAssociate/sec-networkFirewall-useCases.png)     

![sec-networkFirewall-examTip](./images/aws_solutionArchitectAssociate/sec-networkFirewall-examTip.png)     


======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - LEVERAGING AWS SECURITY HUB FOR COLLECTING SECURITY DATA** 
</span>


[Documentation aws - Security Hub](https://aws.amazon.com/fr/security-hub/)      
Automatiser les vérifications de sécurité AWS et centraliser les alertes de sécurité     

AWS Security Hub est un service de gestion de la posture de sécurité dans le cloud qui effectue des vérifications de bonnes pratiques, regroupe les alertes et permet l'utilisation de la correction automatisée    

![sec-SecHub-schema](./images/aws_solutionArchitectAssociate/sec-SecHub-schema.png)    

Security Hub est un service qui regroupe toutes les alertes de securité de Amazon Guard Duty, Amazon Inspector, Amazon Macie, AWS Firewall Manager en 1 SEUL EMPLACEMENT     

Il fonctionne à travers plusieurs accounts    


![sec-SecHub-useCases](./images/aws_solutionArchitectAssociate/sec-SecHub-useCases.png)     

[CIS](https://www.cisecurity.org/)       
[PCI DSS](https://www.globalsign.com/fr/blog/qu-est-ce-que-la-conformite-pci-dss)        




[Documentation aws - data protection](https://aws.amazon.com/fr/compliance/data-protection/)         
Bénéficier d'une protection des données complète dans le cloud     

[Documentation aws - security pillar](https://docs.aws.amazon.com/wellarchitected/latest/security-pillar/welcome.html)     

[Documentation aws - security lake](https://aws.amazon.com/fr/security-lake/)     
Centralisez automatiquement vos données de sécurité en quelques clics      

Amazon Security Lake centralise automatiquement les données de sécurité provenant de sources dans le cloud, sur site et personnalisées dans un lac de données créé à cet effet et stocké dans votre compte    
Avec Security Lake, vous pouvez obtenir une compréhension plus complète de vos données de sécurité dans toute votre organisation    
Vous pouvez également améliorer la protection de vos charges de travail, applications et données     
Security Lake a adopté le [Open Cybersecurity Schema Framework (OCSF)](https://github.com/ocsf), une norme ouverte     
Avec le support de l'OCSF, le service peut normaliser et combiner les données de sécurité provenant d'AWS et d'un large éventail de sources de données de sécurité d'entreprise     

![sec-SecLake-schema](./images/aws_solutionArchitectAssociate/sec-SecLake-schema.png)     

[Documentation aws - data privacy FAQs](https://aws.amazon.com/fr/compliance/data-privacy-faq/)     





![sec-SecHub-examTip](./images/aws_solutionArchitectAssociate/sec-SecHub-examTip.png)     



======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - EXAM TIPS** 
</span>


![sec-examTips01](./images/aws_solutionArchitectAssociate/sec-examTips01.png)     
![sec-examTips02](./images/aws_solutionArchitectAssociate/sec-examTips02.png)     
![sec-examTips03](./images/aws_solutionArchitectAssociate/sec-examTips03.png)     
![sec-examTips04](./images/aws_solutionArchitectAssociate/sec-examTips04.png)     
![sec-examTips05](./images/aws_solutionArchitectAssociate/sec-examTips05.png)     
![sec-examTips06](./images/aws_solutionArchitectAssociate/sec-examTips06.png)     
![sec-examTips07](./images/aws_solutionArchitectAssociate/sec-examTips07.png)     
![sec-examTips08](./images/aws_solutionArchitectAssociate/sec-examTips08.png)     
![sec-examTips09](./images/aws_solutionArchitectAssociate/sec-examTips09.png)     
![sec-examTips10](./images/aws_solutionArchitectAssociate/sec-examTips10.png)      
![sec-examTips11](./images/aws_solutionArchitectAssociate/sec-examTips11.png)     
![sec-examTips12](./images/aws_solutionArchitectAssociate/sec-examTips12.png)     
![sec-examTips13](./images/aws_solutionArchitectAssociate/sec-examTips13.png)     
![sec-examTips14](./images/aws_solutionArchitectAssociate/sec-examTips14.png)     
![sec-examTips15](./images/aws_solutionArchitectAssociate/sec-examTips15.png)     
![sec-examTips16](./images/aws_solutionArchitectAssociate/sec-examTips16.png)     
![sec-examTips17](./images/aws_solutionArchitectAssociate/sec-examTips17.png)     
![sec-examTips18](./images/aws_solutionArchitectAssociate/sec-examTips18.png)     
![sec-examTips19](./images/aws_solutionArchitectAssociate/sec-examTips19.png)     
![sec-examTips20](./images/aws_solutionArchitectAssociate/sec-examTips20.png)       
![sec-examTips21](./images/aws_solutionArchitectAssociate/sec-examTips21.png)     
![sec-examTips22](./images/aws_solutionArchitectAssociate/sec-examTips22.png)     
![sec-examTips23](./images/aws_solutionArchitectAssociate/sec-examTips23.png)     
![sec-examTips24](./images/aws_solutionArchitectAssociate/sec-examTips24.png)     
![sec-examTips25](./images/aws_solutionArchitectAssociate/sec-examTips25.png)     
![sec-examTips26](./images/aws_solutionArchitectAssociate/sec-examTips26.png) 







======================================================================================================================= 


<span style="color: #EA9811">

###  **SECURITY - QUIZZ** 
</span>



![sec-quizz01](./images/aws_solutionArchitectAssociate/sec-quizz01.png)        
![sec-quizz02](./images/aws_solutionArchitectAssociate/sec-quizz02.png)        
![sec-quizz03](./images/aws_solutionArchitectAssociate/sec-quizz03.png)        
![sec-quizz04](./images/aws_solutionArchitectAssociate/sec-quizz04.png)        
![sec-quizz05](./images/aws_solutionArchitectAssociate/sec-quizz05.png)    
![sec-quizz06](./images/aws_solutionArchitectAssociate/sec-quizz06.png)    
![sec-quizz07](./images/aws_solutionArchitectAssociate/sec-quizz07.png)    
![sec-quizz08](./images/aws_solutionArchitectAssociate/sec-quizz08.png)        




**QUIZZ REDO**      







======================================================================================================================= 


_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **AUTOMATION** 
</span>



[Documentation aws - AWS System Manager - User Guide](https://docs.aws.amazon.com/fr_fr/systems-manager/latest/userguide/systems-manager-automation.html)     

Automation, une fonctionnalité de AWS Systems Manager, simplifie les tâches courantes de maintenance, de déploiement et de correction pour des Services AWS comme Amazon Elastic Compute Cloud (Amazon EC2), Amazon Relational Database Service (Amazon RDS), Amazon Redshift, Amazon Simple Storage Service (Amazon S3) et bien d'autres     






======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - WHY DO WE AUTOMATE ?** 
</span>


L'automatisation est faite pour permettre de ne plus faire de manière manuelles des actions répétitives qui prennent du temps ET qui peuvent engendrer des erreurs      


Pour cela AWS propose 3 services qui permettent de mettre en place de l'automatisation     
- **cloudformation**     
- **elastic Beantstalk**      
- **system manager**    


![automate-3ServicesForAutomation](./images/aws_solutionArchitectAssociate/automate-3ServicesForAutomation.png)     


![automate-overview-examTip](./images/aws_solutionArchitectAssociate/automate-overview-examTip.png)    
![automate-overview-examTip02](./images/aws_solutionArchitectAssociate/automate-overview-examTip02.png)    




➡️


======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - CLOUDFORMATION** 
</span>


[Documentation aws - CloudFormation](https://aws.amazon.com/fr/cloudformation/)      
Accélérez le provisionnement cloud avec l'infrastructure en tant que code      

AWS CloudFormation vous permet de modéliser, de provisionner et de gérer les ressources AWS et tierces en traitant l'infrastructure en tant que code     

![automate-CloudFormation-schema](./images/aws_solutionArchitectAssociate/automate-CloudFormation-schema.png)     


AWS CloudFormation est un service qui permet aux développeurs et aux entreprises de créer facilement un ensemble de ressources AWS et tierces liées, de les mettre en service et de les gérer de manière ordonnée et prévisible    


1000 opération de gestionnaire par moi et par compte gratuit     



**CloudFroamtion Template**                 
```yaml
AWSTemplateFormatVersion: 2010-09-09
Description: CloudFormation Demo
Parameters:
  InstanceTypeParameter:
    Type: String
    Default: t2.micro
    AllowedValues:
      - t2.micro
      - m1.small
      - m1.large
    Description: Enter t2.micro, m1.small, or m1.large. Default is t2.micro.
Mappings:
  SubnetConfig:
    VPC:
      CIDR: 10.0.0.0/16
    Public1:
      CIDR: 10.0.0.0/24
    Public2:
      CIDR: 10.0.1.0/24
    Public3:
      CIDR: 10.0.2.0/24
Resources:
  VPC:
    Type: 'AWS::EC2::VPC'
    Properties:
      EnableDnsSupport: 'true'
      EnableDnsHostnames: 'true'
      CidrBlock: !FindInMap 
        - SubnetConfig
        - VPC
        - CIDR
      Tags:
        - Key: Name
          Value: ACloudGuru
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: VPC
  PublicSubnet1:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: 
        Fn::Select: 
          - 0
          - Fn::GetAZs: ""
      CidrBlock: !FindInMap 
        - SubnetConfig
        - Public1
        - CIDR
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public1
      MapPublicIpOnLaunch: true
  PublicSubnet2:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: 
        Fn::Select: 
          - 1
          - Fn::GetAZs: ""
      CidrBlock: !FindInMap 
        - SubnetConfig
        - Public2
        - CIDR
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public2
      MapPublicIpOnLaunch: true
  InternetGateway:
    Type: 'AWS::EC2::InternetGateway'
    Properties:
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  GatewayToInternet:
    Type: 'AWS::EC2::VPCGatewayAttachment'
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway
  PublicRouteTable:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  PublicRoute:
    Type: 'AWS::EC2::Route'
    DependsOn: GatewayToInternet
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
  PublicSubnetRouteTableAssociation1:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet1
      RouteTableId: !Ref PublicRouteTable
  PublicSubnetRouteTableAssociation2:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet2
      RouteTableId: !Ref PublicRouteTable
  PublicNetworkAcl:
    Type: 'AWS::EC2::NetworkAcl'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  InboundHTTPPublicNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref PublicNetworkAcl
      RuleNumber: '100'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '80'
        To: '80'
  InboundHTTPSPublicNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref PublicNetworkAcl
      RuleNumber: '101'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '443'
        To: '443'
  InboundSSHPublicNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref PublicNetworkAcl
      RuleNumber: '102'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '22'
        To: '22'
  InboundEmphemeralPublicNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref PublicNetworkAcl
      RuleNumber: '103'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '1024'
        To: '65535'
  OutboundPublicNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref PublicNetworkAcl
      RuleNumber: '100'
      Protocol: '6'
      RuleAction: allow
      Egress: 'true'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '0'
        To: '65535'
  PublicSubnetNetworkAclAssociation1:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet1
      NetworkAclId: !Ref PublicNetworkAcl
  PublicSubnetNetworkAclAssociation2:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet2
      NetworkAclId: !Ref PublicNetworkAcl
  BastionEC2: 
    Type: AWS::EC2::Instance
    Properties: 
      ImageId: "ami-0e999cbd62129e3b1"
      InstanceType: !Ref InstanceTypeParameter
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash -xe
            yum install -y ec2-instance-connect
            curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
            install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
            curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
            sudo mv /tmp/eksctl /usr/local/bin
      NetworkInterfaces: 
      - AssociatePublicIpAddress: "true"
        DeviceIndex: "0"
        GroupSet: 
          - Ref: "BastionSG"
        SubnetId: !Ref PublicSubnet1
      Tags:
        - Key: Name
          Value: CLI Host
      IamInstanceProfile: !Ref RootInstanceProfile
    DependsOn: VPC
  BastionSG:
    Type: AWS::EC2::SecurityGroup
    Properties:
        GroupDescription: Allow ssh to client host
        VpcId: !Ref VPC
        SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: 0.0.0.0/0
  RootRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      Policies:
        - PolicyName: root
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: Allow
                Action: '*'
                Resource: '*'
  RootInstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Path: /
      Roles:
        - !Ref RootRole
```



Il est nécessaire d'érire du code avec CloudFormation ➡️ **yaml** ou **json** ces 2 formats de fichiers sont acceptés     

Il s'agit d'un fichier "document" qui permet de décrire les services utilisés (dans la partie `resources`) avec ses caractéristiques et ses dépendances     


![automate-CloudFormation-overview](./images/aws_solutionArchitectAssociate/automate-CloudFormation-overview.png)     


[Documentation aws - CloudFormation Template - utilisation](https://docs.aws.amazon.com/fr_fr/AWSCloudFormation/latest/UserGuide/template-guide.html)     


**Creation d'1 stack avec CloudFromation**    

1. Dans la console AWS aller dans le service **cloudformation**             
2. Cliquer sur `Stacks/Create Stack`                
3. Si nous avons déjà créer le fichier (fichier template décrit ci-dessus), on importe ce fichier               
  `Template is ready` + `upload a templarte file` (ex: ce template se nomme cloud.cfn)               
4. Cliquer sur **Next** et nommer cette stack                        
    Ensuite il s'agit de la partie **Parameter**           
    ➡️ définir l'instanceTypeParameter qui sera l'instance type par defaut pour la creation d'instance avec ce template     
    ➡️ la facturation de l'utilisation de cloudfroamtion est calculée par rapport aux instances utilisées    
5. Partie **Configure Stack Options**    
    ➡️ nous ne touchons à rien   
6. **Review <nomTemplateDonnéPrécédemment>**      
    ➡️ une url S3bucket est disponible    ➡️ nous conservons ce template dans un bucket S3    
    ➡️ en bas de page apparait un cadre (en bleu) indiquant que cloudFormation va devoir créer une resource IAM  
      ➡️ un role doit être créé pour utiliser cloudFormation    


![automate-CloudFormation-examTips01](./images/aws_solutionArchitectAssociate/automate-CloudFormation-examTips01.png)      
![automate-CloudFormation-examTips02](./images/aws_solutionArchitectAssociate/automate-CloudFormation-examTips02.png)      





======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - ELASTIC BEANSTALK** 
</span>


[Documentation aws - AWS Elastic Beanstalk](https://aws.amazon.com/fr/elasticbeanstalk/)      
Déployer et mettre à l'échelle des applications web     

AWS Elastic Beanstalk déploie des applications Web afin que vous puissiez vous concentrer sur votre activité.     

![automate-Beanstalk-schema](./images/aws_solutionArchitectAssociate/automate-Beanstalk-schema.png)      


AWS Elastic Beanstalk est le moyen le plus rapide pour rendre opérationnel vos applications web sur AWS.      
Il vous suffit de charger le code de votre application, et le service s'occupe automatiquement de détails tels que l'approvisionnement en ressources, la répartition des charges, la scalabilité automatique et la surveillance.      
Elastic Beanstalk est idéal si vous disposez d'une application Web PHP, Java, Python, Ruby, Node.js, .NET, Go ou Docker          
Elastic Beanstalk utilise les principaux services AWS tels que Amazon Elastic Compute Cloud (EC2), Amazon Elastic Container Service (ECS), AWS Auto Scaling et Elastic Load Balancing (ELB) pour prendre facilement en charge les applications qui doivent être mises à l'échelle pour servir des millions d'utilisateurs.       


AWS Elastic Beanstalk ne génère pas de frais supplémentaires       



**Elastic BeanStalk Demo Instructions**     
```bash
# Lesson: Elastic Beanstalk
# URL: https://learn.acloud.guru/course/certified-solutions-architect-associate/learn/21359b7a-a8b4-4d08-8e30-48623869199f/e3662bd5-361e-43a7-9afc-c2c2f5c921e2/watch

# To create your example application, you'll use the 'Create application' console wizard. It creates an Elastic Beanstalk application and launches an environment within it. An environment is the collection of AWS resources required to run your application code.

# To create a service role for Elastic Beanstalk:

1. Open the Identity and Access Management (IAM) console         
2. Click "Roles"           
3. Click "Create role"               
4. Under "Use cases for other AWS services", choose "Elastic Beanstalk" et selectionner "Elastic Beanstalk - Customizable"      
5. Click "Next"        
6. For "Role name", type "CustomServiceRoleForElasticBeanstalk"         
7. Click "Create role"            

# To create an EC2 instance profile for Elastic Beanstalk:

1. Open the Identity and Access Management (IAM) console         
2. Click "Roles"          
3. Click "Create role"          
4. Under "Common use cases", choose "EC2".
5. Click "Next".
6. Select the "AWSElasticBeanstalkReadOnly" policy name.
7. Click "Next".
8. For "Role name", type "CustomEC2InstanceProfileForElasticBeanstalk".
9. Click "Create role".

# To create an example application:

1. Open the Elastic Beanstalk console.
2. Click "Create application".
3. For Application name enter "beansarefun".
4. For Platform, choose "Python".
5. Click "Next".
6. For "Use an existing service role", choose "CustomServiceRoleForElasticBeanstalk".
7. For "EC2 instance profile", choose CustomEC2InstanceProfileForElasticBeanstalk".               
8. Click "Next".
9. On the "Set up networking, database, and tags - optional" page, click "Next".
10. On the "Configure instance traffic and scaling - optional" page, click "Next".
11. On the "Configure updates, monitoring, and logging - optional" page, under "System" choose "Basic", and under "Managed platform updates" > "Managed updates", uncheck the "Activated" box. Click "Next".
12. The Review page displays a summary of all your choices. Choose "Submit" at the bottom of the page.
13. Wait for the "Successfully launched environment: Beansarefun-env" event and check the URL available under "Environment overview" > "Domain".
```


**What is a PaaS ?**   
  ➡️ **PaaS**: **Platform as a Service**    
  ➡️ environnement de développement et de déploiement complet dans le cloud, avec les ressources nécessaires pour vous permettre de fournir n'importe quel service, de la simple application cloud aux applications d'entreprise sophistiquées


![automate-Beanstalk-overview](./images/aws_solutionArchitectAssociate/automate-Beanstalk-overview.png)     


![automate-Beanstalk-examTips01](./images/aws_solutionArchitectAssociate/automate-Beanstalk-examTips01.png)    
![automate-Beanstalk-examTips02](./images/aws_solutionArchitectAssociate/automate-Beanstalk-examTips02.png)    





======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - SYSTEM MANAGER** 
</span>




[Documentation aws - System Manager](https://docs.aws.amazon.com/fr_fr/systems-manager/index.html)     
AWS Systems Manager vous permet d'organiser, de surveiller et d'automatiser des tâches de gestion sur vos ressources AWS     


**System Manger** est une suite d'outils qui permet de voir, contrôler et automatiser les ressources sur les architecture AWS et on-premise  ➡️ ce n'est pas que pour le cloud      

C'est gratuit     


![automate-sysManger-overview](./images/aws_solutionArchitectAssociate/automate-sysManger-overview.png)      


➡️ Il faut pour utiliser ce service que les agents soient déployés sur les instances     


Dans la `console AWS System Manager / Fleet Manager`   ➡️ une fois l'agent déployé il faut donner un role à l'instance pour qu'elle communique avec le service System Manager     


![automate-sysManger-examTips01](./images/aws_solutionArchitectAssociate/automate-sysManger-examTips01.png)     
![automate-sysManger-examTips02](./images/aws_solutionArchitectAssociate/automate-sysManger-examTips02.png)     






======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - EXAM TIPS** 
</span>


![automate-examTips01](./images/aws_solutionArchitectAssociate/automate-examTips01.png)             
![automate-examTips02](./images/aws_solutionArchitectAssociate/automate-examTips02.png)             
![automate-examTips03](./images/aws_solutionArchitectAssociate/automate-examTips03.png)             
      






======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - LAB - GETTING STARTED WITH CLOUDFORMATION** 
</span>




![automate-lab-enonce](./images/aws_solutionArchitectAssociate/labs/automate-lab/automate-lab-enonce.png)     



AWS account                   
Username: cloud_user                     
password: h7Gl^)0I]Ey5sQyr7RZ-                 
link: https://821557853410.signin.aws.amazon.com/console?region=us-east-1       


[github - course-Certified-Solutions-Architect-Associate](https://github.com/ACloudGuru-Resources/Course-Certified-Solutions-Architect-Associate/tree/master/labs/getting_started_with_cfn)     


[Reférences de types de propirété et de ressources AWS](https://docs.aws.amazon.com/fr_fr/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)     




![automate-lab-scema](./images/aws_solutionArchitectAssociate/labs/automate-lab/automate-lab-scema.png)     



Log in to the live AWS environment using the credentials provided, and make sure you are in the us-east-1 (N. Virginia) region.

The CloudFormation templates and other hands-on lab files can be found on GitHub. Navigate to the needed file and choose the RAW view. From there, use the Save As... functionality in your browser.

A list of AWS resources and what happens when updates occur can be found online.


**Create a CloudFormation Stack**    

1. Download the ``createstack.json` ` file by right-clicking and selecting Save As functionality.       

2. In the AWS console, navigate to CloudFormation.    

3. Click Create stack > With new resources (standard).       

4. Select Template is ready.        

5. Select Upload a template file.       

6. Click Choose file.      

7. Browse to the ``createstack.json` ` file you downloaded and saved.    

8. Select and upload it, and click Next.        

9. Name the stack _cfnlab_.      

10. Click Next.       

11. Scroll through the available stack options, leaving them all at the defaults, and click Next.      

12. Review your selections and click Create stack.       

13. Refresh the page to watch the progress.          

14. Navigate to S3. We didn't specify a name in the json file for this bucket, so AWS names it with the `<STACK_NAME>-<LOGICAL_VOLUME_NAME>-<RANDOM_STRING>` format. Yours will be: `cfnlab-catpics-<RANDOM_STRING>`.  


**Update the CloudFormation Stack**     

**Compare the Original and Updated Templates**     

1. Download and save the ``updatestack1.json`` and `updatestack2.json` files like you did for ``createstack.json` `.    

2. Open the `createstack.json` , `updatestack1.json`, and `updatestack2.json` files in a text editor.

3. Compare the contents of the `createstack.json`  and `updatestack1.json` files, focusing on the differences in the Resources section. You should see that the `updatestack1.json` file contains an additional logical resource named "`dogpics`".

4. Compare the contents of the `createstack.json`  and `updatestack2.json` files, once again focusing on the differences in the Resources section. You should see that the `updatestack2.json` file contains an additional logical resource named "`dogpics`" and includes a bucket name of "`catsareawesome123`" for the "catpics" resource.


**Update #1**     

1. Navigate to CloudFormation.

2. Select the `cfnlab` stack, and click Update.

3. Select Replace current template.

4. Select Upload a template file.

5. Click Choose file, and select and upload `updatestack1.json`.

6. Click Next > Next > Next.   

7. In the Change set preview section, review the changes that will be made based on the `updatestack1.json` template. You should be adding the "`dogpics`" resource.    

8. Click Update stack.    

9. Refresh the page to watch the progress.    

10. Once it's finished updating, navigate to S3. You should see the new `dogpics` bucket.

**Remove the Update**   

1. Navigate back to CloudFormation.

2. Select the `cfnlab` stack, and click Update.

3. Select Replace current template.    

4. Select Upload a template file.    

5. Click Choose file, and select and upload `createstack.json`  again.    

6. Click Next > Next > Next.   

7. In the Change set preview section, review the changes that will be made based on the `createstack.json`  template. You should be removing the "`dogpics`" resource.     

8. Click Update stack.     

9. Refresh the page to watch the progress.     

10. Once it's finished updating, navigate to S3. You should see the `dogpics` bucket is now gone.    

**Update #2** 

1. Navigate to the `updatestack2.json` file that is open in the text editor.    

2. Change the 123 characters in `catsareawesome123` to something unique (e.g., your birthday and today's date).

3. Save the file.   

4. In the CloudFormation console, select the cfnlab stack, and click Update.   

5. Select Replace current template.   

6. Select Upload a template file.   

7. Click Choose file, and select and upload `updatestack2.json`.   

8. Click Next > Next > Next.   

9. In the Change set preview section, review the changes that will be made based on the `updatestack2.json` template. You should be modifying the "catpics" resource and adding the "`dogpics`" resource.   

10. Click Update stack.    

11. Refresh the page to watch the progress.  

12. Once it's finished updating, navigate to S3. You should see 2 changes: The `dogpics` bucket is back, and the catpics bucket has been replaced with the `catsareawesome` bucket.   

**Delete CloudFormation Stack**  

1. Navigate to CloudFormation.

2. Select `cfnlab`.  

3. Click Delete.

4. In the dialog box, click Delete stack.  

5. Click the `cfnlab`, and then click the Events tab to see the resources being deleted.   

6. Once it's all done, navigate to S3. You should see all the `cfnlab` buckets are gone, as well as the `catsareawesome` bucket.






======================================================================================================================= 


<span style="color: #EA9811">

###  **AUTOMATION - QUIZZ** 
</span>



![automate-quizz01](./images/aws_solutionArchitectAssociate/automate-quizz01.png)       
![automate-quizz02](./images/aws_solutionArchitectAssociate/automate-quizz02.png)       
![automate-quizz03](./images/aws_solutionArchitectAssociate/automate-quizz03.png)       
![automate-quizz04](./images/aws_solutionArchitectAssociate/automate-quizz04.png)       
![automate-quizz05](./images/aws_solutionArchitectAssociate/automate-quizz05.png)       



**QUIZZ REDO**      




======================================================================================================================= 

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **CACHING** 
</span>



[Documentation aws - elasticache](https://aws.amazon.com/fr/elasticache/)     
Des performances en temps réel pour des applications en temps réel                 

Amazon ElastiCache est un service entièrement géré, compatible avec Redis et Memcache, qui fournit des performances en temps réel et optimisées en termes de coûts pour les applications modernes             
ElastiCache prend en charge des centaines de millions d'opérations par seconde avec un temps de réponse de l'ordre de la microseconde, et offre une sécurité et une fiabilité de niveau professionnel      


![aws-elasticache-schema](./images/aws_solutionArchitectAssociate/aws-elasticache-schema.png)     


[Documentation aws - cloudfront](https://aws.amazon.com/fr/cloudfront/)       
Diffuser du contenu en toute sécurité avec une faible latence et des vitesses de transfert élevées             

Amazon CloudFront est un réseau de diffusion de contenu (CDN) conçu pour des performances élevées, pour la sécurité et pour la simplicité de développement         

![aws-cache-cloudfront-schema](./images/aws_solutionArchitectAssociate/aws-cache-cloudfront-schema.png)         


[Documentation aws - caching solutions](https://aws.amazon.com/caching/aws-caching/)  


[Documentation aws - CloudWatch](https://aws.amazon.com/cloudwatch/)        
Observez et surveillez les ressources et les applications sur AWS, sur site et sur d'autres clouds            

Amazon CloudWatch collecte et visualise les journaux, les métriques et les données d'événements en temps réel dans des tableaux de bord automatisés afin de rationaliser la maintenance de votre infrastructure et de vos applications        

![aws-cloudWatch-schema](./images/aws_solutionArchitectAssociate/aws-cloudWatch-schema.png)     




[Documentation aws - DynamoDb Accelerator: DAX](https://aws.amazon.com/dynamodb/dax/)     
Amazon DynamoDB Accelerator (DAX) est un cache en mémoire entièrement géré et hautement disponible pour Amazon DynamoDB qui offre jusqu'à 10 fois plus de performances, de quelques millisecondes à quelques microsecondes, même pour des millions de requêtes par seconde           



[Documentation aws - redis vs Memcached](https://aws.amazon.com/elasticache/redis-vs-memcached/)     
Sélectionnez le magasin de données en mémoire (memory data store) qui répond à vos besoins.            



======================================================================================================================= 


<span style="color: #EA9811">

###  **CACHING - OVERVIEW** 
</span>


Les caches permettent un gain de temps lors des requêtes faites par le user      
Ces caches sont conservés en un lieu "plus propre" pour le gain de temps     
- **external** ➡️ chez le user directement (sur le reseau proche du user)               
- **internal** ➡️ dans un outils de cache pour une accélration des databases (moins de requêtes aux databases ➡️ meilleure solution)

  ➡️ dans le 2e cas, on va conserver les requêtes les plus courantes aux databases avec leurs réponses     


![aws-cache-overview-options](./images/aws_solutionArchitectAssociate/aws-cache-overview-options.png)       


![aws-cache-overview-examTips01](./images/aws_solutionArchitectAssociate/aws-cache-overview-examTips01.png)     
![aws-cache-overview-examTips02](./images/aws_solutionArchitectAssociate/aws-cache-overview-examTips02.png)     



======================================================================================================================= 


<span style="color: #EA9811">

###  **CACHING - GLOBAL CACHING WITH CLOUDFRONT** 
</span>


[Documentation aws - cloudfront developer guide](https://docs.aws.amazon.com/fr_fr/AmazonCloudFront/latest/DeveloperGuide/Introduction.html)     

**CloudFront** est un **Content Delivery Network CDN**     

Amazon CloudFront est un service web qui accélère la distribution de vos contenus web statiques et dynamiques, tels que les fichiers .html, .css, .js et image, aux utilisateurs       
CloudFront diffuse votre contenu au travers d'un réseau mondial de centres de données appelés emplacements périphériques      
Lorsqu'un utilisateur demande le contenu que vous proposez avec CloudFront, la demande est dirigée vers l'emplacement périphérique qui fournit la latence la plus faible et, par conséquent, le contenu est remis avec les meilleures performances possibles       



Il utilise **aws edge locations** pour réduire la latence durant les transferts    
[Documentation - aws edge locations](https://www.lastweekinaws.com/blog/what-is-an-edge-location-in-aws-a-simple-explanation/)      
**Edge locations** sont des centres de données AWS conçus pour fournir des services avec la latence la plus faible possible       
Peut mettre en place des regles de filtrages (seulement _Allow_ ou _Deny_) ➡️ préférable d'utiliser le WAF  


![aws-cache-cloudfront-settings](./images/aws_solutionArchitectAssociate/aws-cache-cloudfront-settings.png)     


Lors de la création de cloudfront il est possible de restreindre les cookies, durée d'utilisation du cache, les urls accessibles       

![aws-cache-cloudfront-xamTips01](./images/aws_solutionArchitectAssociate/aws-cache-cloudfront-xamTips01.png)       
![aws-cache-cloudfront-xamTips02](./images/aws_solutionArchitectAssociate/aws-cache-cloudfront-xamTips02.png)       




======================================================================================================================= 


<span style="color: #EA9811">

###  **CACHING - CACHING YOUR DATA WITH ELASTICACHE AND DAX** 
</span>



**ElastiCache**               
Une version de 2 sources opensource managée: **memcache** et **redis**     



![aws-cache-memcacheVsRedis](./images/aws_solutionArchitectAssociate/aws-cache-memcacheVsRedis.png)      
![aws-cache-Dax](./images/aws_solutionArchitectAssociate/aws-cache-Dax.png)        

![aws-cache-Type](./images/aws_solutionArchitectAssociate/aws-cache-Type.png)        



![aws-cache-examTips01](./images/aws_solutionArchitectAssociate/aws-cache-examTips01.png)              
![aws-cache-examTips02](./images/aws_solutionArchitectAssociate/aws-cache-examTips02.png)              





======================================================================================================================= 


<span style="color: #EA9811">

###  **CACHING - FIXING IP CACHING WITH GLOBAL ACCELERATOR** 
</span>


[Documentation aws - global accelerator](https://aws.amazon.com/fr/global-accelerator/)             
Améliorez la disponibilité, les performances et la sécurité de vos applications en utilisant le réseau mondial d'AWS      

AWS Global Accelerator est un service de mise en réseau qui vous aide à améliorer la disponibilité, les performances et la sécurité de vos applications publiques         
Global Accelerator fournit deux IP publiques statiques mondiales qui servent de point de terminaison fixe à vos applications, telles que les Application Load Balancer, les Network Load Balancer, les instances Amazon Elastic Compute Cloud (EC2) et les IP élastiques      

![aws-cache-globalAccelerator-schema](./images/aws_solutionArchitectAssociate/aws-cache-globalAccelerator-schema.png)                 


Il est situé en front de votre application (front au ELB)          



Il permet de masquer les architectures complexes  ➡️ 1 à 2 IPS en cible pour le user (tout ce qu'il se passe derrière lui est géré par AWS)              
Il accelere le traffic       
Il est possible de créer des "groupes pondérés" (weighted groups) derrière les IPs pour tester une nouvelle version SANS casser votre environnement        

En fonction de la region d'où on se connecte dans le monde, global accelerator va se connecter au endpoint le plus proche (en fonction de ce qui sont configurés)    


![aws-cache-globalAccelerator-examTips01](./images/aws_solutionArchitectAssociate/aws-cache-globalAccelerator-examTips01.png)          
![aws-cache-globalAccelerator-examTips02](./images/aws_solutionArchitectAssociate/aws-cache-globalAccelerator-examTips02.png)          

======================================================================================================================= 


<span style="color: #EA9811">

###  **CACHING - EXAM TIPS** 
</span>



![aws-cache-examTips01](./images/aws_solutionArchitectAssociate/aws-cache-examTips11.png)          
![aws-cache-examTips02](./images/aws_solutionArchitectAssociate/aws-cache-examTips12.png)          
![aws-cache-examTips03](./images/aws_solutionArchitectAssociate/aws-cache-examTips03.png)          









======================================================================================================================= 


<span style="color: #EA9811">

###  **CACHING - QUIZZ** 
</span>



![aws-cache-quizz01](./images/aws_solutionArchitectAssociate/aws-cache-quizz01.png)             
![aws-cache-quizz02](./images/aws_solutionArchitectAssociate/aws-cache-quizz02.png)             
![aws-cache-quizz03](./images/aws_solutionArchitectAssociate/aws-cache-quizz03.png)             
![aws-cache-quizz04](./images/aws_solutionArchitectAssociate/aws-cache-quizz04.png)             
![aws-cache-quizz05](./images/aws_solutionArchitectAssociate/aws-cache-quizz05.png)             
![aws-cache-quizz06](./images/aws_solutionArchitectAssociate/aws-cache-quizz06.png)             
![aws-cache-quizz07](./images/aws_solutionArchitectAssociate/aws-cache-quizz07.png)             
![aws-cache-quizz08](./images/aws_solutionArchitectAssociate/aws-cache-quizz08.png)             



**QUIZZ REDO**      





======================================================================================================================= 




_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **GOVERNANCE** 
</span>


[Documentation aws - Gouvernance](https://aws.amazon.com/fr/products/management-and-governance/)        
Par le passé, les organisations devaient choisir entre innover plus vite et contrôler efficacement leurs coûts, les règles de conformité et la sécurité.      
Avec les services de gestion et de gouvernance AWS, les clients n'ont pas à effectuer ce choix : ils peuvent faire les deux. Avec AWS, les clients peuvent mettre en œuvre, fournir et exploiter leur environnement à la fois pour l'agilité des entreprises et le contrôle de la gouvernance

Les piliers sont:           
- **echelle**    ➡️   AWS est adapté à la gestion des ressources dynamiques à grande échelle                     

- **simplicité**    ➡️ AWS simplifie le processus et offre un plan de controle aux clients pour gérer et maitriser les ressources                   

- **solutions tierces**    ➡️ AWS offre un large ecosysteme partenaire pour élargir et dévolopper le systeme de gestion et gouvernance                       

- **reduction des coûts**    ➡️ AWS offre la faciliter d'optimisation des couts                


**Services de gestion et de gouvernance AWS**:     
  _accelerer le travail des dev_:    
    - **AWS Control Tower**  ➡️ [Documentation aws - Control Tower](https://aws.amazon.com/fr/controltower/?c=mg&sec=srv&exp=b)                 
    - **AWS Organizations**  ➡️ [Documentation aws - Organizations](https://aws.amazon.com/fr/organizations/?c=mg&sec=srv)        
    - **AWS Well Architected Tool**  ➡️ [Documentation aws - Well Architected Tool](https://aws.amazon.com/fr/well-architected-tool/?c=mg&sec=srv)             
    - **AWS Budgets**  ➡️ [Documentation aws - Budgets](https://aws.amazon.com/fr/aws-cost-management/aws-budgets/?c=mg&sec=srv)                  
    - **AWS Licence Manager**  ➡️ [Documentation aws - Licence Manager](https://aws.amazon.com/fr/license-manager/?c=mg&sec=srv)              

  _Mettre en service des ressources et app_     
    - **AWS CloudFormation**  ➡️ [Documentation aws - CloudFormation](https://aws.amazon.com/fr/cloudformation/?c=mg&sec=srv)              
    - **AWS Service Catalog**  ➡️ [Documentation aws - Service Catalog](https://aws.amazon.com/fr/servicecatalog/?c=mg&sec=srv)               
    - **AWS OpsWorks**  ➡️ [Documentation aws - opsWorks](https://aws.amazon.com/fr/opsworks/?c=mg&sec=srv)               
    - **AWS Marketplace**  ➡️ [Documentation aws - MarketPlace](https://aws.amazon.com/marketplace/?c=mg&sec=srv)              

  _Exploiter l'environnement_    
    - **Amazon CloudWatch**  ➡️ [Documentation aws - cloudWatch](https://aws.amazon.com/fr/cloudwatch/?c=mg&sec=srv)               
    - **Amazon Managed Grafana**  ➡️ [Documentation aws - Grafana](https://aws.amazon.com/fr/grafana/?c=mg&sec=srv)              
    - **Amazon Managed Service Prometheus**  ➡️ [Documentation aws - Prometheus](https://aws.amazon.com/fr/prometheus/?c=mg&sec=srv)              
    - **AWS CloudTrail**  ➡️ [Documentation aws - cloudTrail](https://aws.amazon.com/fr/cloudtrail/?c=mg&sec=srv)               
    - **AWS Config**  ➡️ [Documentation aws - Config](https://aws.amazon.com/fr/config/?c=mg&sec=srv)               
    - **AWS Systems Manager**  ➡️ [Documentation aws - Systems Manager](https://aws.amazon.com/fr/systems-manager/?c=mg&sec=srv)               
    - **Rapport couts et utilisation AWS**  ➡️ [Documentation aws - Rapport couts et utilisation](https://aws.amazon.com/fr/aws-cost-management/aws-cost-and-usage-reporting/?c=mg&sec=srv)                
    - **AWS Cost Explorer**  ➡️ [Documentation aws - Cost Explorer](https://aws.amazon.com/fr/aws-cost-management/aws-cost-explorer/?c=mg&sec=srv)               
    - **AWS Managed Services**  ➡️ [Documentation aws - Managed Services](https://aws.amazon.com/fr/managed-services/?c=mg&sec=srv)                  
    - **Connecteurs AWS Service Management**  ➡️ [Documentation aws - Connecteurs AWs Service Management](https://aws.amazon.com/fr/service-management-connector/?c=mg&sec=srv)                 
    - **AWS X-Ray**  ➡️ [Documentation aws - X-Ray](https://aws.amazon.com/fr/xray/?c=mg&sec=srv)                 
    - **AWS Distro for open telementry**  ➡️ [Documentation aws - Distro](https://aws.amazon.com/fr/otel/?c=mg&sec=srv)                
    - **AWS Proton**  ➡️ [Documentation aws - Proton](https://aws.amazon.com/fr/proton/?c=mg&sec=srv)                
    - **Amazon DevOps Guru**  ➡️ [Documentation aws - DevOps Guru](https://aws.amazon.com/fr/devops-guru/)                  




**Autres liens utiles**:            
- [Documentation aws - SSO](https://docs.aws.amazon.com/fr_fr/singlesignon/latest/userguide/iam-auth-access.html)    
- [Documentation aws - Cognito](https://aws.amazon.com/cognito/getting-started/)    
- [Documentation aws - Microsoft AD](https://docs.aws.amazon.com/fr_fr/directoryservice/latest/admin-guide/directory_microsoft_ad.html)    
- [Documentation aws - Microsoft AD connector](https://docs.aws.amazon.com/fr_fr/directoryservice/latest/admin-guide/directory_ad_connector.html)    
- [Documentation aws - Delegate account accross IAM roles](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/tutorial_cross-account-with-roles.html)     
- [Documentation aws - Monitoring usage and costs](https://docs.aws.amazon.com/fr_fr/cur/latest/userguide/what-is-cur.html)    
- [Documentation aws - SNS](https://aws.amazon.com/fr/sns/faqs/)       
- [Documentation aws - Schedule Lambda using EventBridge](https://docs.aws.amazon.com/fr_fr/eventbridge/latest/userguide/eb-run-lambda-schedule.html)    







======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - MANAGING ACCOUNTS WITH ORGANIZATIONS** 
</span>


[Documentation aws - biling for Oragizations](https://docs.aws.amazon.com/fr_fr/awsaccountbilling/latest/aboutv2/consolidated-billing.html)         
Vous pouvez utiliser la fonction de facturation consolidée dans AWS Organizations pour regrouper la facturation et le paiement pour plusieurs Comptes AWS ou plusieurs comptes Amazon Web Services India Private Limited      

Dans AWS Organizations, chaque organisation a un compte de gestion qui paie les frais de tous les comptes membres    


AWS Organizations est un outil de gouvernance AWS gratuit qui permet de créer, manager de multiples accounts AWS     
Avec on peut controller tous ces accounts d'1 seul endroit plutot que de se connecter à chacun d'eux 1 par 1     


![governance-organizations-keyFeatures](./images/aws_solutionArchitectAssociate/governance-organizations-keyFeatures.png)      






Les **SCP** sont comme des policies IAM mais c'est elles qui ont le dernier mot     
   ➡️ si on a un "desaccord" dans les regles c'est la SCPs qui aura la décision finale     
   ➡️ elles prédominent même sur les droits du user "root"        

![governance-organizations-SCP](./images/aws_solutionArchitectAssociate/governance-organizations-SCP.png)       
     



![governance-organizations-tip01](./images/aws_solutionArchitectAssociate/governance-organizations-tip01.png)      
![governance-organizations-tip02](./images/aws_solutionArchitectAssociate/governance-organizations-tip02.png)      

======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - SHARING RESOURCES WITH AWS RAM** 
</span>



[Documentation aws - AWS RAM: Resource Access Manager](https://aws.amazon.com/fr/ram/)                    
Partagez simplement et en toute sécurité vos ressources AWS entre plusieurs comptes              

AWS RAM vous aide à partager vos ressources en toute sécurité entre les comptes AWS, au sein de votre organisation ou des unités organisationnelles (OU), et avec les rôles et utilisateurs IAM pour les types de ressources pris en charge     

![governance-RAM-schema](./images/aws_solutionArchitectAssociate/governance-RAM-schema.png)              



![governance-RAM-benefits](./images/aws_solutionArchitectAssociate/governance-RAM-benefits.png)          



![governance-RAM-tip01](./images/aws_solutionArchitectAssociate/governance-RAM-tip01.png)            
![governance-RAM-tip02](./images/aws_solutionArchitectAssociate/governance-RAM-tip02.png)            




AWS Resource Access Manager(AWS RAM) vous permet de partager vos ressources en toute sécurité au sein de votre organisation ou de vos unités organisationnelles (OU), ainsi qu'avec des rôles et des utilisateurs AWS Identity and Access Management (IAM) pour les types de ressources pris en charge     
Comptes AWS Si vous en avez plusieurs Comptes AWS, vous pouvez créer une ressource une seule fois et l'utiliserAWS RAM pour la rendre utilisable par ces autres comptes.     
Si votre compte est géré par AWS Organizations, vous pouvez partager des ressources avec tous les autres comptes de l'organisation ou uniquement avec les comptes appartenant à une ou plusieurs unités organisationnelles (UO) spécifiées.     
Vous pouvez également partager avec un identifiant de compte spécifique Comptes AWS, que le compte fasse partie ou non d'une organisation. Certains types de ressources pris en charge vous permettent également de les partager avec des rôles et des utilisateurs IAM spécifiés.







======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - SETTING UP CROSS-ACCOUNT ROLE ACCESS** 
</span>


Cette partie concerne la delelagtion de droits via les roles IAM     


La creation de roles qui sont transverses pour différents accounts permet de ne pas dubliquer des accounts ce qui créerait une faille de securité     
Ces roles transverses permettent d'avoir des accès temporaires pour différentes actions    
Pour l'account il ne reste plus qu'à avoir le `assumeRole` sur ce role et les droits sont obtenus       

![governance-role-setUpTransversalRole](./images/aws_solutionArchitectAssociate/governance-role-setUpTransversalRole.png)                  


![governance-role-tip01](./images/aws_solutionArchitectAssociate/governance-role-tip01.png)        
![governance-role-tip02](./images/aws_solutionArchitectAssociate/governance-role-tip02.png)        



======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - INVENTORY MANAGEMENT WITH AWS CONFIG** 
</span>



**AWS Config** est un gestionaire d'inventaires et d'outils de controles       
Il permet de voir l'historique de l'infrastructure à travers les creations de regles pour la rendre sûre et la conformer aux best practices      

![governance-config-stateOfInfra](./images/aws_solutionArchitectAssociate/governance-config-stateOfInfra.png)       


![governance-config-tip01](./images/aws_solutionArchitectAssociate/governance-config-tip01.png)          
![governance-config-tip02](./images/aws_solutionArchitectAssociate/governance-config-tip02.png)          




======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - OFFLOADING ACTIVE DIRECTORY TO DIRECTORY SERVICE** 
</span>


[Documentation aws - AWS Directory Service](https://aws.amazon.com/fr/directoryservice/)      
Gagner en efficacité avec un service Microsoft Active Directory entièrement géré          

AWS Directory Service for Microsoft Active Directory, aussi connu sous le nom d'AWS Managed Microsoft AD, permet à vos charges de travail et ressources AWS prenant en charge les répertoires, d'utiliser des AD sur AWS     

![governance-DirectoryService-schema](./images/aws_solutionArchitectAssociate/governance-DirectoryService-schema.png)          



![governance-DirectoryService-types](./images/aws_solutionArchitectAssociate/governance-DirectoryService-types.png)           




![governance-DirectoryService-tip01](./images/aws_solutionArchitectAssociate/governance-DirectoryService-tip01.png)                  
![governance-DirectoryService-tip02](./images/aws_solutionArchitectAssociate/governance-DirectoryService-tip02.png)                  


======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - EXPLORING WITH COST EXPLORER** 
</span>

Il est nécessaire d'optimiser la facturation pour ne pas trop dépenser        

**AWS Cost Explorer** est un outil qui permet la visualisation des couts du cloud       
Des rapports peuvent être générés à partir de différents parametres et resource tags     
Des prédictions de budgets en rapport avec ce qui est fait avant peut se faire     

Que permet ce service ?              
- connaitre quel service coute combien ➡️ savoir si un service est optimisé ou coute trop       
- connaitre les facturation par période ➡️ permet de voir et comparer les facturations sur des periods définies    
- ces infos peuvent être filtrées pour être plus précis        


>_Remarque_: Pour pouvoir filtrer sur les tags il faut d'abord les rendre enable      








======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - USING AWS BUDGETS** 
</span>


**AWS Budgets** permet aux entreprises de planifier et parametrer les attentes budgetaires autour du cloud     
Cela est facilité avec la creation d'alertes qui permettent aux users de savoir lorsque les planifications sont presque atteintes     

![governance-budget-types](./images/aws_solutionArchitectAssociate/governance-budget-types.png)       


Lorsque nous avons créé une alerte il est possible d'y attacher une action ou une policy (ex: arrêter des instances)     






======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - OPTIMIZING COSTS WITH AWS COST AND USAGE REPORTS** 
</span>



[Documentation aws - control access biling](https://docs.aws.amazon.com/fr_fr/awsaccountbilling/latest/aboutv2/control-access-billing.html)    
AWS Billing s'intègre au service AWS Identity and Access Management (IAM), afin que vous puissiez contrôler l'accès de personnes de votre organisation à des pages spécifiques sur la console AWS Billing


[Documentation aws - create reports cur](https://docs.aws.amazon.com/fr_fr/cur/latest/userguide/creating-cur.html)    
Vous pouvez utiliser le pluginRapports sur les coûts et l'utilisation de la console Billing and Cost Management pour créer des rapports de coûts et d'utilisation      


**AWS CUR**: **Aws Cost and Usage Reports**         

Ces rapports sont simples à comprendre             
Sont publiés sur S3 (donc on peut aller en récupérer des anciens)             
Offrent beaucoup de filtre pour affiner les recherches (jour, heure, minutes, ressoures, tags....)       
Sont créés quotidiennement au format csv               
Sont facilement intégrables dans les services AWS Athena, Redshift Quicksight              


**Cas d'usages**:    
- peut être utilisé pour tout AWS Organizations, OU ou account individuel      
- traque les utilisations, charges et allocations de **saving plans**                  
- monitore à la demande la capacité de reservation          
- cas de panne les data transfer charges      
- aller chercher plus loin avec les filtres     




======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - REDUCING COMPUTE SPEND USING SAVINGS PLANS AND AWS COMPUTE OPTIMIZER** 
</span>


[Documentation aws - AWS Compute Optimizer](https://aws.amazon.com/fr/compute-optimizer/)      
Obtenir des recommandations pour optimiser votre utilisation des ressources AWS       

L'Optimiseur de calcul AWS vous permet d'éviter le surprovisionnement ou le sousprovisionnement pour trois types de ressources AWS : les types d'instances Amazon Elastic Compute Cloud (EC2), les volumes Amazon Elastic Block Store (EBS) et les fonctions AWS Lambda, en se basant sur vos données d'utilisation      

![governance-awsOptimizer-schema](./images/aws_solutionArchitectAssociate/governance-awsOptimizer-schema.png)             

Redimensionnez les charges de travail grâce à l'intelligence artificielle et aux analyses basées sur le machine learning pour réduire les coûts jusqu'à 25 %     

Résolvez les problèmes de performance en mettant en œuvre des recommandations qui identifient les ressources sous-provisionnées    

Augmentez les économies de recommandation et la visibilité sur l'utilisation de la mémoire en activant les métriques Amazon CloudWatch     


Ce service utilise aussi des graphes qui permettent de visualiser l'historiques des performances et aussi de faire une projection sur l'avenir calculée par rapport à cet historique     


![governance-awsOptimizer-settings](./images/aws_solutionArchitectAssociate/governance-awsOptimizer-settings.png)             



**Savings Plans**      

[Documentation aws - AWS Savings Plans](https://aws.amazon.com/savingsplans/)      
Économisez jusqu'à 72 % grâce à un modèle de tarification flexible         


Les Savings Plans sont des modèles de tarification flexibles qui peuvent vous permettre de réduire votre facture de 72 % comparé aux tarifs à la demande, en échange d'un engagement d'un ou trois ans de consommation horaire       
AWS propose trois types de Savings Plans : Compute Savings Plans, EC2 Instance Savings Plans et Amazon SageMaker Savings Plans     





![governance-awsOptimizer-savingsPlans](./images/aws_solutionArchitectAssociate/governance-awsOptimizer-savingsPlans.png)      


![governance-awsOptimizer-savingsPlans-types](./images/aws_solutionArchitectAssociate/governance-awsOptimizer-savingsPlans-types.png)      


Comment utiliser ces savings plans ?           

- afficher les recommandations dans la partie facturation de votre console aws        
- des recommandations sont calculées automatiquement pour faciliter l'achat      
- ajouter au panier et acheter directement dans votre compte aws       
- s'appliquent aux tarifs d'utilisation après l'application et l'épuisement des instances réservées         
- famille de facturation consolidée : appliquée d'abord au propriétaire du compte, puis peut être étendue à l'autre            



![governance-awsOptimizer-examTip](./images/aws_solutionArchitectAssociate/governance-awsOptimizer-examTip.png)            





======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - AUDITING WITH TRUSTED ADVISOR** 
</span>

[Documentation aws - AWS Trusted Advisor](https://aws.amazon.com/fr/premiumsupport/technology/trusted-advisor/)           
Réduire les coûts, améliorer les performances et améliorer la sécurité                

AWS Trusted Advisor propose des recommandations qui vous permettent de suivre les bonnes pratiques AWS       
Trusted Advisor évalue votre compte à l'aide de vérifications. Ces contrôles vous aident à identifier des moyens d'optimiser votre infrastructure AWS, d'améliorer la sécurité et les performances, de réduire les coûts totaux et de surveiller les limites de service (Service Quotas)      


![governance-awsTrustedAdivisor-schema](./images/aws_solutionArchitectAssociate/governance-awsTrustedAdivisor-schema.png)             



C'est un outil complètement managé d'audit        
Il scanne 5 parties différentes de votre account et vérifie si les best practices sont utilisées     
![governance-awsTrustedAdivisor-questions](./images/aws_solutionArchitectAssociate/governance-awsTrustedAdivisor-questions.png)             

![governance-awsTrustedAdivisor-examTip01](./images/aws_solutionArchitectAssociate/governance-awsTrustedAdivisor-examTip01.png)             
![governance-awsTrustedAdivisor-examTip02](./images/aws_solutionArchitectAssociate/governance-awsTrustedAdivisor-examTip02.png)             



======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - ENFORCING ACCOUNT GOVERNANCE VIA AWS CONTROL TOWER** 
</span>

[Documentation aws - AWS Control Tower](https://aws.amazon.com/fr/controltower/)       
Configurer et gouverner un environnement AWS multi-comptes sécurisé       

AWS Control Tower simplifie les expériences AWS en orchestrant plusieurs services AWS en votre nom tout en maintenant les besoins de sécurité et de conformité de votre organisation      

![governance-controlTower-schema](./images/aws_solutionArchitectAssociate/governance-controlTower-schema.png)              

Configurez un environnement multi-comptes bien architecturé en moins de 30 minutes               
Automatisez la création de comptes AWS avec une gouvernance intégrée             
Appliquez les bonnes pratiques, les normes et les exigences réglementaires avec des contrôles préconfigurés            
Intégrez de manière transparente des logiciels tiers à grande échelle pour améliorer votre environnement AWS           


![governance-controlTower-def](./images/aws_solutionArchitectAssociate/governance-controlTower-def.png)              


**Termes à connaitre**:                      
  1. **landing zone**: infra well-architectured basé sur une environnement multi-accounts et les best practices niveau securité         
    ➡️ container avec tous les pré-requis pour votre entreprise basé sur les OUs et accounts de Organizations                    


  2. **guardrails**: regles de haut niveau fournissant une gouvernance continie pour l'environnement AWS       
    ➡️ preventive      
    ➡️ detective       


  3. **account factory**: template d'account standardisé préconfiguré       
    ➡️c'est là qu'on standardise les policies pour les nouveaux accounts         


  4. **cloudformation stackSet**: templates de deploiements automatisés  pour un deploiement de ressources répétées pour la gouvernance       
    ➡️ template créé et utilisé pour des déploiements répétitifs de ressources pour les OU ou accounts de Organizations 


  5. **shared accounts**: 3 accounts utilisés par Control Tower pour autoriser la création de la landing zone     
    


![governance-guardrails-def](./images/aws_solutionArchitectAssociate/governance-guardrails-def.png)              



![governance-controlTower-diagram](./images/aws_solutionArchitectAssociate/governance-controlTower-diagram.png)


![governance-controlTower-examTip](./images/aws_solutionArchitectAssociate/governance-controlTower-examTip.png)         




======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - MANAGING SOFTWARE LICENCES IN AWS WITH AWS LICENCE MANAGER** 
</span>


[Documentation aws - Licence Manager](https://aws.amazon.com/fr/license-manager/)              
Gérez vos licences logicielles et ajustez finement les coûts de licence               

AWS License Manager facilite pour vous la gestion des licences des logiciels existants en provenance de fournisseurs comme Microsoft, SAP, Oracle et IBM sur AWS et sur vos environnements sur site          

![governance-licenceManager-schema](./images/aws_solutionArchitectAssociate/governance-licenceManager-schema.png)         


Bénéficiez d'une meilleure visibilité et d'un meilleur contrôle sur la façon dont les licences logicielles sont utilisées et empêchez les abus avant qu'ils ne se produisent      
Faites des économies grâce à l'utilisation maximale des licences, y compris la façon dont vous suivez et gérez les licences     
Réduisez le risque de non-conformité en appliquant des limites d'utilisation des licences, en bloquant les nouveaux lancements et en utilisant d'autres contrôles     
Augmentez votre productivité en automatisant l'emplacement, la publication et la récupération des hôtes à l'aide de groupes de ressources d'hôtes     

![governance-licenceManager](./images/aws_solutionArchitectAssociate/governance-licenceManager.png)         



======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - MONITORING HEALTH EVENTS IN THE AWS PERSONAL HEALTH DASHBOARD** 
</span>


[Documentation aws - AWS Personal Health Dashboard](https://aws.amazon.com/fr/premiumsupport/technology/personal-health-dashboard/)       
Consulter les événements et les changements importants qui affectent votre environnement AWS      


>_Remarque_: il est possible d'automatiser des actions "préventives" avec le service **Amazon EventBridge**      



![governance-health-concepts](./images/aws_solutionArchitectAssociate/governance-health-concepts.png)          

![governance-health-examples](./images/aws_solutionArchitectAssociate/governance-health-examples.png)          



![governance-health-examTip](./images/aws_solutionArchitectAssociate/governance-health-examTip.png)          




======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - STANDARDIZING DEPLOYMENTS USING AWS SERVICE CATALAG AND AWS PROTON** 
</span>


[Documentation aws - Service Catalog](https://aws.amazon.com/fr/servicecatalog/)       
Créez, partagez, organisez et gérez vos modèles d'IaC               

AWS Service Catalog vous permet de gérer de manière centralisée les services informatiques, les applications, les ressources et les métadonnées déployés afin de parvenir à une gouvernance cohérente de vos modèles d'infrastructure en tant que code (IaC)        
Avec AWS Service Catalog, vous pouvez répondre à vos exigences de conformité tout en veillant à ce que vos clients puissent déployer rapidement les services informatiques approuvés dont ils ont besoin     


![governance-servCatalog-schema](./images/aws_solutionArchitectAssociate/governance-servCatalog-schema.png)      

Trouvez et déployez rapidement des ressources cloud approuvées et en libre-service       
Restez agile tout en améliorant la gouvernance des ressources sur plusieurs comptes       
Rationalisez les flux de travail en vous connectant à ServiceNow et Jira Service Management      
Obtenez des définitions d'applications et des métadonnées précises et à jour avec AWS Service Catalog AppRegistry     


![governance-servCatalog-overview](./images/aws_solutionArchitectAssociate/governance-servCatalog-overview.png)      


Quels sont les avantages de service Catalog ?       
- standardise les applications dans aws pour votre entreprise (on ne donne accès qu'à 1 choix "restreint" défini)      
- les clients finaux peuvent choisir dans cette liste l'outil qu'ils veulent      
- les accès à cet outil seront gérés par IAM     
- l'upgrade est automatique ➡️ la nouvelle version est mise en place dans catalog (on peut la tester et la valider avant cela) elle est ensuite propagée dans aws automatiquement    







[Documentation aws - AWS Proton](https://aws.amazon.com/fr/proton/)                  
Améliorez vos opérations grâce à des modèles d'infrastructure en libre-service et à l'automatisation de la mise en service     

AWS Proton est un outil de flux de travail de déploiement pour les applications modernes, destiné à aider les ingénieurs de plateforme et DevOps à stimuler leur agilité organisationnelle     


![governance-proton-schema](./images/aws_solutionArchitectAssociate/governance-proton-schema.png)           

Optimisez les opérations d'ingénierie de plateforme en mettant en œuvre des fonctionnalités en libre-service évolutives pour les développeurs      
Donnez les moyens à vos développeurs d'évoluer plus rapidement grâce à un outil en libre-service destiné à mettre en service l'infrastructure et à gérer le déploiement du code      
Accélérez l'adoption de bonnes pratiques DevOps au sein de votre équipe      

![governance-proton-overview](./images/aws_solutionArchitectAssociate/governance-proton-overview.png)           






======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - OPTIMZING ARCHITECTURES WITH THE AWS WELL-ARCHITECTED TOOL** 
</span>



![governance-WellArchitecturedTool-6pillars](./images/aws_solutionArchitectAssociate/governance-WellArchitecturedTool-6pillars.png)     

![governance-WellArchitecturedTool-examTip](./images/aws_solutionArchitectAssociate/governance-WellArchitecturedTool-examTip.png)     


>_Remarque_: cet outil est un framework utilisé pour vérifier qu'on utilise bien toutes les best practices sur AWS pour les 6 piliers    

======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - EXAM TIPS** 
</span>


![governance-examTips01](./images/aws_solutionArchitectAssociate/governance-examTips01.png)           
![governance-examTips02](./images/aws_solutionArchitectAssociate/governance-examTips02.png)          
![governance-examTips03](./images/aws_solutionArchitectAssociate/governance-examTips03.png)           
![governance-examTips04](./images/aws_solutionArchitectAssociate/governance-examTips04.png)           
![governance-examTips05](./images/aws_solutionArchitectAssociate/governance-examTips05.png)           
![governance-examTips05](./images/aws_solutionArchitectAssociate/governance-examTips06.png)           
![governance-examTips07](./images/aws_solutionArchitectAssociate/governance-examTips07.png)           
![governance-examTips08](./images/aws_solutionArchitectAssociate/governance-examTips08.png)           
![governance-examTips09](./images/aws_solutionArchitectAssociate/governance-examTips09.png)           
![governance-examTips10](./images/aws_solutionArchitectAssociate/governance-examTips10.png)            





======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - LAB - USING AWS TAGS AND RESOUCE GROUPS** 
</span>



![governance-lab-enonce](./images/aws_solutionArchitectAssociate/labs/governance-lab/governance-lab-enonce.png)         


credentials
username: cloud_user                 
password: c{}1w}Y45q2wQMR{kUpN              
link: https://481086170901.signin.aws.amazon.com/console?region=us-east-1          


![governance-lab-objectif](./images/aws_solutionArchitectAssociate/labs/governance-lab/governance-lab-objectif.png)         



**Solution**                
Log in to the live AWS environment using the credentials provided. Make sure you're in the N. Virginia (us-east-1) Region throughout the lab.

**Set Up AWS Config**      

1. Navigate to Config.

2. In the Set up AWS Config window, click 1-click setup.

3. Leave the settings as their defaults.

4. Click Confirm.


**Tag an AMI and EC2 Instance**  

1. In a new browser tab, navigate to EC2 > Instances (running).

2. Select any of the instances listed.

3. Right-click on the name of the selected instance, and select Actions > Image and templates > Create image.

4. For the Image name, enter Base.

5. Click Create image.

6. Click AMIs in the left-hand menu.

7. Once the AMI you just created has a status of available, select it. (It could take 5–15 minutes.)

8. Click Launch instance from AMI.

9. For Name, enter My Test Server.

10. For Instance type, select t3.micro.

11. For Key pair name, select Proceed without a key pair (Not recommended).

12. In the Network settings section, under Firewall (security groups), choose Select existing security group.

13. Under Common security groups, select the one with SecurityGroupWeb in the name (not the default security group).

14. Leave the rest of the default settings.

15. Click Launch instance.

16. Click View all instances, and give it a few minutes to enter the running state.


**Tag Applications with the Tag Editor**

_Module 1 Tagging_

1. In a new browser tab, navigate to Resource Groups & Tag Editor.

2. Click Tag Editor in the left-hand menu.

3. In the Find resources to tag section, set the following values:

  - Regions: Select us-east-1. (It should already be selected.)

  - Resource types:
    - Enter and select AWS::EC2::Instance.

    - Enter and select AWS::S3::Bucket.

4. Click Search resources.

5. In the Resource search results section, set the following values:

  - In the Filter resources search window, enter Mod. 1 and press Enter to execute the search.

 - Select both instances, and click Clear filters.

  - In the Filter resources search window, enter moduleone, and press Enter.

  - Select the listed S3 bucket, and click Clear filters.

6. Click Manage tags of selected resources.

7. In the Edit tags of all selected resources section, click Add tag, and set the following values:

  - Tag key: Enter Module.

  - Tag value: Enter Starship Monitor.

8. Click Review and apply tag changes > Apply changes to all selected.

_Module 2 Tagging_

1. With the same Region and resource types selected from the prevoius step, click Search resources again.

2. In the Resource search results section, set the following values:

  - In the Filter resources search window, enter Mod. 2, and press Enter.

  - Select both instances, and click Clear filters.

  - In the Filter resources search window, enter moduletwo, and press Enter.

  - Select the listed S3 bucket, and click Clear filters.

3. Click Manage tags of selected resources.

4. In the Edit tags of all selected resources section, click Add tag, and set the following values:

  - Tag key: Enter Module.

  - Tag value: Enter Warp Drive.

5. Click Review and apply tag changes > Apply changes to all selected.



**Create Resource Groups and Use AWS Config Rules for Compliance**

_Create the `Starship Monitor` Resource Group_

1. In the left-hand menu, select Create Resource Group.

2. For Group type, select Tag based.

3. In the Grouping criteria section, All supported resource types should already be selected.

4. In the Tags field, select the following:

  - Tag key: Select Module.

  - Optional tag value: Select Starship Monitor.

5. Click Preview group resources.

6. Ensure the three group resources show up in the Group resources section.

7. In the Group details section, enter a Group name of Starship-Monitor.

8. Click Create group.


**Create the `Warp Drive` Resource Group**

1. In the left-hand menu, click Create Resource Group.

2. For Group type, select Tag based.

3. In the Grouping criteria section, All supported resource types should still be selected.

4. In the Tags field, select the following:

  - Tag key: Select Module.

  - Optional tag value: Select Warp Drive.

5. Click Preview group resources.

6. Ensure the three group resources show up in the Group resources section.

7. In the Group details section, enter a Group name of Warp-Drive.

8. Click Create group.


**View the Saved Resource Groups** 

1. In the left-hand menu, click Saved Resource Groups.

2. Click Starship-Monitor.

  - Here, we should see all the resources in our Starship-Monitor group.


**Use AWS Config Rules for Compliance**


1. Navigate back to the EC2 browser tab.

2. Refresh the instances table.
3. Select the My Test Server instance.

4. In the Details section, copy its AMI ID.

5. Navigate to the AWS Config console tab.

6. In the left-hand menu, click Rules.

7. Click Add rule.

8. For the rule type, select Add AWS managed rule.

9. Search for approved-amis-by-id in the search box, and select that rule.

10. Click Next.

11. In the Parameters section, paste the AMI ID you just copied into the Value field.

12. Click Next > Add rule.

13. Back in the EC2 instances console, select all the instances.

14. Click Instance state > Reboot instance.

15. In the Reboot instances? dialog, click Reboot.

16. Navigate back to the AWS Config Console.

17. After a few minutes, refresh the page.

  - You should see there are now noncompliant resources.

18. Click the approved-amis-by-id rule.

19. Choose one of the noncompliant resource IDs, and remember its last four characters.

20. Back in the EC2 console, identify the instance (by matching the last four characters), and then select it.

21. In the Details section, identify its AMI ID.

22. Back in the AWS Config console, identify the AMI ID under Value in the Parameters section.

23. You should see it doesn't match the AMI ID you noted in EC2, which means the rule successfully identified noncompliant resources.







======================================================================================================================= 


<span style="color: #EA9811">

###  **GOVERNANCE - QUIZZ** 
</span>



![governance-quiz01](./images/aws_solutionArchitectAssociate/governance-quiz01.png)           
![governance-quiz02](./images/aws_solutionArchitectAssociate/governance-quiz02.png)           
![governance-quiz03](./images/aws_solutionArchitectAssociate/governance-quiz03.png)           
![governance-quiz04](./images/aws_solutionArchitectAssociate/governance-quiz04.png)           
![governance-quiz05](./images/aws_solutionArchitectAssociate/governance-quiz05.png)           
![governance-quiz06](./images/aws_solutionArchitectAssociate/governance-quiz06.png)           
![governance-quiz07](./images/aws_solutionArchitectAssociate/governance-quiz07.png)           
![governance-quiz08](./images/aws_solutionArchitectAssociate/governance-quiz08.png)           
![governance-quiz09](./images/aws_solutionArchitectAssociate/governance-quiz09.png)           
![governance-quiz10](./images/aws_solutionArchitectAssociate/governance-quiz10.png)           
![governance-quiz11](./images/aws_solutionArchitectAssociate/governance-quiz11.png)           
![governance-quiz12](./images/aws_solutionArchitectAssociate/governance-quiz12.png)           
![governance-quiz13](./images/aws_solutionArchitectAssociate/governance-quiz13.png)           
![governance-quiz14](./images/aws_solutionArchitectAssociate/governance-quiz14.png)           
![governance-quiz15](./images/aws_solutionArchitectAssociate/governance-quiz15.png)           
![governance-quiz16](./images/aws_solutionArchitectAssociate/governance-quiz16.png)           
![governance-quiz17](./images/aws_solutionArchitectAssociate/governance-quiz17.png)           
![governance-quiz18](./images/aws_solutionArchitectAssociate/governance-quiz18.png)           
![governance-quiz19](./images/aws_solutionArchitectAssociate/governance-quiz19.png)           
![governance-quiz20](./images/aws_solutionArchitectAssociate/governance-quiz20.png)           
![governance-quiz21](./images/aws_solutionArchitectAssociate/governance-quiz21.png)           
![governance-quiz22](./images/aws_solutionArchitectAssociate/governance-quiz22.png)           
![governance-quiz23](./images/aws_solutionArchitectAssociate/governance-quiz23.png)           
![governance-quiz24](./images/aws_solutionArchitectAssociate/governance-quiz24.png)           
![governance-quiz25](./images/aws_solutionArchitectAssociate/governance-quiz25.png)           
![governance-quiz26](./images/aws_solutionArchitectAssociate/governance-quiz26.png)           
 





======================================================================================================================= 



_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **MIGRATION** 
</span>


[Documentation aws - Cloud Miragtion](https://aws.amazon.com/fr/cloud-migration/)       
Optimisez les coûts et accélérez l'innovation en migrant et en modernisant vos applications et vos données avec AWS         

Le processus de migration en trois phases: 
- évaluer              
- mobiliser         
- migrer et moderniser          

[Documentation aws - AWS Miragtion evaluator](https://aws.amazon.com/fr/migration-evaluator/?pg=htm&cp=tb&sec=s)      
Développez un cas d'utilisation régi par les données pour AWS           

Migration Evaluator fournit les informations dont vous avez besoin pour élaborer une étude de cas fondée sur des données pour la migration vers AWS, ce qui vous aidera à définir les prochaines étapes de votre parcours de migration       
![migration-awsMigrationEvaluator-schema](./images/aws_solutionArchitectAssociate/migration-awsMigrationEvaluator-schema.png)      




=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - MIGRATION DATA WITH AWS SNOW FAMILLY** 
</span>

[Documentation aws - AWS Snow Family](https://aws.amazon.com/snow/)     
Traitez les données en périphérie ou migrez des pétaoctets de données depuis et vers AWS         

Les solution de snow family sont des solutions utilisées lors de mauvaises (lentes) connexion internet (serveur "portable" physiquement connecté à la société)     


![migration-snowFamily-tip01](./images/aws_solutionArchitectAssociate/migration-snowFamily-tip01.png)                
![migration-snowFamily-tip02](./images/aws_solutionArchitectAssociate/migration-snowFamily-tip02.png)                   



=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - STORAGE GATEWAY** 
</span>


[Documentation aws - Storage Gateway](https://aws.amazon.com/fr/storagegateway/?nc=sn&loc=0)      
Équipez vos applications sur site d'un accès à un stockage dans le cloud quasi illimité        

AWS Storage Gateway est un ensemble de services de stockage cloud hybride qui offrent un accès sur site à un stockage dans le cloud pratiquement illimité        
![migration-StorageGateway-schema](./images/aws_solutionArchitectAssociate/migration-StorageGateway-schema.png)          



Storage Gateway fournit un ensemble standard de protocoles de stockage comme iSCSI, SMB et NFS, ce qui vous permet d'utiliser le stockage AWS sans réécrire vos applications existantes     

[Documentation aws - what is amazon S3 File Gateway](https://docs.aws.amazon.com/filegateway/latest/files3/what-is-file-s3.html)      


Ce service permet un merge entre le on-premise et le cloud des ressources      



![migration-filegateway](./images/aws_solutionArchitectAssociate/migration-filegateway.png)      

Cela n'est rien de plus que des VMs fournies par aws dans votre on-premises    


![migration-volgateway](./images/aws_solutionArchitectAssociate/migration-volgateway.png)      


![migration-tapegateway](./images/aws_solutionArchitectAssociate/migration-tapegateway.png)      



![migration-tip01](./images/aws_solutionArchitectAssociate/migration-tip01.png)               
![migration-tip02](./images/aws_solutionArchitectAssociate/migration-tip02.png)               





=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - AWS DATA SYNC** 
</span>

[Documentation aws - AWS Data Sync](https://aws.amazon.com/fr/datasync/)         
Simplifier et accélérer les migrations de données sécurisées         


**Data-sync** est une solution **agent-based** pour la migration de on-premises vers AWS           
Elle permet la migration des data entre **NFS** et **SMB Shares** vers des solutions de storage AWS            
**Data-sync** est l'outil à utiliser pour une migration en 1 seule fois         

![migration-datasync-settings](./images/aws_solutionArchitectAssociate/migration-datasync-settings.png)       

![migration-datasync-tip01](./images/aws_solutionArchitectAssociate/migration-datasync-tip01.png)                  
![migration-datasync-tip02](./images/aws_solutionArchitectAssociate/migration-datasync-tip02.png)                  
![migration-datasync-tip03](./images/aws_solutionArchitectAssociate/migration-datasync-tip03.png)                  




=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - AWS TRANSFER FAMILY** 
</span>

[Documentation aws - Transfer Family](https://aws.amazon.com/fr/aws-transfer-family/)         
Gérez et partagez facilement vos données grâce à des transferts de fichiers simples, sécurisés et évolutifs    

AWS Transfer Family met à l'échelle de manière sécurisée vos transferts de fichiers récurrents d'entreprise à entreprise vers les services de stockage AWS à l'aide des protocoles SFTP, FTPS, FTP et AS2        

![migration-transferFamily-principes](./images/aws_solutionArchitectAssociate/migration-transferFamily-principes.png)             


![migration-transferFamily-tip01](./images/aws_solutionArchitectAssociate/migration-transferFamily-tip01.png)               
![migration-transferFamily-tip02](./images/aws_solutionArchitectAssociate/migration-transferFamily-tip02.png)               






=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - MOVING TO THE CLOUD WITH MIGRATION HUB** 
</span>


[Documentation aws - AWS Migration Hub](https://aws.amazon.com/fr/migration-hub/)     
Découvrir les outils dont vous avez besoin pour simplifier votre migration et votre modernisation        

**AWS Migration Hub** fournit un emplacement central pour recueillir les données d'inventaire des serveurs et des applications pour l'évaluation, la planification et le suivi des migrations vers AWS           
Migration Hub peut également aider à accélérer la modernisation des applications après la migration        
![migration-awsMigrationHub-schema](./images/aws_solutionArchitectAssociate/migration-awsMigrationHub-schema.png)      



**AWS Migration Hub** s'intègre avec **Server Migration Service (SMS)** et **Database Migration Service (DMS)**            

![migration-sms-principes](./images/aws_solutionArchitectAssociate/migration-sms-principes.png)           


![migration-dms-principes](./images/aws_solutionArchitectAssociate/migration-dms-principes.png)           



![migration-hub-tip01](./images/aws_solutionArchitectAssociate/migration-hub-tip01.png)               
![migration-hub-tip02](./images/aws_solutionArchitectAssociate/migration-hub-tip02.png)               





=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - MIGRATING WORKLOADS TO AWS USING APPLICATION DISCOVERY SERVICE OR AWS APPLICATION MIGRATION SERVICE (AWS MGN)** 
</span>



[Documentation aws - Application Discovery Service](https://aws.amazon.com/fr/application-discovery/)    
Découvrez le comportement et l'inventaire des serveurs on-premises pour planifier les migrations vers le cloud             


**AWS Application Discovery Service** vous aide à planifier la migration vers le cloud de vos projets en recueillant des informations sur vos data centers on-premises
![migration-ApplicationDiscoveryService-schema](./images/aws_solutionArchitectAssociate/migration-ApplicationDiscoveryService-schema.png)        


Il integre Migration Hub pour simplifier la migration et tracquer les status de migration       
Il permet la découverte des serveurs, les regroupe par application et traque chaque application lors de la migration       


![migration-applicationDiscoveryService-types](./images/aws_solutionArchitectAssociate/migration-applicationDiscoveryService-types.png)        





[Documentation aws - Application Migration Service](https://aws.amazon.com/fr/application-migration-service/)         
Déplacez et améliorez vos applications sur site et dans le cloud       

**AWS Application Migration Service (AWS MGM)** limite les processus manuels chronophages et sujets à des erreurs en automatisant la conversion de vos serveurs sources afin qu'ils s'exécutent de manière native sur AWS     
Ce service permet également de simplifier la modernisation des applications grâce à des options d'optimisation intégrées et personnalisées.     
![migration-ApplicationMigrationService-schema](./images/aws_solutionArchitectAssociate/migration-ApplicationMigrationService-schema.png)        


![migration-applicationMigrationService-principes](./images/aws_solutionArchitectAssociate/migration-applicationMigrationService-principes.png)              


![migration-applicationMigrationService-types](./images/aws_solutionArchitectAssociate/migration-applicationMigrationService-types.png)              

![migration-ADS_MGM-examTip](./images/aws_solutionArchitectAssociate/migration-ADS_MGM-examTip.png)        


=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - MIGRATING DATABASES FROM ON-PREMISES TO AWS WITH AWS DATABASE MIGRATION SERVICE (AWS DMS)** 
</span>


[Documentation aws - Database Migration Service](https://aws.amazon.com/fr/dms/)         


**AWS Database Migration Service (AWS DMS)**, est un service géré de réplications et de migrations qui permet de déplacer, rapidement et sûrement, votre base de données et vos charges de travail d'analytique vers AWS et en assurant un temps d'interruption minime et aucune perte de données     

**DMS** est un outil de migration                
  ➡️  il permet une migration de DB relationnelles, data warehouses, NoSQl DB et d'autres data stores     

**DMS** est pour le cloud ou on-premises         
  ➡️ Il permet la migration de datas entre AWS et on-premises        
  ➡️ A savoir qu'il restera toujours des datas sur AWS ➡️ c'est la condition d'utilisation (pas de passage de on-prem à 1 autre on-prem ET pas de passage vers un autre cloud provider directement)        

**DMS** est 1-time ou ongoing       
  ➡️ Il est possible de choisir l'option: ligration en 1-time ou en continue (repliques continues en fonction des modifs)      

**DMS** est un outil de conversion           
  ➡️ Le **SCT: AWS Schema Conversion Tool** autorise la translation des formats de databases vers des formats AWS (Aurora le plus connu)         

**DMS** permet de faire des économies         
  ➡️ gains financiers, de securité, de qualité, de résilience.....

**DMS** fonctionne de la manière suivante:            
  - serveur qui exécute une replication (clone)             
  - crée une source et une cible avec leur connexion              
  - planifie les tasks pour la migration des datas              
  - AWS crée les tables et clés primaires si celles-ci n'existent pas           
  - Il est aussi possible de créer les tables avant la migration si souhaité         
  ➡️ cela permet au **SCT** de créer 1, plusieurs ou toutes les tables, indexes....    
  ➡️ les sources et la data stores cibles sont référencés comme des **endpoints**          

**DMS** permet les migrations de 2 manières (source: endpoint / target: endpoint)           
- en utilisant 1 moteur identique       ➡️ MySQl ➡️ MySQL
- en utilisant des moteur différents    ➡️ Oracle ➡️ PostGreSQL       
➡️ Il est OBLIGATOIRE d'avoir 1 endpoint chez AWS    



![migration-dms-sct](./images/aws_solutionArchitectAssociate/migration-dms-sct.png)          
![migration-dms-types](./images/aws_solutionArchitectAssociate/migration-dms-types.png)          


![migration-dms-migratingLargeData](./images/aws_solutionArchitectAssociate/migration-dms-migratingLargeData.png)        



![migration-dms-examTip](./images/aws_solutionArchitectAssociate/migration-dms-examTip.png)          






=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - REPLICATING AND TRACKING MIGRATIONS WITH AWS MIGRATION HUB AND AWS SERVER MIGRATION SERVICE (AWS SMS)** 
</span>



![migration-hub-examTip](./images/aws_solutionArchitectAssociate/migration-hub-examTip.png)           
![migration-sms-examTip](./images/aws_solutionArchitectAssociate/migration-sms-examTip.png)           




=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - EXAM TIPS** 
</span>


![migration-tip01](./images/aws_solutionArchitectAssociate/migration-examtip01.png)                   
![migration-tip02](./images/aws_solutionArchitectAssociate/migration-examtip02.png)                   
![migration-tip03](./images/aws_solutionArchitectAssociate/migration-examtip03.png)                   
![migration-tip04](./images/aws_solutionArchitectAssociate/migration-examtip04.png)                   
![migration-tip05](./images/aws_solutionArchitectAssociate/migration-examtip05.png)                   
![migration-tip06](./images/aws_solutionArchitectAssociate/migration-examtip06.png)                   
![migration-tip07](./images/aws_solutionArchitectAssociate/migration-examtip07.png)                   






=======================================================================================================================      


<span style="color: #EA9811">

###  **MIGRATION - QUIZZ** 
</span>


![migration-quizz01](./images/aws_solutionArchitectAssociate/migration-quizz01.png)             
![migration-quizz02](./images/aws_solutionArchitectAssociate/migration-quizz02.png)             
![migration-quizz03](./images/aws_solutionArchitectAssociate/migration-quizz03.png)             
![migration-quizz04](./images/aws_solutionArchitectAssociate/migration-quizz04.png)             
![migration-quizz05](./images/aws_solutionArchitectAssociate/migration-quizz05.png)             
![migration-quizz06](./images/aws_solutionArchitectAssociate/migration-quizz06.png)             
![migration-quizz07](./images/aws_solutionArchitectAssociate/migration-quizz07.png)             
![migration-quizz08](./images/aws_solutionArchitectAssociate/migration-quizz08.png)             
![migration-quizz09](./images/aws_solutionArchitectAssociate/migration-quizz09.png)             
![migration-quizz10](./images/aws_solutionArchitectAssociate/migration-quizz10.png)             
![migration-quizz11](./images/aws_solutionArchitectAssociate/migration-quizz11.png)             





=======================================================================================================================      

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **FRONTEND WEB AND MOBILE** 
</span>


[Documentation aws - Frontend web and Mobile](https://aws.amazon.com/fr/products/frontend-web-mobile/)               
Le moyen le plus rapide de créer des applications web et mobiles          

AWS offre une large gamme d'outils et de services pour accompagner les flux de développement pour les développeurs web iOS/ANdroid, React Native et JavaScript natifs.        
Créez plus rapidement des applications             
Innovez de manière centralisée               
Effectuez une mise à l'échelle en toute confiance              


**Cycle de vie**        
  _Developper_                 
  [Documentation aws - AWS Amplify](https://aws.amazon.com/fr/amplify/)              
  Créez des applications web et mobiles complètes en quelques heures           

  AWS Amplify est une solution complète qui permet aux développeurs web et mobiles front-end de créer, d'expédier et d'héberger facilement des applications complètes sur AWS, avec la flexibilité de tirer parti de l'étendue des services AWS à mesure que les cas d'utilisation évolue

  [Documentation aws - AWS AppSync -> API graphQL](https://aws.amazon.com/fr/appsync/)            
  Accélérez le développement des applications avec les API GraphQL et Pub/Sub sans serveur         

  AWS AppSync crée des API GraphQL et Pub/Sub sans serveur qui simplifient le développement d'applications grâce à un point de terminaison unique pour interroger, mettre à jour ou publier des données en toute sécurité        

  [Documentation Amazon API Gateway - API RESTful](https://aws.amazon.com/api-gateway/)              
  Créer, maintenir et sécuriser des API à n'importe quelle échelle          

  [Documentation aws - Kit SDK Amzon Chime - audio, video, partage de bureaux](https://aws.amazon.com/fr/chime/chime-sdk/)       
  Intégrer des capacités de communication intelligente en temps réel dans vos applications          

  Grâce au kit SDK Amazon Chime, les créateurs peuvent facilement ajouter à leurs applications des fonctions de voix, de vidéo et de messagerie en temps réel optimisées par le machine learning             

  [Documentation aws - Amazon Location Service - geolocalisation](https://aws.amazon.com/fr/location/)              
  Ajouter facilement et en toute sécurité des données de localisation aux applications              

  Amazon Location Service permet aux développeurs d'ajouter facilement des fonctionnalités de localisation à leurs applications, telles que des cartes, des points d'intérêt et des options de géocodage, la création d'itinéraires, le suivi et le géorepérage, sans renoncer à la sécurité des données et à la confidentialité des utilisateurs.         

  _Diffuser_                             
  [Documentation aws - AWS Amplify](https://aws.amazon.com/fr/amplify/)              
  Créez des applications web et mobiles complètes en quelques heures  

  [Documentation aws - App Runner - deployer des app conteneurisées](https://aws.amazon.com/fr/apprunner/)        
  Déployer des applications web conteneurisées et des API à grande échelle              

  AWS App Runner est un service d'applications conteneurisées entièrement géré qui vous permet de créer, de déployer et d'exécuter des applications web conteneurisées et des services API sans expérience préalable de l'infrastructure ou des conteneurs         

   _Tester et surveiller_                             
  [Documentation aws - AWS Device Farm  - tests appareils et navigateurs](https://aws.amazon.com/fr/device-farm/)              
  Améliorez la qualité de vos applications web et mobiles en faisant des tests sur les navigateurs de bureau et des appareils mobiles réels hébergés sur le Cloud AWS            

  [Documentation aws - CloudWatch - surveiller](https://aws.amazon.com/fr/cloudwatch/)          
  Observez et surveillez les ressources et les applications sur AWS, sur site et sur d'autres clouds         

  Amazon CloudWatch collecte et visualise les journaux, les métriques et les données d'événements en temps réel dans des tableaux de bord automatisés afin de rationaliser la maintenance de votre infrastructure et de vos applications      

   _Engager_          
   [Documentation aws - pinpoint](https://aws.amazon.com/fr/pinpoint/)             
   Connectez-vous avec les clients grâce à des communications multicanaux évolutives et ciblées           

   Amazon Pinpoint offre aux spécialistes du marketing et aux développeurs un outil personnalisable pour diffuser des communications clients à travers les canaux, les segments et les campagnes à l'échelle       



=======================================================================================================================      


<span style="color: #EA9811">

###  **WEB - OVERVIEW** 
</span>


![web-overview](./images/aws_solutionArchitectAssociate/web-overview.png)       



=======================================================================================================================      


<span style="color: #EA9811">

###  **WEB - DEPLOYING WEB APP WITH AMPLIFY** 
</span>

**Amplify** propose différents outils pour le developpement frontend web et mobile pour le developpement d'applications full-stack sur AWS        

Il propose 2 services   
![web-amplify_hosting](./images/aws_solutionArchitectAssociate/web-ampligy_hosting.png)          
![web-amplify_studio](./images/aws_solutionArchitectAssociate/web-amplify_studio.png)         


![web-amplify_tip](./images/aws_solutionArchitectAssociate/web-amplify_tip.png)       



=======================================================================================================================      


<span style="color: #EA9811">

###  **WEB - TESTING APPS WITH DEVICE FARM** 
</span>


Device Farm est un service qui teste les application  Andoid, IOs et les web apps          
C'est utilisable sur les smartphones, tablettes      

Il offre 2 types de methodes de tests        
- automated  ➡️ on charge des scripts qui vont exécuter des tests     
- remote access ➡️ permet de tester les comportements mobile depuis le navigateur web classique   


![web-deviceFarm](./images/aws_solutionArchitectAssociate/web-deviceFarm.png)     



=======================================================================================================================      


<span style="color: #EA9811">

###  **WEB - ENGAGING CUSTOMERS WITH PINPOINT** 
</span>


Pinpoint est un outil de communication pour les clients            


![web-pinpoint_features](./images/aws_solutionArchitectAssociate/web-pinpoint_features.png)     
![web-pinpoint_uses](./images/aws_solutionArchitectAssociate/web-pinpoint_uses.png)      


![web-pinpoint_tip](./images/aws_solutionArchitectAssociate/web-pinpoint_tip.png)   



=======================================================================================================================      


<span style="color: #EA9811">

###  **WEB - EXAM TIPS** 
</span>


![web-examTip01](./images/aws_solutionArchitectAssociate/web-examTip01.png)            
![web-examTip02](./images/aws_solutionArchitectAssociate/web-examTip02.png)            
![web-examTip03](./images/aws_solutionArchitectAssociate/web-examTip03.png)            



=======================================================================================================================      


<span style="color: #EA9811">

###  **WEB - QUIZZ** 
</span>


![web-quizz01](./images/aws_solutionArchitectAssociate/web-quizz01.png)              
![web-quizz02](./images/aws_solutionArchitectAssociate/web-quizz02.png)              
![web-quizz03](./images/aws_solutionArchitectAssociate/web-quizz03.png)              


=======================================================================================================================      

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **MACHINE LEARNING** 
</span>


[Documentation aws - Machine Learning](https://aws.amazon.com/fr/machine-learning/)           
Accélérez l'innovation grâce à la gamme de services d'IA et de ML la plus complète         

Transformez vos données en informations détaillées tout en réduisant les coûts à l'aide du machine learning (ML) AWS          




=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - OVERVIEW** 
</span>


![ml-overview-tip](./images/aws_solutionArchitectAssociate/ml-overview-tip.png)       





=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - ANALYSING TEXT USING AMAZON COMPREHEND,AMAZON KENDRA, AMAZON TEXTRACT** 
</span>


[Documentation aws - Amazon Comprehend](https://aws.amazon.com/fr/comprehend/)               
Extraire et comprendre des informations précieuses à partir du texte des documents         

Amazon Comprehend est un service de traitement du langage naturel (NLP) qui utilise le machine learning (ML) pour découvrir des informations et des relations utiles dans un texte     
![ml-amaeonComprehend-schema](./images/aws_solutionArchitectAssociate/ml-amaeonComprehend-schema.png)       

Générez des informations précieuses à partir de texte dans des documents, de tickets d'assistance à la clientèle, d'évaluations de produits, d'e-mails, de flux des réseaux sociaux et plus encore       
Simplifiez les flux de traitement de documents en extrayant du texte, des phrases clés, des rubriques, du ressenti et plus encore à partir de documents, tels que des demandes d'indemnisation         
Différenciez votre entreprise en entraînant un modèle pour classer des documents et identifier des termes, sans la moindre expérience de machine learning          
Protégez vos données sensibles en identifiant et en rédigeant les données d'identification personnelle (PII) dans les documents, et contrôlez leur accès        

**Comprehend** est un **NLP: Natural Language Processing** ➡️ c'est un moyen d'automatiser la compréhension (tient compte des sentiements dans le langage écrit ➡️ ex: sait dire si c'est une phrase positive ou négative)          

![ml-comprehend-usecases](./images/aws_solutionArchitectAssociate/ml-comprehend-usecases.png)        







[Documentation aws - Amazon Kendra](https://aws.amazon.com/fr/kendra/)              
Trouvez des réponses plus rapidement grâce à la recherche du contenu d'entreprise intelligente alimentée par le machine learning       

Amazon Kendra est un service de recherche d'entreprise intelligent qui permet aux utilisateurs d'effectuer des recherches dans différents référentiels de contenu grâce à des connecteurs intégré      
![ml-amaeonKendra-schema](./images/aws_solutionArchitectAssociate/ml-amaeonKendra-schema.png)          


**Kendra** permet la creation d'un service de recherche intelligent basé sur le Machine Learning     
Si des datas sont stockées dans différents emplacements (S3, fichiers, web sites) ➡️ kendra va permettre l'indexation des datas et permettre une recherche intelligente et rapide          
![ml-kendra-usecases](./images/aws_solutionArchitectAssociate/ml-kendra-usecases.png)          




[Documentation aws - Amazon textract](https://aws.amazon.com/fr/textract/)                 
Extrayez automatiquement du texte imprimé ou manuscrit ainsi que des données de pratiquement n'importe quel document         

Permet 1 gain de temps (travail manuel qui dure plusieurs heures et reduit en 1 travail automatisé qui dure quelques minutes)        
Permet d'extraire des datas d'un pdf, tableau, documents manuscrit.....     


**OCR: Optical Character Recongnition**  ➡️ texttract utilise le machine learning et OCR pour la reconnaissance des textes, doc manuscrits, tableaux....
![ml-textTract-usecases](./images/aws_solutionArchitectAssociate/ml-textTract-usecases.png)          






![ml-comprehend-tip](./images/aws_solutionArchitectAssociate/ml-comprehend-tip.png)         
![ml-kendra-tip](./images/aws_solutionArchitectAssociate/ml-kendra-tip.png)         
![ml-texTract-tip](./images/aws_solutionArchitectAssociate/ml-TexTract-tip.png)         




=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - PREDICTING TIME-SERIES DATA USING AMAZON FORECAST** 
</span>


[Documentation aws - Amazon Forecast](https://aws.amazon.com/forecast/)         
Prévoir les résultats commerciaux de manière simple et précise grâce au machine learning          

Amazon Forecast est un service de prédiction de séries temporelles basé sur le machine learning (ML) et conçu pour l'analyse des métriques métier        
![ml-amaeonForecast-schema](./images/aws_solutionArchitectAssociate/ml-amaeonForecast-schema.png)           

Mettez à l'échelle vos opérations en prédisant des millions d'articles, en utilisant la même technologie qu'Amazon.com           
Optimisez les stocks et réduisez le gaspillage grâce à des prédictions précises détaillées       
Améliorez l'utilisation du capital et prenez des décisions à long terme avec plus de confiance      
Améliorez la satisfaction des clients grâce à une dotation en personnel optimale pour répondre à des niveaux de demande variables         

![ml-forecast-tip](./images/aws_solutionArchitectAssociate/ml-forecast-tip.png)     



=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - PROTECTING ACCOUNTS WITH AMAZON FRAUD DETECTOR** 
</span>


[Documentation aws - Amazon Fraud Detector](https://aws.amazon.com/fr/fraud-detector/)           
Détecter les fraudes en ligne avec le machine learning         

Amazon Fraud Detector est un service entièrement géré permettant aux clients d'identifier les activités potentiellement frauduleuses et d'intercepter plus rapidement les fraudes en ligne         
![ml-amaeonFraudDetector-schema](./images/aws_solutionArchitectAssociate/ml-amaeonFraudDetector-schema.png)       

Créez, déployez et gérez des modèles de détection des fraudes sans expérience préalable du machine learning (ML)             

Identifier les paiements en ligne suspects       
Détecter la fraude au nouveau compte           
Prévenir les abus des programmes d'essai et de fidélité           
Améliorer la détection de piratage de comptes          





=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - WORKING WITH TEXT AND SPEECH USING AMAZON POLLY, AMAZON TRANSCRIBE, AMAZON LEX** 
</span>


[Documentation aws - Amazon Polly](https://aws.amazon.com/fr/polly/)               
Déployez des voix humaines de haute qualité et naturelles dans des dizaines de langues         

Amazon Polly exploite des technologies avancées de deep learning pour synthétiser la parole de façon naturelle, de manière à convertir les articles en parole     
Avec des dizaines de voix réalistes couvrant un large éventail de langues, utilisez Amazon Polly pour créer des applications à commande vocale        
![ml-amaeonPolicy_creationContenu-schema](./images/aws_solutionArchitectAssociate/ml-amaeonPolicy_creationContenu-schema.png)      


Personnalisez et contrôlez la sortie vocale qui prend en charge les lexiques et les balises SSML (Speech Synthesis Markup Language)     
Stockez et redistribuez la parole dans des formats standard tels que MP3 et OGG       
Proposez rapidement des voix réalistes et des expériences utilisateur conversationnelles dans des temps de réponse constamment rapides      


**Polly** permet de mettre des textes en audio avec des accents, intonations, plus ou moins fort.... ➡️ cela rend plus interactifs le contenu     






[Documentation aws - Amazon Transcribe](https://aws.amazon.com/fr/transcribe/)         
Convertir automatiquement un discours en texte           

![ml-transcribe-usecase](./images/aws_solutionArchitectAssociate/ml-transcribe-usecase.png)          








[Documentation aws - Amazon Lex](https://aws.amazon.com/fr/lex/)            
Créer des chatbots et des voicebots grâce à l'IA conversationnelle                  


Amazon Lex est un service d'intelligence artificielle (IA) entièrement géré doté de modèles de langage naturel avancés pour la conception, la création, le test et le déploiement d'interfaces conversationnelles dans des applications           
![ml-amaeonLex-schema](./images/aws_solutionArchitectAssociate/ml-amaeonLex-schema.png)             

Intégrez facilement une IA qui comprend l'intention, conserve le contexte et automatise les tâches simples en de nombreuses langues      
Concevez et déployez une IA conversationnelle omnicanale en un clic, sans avoir à vous soucier du matériel ou de l'infrastructure, et ce, même si vous n'avez aucune expérience dans le domaine du ML        
Payez uniquement les demandes textuelles et vocales, sans aucun engagement initial ni frais minimums        

![ml-lex-usecase](./images/aws_solutionArchitectAssociate/ml-lex-usecase.png)          




![ml-alexa-fonctionnement](./images/aws_solutionArchitectAssociate/ml-alexa-fonctionnement.png)          


![ml-trasncribe-tip](./images/aws_solutionArchitectAssociate/ml-trasncribe-tip.png)        
![ml-lex-tip](./images/aws_solutionArchitectAssociate/ml-lex-tip.png)        
![ml-trasncribe-tip](./images/aws_solutionArchitectAssociate/ml-polly-tip.png)            



=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - ANALYZING IMAGES VIA AMAZON REKOGNITION** 
</span>

[Documentation aws - Amazon Rekogntion](https://aws.amazon.com/fr/rekognition/)            
Automatisez et baissez le coût de votre reconnaissance d'images et de votre analyse vidéo grâce au machine learning        


Ajoutez rapidement des API de reconnaissance d'image pré-entraînées ou personnalisables à vos applications sans construire de modèles et d'infrastructure de machine learning (ML) à partir de zéro       
Analysez des millions d'images et de vidéos en streaming et archivées en quelques secondes et augmentez les tâches d'examen humain grâce à l'intelligence artificielle (IA)       
Augmentez et réduisez vos capacités en fonction des besoins de votre entreprise grâce à des fonctionnalités d'IA entièrement gérées et ne payez que pour les images et les vidéos que vous analysez         


Identifiez rapidement et précisément le contenu dangereux ou inapproprié dans les ressources image et vidéo en fonction de normes et de pratiques générales ou spécifiques à l'entreprise       
Utilisez la comparaison et l'analyse faciales dans vos flux d'onboarding et d'authentification des utilisateurs pour vérifier à distance l'identité des utilisateurs inscrits        
Détectez automatiquement les segments vidéo clés pour réduire le temps, les efforts et les coûts de l'insertion de publicités vidéo, des opérations sur le contenu et de la production de contenu           
Produisez des alertes opportunes et exploitables lorsqu'un objet d’intérêt est détecté dans vos flux vidéo en direct         


**Recognition** est utilisé pour la reconnaisance facile et utilise le depp learning avec les reseaux neuronaux     

![ml-rekognition-usecase](./images/aws_solutionArchitectAssociate/ml-rekognition-usecase.png)                 



![ml-rekognition-tip](./images/aws_solutionArchitectAssociate/ml-rekognition-tip.png)                 




=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - LEVERAGING AMAZON SAGEMAKER TO TRAIN LEARNING MODELS** 
</span>


[Documentation aws - Amazon SageMaker](https://aws.amazon.com/fr/sagemaker/)            
Créez, entraînez et déployez rapidement et facilement des modèles de machine learning (ML) pour tous les cas d'utilisation avec une infrastructure, des outils et des flux entièrement gérés         


![ml-sagemaker-overview](./images/aws_solutionArchitectAssociate/ml-sagemaker-overview.png)          

![ml-sagemaker-type](./images/aws_solutionArchitectAssociate/ml-sagemaker-type.png)          

![ml-sagemaker-stages](./images/aws_solutionArchitectAssociate/ml-sagemaker-stages.png)          

![ml-sagemaker-modelTraining-fonctionnement](./images/aws_solutionArchitectAssociate/ml-sagemaker-modelTraining-fonctionnement.png)         

![ml-sagemaker-model-creation](./images/aws_solutionArchitectAssociate/ml-sagemaker-model-creation.png)     
![ml-sagemaker-model-creationPerso](./images/aws_solutionArchitectAssociate/ml-sagemaker-model-creation-personalisee.png)     


![ml-sagemaker-neo](./images/aws_solutionArchitectAssociate/ml-sagemaker-neo.png)     


Sagemaker permet la haute dispo et possede l'autoscaling        
Elastic inference permet de diminuer les couts en optimisants les CPU/GPU ....



=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - TRANSLATING CONTENT INTO DIFFERENT LANGUAGES WITH AMAZON TRANSLATE** 
</span>


[Documentation aws - Amazon Translate](https://aws.amazon.com/fr/translate/)            
Traduction automatique fluide et précise            

Amazon Translate est un service de traduction automatique neuronale qui fournit des traductions rapides, de haute qualité, abordables et personnalisables        
La traduction automatique neuronale est une méthode de traduction automatique qui exploite des modèles de deep learning afin de générer une traduction plus fluide et plus naturelle que les algorithmes de traduction traditionnels, basés sur les statistiques et les règles        

Ce service est très simple à intégrer dans votre applications grace aux APIs   
Son coûts est moindre qu'un traducteur "humain"        
Ce service est scalable à la demande    




=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - EXAM TIPS** 
</span>


![ml-examTip01](./images/aws_solutionArchitectAssociate/ml-examTip01.png)            
![ml-examTip02](./images/aws_solutionArchitectAssociate/ml-examTip02.png)            
![ml-examTip03](./images/aws_solutionArchitectAssociate/ml-examTip03.png)            
![ml-examTip04](./images/aws_solutionArchitectAssociate/ml-examTip04.png)            
![ml-examTip05](./images/aws_solutionArchitectAssociate/ml-examTip05.png)            
![ml-examTip06](./images/aws_solutionArchitectAssociate/ml-examTip06.png)            
![ml-examTip07](./images/aws_solutionArchitectAssociate/ml-examTip07.png)            
![ml-examTip08](./images/aws_solutionArchitectAssociate/ml-examTip08.png)            
![ml-examTip09](./images/aws_solutionArchitectAssociate/ml-examTip09.png)            
![ml-examTip10](./images/aws_solutionArchitectAssociate/ml-examTip10.png)            
![ml-examTip11](./images/aws_solutionArchitectAssociate/ml-examTip11.png)            
![ml-examTip12](./images/aws_solutionArchitectAssociate/ml-examTip12.png)            
![ml-examTip13](./images/aws_solutionArchitectAssociate/ml-examTip13.png)            
![ml-examTip14](./images/aws_solutionArchitectAssociate/ml-examTip14.png)            




=======================================================================================================================      


<span style="color: #EA9811">

###  **ML - QUIZZ** 
</span>


![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz01.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz02.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz03.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz04.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz05.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz06.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz07.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz08.png)             
![ml_quizz01](./images/aws_solutionArchitectAssociate/ml_quizz09.png)             




=======================================================================================================================      

_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **MEDIA** 
</span>


[Documentation aws - media](https://aws.amazon.com/fr/media-services/)        
Créer du contenu numérique et générer des flux de travail vidéo en direct et à la demande.         

AWS offre les services, les logiciels et les appareils multimédias les plus spécifiquement conçus pour faciliter et accélérer la création, la transformation et la diffusion de contenu numérique      


Pour la certification ➡️ aucune question ne porte sur ce chapitre        


**Elastic Transcoder**         
  [Documentation aws - Elastic Transcoder](https://aws.amazon.com/fr/elastictranscoder/)        
  Outil permettant de convertir ou transcoder un media dans son format original en un format optimisé pour les smartphones, tablettes ou ordi      

  Il est très facile à utiliser depuis les API, le SDK ou la console aws     
  Il est aussi scalable: utilisé pour les petits fichiers jusqu'aux "grosses" vidéos    


**Kinesis Video Streams**                  
  [Documentation aws - Kinesis Video Streams](https://aws.amazon.com/fr/kinesis/video-streams/)       
  Capturer, traiter et stocker des flux multimédia pour la lecture, les analyses et le machine learning    

  Utiliser pour les camaras (perso ou publiques) ➡️ reconnaissance avec le machine learning ➡️ permet de différentier un homme d'un chat.....
  Utiliser avec les radars.....




![media-quizz01](./images/aws_solutionArchitectAssociate/media-quizz01.png)                 
![media-quizz02](./images/aws_solutionArchitectAssociate/media-quizz02.png)                 
![media-quizz03](./images/aws_solutionArchitectAssociate/media-quizz03.png)                 
![media-quizz04](./images/aws_solutionArchitectAssociate/media-quizz04.png)                 


_______________________________________________________________________________________________________________________    



<span style="color: #CE5D6B">

##  **AWS EXAM PREPARATION** 
</span>



L'examen peut se faire sur place ou à distance    
Sur place, cela doit se faire dans 1 centre d'examen (univeristé aux USA)     


**A distance**       
Présentation de la carte d'identité + webcam pour valider que nous sommes la bonne personne + montrer que tout est "clair" pour l'examen (personne d'autre, rien à part l'ordi pour passer l'examen dans la pièce) + le micro doit est ouvert durant l'examen

Ne pas lire à voix haute les questions


**Resultats**          
score minimum: 720 / 1000 pour réussir l'exam
Les résultats sont fournis 24h après l'examen


**Enregistrement pour l'examen**           
Il est nécessaire d'avoir un compte amazon.com       
Sur https://aws.training choisir la certification souhaitée    

Si on n'est anglophone cliquer sur `request Exam Accomodations` qui donne 30 minutes de plus pour passer l'examen    



Pour valider **AVANT** le jour de l'exam [aller sur ce site](https://home.pearsonvue.com/Clients/Amazon-Web-Services/Online-Proctored.aspx) et exécuter un test qui va valider que le pc est OK pour le test   





=======================================================================================================================      


<span style="color: #EA9811">

###  **PRACTICE EXAM 01** 
</span>




➡️






=======================================================================================================================      


<span style="color: #EA9811">

###  **PRACTICE EXAM 02** 
</span>












=======================================================================================================================      


<span style="color: #EA9811">

###  **PRACTICE EXAM 03** 
</span>











=======================================================================================================================      


<span style="color: #EA9811">

###  **PRACTICE EXAM 04** 
</span>












=======================================================================================================================      


<span style="color: #EA9811">

###  **PRACTICE EXAM 05** 
</span>











=======================================================================================================================      

_______________________________________________________________________________________________________________________    

# FIN