# <span style="color: #CE5D6B"> Mastering the aws Well-Architected Framework </span>




- [lien ressources formation: aws architecture center](https://aws.amazon.com/architecture/?awsf.quickstart-architecture-page-filter=highlight%23new&cards-all.sort-by=item.additionalFields.sortDate&cards-all.sort-order=desc&awsf.content-type=*all&awsf.methodology=*all&awsf.tech-category=*all&awsf.industries=*all&awsf.business-category=*all)       
- [lien ressources formation: this is my architecture](https://aws.amazon.com/architecture/this-is-my-architecture/?tma.sort-by=item.additionalFields.airDate&tma.sort-order=desc&awsf.category=*all&awsf.industry=*all&awsf.language=*all&awsf.show=*all)       
- [lien ressources formation: network protection for advanced threats & malware](https://d1.awsstatic.com/Marketplace/scenarios/security/SEC_02_TSB_Final.pdf)        
- [lien ressources formation: aws well-architected](https://aws.amazon.com/architecture/well-architected/?wa-lens-whitepapers.sort-by=item.additionalFields.sortDate&wa-lens-whitepapers.sort-order=desc&wa-guidance-whitepapers.sort-by=item.additionalFields.sortDate&wa-guidance-whitepapers.sort-order=desc)      
- [lien ressources formation: customer success stories](https://aws.amazon.com/solutions/case-studies/)    


_______________________________________________________________________________________________________________________ 


<span style="color: #CE5D6B">

## Ce framework est organisé en 5 piliers:
</span>

- operation excellence
- security 
- cost optimization 
- reliability
- performance efficiency


_______________________________________________________________________________________________________________________ 


<span style="color: #CE5D6B">

## LES 5 PILIERS:

</span>

1. **operation excellence**

	le design de l'architecture pour qu'elle fonctionne     
	tout ce qui est fait doit être codé     
	➡️ dans ce pilier on parle de l'automatisation    
	tout doit être documenté     
	faire des petites modifications ... mais beaucoup pour eviter les rollbacks    

2. **security** 

	est-ce que le system fonctionne comme attendu?    
	vérifier les privilèges    
	qui fait quoi et quand?    
	le securité fait parti de l'architecture du systeme    
	les taches sont automatisées    
	encrypter les données     
	se préparer au pire     

3. **cost optimization** 

	dépenser seulement ce que nous utilisons    
	mesures efficaces de ce qui est utilisé (ex: + de ram permet d'aller plus vite mais est plus cher ➡️ trouver le bon ratio)    
	laisser aws faire le travail quand c'est possible     

4. **reliability** 

	est-ce que le systeme fonction correctement et se relance rapidement?    
	relance automatique suite aux erreurs    
	deduire les ressources inactives    
	manager les modif depuis l'automatisation     

5. **performance efficiency**

	reduire les goulots d'étranglements, reduire les déchets     
	laisser aws faire le travail tant que possible     
	reduire les latences à travers les regions et les passerelles aws      
	utiliser les serverless si possible     
	tester les nouveaux services et leurs releases (amélioration apportées utiles)     
	penser aux utilisateurs et pas aux tech de notre stack     


_______________________________________________________________________________________________________________________ 


<span style="color: #CE5D6B">

## OPERATION EXCELLENCE
</span>

2 questions:
	est-ce que notre architecture fonctionne?    
	va t elle continuer de fonctionner?     

➡️ tout doit être en "code", les actions faites depuis la console doivent être codées    
➡️ la doc doit être faite et être à jour     
➡️ iterate au maximum (pleins de petites modif)     
➡️ préparer les arrivées futures (app...)     
➡️ apprendre des erreurs et succès      

Il y a 3 phases pour réussir cela :     
- organisation et préparation ➡️ anticiper au maximum      
- operate                     ➡️ automatiser tout ce qui est possible      
- evoluer                     ➡️ apprendre tant que possible     


=======================================================================================================================    

<span style="color: #EA9811"> 

### OPERATION EXCELLENCE: ORGANIZE AND PREPARE 
</span>

      1ere video de ce chapitre



  

======================================================================================================================= 

_______________________________________________________________________________________________________________________ 

<span style="color: #CE5D6B">

## SECURITY 
</span>


_______________________________________________________________________________________________________________________   

<span style="color: #CE5D6B">

## COST OPTIMIZATION 
</span>




_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

## RELIABILITY
</span>




_______________________________________________________________________________________________________________________ 

<span style="color: #CE5D6B">

## PERFORMANCE EFFICIENCY 
</span>
  



 

_______________________________________________________________________________________________________________________ 