_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

##  **RESUME FORMATION AWS SOLUTION ARCHITECT** 
</span>


<div class="resume" style='padding:0.1em; background-color:#29376C; color:#1CF6E5'>
<span>

=======================================================================================================================  
### <span style="color: #EA9811">**AWS FUNDAMENTALS**</span>


AWS est découpé en différentes **regions** qui contiennent différentes **Availability Zones**    
Une **région** est une localisation physique dans le monde   
Une **AZ** est 1 ou plusieurs data center avec son reseau electrique, internet et dans des entrepots séparés    


AWS propose des **services**    
Service type **Compute**: EC2, Lambda, elastic Beanstalk...     
Service type **Storage**: S3, EBS, EFS, FSx, Storage Gateway....    
Service type **Databases**: RDS, DynamoDb, Redshift....           
Service type **Network**: VPCs, Direct Connect, Route53 (DNS), API Gateway...    


AWS est responsable des OS hôte jusqu'à la couche virtualisation     
Le client est responsable des OS invité, application, configuration du firewall, mise en place des securités "entreprise" (IAM, security group, SCPs....)    


Les 6 piliers du framework AWS:    
- **operational excellence**: running and monitoring systems    
- **performance efficiency (efficacité)**: using IT and computing resources afficiently    
- **security**: protecting informations et systems    
- **cost optimization**: avoiding unnecessary cost    
- **reliability (fiabilité)**: ensuring a workload performs     
- **sustainability (durabilité)**: minimizing the environmental impacts


=======================================================================================================================  
### <span style="color: #EA9811">**IAM**</span>


**IAM: Instance and Access Management**    


IAM crée les users et donne les permissions    
IAM crée les groupes et les roles     
IAM controle les accès aux ressources AWS     
IAM est universel (global) ➡️ il ne dépend pas des regions    


IAM commence avec le **root account**     
Le **root account** permet l'administration complète d'AWS  ➡️  nécessaire de sécuriser ce compte:
- mettre en place le MFA    
- créer un groupe admin et assigner les permissions appropriées    
- créer des comptes administateurs     
- ajouter des users dans le groupe admin             


IAM fonctionne avec des **policies**    
Une policy est un fichier json qui respecte un format défini avec des clés spécifiques:    
_exemple de policy_:    

```json
{
  "Version": "2012-10-17",                                            ## version de policy à utiliser (la dernière est 2012-10-17)
  "Statement": [                                                      ## container des éléments. Possible d'avoir plusieurs "statement"
    {
      "Sid": "FirstStatement",                                        ## (optionnel)  inclu un statement ID pour pouvoir différentier les statements
      "Effect": "Allow",                                              ## (obligatoire) "Allow" ou "Deny" sont les seules possibilités -> définit le droit sur les actions suivantes
      "Action": ["iam:ChangePassword"],                               ## inclu une liste d'actions de la policy
      "Resource": "*"                                                 ## indique sur qui la policy va appliquer les actions avoir les droits définis
    },
    {
      "Sid": "SecondStatement",
      "Effect": "Allow",
      "Principal": {"AWS": ["arn:aws:iam::account-id:root"]},         ## (nécessaire que dans certains cas): policy resource-based il est OBLIGATOIRE d'indiquer l'account, user, role ou federated user / policy IAM permission pour attacher un user ou un role
      "Action": "s3:ListAllMyBuckets",
      "Resource": [
        "arn:aws:s3:::mybucket",
        "arn:aws:s3:::mybucket/*"
      ]
    },
    {
      "Sid": "ThirdStatement",
      "Effect": "Allow",
      "Action": [
        "s3:List*",
        "s3:Get*"
      ],
      "Resource": [
        "arn:aws:s3:::confidential-data",
        "arn:aws:s3:::confidential-data/*"
      ],
      "Condition": {"Bool": {"aws:MultiFactorAuthPresent": "true"}}   ## (optionnel)  Spécifie les circonstances dépendantes des droits de la policy
    }
  ]
}
```

>_Remarque_: Si dans les policies se trouve un conflit entre un _Allow_ et un _Deny_ pour la même action ➡️ par defaut se sera **Deny** qui sera pris en compte    


IAM utilise des blocks:    
- **users**: personnes physiques    
- **groups**: fonctions de personnes (ex: dev, admin ....). Un groupe contient des users    
- **roles**: Usage interne AWS    


Les bonnes pratiques sont de faire hériter les permissions des users depuis les groupes, donner à un user les droits minimum pour que le job soit fait    
Un nouvel utilisateur n'a (par defaut) aucun droit lorsqu'il est créé     


Un **Identity provider** (gére les users identity en dehors de aws) est utilisé dans une organisation qui a déjà son systeme de gestion des identités.     
**IAM Federation**: il est possible de combiner notre user account aws avec le login de notre PC (via AD)  ➡️ utiliser les mêmes credentials     
**Identity federation**: utilise SAML avec l'AD    


Un **principal** est **un user ou un role qui fait la requête à la ressource**     

AWS gère 6 types de policies:    
- **Identity-based policies (Politiques basées sur l'identité)**: policies IAM qui gèrent directement les droits des identités IAM (users, groupes, roles)     
- **Resource-based policies (Politiques basées sur les ressources)**: policies des ressources. Ces policies accordent des droits au **principal** qui sont spécifiés dans la policy    
- **Permissions boundaries (Limites d'autorisations)**: policy gérée en tant que limite d'autorisations pour une entité IAM (utilisateur ou rôle)    
- **Organizations SCPs (Politiques de contrôle des services Organizations)**: policy de contrôle des services (SCP) AWS Organizations pour définir les autorisations maximales pour les membres du compte d'une organisation ou d'une **unité d'organisation (OU)**    
- **Access control lists (ACLs) (Listes de contrôle d'accès)**: utilisez les listes de contrôle d'accès (ACL) pour contrôler quels principaux dans d'autres comptes peuvent accéder à la ressource    
- **Session policies (Politiques de session)**: limitent les autorisations que les politiques basées sur l'identité du rôle ou de l'utilisateur accordent à la session


Les **Access Key ID** et les **Secret Access Keys** ne sont pas identiques (comme username et password)    
Ces Keys ne sont visibles qu'1 SEULE fois  ➡️ si on les perd il faut les regenerer   ➡️ les conserver dans un endroit sûr      
Toujours mettre en place une rotation de passwords (par defaut IAM ne le met pas en place)  ➡️ il est possible de créer sa propre policy de password rotation    
  


=======================================================================================================================  
### <span style="color: #EA9811">**S3**</span>


**S3: Simple Service Storage**      


**S3**:    
- **object Storage (stockage d'objets)**: il fournit un stockage d'objets securisés, durables, hautement scalables     
- **Scalable**: il nous autorise à conserver/récupérer n'importe quelle quantité de données depuis n'importe où sur le web à un très bas prix     
- **Simple**: il est facile à utiliser avec une simple interface web    


Il est **object-based storage**: Il permet de gérer les données en tant qu'objets plutôt qu'en tant que fichiers de systeme ou blocks de données    
A utiliser pour conserver les données statiques (photos, vidéos, code, doc, ....)    
Il **NE PEUT PAS** être utilisé pour exécuter un OS ou une database    


**S3 Bucket** a un nom universel   ➡️ tous les accounts aws partage le S3 namespace. Chaque S3 bucket a un **nom unique**    
Sa syntaxe est: `https://<bucket-name>.s3.<Region>.amazonaws.com/<key-name>`   
Une requête vers S3 retourne un code 200 HTTP    


Il conserve les données sous forme **Key-value**     
Il a aussi une version ID pour un objet et des metadatas (le versioning est payant)    


Ses données sont partagées à travers de multiples devices    
  ➡️ disponibilité: 99.99%    
  ➡️ durabibilité: 99.99999999% ➡️ les données ne sont pas supprimées ni perdues    


Les caractéristiques de S3:    
- **High availability and durability**    
- **Design for frequent access**   (accès fréquents aux données)    
- **Adaptés à la plupart des charges de travail** (utilisé aussi pour l'analyse de big data, plateforme de jeux vidéos...)    

>_Remarque_: S3 peut s'adapter aux uses cases auxquels on le confronte.



**S3 sécurise les données**:    
- **Server-Side encryption**: on peut définir par defaut une encryption dans un bucket de tous les nouveaux objets du bucket     
- **ACL**: un accès en fonction des groupes ou accounts. Ils peuvent être attachés à un objet seul dans 1 bucket    
- **bucket policies**: les policies spécifiques S3 bucket vont spécifier quelles actions sont autorisées ou refusées sur S3    

>_Remarque_: les policies S3 sont comme les policies IAM des fichiers json qui donnent aussi des autorisations ou des interdictions    


**S3 a une forte cohérence entre lecture et écriture**:    
- écriture réussie **PUT**  ➡️  la prochaine requête de lecture va afficher la dernière version de l'objet     
- forte cohérence: pour la liste des opérations ➡️ après l'écriture il est possible d'obtenir la liste des objets avec les changements effectués


S3 peut contenir des fichiers de 0 bytes à 5 Tb    
Le nombre d'objets et son volume dans 1 S3 bucket n'a pas de limites    


Les **ACLs (Access Control List)** sont utilisées pour **1 objet SEUL**     
Les **Bucket Policies** sont utilisées pour généraliser les **regles à tout S3 Bucket**     


Par défaut à la création d'un S3 bucket privé ➡️ les accès publiques ne sont pas autorisés    
➡️ il faut les modifier en ajoutant des ACLs public si nous voulons intervenir sur 1 objet individuel     
➡️ il faut modifier les Bucket Policies si on veut mettre la regle sur le bucket entier     


Il est possible d'héberger un site statique sur S3 (pages html)    
Un site dynamique a besoin d'une database       


S3 gère son scale automatiquement     


**S3 gère le versionning**:    
- **toutes les versions**: toutes sont conservées dans S3. Cela inclu toutes les écritures même les delete de l'objet    
- **backup**: très bon outil de backup    
- **ne peut pas être désactivé**: une fois activé le versionning peut être **suspendu** MAIS PAS désactivé    
- **regles de cycle de vie**: peut être intégré dans une regle de cycle de vie     
- **supporte le MFA**: peut avoir l'authentification MFA   


**S3 possède 5 Storage Classes**:       
- **S3 Standard**:    
  - haute dispo et durée (au moins 3 AZs)  ➡️ 99.99% dispo  / 99.999999% durée (ne perd pas la donnée)        
  - conçu pour des accès fréquents    
  - adapté à la plupart des workloads   ➡️  par defaut stocke les données / cas d'usages websites, mobile et gaming applications, big data analytics         

- **S3 standard-Infrequent Access**: **S3 Standard IA**   
  - accès rapide (mais peu fréquent)   
  - on paie pour accéder à la donnée    
  - cas d'usage: backup, disaster recovery  ➡️ long terme    

- **S3 One Zone-Infrequent Access**: comme _S3 Standard IA_ mais les données sont dans 1 seule AZ  ➡️ 20% moins cher  / que des données NON critiques  (99.5% availability; 99.9999999% durability)    

- **S3 Intelligent-Tiering**: utilisé pour l'utilisation de données peu fréquente  ➡️ gain d'argent       

- **S3 Glacier**: permet les archives   
  - **glacier instant retrieval**: accès aux datas en quelques millisecondes    
  - **glacier flexible retrieval**: accès aux datas en quelques minutes à 12h (moins cher)    
  - **glacier deep archive**:  accès aux datas en 12 à 48h (le moins cher)     


Cycle de vie de S3 (si pas d'activités): **S3 Standard** (30 jours)  ➡️  **S3 IA** (31 à 90 jours) ➡️ **S3 Glacier** (après 90 jours)    
On peut agir aussi pour ce même cycle de vie sur le versionning des données     


**S3 Object Lock** est utilisé comme magasin d'objets qui ont pour usage: writing once, read many (WORM)  ➡️ pas d'effacement par erreur possible    
**S3 Governance mode** n'autorise pas à 1 user la modification d'un objet SANS modifier le paramètre du lock ➡️ il faut avoir les autorisations nécessaires    
**S3 Compliance mode** n'autorise AUCUN user (y compris root) à modifier un objet (durant la période définie)      
**S3 Retention Periods** est la durée de la rétention de l'objet en l'"état"    
**S3 Legal Holds** permet de mettre en place une durée de rétention légale (avec S3 Object Lock)     
**S3 Glacier Vault Lock** permet de déployer et d'appliquer des contrôles de conformité pour un coffre S3 Glacier individuel avec une stratégie de verrouillage de coffre     


**Encryptions S3**:     
  - **S3 Encryption en transit**: Se fait via les certificats SSL/TLS et/ou le port 443 avec le protocole HTTPS    
  - **S3 Encryption au repos: Server-Side encryption**: Il existe 2 méthodes ➡️ depuis la console aws / depuis la bucket policy
    - **SSE-S3**: utilise des S3-managed keys avec AES 256-bit    
    - **SSE-KMS**: aws Key Management Service-managed keys      
    - **SSE-C**: Customer-provider keys
  - **S3 Encryption au repos: Client-Side encryption**: Les fichiers sont encryptés avant d'être uploadés dans S3       


La mise en place d'un **prefixe** permet une optimization de la performance        
  ➡️ plus il y a de prefixes dans notre bucket meilleure seront les performances de notre S3 bucket        
      Il est possible d'atteindre `3500 PUT/COPY/POST/DELETE` requêtes ET `5500 GET/HEAD` requête par seconde et par préfixe        
      en utilsant 2 prefixes on peut doubler le nombre de requêtes par secondes (5500 ➡️ 11000 requetes)      


Il y a des limitations de performance avec **KMS**:        
  - upload/download fichiers sont "bloqués" par les quotas KMS     
  - il n'est pas possible d'augmenter un quota KMS    
  - ces taux sont dépendants des regions (region-specific): ils varient de 5500, 10000 ou 30000 requêtes par secondes    


La **performance d'upload** peut se faire par utilisation de **multinode**:    
  - recommendé pour les fichiers de plus de 100Mb     
  - obligatoire pour les fichiers de plus de 5Gb     
  - il est possible de paralléliser les uploads     


La **performance de download** se fait via **S3 Byte-range fetches**:    
  ➡️ parralélisation des downloads en spécifiant les byte ranges    


**replication S3**:    
  ➡️ un objet d'un bucket va vers un autre bucket (le versionning est primordial)    
  ➡️ les objets d'un bucket ne sont pas répliquer automatiquement  ➡️ il faut déclencher la replication (TOUS les objets seront repliqués sauf les objets déjà existant dans la version)  ➡️ il est conseillé de créer 2 buckets (**1 source + 1 destination** pas forcément dans la même region, suivant notre choix)    
  ➡️ supprimer les versions ou les marqueurs qui ne sont pas à repliquer    



=======================================================================================================================  
### <span style="color: #EA9811">**EC2**</span>


**EC2: Elastic Compute Cloud**      


C'est une ressource fondamentale de AWS     
Elle permet de faire des calculs     
Elle est sécurisée et scalable       
Elle correspond a une VM dans notre datacenter mais chez AWS    
Elle est créée pour un **web-scale computing** ➡️ le but est d'avoir la capacité qu'on souhaite lorsqu'on le souhaite    
On ne paie QUE ce qu'on utilise         
Elle est très rapide à créer et à provisionner     


**Différents type d'EC2 (option de paiement)**:    
- **On-demand**: elle se paie à l'heure ou à la seconde et dépend de l'instance qui fonctionne (1 "grosse" instance sera plus chere qu'une "petite" instance)        
- **reserved**: on réserve 1 capacité de 1 ou 3 ans  ➡️ jusqu'à 72% d'éconnomie par heure      
- **spot**: utilisée seulement si ma capacité est disponible ➡️ jusqu'à 90% d'éconnomie mais le prix varie en fonction de la demande  ➡️ **le moins cher**  
- **dedicated**: une serveur EC2 physique réservé ➡️ **le plus cher**   

Les réservations d'instances se font soit à un niveau régional soit à 1 niveau zonal (zone aws)    
Le prix ne varie pas entre ces 2 choix     


**EC2 On-demand**:     
  - **flexible**: couts faibles et flexibles sans mettre en avant un contrat longue durée (sans engagement)       
  - **short-term**: utilisé pour des applications courtes-durées, "pointues" (d'un grand niveau de complexité), qui ne peut pas anticiper la charge et ne peut pas être interrompues      
  - **testing-the-water**: les applications ont été développées et testées sur aws EC2 la 1ere fois      
➡️ on **paie** la **capacité de calcul** et seulement les secondes pour lesquelles les instances sont à l'**état running**    
Le prix à la seconde est fixe     
Le nombre d'instances à la demande en cours d'exécution se compte par compte AWS par Région     
Les limites d'instances à la demande sont gérées en termes de nombre d'unités centrales virtuelles (vCPU) que vos instances à la demande en cours d'exécution utilisent, quel que soit le type d'instance


**EC2 Reserved instances**:     
  - **predictable usage**: applications avec un usage prévisible sur le long terme
  - **specific capacity requirements**: applications qui ont besoin d'une capacité réservée
  - **pay up front**: planification sur le long-terme de la tarification (abonnements ➡️ contrats) ▶️ réduire les couts
  - **standard Reserved Instances(RIs)**: jusqu'à 72% de réduction sur le prix "on-demand"
  - **convertible RIs**: jusqu'à 54% de reduction sur le prix "on-demand". Contient une option de changement pour un autre type de RI équivalent ou d'une valeur plus importante
  - **scheduled RIs**: démarre dans les plages horaires définies qui correspond à la réservation des machines (seulement pour une partie de journée, semaine, mois, ...)    
Ces instances sont un engagement sur 1 ou 3 ans       
Avec les instances réservées, vous payez pour toute la durée de l'abonnement et non en fonction de l'utilisation réelle     
Lorsque des instances réservées expirent, le tarif à la demande est facturé pour l'utilisation d'instance EC2 on-demand    


**EC2 Spot Instance**:     
  - Une instance spot est une instance qui utilise la capacité EC2 de ressources non utilisées ➡️ elles sont beaucoup moins chères (jusqu'à 90%) mais ne sont pas disponibles lorsqu'on le souhaite (indépendant de nous)     
  - L'instance s'exécute à chaque fois que celle-ci est disponible       
  - Les instances Spot sont recommandées pour les applications flexibles sans état, tolérantes aux pannes      
Pas de garantie que les instances soient disponibles jusqu'à la fin du travail ni qu'on puisse avoir une dispo immédiate     

Concepts EC2 Spot:    
  - **Groupe de capacités Spot** – Un ensemble d'instances EC2 inutilisées avec le même type d'instances (par exemple, m5.large) et la même zone de disponibilité      
  - **Prix Spot** – Prix horaire actuel d'une instance Spot: celui-ci varie régulièrement          
  - **Demande d'instance Spot** – Lorsque la capacité est disponible, Amazon EC2 répond à votre demande. Une demande d'instance Spot est soit One-time (Unique) soit Persistent (Persistante)    
  - **Recommandation de rééquilibrage d'instance EC2** – Amazon EC2 émet un signal de recommandation de rééquilibrage d'instances pour vous avertir qu'une instance Spot présente un risque élevé d'interruption    
  - **Spot Instance interruption (Interruption d'instance Spot)** – Amazon EC2 résilie, arrête ou met en veille prolongée votre instance Spot lorsque Amazon EC2 a besoin de récupérer la capacité      


Lorsque le prix passe au-dessus de la limite choisie ➡️ un message est envoyé et nous avons 2 minutes pour décider si on stoppe ou continu l'utilisation de l'instance    
Mise en place un **spot block** ➡️ arrêter mais ne pas supprimer nos instances spot en cas de prix suppérieur à notre limite. (si un workload doit absolument se terminer avant d'arrêter l'instance)     
Nous pouvons définir un spot block entre 1 à 6 heures     


Une **spot fleet** est une collection d'instances spots et d'instances on-demand    
Elle se lance quand le nombre d'instances à atteint le nombre souhaité     
La requête est atteinte lorsque les conditions de la capacité d'instances ET le prix maximum spécifié sont remplis    
La spot fleet aura pour but de ré atteindre sa capcité cible si des instances spots sont interrompues    


Stratégies des spot fleet:    
  - **capacityOptimized**: les instances spot viennent d'un pool avec la capacité optimale pour le nombre d'instances spot démarrées          
  - **diversified**: les instances spot sont disponibles à travers plusieurs pools    
  - **lowestprice**: les instances spot se trouvent dans le pool le moins cher (stratégie par defaut)    
  - **instancePoolsToUseCount**: les instances spot sont disponibles à travers un nombre défini de pool d'instances spot. Le parametre est valide seulement si est associé à lowestPrice    



**EC2 Dedicated Hosts**:        
  Serveur physique EC2 dédié     

Il existe 4 types d'usage:    
  - **compliance**: exigences réglementaires qui peuvent ne pas prendre en charge la virtualisation mutualisée (partage de hardware entre différents utilisateurs aws: une banque ne peut pas partager les cpus ou autre hardware avec un utilisateur "classique" ➡️ risque de piratage )
  - **on-demand**: on peut utiliser un hote dédié à la demande et payer à l'heure
  - **licencing**: super licence qui ne prend pas en charge les déploiements multi-locataires ou cloud (un hôte avec une licence microsoft ne peut pas fonctionner avec des applications linux par exemple)
  - **reserved**: on peut réserver un hôte dédié pour une utilisation ponctuelle (- 70% par rapport à on-demand)

Les Hôtes dédiés vous permettent d'utiliser vos licences logicielles existantes par socket, par cœur ou par machine virtuelle   ➡️  dans ce cas on est responsable de tout           


**Role IAM**:     
  - Un rôle IAM est une **identité IAM que vous pouvez créer dans votre compte et qui dispose d'autorisations spécifiques** (qui sont données par les policies auxquelles il est rattaché)     
  - Un rôle **n'est pas associé à un user unique**, il est conçu pour être endossé par tout utilisateur qui en a besoin       
  - Utiliser des rôles pour déléguer l'accès à des utilisateurs, des applications ou des services qui n'ont normalement pas accès à vos ressources AWS     
  - Un rôle ne dispose pas d'informations d'identification standard à long terme: est utilisé que pendant la session

Ces roles peuvent être utilisés par: 
  - Un utilisateur IAM qui appartient au même Compte AWS que le rôle       
  - Un utilisateur IAM dans un Compte AWS différent de celui du rôle              
  - Un service web proposé par AWS, par exemple Amazon Elastic Compute Cloud (Amazon EC2)                  
  - Un utilisateur externe authentifié par un service de fournisseur d'identité (IdP) externe, compatible avec SAML 2.0 ou OpenID Connect, ou un broker d'identité personnalisé               


**EC2 Security Groups et Bootstrap Scripts**:       
  - Un **security group** contrôle le trafic autorisé à atteindre et à quitter les ressources auxquelles il est associé (équivalent **iptables**)        
  - Un **bootstrap script** est un script qui doit être joué à la suite de la création de l'instance (équivalent **docker-compose**)     

A la création d'un **VPC** un security group par defaut est fourni     
Il est possible de créer autant de security group que souhaité par VPC      
Pour chaque security group, vous ajoutez des règles qui contrôlent le trafic en fonction des protocoles et des numéros de port. Par defaut tout est fermé            
L'utilisation de security group n'ajoute aucun frais supplémentaire     


Un bootstrap script permet la fianlisation de la creation d'une instance.      
C'est un simple script bash (pour instance linux ➡️ fait des `yum install`, `yum update`, `mkdir`, ...)     


**EC2 Metadata et Userdata**:      
  Pour récupérer ces datas, un curl suffit     
  - metadata: `curl http://169.254.169.254/latest/meta-data/<NomMetadataSouhaitee>`      
  - userdata: `curl http://169.254.169.254/latest/user-data`  

userdata ➡️ simples bootstrap scripts    
metadata ➡️ data des datas. on peut utiliser un bootstrap script pour accéder aux metadata            


**EC2 Networking**:           
  - Une instance EC2 appartient à 1 sous-reseau qui est dans 1 VPC      
  - Pour augmenter les performances reseau et réduire la latence ➡️ mettre les instances dans 1 **placement group**    


Il y a 3 différents types de virtual networking cards pour les instances EC2:     
  - **ENI: Elastic Network Interface**    ➡️  pour le basic: networking day-to-day 
      ➡️ virtual network card simple (ipv4 privées et publiques, ipv6, MAC_ADRESS, 1 ou + security group)
  - **EN: Enhanced Networking**           ➡️  utilise un root I/O virtualization pour améliorer la haute performance     
      ➡️ utilisé pour la haute performance reseau (10 à 100 Gb/s)
  - **EFA: Elastic Fabric Adapter**       ➡️  accelère le HPC (High Performance Computing) et appli machines learning     
      ➡️ utilisé pour la très haute performance reseau (>100Gb/s)   ➡️ sur les instances récentes (sinon c'est VF: Virtual Function)    


**EC2 Placement Group**:    
  ➡️ un placement group sert à influencer le placement des instances interdépendantes pour répondre à un besoin    
  ➡️ 3 placement group différents:     
    - **cluster**: instances rapprochées dans 1 AZ ➡️ très bonne performance reseau     
    - **partition**: instances réparties entre partitions logiques pour qu'elles ne soient pas sur le même matériel  ➡️ pour les grosses charges de travail (Cassandra, Hadoop, Kafka)    
    - **spread (répartition**): seulement un petit groupe d'instances sur 1 même matériel pour réduire les défaillances     


Pas de frais pour la création d'un placement group     


Regles de placement group(PG):          
  - **max 500 PGs** par compte dans chaque region    
  - **nom unique** au sein du compte pour la region    
  - PAS de fusion de PGs    
  - **1 instance dans 1 SEUL PG** à la fois    
  - **reservation de capacité on-demand** et **instances reservees zonales** ➡️ reservation de capacité pour les instances EC2 dans 1 AZ spécifique             
  - **PAS** d'hote dédié dans 1 PG    
  - seulement certains types d'instances peuvent être exécutées dans 1 PG (compute optimized, GPU, memory optimized, storage optimized)     
  - AWS recommande des **instances homogènes dans 1 PG**    
  - pour déplacer 1 instance dans 1 PG il faut stopper l'instance PUIS la déplacer    


**EC2 et VMWare**:     
  - **hybrid cloud**: connexion sur on-premise vers le cloud pour gérer les workload hybrides    
  - **cloud migration**: migration de on-premise vers le cloud via les outils VMWare built-in     
  - **disaster recovery**: cela permet un DR gratuit depuis aws vers VMWare     
  - **leverage aws**: aws a + de 200 services pour update des applications ou création de nouvelles     ,

➡️ solution parfaite pour un cloud privé dans le cloud publique     


**AWS Outposts**:     
  ➡️ il est possible d'étendre le cloud avec **aws outpost**    
  ➡️ **AWS Outposts est un service entièrement géré qui étend l'infrastructure AWS, les services, les API et les outils aux sites du client**    
  ➡️ en fournissant 1 accès local à l'infra, aws permet de créer et d'exécuter des applications sur site à partir de la même interface aws console (installation faite par aws dans le datacenter client)  ➡️ reduit les latences    

outpost rack ➡️ pour de grosses infra     
outpost servers ➡️ pour de petites infra    




=======================================================================================================================  
### <span style="color: #EA9811">**EBS - EFS**</span>


**EBS** : **Elastic Block Storage**    
**EFS** : **Elastic File System**  




**EBS**      
  Volumes de différents types attachés aux EC2 (sorte de disque dur virtuel)    
  
  Ils sont créés pour:    
  - workloads     
  - disponibilité ➡️ 1 replica dans 1 AZ pour protéger contre les plantages hardware    
  - scalable ➡️ dynamique en fonction de la capacité et change de type de volume en fonction des performances qui impactent le systeme     

  
  **Amazon EBS Multi-Attach** vous permet d'attacher un volume SSD IOPS provisionnés (io1 ou io2) à plusieurs instances situées dans la même zone de disponibilité    
  Chaque volume attaché a une autorise rw sur le vol partagé     

  *Multi-attach*      
  ➡️ max 16 instances linux dans la même AZ (systeme Nitro)         
  ➡️ prend en charge exclusivement des vol _io1/io2_      
  ➡️ ne peuvent pas être des vol de boot    
  ➡️ ne peut pas être activé lors du lancement de l'instance via la console AWS    
    

  *EBS types de volumes*:    
  - **gp2**     
  ➡️ 3 I/O par seconde par Gb   ➡️ max 16000 I/O par sec par vol   
  ➡️ vol < 1Tb ➡️ jusqu'à 3000 I/O par sec     
  ➡️ bon choix pour boot vol ou dev/test applications    

  - **gp3**     
  ➡️ 3000 I/O par sec et 125Mb/s sans tenir compte de la taille du volume       
  ➡️ idéal pour les app Hautes performances (Casandra, hadoop, virtual desktop....)    
  ➡️ pour users qui cherchent 1 gain de performance 16000 I/O par sec et 1000Mb/s additionnel     
  ➡️ performances gp3 ➡️ 4 * > gp2    

  - **SSD: io1**    
  ➡️ 64000 I/O par sec par volume ➡️ 50 I/O par sec par Gb     
  utilisé que lorsque nécessaire car cher    

  - **SSD io2**     
  ➡️ dernière generation ➡️ même tarif que io1 mais plus performant dans la durée    

  - **HDD st1**     
  ➡️ "vieux" volume magnetique       
  ➡️ 40Mb/s par Tb     
  ➡️ jusqu'à 250Mb/s par Tb    
  ➡️ max 500Mb/s par vol     
  ➡️ fréquement utilisé via loadwork intensifs     
  ➡️ big data, data warehouses, ETL, log processing      
  ➡️ NE peut pas être un boot volume    

  - **HDD: sc1**           
  ➡️ **le moins cher**     
  ➡️ 12 Mb/s par Tb     
  ➡️ jusqu'à 80 Mb/s par Tb     
  ➡️ max 250Mb/s par vol     
  ➡️ NE peut pas être un boot volume     


  *Volumes et Snapshots*         
  ➡️ **volume**: disque dur virtuel      
  ➡️ 1 instance EC2 = au moins 1 volume     ➡️ **root device volume**      

  ➡️ **snapshot**: "photo" d'1 volume à 1 instant t (sont sur S3)     
  ➡️ ils sont incrémentials ➡️ le 1er est le plus gros    
  3 sortes de snapshots:      
    - **consistent snapshot**: on DOIT stopper l'instance pour prendre ce snapshot     
    - **encrypted snapshots**: vol EBS encrypté ➡️ snapshot encrypté    
    - **sharing snapshots**: possible de partage de snapshot SEULEMENT dans la region où ce snapshot se trouve   
      ➡️ pour partage dans 1 autre region, faire une copie puis la déplacer dans une autre region    

  
  *Location*      
  ➡️ EBS vol TOUJOURS dans même AZ que EC2     

  *Resizing*     
    ➡️ fait à la volée     

  *Vol Type*     
  ➡️ possibilité de changer de type de vol à la volée    

  *Suppression volume*     
  ➡️ se fait en 2 étapes:                 
    - détacher le volume (attendre que cette action soit terminée)     
    - supprimer le volume     

  
  *Encryption*     
  ➡️ utilise **aws kms** (Key Management Service) avec une **cmk** (Customer Master Key) lors de creation des vol ou des snapshots    

  ➡️ les datas sont encryptées DANS le volume     
  ➡️ les datas en transit entre l'instance et le volume sont encryptées      
  ➡️ les snapshots sont encryptés     

  ➡️ l'encryption se gere seule     
  ➡️ latence très faible     
  ➡️ la copie de snapshot autorise l'encryption     
  ➡️ possible d'encrypter un boot volume lors de sa creation     

  ➡️ encryption d'une instance à partir d'une AMI encryptée: 4 étapes     
    - créer un snapshot du root volume NON encrypté     
    - créer une copie du snapshot + ajouter les options d'encrytion     
    - créer une AMI depuis le snapshot encrypté     
    - utiliser cette AMI pour lancer une nouvelle instance encryptée   

  *Hibernation*    
    ➡️ lors d'une hibernation les data en mémoire s'écrivent sur le root volume EBS          
  ➡️ à la sortie de l'hibernation elles seront remises en mémoire     
  ➡️ pas besoin de reboot pour sortir d'une hibernation ➡️ très rapide     
  ➡️ la RAM DOIT être inférieure à 150 Gb pour que l'instance puisse passer en hibernation    
  ➡️ l'instance DOIT faire partie de ces familles: C3, C4, C5, M3, M4, M5, R3; R4, R5       
  ➡️ hibernation possible pour windows, Amazon Linux 2 AMI, Ubuntu      
  ➡️ une hibernation ne peut pas dépasser les 60 jours     
  ➡️ hibernation possible sur les instances on-demand et reserved      




**EFS**     

  **EFS**: **Elastic File System**      
  ➡️ montage NFS dans AWS     
  ➡️ peut être monté sur plusieurs instances EC2  (milliers de connexions NFS simultanées)     
  ➡️ fonctionne avec plusieurs instances EC2 dans plusieurs AZ (se situe en dehors des VPC)   
  ➡️ haute dispo et scalable (automatique ➡️ pas besoin de définir un planning de capacité)       
  ➡️ EFS est cher (on paie à l'utilisation)     
  ➡️ protocole NFSv4     
  ➡️ compatible AMI Linux (pas windows)         
  ➡️ KMS utilisé pour l'encryption     
  ➡️choix du type de performance             
    - general purpose ➡️ web servers, cms.....                
    - Max I/O ➡️ big data, media processing ....           

  *types connexions*     
  ➡️ **concurrent connections** 1000 connexions d'instances      
  ➡️ **throughput** 10Gb/s        
  ➡️ **petabytes** scale le storage en petabytes    

  
**EFSx**     
  EFS pour windows    
  ➡️ serveur windows managé avec SMB based file service   
  ➡️ pour les app windows     
  ➡️ support l'Active Directory     


**FSx pour LUSTRE**     
  file system managé et optimisé pour les workloads intensifs     
  ➡️ haute perf de calcul    
  ➡️ machine learning    
  ➡️ media data processing    
  ➡️ Intelligence Artificaielle  

    
**EBS BACKUPS**      
  permet de consolider les backups des service: EC2, EBS, EFS..... 
  ➡️ inclus dans les services types RDS     
  ➡️ peut être utilisé par Organizations pour backup des accounts    
  ➡️ centralisé dans 1 seule console quelque soit le nb d'accounts de backup     
  ➡️ automatisés ➡️ créer des cycles de vie des policies ➡️ génère la durée des backups    
  ➡️ améliore la conformité  ➡️ utilisé lors des audit              

  



=======================================================================================================================  
### <span style="color: #EA9811">**DATABASES**</span>


**DB - RDS: Relational DataBases**       
  _structure d'une DB Relationnelle_          
  - **table** ➡️ les datas sont organisées dans les tables                
  - **ligne** ➡️ item de la data                 
  - **colonne** ➡️ champ de la data dans la database                

  _fournisseurs de Db relationnelles_            
  MySQL, PostGreSQL, SQL Server, Oracle, MariaDB, **Aurora**       

  _RDS avantages_      
  quelques minutes pour la création (très rapide)     
  construites sur des instances EC2     
   ➡️ pas d'accès à l'OS, seulement la DB       
  possibilité d'en avoir dans de nombreuses Availibility zones ➡️ prémunir du failover (bascule d'1 AZ à 1 autre)      
  backup automatisé à la création       

  _RDS OLTP: OnLine Transaction Process_       
  DB utilisées pour les workload OLTP   
   ➡️ utilisées pour les transactions: rapide (en temps casi réel - ex: bancaire)

  _RedShift OLAP: OnLine Analytics Process_    
  DB est pour les workload OLAP ➡️ Redshift  ➡️ big data
   ➡️ utilisées pour les analyses: lent       


**DB - RDS Multi AZ**      

  Copie exacte d'une DB dans plusieurs AZ (donc dans plusieurs data center)     
  ➡️ replication automatique ➡️ managé par AWS

  _types de RDS Multi-AZ_    
  MySQL, MariaDB, PostgreSQL, Oracle, SQL Server  ➡️ tous ces types peuvent être standalone ou multi AZ    
  dans le cas de multi-AZ si une RDS tombe ➡️ AWS gère la bascule vers 1 autre RDS tout seul     

  Le multi-AZ est fait pour améliorer les performances, et/ou pour le DR (disaster recovery) 
  ➡️ améliorer les perf ➡️ utilisation de replicas      


**Utilisation de read replicas**        
  un replica est une copie en read-only de la DB primaire     
  ➡️ très utile dans les cas de grosses charges de requêtes
  ➡️ redirection d'une partie (ou de certaines requetes) vers le replica    
  ➡️ plus rapide    

  Un replica peut être dans plusieurs AZ et/ou plusieurs regions     
  Chaque replica a un DNS endpoint
  Il est nécessaire d'avoir un backup pour pouvoir utiliser un replica     
  ➡️ jusqu'à 5 replicas par databse-instances (RDS par exemple puisque 1 DB RDS est sur 1 instance)            


**DB AURORA**        
  DB RDS propriéatire Amazon ➡️ compatible MySQL, PostgreSQL    
  ➡️ combine la vitesse, capacité, simplicité et coûts bas de l'opensource     

  5 fois + performante que MySQL            
  3 fois + performante que PostgreSQL            
  prix plus bas pour capacités équivalentes               

  Elle prend en charge l'autoscaling ➡️ 10Gb mini ➡️ 128 Tb maxi     
  Resource compte ➡️ jusqu'à 96 vCPUs et 768Gb de RAM       

  2 copies dans chaque AZ et 3 AZ mini ➡️ 6 copies mini    
  conçue pour pouvoir perdre 2 copies sans effet de perf      
  ➡️ se scanne et se répare seule    


  _3 types de replicas Aurora_      
  - **aurora replicas** ➡️ jusqu'à 15 read replicas      
  - **mysql replicas** ➡️ jusqu'à 5 read replicas       
  - **postgreSQL replicas** ➡️ jusqu'à 5 read replicas         

  _Aurora backups_             
  les backups sont automatiques et n'affectent pas la performance      
  possible de prendre des snapshots sans impacts de perf       
  partage de snapshots avec d'autres aws accounts possible        

  _Aurora serverless_     
  ce fait à la demande


**DYNAMODB**         

  DB propriétaire Amazon NON relationnelle        
  ➡️ rapide et flexible NoSQL     
  ➡️ utilisée pour des applications qui ont des besoins conséquents ➡️ latence de moins de 10ms          

  ➡️ complètement managée, supporte les documents ou modeles clés-valeurs       

  Utilise le SSD       
  Sont dans 3 datacenters dans des zones geographiques distinctes           
  Forte cohérence de lecture        

  _read consistency_      
  2 types       
  - **eventually consistent read** ➡️ la requête DOIT retourner le resultat en moins de 1 seconde à travers toutes les copies     
  - **strong consistent read** ➡️ la requête retourne TOUTES les écritures qui ont une réponse en succès avant la lecture (moins d'1 seconde)       

  _DAX: DynamoDb Accelerator_           
  Totalement managé             
  haute capacité en cache mémoire       
  améliore 10 fois les capacity DynamoDB       
  reduit le temps des requêtes à la µs       
  compatible avec appels API dynamoDb     
  pas besoin d'un dev pour gérer le cache        

  _DynamoDB on-demand capacity_       
  prix en fonction du nombre de requêtes      
  équilibe coût / performance      
  pas besoin de minimum capacity       
  ➡️ utilisée pour les lancements de produits    

  _DynamoDB security_             
  encryption au repos ➡️ utilise KMS     
  VPN pour connexion site à site       
  Direct Connect (DX) ➡️ pour les connexions à des reseaux "on-prem"      
  IAM policies et roles pour les accès      
  intégrée dans cloudWatch et cloudtrail ➡️ monitoring       
  utilise les VPC endpoints         

  _DynamoDB ACID: Atomic Consistent Isolated Durable_            
  **Atomic** ➡️ tout ou rien ➡️ toutes les modif d'une data doivent être en succès ou aucun      
  **Consistent** ➡️ la data doit être dans 1 état consistent avant et après la transaction         
  **Isolated** ➡️ aucun autre process ne peut modifier la data au cours de la transaction         
  **Durable** ➡️ la modification générée par la transation DOIT être persistente           
  ➡️ transaction qui se fait à travers 1 ou plusieurs tables             

  _DynamoDB cas des transactions_                 
  - transactions financieres            
  - remplir et gérer les commandes           
  - construire des moteurs de jeux multi players               
  - coordonnées des actions à travers des composants et services distribués       

  _DynamoDB backups_            
  solution de backup et restore on-demand            
  full backup n'importe quand        
  pas d'impacts sur perf et disponibilité      
  consistent en quelques secondes, conservé jusqu'à suppression         
  situé dans la même region que la source         

  _DynamoDB PITR: Point In Time Recovery_            
  protège contre les écritures et suppressions accidentelles          
  restore n'importe quel point des 35 derniers jours      
  backups incrémentiels     
  non activé par defaut       
  point le plus recent restorable: il y a 5 minutes        

  _DynamoDB Flux_          
  flux FIFO      
  chaque entrée est séquencée avec un ID          
  chaque séquence est conservée dans DynamoDB pendant 24h en tant que shard (fragment)      

  _DynamoDB Global Tables_            
  gestion multi-masters et replications multi-regions      
  ➡️ nécessite des applications à l'échelle mondiale      
  ➡️ tables basées sur des flux         
  ➡️ redondance multi regions pour DR et HA       
  ➡️ aucune ré écriture d'application       
  temps de replication < 1s    


**MONGODB**           
  C'est une **document database**            
  ➡️ scalabilité et flexibilité des datas           


**CASSANDRA**       
  DB NoSQL fait pour le big data        
  ➡️ utilise **amazon keyspaces**: service de db scalable à HA ➡️ géré par Cassandra      


**NEPTUNE**          
  DB orientée graphe      
  ➡️ optimisé pour les app à faible latence et débit élevé         
  ➡️ ne paie que pour ce qui est utilisé          


**AMAZON QLDB: Quantum Ledger DataBase**           
  Db de registres avec 1 log transparent, immuable et vérifiable cryptographiquement  
  ➡️ impossible de modifier directement 1 donnée dans la Db        
  ➡️ principalement utilisé pour les crypto monnaies, compagnies pharmaceutiques            


**AMAZON TIMESTREAM**               
  DB chronologique, rapide, évolutive, serverless         
  ➡️ utilisé avec l'IoT et le DevOps       





=======================================================================================================================  
### <span style="color: #EA9811">**VPC NETWORKING**</span>


**VPC: Virtual Private Cloud**  ➡️ réseau privé (sur AWS) dans lequel nous gérons notre SI           
  Un VPC s'étend dans toutes les Availability Zones d'une region              
  Après sa création  ➡️ possibilité d'ajouter 1 ou plusieurs subnets dans chaque AZ             
  A sa création on planifie des plages d'IPs (ipv4 et ipv6) ➡️ CIDR    
  jusqu'à 5 CIDR par VPC             


![vpc-diagram-schema](./images/aws_solutionArchitectAssociate/vpc-diagram-schema.png)  

  ➡️ permet de démarer des instances (dans 1 subnet défini)         
  ➡️ possède 1 plage d'IPs personnalisées dans son subnet       
  ➡️ configure le route table entre les subnets        
  ➡️ crée une internet gateway pour la communication entre internet et le VPC    
  ➡️ utilise les ACL (Access Control Lists) pour les accès aux subnets      
  ➡️ ajoute une securité         


  Un **default VPC** peut être créé par defaut lors de la création d'une instance       
  ➡️ tous les subnets du default VPC ont un accès internet     
  ➡️ toutes les instances EC2 ont 1 IP publique et 1 IP privée

  _Creation d'un VPC_               
  Implique 3 creations       
  - **security group**            
  - **route table**           
  - **network ACL**                  


**SUBNET**              
  Un subnet est une plage d'IP dans le VPC     
  1 seule AZ pour 1 subnet     
  1 subnet peut avoir 2 caractéristiques: 
   - 1 accès internet ➡️ public    
   - 1 accès interne ➡️ privé  

  Lors de sa création 5 IPs sont réservées       
   - `10.0.0.0` ➡️ network address         
   - `10.0.0.1` ➡️ VPC router (réservé aws)      
   - `10.0.0.2` ➡️ dns (réservé aws)         
   - `10.0.0.3` ➡️ dans le cas d'installation future (réservé aws)      
   - `10.0.0.255` ➡️ network broadcast address    


**INTERNET GATEWAY**        
  Permet rendre un subnet accessible à Internet          
  1 seule Internet gateway par VPC        

  Par defaut à sa création elle est _detached_  ➡️ il faut l'_attached_ au VPC souhaité      





**ROUTE TABLE**        
  Par défaut elle est créée à la création du VPC, tous les subnets seront associés à la **main route table**     
  Pour pouvoir filtrer il créer une nouvelle route table    
   ➡️ pour donner l'accès à l'extérieur il faut renseigner dans cette nouvelle route table l'internet gateway créée     
  Pour associer un subnet à cette route table il faut l'associer dans **explicit subnet associations**      


**NAT GATEWAY: Network Address Translation Gateway**        
  Permet l'accès d'instances d'un private subnet vers internet ou d'autres services AWS tout en sécurisant les connexions de ces instances  


  Une instance d'un public subnet va passer par un **Network ACL** puis par la **route table** (sortie vers internet définie), le **router**, et enfin l'**Internet gateway** pour accéder à Internet          

  Une instance d'un private subnet qui passe par le Network ACL va être bloqué par la route table (pas de route définie vers internet)     
  ➡️ création d'un NAT Gateway dans le public subnet et créer une connexion entre l'instance du private subnet et cette NAT Gateway    

  Un NAT Gateway ne PEUT agir à travers plusieurs AZ      



![vpv_natgateway_privateSubentRouteToInternetSolution](./images/aws_solutionArchitectAssociate/vpv_natgateway_privateSubentRouteToInternetSolution.png)



  _5 faits importants sur les NAT Gateway_          
   - Ils sont redondant dans une même Availability Zone           
   - Ils commencent à 5Gbps et vont jusqu'à 45 Gbps       
   - Pas besoin de patch         
   - Ils ne sont pas associés à un security group       
   - Ils sont associés automatiquement à une adresse publique       



**SECURITY GROUP**         
  C'est la dernière ligne de défense de l'instance en sécurité reseau     
  Ce sont des firewalls virtuels pour les instances     
  Par defaut: TOUT EST BLOQUE       
  Ils sont stateful ➡️ si on envoie une requête depuis l'instance "derrière" ce security group, la reponse ne se fera que si les regles de ce dernier autorise cette requête      


**NETWORK ACL**        
  C'est la 1ere ligne de défense       
  Couche optionnelle de sécurité pour votre VPC qui agit comme un firewall pour controler le trafic entrant et sortant d'1 ou plusieurs subnets              
  ➡️ il faut le configurer avec les mêmes regles que le security group            
  1 Network ACL peut être associé avec 1 ou plusieurs subnets             
  1 subnet NE PEUT être associé qu'avec un seul Network ACL             

  _default Network ACLs_     
    par defaut à la creation d'un VPC un ACL est créé avec toutes les autorisations entrantes et sortantes      

  _custom Network ACLs_              
    par defaut à leur creation TOUT est fermé ➡️ il faut ouvrir ce qu'on souhaite

  _subnet associations_       
    chaque subnet DOIT être associé à 1 network ACL       
    si ce n'est pas fait, il sera associé par defaut au default Network ACL     

  _block IP Addresses_       
    permet de bloquer des IPs sans passer par les security groups

  Les regles dans 1 Network ACL contiennent un numéro     
  Ces regles sont exécutées par ordre croissant des numéros     
  Un network ACL est stateless ➡️ les reponses sont sujettes aux regles     


![vpc-rules-schema](./images/aws_solutionArchitectAssociate/vpc-rules-schema.png)        

**VPC ENDPOINT**         
  Ce sont des points d'entrées pour des connexions en privé pour les clients vers les services AWS (reste dans le reseau interne)          

  _2 types de endpoints_      
   - **interface endpoints** ➡️ ENI (Elastic Network Interface) avec IP privée qui donne un point d'entrée       
   - **gateway endpoints** ➡️ virtual device qui permet une connexion S3 et DynamoDB    

![vpc-endpoint-schema](./images/aws_solutionArchitectAssociate/vpc-endpoint-schema.png)          



**PEERING**      
  Utilisé dans le cas de multiple VPCs      
  ➡️ on peut avoir besoin de plusieurs VPCs pour différents usages (1 VPC pour la production + 1 pour nos contenus + 1 pour l'intranet)    
  ➡️ ces VPCs sont amenés à communiquer entre eux  ➡️ il s'agit du **vpc peering**     

  _fonctionnement VPC Peering_         
  - être autorisé à connecter 1 VPC avec 1 autre via un direct network route qui utilise des IP privées     
  - les instances doivent se comporter comme si elles étaient dans le même private network          
  - vous pouvez appairer des VPCs avec des comptes AWS ainsi qu'avec d'autres VPC dans le même compte           
  - peering (appairage) est une configuration en étoile ➡️ 1 VPC central avec 4 autres ➡️ le central communique avec tous MAIS tous NE communique qu'avec le central           
  - On peut appairer entre les regions           

![vpc-peering-configEtoile](./images/aws_solutionArchitectAssociate/vpc-peerin-configEtoile.png)                 
![vpc-peering-schema](./images/aws_solutionArchitectAssociate/vpc-peering-schema.png)          


**PRIVATE LINK**         
  Agit comme le peering mais pour de nombreux VPCs et de nombreux utilisateurs de VPCs     
  Ne nécessite pas de peering, route table, network ACL, NAT gateway, internet gateway....         
  A besoin d'un Network Load Balancer sur le service VPC et d'une ENI sur le VPC du customer     


![vpc-privateLink](./images/aws_solutionArchitectAssociate/vpc-privateLink.png)         


**CLOUDHUB**        
  Service utile dans le cas où l'on a une multitude de sites, chacun avec sa propre connexion VPN          
  ➡️ dans ce cas utiliser AWS VPN CloudHub pour connecter tous ces sites entre eux est utile         
  Utilse un **hub-and-spoke model** (modèle en étoile)                 
  Pas cher et facile à gérer          
  Fonctionne dans l'internet publique MAIS tout le trafic entre l'utilisateur gateway et AWS VPN CloudHub est encrypté     


![vpc_vpnClouHub-diagram](./images/aws_solutionArchitectAssociate/vpc_vpnClouHub-diagram.png)     


**DIRECT CONNECT**           
  Solution de service cloud qui permet d'établir facilement une connection dédiée depuis notre on-premises vers AWS     
  La connexion peut être privée          
  Cela permet une réduction des coûts de notre reseau, augmenter le débit de bande passante et offrir une expérience réseau plus cohérente que les connexions basées sur Internet         

  _2 types de connexion Direct Connect_            
  - **dedicated connection**: connexion ethernet physique associée à un client unique. Les clients peuvent effectuer des requêtes à travers la console aws direct connect, la cli ou les APIs           
  - **hosted connection**: connexion Ethernet physique qu'un partenaire aws direct connect fournit au nom d'un client. Les clients peuvent faire des requêtes en contactant le partenaire qui fourni la connexion dans aws direct connect partner program      


![vpc-directConnect-schema](./images/aws_solutionArchitectAssociate/vpc-directConnect-schema.png)          

  _Difference entre VPN et Direct Connect_        
  - Le **VPN** autorise les communications privées mais dépend du flux du reseau internet publique (peut-être lent en fonction des datas à faire transiter)                
  - **Direct Connect** est rapide (lien direct), securisé, fiable et à son propre flux           
  ⚠️ On peut associer un VPN et Direct Connect ⚠️                  


**TRANSIT GATEWAY**         
  Une archi peut devenir très vite complexe (avec le nb de VPCs qui augmente)        
  ➡️ le peering devient très complexe à mettre en place et maintenir + le VPN + direct connect ....           
  ➡️ **Transit Gateway** simplifie cela            
  Il fonctionne par region MAIS peut être accross multiple regions    
  Il est possible d'utiliser de nombreux accounts AWS avec **RAM (Resource Access Manager)**       
  Il supporte le multicast d'IPs     

  Il connecte les VPCs et les reseaux on-premises à travers un hub central.           
  Cela simplifit le reseau et permet de ne pas à avoir à ajouter des relations complex de peering.               
  Il agit comme un router cloud, chaque nouvelle connexion n'est faite qu'une seule fois                
     ➡️ 1 service connecté à Transit Gateway peut communiquer avec tous les services connectés à la même Transit Gateway       
     ➡️ possibilité d'utiliser les routes tables pour limiter les communicatsions entre VPCs connectés à Transit Gateway           

![vpc-transitGateway](./images/aws_solutionArchitectAssociate/vpc-transitGateway.png)        


**WAVELENGHT**       
  C'est le reseau 5G de AWS      
  Il est conçu pour embarquer les services compute et storage dans le reseau 5G   

![vpc-wavelenght](./images/aws_solutionArchitectAssociate/vpc-wavelenght.png)     



**VPC FLOW LOGS**               
  Les flow logs peuvent vous aider pour de nombreuses tâches, par exemple :          

  - Diagnostiquer les règles de groupe de sécurité trop restrictives          
  - Surveiller le trafic qui accède à votre instance         
  - Déterminer la direction du trafic vers et depuis les interfaces réseau   

  Les données du flow logs sont collectées en dehors du chemin d'accès de votre trafic réseau et n'affectent donc pas le débit réseau ou la latence. Vous pouvez créer ou supprimer des flow logs sans risque d'impact sur les performances du réseau. 


  Un flow log capture tout le trafic d'un sous-réseau et publie les enregistrements du flow log sur Amazon CloudWatch Logs, S3, Kinesis firehose. Le flow log capture le trafic pour toutes les interfaces réseau du sous-réseau

=======================================================================================================================  
### <span style="color: #EA9811">**ROUTE53**</span>

Amazon Route 53 est un service Web DNS (Domain Name System) hautement disponible et évolutif          

Les IPv4 ont 32-bit  ➡️ 4 294 967 296 adresses possibles        
Les IPV6 ont 128-bit ➡️ 340 undecillion (340 milliards de milliards de milliards) d'adresses disponibles          

**4 top domaines**                   
  - **.gov**          
  - **.gov.uk**            
  - **.com.au**          
  - **.edu**      
  ➡️ ce sont les 4 top domaines dans le monde            
  Ils sont controllés par l'**IANA (Internet Assignated Numbers Authority)**            
  ➡️ tous les domaines sont uniques et sont enregistrés ici: https://www.iana.org/domains/root/db       

  Un **registar** est une autorité qui permet d'enregistrer un domaine sous un top-level domain    
  - **domain.com**          
  - **GoDaddy**         
  - **Hover**        
  - **AWS**         
  - **Namecheap**         


**DNS SOA: DNS Start Of Authority**                
  Un enregistrement DNS stocke les informations suivantes:           
  - administrateur de la zone         
  - version des fichiers de données        
  - nb secondes du TTL           
  - nom du serveur qui fourni les data dans la zone          


**Termes DNS**           
  - **TTL: Time To Live** 
   ➡️ temps en secondes qu'1 enregistrement restera en cache (sur PC)           
   ➡️ plus il est petit plus la propagation en cas de changement sera rapide à travers internet       

  - **Enregistrement DNS**         
   ➡️ **Enregistrement A**: enregistrement fondamental du DNS ➡️**A** pour **Address**    ➡️ traduit le nom en IP        
   ➡️ **NS Record**: enregistrement Name Serveur ➡️ où ils sont enregistrés      

  - **CNAME**       
   ➡️ Canonical NAME   ➡️ utilisé pour résoudre un domaine        

  - **Alias**       
   ➡️ utilisés pour mapper les enregistrements de ressources configurés dans les **hosted zones**         
   ➡️ il fonctionnent avec le CNAME        


**Route53**        
  ➡️ il permet la creation de domain names, hosted zones, manager et créer les enregistrements DNS     
  ➡️ il utilise le port 53      

  _7 routing policies_         
   - **Simple Routing**           
    ➡️ 1 seul enregistrement de domaine avec plusieurs IP      
    ➡️ Route53 va toutes les renvoyer à l'utilisateur de manière aléatoire     

   - **Weighted Routing**            
    ➡️ permet de splitter le trafic en se basant sur différents poids       

   - **Failover Routing**                
    ➡️ utilisée lorsqu'on souhaite créer une configuration active/passive (ex: site primaire pour activités + site secondaire pour DIsaster Recovery)              

   - **Geolocation Routing**          
    ➡️ choisir où le trafic va se faire en fonction de la geolocalisation du user        
    ➡️ 1 regle par IP à mettre en place             

   - **Geoproximity Routing**       
    ➡️ utilisation de **Route53 Traffic Flow** pour construire un systeme de routage qui utilise une combinaison de: localisation geographique + latence + disponibilité du trafic         

  - **Latency Routing Policy**           
    ➡️ pour router le trafic vers celui qui la la plus faible latence pour le user final    

   - **Multivalue Answer Routing**         
      ➡️ nous laisse configurer Amazon Route53 pour renvoyer plusieurs values comme les IP adresses des web servers en réponse aux requêtes DNS       


**HOSTED ZONES**       
  _Private Hosted Zones_           
  ➡️ conteneur qui contient les infos sur la manière dont vous souhaitez qu'Amazon Route53 réponde aux requêtes DNS pour 1 domaine et ses sous-domaines dans 1 ou plusieurs VPC que vous créés (service VPC)         


  _Public Hosted Zones_     
  ➡️ conteneur qui contient les infos sur la manière dont vous souhaitez acheminer le trafic sur internet pour 1 domaine spécifique     


**ROUTE53 QUOTAS**     
  Les requêtes et les entités de l'API Amazon Route 53 sont soumises à des quotas: [Documentation aws: service quotas](https://docs.aws.amazon.com/servicequotas/latest/userguide/intro.html)       


**ROUTE53 HEALTHCHECKS**    
  Amazon Route 53 health checks surveillent l'état et les performances de vos applications Web, serveurs Web et autres ressources     

  Il est possible de configurer les healthchecks sur l'état des ensembles de record sets individuels       
  ➡️ Si un record set echoue lors d'1 healthcheck ➡️ il sera supprimé de Route53 jusqu'à ce qu'il le réussisse à nouveau     
  ➡️ une notification SNS eput être mise en place pour alerter d'1 healthcheck en failed      


**ROUTE53 - REGISTER A DOMAIN NAME**       
  Possibilité d'acheter un nom de domaine depuis AWS ➡️ cela prend 3 jours en moyenne      
  ➡️ pour ensuite le visulaiser aller dans `Route53/Hosted Zones`     



=======================================================================================================================  
### <span style="color: #EA9811">**ELB**</span>


**Rappel couches OSI**      
  ![modeleOSI](./images/aws_solutionArchitectAssociate/modeleOSI.png)    

**ELB** : **Elastic Load Balancer**     
  ELB distribut automatiquement les flux entrants des applications à travers de multiples cibles et appareils virtuels dans 1 ou plusieurs Avalabilty Zones (AZs)          

  3 types d'ELB: 
  - **Application Load Balancer** ➡️ HTTP(S) ➡️ couche 7 OSI - application) ➡️ **Intelligent Load balancer**    
  - **Network Load Balancer** ➡️ couhe 4 - transport ➡️ millions de requetes par secondes avec faible latence ➡️ **Performance Load balancer**    
  - **Classic Load balancer** ➡️ historique ➡️ couche 7 ➡️ fonctionnalités comme _X-forwarded_ et _sticky sessions_    


  Tous les LB peuvent être configurés avec un **healthcheck**    
  1 instance "derrère"" un LB est requêtée régulièrement et lui envoie son status  ➡️ si **up** ➡️ des requêtes lui sont envoyées    


**ALB: Application Load Balancer**        

  Un **listener** vérifie les requêtes de connexion des clients en utilisant les protocoles et ports définis dans ses regles       
  un **listener** peut avoir de multiples regles           
  Une **regle** définie comment le LB va router les requêtes vers les cibles (un groupe de cibles pour la disponibilité) ➡️ chaque cible donne son status via le healthcheck        
  Chaque regle correspond à 1 priorité, 1 ou plusieurs actions et 1 ou plusieurs conditions     
  Lorsque les conditions d'1 regle sont atteintes ➡️ les actions de la regles sont réalisées      
  Chaque listener DOIT avoir une regle par defaut défini, puis il est possible d'ajouter de nouvelles regles       

  Un **target group** redirige les requêtes vers 1 ou plusieurs cibles enregistrées      


  _PATH-BASED ROUTING_             
  1 LB dans 1 AZ doit rediriger les requêtes vers les instances dans certains cas ET vers d'autres instances (qui peuvent être dans 1 autre AZ) dans d'autres cas ➡️ cela dépend de l'url renseignée ➡️ des regles de redirections sont mises en place en fonction des URLs     

  **1 LB ne supporte que le HTTP et le HTTPS**               


  _HTTPS LOAD BALANCING_        
  Pour utiliser un listener HTTPS, il est OBLIGATOIRE de déployer au moins 1 certificat serveeur SSL/TLS sur le load balancer        
  Le load balancer utilise un certificat serveur pour mettre fin à la connexion frontend et décrypter les requêtes des clients avant de les envoyer vers les cibles ➡️ la décryption est réalisée sur le load balancer        


**NLB: NETWORK LOAD BALANCER**          

  Ce type de LB est utilisé pour la gestion de "millions" de requêtes par seconde ➡️ extrême performance              
  Il agit au niveau de la couche 4 du modele OSI    
  1 fois 1 requête de connexion reçue, il sélectionne une cible dans le target group ➡️ ouverture connexion TCP vers la cible choisie     

  1 listener dans un NLB renvoie les requêtes vers 1 target group        
  **1 NLB n'a pas de rules comme 1 ALB**               

  _Ports et protocoles utilisés_           
  - **protocoles**: TCP, TLS, UDP, TCP_UDP        
  - **ports**: 1 ➡️ 65535        

  Possibilité d'utiliser 1 TLS listener pour décharger l'encryption/decryption des LB ➡️ les applications ne font que leur travail     
  

**CLASSIC LOAD BALANCER**           
  Ce sont les LB "historiques"               
  Ils agissent sur la couche 7 (ajout de fonctionnalités) ou 4 sur le protocole TCP           
  On peut leur ajouter des fonctionnalités comme le _X-forwarded-for_ ou les _sticky sessions_        

  _Sticky session_         
  1 classic LB route chaque requête indépendamment vers l'instance enregistrée       
  1 **sticky session** autorise le lien entre un _user session_ et 1 instance définie    
   ➡️ pb potentiel lorsque l'instance tombe ➡️ requêtes KO       
   Seule solution pour régler le pb: **disable sticky session**           


**DEREGISTRATION DELAY**             
  Le **deregistration delay** (ou **connection draining**) permet aux LB de conserver une connexion ouverte si les instances EC2 sont "dé enregistrées" puisque le healthcheck est unhealthy ➡️ les requêtes en cours peuvent se terminer        

  Possibilité de disable cela si l'on ne souhaite pas conserver d'instances unhealthy        



=======================================================================================================================  
### <span style="color: #EA9811">**MONITORING**</span>


**AMAZON CLOUDWATCH**        
 Solution de surveillance fiable, évolutive et flexible que vous pouvez commencer à utiliser en quelques minutes. Vous n'avez plus besoin de configurer, de gérer et de faire évoluer vos propres systèmes et infrastructures de surveillance

  Cloudwatch fonctionne avec les accès IAM, reçoit et fournit les métriques des instances, crée des alarmes        
  Cloudwatch collecte les metriques systeme + applicatives et crée des alarmes (par defaut: aucune alarme, il faut les créer)    

  Il existe 2 types de metriques:    
  - **default**: CPU, I/O               ➡️ aucune installation requis             
  - **custom**: Ram, FS utilisation     ➡️ installation de l'agent sur le host                    

  _Installation de l'agent cloudwatch_      
  - `sudo yum install amazon-cloudwatch-agent`    ➡️ installation de l'agent                 
  - `sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-config-wizard`  ➡️ generation du fichier de supervision custom     
  - `sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:sudo /opt/aws/amazon-cloudwatch-agent/bin/config.json`   ➡️ load et run de l'agent avec sa conf custom        
  

**CLOUDWATCH LOGS**       
  Outil qui permet de monitorer, conserver et accéder aux logs de différentes sources.        

  Il existe 3 types de logs:    
  - **log event**: enregistrement de ce qu'il s'est passé. Il contient le timestamp et la data     
  - **log stream**: collection de log event depuis 1 unique source (instance)     
  - **log group**: collection de log stream depuis 1 seule localisation    


  On peut definir des patterns sur les types de logs, les récupérer en 1 fois et en sortir une tendance (requete SQL vers la data base qui conserve ces logs), créer des alarmes     

  
  Il fonctionne avec un agent installé sur chaque instance qui renvoie les datas toutes les 5 minutes         


**PROMETHEUS - GRAFANA**     

  _Amazon Managed Grafana_                
  Amazon Managed Grafana est entièrement géré par Amazon (MCO et MCS)     
  Il est  sécurisé, évolutif et hautement disponible        
  Il est possible d'avoir des workspaces (serveurs logiques grafana) qui permettent une séparation des données et des requêtes     
  Son prix est définit par nombre d'utilisateurs actifs dans 1 workspace           
  Il est directement intégré avec les data sources cloudwatch, prometheus, opensearch, timestream d'amazon      

  Ses use cases sont:       
  - **container metric visualizations**: connecté aux data source comme prometheus pour la visualisation EKS, ECS ou connaitre les metrqiues de votre propre cluster kubernetes        
  - **IoT**: grande possibilité de plugins pour une connexion permettant le monitoring IoT et du devic                
  - **troubleshooting**: la centralisation de dashboards permet une plus grande efficacité dans le recherche et la résolution de problème          


  _Managed Service Prometheus_            
  Service (serverless) de surveillance entièrement géré, compatible avec Prometheus, qui facilite la surveillance sécurisée et à grande échelle des applications conteneurisées         
  APIs compatibles avec prometheus ➡️ donne accès à distance à des metriques à l'aide de _promQL_           

  Evolution automatique en fonction du besoin pour l'ingestion de requêtes        
  Utilise AWS IAM pour l'authentification et les autorisations        




=======================================================================================================================  
### <span style="color: #EA9811">**HIGH AVAILABILITY AND SCALING**</span>


La plupart des providers cloud propose d'avoir une SLA de 99,99% à 99,99999% sur les services     
Pour cela AWS recommande de suivre ses directives pour atteindre un HA robuste     
- **conception (design)**: conception du système pour qu'il n'ait aucun **point de défaillance unique (SPOF: Single Spot Of Failure)** ➡️ utilisation de l'automation de surveillance, détection des pannes, basculement des composants sans état avec état    
  - Les SPOF sont éliminés avec des redondances dans la configuration: N+1 ou 2N (lorsque N+1 est atteint avec l'équilibrage de charge entre les noeuds actif-actif). 2N est atteint par 1 paire de noeuds actif-veille (active standard pair)      
  - AWS propose 2 solutions pou atteindre la HA: **cluster (scalable: évolutif)** avec load balancer ou une **active standard pair**    

- Disponibilité correcte des élément et système de tests      

- Préparation des procédures d'exploitation pour les mécanismes manuels afin de répondre, d'atténuer et de se remettre de la panne        



**HA HORIZONTAL VS VERTICAL**                 
  _HA vertical_     
   c'est le "scaling" historique ➡️ augmentation jusqu'à 1 max de capacité (CPU, RAM....)     

  _HA horizontal_      
   augmentation du nb d'instances (pas de limite)     
   cela permet aussi d'augmenter la disponibilité (ajout d'instances dans plusieurs AZs)    

  _3 questions importantes à se poser sur le scaling_      
   1. **Quel scale faire ?**     
    ➡️ décision des ressources à augmenter        
    ➡️ comment définir/utiliser un template                 
   2. **Où faire le scale ?**             
    ➡️ quand et où appliquer un template       
    ➡️ que doit-on faire évoluer ? (databases, webservers....)                                    
   3. **Quand faire le scale ?**                      
    ➡️ comment savoir quand il faut plus de ressources       
    ➡️ cloudwatch (alarmes) peuvent nous informer qu'il est temps d'augmenter les ressources                            


**LAUNCH TEMPLATE ET LAUNCH CONFIGURATION**            
  **La différence entre ces 2 sujets est expliquer avec la 1ere question: quel scale faire ?**        

  _Launch template_          
   Un **Launch Template** défini tous les besoins de paramétrage lors de la construction d'une instance            
   Cette collection de paramètres permettent la configuration des instances EC2           
   Précaunisé par AWS      
   Supporte le versioning     
   Plus de granularité      
   Permet plus d'évolutions que l'autoscaling          

   > Lors de la création d'un Launch Template NE PAS SPECIFIER de VPC ➡️ cela est dépendant de l'instance que nous souhaiterons créer          
   > La partie **user-data** correspond à la partie où nous pouvons mettre un place le bootstrap script d'une instance        
   > Un **launch template** peut comprendre les AMIs, taille d'instances EC2, security groups, network informations (potentiellement)     
   > **NE PAS** configurer de network dans 1 template sinon le scaling n'est plus possible     

  _Launch configuration_   
   Pratique "historique"             
   Ne permet que l'autoscaling         
   Immutable (sa nature ne peut changer)           
   Configuration des options limitée       
   Non recommandé par AWS          


**AUTOSCALING**        
  **Cette partie répond à la 2e question: Où faire le scale ?**            
  Les **Auto scalling groups** répondent à cette question         
  Un auto scaling group contient une collection d'EC2 instances qui sont considérées en un group collectif dans le but du scaling et du management         
  C'est avec l'auto scaling group que le network va être défini            

  _Etapes auto scaling_        
   1. **Définir un template**     
    ➡️ l'autoscaling se base sur les templates ➡️ avoir 1 template de défini pour pouvoir l'utiliser       

   2. **Définir un reseau**        
    ➡️ le network se définit ici (réponse à la 2e question: où faire le scale ?)        
    ➡️ utilisation de multiples AZs pour une HA         

   3. **Configuration ELB**          
    ➡️ mise en place du Load Balancing et des Healthchecks (primordiale)  ➡️ elle va permettre de supprimer et reconstruire les instances     
    ➡️ les instances sont créées "dérrière" le load balancer ➡️ l'auto scaling group va être définit par le respect du load balancer et des healthchecks                   

   4. **Définir les règles de scaling**        
    ➡️ définit le **minimum**, **maximum** et **souhaité** du nombre d'instances pour répondre aux besoins du service     

   5. **Notifications**     
    ➡️ envoie de notifications (SNS) pour informer le besoin d'augmenter ou réduire le nb de ressources       
    ➡️ cette action peut être manuelle ou automatisée     


  _Définition des limites de capacités_                                      
   Cela permet de ne pas configurer "trop" de ressources      
   - **minimum** : Le nb d'instances minimale online (en général 2 / si 0 et qu'il n'y a pas d'activité : instances détruites ➡️ le service ne pourra plus fonctionner )         
   - **maximum** : Le nb d'instances au max (on ira jamais plus haut). En général mettre 1 peu plus que nécessaire (c'est un plafond)     
   - **souhaitée** : Ce nb DOIT être entre le max et le min. ⚠️ Si nb trop élévé, la facturation va augmenter considérablement ⚠️       

   > Il est possible d'utiliser des spots instances avec l'autoscaling ce qui permet de diminuer la facturation       
   > Le load balancing peut se faire à travers plusieurs AZs     
   > Aucun autre service que EC2 utilise l'autoscaling. Certains utilisent des options de built-in     
   > Avoir des AMIs complètement configurées permet d'avoir des instances disponibles plus rapidement


**AUTO SCALING POLICIES**                
  **C'est la réponse à la 3e question: quand faire le scale ?**        

  On définit des regles à partir du monitoring des instances (ex: RAM, CPU, I/O, ....)     
  Il y a au moins une regle pour chaque de **scale-in** et de **scale-out**           

  _Scale-in_        
   C'est un % "bas" qui définit si on supprime des instances      
   _exemple_: entre 10 et 20 % de RAM utilisée ➡️ maximum 5 instances      
    ➡️ cela veut dire que si le % de RAM est en-dessous de 20% on n'aura pas plus de 5 instances pour rendre le service     

  _Scale-out_           
   C'est le % "haut" qui définit si on ajoute des instances      
   _exemple_: entre 70 à 80% de RAM ➡️ ajout de 10 instances    
    ➡️ cela veut dire que si on atteint les 70% de RAM utilisée on ajoute 10 instances pour que ce % puisse être réparti et diminué    


  _Check-in: 2e étape_      
   On a vu qu'on peut avoir besoin d'ajouter des instances, MAIS, cela n'est pas instantanné. Il y a une période de "warm-up"      
    ➡️ c'est la 2e étape      

  _Check-in: 3e étape_           
   Dans le cas où nous sommes toujours dans la période de "warm-up" et que nous avons une nouvelle alerte demandant l'ajout d'instances ➡️ cela ne va pas être cumulatif, les instances sont déjà en cours de création: pas d'instances supplémentaires demandées      

  _Check-in: 4e étape_            
   Malgré le warm-up nous avons franchi un nouveau seuil de **scale-out** ➡️ "commande" du nombre d'instances supplémentaires pour atteindre le nb d'instances du nouveau seuil (en tenant compte de celles commandées lors du seuil précédent)     
   _exemple_: nouveau seuil: utilisation de la RAM 80 à 90 %: ajout de 15 instances    
    ➡️ 10 avait déjà été "commandée" ➡️ 5 supplémentaires le sont     
    Cela se fait jusqu'à atteindre le maximum d'instances définit            

  _Check-in: 5e étape_             
   La période de warm-up est terminée, les instances sont démarées, on est revenu dans la zone "souhaitée" d'utilisation de RAM (entre 20 et 70% par rapport aux regles précédemment définies)  

  _Check-in: 6e étape_          
   Le fort afflux de requêtes est terminé ➡️ le % de RAM utilisé arrive dans la regle de **scale-in** ➡️ on supprime des instances pour revenu dans les % souhaités.    
   Cela se fait tant qu'on n'est pas dans le % souhaité et jusqu'à atteindre le minimum d'instances définit pour rendre le service         
    
  _Warm-up_      
   Période "longue" durant laquelle les instances sont créées. Le load balancing et healthcheck ne sont pas activés tant que nous sommes dans cette période de warm-up       

  _Cool down_    
   Période inverse du warm-up. C'est la période (instantannée) où l'on supprime les instances exédentaires           
   Elle met en pause l'autoscaling pendant 1 période définie (par defaut 5 minutes)     
   Elle aide à éviter les autoscaling incontrolables    


**3 TYPES DE SCALING AWS**            
  - **reactive scaling** : c'est l'autoscaling    
   ➡️ tout se passe grace aux policies               
   ➡️ pas besoin de connaissance des actions              

  - **scheduled scaling** : utilisé pour des périodes définies comme le blackfriday par exemple     

  - **predictive scaling** : le machine learning utilise l'historique du service pour prendre en compte les besoins et les optimiser. Cela se fait toutes les 24 heures        


**SCALING RDS**           
  Il existe 4 manières différentes pour la mise en place du scaling sur les RDS (DB relationnelles)      

  _Vertical scaling_                       
   Redéfinir la taille des ressources d'une RDS pour qu'elle fonctionne "correctement"      
   _exemple_: manque de CPU ➡️ redimentionner la DB en en créant une nouvelle plus performante     

  _Scaling storage_                
   Redimentionner la capacité d'espace de stockage des data.      
   Cela peut être fait pour être aggrandi (facile: une case à cocher) mais pas diminué       
   Si trop d'espace définit pour "rien" ➡️ augmentation de la facturation sans raison     

  _Read replicas_                
   Création de copies en read-only pour que les workloads puissent être réparties      
   Solution TRES IMPORTANTES des RDS      
   ➡️ on peut créer jusqu'à 50 read replicas par RDS    
   ➡️ cela permet de segmenter les requêtes pour les rediriger vers certains read replicas ➡️ gain de ressources    

  _Aurora serverless_             
   Aurora permet d'automatiser le processus de surveillance des workloads pour l'ajustement des capacités de la DB     
   Cet ajustement se fait 10 Gb par 10 Gb      
   Il n'est pas facturé si non effectué (respect du budget)         
   Il est utilisé pour les workloads non prévus     


**SCALING NON RELATIONNAL DB: DYNAMODB**                         
  DynamoDB est fully managed par AWS ➡️ très facile à utiliser     
  Le scaling est aussi managé par AWS     
  Il existe 2 types de DynamoDB:    
  - **provisioned** :     
   ➡️ solution parfaite lorsqu'on connait avec précision les besoins
   ➡️ solution chère    

  - **on-demand** :     
   ➡️ solution parfaite lorsqu'on en connait pas les besoins     
   ➡️ on paie par lecture ou écriture : si grosse charge peu rentable      

  Il est possible de modifier son choix MAIS seulement 1 fois par 24 heures







=======================================================================================================================  
### <span style="color: #EA9811">**DECOUPLING WORKFLOWS**</span>

Le but est de découpler les flux entre users/frontend/backend pour toujours avoir une dispo même en cas de perte de ressources  ➡️ **mise en place d'ELB / SQS**  - JAMAIS de communication directe       

Outils aidant à cette mise en place: 
 - **SQS** (Simple Queue Service) : service managé de file d'attente de messages permettant de découpler et mettre à l'échelle les microservices et les appli severless           
 - **SNS** (Simple Notification Service) : service managé de file d'attente de messages pour les communications entre appli ou vers humains (sms/mails)          
 - **API Gateway** : service managé pour la création, publication, maintenance, monitoring et sécurisation des APIs     


**SQS**   

  Amazon SQS propose des concepts courants tels que des files d'attente contenant des lettres mortes et des balises de répartition des coûts     
  Il fournit une API de services Web       
  File d'attente de messagerie qui permet le **traitement asynchrone**           

  _SQS - Poll-based messaging_               
   Une instance crée et envoie un message à une autre instance qui réceptionne ce message et le traite      

  _SQS - Important settings_           
   Configuration du service à faire pour son bon fonctionnement      
    - **Delivery delay** ➡️ par defaut 0 ➡️ peut-être jusqu'à 15 minutes       
    - **Message size** ➡️ jusqu'à 256kb dans TOUS les formats           
    - **Encryption** ➡️ messages encryptés par defaut lors du transit      
    - **Message retention** ➡️ defaut 4 jours ➡️ entre 1 minute et 14 jours        
    - **Long vs Short**: les _polling_ demande si les messages dans le _queue_ peuvent être longs ou courts     
       ➡️ conseil: faire des **longs**: 
        - les courts se connectent, appel API, si rien se déconnectent et recommencent régulièrement           
        - un long polling fait un appel API et va attendre que l'appele arrive (ce n'est pas par defaut...)        

  ⚠️⚠️             
  _SQS - Visibility timeout_        
   1. le user va envoyer 1 ordre depuis l'instance frontend           
   2. l'instance backend va chercher régulièrement sur le serveur SQS si 1 ordre est présent   
       si oui il le récupère       
   3. le message est locké pendant 30s (c'est la **visibilty timeout**)      
       message dans la queue mais personne ne le voit     
       c'est pour valider que le message est proprement récupérer (pour ne pas lancer de mauvais traitement)     
       le _lock_ est mis en place pendant cette période     
       mis en place dans le cas où 1 instance est offline ➡️ le message ne serait pas traité, il réapparaitrait alors dans le queue    
   4. le message est correctement traité ➡️ il est supprimé de la queue      

  ⚠️⚠️                


  _Sideling messages with dead-letter queues_           
   Amazon SQS prend en charge les **files d'attente de lettres mortes (DLQ)**, que d'autres files d'attente (files d'attente sources) peuvent cibler pour les messages qui ne peuvent pas être traités (consommés) correctement         
   **Utiles pour le debug**: elles permettent d'isoler les messages non utilisés afin de déterminer pourquoi leur traitement échoue       

   Un **dead-letter queue** est un message "mal renseigné" avec des informations non vérifiées lors de la construction du message     
   ➡️ dans ce cas si pas de traitement spécifique, le message serait récupéré par le backend, le traitement serait en failed, le message remis dans la queue suivi d'1 nouveau traitement en erreur ➡️ perte du message et de sa donnée    
   ➡️ pour ne pas perdre la donnée:     
    lorsque le traitement génère une erreur, le message n'est pas renvoyée dans le queue MAIS dans le DLQ     

   Pour créer une DLQ il faut avoir créé précédemment une _queue_    
   Ensuite il faut cocher une case (_enable_) pour activer la DLQ     
   Indiquer l'arn de la queue précédemment créée     
   Définir le nb d'erreurs avec le message pour qu'il parte vers la DLS (**Maximum receives**: par defaut 10)        


  _Ordered messages with SQS FIFO_                          
   Les files d'attente FIFO (1st In, 1st Out) sont conçues pour améliorer la messagerie entre les appli lorsque l'ordre des opérations et des évènements est critique (ou lorsque les doublons ne sont pas tolérés)             

   Dans 1 cas de file d'attente "standard" lorsque de traietement est "aléatoire/désordonné" et des doublons peuvent être présents     
   Dans 1 cas de file d'attente FIFO (banque par exemple: pas tous les débits PUIS les entrées ➡️ cela peut engendrer jusqu'à des aggios...) tout est "structuré"     

   Une file FIFO peut traiter jusqu'à 300 messages par seconde   


**SNS**         

 Amazon **Simple Notification Service (SNS)** envoie des notifications de 2 manières : **A2A** et **A2P**.     
  ➡️ A2A fournit une messagerie à haut débit, basée sur un système « push », entre des systèmes distribués, des microservices et des applications sans serveur pilotées par les événements          
  SNS est utiliser pour l'alerting         

 _SNS - Pull-based messaging_                              
  Dans ce cas **le destinataire DOIT être prêt à réceptionner à tout moment**     
  Les messages sont délivrés proactivement à des endpoints auxquels on a souscrit (cas alarmes)          

 _SNS - Important Settings_                         
  - Création d'un topic SNS       
  - Définition de la cible pour la réception des données de ces topics (qui, quand, où vont ces messages ?)      
      Kinesis Data Firehose, SQS, Lambda, email, HTTP(s), sms, platform app endpoint     

  - Un message pour aller jusqu'à 256kb (tous formats)    
  - Cas d'erreur: le message part dans DLQ ➡️ SNS peut générer une alerte       
  - FIFO es très rarement utilisé avec SNS mais cela est possible    
  - L'encryption en transit est mise en place par defaut mais il est possible de l'ajouter au repos     
  - Une policy peut être ajoutée à un topic    
  - Associer SNS et SQS permet de faire des envoie de messages intéressant (ex: les grouper par groupe)    
  - CloudWatch fonctionne parfaitement avec SNS    
  - Pour que le service fonctionne TOUS les endpoints doivent être "up" au moment de l'envoie des messages    
  - pour le mailing NE PAS utiliser SNS mais le service SES     


**API GATEWAY** 
 Les API gateway sont la porte d'entrée vers votre application         

 API Gateway crée des API RESTful qui :
  - reposent sur le protocole HTTP           
  - permettent la communication client-serveur sans état          
  - mettent en œuvre les méthodes HTTP standard, telles que **GET**, **POST**, **PUT**, **PATCH** et **DELETE**         

 API Gateway crée des API WebSocket qui :
  - respectent le protocole WebSocket ➡️ permet une communication avec état en duplex intégral entre le client et le serveur          
  - acheminent les messages entrants reposant sur le contenu du message       

 _Fonctionnement API Gateway_         
  - **security**: Le **WAF** (Web Application Firewall) est le 1er rempart de l'application            
     ➡️ ajout de règles de sécurité pour filtrer les appels entrants        

  - **stop abuse**: Les utilisateurs peuvent facilement implémenter des protection DDoS                  

  - **ease of use**: l'utilisation de l'application est faite par l'utilisation d'APIs    
     ➡️ beaucoup plus simple de développer une API plutôt qu'une application   

  - **versioning**: le versionning d'API est très simple pour l'utilisation        

  - **credential**: les credentials sont stockés dans un coffre-fort ➡️ les APIs vont les chercher dans ces coffres pour les connexions    


**BATCH**                        
 Un batch aws est un service qui permet d'exécuter des batch computing workloads dans les environnements AWS (sur EC2, ECS/Fargate)     

  Un batch supprime les configurations et management lourds des infra nécessaire pour la capacité de calculs       
  Un batch permet un scale automatique en fonction du nombre de jobs en cours (plus précis dans la définition des ressources) ➡️ permet une optimisation de la distribution des workloads           
  Un batch ne nécessite pas d'installation        
  Un batch est utilisé pour les **long-running and event-driven workloads**: tout ce qui dépasse 15 minutes de traitement    


 _ECS: Elastic Container Service_            
  Exécutez des conteneurs hautement sécurisés, fiables et évolutifs        

 _Fargate_                 
  Calcul sans serveur pour conteneurs           
        
  
 _Batch - Elements_
  - **jobs**:  ce sont des sccripts exécutables ou des images docker soumis à AWS batch              
  - **job definition**: définir comment vos jobs doivent être exécutés (ex: blue print est un outil de conception de plans détaillés qui aide à optimiser le process)           
  - **job queue**: les jobs doivent être soumis à des queues spécifiques et y rester jusqu'à son exécution planifiée dans un environnement défini       
  - **compute environment**: mise en place de ressouces de calcul managées ou non pour l'exécution des jobs                        

 _aws batch - fargate - ec2 environment_                      
  La recommandation AWS est: utiliser Fargate dans la plupart des cas     
   ➡️ parce qu'il démarre et se scale tout seul pour être au plus près du besoin    
   ➡️ on ne paie pas de ressources non nécessaires


  EC2 peut être un meilleur choix:     
   ➡️ custom AMI (peut faire le run seulement sur EC2)     
   ➡️ pour 1 besoin de + de 4 vCPU ➡️ pas d'autres solutions que EC2         
   ➡️ pour 1 besoin de + 30 Gb RAM ➡️ pas d'autres solution que EC2    
   ➡️ si besoin d'1 GPU ➡️ EC2 seulement      
   ➡️ si utilisation de paramètres linux ➡️ EC2 seulement     
   ➡️ en cas de nb de jobs élevés ➡️ mieux d'utiliser EC2        

 _AWS Bacth ou AWS Lambda_      
  Choix entre les 2 services:      
  - **limite de temps**: Lambda a 1 exécution qui ne peut dépasser 15 minutes       
  - **espace disque**: 
    - lambda limite à 75Go             
    - pour 1 extension EFS des fonctions dans 1 VPC sont nécessaires (difficiles à installer)      
  - Lambda est serverless MAIS a des limites de temps        
  - AWS Batch utilise docker ➡️ n'importe quel runtime peut être utilisé           

 _AWS Batch - managed vs unmanaged compute environments_                  
  **AWS Bacth managed**        
    ➡️ capacités et types d'instances gérées           
    ➡️ specs des resources compute sont définies à la creation de l'environnement      
    ➡️ les instances EC2 sont lancées dans des subnets VPC (il faut 1 accès réseau entre les instances EC2 et le service ECS)     
    ➡️ possibilité d'**utiliser sa propre AMI** (dans ce cas il faut qu'elle valide les recommandantions du service ECS) / l'**AMI** par defaut est **la plus récente**           
    ➡️ optimise les ustilsations fargate, Fargate spot et spot instances 

  **AWS Batch Unmanaged**             
    ➡️ vous gérez entièrement vos propres ressources              
    ➡️ les AMIs utilisées doivent respecter les spec amazon AMI       
    ➡️ vous gérez TOUT       
    ➡️ peu utilisé par rapport au travail de son administration         
    ➡️ très bon choix pour les besoins très complexes ou spécifiques     


**BROKERING MESSAGES WITH MQ**         
 Amazon MQ est un service géré pour des agents de messages opensource (apache activeMQ, rabbitMQ par ex)     
 Il permet une migration plus facile grâce à des endpoints mis à jour et à des protocoles et API conformes aux normes de l'industrie     
 Il facilite l'administration, les maintenances et la gestion de sécurité pour les agents de messages     
 Il assure la haute dispo des applications et la durabilité des messages à travers les zones de dispo AWS      
 Les agents de messages (messages brokers) sont des modules intermédiaires qui permettent la communication entre différents éléments informatiques (applications, systèmes, services, ...)  ➡️ ils convertissent le protocole d'un message             
 Un agent de messages peut valider, stocker, acheminer et délivrer des messages aux destinataires appropriés            
 Le message broker assure la gestion de l'état et le suivi des clients, de sorte que les applications individuelles n'ont pas à assumer cette responsabilité et que la complexité de la livraison des messages est intégrée au message broker lui-même     


 _SNS/SQS vs MQ_              
  Amazon MQ n'a PAS d'intégration AWS par defaut          
  SNS couplé à SQS et MQ proposent le même niveau de service        
  Dans le cas de nouvelles app ➡️ + facile d'utiliser SNS/SQS       
  Dans le cas d'avoir déjà un message broker sur une application ➡️ MQ facilite la migration vers le cloud       
  MQ nécessite un réseau privé (tel qu'un VPC, Direct connect ou VPN)        
  SNS/SQS ont une accessibilité publique par defaut      

 _AWS MQ Brokers_               
  **Configuration de broker**           
   1. single-instance broker ➡️ 1 broker est dans 1 AZ: parfait pour le dev et moins cher - RabbitMQ possède un Network Load Balancer en front      
   2. Highly available ➡️ MQ offre une archi HA ce qui minimise les coupures lors des maintenances. L'archi dépend du type du moteur du broker     
   3. amazon MQ for ActiveMQ ➡️ possède 2 instances (actve/standby) 1 disponible à chaque instant + 1 disponible au besoin     
   4. amazon MQ for RabbitMQ ➡️ Les clusters sont des groupes logiques de 3 noeuds de broker à travers les AZs derrière un Network Load Balancer    


**AWS STEP FUNCTIONS**                       
 AWS Step Functions est un service de flux visuels qui permet aux développeurs qui utilisent les services AWS de créer des applications distribuées, d'automatiser les processus, d'orchestrer des microservices et de créer des pipelines de données et de machine learning (ML)         

 Plusieurs outils d'integrations disponibles: lambda, API gateway, EventBridge      

 Les steps functions permettent de combiner des fonctions aws avec des services aws (serverless orchestration service)              
 Une state machine a 1 workflow particulier basé sur des events particuliers: **state machine**      
 Step function fournit une interface graphique avec 1 diagramme des flux de l'application          
 Chaque état spécifiques correspondents à 1 single unit of work (unité de travail unique): **task**            
 Ses principaux composants sont les state machines et les tasks                



 _state machine_                
 State machine (définie dans 1 json)
 ```json
 {
   "Comment": "A Hello World example of the Amazon States Language using a Pass state",    ## optionnel - description lisible de l'état de la machine
   "StartAt": "HelloWorld",        ## obligatoire - Une chaîne qui doit correspondre exactement (sensible à la casse) au nom de l'un des objets d'état    
   "States": {                     ## obligatoire - Objet contenant un ensemble d'états séparés par des virgules
     "HelloWorld": {
       "Type": "Pass",
       "Result": "Hello World!",
       "End": true
     }
   }
 }
 ``` 

 Lorsqu'une exécution de cette machine d'état est lancée, le système commence par l'état référencé dans le champ `StartAt`     
 Si cet état a un champ `"End": true` ➡️ l'exécution s'arrête et renvoie un résultat     
 Dans le cas contraire, le système recherche un champ `"Next":` et continue avec cet état      
 Ce processus se répète jusqu'à ce que le système atteigne un état terminal (un état avec `"Type": "Succeed"`, `"Type": "Fail"` ou `"End": true`) ou jusqu'à ce qu'une erreur d'exécution se produise.


 

 Il existe 2 types de workflow: **standard** et **express**   
 Chaque workflow a des exécutions         
 Une exécution est 1 instance où vous exécutez votre workflow dans l'ordre pour réaliser les tasks  

 _workflow standard_               
  1 exécution               
  peut durer jusqu'à 1 an         
  utile pour vérifier les stats, datas
  taux > 2000 exécutios par seconde     

 _workflow express_        
  au moins 1 workflow execution         
  peut durer jusqu'à 5 minutes           
  utiles avec les workload à taux d'evenements élevés        
  utilisé dans l'IoT         
  Son prix est défini sur le nombre d'exécutions, la durée et la mémoire consommée      

 _integrated services_                 
  Enormément de services AWS peuvent être intégrés ➡️ les services intégrés seront donc vraiment dépendant de nos besoins

 _differents states_              
  **pass**: utilisé pour passer des datas, PAS pour compléter un job           
  **task**: unité de travail unique réalisée (lambda, batc, sns)              
  **choice**: ajout d'un état logisue (oui/non, 1 valeur...)           
  **wait**: création d'1 time delay spécifique dans chaque state machine               
  **succeed**: si atteint ➡️ on a réussit tout le workflow de la fonction: STOP en succès        
  **fail**: stoppe les exécutions et les décrit comme erreurs               
  **parallel**: exécute des branches d'exécutions en parrallèle dans les state machines        
  **map**: exécute une serie d'étapes basées sur des valeur d'un tableau           


**INGESTING DATA FROM SAAS APPLICATIONS TO AWS WITH AMAZON APPFLOW**                       
 _AppFlow_               
  Ce service permet          
  - d'automatiser les flux de données bidirectionnels entre les applications SaaS et les services AWS        
  - d'exécuter les flux de données quelle que soit l'échelle et la fréquence      
  - de simpliifer la péparation des données avec des transformations, partitions, agrégations       
  - d'automatiser la préparation et le comptage de votre schéma avec le catalogue de données AWS Glue      

  1. **Integration**: service d'intégration complétement géré pour les échanges de data entre SaaS apps et AWS Service    
  2. **Ingest data**: pull des data records depuis les 3e parties SaaS et les stocke dans AWS S3         
  3. **Bi-directional**: transfert de données dans les 2 sens aec des combinaisons limitées   

  AppFlow supporte les app tiers en SaaS quelles soient source ou destination         
  Cela permet de pouvori mapper tous les champs depuis le destination vers la cibles     
  Ne transfère que les data qui "rentrent" dans les filtres définis   
  Supporte plusieurs types de triggers: "run on demand", "run on event", "run on schedule"        

 _Etapes pour le bon fonctionnement de appFlow_               
  1. Connexion source/destination (appFlow vers app tiers party SaaS)        
  2. Création de la map (appFlow vers app tiers party SaaS)         
  3. Création des filtres (appFlow vers app tiers party SaaS)            
  4. Run appFlow (app SaaS vers appFlow)           


**Questions à se poser pour définir correctement le workflow**        
 1. le workload est-il synchrone ou asynchrone ? (pour définir les services à utiliser et son archi)            
 2. quel type de découplage ? (SNS, SQS, pousser les messages dans différentes queues.... ou besoin de StepFunctions pour définir le workflow)         
 3. Est-ce que l'ordre des messages est important ? (FIFO nécessaire ?)          
 4. quelle type d'application chargée doit-on voir ? (⚠️ certaines appli ont des limites ➡️ archi différente)       





=======================================================================================================================  
### <span style="color: #EA9811">**BIG DATA**</span>

**3 V DU BIG DATA**          
 - **Volume** : terrabytes ou petabytes - énormément de données stockées et conservées                            
 - **Variety** : toutes les datas viennent de différentes sources et dans différents formats           
 - **Velocity** : GRANDE VITESSE - Les data doivent être collectées, stockées, traitées et analysées en 1 très courte période      


**REDSHIFT**         
 Redshith est un service complètement managé data warehouse petabyte scale      
 C'est un Db relationnelle très grande  ➡️ RDS énorme          
 Meilleur rapport qualité/prix pour le stockage des données dans le cloud           

 Cluster ➡️ à sa création définition du nombre de noeuds          
         ➡️ pésent dans 1 seule AZ (pas de HA ➡️ pour cela créer des copies du cluster dans plusieurs AZ)       
 Possibilité d'exécuter des requêtes SQL depuis la console AWS       
 ⚠️ redshift n'est pas une RDS ➡️ l'un ne peut pas remplacer l'autre    


**ETL: Extract-Transform-Load**            
  Technologie informatique intergicielle permettant d'effectuer des synchronisations massives d'informations d'une source de données vers une autre          
   **Extract** (data des ERP, CMR, ...) ➡️ **Transform** (processes, routes, maps) ➡️ **Load** (data warehouse) 


**EMR: ELASTIC MAP REDUCE**             
 Exécutez et mettez à l'échelle (petaoctets) facilement Apache Spark, Hive, Presto et d'autres applications Big Data, aide à l'analyse interactive et au machine learning: c'est l'ETL de AWS        

 Cluster: nombre de noeuds à définir, dans 1 VPC           
   Chaque noeud est une instance EC2 (depuis la console, elles ne sont pas identifiées comme appartenant au cluster) ➡️ NE PAS supprimer les instances sans savoir ce qu'elles sont      
   niveau facturation ➡️ cluster facturé comme des instances "uniques" (pas de lien entre elles)        
    ➡️ possibilité d'uiliser des spots instances pour diminution du coût       


**KINESIS**           
 Collectez, traitez et analysez facilement les flux vidéo et de données en temps réel       
 Message broker en temps réel          

 Il existe 2 versions majeures de Kinesis:      
  - **data strem** : pas de limites de endpoints pour envoyer les datas       
   ➡️ ingestion de data streaming en temps réel          
   ➡️ presque en temps réel (très court laps de temps)         
   ➡️ responsable de la création des consommateurs et du scalling du stream        

  - **data firehose** : limite de endpoints existante pour l"envoi des datas         
   ➡️ L'outil de transfert des datas récupère les infos depuis S3, Redshift, Elacticsearch, Splunk       
   ➡️ presque en temps réel (moins de 60s)          
   ➡️ plug and play avec l'architecture AWS         

  _Kinesis - Data Stream_ 
   1. un _producer_ est quelquechose qui crée les datas (instances EC2, webApp, telephone...)        
   2. création des datas (formats: log, stats, vidéos...) puis envoie de celles-ci d'1 endpoint à 1 autre      
   3. creation de shards (1 shard peut prendre en charge 1 nombre defini de datas)        
   4. un _consumer_ (instance EC2) va récupérer les datas, les traiter et les pousser vers leurs cibles      
   5. les cibles (S3, dynamoDb, Redshift, EMR, Firehose)      

  _Kinesis - Data Firehose_ 
   Firehose gère le scaling en automatique et gèer seul le consumer    
   1. Input        
   2. Service Kinesis Data Firehose    
   3. n'importe quel service tier (elasticsearch, S3, Redshift...)      
   4. data niveau utilisateur    

  _Kinesis - Data Analytics et SQL_                
   Permet l'analyse de data en utilisant le standard SQL       
   Peut être utilisé avec Data Stream ou Firehose      
   Facile d'utilisation: très simple de lier Data Analytics dans 1 pipeline utilisant Kinesis      
   Service serverless en temps-réel entièrement managé             
   Prix: ne paie que pour les ressources consommée à travers lesquelles sont passées des datas    

 _SQS_          
  SQS est un message broker. Il ne délivre pas de message en temps réel: si besoin de data en temps réel utiliser Kinesis    


**ATHENA**       
 Analyser des pétaoctets de données là où elles se trouvent, de manière simple et flexible         
 Service d'analyse serverless interactif basé sur des cadres open source, prenant en charge les formats de table et de fichier ouverts            
 Analyse des data ou crée des app à partir d'un data lake chez S3 à l'aide de SQL ou python        


**GLUE**             
 Service d'intégration des données serverless qui facilite la découverte, la préparation, le déplacement et l'intégration des données depuis des sources multiples pour l'analyse, le machine learning (ML) et le développement des applications    
 Fait le design pour les datas        

 **ELT: Extract, Load, Transform** ➡️ extrait des données à partir d’une ou plusieurs sources distantes, mais les charge ensuite dans le datawarehouse cible sans changement de format


 Athena et Glue sont complémentaires ➡️ intéressant de les utiliser ensemble      


**QUICKSIGHT**  
 Il permet à tous les membres de votre organisation de comprendre vos données en posant des questions en langage naturel, en explorant via des tableaux de bord interactif, ou en recherchant automatiquement des modèles et des anomalies alimentés par le machine learning                
 Ce service dispose d'une architecture serverless qui s'adapte automatiquement à des centaines de milliers d'utilisateurs sans qu'il soit nécessaire d'installer, de configurer ou de gérer vos propres serveurs           
 Cela garantit également que vos utilisateurs n'ont pas à gérer des tableaux de bord lents pendant les heures de pointe, lorsque plusieurs utilisateurs de Business Intelligence (BI) accèdent aux mêmes tableaux de bord ou ensembles de données    


**AWS DATA PIPELINE**           
 Service Web qui vous permet de traiter et de transférer des données de manière fiable entre différents services AWS de stockage et de calcul et vos sources de données sur site, selon des intervalles définis  ➡️ c'est un ETL           


**KAFKA - AMAZON MSK**          
 Diffuser vos données en toute sécurité grâce à un service Apache Kafka entièrement géré et hautement disponible         
 Amazon MSK facilite l'ingestion et le traitement des données de streaming en temps réel avec Apache Kafka entièrement géré          


**OPENSEARCH**             
 Permet d'effectuer facilement des analyses interactives de logs , de surveiller des applications en temps réel, de rechercher du contenu sur site web...        
 Permet la creation de dashboards             



=======================================================================================================================  
### <span style="color: #EA9811">**SERVERLESS**</span>

Créer et exécuter des applications sans se soucier des serveurs     

Pourquoi le serverless ? ➡️ beacoup moins de travail  ➡️ gestion uniquement de l'application 
  ➡️ pas de gestion de l'infrastructure          


**AWS Lambda**      
  Exécuter du code sans vous soucier des serveurs ou des clusters      
  Action à faire: écrire du code         

  Le service Lambda utilise **Lambda function** qui sont des fonctions qui peuvent être écrites dans différents langages     
  ➡️ on écrit le code ➡️ on exécute le code ➡️ on stoppe le code ➡️ on met le code à jour  
    ➡️ seules actions à faire      

  Service de calcul idéal pour les scénarios d'application qui doivent augmenter la capacité rapidement, et la réduire à zéro lorsqu'elle n'est pas demandée      

  *Cas d'utilisation*:       
  - **Traitement de fichiers**: utilise **S3**    ➡️ traitement en temps réel               
  - **Traitement des flux**: utilse **Kinesis**   ➡️ traitement en temps réel (activité app; ordre de transactions; analyse de flux; nettoyage données; filtrage logs; indexation; analyse reseaux sociaux; telemetrie datas IoT; metriques)          
  - **Applications Web**: utilise d'autres services ➡️ app web puissantes scale auto et HA        
  - **Backends IoT**: creation backend pour gestion requêtes API web, mobiles, IoT, tierces         
  - **Backends mobiles**: utilise **API Gateway** ➡️ creation backend pour authent et traitement des requêtes API   
                          ➡️ utilise **Amplify** ➡️pour integration facile aux frontends (IOS, android, Web, React)          

  *Lambda Function*: sa création se fait en 5 étapes           
    1. **Runtime**      ➡️ écrire du code dans 1 langage supporté par Lambda (python....)          
    2. **Permissions**  ➡️ attacher un role IAM (avec tous ses droits) à la fonction pour pouvoir faire les actions souhaitées          
    3. **Networking**   ➡️ peut appartenir à 1 VPC ou subnet défini ou depuis n'importe quel network si pas besoin de endpoints         
    4. **Resources**    ➡️ définir les resources maximales (CPU, RAM)              
    5. **Trigger**      ➡️ définir quel va être le déclencher de la fonction          

  
**AWS Fargate**       
  Calcul sans serveur pour containers       
  Action à faire: démarrer un container     
  NE peut travailler seul ➡️ associé à ECS ou EKS           

  Fargate permet la gestion de nombreux containers à la fois (action d'upgrade par ex)       

  *EC2 vs Fargate vs Lambda*      
    - **EC2**       
      ➡️ vous êtes responsables des sous-couches OS       
      ➡️ est moins cher que Fargate suivant le modèle choisit (ex: spot)     
      ➡️ utilisé avec les containers **long-running**      
      ➡️ plusieurs containers se partagent le même host      
    - **Fargate**      
      ➡️ pas de gestion d'OS      
      ➡️ paie pour les resources allouées et utilisées     
      ➡️ pour les containers **short-running**      
      ➡️ pas de dépendance d'environnements ➡️ pas de panne si un autre container ne fonctionne pas    
      ➡️ bon choix pour la standardisation       
    - **Lambda**       
      ➡️ excelle avec l'utilisation d'une fonction            
      ➡️ difficle pour la standardisation        
      ➡️ parfait pour les appels API      


**Serverles Application Repository**       
  C'est un referentiel d'applications serverless      
  Pas besoin d'installation pour l'utilisé     
  Il est géré par AWS      

  Il possède 2 options:       
    - **publish**        
       ➡️ rend dispo les app une fois sur ce repo (par defaut ➡️ privé ➡️ il FAUT le partagé si l'on veut qu'il soit publique)      
       ➡️ est créé à partir de **AWS SAM Template**                         
    - **deploy**     
       ➡️ trouver les app publiées    
       ➡️ pas besoin de compte AWS pour en deploy une     
       ➡️ on peut la choisr avec la console Lambda     
       ➡️ faire attention si on fait confiance ou non          


**AWS ECS**      
  **ECS**: **Elastic Container Service**  ➡️ permet de gérer des 100 - 1000 de containers     
    ➡️ service d'orchestration de containers (qui fonctionnent avec des services AWS) entièrement géré       
    ➡️ permet le déploiement, gestion des app containeurisées facilement (y compris la gestion des ELB est prise en charge)     
    ➡️ permet l'attachement de roles qui donnent les droits sur le container      



**AWS EKS**       
  **EKS**: **Elastic Kubernetes Service** ➡️ comme ECS mais pour aussi ce qui n'est pas AWS (ex: on-premise)     
    ➡️ service Kubernetes géré qui exécute des cluster Kubernetes dans et en dehors de AWS      
    ➡️ à utiliser lorsqu'on parle open-source        


**EventBridge ou CloudWatch Event**      
  Fournit un flux d'events systeme en quasi temps réel       
  Décrit les modif apportées aux ressources AWS     
  ➡️ prend connaissance des changements opérationnels     
  ➡️ répond à ces changements     
  ➡️ si nécessaires prend des mesures correctives et envoie des messages      
  ➡️ active les fonctions      
  ➡️ procède aux modifications en capturants les infos de statut     
  ➡️ planifie et déclenche des actions      
  ➡️ permet de configurer les services AWS en tant que cible (EC2, LambdaFunction, Flux, CloudWatch Logs, ECS.....)     

  - **event**: évènement indiquant un changement dans l'environnement AWS     
  - **regles**: évènements entrant acheminés vers des cibles pour être traités    
  - **cibles**: traintent les events. La cible DOIT être dans la même region que le regle    

  *creation d'une regle*        
    1. définir le declancher       
    2. selectionner l'event (appel API) qui va déclencher      
    3. choisir sa cible ➡️ action à faire (pas de limite avec lambda function)           
    4. tagguer ➡️ permet de filtrer facilement       
    5. laisser faire la regle      


**AWS ECR**        
  **ECR**: **Elastic Container Registry**      
   ➡️ managé par AWS      
   ➡️ privé      
   ➡️multi formats ➡️ supporte OCI, Docker Images, OCI Artifacts      
   ➡️ token pour la connexion     
   ➡️ manage les images
      - scan pour recherche vulnerabilités      
      - regles de nettoyage des images non utilisées    
      - partage ➡️ inter region/account      
      - fait des pull à travaers des regles de cache    
      - préviennent des overwritting des images (avec des tags)    


**AWS EKS DISTRO (EKS-D)**       
  distribution EKS managé par vous     
  ➡️ fonctionne partout (on-premise)      
  ➡️ sous votre responsabilité (upgrade/manage)     


**AWS ECS ANYWHERE**      
  exécuter des containers sur sa propre infra     
  basé sur ECS     
  ➡️ exécute et gère les workloads des containers sur votre infra (VMs, server bare metal)        
  ➡️ MCO/MCS pris en charge    
  ➡️ prise en charge des LB dans le cas de nouveaux containers ou instances    
  ➡️ prise en compte de services "externalisés" comme des service internes avec **EXTERNAL**    
    Pré-requis:    
    - SSM Agent, ECS Agent, Docker installés     
    - enregistrer en 1er les instances externes comme SSM MAnaged Instances       




**AWS EKS ANYWHERE**      
  créer et exploter des cluster Kube n'importe où (même sur votre infra)     
  basé sur EKS Distro     
  ➡️ managé par vous     
  ➡️ datas en local (bien pour data sensibles)      
  ➡️ outils d'aide au management ➡️ Flux (facile à utiliser) ou aws cli   
  ➡️ packages organisés    
  ➡️ seul une entreprise peut soucrire à ce service     


**AWS AURORA SERVERLESS**       
  basé sur Aurora    
  ➡️ configuration à la demande à scalabilité automatique     
  ➡️ start/stop/augmente/reduit la capacité en fonction des besoin de l'application (automatique)     
  ➡️ à la demande  ➡️ paie que ce qu'on utilise     
  ➡️ **ACU**: **Aurora Capacity Units** ➡️ scale auto à partir de ces mesures     
  ➡️ définir les min/max des ACUs (peut être 0)     
  ➡️dans 1 pool de ressources type "warm" pour être prêt à tout moment     
  ➡️ 2 Gb Ram, correspond au CPU et la capacité reseau      
  ➡️ présent dans 6 AZ ➡️ pas de perte de datas + HA       

  Use cases: 
  - capacity planning         
  - workloads variable        
  - app multi usage       
  - app multi-tenant       
  - dev et test         
  - nouvelle app ➡️ pas de certitude sur les besoins de db     
  

**AWS X-RAY**      
  Analysez, debugguez la prod et les app distribuées ➡️ APM      
  ➡️ utilise X-Ray daemon: app qui écoute sur le port UDP 2000       
  ➡️ collecte les infos pour les envoyer aux workers     


**AWS GraphQL**    
  Accélérer le dev des app avec appels API     
  ➡️ interface graphique pour les dev (surtout frontend)      
  ➡️ permet des interactions avec les APIs



=======================================================================================================================  
### <span style="color: #EA9811">**SECURITY**</span>


AWS fournit des services dédiés à la securité dans le cloud     


**DDoS attaques**      
  **DDoS: Distributed Denial of Service**  ➡️ attaque qui rend le site web ou l'application innaccessible par les users    

  *Attaque DDoS couche 4*     
    ➡️ **SYN Flood** ➡️ rend le serveur indisponible par pas de réponses ACK reçues (Syn/Syn-ack/Ack)    

  *Attaque DDoS couche 7*       
    ➡️ Attaque qui envoie à l'application (couche 7) une quantité massive de requêtes GET ou POST (à l'aide de botnets) qui submergent les ressources du serveur qui gère l'application     

  *Amplification/Reflexion attack: couche 3*     
    ➡️ un attaquant peut envoyer à un serveur tiers (tel qu'un serveur NTP, DNS...) une requête en utilisant une adresse IP usurpée     
    ➡️ le trafic reçu par la victime devient énorme (40 à 50 fois plus)


**AWS CloudFront**        
  Diffuser du contenu en toute sécurité avec une faible latence et des vitesses de transfert élevées    
  Amazon CloudFront est un réseau de diffusion de contenu (CDN: Content Delivery Network) conçu pour des performances élevées, pour la sécurité et pour la simplicité de développement     


**AWS CloudTrail**      
  Suivez l'activité des utilisateurs et l'utilisation des APIs sur AWS et dans des environnements hybrides et multicloud    
  AWS CloudTrail contrôle et enregistre l'activité du compte sur l'ensemble de votre infrastructure AWS afin de vous donner le contrôle sur le stockage, l'analyse et les actions correctives    

  CloudTrail permet l'audit, la surveillance de la sécurité et le dépannage opérationnel en suivant l'activité des utilisateurs et l'utilisation des API  
  CloudTrail conserve certains logs seulement sur la region Virginia ➡️ lors d'un debug, il peut être utile d'aller voir dans cette region        


**AWS Shield**     
  Optimisez la disponibilité et la réactivité des applications grâce à une protection DDoS gérée     
  AWS Shield est un service géré pour la protection contre les attaques DDoS et qui protège les applications exécutées sur AWS

  *Shield standard*    
    ➡️ activé automatiquement sans frais                
    ➡️ fonctionne sur EC2, ELB, CloudFront, Global Accelerator, Route53           
    ➡️ protege contre les attaques couche 3 et 4            

  *Shield advanced*                
    ➡️ 3000$ par mois    
    ➡️ protection supplémentaires de plus grande ampleur et contre attaques plus sophistiquées      
    ➡️ surveillance permanente du reseau     
    ➡️ accès à la DRT (DDoS Response Team) 24/7       
    ➡️ protège contre la surfacturation ELB, cloudFront, Route53 en cas d'attaque sur ceux-ci        


**AWS WAF**    
  **WAF : Web Application Firewall**         
  Protégez vos applications web contre les codes malveillants les plus répandus         
  AWS WAF vous aide à vous protéger contre les codes et bots web malveillants les plus répandus qui peuvent affecter la disponibilité, compromettre la sécurité ou consommer des ressources excessives         

  ➡️ protection sur la couche 7   

  *3 comportements du WAF*         
    ➡️ autorise TOUTES les requêtes SAUF celles définies              
    ➡️ bloque TOUTES les requêtes SAUF celles définies                  
    ➡️ compte les requêtes qui matchent avec les propriétés définies             

  *conditions du WAF*              
    ➡️ IP ➡️ localisation source des requêtes                 
    ➡️ region ➡️ localisation source des requêtes                 
    ➡️ parametres dans les headers des requêtes                
    ➡️ présence de code SQL ➡️ SQL injection              
    ➡️ presence de script ➡️ X-cross injection           
    ➡️ string dans la requête soit correspondre aux regex définies            


**AWS GuardDuty**             
  Protéger vos comptes AWS avec la détection intelligente des menaces              
  Amazon GuardDuty est un service de détection des menaces qui surveille en permanence vos charges de travail et vos comptes AWS pour détecter les activités malveillantes et fournir des résultats de sécurité détaillés pour la visibilité et la correction                  

  Gratuit les 14 premiers jours puis prix calculés en fonction de la quantité de cloudTrail events et du volume de données du DNS et flux VPC     

  ➡️ remonte les appels API inhabituels     
  ➡️ remonte les deploiements non autorisés          
  ➡️ remonte les instances compromises           
  ➡️ remonte les scan de ports et login failed      
  ➡️ affiche les alertes dans guradduty console et cloudWatch events    
  ➡️ reçoit les flux des app tiers (crowdstrike)     
  ➡️ monitore les flux VPC, logs DNS, cloudTrail           
  ➡️ fait du machine learning et se base sur les baseline (7 à 14 jours de datas) pour les alertes      


**AWS Firewall Manager**          
  Configurer et gérer de manière centralisée les règles de pare-feu sur vos comptes        
  AWS Firewall Manager est un service de gestion de la sécurité qui vous permet de configurer et de gérer de manière centralisée les règles de pare-feu dans vos comptes et vos applications dans AWS Organizations       
  Lorsque de nouvelles applications sont créées, Firewall Manager facilite également la mise en conformité des nouvelles applications et ressources en appliquant un ensemble commun de règles de sécurité     

  Firewall Manager permet de créer de nouvelles regles AWS WAF pour les applications LB, API gatways et amazon CloudFront    

  Si scénario avec multiple aws accounts et ressources qui on besoin d'être securisée de manière centrale ➡️ Firewall Manager


**AWS Macie**      
  Identifier et protéger vos données sensibles à grande échelle          
  Amazon Macie est un service de sécurité et de confidentialité des données qui utilise le machine learning (ML) et la correspondance des modèles pour découvrir et protéger vos données sensibles         

  ➡️possède une AI qui analyse les datas dans S3  ➡️ aide à identifier les données personnelles(PII), de santé (PHI) et financières     
  ➡️ améliore la compliance     
  ➡️ peut être intégré à d'autres service pour automatiser des actions             


**AWS Inspector**      
  Une gestion des vulnérabilités automatisée et continue à grande échelle         
  Amazon Inspector est un service de gestion automatisée des vulnérabilités qui recherche en permanence les vulnérabilités logicielles et les expositions réseau involontaires dans les charges de travail AWS       

  ➡️ scan de vulnérabilité sur EC2: **hosts assessment**        
  ➡️ scan de vulnérabilité sur VPC: **network assessment**             


**KMS: Key Management Service**                    
  Créez et contrôlez les clés utilisées pour chiffrer ou signer numériquement vos données        
  AWS Key Management Service (AWS KMS) vous permet de créer, de gérer et de contrôler des clés cryptographiques dans vos applications et vos services AWS.          

  ➡️ la 1ere action est de créer une **CMK: Customer Master Key**      
  ➡️ KMS est déjà intégré dans les services EBS, S3, RDS
    ➡️ facilite l'encryption des datas


**AWS CloudHMS**        
  Gérer les modules de sécurité matériels (HSM) à locataire unique sur AWS           
  AWS CloudHSM vous aide à répondre aux exigences de conformité des entreprises, des contrats et des réglementations en matière de sécurité des données       

  **HSM : Hardware Security Module**          
    ➡️ Serveur physique qui conserve de manière sécurisée ces clés et les manage


**CMK**           
  clé à partir de laquelle les clés d'encryption vont être générées           

  *3 manières de générer une CMK*          
    ➡️ aws s'en occupe à partir d'un HSM managé par AWS        
    ➡️ import de la clé material depuis votre propre clé de management d'infrastructure et vous l'associez en tant que CMK            
    ➡️ clé materielle générées et utilisées par aws cloudHsm dans un keystore aws kms


  ➡️ l'accès aux CMK est géré via les policies      
  ➡️ il est OBLIGATOIRE d'attacher 1 policy *resource-based* à votre CMK     

  *3 manières de controler les permissions*     
    ➡️ utilisation de la key policy ➡️ les accès sont définis dans la policy de la CMK          
    ➡️ utilisation d'une policy IAM + key policy ➡️ tous les accès peuvent être configurés dans la policy IAM     
    ➡️ utilisation de grant avec combinaison de key policy ➡️ si les accès sont autorisés dans le key policy une delagation doit autoriser les users à donner les accès à d'autres users        


**AWS Secret Manager**          
  Gérer de manière centralisée le cycle de vie des secrets         
  AWS Secrets Manager vous permet de gérer, récupérer et faire alterner les informations d'identification de base de données, des clés d'API et d'autres secrets tout au long de leur cycle de vie       
    ➡️ l'encryption se fait lors du transit
    ➡️ les credentials ont une rotation automatique

  Les applications dont les secrets sont stockées via Secret Manager utilise l'API secret Manager     
  La rotation des credentials est très simple (mais attention lors de la démarche)      
  Si activé secret manager gère la rotation des credentials     
  S'assurer que les instances soient configurées pour utiliser secret manager AVANT de lancer la rotation des credentials     


**AWS Parameter Store**         
  Gérez vos ressources sur AWS et dans des environnements multicloud et hybrides      
  AWS Systems Manager est une solution de gestion sécurisée de bout en bout pour les ressources sur AWS et dans des environnements multicloud et hybrides     

  *2 limitations*          
    ➡️ limitation à 10000 parametres
    ➡️ pas de rotation de clés


  Parameter store est gratuit           


**AWS Presigned URLs**           
  Les presigned cookies sont utiles lorsqu'on souhaite partager une multitude de fichiers dans un bucket privé    
  Le cookie sera donwloadé sur le PC du user et le user sera capable de naviguer dans la totalité des contenus   

  La creation d'une presigned url nécessite:
    ➡️ fournir (par le owner de l'objet) ses security credentials
    ➡️ spécifier le nom du bucket et la clé de l'objet
    ➡️ indiquer la methode HTTP (GET pour download)
    ➡️ indiquer une date et heure d'expiration

  Pour un partage de fichier à partir d'un S3 privé ➡️ utilisation de presigned URLs     


**IAM Policies**      
  Les policies IAM sont des fichiers json      
  Elles contiennent les regles définies pour les users, accounts, groupes      
  Elles doivent être attachées pour être utilisées     

  Si une regle n'est pas autorisée explicitement ➡️ elle est refusée implicitement      
  Si présence d'un refus explicit ➡️ c'est lui qui gagne      
  Il est possible de joindre plusieurs policies entre elles mais elles obéissent aux regles précédentes    
  Les policies AWS sont managées par AWS, les policies custom par les users     


**AWS Certificate Manager**     
  Provisionner et gérer les certificats SSL/TLS avec les services AWS et les ressources connectées     
  Utilisez AWS Certificate Manager (ACM) pour mettre en service, gérer et déployer des certificats SSL/TLS publics et privés à utiliser avec les services AWS et avec vos ressources internes connectées     
  Le renouvellement de ces certificats est automatisé        

  On ne paie plus pour les certificats, seulement pour le service (ELB, cloudFront, API Gateway) qui utilise les certificats   


**AWS Audit Manager**     
  Auditez en permanence votre utilisation d'AWS pour simplifier l'évaluation des risques et de la conformité    
  Utilisez AWS Audit Manager pour faire correspondre vos exigences de conformité aux données d'utilisation d'AWS grâce à des cadres préconçus et personnalisés et à la collecte automatisée de preuves     

  Si on parle de HIPAA ou GDPR complicance dans un audit continue ou un report d'audit automatique ➡️ audit manager   


**AWS Artifact**      
  Accéder aux rapports de sécurité et de conformité d'AWS et des ISV (prévisualisation)       
  AWS Artifact est votre ressource centrale à laquelle vous pouvez vous référer pour obtenir des informations importantes sur la conformité      

  Est à utiliser dans le cas de report de compliance pour un audit      


**AWS Cognito**          
  Mettez en œuvre une gestion des identités et des accès des clients sécurisée et transparente qui évolue      
  Ajouts de fonctions d'inscription et d'identification des utilisateurs et contrôler l'accès à vos applications web et mobile          
  Fournit un magasin d'identités qui s'adapte à des millions d'utilisateurs, prend en charge la fédération d'identité sociale et d'entreprise, et offre des fonctions de sécurité avancées pour protéger vos consommateurs et votre entreprise         

  *user pool*         
    ➡️ répertoires d'utilisateurs qui fournissent des options pour les enregistrement des utilisateurs d'applications      

  *identity pool*         
    ➡️ permet de donner des accès utilisateurs à d'autres services AWS          


**AWS Detective**                
  Analyser et visualiser les données de sécurité pour enquêter sur les problèmes de sécurité potentiels         
  Simplifie le processus d'enquêtes et aide les équipes de sécurité à mener des enquêtes plus rapides et plus efficaces       

  Fonctionne à travers plusieurs service AWS et analyse la root-cause d'un event     


**AWS Network Firewall**             
  Déployer la sécurité du pare-feu réseau sur vos VPCs             
  Définir des règles de pare-feu qui permettent un contrôle précis du trafic réseau     


**AWS Security Hub**       
  Automatiser les vérifications de sécurité AWS et centraliser les alertes de sécurité             
  Service de gestion de la posture de sécurité dans le cloud qui effectue des vérifications de bonnes pratiques, regroupe les alertes et permet l'utilisation de la correction automatisée           

  Permet de regrouper les services de securité AWS en 1 seul endroit      


=======================================================================================================================  
### <span style="color: #EA9811">**AUTOMATION**</span>

Il existe 3 sevices fournit par AWS pour l'automatisation:     
- **cloudFormation**       
- **Elastic Beanstalk**      
- **System Manager**       


**cloudFormation**      
  ➡️ Infra as Code ➡️ **IaC**            
  Accélérez le provisionnement cloud avec l'infrastructure en tant que code    
  On écrit du code (yaml ou json) et on ajoute ce fichier au service ➡️ l'infra est créée   

  *3 parties importantes dans le fichier*     
    - **Resources**   ➡️  liste des ressources qui vont être créées    
    - **Mappings**    ➡️  comment les ressources vont être liées entre elles   
    - **parameter**   ➡️  définir les customs values des ressources       


  ➡️ l'infra est immuable, on peut le refaire à l'infini (IaC)               
  ➡️ ne pas coder en dur les IDs ➡️ pb inter-region: un ID peut fonctionner dans 1 region et pas dans 1 autre ➡️ utilisation de **parameter store** qui est un coffre-fort de secret ➡️ cloudFormation va chercher ses infos ici     
  ➡️ tout ce qui peut être fait de manière manuelle peut être codé pour cloudFormation (pour l'infrastructure) ➡️ API calls      
  ➡️ en cas d'erreur cloudFormation fait un rollback vers la dernière version qui fonctionne correctement


**Elastic Beanstalk**   
  ➡️ **PaaS**   ➡️ Platform as a Service
  ➡️ Déployer et mettre à l'échelle des applications web     
  ➡️ Seulement quelques parametres à mettre en place ➡️ pas de code      
  ➡️ déploiement gérés par le service     
  ➡️ platform managée par le service (pas la conf des instances est gérées)   

  ➡️ service très utile pour démarrer les app web MAIS à long terme il sera mieux de gérer les applications sans ce service     
  ➡️ service supportant les containers, applications windows et linux      
  ➡️ fonctionne avec une instance EC2 ➡️ ce n'est pas un service serverless    


**System Manager**             
  ➡️ SysAdmin tools      
  ➡️ utilise **automation Documents** ➡️ permet de configurer les parametres d'une instance ou de ressources AWS (comme S3 bucket, IAM account....)        
  ➡️ **automation Documents** == **runbooks**       
  ➡️ déployer l'agent sur chaque instance pour pouvoir utiliser le service     
  ➡️ l'agent permet l'exécution de commandes: scripts, appels OS.....
    ➡️ dans le cas d'une flotte d'instance ce service permet de lancer la commande en 1 fois (pas besoin de se connecter sur chaque instance)       
  ➡️ permet la planification de déploiements de patch (avec la version choisie) ➡️ pas besoin de se connecter à chaque serveur       
  ➡️ utilise le **parameter store** ➡️ pas de harcodage ➡️ va récupérer les clés/valeurs     
  ➡️ est utilisable sur AWS architecture et sur du on-premise         
  ➡️ permet la connexion (session manager) depuis un browser ➡️ pas besoin de connaitre les clés ssh pour la connexion    












=======================================================================================================================  
### <span style="color: #EA9811">**CACHING**</span>


Il existe 2 types de caches différents:      
- **external**    ➡️  proche du user (en général sur le reseau)            
- **internal**    ➡️  dans 1 outil de cache pour une accélération des databases


_4 options de cache chez AWS_        
- **cloudfront**:           
  ➡️ solution externe      
  ➡️ CDN: Content Delivery Network      
- **elasticache**:       
  ➡️ solution interne         
  ➡️ positionné en front de la database         
- **DAX**:         
  ➡️ solution interne          
  ➡️ positionné en front de la database DynamoDB       
- **Global Accelerator**:           
  ➡️ solution externe           
  ➡️ gain de vitesse pour les connexions externes    


**CloudFront**          
  Service web qui accélère la distribution des contenus web statiques et dynamiques        
  Il diffuse le contenu à travers le reseau mondial des data centers         
  La requête d'1 user est dirigée vers la region où la latence est la plus faible     
  Il utilise **aws edge locations** pour réduire la latence durant les transferts       

  Met en place le **HTTPS** ➡️ securisé     
  Crée une connexion sécurisée avec S3           

  Peut mettre en place des regles de filtrages (seulement _Allow_ ou _Deny_) ➡️ préférable d'utiliser le WAF      
  Possible de forcer le refresh du contenu du cache (pas de TTL bloqué)     


**Elasticache**        
  Utilise 2 sources opensource              
  Est mis en place DANS votre infrastructure en front des databases     

  _Memcache_:              
    Storage de contenus cache (pas éternel)      
    Ce n'est pas une database       
    Pas de failover ou multi-AZ support          
    Pas de backups              



  _Redis_:  
    Utilisé comme solution de caches        
    Possède les fonctions comme une database standalone non-relationnelle                
    Possède un failover et le support Multi-AZ         
    Possède des backups    


**DAX: DynamoDB Accelerator**             
  N'est utilisé QUE en front de DynamoDB         
  Possède du in-memory cache       
  Reduit la latence de millisecondes aux microsecondes         
  Le cache est hautement disponible et se trouve à l'intérieur d'un VPC       
  On peut configurer:         
    - taille des noeuds      
    - TTL            
    - maintenances, updates....



**Global Accelerator**               
  Améliore la disponibilité, les performances et la sécurité de vos applications en utilisant le réseau mondial d'AWS             
  Service de mise en reseau qui améliore la dispo, perf et securité des applications publiques       
  Il fournit 2 IPS statiques mondiales qui servent de endpoints aux users      
    ➡️ tout le reste est géré par AWS     
  
  Se connecte au endpoint le plus proche en fonction de la region de connexion du user       



=======================================================================================================================  
### <span style="color: #EA9811">**GOUVERNANCE**</span>

Le but de la **gouvernance** au niveau AWS est de pouvoir contrôler les coûts, les regles de conformités, maitriser les ressources ET innover     

Les piliers sont: 
- **echelle**  ➡️   AWS est adapté à la gestion des ressources dynamiques à grande échelle      
- **simplicité**   ➡️ AWS simplifie le processus et offre un plan de controle aux clients pour gérer et maitriser les ressources     
- **solutions tierces**  ➡️ AWS offre un large ecosysteme partenaire pour élargir et dévolopper le systeme de gestion et gouvernance    
- **reduction de coûts**  ➡️ AWS offre la faciliter d'optimisation des couts        


**Liste des services de gestion et de gouvernance AWS**           
  _accelerer le travail des dev_          
    AWS Control Tower, AWS Organizations, AWS Well Architectured Tool (AWA Tool), AWS Budgets, AWS Licence Manager     

  _mettre en service des ressources et app_           
    AWS CloudFormation, AWS Service Catalog, AWS OpsWorks, AWS Marketplace             

  _exploiter l'environnement_           
    Amazon CloudWatch, Amazon Managed Grafana, Amazon Managed Service Prometheus, AWS CloudTrail, AWS Config, AWS Systems Manager, Rapport coûts et utilisation AWS, AWS Cost Explorer, AWS Managed Services, Connecteurs AWS Service %anagement, AWS X-Ray, AWS Distro, AWS Proton, AWS Devops Guru



**AWS ORGANIZATIONS**     
  AWS Organizations est un outil de gouvernance AWS gratuit qui permet de créer, manager de multiples accounts AWS        
  Avec on peut controller tous ces accounts d'1 seul endroit plutot que de se connecter à chacun d'eux 1 par 1         

  _Logging Accounts_     
    Bonne pratique de dédier un account dédié à la gestion des logs    
      ➡️ cloudTrail supporte l'aggrégation des logs     
      ➡️ cet account possède un S3 bucket pour le stockage des logs et requêtes API     

  _Programmatic Creation_     
    Facilté de creation et destruction de nouveaux accounts AWS     
      ➡️ possibilité de faire ces actions via un appel API     

  _Reserved Instances_           
    Les instances réservées (RIs) peuvent être partagées à tous les accounts de AWS Organizations     

  _Consolidated Billing_     
    L'account "primaire" paie la facturation ➡️ 1 seule facture détaillée (qui fait quoi) centralisée    

  _Service Control Policies: SCP_           
    Ces policies sont les regles de "haut niveau" de AWS qui gère les permissions des utilisateurs   
      ➡️ si on a un "desaccord" dans les regles c'est la SCPs qui aura la décision finale          
      ➡️ elles prédominent même sur les droits du user "root"      


**AWS RAM: Resource Access Manager**        
  AWS RAM vous aide à partager vos ressources en toute sécurité entre les comptes AWS, au sein de votre organisation ou des unités organisationnelles (OU), et avec les rôles et utilisateurs IAM pour les types de ressources pris en charge    

  Il est possible de partager avec AWS RAM entre plusieurs accounts:     
    - Transit gateway     
    - Licence manager      
    - Dedicated hosts         
    - VPC subnets      
    - Route53 Resolver           
    - ......

  **Partage de ressources DANS la même region**  ➡️ **AWS RAM**    
  **partage de ressources A TRAVERS PLUSIEURS regions**  ➡️ **VPC peering**         

  Le partage de ressources permet de ne pas les dupliquer ➡️ gain sur la facturation     
  RAM est gratuit    
  RAM autorise Organizations à partager son archi            


**CROSS ACCOUNT - ROLE ACCESS**        
  C'est la délégation de droits via des roles IAM ➡️ création de **roles transverses**: utilisation de `assumeRole`     
  Plus besoin de créer et/ou dupliquer des accounts pour avoir des droits ➡️  Ces roles transverses permettent d'avoir des accès temporaires pour différentes actions        

  _Mise en place d'un role transverse_          
    1. creation d'un role IAM pour lequel on fera un `assumeRole` sur celui-ci pour les différents accounts IAM pour qui on souhaite avoir les permissions données à ce role                   
    2. on donne les droits aux user IAM définis via le `assumeRole` sur ce role              
    3. l'account fait la requête pour l'action souhaitée ➡️ si le `assumeRole` contient l'action de la requête dans sa policy, alors l'action sera exécutée      

  Ces roles sont accessibles n'importe où       
  Ils ne nécessitent pas de credentials (comme pour les users)          
  Donne des droits temporaires (le temps de la requête)           


**AWS CONFIG**               
  Gestionnaire d'inventaires et d'outils de contrôles             

  _query_ ➡️ permet de voir avec une simple requête l'architecture de votre account (ressource type, tags, infra supprimée)    
  _enforce_ ➡️ des regles peuvent êtes créées pour alerter lorsque quelque chose ne fonctionne pas correctement           
  _ learn_  ➡️ permet d'avoir l'historique de l'environnement de l'account (qui à fait quoi et quand....)      

  Config est payant, il ne resoud pas les problèmes mais permet de les détecter facilement, de connaitre l'historique, de mettre en place des regles et vérifie si les standards AWS sont appliqués    


**AWS DIRECTORY SERVICE**           
  Ce service permet à vos charges de travail et ressources AWS prenant en charge les répertoires, d'utiliser des AD sur AWS       

  Il existe sous 3 types:    
   - **managed microsoft AD** ➡️ Une suite AD (Active Directory) sur AWS        
   - **AD connector**  ➡️ connexion entre AWS et l'AD on-premise      
   - **simple AD**  ➡️ AD standalone Linux Samba compatible serveur      

  
**AWS COST EXPLORER**     
  Outil permettant la visualisation des couts du cloud                
  Il génère des rapports à partir de différents parametres et resource tags      
  Il se projette pour faire de la prédiction de budget (en rapport avec l'historique)     


**AWS BUDGETS**          
  Permet aux entreprises de planifier et parametrer les attentes budgetaires autour du cloud         

  Il existe 4 types de budget que l'on peut créer:         
   - **cost budgets**  ➡️ Combien est dépensé ?  ➡️ liste l'archi et défini une limite pour le mois         
   - **usage budgets** ➡️ Combien nous utilisons ? ➡️ liste ce qui est présent ET ce qui va arriver pour estimer le budget      
   - **reservation budgets** ➡️ Sommes-nous efficaces avec nos RIs ?  ➡️ en rapport avec les projets arrivants a-t-on suffisamment de RIs ?      
   - **saving plan budgets** ➡️ respectons-nous le plan economique ?     

  Ce service est gratuit pendant 2 mois          
  Lorsque nous créons une alerte il est possible d'y attacher 1 action ou 1 policy (ex: arrêter une instance)      


**AWS BILLING**           
  Ce service  s'intègre au service AWS IAM afin que vous puissiez contrôler l'accès de personnes de votre organisation à des pages spécifiques sur la console AWS Billing          


**AWS REPORTS CUR**          
  Ce service permet d'utiliser le pluginRapports sur les coûts et l'utilisation de la console Billing and Cost Management pour créer des rapports de coûts et d'utilisation        
  **CUR**: **Cost and Usage Reports**            

  Ces rapports sont simples, publiés sur S3 (1 historique est présent), offrent beaucoup de filtres (permet des recherches précises); créés quotidiennement en csv, facilement intégrables ) AWS Athena, Redshift, QuickSight     


**AWS COMPUTE OPTIMIZER**        
  Obtenir des recommandations pour optimiser votre utilisation des ressources AWS      
  Ce service permet d'éviter le surprovisionnement ou le sousprovisionnement pour trois types de ressources AWS : les types d'instances Amazon Elastic Compute Cloud (EC2), les volumes Amazon Elastic Block Store (EBS) et les fonctions AWS Lambda, en se basant sur vos données d'utilisation        

  ➡️ redimensionne les workload (basé sur le machine learning) ➡️ reduction des coûts jusqu'à 25%       
  ➡️ resoud les pb de perf en mettant en place les recommandations qui identifient les ressources sous-provisionnées      
  ➡️ active la visibilité de l'utilisation mémoire via les metriques dans cloudWatch      
  ➡️ crée des graphes qui permettent une visibilité sur l'historique et une projection sur l'avenir    

  Par défaut ce service est disabled      


**AWS SAVING PLANS**          
  Les Savings Plans sont des modèles de tarification flexibles qui peuvent vous permettre de réduire votre facture de 72 % comparé aux tarifs à la demande, en échange d'un engagement d'un ou trois ans de consommation horaire    

  Il existe 3 types de saving plans:          
   - **Compute saving plans**     
    ➡️ le + flexible             
    ➡️ applicable pour tout usage EC2, Lambda, Fargate          
    ➡️ jusqu'à 66% d'économie sur Compute        


   - **EC2 Instance saving plans**      
    ➡️ saving plan strict    
    ➡️ applicable pour tout usage sur des instances EC2 spécifiques/ famille d'instances spécifiques/ region specifiques    
    ➡️ jusqu'à 72% d'économie             


   - **Amazon SageMaker saving plans**     
    ➡️ applicable pour les instances Sagemaker concernant les famille d'instances ou le sizing      
    ➡️ n'importe où et n'importe quel component         
    ➡️ jusqu'à 64% d'économie          

  _Comment utiliser ces savings plans ?_            
    - afficher les recommandations dans la partie facturation de votre console aws              
    - des recommandations sont calculées automatiquement pour faciliter l'achat           
    - ajouter au panier et acheter directement dans votre compte aws          
    - s'appliquent aux tarifs d'utilisation après l'application et l'épuisement des instances réservées         
    - famille de facturation consolidée : appliquée d'abord au propriétaire du compte, puis peut être étendue à l'autre       


**AWS TRUSTED ADVISOR**           
  Ce service propose des recommandations qui vous permettent de suivre les bonnes pratiques AWS, évalue votre compte à l'aide de vérifications ➡️  ces contrôles vous aident à identifier des moyens d'optimiser votre infrastructure AWS, d'améliorer la sécurité et les performances, de réduire les coûts totaux et de surveiller les limites de service (Service Quotas)        

  Outil entièrement managé d'audit           

  Il scanne 5 parties de votre account          
   - **cost optimization** ➡️ dépensez-vous pour des ressources non nécessaires ?        
   - **performance** ➡️ vos services sont-ils correctement configurés ?      
   - **security** ➡️ votre archi aws a-t-elle des vulnérabilités ?       
   - **fault tolerence** ➡️ êtes-vous protéger dans le cas où quelchose plante ?      
   - **service limit** ➡️ avez-vous de place pour un scale (augmentation des rsources par exemple) ?      

   Génère des alertes (SNS vers un user par exemple)                     
   Utilise cloudWatch (eventBridge) pour déclencher des actions lambda pour corriger les erreurs               


**AWS CONTROL TOWER**               
  AWS Control Tower simplifie les expériences AWS en orchestrant plusieurs services AWS en votre nom tout en maintenant les besoins de sécurité et de conformité de votre organisation         
  
  Il permet de             
   - configurer une environnement multi accounts en moins de 30 minutes            
   - automatiser la creation de compte AWS avec une gouvernance integrée           
   - appliquer les bonnes pratiques, normes et exigences réglementaires (avec controles préconfigurés)         
   - intégrer les logiciels tiers (en transparence) dans AWS        

  Il possede **3 shared accounts**       
    - **management** ➡️ AWS Organizations ➡️ permet le déploiement de SCPs        
    - **log archive** ➡️ AWS S3 Bucket ➡️ permet de conserver les logs (mise en place avec AWS Config et AWS CloudTrail)     
    - **audit** ➡️ Amazon SNS ➡️ permet de faire des audits


**AWS LICENCE MANAGER**           
  ce service facilite pour vous la gestion des licences des logiciels existants en provenance de fournisseurs comme Microsoft, SAP, Oracle et IBM sur AWS et sur vos environnements sur site          
  Il centralise la gestion des licences pour les accounts AWS et on-premises        
  Il permet la mise en place d'une limitation d'usage (et permet le controle et le visibilité de ces licences)      
  Il permet une reduction des coûts avec la mise en place de regles de controle de consommation des licences        


**AWS HEALTH**      
  Ce service permet de consulter les événements et les changements importants qui affectent votre environnement AWS      
  Il est possible d'automatiser des actions "préventives" avec Amazon EventBridge     

  Les 8 concepts de AWS Health     
   - **AWS Health event** ➡️ envoie de notifications au nom des services AWS         
   - **account-specific event** ➡️ evènement spécifique pour pour aws accounts ou votre OU (Organization Unit)      
   - **public event** ➡️ rapport des evènements des services publiques (non spécifiques à votre account)      
   - **AWS Health Dashboard** ➡️ dashboard montrant la santé des services et les accounts et evènements publiques       
   - **event type code** ➡️ inclus les services affectés et les types d'évènements spécifiques      
   - **event type category** ➡️ associe les catégories qui seront attached à chaque évènement      
   - **event status** ➡️ rapport si un evèneement est ouvert, fermé, entrant      
   - **affected entities** ➡️ quelles ressources AWS sont ou peuvent être affectées par un évènement       


**AWS SERVICE CATALOG**       
  Ce service permet de gérer de manière centralisée les services informatiques, les applications, les ressources et les métadonnées déployés afin de parvenir à une gouvernance cohérente de vos modèles d'infrastructure en tant que code (IaC)                    
  Il fournit une liste de services validés par votre organization qui pourra être déployées facilement par chaque utilisateur final de manière autonome     
  Il permet la mise à jour transparante du service de chque utilisateur finale (la nouvelle version validée est déployée dans Service catalog et l'upgrade se fait tout seul)     
  Il permet la standardisation des applications pour l'entreprise    
  Les accès aux applications déployées via Service Catalog sont gérés via IAM       


**AWS PROTON**          
  Ce service est un outil de flux de travail de déploiement pour les applications modernes, destiné à aider les ingénieurs de plateforme et DevOps à stimuler leur agilité organisationnelle         


**AWS WELL-ARCHITECTURED TOOL: AWA Tool**      
  Ce service est un framework utilisé pour vérifier qu'on utilise bien toutes les best practices sur AWS pour les 6 piliers      



=======================================================================================================================  
### <span style="color: #EA9811">**MIGRATION**</span>

Le but d'une migration vers le cloud est d'optimiser les coûts et accélérer l'innovation      

Le processus de migration se fait en 3 phases:         
- évaluer          
- mobiliser           
- migrer et moderniser    


**AWS SNOW FAMILY**       
  Ce service est utiliser pour la migration de données (jusqu'à 1 trop volume: petaoctets) vers et depuis AWS (utilisé en cas de connexions internet lentes ➡️ serveur physique à mettre dans votre SI)    

  Se compose de 3 produits différents:       
  - **snowcone** ➡️ utilisé pour les petites sociétés ➡️ edge computing - jusqu'à 8Tb de data          
  - **snowball** ➡️ utilisé pour les moyennes et grandes sociétés ➡️ storage, compute, GPU - jusqu'à 81 Tb de data          
  - **snowmobile** ➡️ société géantes (exabytes) ➡️ semi-remorque datacenter ➡️ + de 100Pb     

  Il faut être membre Snow Family pour pouvoir utiliser ce service      
  Le temps moyen est 1 semaine en fonction des clients     


**STORAGE GATEWAY**       
  Permet d'avoir un accès à un storage cloud (securisé et quasi illimité) en utilisant les protocoles ISCSI, SMB et NFS depuis votre SI    

  Il fait une copie entre les data du on-prem et le cloud   ➡️ équivalent à 1 merge     
  La copie peut être complète OU seulement du cache (permet de conserver les données sensibles sur le on-prem)     
  ➡️ correspond à des VMs fournies par AWS dans votre ob-prem     

  Il fournit une aide à la migration     


**VOLUME GATEWAY**       
  Permet un montage de volume sécurisé entre le on-prem et AWS (ISCSI mount)     
  Conserve le mode de caches et de store       
  Crée des snapshots EBS   
  ➡️ parfait pour pour un backup ou une migration


**TAPE GATEWAY**          
  Remplace les cassettes physiques on-prem par des cassettes dans AWS (S3 Glacier / S3 Glacier Deep Archive)     
  Pas besoin de modifier le workflow de sauvegarde (redirection automatique vers AWS)      
  Possède une encryption lors de la communication entre le on-prem et AWS


**AWS DATA SYNC**        
  Simplifier et accélérer les migrations de données sécurisées          
  Data-sync est une solution agent-based pour la migration de on-premises vers AWS             
  Elle permet la migration des data entre NFS et SMB Shares vers des solutions de storage AWS             
  **Data-sync** est l'outil à utiliser pour une **migration en 1 seule fois**              
  **DataSync** est l'outil utilisé pour la migration de fichiers              

  Une fois l'agent installé dataSync va identifier la meilleure solution aws pour le stockage des datas    


**AWS TRANSFER FAMILY**           
  Met à l'échelle de manière sécurisée vos transferts de fichiers récurrents d'entreprise à entreprise vers les services de stockage AWS à l'aide des protocoles SFTP, FTPS, FTP et AS2           

  La gestion du endpoint est prise en charge ➡️ pas de nécessité de modifier du code (on conserve le endpoint on-prem mais le transfert se fait correctement vers du S3 ou de l'EFS)     

  Pas de changements ➡️ conserve le legacy   


**AWS MIGRATION HUB**        
  Fournit un emplacement central pour recueillir les données d'inventaire des serveurs et des applications pour l'évaluation, la planification et le suivi des migrations vers AWS     

  **AWS Migration Hub** s'intègre avec **Server Migration Service (SMS)** et **Database Migration Service (DMS)**         

  _Server Migtaion Service: SMS_    
  Copie de toute l'infra on-prem pour créer des AMIs à l'identique de VMs          


  _Database Migration Service: DMS_         
  2 cas possibles      
  - migre toutes les formes de DB dans de l'**Aurora** ou **RDS** ➡️ **AWS Schema Conversion Tool**         
  - migre plusieurs DB en 1 seule ➡️ **AWS Databse Migration Service**


**APPLICATION DISCOVERY SERVICE**         
  Aide à planifier la migration vers le cloud de vos projets en recueillant des informations sur vos data centers on-premises     

  Il integre Migration Hub pour simplifier la migration et tracquer les status de migration          
  Il permet la découverte des serveurs, les regroupe par application et traque chaque application lors de la migration       

  Il fonctionne suivant 2 modes:      
  _Agentless_     
  Aucun dépot d'agent n"cessaire      
  CRée une collecte de toute l'infra (IP, MAC Address, resource allocations et hostname) vi le depot d'un **fichier OVA** dans vCenter    ➡️ cela correspond à une VM créée par ce fichier qui va scanner et remonter toutes les datas de l'infra       
  Identifie les hosts et les VM      
  Récupère des metriques      

  _Agent Based_        
  Déploie AWS Application Discovery Agent sur chaque VM et serveur physique (linux et/ou windows)        
  Récupère énormément de données (+ que par agentless): config, time-series performance, network connections, OS processes     



**APPLICATION MIGRATION SERVICE - MGN**       
  Limite les processus manuels chronophages et sujets à des erreurs en automatisant la conversion de vos serveurs sources afin qu'ils s'exécutent de manière native sur AWS           
  Permet également de simplifier la modernisation des applications grâce à des options d'optimisation intégrées et personnalisées.  

  Gère de manière autonome la transition de l'infra: cela consiste à faire une copie complète au format aws en atonomie     
  Il existe 2 modèles:       
  _RTO: Recovery Time Objective_: durée maximale fixée par votre entreprise pour restaurer les opérations normales après une panne ou une perte de données ➡️ dépned de l'OS Boot time                              

  _RPO: Recovery Point Objective_ : quantité maximale de données que votre entreprise peut tolérer de perdre ➡️ mesuré en sub-seconds                      


**DATABASE MIGRATION SERVICE - DMS**             
  Service géré de réplications et de migrations qui permet de déplacer, rapidement et sûrement, votre base de données et vos charges de travail d'analytique vers AWS et en assurant un temps d'interruption minime et aucune perte de données       

  _Outil de migration_       
    ➡️ permet la migration ed DB relationnelles, data warehouses, NoSQL, data stores     

  _Fait pour le cloud / on-prem_         
    ➡️ permet la migration de data entre AWS et on-prem         
    ➡️ les datas passeront TOUJOURS par aws ➡️ pas de on-prem vers on-prem ou on-prem vers 1 aautre cloud provider directement     

  _1-time ou OnGoing_          
    ➡️ option à choisir: migration en 1-time ou continue (avec repliques continues en fonction des modifs)     

  _Outil de conversion_        
    ➡️ **SCT: Schema Conversion Tool** autorise la translation des formats de databases vers des formats AWS (ex: Aurora)      

  _Outil de gains_              
    ➡️permet de faire des gains financiers, de sécurité, de qualité, de résilience....         

  _Fonctionnement de DMS_                 
    - serveur qui exécute 1 repilication         
    - crée 1 source et 1 cible avec leur connexion      
    - planifie les taches de migration des data            
    - crée les tables et clés primaires si elles n'existent pas (possibilité de les créer avant la migration si souhaité)            
    - permet au SCT de créer 1 à toutes les tables, indexes....         
    - les sources et cibles data store sont référencés comme des endpoints         

  _Types de migration DMS_                     
    DMS permet 2 types de migration   
      - moteurs identiques: on ne change pas le format de la DB (MySQl ➡️ MySQL)              
      - moteurs différents: format de DB changé (Oracle ➡️ PostGreSQL)    
    ➡️ OBLIGATOIRE d'avoir 1 endpoint chez AWS     
    Une migration **CDC (Change Data Capture)** a une intégrité de transition garantie


  _AWS SCT: Schema Conversion Tool_         
    peut convertir de nombreux formats de DB relationnelles ➡️ **OLAP** et **OLTP** + format data warehouses      
    peut convertir les formats des DB en exécution dans EC2 ou stocker dans S3        



=======================================================================================================================  
### <span style="color: #EA9811">**FRONTEND WEB AND MOBILE**</span>

Des services entièrement managés existent chez AWS pour le développement, déploiement, test, surveillance et engagement d'application web et mobile     

**Amplify**
  Solution de developpement et deploiement    
  2 services sont proposés    
  - **hosting** ➡️ utilise un framework React, Angular ou Vue, gère les environnements (dev/staging/prod), permet du server-side avec Next.js            
  - **studio** ➡️ permet uen authentification des des authorisations facilitées pour l'implementation, simplifie le dev avec une solution graphique, et prêt à l'emploinavec une creation automatisées de backend et des connexions entre back et frontend     

**Device Farm**      
  Solution de tests         
  2 formats de tests        
  - **automated** ➡️ upload des scripts qui vont faire les tests      
  - **remote access** ➡️ qui permet de tester le comportement des app mobiles depuis un navigateur web    

**Pinpoint**       
  Outil de communications pour les clients        
  Permet la communication par divers canaux (mail, sms, notifications....)     
  Permet la mise en place de templates pour la communication, l'utilisation du machine learning avec l'utilisation de modeles...




=======================================================================================================================  
### <span style="color: #EA9811">**MACHINE LEARNING**</span>

Le machine learning (ML) et l'AI (Intelligence Artificicelle) sont présent pour accélérer l'innovation, transformer les datas en datas détaillées tout en réduisant les couts     


**AMAZON COMPREHEND**                           
  Extraire et comprendre des informations précieuses à partir du texte des documents           
  Comprehend est un **NLP: Natural Language Processing** ➡️ c'est un moyen d'automatiser la compréhension (tient compte des sentiements dans le langage écrit ➡️ ex: sait dire si c'est une phrase positive ou négative)           


**AMAZON KENDRA**              
  Permet la creation d'un service de recherche intelligente basée sur le Machine Learning            
  Si des datas sont stockées dans différents emplacements (S3, fichiers, web sites) ➡️ kendra va permettre l'indexation des datas et permettre une recherche intelligente et rapide                      


**AMAZON TEXTRACT**             
  Extraire automatiquement du texte imprimé ou manuscrit ainsi que des données de pratiquement n'importe quel document         
  Se base sur l'**OCR: Optical Character Recongnition**           
  Permet d'extraire des datas d'un pdf, tableau, documents manuscrit.....               


**AMAZON FORECAST**           
  Service de prédiction de séries temporelles basé sur le machine learning (ML) et conçu pour l'analyse des métriques métier      


**AMAZON FRAUD DETECTOR**          
  Service entièrement géré permettant aux clients d'identifier les activités potentiellement frauduleuses et d'intercepter plus rapidement les fraudes en ligne            
  Créez, déployez et gérez des modèles de détection des fraudes sans expérience préalable du machine learning (ML)         


**AMAZON POLLY**          
  Déployer des voix humaines de haute qualité et naturelles dans des dizaines de langues              
  Permet de mettre des textes en audio avec des accents, intonations, plus ou moins fort.... ➡️ cela rend plus interactifs le contenu    


**AMAZON TRANSCRIBE**        
  Convertir un discours en texte           


**AMAZON LEX**                
  Créer des chatbots et des voicebots grâce à l'IA conversationnelle             
  Alexa d'Amazon utilise Lex       


**AMAZON REKOGNITION**              
  Utilisé pour la reconnaisance faciale et utilise le deep learning avec les reseaux neuronaux                    
  Ajout d'APIs de reconnaissance d'images           
  Identifier rapidement du contenu dangereux ou innapproprié    


**AMAZON SAGEMAKER**            
  Créer, entraîner et déployer rapidement et facilement des modèles de machine learning (ML) pour tous les cas d'utilisation avec une infrastructure, des outils et des flux entièrement gérés     
  Peut être utilisé en offline ou online     
   ➡️ quelques différences se présentent dans ces cas  (offline: usage asynchrone ou batch / online: usage synchrone ou temps réel ....)  
            
  _Ground truth_         
   configure et gère les jobs de labllisation       

  _Notebook_           
   accès à un environnement managé Notebook Jupyter     

  _Training_         
   Entrainement et configuration de modeles         

  _Inference_             
   package et deploie les modeles de machine learning   

  Les étapes de creation d'1 modele Sagemaker:         
  1. créer un modele         
  2. créer une configuration endpoint         
  3. créer un endpoint   

  Sagemaker permet la haute dispo et possede l'autoscaling                
  Elastic inference permet de diminuer les couts en optimisants les CPU/GPU           


**AMAZON TRANSLATE**          
  Traduction automatique fluide et précise        


=======================================================================================================================  
### <span style="color: #EA9811">**MEDIA**</span>

AWS offre les services, les logiciels et les appareils multimédias les plus spécifiquement conçus pour faciliter et accélérer la création, la transformation et la diffusion de contenu numérique         

**Elastic Transcoder**        
  Outil permettant de convertir ou transcoder un media dans son format original en un format optimisé pour les smartphones, tablettes ou ordi


**Kinesis Video Streams**       
  Capturer, traiter et stocker des flux multimédia pour la lecture, les analyses et le machine learning
   


=======================================================================================================================  
</span></div> 
_______________________________________________________________________________________________________________________  
