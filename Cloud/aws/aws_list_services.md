# <span style="color: #CE5D6B"> **AWS LIST OF SERVICES** </span>



Le but de cette documentaztion est de lister une partie des services et de résumé leurs action, fonction

Pour connaitre l'ensemble des services accessibles chez aws, se connecter à la console puis aller sur le lien suivant: https://eu-north-1.console.aws.amazon.com/console/services?region=eu-north-1 


![aws_listOfServices](./images/aws_solutionArchitectAssociate/aws_listOfServices/awsConsole_ListOfServices.png)          


Tous les services ne seront pas décrits dans cette documentation mais les plus utilisés

_______________________________________________________________________________________________________________________    


<span style="color: #CE5D6B">

##  **SOMMAIRE** 
</span>


1. [EC2](./aws_list_services.md#ec2-elastic-compute-cloud)         
2. [EC2 auto-scaling](./aws_list_services.md#amazon-ec2-auto-scaling)         
3. [EBS](./aws_list_services.md#amazon-ebs-elastic-block-store)      
4. [EFS](./aws_list_services.md#amazon-efs-elastic-file-system)         
5. [Lambda](./aws_list_services.md#amazon-lambda)         
6. [Step Functions](./aws_list_services.md#aws-step-functions)         
7. [S3](./aws_list_services.md#amazon-s3)         
8. [S3 Glacier](./aws_list_services.md#amazon-s3-glacier)         
9. [Cloud Directory](./aws_list_services.md#amazon-cloud-directory)         
10. [RDS](./aws_list_services.md#amazon-rds)         
11. [Aurora](./aws_list_services.md#aws-aurora)         
12. [DynamoDb](./aws_list_services.md#amazon-dynamodb)         
13. [Athena](./aws_list_services.md#amazon-athena)         
14. [Elasticache](./aws_list_services.md#amazon-elasticache)         
15. [Route53](./aws_list_services.md#amazon-route53-dns)         
16. [ELB](./aws_list_services.md#amazon-elb--elastic-load-balancer)         
17. [NLB](./aws_list_services.md#amazon-nlb--network-load-balancer)         
18. [VPC](./aws_list_services.md#amazon-vpc-virtual-private-cloud)         
19. [API Gateway](./aws_list_services.md#amazon-api-gateway)         
20. [PrivateLink](./aws_list_services.md#amazon-private-link)         
21. [Direct Connect](./aws_list_services.md#aws-direct-connect)      
22. [Transit Gateway](./aws_list_services.md#aws-transit-gateway)      
23. [SNS](./aws_list_services.md#amazon-sns-simple-notification-service)      
24. [SQS](./aws_list_services.md#amazon-sqs-simple-queue-service)      
25. [IAM](./aws_list_services.md#aws-iam-identity-and-access-management)      
26. [Organizations](./aws_list_services.md#aws-organizations)      
27. [Firewall Manager](./aws_list_services.md#aws-firewall-manager)      
28. [KMS](./aws_list_services.md#aws-kms-key-management-service)      
29. [Cognito](./aws_list_services.md#amazon-cognito)      
30. [Inspector](./aws_list_services.md#amazon-inspector)      
31. [Guard Duty](./aws_list_services.md#amazon-guard-duty)      
32. [Security Hub](./aws_list_services.md#aws-security-hub)      
33. [Macie](./aws_list_services.md#amazon-macie)      
34. [ECR](./aws_list_services.md#amazon-ecr-elastic-container-registry)      
35. [ECS](./aws_list_services.md#amazon-ecs--elastic-container-service)      
36. [EKS](./aws_list_services.md#amazon-eks--elastic-kubernetes-service)      
37. [Fargate](./aws_list_services.md#aws-fargate)      
38. [CloudWatch](./aws_list_services.md#amazon-cloudwatch)      
39. [CloudTrail](./aws_list_services.md#aws-cloudtrail)      
40. [Elastic Search](./aws_list_services.md#amazon-elastic-search)      
41. [Kinesis](./aws_list_services.md#amazon-kinesis)      
42. [AppFlow](./aws_list_services.md#amazon-appflow)      
43. [CloudFront](./aws_list_services.md#amazon-cloudfront)      
44. [LightSail](./aws_list_services.md#amazon-lightsail)      
45. [BeanStalk](./aws_list_services.md#aws-elastic-beanstalk)      
46. [Glue](./aws_list_services.md#aws-glue)      
47. [Chime](./aws_list_services.md#amazon-chime)      
48. [SageMaker](./aws_list_services.md#amazon-sagemaker)      
49. [CloudFormation](./aws_list_services.md#aws-cloud-formation)      
50. [Redshift](./aws_list_services.md#amazon-redshift)     


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **EC2: ELASTIC COMPUTE CLOUD** 
</span>


**logo**: ![aws_ec2](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_ec2.png)    

**lien documentation**: https://aws.amazon.com/fr/ec2           

**action**: Capacité de calcul sécurisée et redimensionnable pour pratiquement toutes les charges de travail  


**resumé**: Amazon EC2 est l'un des services de cloud computing AWS qui propose des serveurs virtuels pour gérer tout type de charge de travail. Il facilite l'infrastructure informatique avec les processeurs, les installations réseau et les systèmes de stockage les mieux adaptés. En conséquence, il permet de s’adapter précisément aux charges de travail. Amazon EC2 fournit une infrastructure informatique hautement sécurisée, fiable et performante qui répond aux exigences des entreprises. Cela aide à accéder rapidement aux ressources et à faire évoluer les capacités de manière dynamique en fonction des demandes.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON S3** 
</span>

**logo**: ![aws_s3](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_s3.png)        

**lien documentation**: https://aws.amazon.com/fr/s3       

**action**: Stockage d'objets conçu pour extraire n'importe quelle quantité de données, depuis n'importe où      


**resumé**: Amazon S3 est un service AWS de stockage d'objets hautement évolutif. Il aide principalement les utilisateurs à accéder à n’importe quelle quantité de données depuis n’importe où. Les données sont stockées dans des « classes de stockage » pour réduire les coûts sans aucun investissement supplémentaire et les gérer confortablement, elles sont hautement sécurisées et répondent aux exigences d’audit et de conformité. Vous pouvez gérer n'importe quel volume de données grâce aux contrôles d'accès robustes, aux outils de réplication et à une visibilité accrue d'Amazon S3. De plus, il prend en charge le maintien des contrôles de version des données et la prévention des suppressions accidentelles


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS AURORA** 
</span>

**logo**: ![aws_aurora](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_aurora.png)    

**lien documentation**: https://aws.amazon.com/fr/rds/aurora/         

**action**: Conçu pour un niveau inégalé de performances et de disponibilité à l'échelle mondiale avec une compatibilité complète avec MySQL et PostgreSQL    


**resumé**:  Amazon Aurora est une base de données relationnelle compatible MySQL et PostgreSQL avec de hautes performances. Elle est 5 fois plus rapide que les bases de données MySQL standard. Et elle permet d'automatiser des tâches cruciales telles que l'approvisionnement en matériel, la configuration et les sauvegardes de bases de données, ainsi que l'application de correctifs. Amazon Aurora est un système de stockage distribué, tolérant aux pannes et à réparation automatique, qui peut évoluer automatiquement en fonction des besoins. En outre, vous pouvez même réduire considérablement les coûts et améliorer la sécurité, la disponibilité et la fiabilité des bases de données


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON DYNAMODB** 
</span>

**logo**: ![aws_dynamoDb](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_dynamoDb.png)    

**lien documentation**: https://aws.amazon.com/fr/dynamodb/         

**action**: Service de base de données NoSQL rapide et flexible pour des performances de latence de l'ordre de quelques millisecondes à n'importe quelle échelle    


**resumé**: DynamoDB est un service AWS de base de données NoSQL entièrement géré et serverless. Et il s'agit d'un système de base de données rapide et flexible qui offre des opportunités innovantes aux développeurs à faible coût. Il vous offre des performances en millisecondes à un chiffre avec un débit et un stockage illimités. DynamoDB dispose d'outils intégrés pour générer des informations exploitables, des analyses utiles et surveiller les tendances du trafic dans les applications


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON RDS** 
</span>

**logo**: ![aws_rds](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_rds.png)    

**lien documentation**: https://aws.amazon.com/fr/rds/           

**action**: Configurez, exploitez et mettez à l'échelle une base de données relationnelle dans le cloud en quelques clics    


**resumé**: Amazon RDS est le service AWS (RDS) de base de données relationnelle géré pour MySQL, PostgreSQL, Oracle, SQL Server et MariaDB. Il permet la configuration, l'exploitation et la mise à l'échelle d'une base de données relationnelle dans le cloud rapidement. Il atteint des performances élevées en automatisant les tâches telles que l'approvisionnement en matériel, la configuration de la base de données, l'application des correctifs et les sauvegardes. Lorsque vous utilisez Amazon RDS, vous n'avez pas besoin d'installer et de maintenir le logiciel de base de données. Dans l'ensemble, vous pouvez optimiser les coûts en adoptant ce service et obtenir une haute disponibilité, sécurité et compatibilité pour vos ressources


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON LAMBDA** 
</span>

**logo**: ![aws_lambda](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_lambda.png)    

**lien documentation**: https://aws.amazon.com/fr/lambda         

**action**: Exécuter du code sans vous soucier des serveurs ou des clusters    


**resumé**: Amazon Lambda est un service AWS informatique serverless basé sur les événements. Cela permet d'exécuter des codes automatiquement sans se soucier des serveurs et des clusters. En termes simples, les codes peuvent être téléchargés directement pour être exécutés sans se soucier du provisionnement ou de la gestion de l'infrastructure. Ainsi, ce service accepte automatiquement les « demandes d'exécution de code », quelle que soit son ampleur. Vous ne payez le prix que pour le temps calculé, ce qui permet à AWS Lambda de contrôler efficacement les coûts


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON VPC: VIRTUAL PRIVATE CLOUD** 
</span>

**logo**: ![aws_vpc](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_vpc.png)    

**lien documentation**: https://aws.amazon.com/fr/vpc        

**action**: Définir et lancer des ressources AWS dans un réseau virtuel logiquement isolé   


**resumé**: Amazon VPC est le Virtual Private Cloud, qui est une ressource cloud isolée. Il contrôle l'environnement de réseau virtuel, tel que le placement des ressources, la connectivité et la sécurité. Et il permet de créer et de gérer des réseaux VPC compatibles sur les ressources cloud AWS et les ressources sur site. Il améliore la sécurité en appliquant des règles pour les connexions entrantes et sortantes. Il surveille les journaux de flux VPC fournis à Amazon S3 et Amazon Cloudwatch pour gagner en visibilité sur les dépendances du réseau et les modèles de trafic. Amazon VPC détecte également les anomalies dans les modèles, empêche les fuites de données et résout les problèmes de connectivité et de configuration du réseau


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON CLOUDFRONT** 
</span>

**logo**: ![aws_cloudfront](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_cloudfront.png)    

**lien documentation**: https://aws.amazon.com/fr/cloudfront         

**action**: Diffuser du contenu en toute sécurité avec une faible latence et des vitesses de transfert élevées   


**resumé**: Amazon CloudFront fournit du contenu à l'échelle mondiale, offrant des performances et une sécurité élevées. Principalement, il fournit des données à haute vitesse et à faible latence. Le contenu est livré avec succès vers les destinations grâce à un mappage de réseau automatisé et à des mécanismes de routage intelligents. La sécurité des données est renforcée par des méthodes de cryptage du trafic et des contrôles d'accès. Les données peuvent être transférées en quelques millisecondes grâce à sa compression de données intégrée, ses capacités informatiques de pointe et son cryptage au niveau du champ. Vous préparez la diffusion de vidéos de haute qualité à l'aide des services multimédias AWS sur n'importe quel appareil de manière rapide et cohérente à l'aide d'Amazon CloudFront.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS ELASTIC BEANSTALK** 
</span>

**logo**: ![aws_beanstalk](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_beanstalk.png)    

**lien documentation**: https://aws.amazon.com/fr/elasticbeanstalk         

**action**: Déployer et mettre à l'échelle des applications web   


**resumé**: Ce service AWS prend en charge l'exécution et la gestion d'applications Web. Elastic Beanstalk permet de déployer facilement des applications depuis le provisionnement de capacité, le load-balacing et l'auto-scaling jusqu'à la surveillance de l'état des applications. Grâce à ses propriétés d'auto-scaling, ce service simplifie les demandes d'auto-scaling pour s'adapter aux besoins de l'entreprise. Il permet de gérer les pics de charge de travail et de trafic avec un minimum de coûts. Fondamentalement, AWS Elastic Beanstalk est un outil convivial pour les développeurs car il gère simplement les serveurs, les load balancers, les pare-feu et les réseaux. En conséquence, ce service permet aux développeurs de se concentrer beaucoup plus sur le codage


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON EC2 AUTO-SCALING** 
</span>

**logo**: ![aws_ec2_auto-scaling](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_ec2_auto-scaling.png)    

**lien documentation**: https://aws.amazon.com/fr/ec2/autoscaling         

**action**: Ajoutez ou supprimez de la capacité de calcul pour répondre à l'évolution de la demande   


**resumé**: Ce service AWS adapte la capacité de calcul pour répondre avec précision aux demandes. Et cela est réalisé en ajoutant ou en supprimant automatiquement des instances EC2. Il existe deux types d'auto-scaling, à savoir la mise à l'échelle dynamique et la mise à l'échelle prédictive. Ici, la mise à l'échelle dynamique répond aux demandes actuellement changeantes, tandis que la mise à l'échelle prédictive répond sur la base de prédictions. Grâce à l'auto-scaling automatique Amazon EC2, vous pouvez identifier les instances EC2 défectueuses, les résilier et les remplacer par de nouvelles instances


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ELASTICACHE** 
</span>

**logo**: ![aws_elasticache](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_elasticache.png)    

**lien documentation**: https://aws.amazon.com/fr/elasticache         

**action**: Des performances en temps réel pour des applications en temps réel   


**resumé**: Amazon ElastiCache est un service AWS de mise en cache en mémoire entièrement géré et flexible. Il prend en charge l’augmentation des performances de vos applications et de votre base de données. Il permet de réduire la charge dans une base de données en mettant en cache les données en mémoire. Amazon ElastiCache accède aux données en mémoire avec une vitesse élevée, une latence de l'ordre de la microseconde et un débit élevé. Avec un service de cache autogéré, vous pouvez réduire les coûts et éliminer les frais opérationnels de votre entreprise


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON S3 GLACIER** 
</span>

**logo**: ![aws_s3_glacier](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_s3_glacier.png)    

**lien documentation**: https://aws.amazon.com/fr/s3/storage-classes/glacier        

**action**: Classes de stockage à long terme, sûres et durables pour l'archivage des données au moindre coût et un accès en quelques millisecondes   


**resumé**: Amazon S3 Glacier est le stockage d'archives dans le cloud à faible coût. Il est construit avec trois classes de stockage telles que la récupération instantanée S3 Glacier, la récupération flexible et l'archivage approfondi. La classe instantanée prend en charge un accès immédiat aux données, et la classe flexible permet un accès flexible en quelques minutes ou quelques heures sans frais. Le troisième, l’archivage profond, permet d’archiver les données de conformité et les médias numériques. Dans l’ensemble, ils vous aident à accéder plus rapidement aux données des archives


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON LIGHTSAIL** 
</span>

**logo**: ![aws_lightsail](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_lightsail.png)    

**lien documentation**: https://aws.amazon.com/fr/lightsail         

**action**: Créer rapidement des applications et des sites web grâce à des ressources cloud préconfigurées et peu coûteuses   


**resumé**: Amazon Lightsail est le site Web et les applications créant le service AWS. Ce service propose des instances de serveur privé virtuel, des conteneurs, des bases de données et du stockage. Il permet un service informatique serverless avec AWS Lambda. Avec Amazon Lightsail, vous pouvez créer des sites Web à l'aide d'applications préconfigurées telles que WordPress, Magento, Prestashop et Joomla en quelques clics et à faible coût. En plus de cela, c'est le meilleur outil de test, vous pouvez donc créer, tester et supprimer des sandbox avec votre nouvelle idée


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON SAGEMAKER** 
</span>

**logo**: ![aws_sagemaker](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_sagemaker.png)    

**lien documentation**: https://aws.amazon.com/fr/sagemaker         

**action**: Créez, entraînez et déployez rapidement et facilement des modèles de machine learning (ML) pour tous les cas d'utilisation avec une infrastructure, des outils et des flux entièrement gérés   


**resumé**: Amazon Sagemaker est le service AWS qui permet de créer, de former et de déployer des modèles de Machine Learning (ML) à grande capacité. Il s'agit d'un outil analytique qui fonctionne sur la base de la puissance du Machine Learning pour analyser les données plus efficacement. Grâce à son ensemble d'outils unique, vous pouvez créer rapidement des modèles ML de haute qualité. Amazon Sagemaker génère non seulement des rapports, mais permet également de générer des prédictions. De plus, Amazon Ground Truth Plus crée des ensembles de données sans étiqueter les applications


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON SNS: SIMPLE NOTIFICATION SERVICE** 
</span>

**logo**: ![aws_sns](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_sns.png)    

**lien documentation**: https://aws.amazon.com/fr/sns         

**action**: Service Pub/Sub entièrement géré pour la messagerie A2A et A2P   


**resumé**: Service de notification simple Amazon (SNS). Il s'agit d'un service de messagerie entre Application to Application (A2P) et Application to Person (A2Person). A2P prend en charge la messagerie plusieurs-à-plusieurs entre les systèmes distribués, les microservices et les applications sans serveur pilotées par événements. De plus, A2P prend en charge les applications permettant d'envoyer des messages à de nombreux utilisateurs par courrier, SMS, etc. Par exemple, vous pouvez envoyer jusqu'à dix messages dans une seule requête API. Grâce à des systèmes de filtrage efficaces, les abonnés recevront les messages qui les intéressent. Amazon SNS travaille aux côtés d'Amazon SQS pour transmettre les messages avec précision et cohérence


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON SQS: SIMPLE QUEUE SERVICE** 
</span>

**logo**: ![aws_sqs](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_sqs.png)    

**lien documentation**: https://aws.amazon.com/fr/sqs        

**action**: Mise en file d'attente des messages entièrement gérée pour les micro-services, les systèmes distribués et les applications sans serveur   


**resumé**: Amazon SQS est un service de mise en file d'attente de messages entièrement géré. Il existe deux types de services de mise en file d'attente de messages : SQS Standard et SQS FIFO. Ici, la norme SQS offre des fonctionnalités telles qu'un débit maximal, une commande au mieux et une livraison rapide. Et SQS FIFO ne traite les messages qu'une seule fois dans le même ordre dans lequel ils ont été envoyés. Amazon SQS permet de découpler ou de faire évoluer les microservices, les systèmes distribués et les applications serverless. Il vous aide à envoyer, recevoir et gérer des messages en grand volume. De plus, il n’est pas nécessaire d’installer et de maintenir d’autres logiciels de messagerie, ce qui réduit considérablement les coûts. Enfin, la mise à l'échelle est effectuée rapidement et automatiquement dans ce service


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON EBS: ELASTIC BLOCK STORE** 
</span>

**logo**: ![aws_ebs](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_ebs.png)    

**lien documentation**: https://aws.amazon.com/fr/ebs         

**action**: Stockage par bloc haute performance et facile à utiliser à n'importe quelle échelle   


**resumé**: Amazon Elastic Block Store (EBS) est le service de stockage par blocs. Il prend en charge la mise à l'échelle des charges de travail hautes performances telles que les produits SAP, Oracle et Microsoft. Et il offre une meilleure protection contre les pannes jusqu'à 99,999 %. Il permet de redimensionner les clusters pour les moteurs d'analyse Big Data tels que Hadoop et Spark. Vous pouvez également créer des volumes de stockage, optimiser les performances de stockage et réduire les coûts. La gestion du cycle de vie d'Amazon EBS crée des politiques qui aident à créer et à gérer efficacement les sauvegardes


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON KINESIS** 
</span>

**logo**: ![aws_kinesis](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_kinesis.png)    

**lien documentation**: https://aws.amazon.com/fr/kinesis        

**action**: Collectez, traitez et analysez facilement les flux vidéo et de données en temps réel   


**resumé**: C'est le service AWS qui analyse la vidéo ainsi que les flux de données. Amazon Kinesis collecte, traite et analyse tous les types de données en streaming. Ici, les données peuvent être audio, vidéo, logs, flux de clics de sites Web et télémétrie IoT. Ensuite, il génère des informations en temps réel quelques secondes après l’arrivée des données. Avec l'aide d'Amazon Kinesis, vous pouvez diffuser et traiter très simplement une grande quantité de données en temps réel avec de faibles latences


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON EFS: ELASTIC FILE SYSTEM** 
</span>

**logo**: ![aws_efs](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_efs.png)    

**lien documentation**: https://aws.amazon.com/fr/efs         

**action**: Stockage de fichiers serverless et totalement élastique   


**resumé**: Amazon EFS est le système de fichiers entièrement géré pour Amazon EC2 (équivalent NFS sur AWS). Système de fichiers élastique simple et serverless. Vous pouvez créer et configurer des systèmes de fichiers sans provisionnement, déploiement, application de correctifs ni maintenance à l'aide d'Amazon EFS. Ici, les fichiers peuvent être ajoutés et supprimés selon les besoins de mise à l'échelle. Surtout, vous ne pouvez payer que pour l'espace utilisé, ce service contribue donc à réduire les coûts.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS IAM: IDENTITY AND ACCESS MANAGEMENT** 
</span>

**logo**: ![aws_iam](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_iam.png)    

**lien documentation**: https://aws.amazon.com/fr/iam         

**action**: Gérer de façon sécurisée les identités et l'accès aux services et ressources AWS   


**resumé**: Il s'agit du service de gestion des identités et des accès (IAM) proposé par AWS pour accéder en toute sécurité aux applications et aux ressources. Il régule l'accès à diverses ressources en fonction des rôles et des politiques d'accès ; en conséquence, vous pouvez obtenir un contrôle d’accès précis sur vos ressources. L'analyseur d'accès AWS IAM permet de rationaliser la gestion des autorisations en définissant, en vérifiant et en affinant. Le contrôle d'accès basé sur les attributs AWS IAM permet de créer des autorisations précises basées sur les attributs utilisateur tels que le service, le rôle, le nom de l'équipe, ... 


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON REDSHIFT** 
</span>

**logo**: ![aws_redshift](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_redshift.png)    

**lien documentation**: https://aws.amazon.com/fr/redshift         

**action**: Meilleur rapport qualité-prix pour l'entreposage de données dans le cloud    


**resumé**: (RDS énorme sous forme de data warehouse) Amazon Redshift est un service d'entreposage de données rapide, simple et économique. Il permet d'analyser toutes les données des bases de données opérationnelles, des data lakes, des data warehouses et des données tierces. Et Amazon Redshift permet d'analyser un grand volume de données et d'exécuter des requêtes analytiques complexes. Grâce à ses capacités d'automatisation, ce service augmente la vitesse des requêtes et offre le meilleur rapport qualité/prix


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON CLOUDWATCH** 
</span>

**logo**: ![aws_cloudwatch](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_cloudwatch.png)    

**lien documentation**: https://aws.amazon.com/fr/cloudwatch        

**action**: Observez et surveillez les ressources et les applications sur AWS, sur site et sur d'autres clouds   


**resumé**: Ce service AWS surveille attentivement les ressources et les applications cloud. Il s'agit d'une plate-forme unique qui permet de surveiller toutes les ressources et applications AWS ; cela augmente la visibilité pour répondre rapidement aux problèmes. Amazon Cloudwatch fournit principalement des informations exploitables pour optimiser les applications de surveillance, les changements de performances à l'échelle du système et l'utilisation des ressources. Vous pouvez obtenir une vue complète de l'état des ressources, des applications et des services AWS exécutés sur AWS et sur site. De plus, Amazon Cloudwatch permet de détecter les anomalies dans le comportement de l'environnement cloud, de définir des alarmes, de visualiser les journaux et les métriques, d'effectuer des actions automatisées, de résoudre les problèmes et de découvrir des informations


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS CLOUDTRAIL** 
</span>

**logo**: ![aws_cloudtrail](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_cloudtrail.png)    

**lien documentation**: https://aws.amazon.com/fr/cloudtrail         

**action**: Suivez l'activité des utilisateurs et l'utilisation des API sur AWS et dans des environnements hybrides et multicloud   


**resumé**: AWS CloudTrail est un service AWS qui vous aide à activer l'audit opérationnel et des risques, la gouvernance et la conformité de votre compte AWS. Les actions entreprises par un utilisateur, un rôle ou un service AWS sont enregistrées en tant qu'événements dans CloudTrail


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON CHIME** 
</span>

**logo**: ![aws_chime](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_chime.png)    

**lien documentation**: https://aws.amazon.com/fr/chime         

**action**: Travailler ensemble, plus facilement   


**resumé**: (Equivalent Mirosoft teams) Amazon Chime est un service de communication. Solution unique qui offre des fonctionnalités d'appel audio, d'appel vidéo et de partage d'écran. Avec l'aide de ce service, vous pouvez organiser des réunions, des discussions et des appels vidéo de qualité à l'intérieur et à l'extérieur de votre organisation. Et davantage de fonctionnalités peuvent être ajoutées à ce service selon les besoins de votre entreprise. Principalement, vous pouvez définir des appels pendant une heure prédéfinie pour passer automatiquement des appels à l'heure. Amazon Chime vous aide à ne manquer aucune réunion malgré votre emploi du temps chargé au travail. Vous pouvez payer en fonction de l'utilisation des ressources, ce qui vous permet de réduire considérablement les coûts.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON COGNITO** 
</span>

**logo**: ![aws_cognito](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_cognito.png)    

**lien documentation**: https://aws.amazon.com/fr/cognito         

**action**: Mettez en œuvre une gestion des identités et des accès des clients sécurisée et transparente qui évolue   


**resumé**: Il s'agit du service AWS de gestion des identités. Amazon Cognito gère les identités pour accéder à vos applications et ressources. Principalement, ce service permet d'ajouter rapidement la connexion, l'inscription et le contrôle d'accès aux applications Web et mobiles. Il peut aider des millions d'utilisateurs à se connecter avec des applications familières telles que Apple, Facebook, Google et Amazon. Dans Amazon Cognito, la fonctionnalité « Groupes d'utilisateurs Cognito » peut être configurée rapidement sans aucune infrastructure, et les membres du pool auront un profil d'annuaire. Il prend en charge l'authentification multifacteur et le chiffrement des données au repos et des données en transit.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON INSPECTOR** 
</span>

**logo**: ![aws_inspector](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_inspector.png)    

**lien documentation**: https://aws.amazon.com/fr/inspector          

**action**: Une gestion des vulnérabilités automatisée et continue à grande échelle   


**resumé**: Amazon Inspector est un service automatisé de gestion des vulnérabilités. Ce service offre une gestion continue et automatisée des vulnérabilités pour Amazon EC2 et Amazon ECR. Il permet d'analyser les charges de travail AWS à la recherche de vulnérabilités logicielles et d'expositions réseau indésirables. Amazon Inspector identifie rapidement les vulnérabilités, ce qui permet de prendre des mesures immédiates pour les résoudre avant que cela ne détériore les applications. De plus, il permet de répondre aux exigences de conformité et réduit les vulnérabilités en attendant la correction. Et il vous fournit des scores de risque précis et un flux de travail rationalisé


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS FIREWALL MANAGER** 
</span>

**logo**: ![aws_fw_manager](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_fw_manager.png)    

**lien documentation**: https://aws.amazon.com/fr/firewall-manager         

**action**: Configurer et gérer de manière centralisée les règles de pare-feu sur vos comptes   


**resumé**: C'est le service central de gestion des règles de pare-feu. Le gestionnaire de pare-feu prend en charge la gestion des règles de pare-feu pour toutes les applications et tous les comptes. Les règles de sécurité communes permettent de gérer les nouvelles applications incluses au fil du temps. Il s'agit d'une solution unique permettant de créer de manière cohérente des règles de pare-feu et des politiques de sécurité et de les mettre en œuvre dans l'ensemble de l'infrastructure. Le gestionnaire de pare-feu AWS vous aide à auditer les security groups VPC pour vérifier les exigences de conformité et à contrôler efficacement le trafic réseau.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON APPFLOW** 
</span>

**logo**: ![aws_appflow](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_appflow.png)    

**lien documentation**: https://aws.amazon.com/fr/appflow/           

**action**: Les données automatisées circulent entre les logiciels en tant que service (SaaS) et les services AWS   


**resumé**: Amazon Appflow est un service no-code qui permet l'intégration d'applications SaaS et de services AWS sans effort. Pour être plus précis, il automatise de manière sécurisée les flux de données intégrant des applications tierces et des services AWS sans utiliser de code. Vous pouvez transférer des données entre des applications SaaS telles que Salesforce, SAP, Zendesk, ... puisqu'Amazon Appflow peut être intégré à d'autres applications en quelques clics. En particulier, un grand volume de données peut être déplacé sans les diviser en lots grâce à ce service


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ROUTE53: DNS** 
</span>

**logo**: ![aws_r53](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_r53.png)    

**lien documentation**: https://aws.amazon.com/fr/route53          

**action**: Un moyen fiable et économique d'acheminer les utilisateurs finaux vers des applications Internet    


**resumé**: Il s'agit d'un service cloud évolutif de système de noms de domaine (DNS). Il permet aux utilisateurs finaux de se connecter à Amazon EC2, aux ELB (Elastic Load Balancers), aux S3 buckets et même en dehors d'AWS. Dans ce service, la fonctionnalité « Contrôleurs de récupération d'applications Route 53 » configure les vérifications de l'état DNS et permet de surveiller la capacité des systèmes à récupérer après une panne. Et le « flux de trafic Route 53 » permet de gérer le trafic à travers le monde à l'aide de méthodes de routage telles que le routage basé sur la latence, le géoDNS, la géoproximité et le round-robin pondéré


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS CLOUD FORMATION** 
</span>

**logo**: ![aws_cloudform](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_cloudform.png)    

**lien documentation**: https://aws.amazon.com/fr/cloudformation         

**action**: Accélérez le provisionnement cloud avec l'infrastructure en tant que code   


**resumé**: (Equivalent Terraform) Ce service AWS crée et gère des ressources avec des templates. Il s'agit d'une plate-forme unique capable de gérer tous les comptes AWS dans le monde. Il automatise la gestion des ressources avec l'intégration des services AWS et offre des contrôles de distribution et de gouvernance d'applications clé en main. De plus, AWS Cloud Formation peut automatiser, tester et déployer une infrastructure avec une intégration et une livraison continues. Et vous pouvez exécuter des applications directement depuis AWS EC2 vers des applications multirégionales complexes à l'aide de ce service.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS KMS: KEY MANAGEMENT SERVICE** 
</span>

**logo**: ![aws_kms](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_kms.png)    

**lien documentation**: https://aws.amazon.com/fr/kms/         

**action**: Créez et contrôlez les clés utilisées pour chiffrer ou signer numériquement vos données   


**resumé**: AWS KMS gère la création et le contrôle des clés de chiffrement. Cela signifie qu'AWS KMS crée des clés cryptographiques et contrôle leur utilisation dans diverses applications. Vous pouvez obtenir un service sécurisé et résilient en utilisant des modules matériels résilients pour protéger les clés. Ce service peut être intégré à AWS Cloudtrail pour fournir des journaux de toutes les utilisations de clés afin de répondre avec précision aux exigences de conformité et réglementaires


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON CLOUD DIRECTORY** 
</span>

**logo**: ![aws_clouddir](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_clouddir.png)    

**lien documentation**: https://aws.amazon.com/fr/cloud-directory          

**action**: Magasin de données hiérarchisées, entièrement géré, dans le cloud AWS   


**resumé**: Amazon Cloud Directory est un répertoire spécialisé basé sur des graphiques qui constitue un élément de base pour les développeurs. Il s'adapte automatiquement à des millions d'objets et fournit également un schéma extensible partagé avec plusieurs applications. Cela réduit le temps passé car il vous suffit de définir le schéma, de créer un répertoire et de le remplir en appelant l'API. Vous pouvez créer des répertoires pour diverses utilisations telles que des catalogues de cours, des organigrammes et des registres. De plus, avec Cloud Directory, il est possible d'augmenter la flexibilité de création de répertoires avec des hiérarchies à plusieurs dimensions. Vous pouvez également rechercher les objets parents le long d'une dimension sans créer plusieurs requêtes. Il est également intégré à AWS CloudTrail (enregistrez la date, l'heure et l'identité des utilisateurs qui accèdent aux données de l'annuaire) et au balisage des ressources (étiquetez vos répertoires et schémas)


_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

##  **AWS ORGANIZATIONS** 
</span>

**logo**: ![aws_organizations](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_organizations.png)    

**lien documentation**: https://aws.amazon.com/fr/organizations         

**action**: Gérez de façon centralisée votre environnement à mesure que vous faites évoluer vos ressources AWS   


**resumé**: AWS Organizations est un service de gestion de compte qui vous permet de gérer de manière centralisée plusieurs comptes AWS. Il vous permet de mieux répondre aux besoins budgétaires, de sécurité et de conformité en tant qu'administrateur d'une organisation.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ATHENA** 
</span>

**logo**: ![aws_athena](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_athena.png)    

**lien documentation**: https://aws.amazon.com/fr/athena         

**action**: Analyser des pétaoctets de données là où elles se trouvent, de manière simple et flexible   


**resumé**: Amazon Athena est un service de requêtes serverless qui vous permet d'interroger et d'analyser les données des S3 buckets à l'aide du SQL standard. Athena est facile à utiliser. Pointez simplement vos données dans Amazon S3, définissez le schéma et lancez l'interrogation à l'aide du SQL standard. La plupart des résultats sont fournis en quelques secondes. Avec Athena, vous n’avez pas besoin de tâches ETL complexes pour préparer vos données à l’analyse. Cela permet à toute personne possédant des compétences SQL d’analyser rapidement des ensembles de données à grande échelle.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ELASTIC SEARCH** 
</span>

**logo**: ![aws_elsearch](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_elsearch.png)    

**lien documentation**: https://aws.amazon.com/fr/opensearch-service/         

**action**: Débridez la recherche temps réel, le monitoring et l'analyse de données métier et opérationnelles, de manière sécurisée   


**resumé**: AWS Elasticsearch Service est un service entièrement géré qui vous permet de déployer et d'exécuter facilement Elasticsearch de manière rentable et à grande échelle. Le service prend en charge les API Elasticsearch open source, Kibana géré, l'intégration avec Logstash et d'autres services AWS, ainsi que des alertes et des requêtes SQL intégrées. 


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS GLUE** 
</span>

**logo**: ![aws_glue](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_glue.png)    

**lien documentation**: https://aws.amazon.com/fr/glue             

**action**: Découvrez, préparez et intégrez toutes vos données à n'importe quelle échelle   


**resumé**: AWS Glue est un service d'intégration de données permettant de découvrir et de combiner des données à des fins d'analyse, de ML ou de développement d'applications. Glue permet de découvrir et d'extraire des données de diverses sources, puis d'enrichir, de combiner ou de normaliser les données avant de les transférer vers des bases de données ou des lacs de données. Glue fournit un accès visuel (Glue Studio) et programmatique aux ingénieurs de données et aux développeurs ETL pour créer et surveiller les flux de travail ETL.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS STEP FUNCTIONS** 
</span>

**logo**: ![aws_stepFunction](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_stepFunction.png)    

**lien documentation**: https://aws.amazon.com/fr/step-functions         

**action**: Flux de travail visuels pour des applications distribuées   


**resumé**: AWS Step Functions est un orchestrateur de fonctions serverless qui vous permet de séquencer les fonctions AWS Lambda et plusieurs services AWS dans des applications critiques pour l'entreprise. Grâce à l'interface visuelle de Step Functions, vous pouvez créer et exécuter une série de flux de travail pilotés par des points de contrôle et des événements qui maintiennent l'état de l'application.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ECR: ELASTIC CONTAINER REGISTRY** 
</span>

**logo**: ![aws_ecr](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_ecr.png)    

**lien documentation**: https://aws.amazon.com/fr/ecr        

**action**: Stocker, partager et déployer facilement votre logiciel de conteneur où que vous soyez   


**resumé**: ECR est un registre de conteneurs entièrement gérés qui vous permet de stocker, gérer, partager et déployer vos images et artefacts de conteneurs n'importe où. Amazon ECR vous évite d'avoir à exploiter vos propres référentiels de conteneurs ou à vous soucier de l'infrastructure sous-jacente. Amazon ECR héberge vos images dans une architecture hautement disponible et hautes performances, vous permettant de déployer des images de manière fiable pour vos applications de conteneur


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ECS : ELASTIC CONTAINER SERVICE** 
</span>

**logo**: ![aws_ecs](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_ecs.png)    

**lien documentation**: https://aws.amazon.com/fr/ecs/        

**action**: Exécutez des conteneurs hautement sécurisés, fiables et pouvant être mis à l’échelle  


**resumé**: (Equivalent docker swarm) Amazon Elastic Container Service (Amazon ECS) est un service de gestion de conteneurs évolutif et rapide qui vous permet d'exécuter, d'arrêter et de gérer des conteneurs sur un cluster. Vous pouvez exécuter vos tâches et services sur une infrastructure serverless gérée par AWS Fargate. Alternativement, pour plus de contrôle sur votre infrastructure, vous pouvez exécuter vos tâches et services sur un cluster d'instances Amazon EC2 que vous gérez.


_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

##  **AMAZON EKS : ELASTIC KUBERNETES SERVICE** 
</span>

**logo**: ![aws_eks](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_eks.png)    

**lien documentation**: https://aws.amazon.com/fr/eks/        

**action**: Le moyen le plus sûr de démarrer, d'exécuter et de mettre à l'échelle Kubernetes   


**resumé**: Amazon Elastic Kubernetes Service (Amazon EKS) vous offre la flexibilité nécessaire pour démarrer, exécuter et faire évoluer des applications Kubernetes dans le cloud AWS ou sur site. Amazon EKS vous aide à fournir des clusters hautement disponibles et sécurisés et automatise les tâches clés telles que l'application de correctifs, le provisionnement de nœuds et la mise à jour.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS FARGATE** 
</span>

**logo**: ![aws_fargate](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_fargate.png)    

**lien documentation**: https://aws.amazon.com/fr/fargate         

**action**: Calcul serverless pour conteneurs   


**resumé**: AWS Fargate est un moteur de calcul serverless pour conteneurs qui fonctionne à la fois avec ECS et EKS. Fargate supprime le besoin de provisionner et de gérer des serveurs, vous permet de spécifier et de payer des ressources par application et améliore la sécurité grâce à l'isolation des applications dès la conception


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON API GATEWAY** 
</span>

**logo**: ![aws_apigw](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_apigw.png)    

**lien documentation**: https://aws.amazon.com/fr/api-gateway        

**action**: Créer, maintenir et sécuriser des APIs à n'importe quelle échelle   


**resumé**: Amazon API Gateway est un service entièrement géré qui permet aux développeurs de créer, publier, maintenir, surveiller et sécuriser des APIs à n'importe quelle échelle. À l'aide d'API Gateway, vous pouvez créer des API RESTful et des API WebSocket qui permettent une communication bidirectionnelle en temps réel des applications. API Gateway prend en charge les charges de travail conteneurisées et serverless, ainsi que les applications Web. API Gateway gère toutes les tâches impliquées dans l'acceptation et le traitement de centaines de milliers d'appels API simultanés, y compris la gestion du trafic, la prise en charge des CORS, l'autorisation et le contrôle d'accès, la limitation, la surveillance et la gestion des versions d'API


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON PRIVATE LINK** 
</span>

**logo**: ![aws_privateLink](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_privateLink.png)    

**lien documentation**: https://aws.amazon.com/fr/privatelink/        

**action**: Établissez la connectivité entre les VPC et les services AWS sans exposer les données à l'Internet   


**resumé**: AWS PrivateLink fournit une connectivité privée entre vos VPCs, vos services AWS et vos réseaux sur site, sans exposer votre trafic à l'Internet public. AWS PrivateLink vous permet de connecter des services sur différents comptes et VPC pour simplifier votre architecture réseau


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON NLB : NETWORK LOAD BALANCER** 
</span>

**logo**: ![aws_nlb](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_nlb.png)    

**lien documentation**: https://aws.amazon.com/fr/elasticloadbalancing/network-load-balancer         

**action**: Équilibrez la charge du protocole de contrôle de transmission, du protocole de datagramme utilisateur et du trafic de sécurité de la couche de transport avec des performances élevées  


**resumé**: Network Load Balancer fonctionne au niveau de la couche 4, acheminant les connexions vers des cibles (instances Amazon EC2, microservices et conteneurs) au sein d'Amazon VPC, sur la base des données du protocole IP. Idéal pour l'équilibrage de charge du trafic TCP et UDP, Network Load Balancer est capable de gérer des millions de requêtes par seconde tout en maintenant des latences ultra faibles. Network Load Balancer est optimisé pour gérer des modèles de trafic soudains et volatils tout en utilisant une seule adresse IP statique par zone de disponibilité. Il est intégré à d'autres services AWS populaires tels que Auto Scaling, Amazon EC2 Container Service (ECS), Amazon CloudFormation et AWS Certificate Manager (ACM)


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON ELB : ELASTIC LOAD BALANCER** 
</span>

**logo**: ![aws_elb](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_elb.png)    

**lien documentation**: https://aws.amazon.com/fr/elasticloadbalancing         

**action**: Répartissez le trafic de réseau pour améliorer la capacité de mise à l'échelle de vos applications   


**resumé**: Elastic Load Balancing (ELB) répartit automatiquement le trafic entrant d'applications sur plusieurs cibles et appliances virtuelles dans une, ou plus d'une, zones de disponibilité (AZ).


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS DIRECT CONNECT** 
</span>

**logo**: ![aws_dirco](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_dirco.png)    

**lien documentation**: https://aws.amazon.com/fr/directconnect         

**action**: Créer une connexion réseau dédiée vers AWS   


**resumé**: AWS Direct Connect est une solution de service cloud qui facilite l'établissement d'une connexion réseau dédiée depuis vos locaux vers AWS. Grâce à AWS Direct Connect, vous établissez une connexion privée entre AWS et votre centre de données, votre bureau ou votre environnement de colocation. Cela peut augmenter le débit de bande passante et offrir une expérience réseau plus cohérente que les connexions Internet


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS TRANSIT GATEWAY** 
</span>

**logo**: ![aws_trgw](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_trgw.png)    

**lien documentation**: https://aws.amazon.com/fr/transit-gateway         

**action**: Connectez facilement les VPC Amazon, les comptes AWS et les réseaux sur site sur une passerelle unique   


**resumé**: AWS Transit Gateway connecte les VPCs et les réseaux sur site via un hub central. Cela simplifie votre réseau et met fin aux relations de peering complexes. Il agit comme un routeur cloud : chaque nouvelle connexion n'est établie qu'une seule fois.


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON GUARD DUTY** 
</span>

**logo**: ![aws_gudty](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_gudty.png)    

**lien documentation**: https://aws.amazon.com/fr/guardduty        

**action**: Protéger vos comptes AWS avec la détection intelligente des menaces  


**resumé**: Amazon GuardDuty est un service de détection des menaces qui surveille en permanence vos charges de travail et vos comptes AWS pour détecter les activités malveillantes et fournir des constatations de sécurité détaillées pour la visibilité et la correction


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AWS SECURITY HUB** 
</span>

**logo**: ![aws_sechub](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_sechub.png)    

**lien documentation**: https://aws.amazon.com/fr/security-hub        

**action**: Automatiser les vérifications de sécurité AWS et centraliser les alertes de sécurité   


**resumé**: AWS Security Hub est un service de gestion de la posture de sécurité dans le cloud (CSPM) qui effectue des vérifications de bonnes pratiques, regroupe les alertes et permet l'utilisation de la correction automatisée


_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **AMAZON MACIE** 
</span>

**logo**: ![aws_macie](./images/aws_solutionArchitectAssociate/aws_listOfServices/logo_aws_macie.png)    

**lien documentation**: https://aws.amazon.com/fr/macie         

**action**: Identifier et protéger vos données sensibles à grande échelle   


**resumé**: Amazon Macie est un service de sécurité et de confidentialité des données qui utilise le machine learning (ML) et la correspondance des modèles pour découvrir et protéger vos données sensibles


_______________________________________________________________________________________________________________________  
