########## GEREZ VOTRE PROJET AVEC UNE EQUIPE SCRUM ##########


LES 3 PILIERS SCRUM


        1. Transparence
                -> Vous devez garantir que toutes les informations relatives à la bonne compréhension du projet sont bien communiquées aux membres de votre équipe et aux différentes parties prenantes.
                
                    => langage commun; management visuel qui propose des informations libres d’accès, pertinentes et utiles; communication bienveillante et conviviale

        2. Inspection
                -> Vous vérifiez à intervalles réguliers que le projet respecte des limites acceptables et qu’il n’y a pas de déviation indésirable par rapport à la demande de votre client.
         
        3. Adaptation
                -> Vous encouragez la correction des dérives constatées et proposez des changements appropriés afin de mieux répondre aux objectifs de votre gestion de projet.
                
                
LES 4 PRINCIPES SCRUM

        
        1. Les individus et leurs interactions plus que les processus et les outils.
        2. Un logiciel qui fonctionne plus qu’une documentation exhaustive.
        3. La collaboration avec les clients plus que la négociation contractuelle.
        4. L’adaptation au changement plus que le suivi d’un plan
        
        
LES 12 VALEURS SCRUM

        
        1. Notre plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée.
        2. Accueillez positivement les changements de besoins, même tard dans le projet. Les processus agiles exploitent le changement pour donner un avantage compétitif au client.
        3. Livrez fréquemment un logiciel opérationnel avec des cycles de quelques semaines à quelques mois et une préférence pour les plus courts.
        4. Les utilisateurs ou leurs représentants et les développeurs doivent travailler ensemble quotidiennement tout au long du projet.
        5. Réalisez les projets avec des personnes motivées. Fournissez-leur l’environnement et le soutien dont elles ont besoin et faites-leur confiance pour atteindre les objectifs fixés.
        6. La méthode la plus simple et la plus efficace pour transmettre de l’information à l'équipe de développement et à l’intérieur de celle-ci est le dialogue en face à face.
        7. Un logiciel opérationnel est la principale mesure d’avancement.
        8. Les processus agiles encouragent un rythme de développement soutenable. Ensemble, les commanditaires, les développeurs et les utilisateurs devraient être capables de maintenir indéfiniment un rythme constant.
        9. Une attention continue à l'excellence technique et à une bonne conception renforce l’agilité.
        10. La simplicité – c’est-à-dire l’art de minimiser la quantité de travail inutile – est essentielle.
        11. Les meilleures architectures, spécifications et conceptions émergent d'équipes auto-organisées.
        12. À intervalles réguliers, l'équipe réfléchit aux moyens de devenir plus efficace, puis règle et modifie son comportement en conséquence.
        
        
        
        
        
    => Adaptez en continu votre gestion de projet en analysant les conditions réelles de travail. Réagissez le plus rapidement possible aux changements ! Théoriquement, vous pouvez appliquer le modèle Scrum dans n'importe quel contexte professionnel. Je vous préconise simplement de vérifier que tous les acteurs du projet adhèrent à ces 3 piliers, ainsi qu'aux 4 principes et aux 12 valeurs du manifeste agile.
    
    
    
LES CARACTERISTIQUES SCRUM


        1. Une approche empirique
                -> C'est votre inspection quotidienne de l'état du projet qui oriente les décisions. Vous participez à l'amélioration continue du produit ou du service par l'observation de faits mesurables.
         
        2. Un cadre de travail holistique
            Votre équipe va diviser le projet en différentes parties. Vous devez cependant considérer que la valeur totale du produit ou du service sera toujours supérieure à la somme de ces divisions.
         
        3. Une méthode itérative 
                -> Votre équipe va découper le projet en plusieurs cycles identiques ou itérations. Vous vous rapprocherez graduellement du produit ou du service final afin de limiter les risques d'erreurs.
         
        4. Un développement incrémental
                -> La partie du projet que votre équipe a réalisée doit être utilisable. Vous pouvez donc livrer votre client régulièrement avec des fonctionnalités complètes.
         
        5. Une pratique agile
                -> Vous impliquez votre client et vos utilisateurs dans votre gestion de projet. Vous choisissez toujours des méthodes pragmatiques et adaptatives pour être plus réactif aux demandes.
                
                
ROLE DU PRODUCT OWNER

        Le product owner est le représentant du client au sein de l'équipe Scrum.
        
        Le client peut avoir plusieurs difficultés à intervenir dans le projet de manière agile:
            - Manque de disponibilité pour le projet
            - Manque d'expérience en gestion de projet agile
            - Manque d'informations sur les utilisateurs
            
        => c'est là qu'intervient le product owner, il est là pour garantir les intérets du client. Il devient donc le seul qui peut compléter ou modifier à tout moment la liste des fonctionnalités.
        
        Le product owner doit être capable d'affiner les objectifs du projet sans les dénaturer ! En qualité de scrum master, vous veillez à ce qu'il n'impose aucun changement radical ou arbitraire pendant la gestion du projet. Apportez-lui régulièrement de nouvelles connaissances afin de faciliter toutes ses négociations avec l'équipe de développement.
        
        En gestion de projet agile, l'équipe de développement est toujours auto-organisée. Vous n'avez pas besoin d'un chef ou d'un gestionnaire de projet. Le product owner n'est pas votre supérieur hiérarchique. Je vous conseille de l'installer dans la même pièce que l'équipe. Vous devez en effet l'inviter à rester très disponible pour qu'il réponde aux questions et donne son avis.
        

MISSIONS DU PRODUCT OWNER


            - L'expression des besoins avec l’équipe
            - La priorisation des besoins pour l'équipe
            - La validation des résultats de l'équipe
            
        
        Dans le modèle Scrum, le product owner fait partie intégrante de votre équipe. La réussite de votre gestion de projet sera nécessairement collective ! 🤝 Partagez ensemble les enjeux, la vision et la valeur du projet. Représentez ensemble l’équipe vis-à-vis des parties prenantes.
        
        
        L'expression des besoins de votre projet se fait obligatoirement sous la forme de récits utilisateur (user stories).
        
            =>  En tant que <utilisateur>, je veux <une fonctionnalité> afin de <répondre à mon besoin>
            
        La liste de l'ensemble des récits utilisateur constitue le carnet de produit (product backlog).C'est un document qui peut évoluer constamment au cours du projet.
        Le product backlog est une liste ordonnée de tout ce qui pourrait être requis dans le produit et est l’unique source des besoins pour tous les changements à effectuer sur le produit.
        
            - Le product owner est l'unique responsable de l'actualisation du product backlog.
            - Le product owner priorise les user stories formulées dans le product backlog.
            - Le product owner prend des décisions structurantes à partir du product backlog.
            - Le product owner surveille le budget et le planning grâce au product backlog.
            - Le product owner participe à la transparence du projet avec le product backlog.
            
            => Donnez ce rôle à une seule et même personne, pas à un comité de pilotage ! Je vous préconise surtout de nommer quelqu'un d'assez expérimenté pour optimiser la valeur du projet. Votre product owner est une personne expérimentée, disponible et diplomate.
            
            
        En qualité de scrum master, vous aidez le product owner à jouer son rôle : représenter le client ou le porteur du projet au sein de l'équipe Scrum.
        Inspectez ensemble la formalisation des user stories dans le product backlog du projet.
        
        
ROLE D'EQUIPE DE DEVELOPPEMENT SCRUM


        Les membres de votre équipe de développement sont en charge des opérations du projet. Ils livrent votre client à intervalles réguliers des fonctionnalités complètes.

                - Ses membres ont de multiples compétences.
                - Ses membres sont pluridisciplinaires.
                - Ses membres sont autonomes.
                
        Constituez une équipe de 3 à 9 personnes capables de réaliser le projet sans l'aide de membres extérieurs. Ce mode de fonctionnement favorisera votre efficacité et votre réactivité. 💪 Vous devez toujours encourager le partage des responsabilités : bon ou mauvais, n'attribuez jamais les résultats à un seul individu !
        
        Les membres dee l'équipe s'adressent directemnt au product owner
            -> encadrez l'activité de l'équipe autour des besoins du product backlog.
            
                - L'équipe ne peut pas être multi-produits.
                - L’équipe détermine seule ses choix de solution.
                - L’équipe est indissociable et tous les membres sont considérés au même niveau.
                - L'équipe estime sa charge de travail et détermine sa capacité à réaliser une tâche.
                - L’équipe organise et réalise les tâches du projet en respectant le modèle Scrum.
                - L’équipe est responsable de la réussite du projet et de l’atteinte des objectifs.
                - L’équipe participe avec le product owner à toutes les cérémonies Scrum.
                
            => Dans le modèle Scrum, votre équipe de développement est indépendante et les membres choisissent seuls comment accomplir le travail : pas d'aide externe, pas de hiérarchie interne et surtout pas de chasse gardée ! Assurez-vous que toutes les décisions de l'équipe soient bien prises unanimement.
        
            
MISSIONS D'EQUIPE DE DEVELOPPEMENT SCRUM


        Votre équipe de développement doit être stable sur la durée et de préférence pour plusieurs itérations. Vous aurez ainsi simplement à maintenir sa cohésion et ses performances. 🏋 Capitalisez sur votre connaissance des différents métiers de l'équipe. Félicitez chaque membre pour sa participation au projet commun.
        
        -> associer ce cadre de travail à un ensemble de pratiques agiles complémentaires:
        
                - Le planning poker
                        Votre équipe doit estimer la complexité de chaque user story avec le product owner. Vous pouvez utiliser un jeu de cartes représentant différentes valeurs pour l'estimation en points des user stories. Découvrez simultanément les cartes des participants et lancez un débat à partir des valeurs les plus extrêmes. Si nécessaire, répétez le jeu jusqu'à obtenir un consensus.
                 
                - Le tableau kanban
                        Votre équipe représente chaque user story sur des affichettes (bristols ou numériques). Vous allez les repartir sur un tableau divisé en 3 colonnes : "à faire", "en cours" et "terminé". Actualisez ce tableau lors de vos réunions quotidiennes afin de visualiser la progression de l'équipe. Remettez-le à zéro en début d'itération afin d'ajouter de nouvelles user stories.
                 
                - L'attribution des tâches
                        Votre équipe liste les tâches de chaque user story pour développer les fonctionnalités du produit ou du service. Vous ne devez surtout pas désigner un chef de projet chargé d'affecter ces tâches aux membres de l'équipe. Répartissez plutôt les user stories sur la base du volontariat et de la discussion lors de vos réunions quotidiennes.
                        
                        
ROLE D'UN SCRUM MASTER


        Le rôle de scrum master du modèle Scrum et plus communément celui de facilitateur pour votre gestion de projet agile.
        
            -> Vous êtes responsable de la compréhension, de l'adhésion et de la mise en œuvre du modèle Scrum. Vous êtes au service de l'équipe de développement, du product owner et du projet. Aidez chacun de ces membres à exercer leurs rôles. Favorisez les bonnes interactions afin d'optimiser la valeur finale du produit ou du service de votre client. Devenez un "leader au service de l'équipe".
            
            -> Vous n'êtes pas un chef de projet, ni un développeur, ni un agent de communication du client ! Votre rôle de scrum master ne doit jamais se cumuler avec celui de product owner. Vous n'exercez aucune autorité sur les membres de l'équipe de développement. 
            
            -> Votre objectif principal est de permettre à toute l'équipe de travailler sans perturbation :
            
                    - Vous aplanissez les difficultés qui se présentent à l’équipe pour fluidifier la production.
                    - Vous améliorez la communication à l’intérieur et en dehors de l’équipe.
                    - Vous suivez et communiquez tous les indicateurs pendant la gestion du projet.
                    - Vous expliquez et faites respecter les règles, les usages et les valeurs du modèle Scrum.
                    - Vous coachez et animez des ateliers autour de l’amélioration continue des pratiques agiles.
                    - Vous encouragez l’esprit d’équipe et maintenez la motivation au plus près des enjeux du client.
                    - Vous facilitez l'intégration du modèle Scrum dans l'entreprise ou l'association.
                    
            => Je vous conseille de faire preuve de transparence afin de partager une vision et des buts communs avec tous vos collaborateurs. Vous pouvez aider le product owner à structurer les user stories du product backlog. Devenez un "inspecteur des travaux en cours".
            Le scrum master n’est pas un manager qui pourrait prendre des décisions à la place de l’équipe. Vous n'avez pas de mission technique ou opérationnelle. Vous n’intervenez pas directement dans le développement du projet. Vous ne définissez pas le budget.
            
            Le rôle de facilitateur nécessite autant de qualités professionnelles que de qualités humaines : l'empathie, la bienveillance et l'humour.
            
            
            
MISSION D'UN SCRUM MASTER


        Le scrum master est au service des acteurs du projet pour lesquelles il remplit une mission de facilitation. Vous devez réunir toutes les conditions afin que votre équipe atteigne des objectifs préalablement fixés.
        
        
        Comment aider l'équipe à comprendre ses propres objectifs ? Comment accompagner l'équipe dans l'organisation et la réussite de ces objectifs communs ?    
            
            Voici 5 exigences à instituer pour créer des conditions de travail satisfaisantes :
    
                    - Valorisez la participation des acteurs du projet (sans les freiner).
                    - Assurez l'équilibre au sein de l'équipe (sans jeu d'influence).
                    - Présentez des textes et des illustrations agréables (sans superficialité).
                    - Garantissez le respect des uns et des autres (sans autoritarisme).
                    - Simplifiez la complexité des organisations (sans les vulgariser à l’extrême). 
                
                
        Comment faire la distinction entre la mission de facilitation du scrum master et celle d'un animateur, d'un médiateur, d'un formateur, d'un passeur ou d'un coach ?
        
             Voici précisément les actions que vous devez mener dans une entreprise ou une association :

                    - Accompagnez les salariés ou les adhérents dans l'adoption du modèle Scrum.
                    - Planifiez la mise en œuvre progressive du modèle Scrum.
                    - Aidez toutes les parties prenantes à comprendre l'approche empirique du modèle Scrum
                    - Provoquez des changements qui augmentent l'efficacité du modèle Scrum
                    - Coopérez avec d’autres scrum masters pour améliorer l'utilisation du modèle Scrum.
                    
        Vous pourrez souvent exercer votre métier de scrum master en tant que prestataire. Vous aurez l'avantage d'être plus neutre, mais le désavantage d’appréhender moins rapidement l'équipe du projet. 
        
        
        
        
LES PARTIES PRENANTES DU MODELE SCRUM


        Un scrum master peut vous amener à interagir avec d'autres personnes que le product owner et les membres de l'équipe de développement.
        
        
        Les parties prenantes ("stakeholders") sont des personnes qui sont en dehors de l’équipe Scrum, mais qui ont des connexions plus ou moins fortes avec le projet. Par exemple :

                - Le sponsor impacté financièrement par la fabrication
                - L'utilisateur final concerné directement par les usages
        
        Ces personnes peuvent exprimer des demandes au product owner et doivent répondre à ses questions. Vous les informez régulièrement de l’avancement du projet avec différentes démonstrations.
        
            -> Vous distinguez désormais les 3 rôles de l'équipe Scrum (product owner, membre de l'équipe de développement et scrum master) des autres parties prenantes du projet.
            
        En théorie, le modèle Scrum insiste sur l'importance d'une équipe pluridisciplinaire et polyvalente. En pratique, votre gestion de projet sera certainement meilleure avec l'aide d'experts ou de spécialistes. Vous avez donc la liberté de former vos équipiers ou recruter des prestataires externes :

                - Vous cherchez des compétences indispensables que les membres de votre équipe n'ont pas.
                - Vous validez avec l'équipe la participation ponctuelle d'un ou plusieurs membres extérieurs.
                - Vous trouvez un expert fonctionnel pour comprendre un besoin du projet (métier)
                - Vous trouvez un expert technique pour réaliser une tâche du projet (solution).
                - Vous contactez des personnes capables d'aider votre équipe à développer le projet.
                
        En qualité de scrum master, je vous conseille de ne pas rendre l'équipe trop dépendante de ces parties prenantes. Justifiez précisément leur légitimité ! ☝ Limitez leur participation en fonction de leur situation professionnelle ou de l'état d'avancement du projet.
        
        
ENVIRONEMENT DU MODELE SCRUM


        Si possible, installez votre équipe dans un local dédié à plein temps pour le projet. Le cas échéant, je vous préconise même de séparer cet environnement des autres activités de l'entreprise. Le local doit aussi être en mesure de répondre à toutes vos attentes logistiques :

                - Postes informatiques adaptés pour le travail en équipe
                - Grands tableaux blancs effaçables avec des feutres de couleurs
                - Espaces aux murs pour l'affichage (tableau kanban, par exemple)
                
        Cette salle dédiée va favoriser votre communication, ainsi que la transparence de l'information dans votre gestion de projet. 📞 Évitez le téléphone, les courriels et les réunions afin de partager le plus de conversations possible avec l'équipe. Mettez fin aux interruptions extérieures en vous concentrant sur des indicateurs visuels.
        
        Pas d'open space. Vous avez pour mission de protéger l'équipe de toutes les distractions néfastes. Les discussions et les avis de personnes étrangères au projet risquent souvent de porter atteinte au développement du produit ou du service.
        
        Servez-vous des murs pour maximiser votre management visuel.
        
        
        
        
PREPARER UN SPRINT


        L'itération ou sprint est une boîte de temps d'1 à 4 semaines (maximum). C'est la période au cours de laquelle une fonctionnalité complète du produit sera développée et incrémentée.

                - La durée des sprints est la même pendant toute la durée du projet.
                - Le nombre de sprints dépend de la capacité de l’équipe à couvrir les besoins.
                - Un nouveau sprint commence dès que le précédent est terminé.
                
        La décision de terminer un sprint ne peut être prise que par le product owner. Vous pouvez en effet observer avec lui que des objectifs initiaux ne sont plus atteignables. Cette fin anormale sera souvent la conséquence d’un contexte particulier et inattendu au sein de votre gestion de projet.
        
        Vous veillez à respecter plusieurs impératifs avec votre équipe Scrum:
        
                - Votre équipe ne peut pas modifier l'objet du sprint.
                - Votre équipe doit rester constante et régulière.
                - Vous ne négociez jamais à la baisse la qualité du travail.
                - Vous aidez l'équipe de développement à renégocier le nombre de tâches avec le product owner.
                - Vous limitez en amont la complexité des solutions et donc les risques de dérives liés au sprint.
                
        Alignez vos itérations avec les semaines civiles d'un calendrier. Débutez le lundi pour terminer un vendredi, et travaillez les jours fériés si possible. 🔎 Cette répétition de périodes de temps bien identiques (40 heures) va vous permettre d'analyser plus précisément votre gestion du projet.
        
        

SPRINT POUR LE SCRUM MASTER


        Vous avez utilisé le planning poker avec votre équipe Scrum pour estimer la complexité de chaque user story du product backlog. 
        
        Comment puis-je désormais calculer la vélocité de mon équipe de développement à partir des story points ? Comment vais-je pouvoir fixer durablement la date de fin du projet ?
        
            -> Additionnez les story points des user stories terminées au cours d'une itération. C'est ce total qui est appelé vélocité en gestion de projet agile. La périodicité des sprints vous donne ensuite la possibilité de déduire facilement la durée prévisionnelle du projet. Appliquez une formule de proportionnalité en tenant compte des users stories encore planifiées.
            
        REMARQUE: organiser un sprint d'échauffement afin de calculer la vélocité de l'équipe    
        
            ex: 1 sprint = 1 semaine (chaque sprint à la même durée tout au long du projet)
                1 user story = 3 points (si possible il faut qu'elles soient équivalentes, sinon les diviser en plusieurs petites)
                -> lors du sprint d'échauffement 2 user stories sont finies     => 6 points
                
                Si le total de toutes est 54 points --> il faut prévoir 9 semaines (9 sprints) pour finaliser le produit
                
                IMPORTANT: recalculer chaque semaine en fonction de l'avancement pour avoir la meilleure estimation du délais et le faire respecter
                
        Vos estimations ne sont jamais définitives : elles dépendent toujours d'informations qui peuvent évoluer. Vous pouvez aussi relier le nombre de points réalisés à la présence effective de l'équipe. Cette pratique de la vélocité relative vous permettra notamment de planifier les périodes de congés.         
        
        
        
L'INCREMENT DANS LE MODELE SCRUM


        Vous allez conduire une démarche non seulement itérative, mais incrémentale. C'est une compétence indispensable pour le scrum master et tous les acteurs du modèle Scrum.
        
        Les notions d'itération et d’incrément sont indissociables dans le modèle Scrum. Vous devez développer le produit ou le service de votre client en ajoutant successivement de nouvelles fonctionnalités. L'incrément correspond bien au résultat opérationnel de votre sprint. C'est une version intermédiaire du produit final. Vous êtes donc capable de le présenter aux parties prenantes du projet lors d'une démonstration.

                - L’incrément du sprint est réalisé sur la base des user stories que votre équipe a terminées.
                - L’incrément du sprint intègre les incréments réalisés lors de vos sprints précédents.
                - L’incrément du sprint peut être livré ou mis en attente pour intégrer une version.
                - L’incrément du sprint est opérationnel (ce n’est pas une maquette ou une preuve de concept).
                
            -> Votre équipe de développement réalise à échéances régulières des éléments fonctionnels et utilisables, plutôt que des composants techniques et dispersés. 
            

L'INCREMENT POUR LE SCRUM MASTER


        En gestion de projet agile, le modèle Scrum vous permet toujours de présenter un produit ou un service exploitable. Tirez tous les avantages de ce développement à la fois incrémental et itératif. Livrez votre client fréquemment et intégrez les évaluations à vos sprints.
        
        REMARQUE: Si l'équipe est consciente d'un défaut, votre rôle de scrum master vous oblige à intervenir. Ne laissez surtout pas persister une situation où vous n'êtes plus en mesure de livrer une version prête à l'emploi. Vous pouvez aider l'équipe en invitant des utilisateurs à formuler des critiques et des appréciations.
        
        Le modèle Scrum tend à supprimer l'effet tunnel 🚇 dans le travail collaboratif. Autrement dit, l'équipe ne pourra pas réaliser un seul et unique sprint d'intégration à la fin du projet !
        
            Vos incréments s'ajoutent à une version partielle, mais potentiellement utilisable, du produit ou du service développé.
            En qualité de scrum master, vous vérifiez que les incréments correspondent bien à des fonctionnalités terminées pendant les sprints.
            Profitez du développement incrémental pour organiser des livraisons fréquentes et une intégration continue avec le modèle Scrum.
            
            
            
SPRINT PLANING DANS LE MODELE SCRUM


        Vous préparez la réunion de planification d'une itération (sprint planning). C'est un événement rituel du modèle Scrum qui concerne tous les acteurs de l'équipe.
        
        Vous débutez chaque sprint par un événement rituel (ou cérémonie) destiné à définir l’objectif du sprint et son contenu. Vous décidez avec l'équipe Scrum quelles seront les user stories à développer au cours de la prochaine itération. Restez concentré sur le product backlog afin de définir tous ensemble l'organisation la plus pertinente pour l'équipe de développement.
        
            REMARQUE: vous devez limiter la durée de cette réunion à 8 heures pour un sprint d'un mois et à 2 heures pour un sprint d'une semaine. Je vous conseille cependant de prendre tout le temps de discuter avec l'équipe pour les sprints inférieurs à un mois.
        
        La sprint planning se divise en deux parties, le périmètre et le plan de l'itération :

                1. Qu’est-ce qui peut être terminé au cours de ce sprint ?
                L'équipe choisit ce qui sera développé au cours du prochain sprint. Le product owner définit l'objectif de l'itération à partir de vos analyses et de vos évaluations du product backlog.
                 
                2. Comment sera effectué le travail choisi ?
                L'équipe se demande comment atteindre l'objectif du sprint. Vous décomposez le travail en journée (ou moins) afin de lister toutes les tâches des users stories candidates.
                
            -> La première partie est sous le contrôle du product owner qui définit les objectifs et les responsabilités de l’équipe. Passez chaque user story candidate en revue afin de confirmer ensemble les estimations, ainsi que les critères d’acceptation. 
            -> La seconde partie est dirigée par l’équipe de développement qui cherche les meilleures solutions. Vous l'aidez à découper les user stories et à estimer les tâches.
            
            
SPRINT PLANING POUR LE SCRUM MASTER


        Vous vérifiez que toutes les user stories retenues pour le sprint sont bien en adéquation avec l’objectif défini par le product owner. 🃏 Vous utilisez le planning poker afin d'estimer la complexité de toutes les tâches listées avec l'équipe de développement. Anticipez la préparation de ce rituel en respectant

                        - La capacité de l'équipe
                        - La priorisation du product backlog
                        - Les conditions du secteur d'activité
                        - L'état actuel du produit ou du service
                        - L'impact technologique
                        
        Que faire lorsque mon client s'oppose à l'idée d'un sprint d'échauffement ? Comment produire de la valeur dès la première itération avec le modèle Scrum ?
        
            En tant que scrum master, vous devez faire une distinction assez stricte entre la préparation et la gestion du projet. Je consacre généralement deux semaines à collecter les besoins du client et des utilisateurs avant de proposer un product backlog cohérent. 😤 Faites preuve de pédagogie afin de convaincre votre client, et sinon utilisez des estimations théoriques pour le tout premier sprint.
            
            
LE SPRINT BACKLOG DANS LE MODELE SCRUM


        Décrire tous les enjeux de l'itération dans le sprint backlog. C'est avec cet artefact Scrum que vous suivrez le travail quotidiennement.
        
        Cet extrait du product backlog correspond à tout le périmètre qui doit être produit au cours du prochain sprint. Vous le présentez sous la forme d'un tableau kanban (ou scrum board) pour rendre visible toute la gestion de l'itération. Affichez au mur les user Stories du sprint, ainsi que les tâches décrivant les actions définies par l'équipe de développement.
        
        C'est l'équipe de développement qui est responsable du sprint backlog. Vous devez l'encourager à le faire évoluer si de nouvelles tâches sont identifiées. Utilisez vos réunions quotidiennes pour analyser les tâches qui n'avaient pas été détectées lors de la sprint planning.
        
            -> Le Sprint Backlog est une vue en temps-réel, très visible du travail que l’Équipe planifie d’accomplir durant le Sprint et il appartient uniquement à l’Équipe de Développement.
            
        Les tâches sont la propriété collective de votre équipe de développement. En qualité de scrum master, vous facilitez la communication et la collaboration des membres de l'équipe. Évitez notamment les dégradations liées à l'isolement d'un ou plusieurs équipiers.
        
        
LE SPRINT BACKLOG POUR LE SCRUM MASTER

        
        Vous pouvez calculer la somme totale de travail restant dans le sprint backlog à n’importe quel moment du sprint. Créez un graphe en grand format (burndown chart) dans votre local :

                - L'axe vertical représente la quantité de travail restant à effectuer (en story points).
                - L'axe horizontal correspond à la durée du sprint (en jours).
                
        Vous accompagnez la progression de l'équipe en assurant la transparence du sprint backlog. Faites le suivi du total de story points avec l'équipe de développement pendant chaque réunion quotidienne. Anticipez vos dates de livraison et calculez votre probabilité d’atteindre l’objectif du sprint.
        
            REMARQUE: Aucune user story supplémentaire n’arrive en cours de sprint, même sous la pression d'un manager. Seules les users stories retenues pendant la sprint planning seront développées.
            
        L’équipe est libre de choisir la manière dont elle veut incrémenter une fonctionnalité au produit ou service de votre client. Vous l'orientez à renégocier le périmètre du sprint avec le product owner lorsque le travail ne se déroule pas comme prévu. Gardez toujours à l'esprit l’objectif du sprint ! Favorisez la cohésion en motivant l’équipe à travailler ensemble, plutôt qu'individuellement.
        
        Comment interpréter correctement les déviations de l'équipe de développement ? Quelles sont les interventions que le scrum master met en oeuvre pour corriger le tir ?
        
            Je vous conseille d'observer au plus près le travail de l'équipe (sans l'espionner) 🔬 afin de mieux comprendre la différence entre les résultats attendus et les résultats réels. Vous avez aussi la possibilité de proposer des ateliers de formation. Soyez persuasif et n'hésitez pas à enclencher des changements de mentalité !
            
            
        Votre sprint backlog réunit toutes les user stories à développer pendant le sprint.
        Aidez le product owner à prioriser les user stories et l'équipe de développement à utiliser le tableau kanban (ou scrum board).
        En qualité de scrum master, vous accompagnez la progression de l'équipe Scrum avec la pratique agile du burndown chart.
        
        
        
LA DAILY SCRUM DANS LE MODELE SCRUM


        Vous réunissez l'équipe Scrum une fois par jour pour partager l’état d'avancement du sprint et signaler les obstacles rencontrés. Ce moment doit être considéré comme l'occasion d’échanger entre tous les équipiers et pas uniquement de suivre la progression du projet. Utilisez les questions suggérées par le modèle Scrum pour structurer correctement la réunion :

                - Sur quoi ai-je travaillé hier ?
                - Sur quoi vais-je travailler aujourd'hui ?
                - Quelles sont les difficultés que je rencontre ? Et comment peut-on m'aider ?
                
        La daily scrum dure 15 minutes au maximum. En qualité de scrum master, vous pouvez intervenir comme facilitateur, mais c'est principalement l'équipe de développement qui prend la parole. Vous pouvez aussi inviter ponctuellement les parties prenantes à assister aux réunions, sans s'impliquer.
        
                    ✅ La daily scrum se tient tous les jours, de préférence à heure fixe pour générer une forme de rituel.
                    ✅ La daily scrum se fait debout pour encourager les participants à respecter la limite de temps.
                    ✅ La daily scrum est obligatoirement composée des membres qui développent le projet.
                    ✅ La daily scrum est l’occasion d'actualiser le tableau kanban et le burndow chart.
                    ❌ La daily Scrum n’est pas l’endroit pour résoudre les problèmes de l'équipe.
                    

LA DAILY SCRUM POUR LE SCRUM MASTER


        Vous devez noter tous les sujets à aborder individuellement ou en plus petit groupe. Ne prenez pas le risque de déborder du cadre chronométré de la daily scrum. Voici comment rater cette réunion :
        
                    ❌ La réunion est improvisée.
                    ❌ Les échanges sont en face à face.
                    ❌ L’heure est variable en fonction des situations.
                    ❌ Les participants arrivent ou partent quand ils veulent.
                    ❌ Les participants n’osent pas évoquer les difficultés.
                    ❌ Les participants règlent leurs comptes.
                    ❌ Le scrum master ou le product owner dirige la réunion.
                    ❌ Les parties prenantes font des propositions.
                    
            -> La réunion quotidienne ne consiste pas à faire un compte-rendu du sprint. 
            
        
LA PREPARATION DES USER STORIES


        Le product backlog doit être correctement défini avant le début de votre gestion de projet. Vous répondrez donc à deux impératifs essentiels lors de votre préparation :

                - La collecte et l'étude des besoins du produit ou service.
                - La définition d'un langage commun avec l'équipe.
        
        Commencez par analyser les besoins exprimés par votre client et vos utilisateurs. Formalisez ensuite votre cadre de travail avec le modèle Scrum. Votre but est de lister avec l'équipe l'ensemble des fonctionnalités en limitant d'abord au maximum votre niveau de précision.
        
        La réussite du projet dépend aussi de votre capacité à coacher l'équipe Scrum. Vous apportez aux membres de l'équipe tous les moyens de travailler sans perturbation. Votre plus grande difficulté restera néanmoins d'anticiper la maturité des demandes entrantes et leur impact sur le sprint à venir. 🏁 Accompagnez le product owner en formalisant la définition du mot "prêt" :

                - Favorisez une bonne compréhension des besoins et des tâches.
                - Vérifiez la conformité des user stories aux objectifs du projet.
                - Explicitez les critères d’acceptation pour chaque user story.
                - Validez les compétences requises au sein de l’équipe Scrum.
                - Éliminez les dépendances externes de votre gestion de projet.
                
        En suivant ces conseils, vous obtiendrez à coup sûr une bonne définition de "prêt" (DoR, Definition of Ready). Vous pouvez ainsi garantir collectivement que les user stories sont assez précises et matures pour candidater à un sprint.
        
        
OPTIMISATION DES USER STORIES


        Vous jugez une user story avec la grille de critères INVEST. Elle vous aidera à reformuler un énoncé ou même à modifier en profondeur l'expression d'un besoin. Selon cet acronyme, une user story est :

                - Indépendante
                        L'équipe est capable de développer la fonctionnalité avant ou après n'importe quelle autre. C'est un critère qui est plus facile à respecter avec des produits informatiques. Faîtes preuve d'imagination et d'abstraction afin d'encourager l'équipe à isoler les user stories.
                 
                - Négociable
                        Le product owner va essentiellement rédiger l'objectif fonctionnel de la user story. C'est l'équipe qui discute ensuite des meilleures solutions pour développer le produit ou le service. Évitez les détails trop techniques afin de pouvoir rédiger collectivement les tâches à réaliser.
                 
                - Verticale (valuable)
                        L'équipe doit incrémenter des fonctionnalités réellement utiles. C'est votre collecte des besoins du client et de l'utilisateur final qui orientent la formulation de toutes les user stories. Découpez votre projet afin d'ajouter de la valeur au produit ou au service à l'issue de chaque sprint.
                 
                - Évaluée (estimable)
                        L'équipe a une vision précise de la complexité que représente une user story. C'est souvent une bonne formulation des critères d'acceptation qui facilitera cette estimation. Quantifier collectivement l'effort avec le planning poker afin d'obtenir un consensus unanime.
                 
                - Suffisamment petite (small)
                        L'équipe est en mesure de décrire jusqu'à deux jours de travail consécutifs. C'est un critère qui dépend surtout du nombre d'équipiers et de la durée du sprint. Planifier entre une et cinq fonctionnalités par itération afin de livrer votre client fréquemment.
                 
                - Testable
                        Le scrum master s'assure que la user story est bien comprise et que l'équipe peut fournir un exemple concret. C'est l'intégration de la fonctionnalité dans le produit ou le service que vous devez observer. Définissez en amont les tests de validation afin de garantir la qualité.
                        
                        
LA FINALISATION DES USER STORIES


        Vous allez établir des règles indispensables pour le travail collectif. Le modèle Scrum vous laisse en effet la liberté (et la responsabilité) de définir comment finaliser vos user stories. 
        
        Avant de vous lancer dans votre premier sprint, vous devez formaliser avec l'équipe Scrum la définition du mot "terminé" (DoD, Definition of Done). Réunissez tous les acteurs du projet afin de rédiger une définition commune et cohérente. Posez la question : comment considérer ensemble qu'une activité est terminée ? Vous permettez ainsi aux équipiers de s’assurer qu’il n’y a plus rien à faire et que la fonctionnalité peut être présentée au client.
        
        En gestion de projet agile, et avec le modèle Scrum en particulier, vous appliquez 3 actions :

                - Comprendre
                - Faire
                - Vérifier
                
        Je vous conseille donc de rendre exhaustifs et vérifiables les critères d’acceptation de vos user stories. Vous pourrez ensuite définir des règles plus précises pour chaque tâche du sprint backlog. Voici un exemple pour l'incrément d'un produit informatique :
        
                - La fonctionnalité est vérifiée avec des tests unitaires.
                - La fonctionnalité est disponible pour acceptation.
                - La fonctionnalité est déployée dans un environnement d'essai.
                - La fonctionnalité est validée par l'équipe Scrum.
                - La fonctionnalité est accompagnée d'une note de version.
                - La fonctionnalité est opérationnelle pour le produit.
                - La fonctionnalité est présentée au client.
                
        Vous affichez au mur cette liste de critères génériques afin de pouvoir considérer une fonctionnalité comme "finie". Si l'équipe de développement ne parvient pas à remplir toutes ces attentes, ne comptabilisez pas le travail réalisé dans votre calcul de la vélocité.
        
        
LA GESTION DES USER STORIES


        Vous devez trouver le bon équilibre pour gérer efficacement l'acceptation de chaque user story. Je vous préconise notamment d'afficher votre définition minimale dans l'environnement de travail. Ne prenez pas le risque de rendre votre équipe contre-productive avec des listes à rallonge. Sensibilisez les parties prenantes du projet en partageant explicitement cette information.
        
        Comment reconnaître une bonne gestion des user stories ? Comment rédiger une liste pertinente de tests ? Quels avantages tirer de cette pratique agile en qualité de scrum master ?
        
            La définition de "terminé" fonctionne comme une check-list qui guide les réflexions de votre équipe. Tous les acteurs du projet consultent également cette liste de critères pour suivre l'estimation ou la réalisation d'une fonctionnalité. 📝 Je vous recommande l'utilisation de la matrice Given-When-Then pour formuler correctement les tests de vos user stories.
            
                    => Étant donné (given en anglais) <un contexte>, lorsque (when) <l'utilisateur effectue certaines actions> alors (then) <vous constatez telles conséquences>
                    
        => une user story est:
             
            - le carton             -> Vous donnez rapidement une forme concrète et pérenne à la user story dans votre tableau kanban.
            - la conversation       -> Vous en discutez oralement avec tous les acteurs et toutes les parties prenantes du projet.
            - la confirmation       -> Vous définissez précisément les critères à respecter pour atteindre son objectif.
            
        
LA SPRINT REVIEW DANS LE MODELE SCRUM


        La sprint review est un rituel Scrum destiné à évaluer avec votre équipe l'atteinte des objectifs.
        
        Ce rituel a une durée d'1 heure par semaine de sprint. Votre ordre du jour est toujours le même 👌 valider ensemble l'incrément réalisé pendant le sprint par l'équipe de développement :

                - Présentez toutes les user stories sélectionnées lors de votre sprint planning.
                - Faites la démonstration des fonctionnalités complètement terminées.
        
        Vous ne devez jamais montrer à votre client des incréments partiellement réalisés ou en cours de finalisation pendant la sprint review. En gestion de projet agile, votre équipe ne reconnaît pas d'état intermédiaire : soit la fonctionnalité est finie, soit elle ne l'est pas !
        
        Tout ce qui est produit doit être démontré à votre client et aux parties prenantes du projet. Vous préparez donc la sprint review en répondant à la question : qu’avons-nous développé ?
        
        La sprint review vous permet de faire le bilan du sprint réalisé. Le product owner peut ensuite mettre à jour le product backlog en fonction de ce qui a déjà été réalisé.
        Discutez avec les parties prenantes de votre gestion collective du projet pour détecter de nouvelles opportunités. Vous serez alors en mesure d'optimiser les user stories et d'améliorer votre prochaine sprint planning.
        
        
        
LA SPRINT REVIEW POUR LE SCRUM MASTER


        La sprint review est une réunion informelle, pas un comité de pilotage. 😏 Vous devez obtenir des réactions et favorisez la collaboration en présentant les fonctionnalités. Profitez également de cette sprint review pour déterminer quels seront les prochaines user stories candidates au sprint suivant.
        
            REMARQUE: En qualité de scrum master, vous fixez la date et le lieu de cette réunion. Je vous conseille aussi de faire une courte introduction pour rappeler le but de la sprint review aux participants. Vous apprenez notamment aux parties prenantes à respecter la boîte de temps.
            
        Vous facilitez la prise de parole ou les interactions entre l'équipe de développement et les différentes parties prenantes du projet. Voici des exemples de sujets à aborder pendant la sprint review :

                - Déroulement du sprint
                - Problèmes rencontrés
                - Résolutions trouvées
                - Questions sur l'incrément
        
        Vous aidez le product owner à redéfinir les dates de livraisons par rapport au progrès de l'équipe. Vos analyses du product backlog, du budget et du marché pourront aussi orienter la prochaine livraison.
        
        
        Structurer la planification de vos livraisons. Organisez les user stories selon les usages des utilisateurs et les activités principales du projet à développer :

                - Placez en haut les user stories qui sont indispensables pour livrer le produit ou le service.
                - Positionnez les autres plus bas dans votre story map.
                
            -> Vous anticipez le nombre de sprints entre les livraisons et planifiez leurs dates potentielles avec l'équipe. Le résultat de votre sprint review est un product backlog bien révisé pour la prochaine itération.
            
            
LA SPRINT RETROSPECTIVE DANS LE MODELE SCRUM


        Vous tenez ce rituel juste après la sprint review afin de mettre en évidence ce qui a bien marché au cours du sprint et ce qui doit être amélioré. La rétrospective du sprint ne réunit que l'équipe Scrum.
        
        Votre objectif est d’inspecter en détail l'itération précédente. En respectant les piliers Scrum, vous repartez du plan d’action précédent afin d'actualiser un radar de satisfaction 💖:

                1. Ce qui a bien fonctionné
                2. Ce qui doit être changé
                3. Les axes d’amélioration
                4. Les inconnus
                
            -> La rétrospective du sprint dure trois heures pour un sprint d'un mois. En qualité de scrum master, je vous conseille de prendre deux heures minimum pour les itérations moins longues.
            
            
        Rédigez ensuite un nouveau plan d’actions pour le sprint suivant. Vous facilitez l'adaptation de votre équipe aux changements et participez à l'amélioration continue du produit ou du service développé.

                - La rétrospective du sprint est généralement animée par le scrum master.
                - Les axes du radar peuvent varier mais sont en général : autonomie, finalité et maîtrise.
                - Le plan d’actions doit comprendre 2 ou 3 actions d’amélioration très concrètes.    
                
        Décrivez précisément les personnes, les relations, les processus et les outils de votre gestion de projet agile. Tous les moments sont bons pour corriger le tir, mais la rétrospective de sprint vous fournit un événement dédié à l'inspection et à l'adaptation
        
        
LA SPRINT RETROSPECTIVE POUR LE SCRUM MASTER


        Votre rôle de scrum master est déterminant lors des rétrospectives de sprint. Vous analysez explicitement les faits marquants survenus depuis la précédente réunion de ce type. Vous décidez collectivement des actions d'amélioration ou de remédiation grâce à vos échanges avec l'équipe.
        
        Utilisez un format prédéterminé pour faciliter cette réunion. Vous aurez toujours la même intention de départ : donner à tous les acteurs du projet les moyens de s'exprimer librement !
        
        Vous responsabilisez votre équipe en rendant votre gestion de projet plus transparente. Vous favorisez aussi l'appropriation des décisions en tirant tous les avantages de la répétition de cette cérémonie. La montée en compétence des acteurs du projet sera nécessairement progressive. Par exemple :

                - Proposez un atelier de réflexion sur la définition de "terminé" à chaque rétrospective de sprint.
                - Affinez ensemble des scénarios et des cas d'usage (user scenarios et use cases).
                - Partagez les informations les plus pertinentes de votre veille collaborative.
                - Créer une pratique ludique et conviviale en lien avec le projet.
                
        En tant que facilitateur de l'équipe Scrum, vous devez établir un climat de confiance entre les participants. Surveillez attentivement les réticences, les non-dits, ainsi que les attitudes corporelles. Constatez et agissez avant la prochaine rétrospective de sprint. Ce rituel très utile fera alors de vous un meilleur scrum master.
        
        
PRODUCT BACKLOG GROOMING


        Vous allez encourager l'équipe Scrum à faire le raffinage du product backlog ou toilettage (product backlog grooming).
        
        Cette pratique agile prend la forme d'une véritable réunion pour votre équipe. Ce n'est pas un rituel officiel du modèle Scrum, mais je vous conseille vivement de planifier un toilettage après chaque rétrospective du sprint. Vous procédez ensemble à l'affinage de toutes les user stories restantes :

                    - Détectez les user stories qui n'ont plus aucun sens pour le projet.
                    - Formulez et estimez les nouvelles user stories si des besoins apparaissent.
                    - Réévaluez l'ordre de priorité des user stories dans le product backlog.
                    - Corrigez les estimations en fonction de nouvelles informations.
                    - Découpez les user stories qui pourraient candidater aux prochains sprints.
                    
        Le product backlog est un document qui évolue constamment pendant le développement du projet de votre client. Contrairement à un cahier des charges, cet artefact Scrum n'est jamais définitif.
        
        -> Vous évitez à l'équipe de se disperser lors de la préparation du projet. Ne prenez pas le risque de découper finement toutes les user stories avant le premier sprint. Orientez surtout votre product owner à maintenir un product backlog "juste à temps".
        
        Le product backlog a tendance à grossir pendant la gestion du projet. L'équipe y ajoute principalement des fonctionnalités de confort. Ce ne sont pas des user stories prioritaires, mais de bonnes propositions à ne pas oublier... Votre raffinage est donc essentiel :

                    - Garantissez la pertinence du product backlog.
                    - Évitez la dégénérescence du projet (fonctionnalités obsolètes ou à reporter).
                    - Challengez l'équipe sur la valeur et la priorisation des besoins.
                    - Préparez les membres à réaliser les tâches efficacement.
                    - Respectez le calendrier de livraison.
                    
        Lorsque un élément du product backlog ne contribue pas ou plus aux objectifs du projet, alors vous devez inciter l'équipe Scrum à le supprimer. Vous déchirez l'affichette (user story, tâches, critères d'acceptation, etc.). À l'inverse, vous motivez aussi le product owner et l'équipe de développement à inclure de nouvelles descriptions, priorisations, estimations ou évaluations,
        
        
PRODUCT BACKLOG GROOMING POUR LE SCRUM MASTER


        Le product owner aura besoin de votre aide pour confronter ses prises de décision aux indicateurs concrets du modèle Scrum. L'équipe de développement sollicitera votre accompagnement pour faire des estimations précises et réalistes (planning poker, notamment).
        
        Donnez de l'importance à votre approche empirique pour servir efficacement les membres de votre équipe Scrum. Le toilettage du product backlog sera aussi un bon moyen d'assurer sa transparence.
        
        En qualité de scrum master, vous pouvez créer une boîte de temps pour chronométrer cet atelier. Ne dépassez pas 10% de la durée totale de votre projet pour planifier toutes les réunions de ce type. Personnellement, je dédie une journée pour la product backlog grooming et la sprint planning, puis une autre journée pour la sprint review et la sprint retrospective. J'obtiens ainsi des itérations de 3 jours sur 1 semaine ou de 8 jours sur 2 semaines.
        
        Vous devez aussi jouer votre rôle de facilitateur pour ce rituel. ✊ Ne cherchez pas des coupables, cherchez des solutions. Ne rabaissez jamais un membre de l'équipe. Soyez réactif et exigeant afin de calmer rapidement les discussions conflictuelles. À contrario, méfiez-vous aussi d'une fausse politesse qui pourrait masquer un désintérêt plus ou moins profond vis-à-vis du projet.
        
        
        Vos responsabilités de scrum master vous engagent à devenir le protecteur de l'équipe, le garde-fou du projet, le garant du modèle Scrum. 