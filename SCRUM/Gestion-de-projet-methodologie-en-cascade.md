########## GESTION DE PROJET METHODOLOGIE EN CASCADE ##########


    Plus un projet est imprévisible plus ce chef de projet aura intérêt à employer des méthodologies dites "agiles". Au contraire, plus un projet est prévisible, plus il/elle aura tendance à employer une méthodologie dite classique, séquentielle ou encore en cascade.
    



##### PHASE 1 : INITIALISATION #####
    
ANALYSER LES BESOINS DU CLIENT

        
        Un projet doit respecter des exigences:
            - qualité
            - cout
            - délai
            
        Un projet à une date de début et une date de fin.
        
        Un projet commence avec un besoin.
            
            -> confirmation des besoins explicités
            -> explorer les besoins au travers de la reformulation
            -> informations sur le contexte de l'entreprise
            -> besoin explicite
            -> explorer les besoins à travers des questions ouvertes
            -> précisez les besoins
            -> besoin implicite
            
            
    => phase d'initialisation   -> recueillir et analyser les besoins du client
    
    
    
    Sites d'appel d'offres:     http://www.bepub.com/appels-d-offres/multimedia
                                https://www.francemarches.com/
    
    
    
CADREZ LE PROJET
    
    
        Cadrez le projet avec l'équipe.
        -> donner un ordre du jour à la réunion
        -> réunion pas trop longue
        -> rappeler l'ordre du jour si nécessaire
        -> recadrer la réunion si besoin
        -> restituez les besoins du client potentiel
        -> expliquez les points positifs ET les points négatifs du client
            -> rassurer l'équipe si nécessaire
        -> consultez les experts pour élaborer la solution
        -> expliquez les choix de methodes
        -> consultez les experts pour estimer le temps de travail
        -> re-précisez les livrables si nécessaire
        

PLANIFIER LA REALISATION DU PROJET AVEC UN DIAGRAMME DE GANTT


        On va connaitre la charge de travail estimée des collaborateurs (qu'ils auront défini) -> on va pouvoir planifier
        -> pour ça on va se servir du diagramme de Gantt: methode classique de planification et de gestion qui permet de représenter visiuellement m'état d'avancement d'un projet.
            -> choisir un outil adapté (ex: dapulse ou template excel: https://docs.google.com/spreadsheets/d/1L7FlpkTG_3XpELN7U5TU0xW2Rgys_Y1SUAo-edxcEPc/edit#gid=1920223689)
            
            => assignez les taches aux membres de l'équipe (savoir gérer et connaitre son équipe: qualité, expérience)
            => donner une date de début et de fin à chaque tache
            => tenir compte des dépendances et des chavauchements dans cette planification
            
            
            => mettre à jour la progression
            
            
    Un projet de fait en 5 phases:
    
        1. Initialisaton
        2. Lancement
        3. Conception
        4. Production
        5. Exploitation
        
    Faire un diagramme de Gantt:
        - Notez les 5 phases
        - Pour chacune des ces phases, notez toutes les taches qui les composent
            ex: phase d'initialisation:
                    - recueil des besoins
                    - etude de faisabilité
                    - cadrage
                    - proposition commerciale
                    - soutenance
                    
        REMARQUE: chaque tache se calcule en jour-homme
        
        
        
BUDGETISEZ LE PROJET


        Identifier les différents postes de dépenses du projet.
        
            cout d'un profil(salaire brut + charges patronales + charges salariales) + marge (20%) -> pour chaque acteur du projet
            
            
            
REDIGEZ UNE PROPOSITION COMMERCIALE


        - On calcule le prix de vente de la manière suivante:
        
            cout de reviens: calcul de cout de toutes les ressources utilisées (humaines ou matérielle)
            marge commerciale: minimum: prix de vente - cout de reviens > 50% cout de reviens
                ex: cout de reviens 8000
                    prix de vente: 15000
                        -> marge= 15000 - 8000 = 7000 (supérieur à 50% du cout de reviens -> ok)
            
            => prix de vente = cout de reviens + marge (qui est au moins 50% du cout de reviens)
            
            
        - Structurer un document:
        
            1 page de garde
            1 note de synthèse 
            1 devis
            
            (parfois en plus)
            
            des conditions de ventes annexes
            une version rédigée de l'étude de faisabilité
            
            
            PAGE DE GARDE:
                Soignez la page de garde, c'est souvent elle qui montre au client potentiel qui vous êtes.
                    ex: avec en titre : Réponse à l'appel d'offre + nom du projet + nom client + logo société + créer un aperçu du site à fournir
                    
            SYNTHESE:
                1. description du projet et des besoins
                2. solution préconisée pour répondre aux besoins
                3. objectif daté et si possible chiffré
                
            DEVIS:
                durée de validité de l'offre
                indication manuscrite datée et signée du client "lu et accepté"
                indication manuscrite datée et signée de l'entrepreneur "lu et accepté"
                
                
                
DEFENDEZ VOTRE PROPOSITION COMMERCIALE


        Vous devez donc être capable de répondre aussi bien à des questions techniques, que graphiques ou marketing. Ainsi, pour préparer votre soutenance, assurez-vous de comprendre les choix préconisés par vos experts, au moins dans les grandes lignes. Ensuite ça sera à vous de traduire ces choix dans un langage compréhensible par vos interlocuteurs.
        
        Créer un support de présentation a une valeur intrinsèque: ne pas savoir si la salle de réunion sera équipée d'un rétro-projecteur n'est pas une excuse ! Amenez un ordinateur portable, une tablette, un téléphone, imprimez vos slides, peu importe mais trouvez un moyen de mettre vos visuels devant les yeux de la personne à convaincre. 
            => ne pas passer plus d'une minute par slide
            => ne pas omettre d'informations capitales
            
            
        Passer une soutenance, c'est bien plus que transmettre de l'information factuelle, c'est tout un jeu d'acteur, scripté par moments, improvisé à d'autres, mais dans tous les cas vivant et interactif.
        
        
        Adoptez la bonne posture:
            - vous tenir droit
            - détendre vos épaules
            - sourire
            - vous exprimer à un rythme contrôlé
            - être à l'écoute active de votre interlocuteur
            - vous habiller en répondant aux codes de celui-ci
            
            
            
    RESUME:
        phase d'initialisation d'un projet:
            - Analyser et recueillir des besoins client
            - Préparer, organiser, animer des réunions et en rendre compte
            - Construire un planning de réalisation sous forme de diagramme de Gantt
            - Estimer les coûts d'un projet
            - Formuler et présenter une proposition commerciale
            
            


##### PHASE 2 : LANCEMENT #####

            
CREER UN CAHIER DES CHARGES


        On se trouve dans la phase de lancement du projet.
        => préparer et formaliser en amont tous les éléments nécessaires au bon déroulement des phases de conception, de production et d'exploitation du projet.
        => création du cahier des charges: document contractuel détaillant toutes les informations nécessaires à la réalisation d'un projet.
        
        On peut diviser un cahier des charges en 7 parties:
        
            - cadre du projet               -> déjà expliciter lors de la phase d'initialisation
                                                => résumé ; contexte de l'entreprise ; enjeux et objectifs ; livrables ; présentation de l'équipe ; planning prévisionnel
                                                
            - benchmark                     -> analyse concurentielle
            
            - considérations marketing      -> on y définit les cibles ; le référencement ; international (en plusieurs langues) ; charte éditoriale
            
            - conception graphique          -> on y définit un brief créatif ; charte graphique
            
            - spécifications fonctionnelles -> toutes les informations liées aux fonctionnalités
                                                => périmètre fonctionnel (front et back) ; arborescence ; aperçu des contenus (maquettes)
                                                
            - spécifications techniques     -> ce qui concerne le dev
                                                => choix technologiques ; domaine et hébergement ; base de données ; accessibilité ; services tiers ; sécurité
                                                
            - budget
            
            
        REMARQUE: un cahier des charges très précis permet de "régler" les conflits juridiques MAIS est difficle à modifier
        


BENCHMARK


        Un benchmark désigne un état de l'art, une analyse des pratiques de la concurrence dans le but de se positionner sur un marché.
        
        Il faut s'inspirer de sites leaders concurentiels pour identifier leurs réussites et ne pas commettre les mêmes ereurs.
            - Ergonomie (navigation, adaptabilité mobile)
            - Charte éditoriale (ton, langues)
            - Charte graphique (typographie, palette de couleurs)
            - Fonctionnalités (ex. système de réservation, newsletter)
            - Technologie utilisée (ex. WordPress)
            - ...
            
        Pour pouvoir retranscrire cette partie:
            Listez chaque projet étudié accompagné d'une capture d'écran et d'un tableau de ses points forts et ses points faibles.
            
        
REFLEXIONS MARKETING

        
        Vous allez apporter des précisions sur le marché et dans une certaine mesure la manière de s'y implanter.
            => assurez-vous de bien identifier et comprendre la demande de votre marché et client-cible (persona). 
            
        
        
SPECIFICATIONS FONCTIONNELLES


        Il décrit, précise les fonctionnalités du site, de l'application ou du logiciel en question.
        
        Pour les parties suivantes s'aider d'un logiciel de carte mentale (https://www.mindomo.com/fr/?join=f4Rb)
        
            -> délimiter un périmètre fonctionnel:
                Si le périmètre fonctionnel de votre site n'est pas ou mal délimité, vous ne saurez jamais ou pas exactement où vous arrêter dans son développement. Listez donc de manière aussi exhaustive que possible les fonctionnalités dont vos utilisateurs auront besoin pour se servir du site.
                    => dans 1 projet informatique complexe on peut utiliser la méthode UML (Unified Modeling Language) 
                    => dans 1 projet simple, on fait un impact mapping : se poser une succession de bonnes questions pré-établies :

                            Quel est l'objectif de mon site ?
                            Quels sont les différents profils de ses utilisateurs ?
                            Quels actions cherchent-ils respectivement à faire sur mon site ?
                            Quelles fonctionnalités vont leur permettre de mener à bien ces actions ?
                            
                        ==> en répondant à ces questions (sous forme de tableau par ex) on pourra effectuer une cartographie du périmètre
                        
                        
            -> schématiser l'arborescence du site:
                L'arborescence d'un site est une représentation schématique des différentes pages qui composeront le site, liées entre elles et hiérarchisées par niveaux de profondeur.
                
                
                
                
ZONING ET WIREFRAMING


        Après avoir délimité le périmètre fonctionnel et créé l'arborescence de votre site, vous et votre équipe aurez tous les éléments pour en faire des ébauches. Ces croquis, volontairement simplistes, serviront de document de référence par la suite, en phase de conception.
        
        -> Le zoning est une technique de maquettage basse fidélité qui permet d'organiser le contenu d'une page web en la divisant en différentes zones. 
            c'est en quelques sortes la 1ere ébauche graphique du site. Rien n'est figée, elle peut être modifiée.
            
        -> Un wireframing (ou maquette fil de fer en français) est une technique utilisée pour créer des versions basse/moyenne fidélité d'un livrable.
            le wireframing permet de rajouter une couche de détail à vos maquettes. Si vous partez d'un zoning bien fait, tout ce que vous aurez à faire, c'est remplacer vos zones par les éléments qui les composent. 
            
            OUTILS: https://balsamiq.com/
                    https://moqups.com/
                    https://wireframe.cc/
                    
                    
        -> Documentez la partie Conception Graphique
            
        
        RESUME:
            - Se discipliner à d'abord passer par des maquettes basse puis moyenne fidélité à moyenne pour préparer les maquettes haute fidélité, n'est pas une perte de temps, au contraire.
            - Réalisez vos maquettes en étroite collaboration avec votre graphiste ou déléguez-lui.
            - Faites régulièrement valider vos maquettes par le commanditaire. 
            
                
            
SPECIFICATIONS TECHNIQUES

        Il faut traduire ou faire traduire vos spécifications fonctionnelles en spécifications techniques. 
        
        Les spécifications techniques d'un cahier des charges est une documentation des méthodes, procédés, et technologies sélectionnées pour faire face aux contraintes de réalisation du projet.
        
        Tous les choix ne seront pas forcément accepté par tout le monde (commanditaire ou équipe)
            -> Il faut rationaliser et à expliquer chacun de vos partis pris technologiques: la technique de la scorecard
                1. Créez un tableau
                2. Listez vos options dans la première colonne.
                3. Listez vos critères d'évaluation dans la première ligne.
                4. Donnez un poids (pondération) à chacun de ces critères en fonction de leur importance.
                5. Cellule par cellule, pour chaque critère, donnez un score sur 1 à vos options.
                6. Calculez le score total (pondéré) de chaque option en multipliant chaque score par le poids de sa colonne.
                
            => Vous pouvez utiliser la technique de la score card pour rationaliser n'importe quelle autre prise de décision, que ce soit pour choisir un hébergeur, recruter un collaborateur ou choisir une destination de voyage.
            
        La finalisation et la validation de votre cahier des charges marquera la fin de la phase de lancement du projet.    
        
        
       REMARQUE: Le cahier des charges doit être signé par le client 


##### PHASE 3 : CONCEPTION #####


        La phase de conception sert à finaliser les maquettes et la rédaction des contenus pour que vos développeurs, en bout de chaîne, n'aient pas à prendre ces décisions à la place de votre graphiste ou votre marketer.
        -> Le graphiste n'aura donc qu'à livrer une charte graphique finale et l'appliquer à ces maquettes en lors du passage en haute fidélité. 
        
        Une fois le projet bien initialisé et lancé, le rôle et la posture du chef de projet vont légèrement changer. Il va passer d'un producteur actif de livrables au centre de toutes les interactions à un accompagnateur, facilitateur et superviseur un peu plus en retrait mais toujours disponible. 
            -> Trouvez un équilibre entre laisser-faire et interventionnisme pour instaurer un climat de confiance propice au respect de qualité, de délai et de coûts du projet. 
            
        Sensibilisez vos équipiers à votre propre métier pour réduire au maximum leur dépendance vis-à-vis de vous et votre vision globale du projet. 
        Plus vous pourrez faire confiance à vos équipiers, plus ils prendront du plaisir à travailler et plus ils rendront des livrables de qualité. Essayez donc, dans la mesure de la confiance que vous pouvez raisonnablement accorder à chacun, d'instaurer ce cercle vertueux.    
        
        Soyez vigilant quant à l'optimisation excessive et les glissements de portée.
        
        Une fois que vos maquettes et vos contenus seront terminées et validés, vous pourrez passer à la phase suivante et tant attendue : la production !

        
        IMPORTANT: Tenir les délais, la qualité et le couts.
                    -> il faut s'assurer que le projet corresponde bien au cahier des charges
        
        

##### PHASE 4 : PRODUCTION #####
                    
        
        Les développeurs vont traduire les maquettes dans les langages informatiques sélectionnés plus tôt, tels qu'ils sont documentés dans le cahier des charges. Ces maquettes qui étaient alors des images statiques vont prendre vie et devenir un site à proprement dit. On parle "d'intégration" des maquettes et de programmation des fonctionnalités du site.
        
        
        
        GERER LA PRESSION:
            Au fur et à mesure que la deadline arrive, le stress augmente -> risque d'augmenter les erreurs
            Si on voit correctement l'avancement du projet, en intervanant suffisamment tôt on peut par exemple avoir un dev supplémentaire pour respecter les délais sans pour autant tout faire à la dernière minute.
            
        TESTER AVANT DE LIVRER:
            Tester n'est pas optionnel.
            L'inconvénient majeur de la méthodologie en cascade pour la gestion de projet digital est la détection potentiellement tardive des erreurs. Pour compenser, intégrez donc des batteries de tests plus ou moins fréquents en fonction de la complexité et des risques techniques du projet.
            
                    
                    
                    
##### PHASE 5 : EXPLOITATION #####                    

        Suite à la livraison du projet pour le commanditaire, si tout est ok, vous allez passer dans la phase d'exploitation.
        
        Même si le projet a déjà été vendu et que vous êtes certain d'avoir respecté scrupuleusement le cahier des charges, ce n'est pas encore le moment de se relâcher. Vous aurez tout le loisir de célébrer la clôture du projet une fois que vous serez assuré de la satisfaction du client. 
        
        
        Durant cette étape, toute votre attention doit être portée sur la satisfaction client. Il n'y a que 2 cas de figure possible.

                Votre performance est supérieure aux attentes du client, auquel cas il est satisfait.
                Votre performance est inférieure aux attentes du client, auquel cas il sera insatisfait.
                
        Pour que le client s'approprie le site, ne vous contentez pas de lui faire une démonstration. S'il va être amené à s'en servir, prenez le temps qu'il faut pour le former à son utilisation. L'être humain a peur de ce qu'il ne comprend pas. Parfois il suffit d'un peu d'éducation pour changer cette peur en curiosité voire en passion. Si vous voulez que le client aime son site ou son application, commencez par lui partager votre passion pour son produit.
        
        Pour que le client s'assure par lui-même que son site est bien conforme à ce que vous lui avez vendu, proposez-lui d'effectuer une recette fonctionnelle (aussi appelée vérification d'aptitude au bon fonctionnement). 

                - Préparez-lui un cahier de recette, listant tous les tests à effectuer.
                - Donnez-lui un protocole de recette détaillant le processus de remontée d'anomalies. 
                - Mettez-vous d'accord sur un planning de recette, période pendant laquelle il pourra faire remonter ces anomalies pour que celles-ci soient corrigées sans entraîner de surcoûts.
                
        Lorsque vous aurez rempli tous vos engagements, assurez-vous que le client tienne les siens en vous payant en temps et en heure.
        
        Tous les projets ont une date de début et une date de fin. Une fois le projet clôturé, votre rôle en tant que chef de projet sera de dresser un bilan pour pouvoir faire un reporting à votre équipe.
        
        
        
        
RESUME POUR UN PROJET

        1. Analyser et recueillir des besoins client
        2. Préparer, organiser, animer des réunions et en rendre compte
        3. Construire un planning de réalisation sous forme de diagramme de Gantt
        4. Estimer les coûts d'un projet
        5. Formuler et présenter une proposition commerciale
        6. Rédiger un cahier des charges fonctionnel
        7. Rédiger un cahier des charges technique
        8. Structurer et encadrer une équipe pluridisciplinaire