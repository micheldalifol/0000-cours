########## GESTION DE PROJETS ##########

METHODES DE GESTIONS DE PROJETS CLASSIQUES

    Les phases s'enchainent. Lorsqu'une phase est terminée on passe à la suivante:
     - methode en cascade
     - methode de cycle en V:
        -> Conception Générale --> Conception Détaillée --> Développement --> (on remonte dans le V) Tests détaillés --> Tests Finaux --> Production
        
    => ces méthodes ne permettent pas de retour en arrière    
        
        
METHODES AGILES

    Ce sont des petits cycles de dévelopement (sprint) -> on peut s'adapter au cours du développement
    
        => ces méthodes permettent 1 gestion au plus près du projet avec le client
        
    
    
METHODOLOGIE EN CASCADE

    Les 5 grandes phases:
        - Initialisation
        - Lancement
        - Conception
        - Production
        - Exploitation et Maintenance
        
        
        1. INITIALISATION:
            - besoin        -> ce qu'à succité le besoin du commanditaire et le produit qu'il veut
                            -> EST-CE QUE C'EST POSSIBLE?   => faisabilité du produit:
                                                                    - délais
                                                                    - budget
                                                                    - techniquement réalisable
                                => le chef de projet va faire:
                                    - étude du besoin
                                    - lister les riques possibles qui mettet en danger ce projet
                                    - participer au 1er cadrage du besoin
                                
            - 1 projet commence toujours dans 1 entreprise
                => il faut que la direction crée pour ça 1 équipe de projet qui comprend:
                    - 1 chef de projet
                    - 1 responsable de la DSI (Direction des Systèmes d'Information) qui seront amenés à mettre en ligne ce projet
                    - 1 développeur
                    
            
            
            La 1ére étape du chef de projet est de comprendre le besoin, de voir s'il dispose des ressources nécessaires ET dans quels délais le projet pourra démarrer.
                => en résultat 1 note de cadrage qui va reformuler et bien clarifier le besoin + 1 estimation des couts du projet (hommes et machines)
                Cette note est le document qui permet de mettre en place les objectifs du projet: IMPORTANT
                
            La 2e étapes est d'identifier les risques qui pourraient mettre en péril le projet.
                => ex:  - risques techniques: on change de technologie et on arrive sur 1 nouveau langage
                        - risques externes: le développeur est réaffecté sur 1 autre projet -> la ressource à disparue -> on ne peut plus mener le projet à bien ou manque de personnes compétentes pour l'effectuer ou encore que la direction ne met pas les moyens pour le réaliser
                        - risques juridiques: avoir anticiper de déposer un nom auprès des organismes ou d'avoir vérifier quels sont les éléments importants avec la CNIL
                        
                        
            Une fois la note de cadrage validée par le client, les risques étudiés et validés  => on peut lancer le projet
            
            Dans le cas où on se situe en agence (pas en interne) on se trouve dans la position où on répond à 1 appel d'offres
                => à la fin de cet appel d'offres le client se met en contact avec le meilleur prestataire et le projet peut se lancer
                
                
        2. LANCEMENT:
            - On rentre dans le détails du projet => on va établir le cahier des charges. 
            Cette phase à pour objectifs de:
                - clarifier et approfondir le besoin du client
                - élaborer les documents qui vont permettent de brieffer les équipes techniques dans la conception (phase suivante)
                - poser les bases du planing et attribuer les tâches
                
                => approfondir les besoins du commanditaire:
                    sur la base de la note de cadrage on va creuser les éléments:
                        * qui sont les cibles du projet? (utilisateurs finaux: jeunes; vieux; hommes; femmes;cibles fidèles ou à acquerir; ...)
                        * quelles sont les pratiques du client? (client habitué à utiliser des outils informatiques ou faut-il le former?; Sont-ils diponibles? Vont ils s'adapter au produit?)
                        * Les contenus? (que peut nous fournir le client?)
                    
                    => tous ces éléments vont nous permettre de créer un concept qu'on pourra décliner en fonctionnalités   
                    
                        -> le concept va être le fil rouge du projet: on va faire un site qui touche de nouvelles personnes qui ne connaissent pas forcément le milieu (ex: jazz) => le concept sera la convivialité plutôt que l'élitisme....
                        => ce concept va définir l'aspect graphique, le nom des rubriques, le "ton" du site, nourrir les fonctionnalités proposées
                        => on va pouvoir décliner des fonctionnalités
                            -> on se met à la place de l'utilisateur et les fonctionalités vont me permettre de sélectionner un produit... on se trouve donc sur l'interface
                                => c'est un énoncé de résultats MAIS pas de solutions
                                
                    Une fois toutes ces fonctionnalités validées et vérifiées qu'elles sont complètes, par le client on va pouvoir passer en production du projet :
                     => le chef de projet va demander à chaque personnes de son équipe d'estimer le temps nécessaire à la réalisation du produit
                        => une fois ce retour fait il va pouvoir mettre en place 1 planning
                        => il va faire le cahier des charges qui se compose de:
                            - la note de cadrage
                            - liste des fonctionnalités (on ne parle pas encore des solutions à apporter)
                            - planning
                            - attribution des tâches
                            
                    => on a une liste exaustive de fonctionnalités => on va commencer à les prioriser -> ce qui permet de commencer à structurer le site, voir comment intégrer les contenus, envisager un système de promotion du produit
         
        3. CONCEPTION:
            C'est une phase forte de définitions et d'organisation.
                -> on définit le contenu => on peut proposer au client une 1ère arborescence (structure du site sus forme de schéma)
                -> on définit la charte éditoriale => le "ton", les mots clés
                -> on s'occupe du plan de référencement (SEO)
                -> on définit la charte graphique et ergonomique du site (principes visuels et de navigation de chaque page: il faut une homogénéité)
                -> on fait des wireframes(zoning) et des maquettes (retravaillées avec les couleurs et les polices à utiliser)
                -> lister les fonctionnalités (ex: 1 bouton fait ça, ...)
                -> lister les spécifications techniques du projet
                
                -> le plan marketing: tout ce qui va pouvoir promouvoir le site: emails, flyers, vidéos, resaux sociaux, boutiques physiques, affiches (avec qr code)....
                
                -> il doit, une fois les maquettes validées, expliquer les spécifications techniques de manière détaillée pour que le dev sache quoi faire ET pour que le client les valide en s'étant assurer que tout est réalisable techniquement.
        
        
        4. PRODUCTION:
            Cette phase démarre par la livraison des maquettes et des spec aux dev  => c'est le codage
            Le role du chef de projet est de vérifier que le developpement correspond bien au cahier des charges et aux spec et de faire des tests de validation du produit.
            Il doit rédiger un cahier de tests:
                - noter toutes les fonctionnalités qui doivent être présente et inscrire si c'est ok ou non
                    => si c'est non -> faire remonter les nons au dev. Ca se fait généralement par une pateforme où on inscrit toutes les erreurs et où on note les solutions qui ont été faite pour les traiter.
                    On peut aussi catégoriser ces erreurs: urgentes, esthétiques, fonctionnelles...
            Le client doit aussi faire des tests qui correspondent à la recette du produit.
            Pour cela le chef de projet rédige un cahier de recettes qui liste l'ensemble des fonctionnalités (front et back).
                -> il doit en faire un retour
                    -> si négatif (des fonctionnalités qui ne marchent pas) -> le chef de projet doit avec le dev résoudre le problème
                    
            Cette phase se termine par la signature d'un PV de recettes: document qui confirme que les demandes du client définies ultérieurement sont atteintes   => le site peut être mis en ligne        
        
        
        5. EXPLOITATION ET MAINTENANCE:
            Si on est chef de projet interne on est en maintenance
            Si on est chef de projet externe on est dans 1 garantie: mise à jour des erreurs qui pourraient survenir 
            
            C'est la phase de suivi.
            
            
            
DEVENIR UN BON CHEF DE PROJETS


    Les grandes fonctions du chef de projet:
        - délimiter
        - concevoir
        - piloter
        - coordonner
        
        A chacune de ces grandes fonctions il y a des livrables à produire. Un livrage est un document.

    1. DELIMTER LE PROJET
    
        C'est analyser, comprendre et cadrer le projet.
        
            - définir le périmètre projet: ensemble des résultats attendus par le client.
                -> ça permet d'avoir une vision globale et finale du projet
                -> ça permet au client et aux équipes de se comprendre (même vision du produit final)
                
            => les livrables sont la note de cadrage ET les spécifications fonctionnelles
            
            
            * Note de cadrage:
                
                En collaboration avec le client vont être défini un certain nombre d'élements.
                    - enjeux        -> fidéliser; acquérir de nouveaux clients....
                    - objectifs     -> refonte de site
                    - cibles        -> jeunes; vieux...
                    - budget        -> en fonction des services à voir avec le client et les équipes
                    - délais        -> en fonction des services à voir avec le client et les équipes
                    
                Cette note sert à avoir une vision globale du projet et sert à préciser les résultats attendus.
                
                Avant de faire cette note il est conseillé de faire de la recherche pour conseiller au mieux le client.
                
                    - étudier la concurrence (connaitre les concurrents directs et indirects)
                    - forces et faiblesse du client
                    - existant: quelles sont les données que le client possède déjà et qu'on sera suceptible de reprendre
                    - les cibles: qui sont-elles? age? pratique? identité?      => personnas
                        -> avec ces personnages on va pouvoir commencer à penser aux différentes fonctionnalités et à l'arborescence du site
                        
                => on a cadrer les besoins du client
                
            * Specifications fonctionnel:    
            
                => on va trouver des fonctions pour répondre aux besoins du client définis dans la note de cadrage
                
                ex: si le client à besoin de communiquer avec ses utilisateurs => abonnement à 1 newsletter
                
                => dans ce cahier des charges fonctionnel on va lister des fonctionnalités: description d'un élément en terme de services rendus
                    -> on va expliquer ce qu'on peut faire ou ne peut pas faire avec ET aussi les contraintes liées à cet fonction
                        ex: newsletter      -> s'abonner, se désabonner et dans les contraintes: si l'utilisateur n'accède pas au message il pourra cliquer sur un lien pour y accéder en ligne
                        
                        
                Une fois ce cahier des charges fonctionnel réalisés  on y associe le cahier des charges techniques (écrit par le dev) et on pourra après validation du client passer à la conception.      
        
    2. CONCEVOIR
    
        C'est imaginer le futur produit.
        
        On va réaliser l'architecture du futur produit. Une fois cette architecture posée, on va piloter les équipes internes pour qu'elles réalisent les pages du site et qu'elles agencent les contenu pour répondre au besoin du client.
        
        Le livrable est ici l'arborescence du site (son architecture) qui vont illustrer les différents niveaux avec leur différents accès.
        -> on peut savoir maintenant quel contenu on va y mettre    -> on va pouvoir créer des wireframes et maquettes
        => il faut maintenant rédiger les specifications fonctionnelles : expliquer comment fonctionnent les éléments présents sur la maquette
        
        Ces spec sont là pour expliquer au dev comment il doit développer ces fonctions sur le produit (ex: champ de saisie ou menu déroulant, autocompetion, ...)
            ex: autocompletion      -> il faut se connecter à 1 base de données pour avoir accès à ces mots et dans le cas où le mot n'existe pas quoi faire?
                                        -> redirection vers une nouvelle page expliquant à l'utilisateur que sa recherche n'a pas fonctionner et peut-être expliquer comment faire
                                        
                                        
        Les spécifications fonctionnelles sont remise au dev, on peut travailler avec lui pour les spec techniques.
        Une fois fait on peut passer en phase de production.
        
        
        
    
    3. PILOTER
    
        C'est réaliser un ensemble d'indicateurs qui vont permettre de suivre le bon déroulement du projet.
        
        Quels sont ces indicateurs:
            - nombre de jours passés sur 1 projet
            - réalisation d'un livrable
            - réalisaion de taches
            - avancement d'une tache
            
            
        Le role du chef de projet est de réaliser les documents  qui permettent de suivre ces indicateurs.
        
        Les documents qui permettent de piloter 1 projet son le planning et le budget.
        
        * Le planning:
        
            C'est l'organisation de la production:
                - réaliser l'ensemble des taches et la façon dont elles se coordonnent.
                Un ensemble de taches accomplies amènent à la réalisation d'un livrable (ex: wireframes)
                    => il faut tout d'abord lister l'ensemble des taches
                    => une fois cette liste établie on va pouvoir les planiifer
                    
            Au début d'un projet on part sur 1 planning prévisionnel qu'on va affiner au fur et à mesure.
            
            -> Pour effectuer le planning on va avoir à faire à des compétences:
                - créative
                - dév ....
            -> Une fois ces poles de compétences définis on va pourvoir lister les attendus par compétences    
            -> une fois cahque attendu définis on va pouvoir les découper en livrables par pole
            -> on peut définir les taches
            -> on peut donner ces taches aux personnes de l'équipe
            -> on peut maintenant estimer le temps  => faire un planning
            
            
            CONSEILS:
            
                - détailler au maximum les taches à définir ce qui permettra de convaincre le client du temps qu'il faut leur allouer
                - faire démarrer des taches les une en parrallèle des autres pour ne pas perdre de temps
                - on peut faire un planning détaillé pour le client MAIS en faire 1 aussi pour ses équipes (ce n'est pas forcément le même)
                - faire en sorte que tout le monde est accès à l'avancement du projet et aux dates de rendu
                - comme les spec détaillées ne sont pas encore définies à ce moment il faut donner une estimation du temps
        
        * Le budget:
        
            Les enjeux et les difficultés:
            
                - donner un budget lorsque le projet n'a pas démarré n'est pas simple
                - tenir son budget
                
                
            Construire son budget:
            
                2 positions différentes: interne ou externe
                
                - interne       -> cout des ressources humaines et des machines
                                    humaine: ensemble des salaires brut + charges patronales + charges salariales
                                    
                - externe       -> calculer le nombre de jours pour effectuer le projet et définir le prix d'un jour-homme:
                                        cout d'un profil(550 € jour) + marge (20%)
                
                
    
    4. COORDONNER
            
        C'est coordonner les tâches ET les acteurs du projet.    
        
        Il y a plusieurs livrables qu'on va récupérer mais pas rédiger:
            - cahier des charges techniques
                environnement du client, format de livraison final du produit
                    environnement client: existant: url de livraison, url de test, matériel utilisé par le client, hébergeur, la sécurité
                    format: à remettre au client pour la mise en ligne
                    
                    
            - wireframes (dessins des pages)
            - charges ergonomique (navigation), graphique, éditoriales
            - maquettes
            
            . wireframes  -> maquettes succintent avec du faux texte
            . Dans la charte graphique on trouve le logo, les polices, les couleurs avec leurs références, les tailles de polices, les icones, les boutons
            . La charte ergonomique parle du système de navigation et de l'agencement des pages (homogène), conception des formulaires, traitement des attentes
            . maquettes -> wireframes avec les chartes graphique et ergonomique
            . charte éditoriale -> la façon dont on va communiquer, écrire les textes, dont on va s'adresser aux utilisateurs (formels ou non par ex)
            
            
            
MENEZ VOTRE PROJET AVEC SUCCES


        Les 2 buts principaux sont que le projet arrive à terme dans le temps et dans le budget défini.
        
        Compétences du chef de projet:
            
            - analyse                   -> il faut intégrer l'univers du client qui est différent à chaque projet:
                                                - univers concurentiel
                                                - tenant et aboutissant du projet
                                                
            - organisation              -> c'est le chef de projet qui organise tout le dérooulé du projet:
                                                - découper les taches
                                                - les donner aux bonnes personnes...
                                                
            - gérer le périmètre projet -> pour cela il faut s'appuyer sur les livrables (le client faut souvent des nouvelles demandes hors périmètres)        
            - coordonner                -> flux d'informations et acteurs nombreux
                                                - savoir transmettre ces infos au bon moment et aux bonnes personnes
                                                - savoir dialoguer avec differents corps de metier et répondre à leurs questions
            
            - communiquer               -> vous avez les connaissances métiers web, techniques, ... que le client n'a pas. 
                                            Il va falloir lui expliquer correctement les problèmes que vous pouvez rencontrer
                                            ET SURTOUT comprendre ses attentes pour pouvoirs les expliques à vos équipes
                                                -> faire un compte-rendu qui acte les principaux points validés
                                                -> brief des équipes; ne pas seulement rapporter le besoin
                                                
            - gérer les conflits        -> rappeler les responsabilités de chacun                                    