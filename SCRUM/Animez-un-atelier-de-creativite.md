########## ANIMEZ UN ATELIER DE CREATIVITE ##########


EVALUEZ LES BESOINS ET LES CONDITIONS DE REUSSITE


        Il n'existe pas d'atelier type.
        Chaque atelier va s'adapter à un besoin en particulier.
        
        
            1. DEFINISSEZ LE BUT DE L'ATELIER
            
                Pour cela répondre aux questions:
                    - comprendre la situation actuelle (pourquoi elle est bloquante) et celle désirée (pourquoi il faut l'atteindre) : QUOI
                    - connaître un minimum les individus qui vont participer aux ateliers : QUI
                    - savoir quelles sont leurs attentes et leurs contraintes : COMMENT
                    - avoir conscience de l'objectif sous-jacent : POURQUOI
                    
                En tant qu'organisateur, animateur ou encore facilitateur, ou en tout cas, si vous êtes dans la boucle pour prévoir un atelier, il est primordial que vous vous posiez la question de savoir POURQUOI vous allez faire cet atelier, et quels sont ses objectifs.
                Est-ce un atelier qui a vocation à :
                
                    - aboutir à une innovation de service dans l'entreprise ?
                    - rendre les participants plus créatifs ?
                    - résoudre un problème dans l'entreprise ?
                    - instaurer une nouvelle culture, de nouvelles pratiques agiles ?
                    - initier à la résolution de problème (problem solving), au design thinking… ?
                    - déclencher un déclic, un changement de paradigme dans la tête des gens ?
                    - redonner confiance aux individus sur leur capacité à être créatif ?
                    - trouver un nom pour la dernière innovation née au sein de votre entreprise ?
                    - définir une charte graphique ?
                    - établir une stratégie d'avenir pour votre structure ?
                    - ...
                    
            2. REDIGEZ UN PLAN D'ANIMATION
            
                En fonction de la réponse à cette question, le contenu de l'atelier va changer.
                    -> S'il s'agit d'aboutir à une innovation de service, il faudra sûrement organiser un brainstorming ciblé sur le problème à résoudre. Alors que s'il s'agit d'instaurer une nouvelle culture agile, il faudra sûrement préparer plusieurs activités /jeux qui vont servir à prédisposer les mentalités à changer.
                    
                Rédiger un plan d'animation pour un atelier de créativité, dites-vous que c'est comme dessiner un parcours créatif pour les participants.
                    => Vous devez penser à où vous voulez les amener.
                    -> Ce que vous voulez qu'ils pensent/fassent/ressentent à la fin de l'atelier va vous guider pour organiser le contenu.
                    
                    
                    - Démarrez l'atelier par un coup d'envoi (kick-off)
                        -> C'est le moment où vous, animateur, allez présenter l'atelier, son déroulé et son objectif, tout en expliquant aux participants l'état d'esprit dans lequel ils doivent se trouver pour que l'atelier se déroule au mieux (= respecter et écouter les autres, rebondir sur les idées des autres, ne pas se juger, etc.).
                        
                    - Mettez à l'aise et brisez la glace (ice-breaker)
                        -> En effet, les participants ont souvent peur d'être jugés, observés pendant un exercice de brainstorming et d'emblée auront le réflexe de penser qu'ils ne sont pas créatifs. Il faut donc les rassurer sur la question. Il faut qu'ils s'expriment librement.
                        
                    - Lancez le brainstorming
                    
                    - N'oubliez pas de faire des pauses
                    
                    - Terminez par un débriefing
                        -> En général, suite à un brainstorming, la bonne pratique est de prendre une vue globale sur toutes les idées qui ont été émises, de les classer par grands thèmes puis de procéder à un vote : le mieux est de ne pas se focaliser sur une seule idée, mais d'en sélectionner entre 3 et 5 ; les meilleures.
                        
            3. ETABLISSEZ DES CRITERES DE REUSSITE
            
                On oublie souvent de définir ce qui fait que l'on saura si l'atelier a été un succès ou un échec.
                En général, c'est parce qu'il aura répondu au besoin initial :
                        - aboutir à une innovation
                        - trouver un nom pour votre nouvelle start-up
                        - initier à la pensée design
                        - ...
                        
                Etablissez des critères pour pouvoir le mesurer et affirmer qu'un atelier a fonctionné:
                        - critère 1 : l'innovation respecte l'environnement.
                        - critère 2 : l'innovation est socialement souhaitable.
                        - critère 3 : l'innovation répond au besoin des utilisateurs.
                        - critère 4 : l'innovation coûte moins de 20 000euros à mettre en place.
                        
                    N'oubliez pas de le faire, cela vous rendra un grand service et légitimera l'exercice. Ne serait-ce que parce que parfois un brainstorming n'est pas toujours facile à justifier en entreprise ; jugé comme non sérieux, ou comme une perte de temps.
                
                
                
ANTICIPEZ LES RISQUES D'UN BRAINSTORMING DIFFICILE


        Un atelier difficile, est un atelier qui a rencontré au moins un de ces problèmes :

            - l'énergie du groupe est restée à un niveau bas, il n'y a pas eu d'émulation, peu de personnes ont participé activement, et les idées qui sont ressorties de l'atelier ne sont ni stimulantes, ni originales, ni rafraichissantes.
            - l'objectif de l'atelier n'a pas été défini en amont, ce qui fait que l'on ne sait pas trop quoi faire de ce qui vient de se passer ; ça tombe à plat et aucune suite ne sera donnée.
            
        Ces problemes sont liés à des risques: humain ou technique.
        Voici une liste des riques:
        
            1. QUAND PERSONNE NE SAIT CE QU'IL FAUT FAIRE
            
                    -> L'atelier n'est pas préparé: il n'a pas d'objectif précis
                        -> tout va partir dans tous les sens sans rien de précis
                        
                ==> Donner une direction à un atelier est la première étape ; il doit répondre à un besoin. Il faut également l'annoncer aux participants : pourquoi ils sont là, ce qu'il vont faire.
                
                    -> L'organisateur ne joue pas son role: Le rôle de l'organisateur n'est pas de contrôler l'atelier, mais de lui donner des ailes.
                        Vous n'êtes pas d'être le chef de l'atelier.
                        
                        Comprenez bien que dans la plupart des cas, les gens ont peur d'exprimer leurs idées voire même de parler en public. Alors si vous rajoutez du stress à l'exercice, vous allez complètement passer à côté.
                        
                ==> Séquencez l'atelier en plusieurs petits jeux ou activités courtes, et expliquez toujours les règles du jeu ; dans la bienveillance et avec ferveur.
            
            2. QUAND LA MAYONNAISE NE PREND PAS
            
                    -> Le groupe n'est pas d'humeur:
                        -> Il est très important de ne pas former des groupes de participants au hasard : il vous faut les sélectionner avec soin pour assurer une bonne synergie.
                        
                        Au minimum, invitez à l'atelier des personnes qui sont :
                            - intéressées par le principe de participer à un atelier,
                            - intéressées par la thématique de l'atelier.
                            
                        Et si possible, choisissez des personnes qui sont :
                            - en général de bonne humeur / positives / optimistes / drôles … (au choix)
                            - de bons éléments de l'entreprise (motivées et passionnées par leur travail).
                            
                    -> Le contexte ne donne pas envie:
                        -> Pensez à humaniser l'espace où va se dérouler l'atelier ! À le rendre chaleureux, agréable…
                        -> Faites l'atelier le matin quand les participants sont frais et dispos.
                        -> Faites en sorte que l'atelier se passe dans une salle qui se prête à l'expérience. (laissez de la place pour que les personnes puissent bouger, se lever -> ça augmente la créativité)
                        -> Rendez l'exercice confortable et agréable (prévoyez des viennoiseries et du café, et faites régulièrement des pauses dans l'atelier).
                        -> Le mobilier et le matériel nécessaires sont présents et adéquats.
                        
            3. QUAND IL Y A UN ELEMENT PERTURBATEUR DANS LE GROUPE
            
                    -> Le bavard:
                        C'est celui ou celle qui monopolise la parole, il ou elle a toujours une opinion et des idées prêtes sur chaque sujet, aime plaire et prend pour challenge de convaincre les autres.
                        
                    -> L'egocentrique:
                        C'est celui ou celle qui veut faire passer ses idées avant celles de tout le monde, parce qu'elles sont supposément meilleures que les autres. 
                        
                        On reconnait ce profil de la façon suivante :
                            - Dès le début de l'atelier il ou elle va dire ce type de phrases : "Avant de commencer la séance, je voulais vous dire que j'avais déjà une idée et je pense que c'est une bonne idée parce que (argument 1 + argument 2 + argument 3)…" ;
                            - il ou elle va répéter des variantes de la même idée inlassablement jusqu'à ce qu'elles soient prise en compte ;
                            - il ou elle va rebondir sur les idées des autres en essayant de ramener le débat autour de son idée.
                            
                    -> Le démotivateur:
                        C'est celui ou celle qui casse les idées de tout le monde.
                        
                        On le reconnait facilement, c'est celui qui, dès qu'une idée a été exposée, va dire : "Oui mais…"
                        Il est sincèrement convaincu du bien qu'il apporte.
                        
                ==> Dans un brainstorming on doit laisser les idées se dessiner librement et EN QUANTITÉ.
                
                
PREPAREZ DES SUPPORTS PHYSIQUES OU NUMERIQUES

        
        Pour qu'un atelier de créativité soit stimulant et efficace, il faut prévoir, voire concevoir soi-même des supports personnalisés et spécifiques pour l'atelier en question.
        
            1. PREVOYEZ UN DIAPORAMA DE PRESENTATION
            
                    Un support de présentation sert à la fois de sommaire et de boussole : il présente les différentes étapes de l'atelier (une étape par slide) et permet de savoir où en en est dans l'atelier. Il annonce la couleur, le timing, les pauses.
                    
                    Il ne doit pas contenir beaucoup de texte.
                    
                        - Slide 1 : Kick Off
                        - Slide 2 : Ice Breaker
                        - Slide 3 : Activité 1
                        - Slide 4 : Pause café
                        - Slide 5 : Activité 2
                        - Slide 6 : Pause
                        - Slide 7 : Activité 3
                        - Slide 8 : Fin
                        
            2. AFFICHEZ LES REGLES DU JEU
            
                    Un brainstorming se déroule beaucoup mieux lorsqu'il est cadré.
                    Il faut réussir de (1) permettre aux participants de se sentir libres d'exprimer leurs idées tout en (2) les guidant dans un cadre de réussite.
                    -> vous allez annoncer dès le début de l'atelier les règles du jeu pour que l'atelier se passe en bonne intelligence.
                        - Ne jugez pas, ni vous-mêmes, ni les autres
                        - Rebondissez sur les idées des autres.
                        - Privilégiez la quantité (et non la qualité).
                        - Laissez-vous porter.
                        (- Identifiez un facilitateur.)
                        
                    -> Pour une efficacité optimale, vous devez les présenter une à une et les laisser affichées dans la pièce pour que tout le monde les garde toujours à l'esprit.
                    
            3. FABRIQUEZ LES OUTILS DE L'ATELIER
            
                    Au cours d'un atelier de créativité, vous allez proposer un certains nombre d'activités.
                    Ces activités sont supposées être stimulantes par nature, mais c'est encore mieux si vous avez prévu des supports physiques qui vont venir justement soutenir vos intentions et ce que vous attendez des participants.
                    
                        -> Matrice Morphologique:
                            Ce sont des cartes que vous pouvez fabriquer et découper vous-mêmes.
                            Le principe est simple : il s'agit de créer entre 2 et 3 catégories de cartes afin d'amener les participants à croiser des concepts entre eux dans le but de trouver des idées auxquelles ils n'auraient pas pensé sans cela.
                            
                        -> Carte d'empathie:
                            Ce support est très connu et utilisé ; en anglais on dit "Empathy map" ; et il consiste à analyser les besoins des utilisateurs.
                            Très utilisé dans le domaine de la recherche utilisateur.(voir image jointe: empathy-map.png)
                            
                            C'est une carte qui a 6 espaces:
                                - Ce que l'utilisateur ressent et pense.
                                - Ce que l'utilisateur entend.
                                - Ce que l'utilisateur voit.
                                - Ce que l'utilisateur dit et fait.
                                - Ce que l'utilisateur craint.
                                - Ce que l'utilisateur espère et attend.
                                
                        -> Personas:
                            Excellent moyen de se mettre dans la peau des utilisateurs pour qui on crée un produit ou un service.
                            Cela consiste à créer des cartes sur les différents profils utilisateurs avec pour chacune :
                                - Une photo (ou une représentation graphique du profil)
                                - Un nom
                                - un age
                                - Un métier / une activité
                                - Des traits de caractère
                                - Un besoin
                                
                        -> Palette d'émotions:
                            Pour creuser encore plus loin dans l'UX et la compréhension de l'utilisateur, on peut créer des cartes "Emotions".
                            Cette pratique est très utilisée pour définir les scénarii utilisateurs et renforcer un service client.
                            
                        -> Journey Map:
                            La "journey map" consiste à reproduire dans le détails, étape par étape, l'ensemble des activités d'un utilisateur dans l'utilisation d'un produit: un sénario utilisateur.
                            
                            Le principe : décortiquer une à une toutes les tâches que va effectuer un utilisateur pour arriver à ses fins dans l'utilisation d'un produit ou d'un service
                            
                            
OCCUPEZ VOUS DES DETAILS LOGISTIQUES


            1. INFORMEZ LES PARTICIPANTS
            
                    - date de l'atelier
                    - durée de l'atelier
                    - objectifs de l'atelier
                    - thèmes de l'atelier
                    - nombre de participants
                    - nom des participants
                    
            2. RESERVEZ UNE SALLE EN AVANCE
            
                Une salle doit:
                    - être bien adaptée à l'exercice
                    - disponible le jour J et pour toute la durée
                    
            3. APPORTEZ TOUT CE QUI EST NECESSAIRE
            
                    - Les supports des activités que vous avez prévu et de les avoir photocopiés en nombre nécéssaire
                    - De quoi écrire, découper, coller afin que les participants puissent être efficaces ; ils ne penseront pas à venir eux-mêmes équipés, donc faites bien attention.
                    - De quoi se sustenter
                    
                    
LES DIFFERENTES PHASES DU BRAINSTORMING: KICK-OFF


            Objectifs:
                        - Accueillir les participants.
                        - Présenter le but et la structure de l'atelier.
                        - Expliquer les règles du jeu.
            Protocole:  Tous les participants sont présents, assis, face à l'animateur qui présente l'atelier.
            Durée:      5 minutes 
            Supports:
                        - Affiche du plan de l'atelier de créativité
                        - Affiche des règles d'or du brainstorming
            Materiel:   Un tableau pour maintenir les affiches.
            
            L'objectif d'un "Kick Off" est de prédisposer les participants à ce qui va suivre. Le "Kick Off" doit donner envie aux participants d'avancer sur la suite de l'atelier, et non les stresser ou leur donner envie de partir.
            
            Le "Kick Off" peut avoir lieu dans un autre endroit que celui de l'atelier, de préférence à l'extérieur.
            
            


LES DIFFERENTES PHASES DU BRAINSTORMING: ICE-BREAKER

            Objectifs:
                        - Briser la glace
                        - Faire connaissance
                        - Mettre les participants à l'aise
            Protocole:  Tous les participants sont assis ensemble à coté du briseur de glace (ex: magicien qui fait un tour)
            Durée:      10 minutes
            Supports:   aucun
            Materiel:   celui du briseur de glace (ici cartes si tour de cartes)
            
            L'objectif est de briser la glace entre les participants et créer un climat favorable à la réflexion collective.
            
            Briser la glace se fait souvent au travers d'un jeu. Il faut faire retrouver leur âme d'enfant aux participants.
            
            Autres possibilités du ice-breaker:
                - faire bouger les participants: se mettre debout, respirer, marcher, se relaxer, ...
                - faire un jeu de construction en équipe: Marshmallow Challenge
                - faire faire connaissance aux participants: mini-jeu qui permet d'apprendre et de retenir les prénoms des gens, voire aussi ce qu'ils font dans la vie.
                

LES DIFFERENTES PHASES DU BRAINSTORMING: WARM-UP


            Objectifs:
                        - Echauffer les participants en douceur
                        - Initier les participants à la pensée divergente (outside the box)
            Protocole:  petit jeu ou chacun doit répondre à son en suivant des consignes définies
            Durée:      1 à 5 minutes
            Supports:   aucun
            Materiel:   aucun
            
            Il peut y avoir plusieurs activités lors du warm-up.
            Si vous faites plusieurs activités au sein d'un même atelier, c'est plus logique de commencer par quelque chose de simple pour s'échauffer.
            
            L'objectif est d'initier les participants à la pensée divergente, mais aussi de les mettre dans le bain et leur faire comprendre qu'il ne sert à rien de juger ses propres idées, et que les autres ne vont pas juger leurs idées respectives.
            
            
            Autres possibilités d'activités du warm-up:
                - faire un jeu de role: improviser "libère" son imagination
                - inventer des noms ou utilisations à des objets
                - lancer une énigme à résoudre


LES DIFFERENTES PHASES DU BRAINSTORMING: DEFOULOIR


            Objectifs:
                        - Libérer les participants de leur complexe et de leur jugement
                        - Amener les participants à lâcher prise (donner des idées "ridicules")
                        - Initiez les participants au "rebond créatif" via le croisement de concepts
            Protocole:  L'animatuer indique les consignes, Les participants s'expriment dès qu'une idée leur vient, la marquent sur un post-it et la mettent dans la jarre.
            Durée:      10 minutes
            Support:    celui amené par l'animateur en fonction de l'activité
            Materiel:   post-it; stylos; jarre
            
            L'objectif du defouloir et de permettre de lacher prise. En effet les participants se jugent souvent eux-même avant de donner leurs idées. C'est pour éviter cela, chaque idée est potentiellement bonne, elle peutt aussi permettre à 1 autre participant de rebondir.
            
            Autres possibilités d'activités du defouloir:
                - associer des concepts entre eux
                - favoriser le rebond créatif: on reprend une activité avec "Et si..." (et si j'étais riche....)
                - exagérer les variables du sujet: temps; quantité; ... (temps: et s'il pleut le matin, que fais-je?...; Et si je n'ai que 10 minutes....)


LES DIFFERENTES PHASES DU BRAINSTORMING: COLOR-RUN


            Objectifs:
                        - Générer un maximum d'idées.
                        - Pratiquer le brainstorming en roue libre.
                        - Réutiliser les principes vus précédemment
            Protocole:  A la suite des consignes, Les participants ont chacun leur bloc de post-it ; ils écrivent leurs idées sur des post-it qu'ils collent sur la table. 
            Durée:      5-6 minutes
            Support:    aucun
            Materiel:   post-it; stylos
            
            Cette phase a pour but de réutiliser les bonnes pratiques vues dans les phases précédentes.
            C'est la raison pour laquelle, elle intervient plutôt vers la fin de l'atelier : en effet, à ce stade, les participants sont plus détendus, ils ont compris le principe de ce qu'ils font et pourquoi, et ils se sentent suffisamment en confiance pour générer des idées. On peut donc les laisser en roue libre sur un brainstorming classique.
            
            Le fait que les blocs de post-it soient de couleur différente permet d'insuffler un esprit de compétition ludique que les participants sont alors en mesure d'accepter puisque cela se fait dans la bonne humeur et la bienveillance. 
            
            Les bonnes pratiques:
                - favoriser le rebond créatif
                - mettre à disposition  1 grand support d'idées (ici post-it, mais tableau, vitre, ...)
                - favoriser l'intelligence collective
                - chronometrer pour créer une urgence


LES DIFFERENTES PHASES DU BRAINSTORMING: VOTE

            Objectifs:
                        - Avoir une vue d'ensemble des idées générées.
                        - Partager en groupe les idées émises par chacun.
                        - Trier les idées en grands thèmes.
                        - Sélectionner 9 idées.
                        - Evaluer les idées sélectionnées sur la base de 5 critères
            Protocole:  Les participants partagent leurs idées au groupe en même temps qu'ils les trient par grands thèmes. Ensuite, chacun choisi ses trois idées favorites sur une base subjective, les dessine puis les évalue à la lumière des critères objectifs.
            Durée:      6-7 minutes
            Support:    fiche d'évaluation
            Materiel:   pots-it; stylos
            
            Ce moment est important car il a pour objectif de mettre en avant le meilleur de ce qui a été produit pendant l'atelier.
            C'est essentiel de terminer sur une prise de recul afin d'analyser les idées, les classer, les ranger, choisir les meilleures (selon les critères établis en amont) et évaluer ces idées sur la base de variables objectives comme la faisabilité par exemple, ou encore le coût.
            
            
PRENEZ EN COMPTE LE REOUR DES PARTICIPANTS

        
            Prenez un court moment à la fin de l'atelier pour demander aux participants ce qu'ils 
                        - ont pensé de l'atelier,
                        - ont appris,
                        - auraient aimé faire mieux,
                        - ont le plus apprécié,
                        - ...
                        
                        
SAUVEGARDER UNE TRACE ET CONSERVER LES IDEES


            1. PRENDRE DES PHOTOS
            
                Permet de se rememorer les activités, l'energie, la bonne ambiance....
                Si vous prenez des photos des participants et qu'en plus vous les accrocher au mur, non seulement vous mettez en valeur des personnes (qui vont par là même se sentir plus appréciées, importantes et donc confiantes), mais vous diffusez au reste des collaborateurs un message positif (qui vont se sentir autorisés à faire les mêmes choses).
                
                Quand l'atelier est terminé, pensez à prendre en photo les idées dans leur ensemble. Que se soit une table, une vitre, un mur, un tableau que l'on a collé des idées, c'est bien de capturer ce paysages de couleurs, de mots, et d'idées.
                
                
            
            2. CONSERVER LE MATERIEL
            
                Les supports, les idées....
            
            3. COLLECTER LES EVALUATIONS
            
                Ces évaluations permettent de graver une réflexion qui a eu lieu et de voir en un clin d'oeil le résultat sur lequel un atelier a abouti. C'est crucial et cela légitime, renforce la valeur de l'atelier.
                
                
FAIRE UN COMPTE RENDU POUR LES PARTIES PRENANTES

            
            1. RAPPELER LE CONTEXTE
        
                -> Les variables logiques:    
                            - date de l'atelier
                            - durée de l'atelier
                            - objectifs de l'atelier
                            - thèmes de l'atelier
                            - nombre de participants
                            - nom des participants
                -> l'objectif de l'atelier:
                            Reprenez en un court paragraphe l'objectif de l'atelier en expliquant pourquoi il a été organisé, dans quel contexte, pour répondre à quels besoins.
                
            2. RACONTER CE QU'IL S'EST PASSE
                
                -> Détailler le plan d'animation:
                            - activité 1: ......(nom; objectif; consigne; protocole; materiel)
                            - pause
                            - activité 2: ......(nom; objectif; consigne; protocole; materiel)
                            - pause
                            - ...
                        Si vous avez pris des photos des activités pendant l'atelier, vous pouvez les intégrer aux endroits correspondant du plan d'animation.
                -> Transcrivez les idées retenues:
                            Placez la ou les photos du "canevas" des idées(photo de tous les post-it par ex) pour montrer ce qui a été produit dans l'ensemble.
                            Il y a eu des grands themes: nommez et décrivez ces catégories d'idées dans le compte rendu.
                            Annoncez les idées qui ont été sélectionnées et montrez leurs valeurs.
                            
            3. EVALUER LES SUCCES ET LES ECHECS DE L'ATELIER
            
                -> Intégrer des verbatims:
                            Des témoignages courts ou phrases clés qui ont été dites/entendues pendant l'atelier et/ou à partir du questionnaire de satisfaction que vous aurez envoyé aux participants.
                -> Representer le niveau de reussite:
                            Que ce soit par un schéma, un tableau, des chiffres… vous devez montrer et expliquer en quoi l'atelier a rempli son objectif ou non. Ce sont les critères établis en amont qui vont vous aider à établir le diagnostique.
                -> Faire une ouverture:
                            Expliquer ce qu'est la suite de l'atelier.
                                -> prochaine étape, avec qui, objectif, ...
                                
        ATTENTION: Ce compte rendu est fait pour les parties prenantes (client, collaborateurs, participants, managers, ...).
        
        
POUR FINIR


        Penser à remercier les participants (avec une certification par exemple).
        Envoyer un questionnaire de satisfactions.  -> à la fin de l'atelier les participants doivent rester dans une "détente" => envoyer ce questionnaire quelques jours après.(pas plus de 10 questions)
        Tenir les participants au courant de ce que l'atelier a permis de raliser, de leur contribution, ce qu'il s'est passé ensuite....