########## GESTION DE PROJETS AVEC LA METHODE AGILE ##########


DEFINITIONS

        1. Les individus et leurs interactions
            - Échecs ou réussites, ce sont bien vos équipes qui sont responsables des projets. Votre prise en compte du facteur humain est donc essentielle.
            - C’est incontournable, vous devez intégrer les forces et surtout les faiblesses des interactions humaines dans votre gestion de projet agile.
            - L’agilité propose des solutions pour souder, dynamiser et responsabiliser vos équipes au lieu de les mettre en concurrence.
            
        2. Logiciels (produits, services, etc.) opérationnels
            - Les méthodes agiles se veulent avant tout pragmatiques, c’est donc l’opérationnel qui prime toujours. Votre équipe est à la fois auto-organisée et autonome.
            - Les livraisons intermédiaires, régulièrement proposées à votre client, sont toujours une part cohérente et utilisable de votre solution complète.
            - La qualité est toujours évaluée en continu pendant votre gestion du projet.
            
        3. La collaboration avec les clients
            - Le contrat doit soutenir votre réussite du projet et non contraindre son exécution.
            - Les interactions fortes et permanentes avec votre client facilitent la négociation.
            - Les enjeux de l’agilité et ses principes sont bien compris et intégrés par votre client.
            
        4. L’adaptation au changement
            - Les entreprises doivent constamment faire preuve de réactivité face à la concurrence et aux attentes du marché.
            - L’accélération permanente du progrès technologique augmente l’obsolescence des applications qui sont contraintes de s’adapter en continu.
            - L’usage des réseaux sociaux a définitivement révolutionné l’approche des utilisateurs qui deviennent de plus en plus exigeants.
            - Le contexte économique oblige les équipes à être capables de produire efficacement de plus en plus de valeur pour les utilisateurs finaux.
            


##### CONSTITUEZ UNE EQUIPE #####        
        
MODELE TUCKMAN


        Former votre équipe, sans contraindre, sans brider et sans faire preuve d'autoritarisme.
        Exigences d'une équipe agile:
            1. Forming/Formation, les membres de votre équipe apprendront à se connaître et à se faire accepter. Les conflits latents ne seront pas ou peu abordés.
            2. Storming/Lancement, les membres de votre équipe se placeront les uns par rapport aux autres, se confronteront et pourront parfois entrer en conflit.
            3. Norming/Régularisation, votre équipe commencera à développer un cadre coopératif et collaboratif plus fluide. Les membres partageront des valeurs et des objectifs communs. La confiance s’installera dans votre équipe qui s’auto-organisera progressivement.
            4. Performing/Exécution, chaque membre de votre équipe sera interdépendant, autonome et compétent. La confiance sera bien installée ; but à atteindre pour gérer un projet efficacement.
            
        
        Dans 1 équipe agile, le manager, chef de projet, développeurs sont tous au même niveau. Ils vont travailler ensemble. Ils ont des objectifs communs.
            En créant une équipe "auto-organisée" on obtient de meilleurs résultats.
        Les méthodes agiles sont utilisées des les projets inovants où on a besoin d'itérations pour arriver au meilleur produit final.
        Il faut prendre en compte l'utilisateur final -> user stories
        
        
ALLER VERS LES AUTRES


    La première étape de constitution d’équipe (Forming/Formation) est une période calme en apparence, mais elle s’avère souvent décisive. Vous devez donc être particulièrement vigilant.
    
    Les membres de votre équipe se rencontrent, apprennent à se connaître, se testent entre eux, se positionnent les uns par rapport aux autres et découvrent la légitimité de chacun. Comment vont-ils travailler ensemble ? Comment seront évaluées leurs capacités et leurs compétences ? Vous allez être en mesure de lever toutes ces inquiétudes initiales.
    
    En qualité de chef de projet ou de facilitateur (coach agile, par exemple), les membres de l'équipe comptent sur vous ! Vous devez exposer clairement les buts de l’équipe et les objectifs du projet afin de donner une direction la plus précise possible.
    
        - Gardez des objectifs clairs.
        - Aidez à définir les rôles.
        - Créez du lien entre les participants.
        - Soutenez ceux qui font des propositions.
        - Formez les équipiers (théorie et/ou pratique).
        
    En créant un climat de confiance le plus tôt possible, vous pouvez avoir un impact déterminant sur la cohésion de l’équipe dès sa création.
    
    
    Le niko-niko (“niko” signifie “sourire” en japonais) est une pratique agile que vous pouvez mettre en place dès cette première étape de constitution de l'équipe. Au début, au milieu ou à la fin de chaque journée, tous les membres sont invités à partager leur état d’esprit du jour. 🙂😑 Ils indiquent ainsi sur un mur visible par chacun, avec un dessin ou des gommettes de couleur, leurs évaluations subjectives de la journée. L'intérêt de cette pratique, formalisée par Akinori Sakata, est de parvenir à objectiver la motivation ou le bien-être de l'équipe, facteurs habituellement subjectifs et difficiles à mesurer au cours de votre gestion de projet.
        => Le niko-niko est la mise en pratique d’un principe très important dans la gestion de projet agile
        
    Vous ne devez pas utiliser la pratique agile du niko-niko comme un outil de management paternaliste. Privilégiez plutôt la fraternité dans vos rapports avec l'équipe !
    
    Dans votre approche agile, vous devez surtout chercher à mesurer ce qui est pertinent ou ce qui est déterminant pour votre équipe et votre projet, même quand c'est quelque chose de difficile à évaluer. 
    
    
    RESUME:
        - Vous facilitez la rencontre des membres : en présentant les objectifs et les rôles, en valorisant les relations et les propositions, et en formant l'équipe aux méthodes agiles.
        - Utilisez la pratique agile du niko-niko pour afficher chaque jour l'état d'esprit des membres de l'équipe dans votre environnement de travail.
        - En qualité de coach agile, vous appliquez le principe de Gilb afin de rendre visibles différents indicateurs de motivation et de performance.
        
        
        
        
        
RENTREZ DANS LE VIF DU SUJET        
    
    
    La deuxième étape de constitution d’équipe (Storming/Lancement) est une phase compliquée durant laquelle votre équipe peut faire face à de nombreux conflits. Vous ne devez surtout pas essayer de reprendre la main de façon autoritaire, mais plutôt accompagner la transformation avec subtilité. 😎
    
    La confrontation des opinions et les divergences de points de vue peuvent faire apparaître de nombreuses tensions. Chaque membre cherche à se faire entendre, souvent au détriment des autres, pour imposer sa vision personnelle du projet. Si cette étape délicate est mal négociée, elle aboutira vite à l’éclatement de votre équipe ou à un changement de leadership.
    
    Vous aidez votre équipe à passer cette étape en vous assurant qu’ils s’écoutent, qu’ils comprennent le point de vue de chacun et qu'ils se respectent.
    
        - Installez un climat de confiance, de tolérance et d’empathie.
        - Aidez à maintenir le respect, l’écoute et le dialogue.
        - Rendez explicites certains problèmes et initiez une démarche pour les corriger.
        
    Lors de cette étape, vous avez pour rôle de réduire les tensions et de réguler les échanges. Vous devez en effet permettre à chacun de s’exprimer librement. Pour cela vous êtes le plus assertif possible, c’est-à-dire que vous prenez le rôle de leader, tout en respectant et en écoutant les différents membres de l’équipe. Vous gérez les conflits rapidement et ne laissez pas les discussions s’envenimer ou s’éterniser.
    
    
    La “boîte de temps” (time boxing) est une pratique agile que vous pouvez mettre en œuvre dès la deuxième étape de constitution de l'équipe. L'objectif est de réaliser une ou plusieurs activités en un temps limité et connu par tous.
        
        - Expliquez l’objectif aux participants.
        - Définissez les livrables attendus.
        - Indiquez la démarche qui va être utilisée.
        - Maîtrisez la répartition du temps.
        - Désignez un “gardien du temps” (time keeper).
        - Déroulez les activités prévues.
        - Vérifiez que les objectifs sont atteints.
        - Évaluez que les participants sont satisfaits.
        
    C'est une variante de la “date limite” (deadline) dont l’utilisation explicite pour une stratégie de gestion de projets.
    
    Les avantages de la boîte de temps sont nombreux car c’est une pratique qui favorise un travail à la fois performant et focalisé sur l’essentiel. Elle limite ainsi drastiquement le nombre de réunions et le temps total investi dans la gestion d’un projet. Les inconvénients relatifs à la courte période d’adaptation ne doivent surtout pas vous décourager ! 🏁 Faites-en un exercice ludique, sans compétition, afin de vous approprier cette pratique agile progressivement.
    
    
    Effectuer un bilan dès la fin du travail chronométré :
        - L'objectif est-il atteint ?
        - Si vous avez fixé plusieurs tâches, l’objectif est-il partiellement atteint ?
        - Dans la mesure du possible, il n’y a que deux états de complétion (0% ou 100%).
        
    Utiliser la technique Pomodoro: réitérer le travail d’équipe en plusieurs boîtes de 25 minutes (et 5 à 15 minutes de pause)
    
    
    RESUME:
        - Vous limitez les conflits entre les membres de l'équipe : en installant un climat de confiance, en privilégiant le dialogue et en explicitant les problèmes à résoudre
        - Utilisez la pratique agile des boîtes de temps afin de découper votre gestion de projet en périodes de travail égales et répétitives (méthode itérative).
        - En qualité de coach agile, vous entraînez votre équipe agile à la pratique des boîtes de temps avec la technique Pomodoro (itérations de 25 minutes).
        
        

DEVELOPPEZ LA PRODUCTIVITE


        La troisième étape de constitution d’équipe (Norming/Régularisation) est une phase où vous serez peut-être tenté de prendre des décisions à la place de votre équipe...
        
        La structuration de l’équipe doit rester votre objectif prioritaire. Pour y parvenir, la mise en place de règles acceptées par tous, la recherche de consensus et la création d’un langage commun sont des atouts essentiels. C’est bien l’ensemble des principes et des symboles que vous réussirez à réunir qui offrira un cadre de travail harmonieux pour votre équipe.
        
        Vous devez continuer à vous assurer que l’équipe fonctionne toujours collaborativement et qu'elle résout rapidement les conflits.
        
            - Aidez l’équipe à se discipliner.
            - Aidez l’équipe à être plus rigoureuse.
            - Aidez les membres de l'équipe à résoudre les conflits.
            
        Avec ou sans “open space”, vous donnez à chaque membre les moyens de participer à la création d'un cadre commun de travail. Vous allez en effet définir un véritable environnement agile (“war room” ou “digital workplace”) dans lequel votre équipe pourra travailler sans perturbations.
        
        
        Le “ROTI” (pour Return On Time Invested) est un bon moyen de mesurer la pertinence d’une réunion. C’est une pratique agile qui va vous permettre d’optimiser leur qualité et leur fréquence. Vous pouvez consacrer systématiquement 60 secondes en fin de réunion pour réaliser cette rapide évaluation.
            => tous les participants doivent donner une note sur la façon dont la réunion s'est passée afin de qualifier le retour sur le temps investi. 
            
                1 doigt → inutile, je n’ai rien gagné ou rien appris (perte de temps).
                2 doigts → utile, mais ça ne valait pas tout le temps passé (pertes de temps).
                3 doigts → moyenne, je n’ai pas perdu mon temps (sans plus).
                4 doigts → bonne, j’ai gagné plus que le temps que celui passé en réunion (positif).
                5 doigts → excellente, ça valait bien plus que le temps qu’on y a passé (bénéfices).
                
            La règle veut qu’on interroge seulement les participants qui ont noté 1 et 2 doigts, mais je vous conseille plutôt de poser une seule et même question aux membres de votre équipe : « Qu'est-ce qui pourrait être fait pour augmenter ma note d'un point  ? ». 🤔
            
        Après l’évaluation du ROTI, vous devez simplement prendre acte de la note moyenne :
            - Il n'y a pas de longue discussion interminable.
            - Il n’y a surtout pas de débat avec des arguments et des exemples.
            - Il n’y a pas non plus de tentative de résolution d'un problème.
            - Il n’y a pas de préjudice moral entre les membres de l’équipe (idéalement).
            
        Cette pratique agile est une parfaite initiation à la transparence et au management visuel. 
        
        RESUME:
            - Vous motivez l'engagement des membres de l'équipe agile : en encourageant leur discipline, leur rigueur et leur autonomie.
            - Utilisez la pratique agile du ROTI (Return On Time Invested) afin de mesurer la pertinence de vos réunions avec un rapide vote à main levée.
            - En qualité de coach agile, vous faites preuve de transparence et d'humilité pour garantir à votre équipe une gestion de projet efficace.
            
            
            
TRAVAILLEZ AVEC UN BUT COMMUN


        La quatrième étape de constitution d’équipe (Performing/Exécution) est une phase dans laquelle vous serez moins tenté de renforcer vos liens avec l'équipe, d’assurer une présence routinière ou de garder une forte dépendance avec chaque membre… Au contraire,  vous prenez du recul et analysez en continu le travail de votre équipe afin de partager vos observations.
        
        Si vous avez bien appliqué les pratiques agiles des trois chapitres précédents, cette dernière étape doit correspondre à celle d'un travail effectif de votre équipe. Les bases de fonctionnement qui ont été mises en place sont pérennes et votre équipe coopère pour atteindre les objectifs du projet. 🎯
        
        Votre équipe peut maintenant résoudre ses problèmes toute seule, mais votre rôle reste crucial.
        
            - Soyez la passerelle entre l’équipe et les parties prenantes du projet.
            - Soyez le facilitateur de la prise de décision (à niveau supérieur d’organisation, le cas échéant).
            
        Lors de cette étape, vous ne devez plus vous impliquer directement dans les prises de décisions de l’équipe. Les membres sont désormais autonomes et auto-organisés : ils travaillent ensemble et prennent leurs responsabilités sans vous.
            =>  Votre équipe agile est prête !
            
            
        Dès lors que votre équipe produit systématiquement avec un haut niveau de performance, vous pouvez déployer des outils dédiés à la gestion de projet agile:
            - trello        https://trello.com/
            
        Rester en veille sur l’apparition de nouveaux logiciels qui favorisent le pilotage par les livrables.Le pilotage par les livrables se concentre sur vos résultats et sur vos anticipations afin de sécuriser le projet.
        
        -> Chaque membre est concentré sur l’atteinte des objectifs en tant qu’équipe:
            - L’individu n’existe plus ou disparaît.
            - Les membres de votre équipe ne dépendent pas d'un chef de projet.
            - Les membres peuvent résoudre des problèmes sans manager.
            - La supervision quotidienne de votre équipe n’est plus indispensable.
            
        RESUME:
            - Vous partagez des objectifs communs avec tous les membres de l'équipe agile et jouez le rôle de passerelle ou d’intermédiaire avec chaque partie prenante du projet.
            - Utilisez un logiciel collaboratif dédié à votre gestion de projet agile afin de répondre le plus vite possible aux attentes de votre équipe ou de votre client.
            - En qualité de coach agile, votre management est visuel dans un environnement de travail convivial, sans avoir à désigner un chef de projet.
            
            

##### PRENEZ EN COMPTE LES BESOINS UTILISATEURS #####


COLLECTEZ LES BESOINS


        Posez des questions:
            Lors du premier rendez-vous, votre objectif sera surtout de faire parler le client ! Vous devez recueillir le plus d'informations possible : délai, budget, disponibilité, finalité, vision, évolution, inspiration, concurrence, etc. Je vous conseille de poser des questions ouvertes, c'est-à-dire des questions auxquelles la personne ne peut pas répondre avec de simples “oui” ou “non”. 
            Écoutez bien le client, prenez des notes et n'hésitez pas à schématiser le projet dès le premier rendez-vous. Munissez-vous de grandes feuilles et de différents feutres car votre ordinateur est loin d'être le seul outil pertinent pour enregistrer des informations efficacement.
            
        N'acceptez pas n'importe quel projet:
            C'est toujours un plaisir pour moi de faire la connaissance de nouvelles personnes et de découvrir leurs projets. Cependant, je m'efforce à apprendre comment refuser les missions trop contraignantes. 
            Vous allez rencontrer des personnes qui ne souhaitent pas coopérer et d'autres qui ont déjà une vision trop figée du produit ou du service à concevoir.  Vous pourrez vite déterminer si votre client et votre gestion de projet agile sont compatibles en présentant dès le départ vos méthodes de travail.
            
        N'ayez pas peur de vous sentir “débutant”, au contraire !
            Lorsque je ne connais pas bien le sujet d'un projet, je demande au client de faire une synthèse écrite de ses besoins. C'est ensuite à partir de l'analyse de son vocabulaire (ou champ lexical) que je propose une direction plus précise. Vous pouvez réaliser un nuage de mot, manuellement ou automatiquement (service gratuit : http://www.nuagesdemots.fr) pour mettre en évidence les enjeux du projet.
            
            Comment répondre à toutes les attentes du client ?  Comment éviter le hors-sujet ? Comment définir une gestion de projet cohérente ?
            
                -> Les livrables de l'expérience utilisateur vont vous permettre de diviser le projet de votre client en différentes propositions. Cartes mentales, entretiens utilisateurs, maquettes ou prototypes sont autant de solutions qui garantiront à votre client une gestion de projet agile.
                
                
        Les livrables du projet:
            C'est la première valeur du Manifeste agile : vous devez privilégier les individus et leurs interactions aux processus et aux outils. Pour y parvenir, voici trois livrables que vous pourrez présenter au client après votre premier rendez-vous.
                
                - Les histoires                 => L'expérience d'un utilisateur est une bonne histoire qui peut aider votre client à détecter un problème
                                                ou une opportunité. C'est aussi un récit capable de marquer les esprits et de motiver votre équipe.
                                                
                - Les proverbes                 => Les comparaisons, les analogies et les métaphores invoquent des références culturelles. 
                                                Elles sont très utiles pour faciliter la compréhension d'un projet avec votre client et votre équipe.        
                                                
                - Les rapports                  => L'écriture est un excellent outil de réflexion et d'organisation. 
                                                Un rapport synthétique peut ainsi présenter vos résultats détaillés, vos analyses précises et vos recommandations formelles                                
            
        RESUME:
            - Organisez votre premier rendez-vous dans un lieu neutre (si possible), posez des questions ouvertes et schématisez le projet du client afin de collecter ses besoins. 
            - En qualité de coach agile, vous présentez l'ensemble des méthodes de travail collaboratives dans votre gestion de projet afin de gagner l'adhésion du client.
            - Vous faites participer le client à la conception du projet avec la rédaction d'un texte synthétique, la réalisation d'une carte mentale, l'analyse de son vocabulaire, etc.
            - Vous livrez rapidement un rapport au client après votre premier rendez-vous. 
            
            
            
CARTOGRAPHIEZ LE PROJETS


        Idéalement, je réserve tous les jours une à deux heures pour faire de la veille sur différents sujets qui me passionnent.
        
        Imprégnez-vous du sujet:
            Pour faire ma veille, j'utilise principalement Facebook, Twitter et Pinterest, tv, journeaux, .... Mon conseil est simple : soyez éclectique, mais pointu !
            comment faire de la veille de qualité?  -> Vous devez être capable de définir une problématique à partir des besoins de votre client.
            Essayer tous les termes corresponant aux grandes lignes du projets dans tous les moteurs de recherche, ainsi qu'avec les outils Google+, Google Trends ou Google Alertes.
            
        
        Allez sur le terrain:
            En tant que chef de projet, je ne connais pas de meilleur expérience qu'une immersion pour comprendre les usages d'un produit ou d'un service. Vous devez prendre le temps d'observer les utilisateurs et leurs activités dans des conditions réelles afin de mener à bien votre projet. 
            
            
        Tâchez d'avoir une vision d'ensemble:
            Pour le second rendez-vous, je réalise toujours une présentation complète pour confirmer la direction du projet avec le client. C'est à partir de cette synthèse du projet que je construis ma proposition et que je négocie les différentes enveloppes budgétaires.
            La présentation synthétique, la vision d'ensemble, la feuille de route... Plus le projet est imprévisible, plus ce document est important. En gestion de projet agile, je vous conseille d'y accorder la plus grande attention, même si ce livrable apparaît souvent avant la validation définitive des devis. 
            
            
        Les livrables du projet:
            -> C'est la troisième valeur du Manifeste agile : vous devez privilégier la collaboration avec votre client !
                - Les présentations                 => Les diaporamas (et les vidéos) peuvent être géniaux pour raconter une histoire... 
                                                        Ils peuvent également être très ennuyeux ! Vous ferez donc preuve d'originalité et de rigueur.
                                                                
                - Les plannings                     =>  Les plans du projet, les feuilles de route ou les agendas guident la conception et
                                                        l'activité de votre équipe en clarifiant les rôles et les responsabilités de chacun.  
                                                        
                - Les cartes                        => Les territoires peuplés de concepts doivent être cartographiés pour vous inciter 
                                                        à établir des repères, à clarifier des relations, à identifier des priorités et enfin à décider quoi faire.                                        
                
                
        RESUME:
            - Facilitez toutes vos gestions de projet à venir en consacrant chaque jour ou chaque semaine un moment dédié à votre veille (stratégique, technique, culturelle, etc.).
            - Vous devez vous imprégner du sujet en allant sur le terrain (si possible) et en observant les utilisateurs potentiels du produit ou du service à développer.
            - En qualité de coach agile, vous présentez une vision d'ensemble du projet avant la validation des différentes enveloppes budgétaires.
            - Vous cartographiez et vous planifiez le projet avec la participation du client.
            
            
            

MENEZ DES ENTRETIENS


        Ne faites pas qu'un seul entretien:
            Je réalise désormais entre 5 et 10 entretiens semi-directifs pour chacun de mes projets. Ce sont des études qualitatives de 30 à 45 minutes en présence ou à distance.
            Votre objectif principal est d'avoir accès à des points de vue personnels. Vous devez recueillir les impressions, comprendre les comportements et décrypter les attitudes.
            Vous menez les entretiens avec un guide d’animation et des questions préétablies auxquelles l'utilisateur doit répondre le plus instinctivement possible.
            L'utilisateur va très certainement compléter et approfondir les besoins du client. 
            Conseil: filmer ces entretiens pour les revoir plus tard.
            
            
        Expliquez les raisons de l'entretien:
            Vous pouvez établir le guide d'entretien en fonction des objectifs de votre enquête, des besoins de votre client ou de vos propres hypothèses. Je commence toujours par donner une présentation très synthétique du projet et par préciser que l'enregistrement vidéo restera confidentiel.
            Je vous encourage à obtenir des informations personnelles dès le début de l'entretien pour mettre la personne en confiance. Vous pouvez lui demander : son âge, son lieu de résidence, sa connaissance du secteur d'activité, sa profession, ses passions, ses qualités, ses artistes préférés, etc. 
            Le guide d'entretien a aussi pour but de mettre en évidence les besoins que vous n'avez pas encore collectés ou détectés. Il vous permet ensuite de comparer les résultats entre différents utilisateurs. 
            
            
        Posez des questions ouvertes:
            Vous allez poser à l'utilisateur des questions très ouvertes. Vous devez être capable de le mettre à l’aise, puis de le faire parler, tout en analysant son comportement. 🙄 Gardez aussi à l'esprit que l'utilisateur sait qu'il est observé.
            L’observation fait partie intégrante de votre entretien. Les émotions de l'utilisateur, le ton qu’il emploie, sa posture corporelle, ses silences et ses hésitations sont autant d'informations à recueillir.
            
        
        Posez des questions précises:
            Je sélectionne toujours entre 3 et 5 ressources à soumettre aux utilisateurs. Il n'y a rien de plus instructif que d'observer la navigation d'un internaute sur des sites de référence pour votre projet.
            
            
        Laissez le temps de répondre:
            Lorsque vous abordez un sujet délicat, vous pouvez préciser que cet entretien ne recherche pas à recueillir des positions personnelles en termes de pour ou de contre.
            Quand l'utilisateur est gêné, n'hésitez pas (si possible) à faire preuve d'un peu d'humour. 
            
            
        Demandez des retours d'expérience:
            Vous pouvez utiliser les entretiens de façon stratégique dans votre gestion de projet agile. En effet, votre client aura peut-être tendance à croire que sa connaissance du secteur d'activité lui donne toujours raison. Les retours d'expérience des personnes interrogées pourront appuyer ou illustrer vos idées sans les rendre trop personnelles. 
            
        
        La notion de storytelling est de plus en plus importante en gestion de projet. C'est pourquoi je vous incite aussi à faire le montage de vos entretiens sous la forme de récits à raconter.
        
        
        Les livrables du projet:
            - Les sondages                          => Les mêmes questions posées à de nombreux utilisateurs peuvent révéler des besoins cachés.
            - Les systèmes                          => La représentation visuelle des objets et des relations au sein d'un système peut améliorer
                                                        la compréhension des utilisateurs pour toutes les parties prenantes de votre projet.
            - Les flux                              => Les interactions des utilisateurs avec le produit ou le service forment des flux de procédure.
                                                        Une représentation symbolique peut mettre en évidence les différentes lignes de désir et montrer les avantages des chemins choisis par votre équipe.
                                                        
                                                        
        RESUME:
            - Menez plusieurs entretiens semi-directifs avec des utilisateurs potentiels du produit ou du service à développer.
            - Comparez les résultats de votre étude qualitative en conservant toujours le même guide d'entretien et en filmant les utilisateurs (ou leurs usages).
            - Vous prêtez une attention particulière aux comportements des utilisateurs afin de recueillir des informations pertinentes pour votre gestion de projet.
            - Vous proposez à l'utilisateur de consulter différentes ressources, sans rien imposer.
            - Vous posez des questions courtes et précises pour favoriser l'écoute et l'observation.
            - En qualité de coach agile, vous collectez les retours d'expérience de chaque utilisateur afin de faciliter votre storytelling avec l'équipe et le client.
            - Complétez vos entretiens avec des sondages (mais pas l'inverse).
            
            
            
            
CREEZ DES PERSONAS


        Les personas vous permettent de répondre à deux questions essentielles en gestion de projet agile : 
            Qui sont les utilisateurs ? 
            Comment les utilisateurs utilisent-ils mon produit (ou mon service) ?
            
        
        
        Identifiez les utilisateurs “extrêmes”:
            Vous concevez des produits ou des services pour des utilisateurs bien identifiés. Votre équipe doit toujours avoir la possibilité de se mettre dans la tête de l'utilisateur afin de mieux le servir. 
            Un utilisateur de 14 ans n'a pas le même comportement qu'un de 45 ans.
            
            
            
        Identifiez les utilisateurs “types”:
            Les personas vous permettent de répondre à plusieurs attentes de la gestion de projets agile. Vous donnez un visage humain à l'utilisateur final. Ces archétypes d'utilisateurs forment un langage commun entre votre équipe et votre client.
            
            
        Listez les caractéristiques déterminantes de chaque persona:
            Vous devez savoir comment l’utilisateur réalise une activité et consulte des ressources.
            créer un persona:
                - type d’utilisateur (âge, genre, profession, catégorie socioprofessionnelle, etc.)
                - besoins ou objectifs vis-à-vis du produit ou service
                - critères de choix et d'habitude
                - façon de se servir des fonctionnalités
                - expertise du domaine
                
            
        Détectez les profils utilisateurs double-face:
            En synthétisant les données, vous allez faire apparaître différents groupes d’utilisateurs présentant des comportements similaires. Plus les ressemblances sont flagrantes, plus le persona est solide ! 🎭 Si un ou plusieurs utilisateurs se situent entre deux groupes, n'hésitez pas à leur créer un profil. Vous pouvez identifier des profils types, extrêmes, mais aussi double-face pour vos personas.
            => Quelle est la meilleure façon de reconnaître les profils utilisateurs ? Quel est le nombre de personas idéal en gestion de projet agile ? 
            Je vous conseille de définir en équipe une hypothèse pour chaque persona: le joueur, l'expert, l'acheteur, le prescripteur, ...(min: 3 max 20)
            Vérifiez ensuite que vos hypothèses de départ sont bien validées selon les données récoltées lors de vos entretiens individuels.
            
            
        Donnez-leur un visage et des émotions:
            Vos personas auront besoin de prénoms pour attirer l’empathie de votre équipe. Vous pouvez également les humaniser en racontant leur histoire personnelle à partir de faits réels. Évitez les informations trop fictives afin de garantir des personas les plus réalistes possible !
            Chacun de vos personas doit avoir des objectifs, des attitudes, des émotions, etc. Ce seront des informations déterminantes pour concevoir l'usage de votre produit ou service final.
            
            
        Invoquez-les à chaque étape du processus:
            Présentés sous forme de fiche, les personas sont régulièrement consultés par les membres de votre équipe. Ils motivent votre équipe à tester et à évaluer leurs travaux. Je vous encourage même à les afficher dans votre environnement de travail pendant toute la durée de votre gestion de projet agile.
                =>  1. Aidez votre équipe à partager une même compréhension des utilisateurs cibles.
                    2. Hiérarchisez les solutions en fonction des besoins d'un ou plusieurs personas.
                    3. Donnez un visage aux personnes pour lesquelles doit être conçu le projet.
                    
        
        Les livrables du projet:
            - Les personas                          => La synthèse des objectifs et des comportements de vos utilisateurs. 
                                                        Les personas rappellent tous les jours à votre équipe et à votre client que : « Vous n'êtes pas les (seuls) utilisateurs ».
                                                        
            - Les scénari                           => Les utilisateurs sont positionnés dans des situations réelles. 
                                                        A vous d'imaginer comment le produit ou le service répond à leurs attentes.                          
                                                        
            - Les listes                            => Les exigences décrivant le comportement d'un persona vous aideront à passer de la conception 
                                                        au développement du projet.                                            
        
        
        
        RESUME:
            - Appliquez la méthode des personas afin de définir des profils d'utilisateurs bien distincts pour le produit ou le service à développer.
            - En qualité de coach agile, vous valorisez la consultation des personas afin de créer un langage commun avec l'équipe et le client.
            - Vous pouvez associer la méthode des personas à celle des entretiens utilisateurs, sans calquer cette étude sociologique sur votre panel. 
            - Vous détectez les profils d'utilisateurs double-face pour définir un persona à part.
            - Donnez un visage et des émotions crédibles à chacun de vos personas.
            - Affichez la représentation des personas dans votre environnement de travail.
            - Imaginez des scénari et des jeux de rôles avec les membres de votre équipe.
            
            
            
REALISEZ UN STORYBOARD


        Mettez en scène l'expérience utilisateur:
            Le storyboard vous permet de synthétiser l'expérience utilisateur avant le développement du projet. C'est aussi un document de référence qui améliore la circulation des informations entre les membres de votre équipe. Vous pouvez en réaliser plusieurs pour un même projet.
            => Représentez vos personas dans différentes situations ou scénarios.
            
            Dans quelles circonstances l'utilisateur peut-il avoir besoin du produit ou du service de votre client ? Est-il à son domicile ? Est-il en déplacement ? La mise en scène de vos personas rendra plus concrètes les solutions que vous proposerez avec votre équipe.
            
            
        Faites une bande dessinée:
            La bande dessinée est un medium très efficace pour partager une idée. Vous réalisez une succession de 5 à 9 images pour décrire les interactions de vos utilisateurs avec le produit ou le service. Commencez par une situation de départ, mettez en scène votre solution et finissez par la résolution du besoin.
            
            
        Oubliez les éléments d'interface utilisateur:
            Dessinez votre storyboard avec des éléments symboliques, les plus universels possibles. Ne rentrez pas dans les détails du produit ou du service. Vous allez surtout représenter des formes très simples, comme un ovale pour une tête par exemple.
            
            
        Dessinez à la main et avec un feutre:
            Pour vous entraîner, rien de mieux qu'un grand tableau blanc avec des feutres effaçables de différentes couleurs. Dessinez des séries de cases bien identiques et lancez-vous ! Une fois que vous êtes satisfait du résultat, présentez-le à votre équipe, puis photographiez-le pour obtenir une version numérique.
            ou  https://www.autodraw.com/
            
        
        Partagez le storyboard avec vos collaborateurs:
            Affichez vos storyboards à coté des personas pour les garder toujours visibles dans votre espace de travail.
            Encouragez chaque membre de votre équipe à les consulter régulièrement pour respecter les objectifs, tester une solution ou mettre fin à des conflits.
            
            
        En gestion de projet agile, votre devez impérativement construire une vision commune avec votre équipe et votre client. 
        
        
        Les livrables du projet:
            - Le storyboard                         => Le récit découpé en vignettes doit mettre en contexte les différentes interactions 
                                                        entre vos utilisateurs et le produit ou le service de votre client.
                                                        
            - Le Wireframe                          => La maquette ou croquis amène votre équipe à se concentrer sur la structure, 
                                                        l'organisation et les interactions. 
                                                        C'est un livrable très pertinent avant d'investir du temps et de l'attention au choix des couleur, des polices ou des images.                                            
                                                        
            - Les prototypes                        => Les modèles de travail (papiers ou informatiques) ont pour objectif d'analyser 
                                                        l'engagement de votre client et de vos utilisateurs en montrant à quoi le produit ou le service ressemblera.                                            
                                                        
                                                        
        RESUME:
            - Mettez vos personas en scène dans des storyboards qui serviront de références aux membres de votre équipe agile.
            - Décrivez les interactions entre les utilisateurs et le produit ou le service : en agençant des cases, en dessinant des situations concrètes et en ajoutant des dialogues.
            - En qualité de coach agile, vous êtes capable de faire preuve de bon sens pour représenter symboliquement des personnages, des mouvements, des objets, etc.
            - Vous pouvez utiliser une banque d'images en ligne, mais je vous conseille surtout un grand tableau blanc et différents feutres effaçables.
            - Vous devez partager et afficher vos storyboards dans votre environnement de travail.
            - Vous respectez chaque storyboard pour concevoir vos prototypes.
            
            
            


##### REPRESENTEZ VISUELLEMENT VOTRE GESTION DE PROJET #####


EVALUEZ LES USERS STORIES


        En gestion de projet agile, ces user stories sont des phrases simples rédigées dans le langage de tous les jours. Elles vont permettre à votre équipe de décrire précisément toutes les fonctionnalités du projet. 
        
        
        le recueil de vos entretiens individuels mélange:
            - Des usages personnalisés
            - Des situations professionnelles
            - Des procédures plus ou moins précises
            - Des organisations idéales
            - Des solutions pratiques
            
            
        Une user story est d'abord la description simplifiée d’un besoin. En gestion de projet agile, vous pourrez toujours l'affiner selon la maturité et la priorisation de la demande.
        
        Votre équipe rédige une user story pour rendre le besoin utilisateur simple et compréhensible. Cette phrase doit seulement contenir 3 éléments descriptifs : Qui ❔ Quoi ❔ Pourquoi ❔
        
            1. Le contexte → en tant que <rôle/personas>
            2. La fonction → je veux <fonctionnalité>
            3. Le bénéfice → afin de (pour) <raison/objectif>
            
        
        Les users stories pour le coach agile:
            Toutes les user stories pertinentes sont réunies dans le carnet du projet ou product backlog qui reste sous la responsabilité de votre client (ou de son représentant au sein de l'équipe). En qualité de coach agile, vous ne rédigez pas les user stories, mais vous devez être capable d'évaluer leur qualité. 
            
            La grille des critères INVEST va vous permettre de motiver les membres  de votre équipe à modifier ou à mieux reformuler l'énoncé d'une user story. Ce concept agile vous assure que les user stories ont bien les qualités requises pour figurer dans le product backlog. Une bonne user story est :
                - Independent → pas de dépendance entre les user stories
                - Negotiable → la user story peut être arbitrée par le client et l'équipe
                - Valuable → un besoin est toujours associé à la user story
                - Estimable → l’équipe doit être en capacité d'estimer la user story
                - Small → la user story est décrite précisément
                - Testable → les critères d’acceptation de la user story sont atteignables
                
            Si une user story est trop "longue" il faut la découper en plusieurs petites.
            Un bon découpage aura un impact réel sur le déroulé du projet. C'est votre ultime marge de manœuvre pour affiner les fonctionnalités du projet et anticiper les incertitudes de l'équipe.
            
            La motivation de votre équipe augmente lorsque l’aboutissement de son travail est concret. C'est pourquoi les user stories du projet se divisent toujours en courtes actions. La tâche représente bien le niveau de découpage élémentaire pour une user story
            Votre client sera satisfait de comprendre l'avancée du travail en consultant le product backlog.
            
            Les user stories que vous avez prévu de gérer à une date lointaine sont décrites synthétiquement. Encouragez votre équipe à détailler les tâches dont la réalisation est imminente. Vous pouvez alors évaluer la pertinence des tâches avec la grille de critères SMART :
                - Specific → toute votre équipe comprend ce qu’il y a à faire
                - Mesurable → votre équipe sait comment s’assurer que la tâche est réalisée
                - Achievable → votre équipe dispose de tous les moyens pour réaliser la tâche
                - Relevant → la tâche participe bien à la concrétisation de la user story
                - Time Bound → la tâche a une durée connue et limitée de travail
                
                
ANALYSEZ LE PRODUCT BACKLOG


        Un product backlog est le recueil des besoins de l'utilisateur. Ces user stories contiennent les tâches : le product backlog est donc aussi la liste ordonnée de tout ce qui est requis pour développer le produit ou le service de votre client.
        
        Avec le product backlog, vous transformez les intentions du projet en commandes explicites:
            - Les besoins
            - Les améliorations
            - Les correctifs à apporter
            
        Partagez le product backlog avec votre client et toutes les parties prenantes du projet. Les user stories et les tâches y sont décrites, classées et évaluées afin de faciliter votre gestion quotidienne.
        
        En gestion de projet agile, vous avez pour objectif de produire de la valeur en priorité.
        Analysez chaque tâche du product backlog en fonction de cette valeur afin de faciliter les arbitrages et la priorisation de l'équipe. 😎 Votre calcul de la valeur sera composé de critères pondérés afin de déterminer objectivement les gains attendus : qualité, notoriété, renforcement du savoir-faire, adéquation au marché, etc.
         
        Dans un premier temps, vous pouvez utiliser la méthode MoSCoW pour prioriser les besoins ou les exigences du product backlog. Votre objectif est que l'équipe s'accorde sur l'importance des fonctionnalités à réaliser en respectant les délais prévus. Estimez la valeur d'une user story ou d'une tâche avec l'un des quatre niveaux de cette échelle :
                - “Must have”, les fonctionnalités indispensables
                - “Should have”, les fonctionnalités importantes
                - “Could have”, les fonctionnalités de confort
                - “Want to have but Won’t have”, les fonctionnalités souhaitables, mais reportées    
            Votre équipe affecte ces priorités au product backlog afin de garantir les intérêts de votre client. Elle réalise les tâches M, puis les tâches S et C. Lorsque les délais ne peuvent pas être respectés, les tâches C seront les premières à être annulées, suivies par les tâches S. 
            
        Dans un second temps, une autre technique de priorisation peut vous aider à calculer la valeur des user stories et des tâches du product backlog. Appliquez un questionnaire de satisfaction en posant les deux mêmes questions à tous les membres de votre équipe :
                - Que pensez-vous du produit s’il contient cette fonctionnalité 
                - Que pensez-vous du produit s’il ne contient pas cette fonctionnalité
                
            => 5 réponses possibles:
                - Cela me ferait plaisir
                - Ce serait un minimum
                - Je n’ai pas d’avis
                - Je l’accepterais
                - Cela me dérangerait
                
            => matrice composée de six critères de priorisation:
                - Obligatoire
                - Exprimée
                - Latente
                - Indifférente
                - Incertaine
                - Annulée
                
                
        RESUME:
            - Vous priorisez la valeur du produit ou du service dans un product backlog évolutif afin de faciliter votre gestion de projet agile.
            - En qualité de coach agile, vous utilisez la méthode MoSCow, les grilles d'évaluation ou les questionnaires matriciels de satisfaction pour challenger l'équipe et le client. 
            
            
ESTIMEZ LA COMPLEXITE D'UN PROJET

        Les membres de votre équipe auront souvent des niveaux d'expérience et d'expertise différents. Le planning poker est une pratique à la fois agile et ludique qui va vous aider à estimer tous ensemble la complexité des fonctionnalités à développer. Faites-les s'exprimer le plus librement possible afin de favoriser les échanges. Plus votre équipe sera unanime, plus les estimations seront précises !
        
        Le planning poker est une estimation collective des efforts nécessaires pour produire tout ou partie d’une user story. Votre objectif étant au final d’obtenir une estimation consensuelle après avoir discuté de la mise en oeuvre la plus simple.
            => La suite de Fibonacci (0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144) permet d'estimer ce planing poker.
            
            
        En gestion de projet agile, votre planification est basée sur le calcul de la vélocité. Vous constatez à échéance régulière (jours ou semaines) combien de fonctionnalités ont été développées. Vous additionnez ensuite les estimations (story point) qui correspondent aux user stories terminées. C'est seulement à partir de ce calcul que vous anticipez la période suivante et la date de fin du projet.
        
         Le planning poker vous permet au contraire d'exploiter la connaissance de tous les membres de votre équipe agile.
         
         Voici la démarche que vous devez suivre pour réussir votre planning poker :
                - Votre équipe propose une solution simple.
                - Dans un premier tour d’évaluation, chaque membre de votre équipe propose son estimation.
                - S’il n’y a pas de consensus, les membres aux estimations les plus extrêmes expliquent leur choix.
                - Vous organisez un ou plusieurs autres tours d’estimation pour obtenir l'unanimité.
                - L’estimation est ajoutée à la user story.
                
        Voici les avantages que vous apporte cette réunion :
                - Une estimation partagée et transparente pour votre client
                - Une responsabilisation sur l’engagement de votre équipe
                - La suppression des jeux d’influence
        
        Le planning poker est aujourd'hui une compétence obligatoire pour le coach agile !
        
        Vous ajoutez des story points aux user stories en jouant au planning poker. C'est la toute dernière étape de préparation avant de lancer le développement du produit ou du service. 
        
        
        RESUME:
            - Estimez collectivement la complexité du travail à réaliser en organisant une partie de planning poker avec votre équipe et votre client.
            - En qualité de coach agile, vous réunissez les participants autour d'une table et arbitrez les débats du planning poker.
            
            
            
METHODE KANBAN


        C'est un système visuel de gestion de projet agile.
        
        Présentez chaque user story sous forme d’affichette et positionnez-les sur un tableau en fonction de leur statut d’avancement. En gestion de projet agile, vous pouvez déplacer les affichettes selon 3 statuts :
            - Aucune tâche de la user story n’a encore été exécutée (To do/À faire).
            - La user story est en cours de réalisation (In progress/Doing/En cours).
            - Toutes les tâches de la user story sont réalisées (Done/Terminé).
            
            - tester les réalisations de votre équipe (To Verify/À tester)
            
        
        Chaque membre de votre équipe est libre de choisir la user story qu’il souhaite réaliser. Le travail n’est pas imposé par un tiers. Voici le mode de fonctionnement :
            - Il prend seul la responsabilité de la user story de bout en bout. 
            - Il commence par déplacer la user story de “À faire” vers “En cours”.
            - Il déplace ensuite l’affichette de “En cours” vers “Terminé”, lorsque la user story est finalisée.     
            
            
        Chaque affichette (post-it bristol) du tableau kanban présente toutes les informations suivantes :
            - La user story de référence
            - La valeur de la user story
            - La complexité de la user story
            - La description des tâches à réaliser
            - Les critères d’acceptation (au dos de l’affichette)    
            
            
        En gestion de projet agile, le Kanban valorise votre management visuel. Écrivez sur les murs et partagez des objectifs avec tous les membres de votre équipe agile. 👀 Vous mettez tous les membres au même niveau d’information (avancements, difficultés, etc.). Vous respectez également tous les fondamentaux du projet (architecture, contraintes diverses, etc.).
        
        
        La méthode Kanban est incrémentale. Autrement dit, elle garantit les changements continus, augmentés et évolutifs du produit ou du service à développer. C'est aussi une méthode itérative lorsque les cycles de développement sont bien identiques. Vous devez alors faire le choix de remettre à zéro le tableau kanban chaque jour, chaque semaine ou chaque mois.
        
        
        Voici les 5 avantages principaux de la méthode Kanban pour le coach agile :
            - Visualiser le flux des travaux (workflow)
            - Limiter le nombre de tâches en cours
            - Gérer le déroulement du travail
            - Établir des règles d'organisation
            - Proposer des actions d’amélioration
            
            
        => Tout ceci revient à faire un burndown chart pour une gestion lean du projet    
        
        
        RESUME:
            - Utilisez la méthode Kanban pour afficher les user stories dans votre environnement de travail et suivre en continu la progression de votre équipe.
            - En qualité de coach agile, vous exercez un management visuel et transparent grâce au tableau kanban.
            
            
EVITEZ LES GASPILLAGES


        Les pratiques lean (“dégraissé”, en anglais) sont issues de l’industrie automobile (Toyota au Japon). C’est un ensemble de démarches qui va améliorer l’efficacité opérationnelle de votre équipe, votre gestion de projet et de votre entreprise ou organisation agile.
        
        Cette théorie repose sur 4 principes fondamentaux :
            - Challengez quotidiennement la performance des membres de votre équipe.
            - Rendez tous les problèmes visibles pendant la gestion du projet.
            - Mettez en oeuvre des solutions pratiques, peu coûteuses et immédiatement applicables.
            - Tirez les enseignements de votre coaching et vérifiez si les solutions sont transposables.
            
        Le Lean se concentre sur la recherche de la performance : productivité, qualité, délais, et coûts. 
        Cette théorie insiste sur l'amélioration continue et l'élimination des gaspillages. En gestion de projet agile, le Lean va vous aider à augmenter la valeur globale du produit ou du service de votre client.
            - Production en flux tendu → maximisez la production de valeur au fil des demandes du client en limitant les attentes et les traitements superflus.
            - Élimination des gaspillages, des surcharges, des variations → réduisez les causes de perturbation et d’inefficacité dans votre gestion de projet agile.
            - Construire la qualité → intégrez des contrôles de la qualité à chaque étape du développement.
            - Management visuel → mettez en place des consignes simples et visuelles permettant d’identifier rapidement les problèmes de votre équipe agile.
            - Standardisation créative → harmonisez les activités qui sont régulièrement planifiées pour favoriser l’amélioration continue du produit ou du service à développer.
            - Formation, coaching et mentorat → formez les membres de votre équipe agile, accompagnez-les et motivez-les tout au long de leur évolution professionnelle.
            
        Lean sert donc à éviter 7 formes:
            - La surproduction (excès) → fonctions non attendues, non utilisées ou en doublons.
            - Les attentes (retards) → projet ralenti par des prises de décisions et des validations.
            - Les retouches (défauts) → effort non prévu dont le coût est fonction du délai de détection.
            - L'inadaptation (procédures inutiles) → actions qui ne sont pas sources de valeur pour le client.
            - Les ruptures (transports inutiles) → passage difficile et coûteux d’une tâche à une autre.
            - La dispersion (mouvement inutile) → transmission d’information superficielle au client.
            - L'inachevé (stocks inutiles) → travail partiellement terminé ou fonctions non testées.
            
            => En gestion de projet agile, je vous conseille d'ajouter l'oubli comme huitième forme de gaspillage afin de respecter les apports de l'intelligence collective
            
        L'outil lean des “5 S” est une démarche d’amélioration élémentaire qui va vous permettre d'appliquer le principe de standardisation créative : 
                - Seiri (trier) → débarrassez-vous de l’inutile.
                - Seiton (ranger) → priorisez régulièrement l’utile.
                - Seiso (nettoyer) → inspectez et optimisez le projet.
                - Seiketsu (Standardiser) → définissez des règles communes.
                - Shitsuke (respecter) → affichez et pérennisez tous les indicateurs. 
                
                
        Les “5 pourquoi” est un autre outil lean qui va vous donner les moyens d'analyser les racines d’un problème et ainsi d’être plus efficace dans sa résolution.
        C'est une démarche itérative de questions dont le choix et l'enchaînement dépendent toujours des analyses collectives de votre équipe agile. Évitez impérativement toutes perturbations extérieures.
        
            ex: 👎 Le monument présente des fissures.
                ❓ Les ouvriers le nettoient avec des produits chimiques puissants.
                ❓ Le monument est couvert de déjections d’oiseaux.
                ❓ Les oiseaux viennent pour manger des araignées.
                ❓ Les araignées chassent les nombreux insectes.
                ❓ Les insectes sont attirés par l’éclairage nocturne du monument. 
                👍 Vous devez peut-être réduire l’éclairage du monument !
                
        Le burndown chart est la vision graphique du “reste à faire” que je vous préconise en gestion de projet agile. Vous affichez au mur, en grand format, un graphe représentant :
                - La quantité de travail restant à effectuer (sur l'axe vertical)
                - La durée du projet (sur l'axe horizontal)        
                
            => Calculez l'indicateur à l’issue de chaque réunion quotidienne. Proposez des réductions de périmètre du livrable si les divergences et les retards sont trop importants
            
            
        RESUME:
            - Mettez en pratique les principes du management lean afin de réduire les perturbations dans votre gestion de projet agile.
            - En qualité de coach agile, vous utilisez le burndown chart pour calculer la vélocité de votre équipe et livrer votre client régulièrement.