########## GEREZ VOTRE PROJET INFORMATIQUE ##########


CAHIER DES CHARGES

        
        Un cahier des charges est un document qui explique les besoins et les attentes du client. Ce dernier le rédige et l’envoie à la personne qui réalisera le projet : prestataire externe, agence ou développeur en interne. Le plus souvent, le prestataire externe questionne le client sur le cahier des charges pour être sûr d’avoir bien compris les attentes de son client. Le cahier des charges peut être modifié en conséquence afin d’y indiquer le budget, les contraintes éventuelles et les deadlines.
        
        Quand les deux parties sont d’accord sur le contenu, elles signent un contrat qui se base sur le cahier des charges. 
        
        
        
        Pour les tests de validations du projet, nous pouvon sles faire à la main ou de manière automatique: les tests unitaires.
        Chaque test unitaire a pour but de vérifier qu’une partie de l’application produit bien le résultat attendu quand un certain événement a lieu. 
        
        Puis le maître d’ouvrage réalise des tests de recette, c’est-à-dire qu’il va tester manuellement que l’application correspond à ce qui a été demandé.
        
        
        
        
METHODOLOGIES AGILES


        Une méthode agile est une approche itérative et incrémentale pour le développement de logiciel, réalisé de manière très collaborative par des équipes responsabilisées appliquant un cérémonial minimal qui produisent, dans un délai contraint, un logiciel de grande qualité répondant aux besoins changeants des utilisateurs.
        
            Approche Itérative:
                L’itération est le principe de répéter un processus plusieurs fois dans le but d’améliorer un résultat.
                
            Approche Incrémentiale:
                Un processus incrémental permet de construire un produit petit à petit en le découpant en pièces détachées. Elles sont le plus souvent indépendantes les unes des autres mais ont pour caractéristiques d’améliorer le produit.
                
            Manière Adaptative:
                Suivre une approche incrémentale et itérative suppose une grande communication avec ses utilisateurs et une certaine humilité. Votre produit sera d’abord très minimaliste et s’améliorera en fonction des retours. Puis vous ajouterez de nouvelles fonctionnalités qui, à chaque étape, feront l’objet de modifications.

                Une méthode incrémentale et itérative mettra en œuvre les quatre phases (définition, conception, code et test) pour chaque incrément, donc pour chaque nouvelle fonctionnalité. Le responsable du produit communiquera alors avec les utilisateurs pour connaître les modifications éventuelles à apporter lors d’une itération ultérieure.
                
                Les intérêts sont multiples. Outre le fait que vous communiquez régulièrement avec vos utilisateurs, vous avez un réel sentiment de satisfaction. Votre produit s’améliore au jour le jour, concrètement, et vous avez l’impression de passer moins de temps à écrire de la paperasse.
                
                => le projet est complètement centré sur l’utilisateur ! 
                
                
                
                
SCRUM


        Scrum est la méthode agile la plus populaire.
        
        Au début du projet, le projet est découpé en fonctionnalités qui sont listées dans un backlog, une sorte de grand tableau.
        
        Puis nous allons nous intéresser à la première version qui sera mise en ligne. Elle sera volontairement minimale, se concentrant sur les fonctionnalités essentielles qui ont été indiquées comme prioritaires dans le backlog. Chaque mise en production est appelée release et est constituée de plusieurs sprints.
        
        Un sprint dure entre une et deux semaines et sert à développer une ou plusieurs fonctionnalités. Elles sont décidées par l’équipe et le Product Owner.
        Chaque sprint est découpé en Stories.
        Le développeur de chaque story produira les mêmes livrables que ceux que nous avons vus dans les méthodologies séquentielles mais à l’échelle d’une story et non du projet tout entier.
        Il aura donc à :
            - créer des spécifications fonctionnelles
            - concevoir l’architecture de la story
            - coder et tester
            
        Pendant un sprint, des points sont effectués lors des mêlées quotidiennes.
        À la fin de chaque sprint, l’équipe ajoute une nouvelle brique au projet qui devient ainsi, de sprint en sprint, de plus en plus complet. Son évaluation et le feedback récolté permettent d’ajuster le backlog pour le sprint suivant. Cela s’appelle l’inspection.
        
        
        
        Les responsabilités et les missions des intervenants d’un projet agile sont très différentes en comparaison avec un projet séquentiel car l’accent est mis sur l’appropriation du projet par chaque membre de l’équipe et sur la collaboration:
        
            - Product Owner                 ->  Il représente les utilisateurs finaux.
                                                Il est responsable de la définition du contenu du produit et de la gestion des priorités. 
                                                Il alimente régulièrement le backlog en nouvelles fonctionnalités et trie les anciennes par ordre de priorité.
                                                Il définit l’objectif d’une release et prend les décisions sur son contenu.
                                                
            - ScrumMaster                   ->  Il aide l’équipe à appliquer les principes de Scrum.
                                                Il fait également en sorte d’éliminer tout obstacle qui pourrait entraver le projet.
                                                
                                                
            - Equipe                        ->  Il s’agit du rôle principal ! C’est elle qui réalise le produit.
                                                Elle doit posséder toutes les compétences nécessaires à son bon développement.
                                                Elle est choisit par le scrum master
                                                Elle définit elle-même la façon dont elle organise ses travaux
        
        
    DEBUTER UN PROJET: Sprint 0
    
        On appelle “Sprint 0” la période précédant le premier sprint. Elle est dédiée à l’organisation du projet : prise de connaissance avec le produit et avec les attentes des utilisateurs, recrutement de l’équipe et création du backlog.
        
            - Vision                        ->  Le product owner doit avoir une excellente vision du produit final
                                                C'est souvent le "pourquoi?": pourquoi développer cette application?
                                                
            - Utilisateurs                  -> Le product owner doit connaitre parfaitement les utilisateurs qu'il représente 
                                                => le Product Owner crée une ou plusieurs personas et les partage au reste de l’équipe.
                                                
            - Backlog                       -> Le Product Owner remplit le backlog: 
                                                grand tableau listant les fonctionnalités prévues pour le produit et leur état d’avancement 
                                                
                un tableau de backlog contient les colonnes:
                    - Product backlog : les fonctionnalités qui ont été priorisées par le Product Owner et qui sont en attente de développement
                    - Development : User Stories en développement
                    - Done : User Stories qui ont été mises en production
                    
                    
    
    
SPRINTEZ


        Avant de démarrer un sprint, l’équipe se réunit pour analyser les fonctionnalités en attente et déterminer le scope du prochain sprint.
        
        Une fonctionnalité n'est pas un sprint. Une fonctionnalité peut être très longue à mettre en place.
        Or les méthodes agiles ont pour but de tout découper en petites taches facilent à plannifier et à tester: stories
        
        Une Story est une action réalisée par l’utilisateur dans un certain but. (ex: cliquer pour se connecter à  son compte)
        
            => La première tâche est donc de découper une fonctionnalité en Stories.
            
            
            
        Écrire une User Story:
            
            MODELE:     <En tant que>, je veux <faire une action> pour <résultat de l’action>
                        <As>... <I want> ....<so that>
                        
            Une User Story raconte une histoire et peut être réalisée en un sprint. Elle a 3 caractéristiques:
                - La carte : toute User Story est représentée par une carte
                - La conversation : le titre de la carte raconte une histoire
                - La confirmation : le contenu de la carte détaille le résultat attendu et donc le moment où nous pouvons indiquer que la story est terminée
                
            
        Définir un planing:
        
            Chaque Story inclut également une estimation de sa complexité en nombre de points. Elle est calculée en équipe pendant un “planning poker”.
            
            L’équipe pose des questions sur le contenu de la story puis chaque membre montre, en même temps, son estimation. L’équipe en discute et mise sur un chiffre => elle propose un planning
            
            
        Découper les taches:
        
            Le responsable de la story la découpe en tâches. Chaque tâche est un jour de travail.
            
            
            
            
        Développez une story:
        
            Chaque story suit les mêmes étapes qu’un projet séquentiel mais à son échelle:
                - écriture des spécifications fonctionnelles
                - écriture des spécifications techniques
                - code et test
                
            Quand la story passe les tests de confirmation, elle est déclarée finie et son responsable la déplace dans la colonne “Done”
            
            
        RESUME:
            une story passe par les phases suivantes :
                - Proposition : un jour, une personne suggère une nouvelle fonctionnalité
                - Acceptée : le Product Owner accepte la proposition et l’ajoute dans le backlog
                - Estimée : l’équipe estime la taille de la story
                - Prête : la story est écrite et estimée. Elle est en attente de développement
                - En cours : l’équipe est en train de la développer
                - Fini : elle est en ligne
            
            