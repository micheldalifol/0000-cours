# SUDO

> Le commande **`sudo`** permet de lancer des commandes avec les mêmes droits que root.
Mais pour sécuriser cela on peut mettre en place des règles.
Ces règles sont situées dans le fichier **`/etc/sudoers`**



#### Lister les droits sudo d'un utilisateur
```bash
# affiche tous les droits en sudo de l'utilisateur
dali@dali ~ % sudo -l
Matching Defaults entries for dali on dali:
    env_reset, ..., env_keep+="EDITOR VISUAL", env_keep+="HOME MAIL", lecture_file=/etc/sudo_lecture

User dali may run the following commands on MacBook-Pro-de-macbookprodali:
    (ALL) ALL
    (ALL) ALL
```

### Fonctionnement de la commande sudo
1. sudo va chercher dans le fichier sudoers si le user qui exécute la commande est référencé
2. sudo va chercher si la machine sur laquelle la commande est exécutée est référencée dans sudoers
3. sudo va vérifier si la commande demandée est autorisée par cet utilisateur pour cette machine dans sudoers

## Ficher /etc/sudoers

Ce fichier se modifie avec la commande **`visudo`**.

Il se définit par:
1. un nom d'utilisateur
2. une liste d'utilisateur (user1, user2, ..., userN)
3. un groupe unix `%<nomGroupe`, alors tous les membres du groupes sont autorisés à effectuer ces commandes
4. faire des alias

> _Remarque_: on peut aussi **exclure** un user pour une commande sudo définie, pour cela on ajoute **`!`** avant le nom du user dans le group

```bash
dali@dali ~ % sudo visudo
###############################################
##
# Override built-in defaults
##
Defaults        env_reset
Defaults        env_keep += "EDITOR VISUAL"
Defaults        env_keep += "HOME MAIL"
Defaults        lecture_file = "/etc/sudo_lecture"

##
# User alias specification
##
# User_Alias    FULLTIMERS = millert, mikef, dowdy
User_Alias      ADMIN = yves, pierre, jean

##
# Runas alias specification
##
# Runas_Alias   OP = root, operator

##
# Host alias specification
##
# Host_Alias    SERVERS = master, mail, www, ns


##
# Cmnd alias specification
##
# Cmnd_Alias    PAGERS = /usr/bin/more, /usr/bin/pg, /usr/bin/less
Cmnd_Alias      USERS = /usr/sbin/adduser, /usr/bin/passwd
Cmnd_Alias      TEST  = /usr/bin/echo, /usr/bin/id, /usr/bin/vi

##
# User specification
##

# root and users in group wheel can run anything on any machine as any user
root                  ALL = (ALL) ALL
%admin, !user1        ALL = (ALL) ALL
# user 1 ne peut pas faire les commandes sudo malgré le fait qu'il fasse parti du groupe admin qui a tous les droits
dali                  ALL = (ALL) ALL
#pierre                ALL = (yves) NOEXEC: /usr/bin/vi
pierre                ALL = (yves) NOEXEC: TEST
jean                  ALL = (ALL) NOPASSWD: /usr/bin/id


```

##### Explications du fichier
> **_Remarque_**: Ce fichier est lu de haut en bas, **l'ordre est donc très important** puisque **le dernier a raison** en cas de 2 valeurs différentes pour la même clé.
######  Defaults
Ce sont les actions par defaut qui se font lorsqu'on lance la commande sudo
On peut définir une regle pour un user particulier
```bash
Defaults:user1    regle
```


######  alias
Il y a 3 types d'alias: **alias de machines**, **alias d'utilisateurs** ou **alias de commandes**.
Cela correspond à des regroupement: plus facile pour la gestion
Il est important que ces alias commencent par une majuscules
```bash
User_Alias      ADMIN = yves, pierre, jean

Cmnd_Alias      USERS = /usr/sbin/adduser, /usr/bin/passwd
Cmnd_Alias      TEST  = /usr/bin/echo, /usr/bin/id, /usr/bin/vi
```
- ADMIN  :  groupe d'utilisateurs
- USERS  :  groupe de commandes pour la gestion d'utilisateurs
- TEST   :  groupe de commandes de test



######  user
```bash
root                  ALL = (ALL) ALL
%admin, !user1        ALL = (ALL) ALL
#ADMIN                 ALL = (ALL) ALL
dali                  ALL = (ALL) ALL
#pierre                ALL = (yves) NOEXEC: /usr/bin/vi
pierre                ALL = (yves) NOEXEC: TEST
jean                  ALL = (ALL) NOPASSWD: /usr/bin/id
```
- root    :  user root
- ALL     :  hôte pour lequel la règle est mise en place
- (ALL)   :  utilisateur
- ALL     :  groupe
- %admin  :  groupe admin
- ADMIN   :  groupe admin définit dans les alias (ici seulement 3 users)
- dali    :  user dali (tous les droits)
- pierre  :  user pierre lance en tant que yves le `NOEXEC` sur la commande `vi`. Cela permet de lancer certaines commandes sans pour autant en tant que root (securité)
```bash
pierre@dali sudo vi
#KO par le droit d'ouvrir vi en tant que pierre
pierre@dali sudo -u yves vi
# OK on peut ouvrir vi en tant que yves
```
- pierre  :  même chose avec TEST qui est un alias: sur toutes les commandes de test, pas le droits en sudo
- jean    :  peut lancer avec sudo la commande `id` sans que le mot de passe lui soit demandé

> _Remarques_:
>* le chemin complet de la commande est renseignée dans ce fichier (question sécurité)
>* on peut ajouter des commandes pour un user ou groupe en les séparants par une virgule
`user1    ALL = (ALL) /usr/bin/vi, /usr/bin/id`
