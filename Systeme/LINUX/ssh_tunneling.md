# TUNNEL SSH

https://www.malekal.com/comment-configurer-le-tunnel-ssh-redirection-de-port/             
https://www.it-connect.fr/chapitres/tunneling-ssh/            
https://www.tuteurs.ens.fr/internet/loin/tunnel.html              
https://www.linuxtricks.fr/wiki/ssh-creer-un-tunnel-pour-rediriger-des-ports-d-une-machine-a-l-autre           


![tunnel ssh](./images/ssh-tunnel-to-tunnel-server.png)      


tunnel ssh pour connexion machines rebong   

```bash
## export du proxy
export {http,https,ftp}_proxy="http://<fqdn proxy>:<port proxy>";
export {HTTP,HTTPS,FTP}_PROXY="http://<fqdn proxy>:<port proxy>";
export all_proxy="socks://http://<fqdn proxy>:<port proxy>/";
export ALL_PROXY="socks://http://<fqdn proxy>:<port proxy>/";


## configuration connexion GCP
gcloud config unset proxy/type
gcloud config unset proxy/address
gcloud config unset proxy/port

## definition de la cible pour le tunelling
target=192.168.x.x

## liste ressources distantes
gcloud compute instances list --zones=<nom zone de connexion> --project <nom projet connexion>

## demarrer la VM rebond
gcloud compute instances start <nom VM rebond> --project <nom projet connexion> --zone <nom zone de connexion> &    

## se connecter a la VM rebong
gcloud compute ssh <nom VM rebond> --tunnel-through-iap --project <nom projet connexion> --zone <nom zone de connexion>    

## etablir un tunnel avec la VM rebong
gcloud compute ssh <nom VM rebond> --tunnel-through-iap --project <nom projet connexion> --zone <nom zone de connexion> -- -L 127.0.0.1:22:${target}:22 -L 127.0.0.1:<port utilisé par appli>:${target}:<port utilisé par appli> -L 127.0.0.1:<port utilisé par https>:${target}:<port utilisé par https> -N
```