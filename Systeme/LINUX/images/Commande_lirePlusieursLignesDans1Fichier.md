# LIRE PLUSIEURS LIGNES DANS 1 FICHIER


```bash
## afficher plusieurs lignes dans 1 recherche dans 1 fichier
grep -[A|B] <nb lignes> <pattern recherchee> <filename>
## avec A after
## avec B before
```

exemple
```bash
cat fichier1
#--------------------------------
Running with gitlab-runner 16.6.1 (f5da3c5a)
  on gitlab-gitlab-runner-7bc9b8f655-4g94h nDvzRvNu, system ID: r_zCQGFZKCJduT
section_start:1706025451:resolve_secrets
Resolving secrets
section_end:1706025451:resolve_secrets
section_start:1706025451:prepare_executor
Preparing the "kubernetes" executor
Using Kubernetes namespace: runners
Using Kubernetes executor with image gcr.io/thl-ops-p-imghub/toolbox:latest ...
Using attach strategy to execute scripts...
section_end:1706025451:prepare_executor
section_start:1706025451:prepare_script
Preparing environment
Using FF_USE_POD_ACTIVE_DEADLINE_SECONDS, the Pod activeDeadlineSeconds will be set to the job timeout: 2h0m0s...
Waiting for pod runners/runner-ndvzrvnu-project-3111-concurrent-1-0q0l7nsd to be running, status is Pending
Running on runner-ndvzrvnu-project-3111-concurrent-1-0q0l7nsd via gitlab-gitlab-runner-7bc9b8f655-4g94h...
#--------------------------------

grep -A 2 "section_start:" fichier
#--------------------------------
section_start:1706025451:resolve_secrets
Resolving secrets
section_end:1706025451:resolve_secrets
section_start:1706025451:prepare_executor
Preparing the "kubernetes" executor
Using Kubernetes namespace: runners
--
section_start:1706025451:prepare_script
Preparing environment
Using FF_USE_POD_ACTIVE_DEADLINE_SECONDS, the Pod activeDeadlineSeconds will be set to the job timeout: 2h0m0s...
#--------------------------------

grep -B 3 "Waiting for pod" fichier
#--------------------------------
section_start:1706025451:prepare_script
Preparing environment
Using FF_USE_POD_ACTIVE_DEADLINE_SECONDS, the Pod activeDeadlineSeconds will be set to the job timeout: 2h0m0s...
Waiting for pod runners/runner-ndvzrvnu-project-3111-concurrent-1-0q0l7nsd to be running, status is Pending
#--------------------------------
```