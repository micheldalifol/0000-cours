# COMMANDES FIND ET GREP

## FIND


### Equivalent à locate
```bash
# equivalent à la commande `locate purge`
find / -type f -name "*purge*" 2>/dev/null
#2>/dev/null sert à éliminer les erreurs
```

### Lister tous les fichiers du répertoire où nous sommes
```bash
# lister a l'ecran
find $(pwd) -type f -not -path '*/\.*'
## -not -path '*/\.*' est pour ne pas afficher les fichiers cachés


# lister dans 1 fichier
find $(pwd) -type f -not -path '*/\.*' >> liste.txt

# lister seulement le nom du fichier sans le chemin
basename <fichierAvecCheminComplet>

# lister seulement les nom des fichiers listé dans 1 fichier
for line in  $(cat <nomFichier>); do basename "$line" ; echo ;  done >> <fichierSortie>

# lister seulement le chemin du fichier sans le novembre
dirname <fichierAvecCheminComplet>
```
find $(pwd) -type f >> ~/Documents/videos.txt

## GREP
```bash
# recherche dans tous les fichiers des expressions suivantes
dali@dali A_verifier_avant-Git % grep -ri xavki 2>/dev/null
Binary file ./PDF/Xavki/xavki.blog-Terraform.pdf matches
Binary file ./PDF/Xavki/xavki.blog-SSH.pdf matches
Binary file ./PDF/Xavki/xavki.blog-DockerCompose.pdf matches
Binary file ./PDF/Xavki/xavki.blog-Ansible.pdf matches
## -i est pour être insesible à la casse
## 2>/dev/null pour ne pa afficher les erreurs
```


```bash
## lire plusieurs lignes d'un fichier à partir d'une recherche  
grep -[A|B] <nb lignes> <pattern recherchee> <filename>

## A: after
## B: before  

grep -B 5 SUSPENDED test.json
#--------------------------------------
{
            "Id": "575290016478",
            "Arn": "arn:aws:organizations::xxx:account/o-zzz/575290016478",
            "Email": "email.aws@orga.com",
            "Name": "accountTest",
            "Status": "SUSPENDED"
#--------------------------------------
```