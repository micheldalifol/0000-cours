# INSTALLATION DE PAQUETS

[Documentation apt-get](https://doc.ubuntu-fr.org/apt-get)
[Documentation yum](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/system_administrators_guide/ch-yum)
[Documentation rpm](https://rpm.org/documentation.html)
[Documentation dpkg](https://debian-handbook.info/browse/fr-FR/stable/sect.manipulating-packages-with-dpkg.html)

## COMMANDES

| Action       |     apt-get    |        yum | rpm | dpkg |
| :------------ | :-------------: | :-------------: |:-------------: |-------------: |
| connaitre la version et les dépendances d'un paquet  | `apt-get show <nomPaquet>` | `` | `` | `` |
| installation de paquet | `apt-get install <nomPaquet> <nomPaquet2>` | `` | `` | `dpkg -i <paquet.deb>` |
| installation d'une version présente dans le dépôt | `apt-get install <nomPaquet>=<version> -V` | `` | `` | `` |
| réparer une installation | `apt-get install -f <nomPaquet>` | `` | `` | `` |
| recherche de paquet | `apt-cache search <mot1 mot2>` | `yum search <nomPaquet>` | `` | `` |
| liste les paquets installés | `` | `` | `yum list installed` | `dpkg -l <nomPaquet> <nomPaquet2>` |
| suppression paquet | `apt-get remove <nomPaquet> <nomPaquet2>` | `yum unistall <nomPaquet>` | `` | `dpkg -r <nomPaquet> <nomPaquet2>` |
| suppression de paquet et ses dépendances | `apt-get autoremove <nomPaquet> <nomPaquet2>` | `` | `` | `` |
| purge de paquet avec ses fichiers de configuration | `apt-get autoremove --purge <nomPaquet> <nomPaquet2>` | `` | `` | `dpkg -P <nomPaquet> <nomPaquet2>` |
| nettoie les copies des paquets installés dans _/var/cache/apt/archives_ | `apt-get clean` | `` | `` | `` |
| supprime les "vieilles" versions copies de paquets dans _/var/cache/apt/archives_ | `apt-get autoclean` | `` | `` | `` |
| mise à jour des paquets | ` apt-get update` | `yum update` | `` | `` |
| mise à jour des paquets définis | `apt-get update <nomPaquet> <nomPaquet2>` | `yum update <nomPaquet>` | `` | `` |
| upgrade des paquets | `apt-get upgrade` | `yum upgrade` | `` | `` |
| upgrade des paquets définis | `apt-get upgrade <nomPaquet> <nomPaquet2>` | `yum upgrade <nomPaquet>` | `` | `` |
| upgrade de tous les paquets et de la distribution | `apt-get dist-upgrade` | `` | `` | `` |


> _Remarques:
>  il arrive que des paquets ne se trouvent pas dans 1 dépôt, là on utilise dpkg
>  **dpkg** ne gère pas les dépendances, il faut souvent faire une réparation à sa suite (`apt-get install -f <nomPaquet>`)_



## BLOQUER LES MISES A JOUR D'UN PAQUET

Il faut aller dans le fichier **`/etc/apt/preferences`** et ajouter les lignes ci-dessous:
```bash
Package: nom_du_paquet
Pin: version du paquet à conserver
Pin-priority: 1001
```
> _Remarque: si ce fichier n'existe pas, il faut le créer._

Dans notre exemple, _Pin-priority_ à **1001** bloque les futures versions.
