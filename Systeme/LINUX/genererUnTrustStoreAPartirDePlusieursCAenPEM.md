# GENERATION DE TRUST STORE A PARTIR DE PLUSIEURS CA AU FORMAT PEM

Pour générer un trust store à partir de plusieurs chaines de CA en format PEM:

```bash
keytool.exe -importcert -v -noprompt -trustcacerts -file certificat_2021_01.crt -alias CA_chain_2021_01 -keystore mon_truststore.jks -storepass <encryptionService>

keytool.exe -importcert -v -noprompt -trustcacerts -file certificat_2023_02.crt -alias CA_chain_2023_02 -keystore mon_truststore.jks -storepass <encryptionService>

keytool.exe -importcert -v -noprompt -trustcacerts -file certificat_2023_06.crt -alias CA_chain_2023_06 -keystore mon_truststore.jks -storepass <encryptionService>
```

Pour visualiser vite fait:

```bash
keytool -list -keystore  mon_truststore.jks -storepass <encryptionService>
#-------------------------------
Keystore type: JKS
Keystore provider: SUN

Your keystore contains 3 entries

ca_chain_2021_01, 7 nov. 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 98:54:...:4E:4E
ca_chain_2023_06, 7 nov. 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 50:0A:...:01:2C
ca_chain_2023_02, 7 nov. 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 59:A7:...:E0:B5
#-------------------------------
```
 


Pour visualiver avec + de détails :

```bash
keytool -list -keystore  mon_truststore.jks -storepass Krypton -v | egrep --color -C 1 -i "keystore|certificate\[|chain|CN=|alias|entry|Owner|Issuer|valid|\*|$"
#-------------------------------
Keystore type: JKS
Keystore provider: SUN

Your keystore contains 3 entries

Alias name: ca_chain_2021_01
Creation date: 7 nov. 2023
Entry type: trustedCertEntry

Owner: CN=totocloud.io, OU=DIS, O=toto, C=FR, ST=BDR, L=La Ciotat
Issuer: CN=Gemalto Business Solutions Certificate Authority, O=Gemalto, L=Tours, C=FR
Serial number: xxxxxxxxxx
Valid from: Wed Jan 27 09:31:31 CET 2021 until: Mon Oct 29 11:42:35 CET 2029

*******************************************
*******************************************

Alias name: ca_chain_2023_06
Creation date: 7 nov. 2023
Entry type: trustedCertEntry

Owner: CN=CCoE Sub CA, OU=COO, O=toto DIS, C=FR, ST=BDR, L=La Ciotat
Issuer: CN=toto DIS Servers Certificate Authority, O=toto DIS, L=Tours, C=FR, EMAILADDRESS=certificate.automation@toto.com
Serial number: yyyyyyyy
Valid from: Wed Jun 07 17:23:42 CEST 2023 until: Wed Jun 07 17:24:42 CEST 2028

*******************************************
*******************************************


Alias name: ca_chain_2023_02
Creation date: 7 nov. 2023
Entry type: trustedCertEntry

Owner: CN=Root CA, O=toto Cloud
Issuer: CN=Root CA, O=toto Cloud
Serial number: zzzzzzzzz
Valid from: Thu Feb 23 16:46:45 CET 2023 until: Wed Feb 23 22:47:14 CET 2028
#-------------------------------
```
