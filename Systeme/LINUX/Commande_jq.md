# COMMANDE JQ  

La commande `jq` permet de parser les fichiers json    

[Documentation officielle jq](https://jqlang.github.io/jq/manual/)         

## PRE-REQUIS

le package `jq` doit être installé pour pouvoir l'utiliser


```bash
## INSTALLATION
#---------------------------------
## Debian/Ubuntu
sudo apt-get update
sudo apt-get install jq


## RHEL/Fedora
sudo yum install jq


## MacOSx
brew install jq
#---------------------------------

## VERIFICATION DE L'INSTALLATION
#---------------------------------
jq --version
#---------------------------------
```

## SYNTAXE ET OPTONS

La syntaxe d'utilisation de la commande `jq` est la suivante:    
`jq [options] <filter> <input-file>`        
ou     
`jq [options] --args  [json_string]`          

 - **options**: Les **options** spécifient des indicateurs ou des options facultatives que vous pouvez utiliser pour modifier le comportement de la commande `jq`. Par exemple, vous pouvez spécifier des options telles que `--compact-output` pour générer une sortie compacte sans espace                    
 - **filter**: L'expression du **filtre** JQ est entourée de guillemets simples (''). Cette expression de filtre précise les transformations ou opérations à appliquer aux données JSON       
 - **input-file**: Le **input-file** est le chemin ou le nom de fichier du fichier JSON d'entrée que vous souhaitez traiter avec la commande `jq`    


**Commande permettant de connaitre la structure des fichier json (utile pour la suite des commandes)**: 
```bash
jq -c 'paths' <NomFichierJson>
```


_exemple_: fichier data.json              
```json
{
   "name": "Linux User",
   "age": 30,
   "email": "linuxUser@example.com",
   "languages": ["Python", "JavaScript", "Java"],
   "country": { "name": "India", "code": "IN" }
}
```
Avec la commande suivante on a en sortie:    

```bash
## connaitre la structure du fichier data.json
jq -c "paths" data.json
#-----------------------------------------
["name"]
["age"]
["email"]
["languages"]
["languages",0]
["languages",1]
["languages",2]
["country"]
["country","name"]
["country","code"]
#-----------------------------------------



## sortie formatée du fichier
jq '.' data.json
#--------------------------------------
{
  "name": "Linux User",
  "age": 30,
  "email": "linuxUser@example.com",
  "languages": [
    "Python",
    "JavaScript",
    "Java"
  ],
  "country": {
    "name": "India",
    "code": "IN"
  }
}
#--------------------------------------

## sortie spécifique avec ".name" étant un filtre
jq '.name' data.json
#--------------------------------------
"Linux User"
#--------------------------------------

## idem mais dans le json du json data.json
jq '.country.name' data.json
#--------------------------------------
"India"
#--------------------------------------

## sortie d'un tbaleau dans le json
jq '.languages[1]' data.json
#--------------------------------------
"JavaScript"
#--------------------------------------

## sortie d'une partie de tableau dans le json
jq '.languages[0:2]' data.json
#--------------------------------------
[
  "Python",
  "JavaScript"
]
#--------------------------------------
```

Il est possible d'aller plus loin dans le filtre avec des critéres spécifiques:    

```bash
## fonction "with_entries"
jq 'with_entries(select(.value | type == "string"))' data.json
#--------------------------------------
{
  "name": "Linux User",
  "email": "linuxUser@example.com"
}
#--------------------------------------
## La fonction with_entries de JQ vous permet de transformer les propriétés d'un objet JSON individuellement. Il prend une expression de filtre comme argument et l'applique à chaque paire clé-valeur de l'objet
## La fonction select de JQ vous permet de filtrer les données JSON en fonction de conditions ou de critères spécifiques
```


Utilisation de conditions     

```bash
jq '. + { "isAdult": (.age >= 18) }' data.json
#--------------------------------------
{
  "name": "Linux User",
  "age": 30,
  "email": "linuxUser@example.com",
  "languages": [
    "Python",
    "JavaScript",
    "Java"
  ],
  "country": {
    "name": "India",
    "code": "IN"
  },
  "isAdult": true
}
#--------------------------------------
## mise en place de la consition "isAdult" si l'age est >= 18 ans (cas ici puisque 30 ans) -> la condition isAdult est retournée en "true"
## dans le cas contraire elle est en "false"
```


Changement de format d'écriture de certain contenu du json

```bash
jq '.languages[] |= ascii_upcase' data.json
#--------------------------------------
{
  "name": "Linux User",
  "age": 30,
  "email": "linuxUser@example.com",
  "languages": [
    "PYTHON",
    "JAVASCRIPT",
    "JAVA"
  ],
  "country": {
    "name": "India",
    "code": "IN"
  }
}
## les valeurs du tableau "languages" sont en capitales
#--------------------------------------
```



### OPTIONS 

| OPTION       |        OBJECTIF |
| :------------ | -------------: |
| `-c` ou `--compact-out`       | La commande `jq` imprime la sortie JSON dans le joli format d'impression par défaut. Cette option est utilisée pour imprimer la sortie JSON en supprimant les espaces inutiles qui minimisent la taille du fichier JSON. |
| `-f <filename>` ou `--from-file <filename>` | Normalement, la commande `jq` est exécutée depuis le terminal. Cette option est utilisée pour lire la commande `jq` depuis le fichier |
| `-n` ou `--null-input` | L'option est utilisée pour générer les données JSON sans aucune entrée. |
| `--raw-input` ou `-R` | utilisée pour prendre l'entrée sous forme de chaîne au lieu de JSON |
| `--color-output` ou `-C` | utilisée pour générer la sortie couleur avec force lorsque la sortie est envoyée vers un tube ou un fichier |
| `-–ascii-output` ou `-a` | utilisée pour générer la sortie ASCII avec force |
| `–-sort-keys` ou `-S` | utilisée pour générer la sortie triée en fonction de la clé |
| `--unbuffered` | utilisée pour vider la sortie après l'impression de l'objet JSON |
| `--version` | utilisée pour afficher la version de jq |


D'autes existent, pour un complément voir la documentation officielle







### FILTERS

| FILTRE | OBJECTIF |
| :------------  | -------------:   |
| `.` | appelé l'opérateur d'identité utilisé pour générer la sortie inchangée de l'entrée |
| `.object` | appelé l'identifiant d'objet utilisé pour générer la valeur de l'objet |
| `.[]` | appelé l'itérateur de tableau ou de valeur d'objet qui renvoie tous les éléments du tableau |
| `.[index]` | appelé un index de tableau utilisé pour lire les valeurs clés en fonction de l'index |
| `.[5:10]` | appelé la tranche de tableau ou la tranche de chaîne qui est utilisée pour renvoyer un sous-tableau ou la sous-chaîne |



D'autes existent, pour un complément voir la documentation officielle



## SORTIE FORMATTEE

Si vous ne vous souciez que du formatage de sortie (jolie impression), exécutez

`jq . my.json`

ou lors de la lecture à partir d'un pipeline

`cat my.json | jq`

>_Remarque_: pour la redirection, vous devez également passer un filtre pour éviter une erreur de syntaxe:

`jq . my.json > output.json`


## EXEMPLES D'EXTRACTION AVEC JQ    

Par exemple le fichier suivant data2.json:    

```json
{
	"timestamp": 1234567890,
	"report": "Age Report",
	"results": [
		{ "name": "John", "age": 43, "city": "TownA" },
		{ "name": "Joe",  "age": 10, "city": "TownB" }
	]
}
```

Pour extraire les attributs de niveau supérieur “timestamp” et “report”    
```bash
jq '. | {timestamp,report}' data2.json
#--------------------------------------
{
  "timestamp": 1234567890,
  "report": "Age Report"
}
#--------------------------------------
```
    

Pour extraire le nom et l’âge de chaque élément “results”     

```bash
jq '.results[] | {name, age}' data2.json
#--------------------------------------
{
  "name": "John",
  "age": 43
}
{
  "name": "Joe",
  "age": 10
}
#--------------------------------------
```   

Pour extraire name et age sous forme de valeurs de texte au lieu de JSON     
```bash
jq -r '.results[] | {name, age} | join(" ")' data2.json 
#--------------------------------------
John 43
Joe 10
#--------------------------------------
```
  


Filtrer ceci par attribut:        

```bash
jq '.results[] | select(.name == "John") | {age}' data2.json  # Obtenez l'age de Joe
#--------------------------------------
{
  "age": 43
}
#--------------------------------------

jq '.results[] | select((.name == "Joe") and (.age = 10))' data2.json # Obtenez toutes les données pour Joe agé de 10 ans
#--------------------------------------
{
  "name": "Joe",
  "age": 10,
  "city": "TownB"
}
#--------------------------------------

jq '.results[] | select(.name | contains("Jo"))' data2.json   # Obtenez toutes les données pour les noms contenant Jo
#--------------------------------------
{
  "name": "John",
  "age": 43,
  "city": "TownA"
}
{
  "name": "Joe",
  "age": 10,
  "city": "TownB"
}
#--------------------------------------

jq '.results[] | select(.name | test("Joe\\s+Smith"))' data2.json  # Obtenez des enregistrements complets pour tous les noms correspondant à l'expression régulière PCRE 'Joe\+Smith'
#--------------------------------------
      ## rien car pas de correspondance
#--------------------------------------

jq '.results[] | select(.name | test("Joe"))' data2.json
#--------------------------------------
{
  "name": "Joe",
  "age": 10,
  "city": "TownB"
}
#--------------------------------------
```




### ACCEDER A DES CLES INCONNUES    

Lors du traitement d'objets, vous ne connaissez peut-être pas certaines clés, dans ce cas, utilisez to_entries. Par exemple, si vous souhaitez avoir tous les champs de propriété du JSON suivant:

```bash
echo '{
	"name": "R1",
	"type": "robot",
	"prop1": "a5482na",
	"prop2": null,
	"prop3": 55 
}' |\
jq '. | to_entries[] | select( .key | contains("prop"))'
#--------------------------------------
{
  "key": "prop1",
  "value": "a5482na"
}
{
  "key": "prop2",
  "value": null
}
{
  "key": "prop3",
  "value": 55
}
#--------------------------------------
```



## CHANGER LES VALEURS AVEC JQ    

Fusionner/écraser des clés    

```bash
echo '{ "a": 1, "b": 2 }' |\
jq '. |= . + {
  "c": 3
}'
#--------------------------------------
{ "a": 1, "b": 2, "c": 3 }
#--------------------------------------
```

Ajouter des éléments aux listes

```bash
echo '{ "names": ["Marie", "Sophie"] }' |\
jq '.names |= .+ [
   "Natalie"
]'
#--------------------------------------
["Marie", "Sophie", "Natalie"]
#--------------------------------------
```
   
## SUPPRIMER DES VALEURS AVEC JQ   

Syntaxe: `jq 'del(.somekey)' input.json`   


## FUSIONNER LES CHAINES JSON    

Par exemple fusionner trois listes d'objets:

```bash
echo '[ {"a":1}, {"b":2} ]' | \
jq --argjson input1 '[ { "c":3 } ]' \
   --argjson input2 '[ { "d":4 }, { "e": 5} ]' \
   '. = $input1 + . +  $input2'
#--------------------------------------
[ { "c":3 }, {"a":1}, {"b":2}, { "d":4 }, { "e": 5} ]
#--------------------------------------
```

### FUSIONER LES FICHIERS (dispo depuis jq v1.4)    

La commande suivante fusionnera "somekey" des deux fichiers transmis

`jq -s '.[0] * .[1] | {somekey: .somekey}' <file1> <file2>`    


### PRENDRE EN CHARGE DES TABLEAUX VIDES   

Lorsque vous souhaitez parcourir un tableau et que le tableau auquel vous accédez est vide, vous obtenez quelque chose comme     
`jq: error (at <stdin>:3): Cannot iterate over null (null)`        

Pour contourner le tableau facultatif, protégez l'accès avec     
`select(.my_array | length > 0)`      



### EXTRAIRE LE NOM DES CLES    

Soit le fichier data3.json suivant

```json
{
   "animals": [
	   "dog": { },
	   "cat": { }
	 ]
}
```

vous pouvez extraire les noms des animaux en utilisant:     
`jq '.animals | keys' data3.json`       


## UTILISER JQ DANS DES SCRIPTS SHELL    


### ANALYSER LES VARIABLES D'ENVIRONNEMENT DANS UN JSON   

Pour remplir les variables d'environnement à partir des clés d'objet JSON (par exemple `$FOO` de la requête jq « .foo »)        
`export $(jq -r '@sh "FOO=\(.foo) BAZ=\(.baz)"')`    

Pour créer un tableau bash à partir d'une liste JSON:     

```bash
bash_array=($(jq -r '.mylist | @sh'))
# or

read -a bash_array < <(jq -r .|arrays|select(.!=null)|@tsv)
```

### JSON TEMPLATE UTILISANT ENV VARS 

Pour créer un JSON approprié à partir d'un script shell et échapper correctement aux variables:        
`jq -n --arg foobaz "$FOOBAZ" '{"foobaz":$foobaz}'`    




### CONCATENATION   

Exemple de Concatenation:

```bash
echo '{ "object" : { "name": "banana", "color": "yellow" }}' |\
jq -r '.object | (.name)+" is "+(.color)'
#--------------------------------------
banana is yellow
#--------------------------------------
```


### INTERPOLATION   

exemple d'Interpolation:        

```bash
echo '{ "object" : { "name": "banana", "color": "yellow" }}' |\
jq -r '.object | "\(.name) is \(.color)"'
#--------------------------------------
banana is yellow
#--------------------------------------
```





## FONCTIONS MATHEMATIQUES    

`jq` peut utiliser la fonction mathématique de votre libc. Par exemple:

```bash
echo '{ "a": 1234.56 }' | jq '.a | round'     # gives 1235
echo '{ "a": 1234.56 }' | jq '.a | floor'     # gives 1235
echo '{ "a": 1234.56 }' | jq '.a | ceil'      # gives 1234
```
