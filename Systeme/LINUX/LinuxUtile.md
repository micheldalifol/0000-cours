# <span style="color: #CE5D6B"> LINUX UTILES</span>


_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

## COMMANDES LINUX
</span>

=======================================================================================================================  

<span style="color: #EA9811">

### COMMANDE `cat -A nomFichier`
</span>

```bash
cat test.txt
Ceci est un fichier de Test
pour voir certaines choses qui peuvent être utile dans certains cas pour par exemple m assurer de la présence de tous les caractères dans 1 ligne en validant le début ET la fin de la ligne

autre exemple avec une          tabulation  ## 2 tabulations ont été mises avant le mot tabulation

et un autre avec                            ## les touches CTRL-M ont été faite ici -> retour à la ligne
CTRL-M

## commande cat -A nomFichier
cat -A test.txt
Ceci est un fichier de Test$
pour voir certaines choses qui peuvent M-CM-*tre utile dans certains cas pour par exemple m assurer de la prM-CM-)sence de tous les caractM-CM-(res dans 1 ligne en validant le dM-CM-)but ET la fin de la ligne$
$
autre exemple avec une ^I^Itabulation$
$
et un autre avec $
CTRL-M$
```

> Cette commande permet d'afficher tous les caractères d'un fichier en indiquant avec un **$** la fin d'une ligne





=======================================================================================================================  

<span style="color: #EA9811">

### COMMANDE `<command> 2>&1 > /dev/null | echo $?`
</span>




Cette commande permet d'envoyer vers `/dev/null` tous les resultats de la commande            

-  `2>` envoie les erreur ; &1 envoie les "non" erreurs ; `> /dev/null` vers le fichier `/dev/null`      
-  `| echo $?` permet d'avoir un retour sur la commande        
  -> 0 : l'action "normale" de la commande se fait            
  -> 1 : on est en erreur         

```bash
## commande classique OK
nslookup google.fr
#---------------------------------------------
Server:         10.10.23.36
Address:        10.10.23.36#53

Non-authoritative answer:
Name:   google.fr
Address: 64.233.184.94
Name:   google.fr
Address: 2a00:1450:400c:c0b::5e
#---------------------------------------------

## commande avec redirection
nslookup google.fr 2>&1 > /dev/null | echo $?
#---------------------------------------------
0
#---------------------------------------------


## commande classique KO
nslookup toto                     ## toto n'existe pas -> en erreur
#---------------------------------------------
;; Got SERVFAIL reply from 10.10.23.36, trying next server
;; Got SERVFAIL reply from 10.42.178.146, trying next server
Server:         10.42.178.145
Address:        10.42.178.145#53

** server can t find toto: SERVFAIL
#---------------------------------------------

## commande avec redirection
nslookup toto 2>&1 > /dev/null | echo $?
#---------------------------------------------
1
#---------------------------------------------
```

=======================================================================================================================  

<span style="color: #EA9811">

### COMMANDE `<command> | tee -a <fichiercible>`
</span>


Cette commande permet de renvoyer tout le contenu du terminal lors de l'exécution des commande dans 1 fichier cible 

```bash
## exemple de commande 

echo "salut debut du fichier cible" | tee -a ./fichierCible.txt
#-----------------------------------------------
salut debut du fichier cible
#-----------------------------------------------
cat ./fichierCible.txt
#-----------------------------------------------
salut debut du fichier cible
#-----------------------------------------------

echo "test2" | tee -a ./fichierCible.txt
#-----------------------------------------------
test2
#-----------------------------------------------
cat ./fichierCible.txt
#-----------------------------------------------
salut debut du fichier cible
test2
#-----------------------------------------------
```


=======================================================================================================================  




_______________________________________________________________________________________________________________________  

# FIN