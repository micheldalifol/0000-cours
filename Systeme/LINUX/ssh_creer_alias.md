# CREER UN ALIAS SSH

1. Editer le fichier `~/.ssh/config` et saisir les `alias`

_Exemple_

```bash
Host monSite-preprod
    User monSitepreprod
    Hostname xxx.xxx.xx.xxx
Host monSite-prod
    User monSiteprod
    Hostname xxx.xxx.xx.xxx
```

A été défini 2 alias
 * le nom (host)
 * l'utilisateur (user)
 * l'IP du serveur (IPAdress)

2. Se connecter au site

```bash
ssh monSitepreprod
```
Le terminal demande le mot de passe

3. Vous êtes connecté
