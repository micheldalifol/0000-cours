# CERTIFICAT SSL

## POURQUOI LE HTTPS

Le **HTTPS** est un protocole qui permet d’encrypter les échanges entre 1 serveur et le navigateur web.
Il nécessite de créer un **certificat SSL** qui permettra d’initialiser les échanges cryptés.

Pour être reconnu par un navigateur web, le certificat SSL doit être signé par un organisme de certification. Ces organismes font payer cette certification.

Il est aussi possible d'avoir des **certificats auto-signés** qui seront visibles sur le navigateur avec 1 message d'alerte mais pour un site perso, cela n'est pas grave, le ssl est tout de même en place.

Le cas d’un certificat auto-signé peut être intéressant pour la gestion d’un intranet. L’idée serait de générer un certificat maître auto-signé qui signerait ensuite lui même tous les certificats SSL de l’ensemble des services utilisé dans l’intranet. C’est ce qu’on appelle une **PKI (Public Key Infrastructure)**.

On peut ensuite installer le certificat maître sur tous les postes des utilisateurs de l’intranet. Ces derniers pourront alors consulter tous les sites en HTTPS sans avoir d’alerte de sécurité.

## CREER UN CERTIFICAT AUTO-SIGNE

Pour cela nous devons avoir installé **openssl**.

```bash
## 1. installation de openssl
apt-get install openssl

## 2. Génération de la clé privée
### clé privée de 4096 bit encrypté avec l’algorithme de cryptage AES 256 bit
openssl genrsa -aes256 -out certificat.key 4096

## 3. renommer notre clé
mv certificat.key certificat.key.lock


## 4. générer le certificat déverrouillé
### si ce certificat reste vérouillé, le mot de passe de la clé sera demandé à chaque redémarrage du service
openssl rsa -in certificat.key.lock -out certificat.key

## 5. nous avons les 2 clés: vérrouillée et dévérrouillée
ls -la
certificat.key          # déverrouillée
certificat.key.lock     # verrouillée
```

## GENERATION DU FICHIER DE DEMANDE DE SIGNATURE

```bash
## fichier utile pour obtenir notre certification de l’organisme ou pour auto-signer notre certificat
openssl req -new -key certificat.key.lock -out certificat.csr

### renseigner les differents champs

## fichier de demande de signature : certificat.csr
ls -la
certificat.csr
```

## GENERATION DU CERTIFICAT

```bash
## Pour auto-signer ton certificat, exécuter la commande suivante :
openssl x509 -req -days 3650 -in certificat.csr -signkey certificat.key.lock -out certificat.crt
### le certificat est créer pour 10 ans (3650 jours)

##    ### autre exemple avec un autre format
      openssl req -x509 -new -nodes -key certificat.key -sha256 -days 3650 -out certificat.pem
      openssl x509 -in certificat.pem -inform PEM -out certificat.crt

## Ton certificat : certificat.crt
ls -la
certificat.crt

##  ### autre exemple
##  certificat.pem    certificat racine au format pem
##  certificat.key    clé privée
##  certificat.crt    certificat racine au format crt
```

## INDIQUER A  APACHE D'UTILISER LE CERTIFCAT SSL

```bash
## 1. vérifier que le ssl est activé sur apache
a2enmod ssl

## 2. créer un vHost sur le port 443 (port du https)
vi /etc/apache2/site-availables/tuto.conf
################################################################
<VirtualHost *:80>
    ServerName      tuto.monSite.fr
    # On redirige le port HTTP vers le port HTTPS
    Redirect        / https://www.rmonSite.fr
</VirtualHost>

<VirtualHost *:443>
    ServerName      tuto.monSite.fr
    DocumentRoot    /var/www/tuto

    SSLEngine on
    SSLCertificateFile    /etc/ssl/www/certificat.crt
    SSLCertificateKeyFile /etc/ssl/www/certificat.key
    # Facultatif, ici, on dit qu'on accepte tout les protocoles SSL sauf SSLv2 et SSLv3 (dont on accepte que le TLS ici)
    SSLProtocol all -SSLv2 -SSLv3
    # Facultatif, on dit que c'est le serveur qui donne l'ordre des algorithmes de chiffrement pendant la négociation avec le client
    SSLHonorCipherOrder on
    # Facultatif, algorithme de chiffrement disponibles (ne pas être trop méchant sinon beaucoup de navigateur un peu ancien ne pourront plus se connecter)
    SSLCipherSuite ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES
</VirtualHost>
################################################################

## 3. activer le vhost
a2ensite tuto.conf
service apache2 restart

### le site fonctionne en https
```
