# PROCESSUS ZOMBIES

Tous programmes mal écrits ou fonctionnant mal peuvent laisser des processus zombies cachés.

## FONCTIONNEMENT DES ETATS DE PROCESSUS SOUS LINUX

[processus linux](https://linux.goffinet.org/administration/processus-et-demarrage/processus-linux/)

Toutes applications ou démons ont un trace. Cette trace alimente la table des processus.

Chaque processus est identifier par son **PID**.
Ce PID peut-être utiliser pour changer la priorité d'un processus ou l'arrêter.
Un processus peut-être un processus fils ou parent (PID2 ou PID1)

Pour connaitre l'arborescence d'un processus on utilise la commande `pstree` avec les options `-p` (affiche les PIDs) et `-h` (mets en gros les processus utilisateurs)

### Recherche de processus en cours d'execution
La commande `ps` avec diverses options est utilisés pour lister les processus en cours d'exécution.
```bash
ps -edfjH
#affiche toutes les informations intéressantes avec une arborescence
```
La commande `top` permet de lister combien de processus zombies sont en cours (en haut à droite)
```bash
[admb0023022@bilps193159m ~]$ top
top - 11:00:49 up 6 days, 19:01,  1 user,  load average: 1.00, 1.00, 1.00
Tasks: 310 total,   3 running, 307 sleeping,   0 stopped,   0 zombie
%Cpu(s): 20.1 us, 30.4 sy,  0.0 ni, 49.3 id,  0.0 wa,  0.2 hi,  0.0 si,  0.0 st
MiB Mem :   3736.1 total,   1875.4 free,    618.9 used,   1241.8 buff/cache
MiB Swap:   3972.0 total,   3972.0 free,      0.0 used.   2843.8 avail Mem

PID USER      PR  	NI    	VIRT    	RES    	SHR 	S  	%CPU  	%MEM    TIME+ 	COMMAND
1209 rngd      20   0  		233876   	4320   	3572 	R 	100.0   0.1   	9739:20 rngd
1255 root      20   0 		2684492 	212988  22100 	S   0.3   	5.6   	5:14.21 java

```
La colonne "S" permet de voir l'état du processus
* R: runnable
* S: sleeping
* D: dead
* T: traced
* Z: zombie

## COMMENT TUER UN PROCESSUS ZOMBIE

Il n'est pas possible de le tuer puisqu'il est déjà mort.
La seule solution est le tuer le processus parent

1. La commande `ps aux | egrep "Z|defunct"` permet de lister les processus zombies (donc de connaitre leur PID)

2. La commande `ps -o ppid= -p <numPIDzombie` permet de connaitre le PID parent

3. La commande `kill -SIGKILL <PIDparent>` tue le processus parent donc le processus zombie
