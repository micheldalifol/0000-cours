# SCRIPT BASH


Un script bash est un script sous linux qui permet d'exécuter des commandes plus ou moins complexes sans à avoir à les ré-écrire à chaque fois    

Sa syntaxe est la suivante

```bash
#!/bin/sh -e          ## shebang, cette ligne DOIT être la 1ere ligne du script    

## ceci est un commentaire, on le place où on le souhaite dans le script pour donner des indications au lecteur du script 

set -v        ## utilisé pour le mode verbeux (lors du debug) -> peut être supprimé ou commenté une fois le script validé  
## se positionne en début de script (ou en debut de session à débugguer) 
## par defaut le debug commence où se trouve cette commande, si pas la même commande dans le script, le mode verbeux se fera alors jusqu'à la fin de celui-ci  

set -vx       ## debug où on affiche les commandes PUIS les réponses à ces commandes ...

## variables, fonctions, commandes souhaitées pour le script
```