# COMMANDES RSYNC, CP, MV

## pour synchroniser
* tous("a") les fichiers
* en parlant("v");
* `--delete-before` permet de supprimer dans la destination ce qui a été supprimé dans la source avant de faire la synchronisation

```bash
rsync -avzh --delete-before <source> <destination>
```

### pour copier de tous("a") les fichiers en parlant("v") de manière récurcive ("r")

```bash
cp -avr <source> <destination>
```

### pour déplacer de tous("a") les fichiers en parlant("v")

```bash
mv -avr <source> <destination>
```
