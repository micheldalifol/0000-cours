# FFMPEG

## Documentaion officielle
[ffmpeg](https://ffmpeg.org/)
[commandes utiles_1](https://digitalfortress.tech/tricks/encode-videos-with-ffmpeg/ )
[commandes utiles_2](https://dev.to/benjaminblack/use-ffmpeg-to-compress-and-convert-videos-458l )
## Installation

[explications et commandes](https://emirchouchane.com/tutoriel-ffmpeg/)
[git pour installation complète](https://gist.github.com/Piasy/b5dfd5c048eb69d1b91719988c0325d8)

* installation ffmpeg avec toutes les options
```bash
brew install ffmpeg $(brew options ffmpeg | grep -vE '\s' | grep -- '--with-' | tr '\n' ' ')
```
## Utilisation
### Connaitre les infos d'une vidéos
```bash
ffmpeg -i <nomVideo>
```

### Convertir une vidéo dans un autre format
```bash
ffmpeg -i <nomVideoAConvertir.nomFormat> <nomVideoConvertie.nouveauFormat>
```
### Changer le format d'une vidéo
```bash
ffmpeg -i <nomVideoAConvertir.nomFormat> -acodec copy -vcodec copy <nomVideoConvertie.nouveauFormat>
```
#### Convertir un.mkv en .avi
**un filtre supplémentaire est nécessaire**
```bash
ffmpeg -i <nomVideoAConvertir.nomFormat> -acodec copy -vcodec copy -bsf:v h264_mp4toannexb <nomVideoConvertie.nouveauFormat>
```

### CHANGER LA TAILLE D'UNE VIDEO SANS PERDRE LA QUALITE (non)
#### h264
```bash
ffmpeg -i inputFile.mkv -c:v libx264 -preset ultrafast -crf 0 outputFile.mkv
```
__crf__: option avec "0" pour ne pas perdre en qualité

####  h265
```bash
ffmpeg -i inputFile.mkv -c:v libx265 -crf 0 outputFile.mkv
```
