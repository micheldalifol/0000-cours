# COMMANDE CUT

## SYNTAXE ET OPTIONS  

`cut options [fichier]`  

| Options | Descriptions |
| :------ | -----: |
| `-f`  | Spécifiez les champs que vous souhaitez extraire | 
| `-c` | Spécifiez les caractères que vous souhaitez extraire | 
| `-b` | Fournissez les octets que vous souhaitez extraire | 
| `-d` | Ici, vous spécifiez le délimiteur que vous souhaitez utiliser avec la commande CUT. Par défaut, l’onglet est considéré comme un délimiteur | 
| `-–complement` | Il est utilisé pour obtenir des colonnes non spécifiées par -f, -c ou -b Options | 
| `-–output-delimiter` | Par défaut, la commande CUT utilise le délimiteur d’entrée en tant que délimiteur de sortie, mais vous pouvez modifier ce comportement en utilisant cette option | 


## EXEMPLES 

```bash
## fichier ex1.txt
#-----------------------------------------
Riveau Alain	  1919-05-22	2020-02-01	Directeur des operations
Cavalier Bart	  1987-09-09	2020-09-01	Directeur des ventes
Stone Emma	    1991-01-30	2021-01-02	Directrice des communication
Trembblay Guy	  1962-02-02	2020-08-01	Vendeur
#-----------------------------------------


## sortir des éléments du fichier
cut -f 2 ex1.txt
#-----------------------------------------
1919-05-22
1987-09-09
1991-01-30
1962-02-02
#-----------------------------------------

cut -f 1-3 ex1.txt
#-----------------------------------------
Riveau Alain    1919-05-22      2020-02-01
Cavalier Bart   1987-09-09      2020-09-01
Stone Emma      1991-01-30      2021-01-02
Trembblay Guy   1962-02-02      2020-08-01
#-----------------------------------------

cut -f -3 ex1.txt
#-----------------------------------------
Riveau Alain    1919-05-22      2020-02-01
Cavalier Bart   1987-09-09      2020-09-01
Stone Emma      1991-01-30      2021-01-02
Trembblay Guy   1962-02-02      2020-08-01
#-----------------------------------------

cut -f 2- ex1.txt
#-----------------------------------------
1919-05-22      2020-02-01      Directeur des operations
1987-09-09      2020-09-01      Directeur des ventes
1991-01-30      2021-01-02      Directrice des communication
1962-02-02      2020-08-01      Vendeur
#-----------------------------------------

cut -f 2-4 ex1.txt
#-----------------------------------------
1919-05-22      2020-02-01      Directeur des operations
1987-09-09      2020-09-01      Directeur des ventes
1991-01-30      2021-01-02      Directrice des communication
1962-02-02      2020-08-01      Vendeur
#-----------------------------------------

#=====================================================

## couper des parties du fichier
cut -d ',' -f 2 ex1.txt     ## pas de ',' donc pas de changements
#-----------------------------------------
Riveau Alain      1919-05-22    2020-02-01      Directeur des operations
Cavalier Bart     1987-09-09    2020-09-01      Directeur des ventes
Stone Emma        1991-01-30    2021-01-02      Directrice des communication
Trembblay Guy     1962-02-02    2020-08-01      Vendeur
#-----------------------------------------

cut -d ' ' -f 2 ex1.txt   ## le délimiteur est l'espace et on cherche la 2e colonne du fichier (après le 1er espace)
#-----------------------------------------
Alain
Bart
Emma
Guy
#-----------------------------------------

cut -d ' ' -f 1 ex1.txt
#-----------------------------------------
Riveau
Cavalier
Stone
Trembblay
#-----------------------------------------

#=====================================================

## modifier la sortie en ajoutant 1 delimiter
cut -f 1,2,4 --output-delimiter=',' ex1.txt   ## on ajoute le délimiter ','
#-----------------------------------------
Riveau Alain,1919-05-22,Directeur des operations
Cavalier Bart,1987-09-09,Directeur des ventes
Stone Emma,1991-01-30,Directrice des communication
Trembblay Guy,1962-02-02,Vendeur
#-----------------------------------------

#=====================================================

cut -c 1,2,3 ex1.txt
#-----------------------------------------
Riv
Cav
Sto
Tre
#-----------------------------------------
```