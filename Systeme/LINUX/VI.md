# EDITEUR VI
_vi_ est un éditeur.
il a les commandes suivantes:

* **i**: permet d'inserer (écrire)
* **esc**: repasse en mode commande
* **:q!**: permet de sortir sans sauvegarder un fichier
* **:wq!**: permet de sortir en sauvegarder les modif du fichier
* **:w**: sauve sans sortir du fichier
* **:!<commandeShell>**: permet de lancer une commande shell dans le fichier
* **dd**: coupe la ligne au niveau du curseur
* **[n]dd**: coupe "n" lignes à partir de celle où se trouve le curseur
* **d0**: coupe les caractères de la ligne avant le curseur
* **d$**: coupe les caractères de la ligne après le curseur
* **yy**: copie la ligne où se trouve le curseur
* **yy**: copie n lignes à partir de celle où se trouve le curseur
* **y0**: copie les caractères de la ligne avant le curseur
* **y$**: copie les caractères de la ligne après le curseur
* **p**: colle ce qui est dans le presse papier
* **:m[n]**: déplace la ligne courante à la ligne [n]
* **u**: undo la dernière action
* **gg**: déplace le curseur sur la 1ere ligne du fichier
* **G**: déplace le curseur sur la dernière ligne du fichier
* **[n]G**: déplace le curseur sur la ligne [n] du fichier
* **zz**: met la ligne courante au milieu de la fenêtre
* **/[terme]**: recherche en avant de [terme] dans le fichier
* **?[terme]**: recherche en arrière de [terme] dans le fichier

On peut personnalisé _vi_ en modifiant son fichier **~/.virc**.
