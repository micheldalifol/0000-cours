# NETTOYAGE FICHIER LINUX  


Il est possible qu'en faisant un "copié/collé" d'une documentation dans 1 fichier linux directement des caractères qu'on ne veut pas s'ajoutent....

exemple: copie de la [doc gitlab](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#set-up-the-initial-backend) pour la partie `terraform init` 

le fichier ressemble à cela : 
```bash
#!/bin/bash

############################################################################################################
## le but de ce script est de faire un terraform init en local pour un repo sur gitlab
### ainsi il sera possible de faire différente action sur les terraform states en local et pouvoir les renvoyer sur gitlab
##########
## ./init.sh
##########
############################################################################################################

#-----------------------------------------------------------------------------------------------------------
## Variables
# PROJECT_ID="<gitlab-project-id>"
# OLD_STATE_NAME="<old-state-name>"
# NEW_STATE_NAME="<new-state-name>"
# TF_USERNAME="<gitlab-username>"
# TF_PASSWORD="<gitlab-personal-access-token>"  ## access token créé sur le repo gitlab
# TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${OLD_STATE_NAME}"
PROJECT_ID="<gitlab-project-id>"
OLD_STATE_NAME="<old-state-name>"
NEW_STATE_NAME="<new-state-name>"
TF_USERNAME="<gitlab-username>"
TF_PASSWORD="<gitlab-personal-access-token>"
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${OLD_STATE_NAME}"
#-----------------------------------------------------------------------------------------------------------

terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5

```

mais avec la commande suivante `cat -A init.sh` on voit les caractères cachés suivants `^M` en fin de chaque ligne
```bash
#!/bin/bash^M$
^M$
############################################################################################################^M$
## le but de ce script est de faire un terraform init en local pour un repo sur gitlab^M$
### ainsi il sera possible de faire diffM-CM-)rente action sur les terraform states en local et pouvoir les renvoyer sur gitlab^M$
##########^M$
## ./init.sh^M$
##########^M$
############################################################################################################^M$
^M$
#-----------------------------------------------------------------------------------------------------------^M$
## Variables^M$
# PROJECT_ID="<gitlab-project-id>"^M$
# OLD_STATE_NAME="<old-state-name>"^M$
# NEW_STATE_NAME="<new-state-name>"^M$
# TF_USERNAME="<gitlab-username>"^M$
# TF_PASSWORD="<gitlab-personal-access-token>"  ## access token crM-CM-)M-CM-) sur le repo gitlab^M$
# TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${OLD_STATE_NAME}"^M$
PROJECT_ID="4018"^M$
OLD_STATE_NAME="cplmgt_alpha_dev_bl_2"^M$
NEW_STATE_NAME="cplmgt_ccoesandbox_dev_alpha"^M$
TF_USERNAME="user"^M$
TF_PASSWORD="Hd7-7VfebwXxAZh7y4sh"^M$
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${OLD_STATE_NAME}"^M$
#-----------------------------------------------------------------------------------------------------------^M$
^M$
terraform init \^M$
  -backend-config=address=${TF_ADDRESS} \^M$
  -backend-config=lock_address=${TF_ADDRESS}/lock \^M$
  -backend-config=unlock_address=${TF_ADDRESS}/lock \^M$
  -backend-config=username=${TF_USERNAME} \^M$
  -backend-config=password=${TF_PASSWORD} \^M$
  -backend-config=lock_method=POST \^M$
  -backend-config=unlock_method=DELETE \^M$
  -backend-config=retry_wait_min=5^M$
```


Il faut donc corriger cela avec la commande suivante `sed -e "s/^M//" init.sh > init2.sh`         
Maintenant le fichier est "propre" on peut lancer l'exécution du script     
