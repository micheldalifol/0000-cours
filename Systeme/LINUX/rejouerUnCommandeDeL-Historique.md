# REJOUER UNE COMMANDE DEPUIS L'HISTORIQUE BASH

Chaque commande de l'historique est classé par numéro    

Pour rejouer une commande il suffit de taper ceci `!<numCommande>`    

_Exemple_:    

```bash
history
#-------------------------------
 1870  git branch
 1871  git checkout review
 1872  git branch
 1873  git status
 1874  git add . && git commit -m "add policy test2 on BL_3"
 1875  git push
 1876  git status
 1877  git add . && git commit -m "add policy test2 on BL_3 fixed"
 1878  git push
 1879  git status
 1880  git add . && git commit -m "add policy test2 on BL_3 update"
 1881  git push
 1882  git status
 1883  git add . && git commit -m "remove policy test1 on BL_3"
 1884  git push
 1885  dnslog
 1886  ll
 1887  git branch
 1888  clear
 1889  tree
#-------------------------------

## rejouer la commande 1872: "git branch"
!1872
#-------------------------------
git branch
  master
* review
#-------------------------------
```