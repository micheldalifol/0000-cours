# CREER UN SERVICE

[lien utile](https://www.linuxtricks.fr/wiki/systemd-creer-des-services-timers-unites)

Un service peut être créé en créant le fichier du service.
Un service est un programme qui est exécuté en tache de fond.
Chez systemd, on ne parle pas de service, mais d'unité.
Les unités du système sont créées dans `/usr/lib/systemd/system`
Les unités créées personnelles seront à placer dans `/etc/systemd/system`


## PRE REQUIS
Il faut disposer des droits admin sur le serveur si lequel nous voulons créer un service.

Dans un premier, faire un sauvegarde sur le serveur du dossier dans lequel se trouve tous les services en cas de problèmes
`sudo cp -r /etc/systemd/system /etc/systemd/system.save$(date +%Y%m%d)`

Des fichiers de configurations liés au service peuevtn aussi se trouver ici: `/lib/systemd/system/`

Les fichiers de log du service sont dans ce répertoire: `/var/log/journal/`

Une fois que le service sera créé, il faudra l'activer afin qu'il puisse être utilisé et lancé à chaque reboot:
`systemctl enable <nom du service>.service`

_Remarque_: Pour modifier un service, il faut avant toute action le désactiver:
`sudo systemctl disable <Nom_du_service>.service`

Il pourra ensuite être lancé de manière classique de la manière suivante:

```bash
systemctl start <nom du service>.service
systemctl stop <nom du service>.service
systemctl status <nom du service>.service
systemctl restart <nom du service>.service
systemctl resload <nom du service>.service
```

* Pour lister les services démarrés: `systemctl list-unit-files --type=service`
* Pour lister les sevrices actifs: `systemctl list-units --type=service`
* Pour lister les services actifs en cours d'exécution: `systemctl list-units --type=service --state=running`
* Pour lister les sevrices lancés au démarrage: `systemd-analyze blame`


Il est aussi possible d'utiliser un service au niveau **user**, dans ce cas les fichiers de configuration sont dans :`~/.config/systemd/user/`
Il faut alors ajouter le paramètre `--user` dans la commande du service

```bash
systemctl --user enable <nom du service>.service
systemctl --user start <nom du service>.service
systemctl --user status <nom du service>.service
```

* Pour créer un service utilisateur : `systemctl --user edit <nom du service>.service --full --force`
* Pour éditer un service utilisateur : `systemctl --user edit <nom du service>.service --full`
* Pour ajouter une surcharge (drop-in) sur un service utilisateur : `systemctl --user edit <nom du service>.service`



## TYPE D'UNITES
* **service** : pour un service/démon
* **socket** : pour une socket réseau (de tous types : UNIX, fichier ...)
* **mount** : pour un système de fichiers (exemple : home.mount), /etc/fstab est aussi utilisé sur le système
* **swap** : comme mount mais pour les partitions d'échanges
* **automount** : comme mount mais pour un système de fichiers monté à la demande
* **device** : pour un périphérique
* **timer** : pour l'activation basée sur une date
* **path** : pour l'activation basée sur des fichiers ou des répertoires ;
* **target** : macro-unité qui permet de grouper plusieurs unités (exemple : multi-user.target pour définir une cible) : Ce sont les niveaux d'exécutions de l'ancien système d'init.



## TYPE DE SERVICES
* **simple**: service par défaut. Il lance un processus principal. Si ce processus offre une fonctionalité à d'autres processus sur le système, ses canaux de communications doivent être installés (before) avant que celui-ci soit démarré.
* **forking**: lance un processus père qui crée un processus fils pour le démarrage. Le processus parent s'arrête un fois le démarrage complet et que tous les canaux de communication soient mis en place. (services unix traditionnels)
* **oneshot**: processus assimilable à _simple_, cependant, systemd attend que ce processus se termine avant de continuer ses traitements.
* **dbus**: similaire à _simple_, mais le processus doit obtenir un nom via D-Bus, ensuite systemd pourra traiter les autes unités.
* **notify**: similaire à _siple_, mais, c'est le processus de service qui avertira systemd (via sd_notfy(3)) qu'il peut traiter les autres unités.



## STRUCTURE DU FICHIER DE SERVICE

Pour les serveurs utilisant systemd
Créer le fichier monservice.service dans `/etc/systemd/system/monservice.service`


Ce fichier est de la forme suivante:

* La section **Unit** permet de décrire le service, de vérifier des prérequis.
* La section **Service** définit le type de service, et ce qu'il va faire
* La section **Install** s'occupe des circonstances et des déclencheurs


```bash
[Unit]
#Description= permet de donner une description du service qui apparaîtra lors de l'utilisation de la commande `systemctl status <nom_du_service>`
#After= indique quel pré-requis est nécessaire pour le fonctionnement du service
#Requires=<service qui doit être rquis pour lancer le service que nous créons>
#After|Before=<si le service que nous créons doit être lancé après (after) ou avant (before) un service>
# Remarque: si "Requires" est défini, on a "after" avec le même service indiqué

Description=mon service nginx
Requires=docker.service
After=docker.service


[Service]
# decrit le fonctionnement du service que nous créons
#User=; Group=; Umask= identifie qui est le propriétaire du processus et donc les attributs des fichiers téléchargés
#ExecStartPre=  avant de lancer la commande start du service pn peut faire une pré commande (pour une autre action)
#SyslogIdentifier= identifier dans les syslog: utile dans une remontée des logs sur elk pour les identifer
#ExecStop= permet d'indiquer une commande a exécuter pour arrêter le service
#RemainAfterExit= la valeur "yes" permet d'indiquer que quand la commande de lancement (ExecStart) est terminée, le service est considéré comme toujours lancé (utile avec oneshot)
#Type= permet de specifier le type de service
#Restart= permet de relancer le service automatiquement en cas de plantage (ex: `Restart=on-failure`)

#commandes obligatoires:
#ExecStart= permet d'indiquer la commande à exécuter au lancement du service. Ce paramètre est obligatoire pour tout les types de service (ex: si on lance un script: chemin du script)


ExecStartPre=/usr/bin/docker-compose -f /cheminComplet/docker-compose.yml down -v
ExecStart=/usr/bin/docker-compose -f /cheminComplet/docker-compose.yml up
ExecStop=/usr/bin/docker-compose -f /cheminComplet/docker-compose.yml down -v
SyslogIdentifier=monnginx

[Install]
#WantedBy= permet de spécifier dans quel Target doit être actif le service
WantedBy=multi-user.target
```

###### exemple: service appDyn-agent-machine

```bash
[Unit]
Description=AppDynamics Machine Agent

[Service]

# The AppDynamics machine agent startup script does not fork a process, so
# this is a simple service.
# Note: If you are changing the User running the machine agent, you must also ensure
# that the desired user has read access to controller-info.xml as well as write access
# to the log file. You can change specific file permissions or, most simply, do a
# chown command to give the desired user ownership of the MACHINE_AGENT_HOME directory.

Type=simple

Environment=MACHINE_AGENT_HOME=/tech/apm/agent_machine/current
Environment=JAVA_HOME=/tech/apm/agent_machine/current/jre

# Specify agent system properties for systemd here by setting or editing JAVA_OPTS, e.g.,
#Environment="JAVA_OPTS=-D<sys-property1>=<value1> -D<sys-property2>=<value2>"

# Modify the next two lines to specify the user to run the machine agent as. Note that
# you will need to ensure that:
# 1. The controller-info.xml in the agent conf directory is readable by this user
# 2. The logs directory is writeable by this user
# 3. The scripts directory is writeable by this user
User=root
Environment=MACHINE_AGENT_USER=root

# The next three lines must point to the same location (i.e. the
# PIDFILE env var and the PIDFile property.)
Environment=PIDDIR=/logs/apm/agent_machine
Environment=PIDFILE=${PIDDIR}/appdynamics-machine-agent.pid
PIDFile=/logs/apm/agent_machine/appdynamics-machine-agent.pid

# Killing the service using systemd causes Java to exit with status 143. This is OK.
SuccessExitStatus=143

# Run ExecStartPre with root-permissions
PermissionsStartOnly=true

# Create the pid dir
ExecStartPre=/usr/bin/install -o $MACHINE_AGENT_USER -d $PIDDIR

# This specifies the command line to use
ExecStart=/bin/sh -c "\"${MACHINE_AGENT_HOME}/bin/machine-agent\" -d -p ${PIDFILE}"

[Install]
# Start the AppDynamics machine agent service during the setup for a
# non-graphical multi-user system.
WantedBy=multi-user.target
```
