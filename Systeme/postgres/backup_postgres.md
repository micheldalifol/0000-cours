# backup postgres


## COMMANDES DE BACKUPS
```bash
#commande de backup:
pg_dump postgres_database_name > dbbackup.sql
```
```sql
-- connaitre le nom de la database:
SELECT current_database();
--		renvoie "zabbix"
```

```bash
#commande de restore:
psql postgres_database_name < dbbackup.sql
```

## SCRIPTS
[script complet à lire](https://wiki.postgresql.org/wiki/Automated_Backup_on_Linux)
[script de backup db_backup.sh](https://simplebackups.com/blog/the-ultimate-postgresql-database-backup-script/) 
	
```bash	
vi db_backup.sh
#-----------------------
#!/bin/bash
chemin=/usr/pgsql-14/bin/
postgres_database_name=zabbix
dbbackup.sql=$(date '+%Y%m%d-%H%M%S')_backup.sql
postgres_user=zabbix
postgres_host=$(hostname -s)
postgres_port=5432

#PGPASSWORD='postgres_password' pg_dump --inserts --column-inserts --username=postgres_user --host=postgres_host --port=postgres_port postgres_database_name > dbbackup.sql
$chemin/pg_dump --username=$postgres_user --host=$postgres_host --port=$postgres_port -w $postgres_database_name > $dbbackup.sql
#-----------------------


## mise en mode exécutable le script
chmod +x db_backup.sh

## modification de la crontab
crontab -e 
	0 2 * * * /cheminScript>/db_backup.sh
	
## backup de la base zabbix	
pg_dump zabbix > 20220603_backup.sql
```