# UPGRADE POSTGRES
Procédure d'upgrade de postgres13 à postgres14.

## Liens utiles
* [video youtube](https://www.youtube.com/watch?v=DXHEk4fohcI)
* [doc postgres](https://docs.postgresql.fr)
* [doc postgres upgrade](https://docs.postgresql.fr/10/pgupgrade.html)
* [doc postgres initdb](https://www.postgresql.org/docs/current/app-initdb.html)
* [doc REINDEX](https://docs.postgresql.fr/9.6/sql-reindex.html)
* [etapes upgrade v13 vers v14](http://www.dbaglobe.com/2021/10/upgrade-postgresql-13-to-postgresql-14.html)


## se connecter sur le serveur postgresql
(.venv) [adm5259sys@svlindus1 /data/adm5259sys]$ssh -i .ssh/auto auto@172.20.189.136
Last login: Thu Jun  2 06:17:03 2022 from 172.20.100.106
[auto@svlz6zabxsql1 ~]$ sudo su -
Last login: Thu Jun  2 06:18:43 UTC 2022 on pts/0

## connaitre la version de centos
```bash
[root@svlz6zabxsql1 ~]# psql -V
psql (PostgreSQL) 13.6
```
```bash
[root@svlz6zabxsql1 ~]# systemctl status postgresql-13.service
● postgresql-13.service - PostgreSQL 13 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-13.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/postgresql-13.service.d
           └─override.conf
   Active: active (running) since Tue 2022-05-10 13:57:43 UTC; 3 weeks 1 days ago
     Docs: https://www.postgresql.org/docs/13/static/
  Process: 1196 ExecStartPre=/usr/pgsql-13/bin/postgresql-13-check-db-dir ${PGDATA} (code=exited, status=0/SUCCESS)
 Main PID: 1214 (postmaster)
   CGroup: /system.slice/postgresql-13.service
           ├─   550 postgres: zabbix zabbix 172.20.189.131(43054) idle
           ├─  1214 /usr/pgsql-13/bin/postmaster -D /data/prod/pgdata/13/data
           ├─  1298 postgres: logger
           ├─  1324 postgres: checkpointer
           ├─  1325 postgres: background writer
           ├─  1328 postgres: walwriter
           ├─  1329 postgres: autovacuum launcher
           ├─  1330 postgres: stats collector
           ├─  1331 postgres: logical replication launcher
           ├─  1449 postgres: zabbix zabbix 172.20.189.131(56675) idle
           ├─  1541 postgres: zabbix zabbix 172.20.189.131(56665) idle
           ├─  1650 postgres: zabbix zabbix 172.20.189.131(56667) idle
           ├─  1651 postgres: zabbix zabbix 172.20.189.131(56677) idle
           ├─  2145 postgres: zabbix zabbix 172.20.189.131(56669) idle
           ├─  2151 postgres: zabbix zabbix 172.20.189.131(56659) idle
           ├─  2152 postgres: zabbix zabbix 172.20.189.131(56661) idle
           ├─  2153 postgres: zabbix zabbix 172.20.189.131(56683) idle
           ├─  2156 postgres: zabbix zabbix 172.20.189.131(56611) idle
           ├─  2157 postgres: zabbix zabbix 172.20.189.131(56613) idle
           ├─  2158 postgres: zabbix zabbix 172.20.189.131(56615) idle
           ├─  2159 postgres: zabbix zabbix 172.20.189.131(56617) idle
           ├─  2160 postgres: zabbix zabbix 172.20.189.131(56619) idle
           ├─  2161 postgres: zabbix zabbix 172.20.189.131(56621) idle
           ├─  2163 postgres: zabbix zabbix 172.20.189.131(56623) idle
           ├─  2192 postgres: zabbix zabbix 172.20.189.131(56643) idle
           ├─  2195 postgres: zabbix zabbix 172.20.189.131(56699) idle
           ├─  2249 postgres: zabbix zabbix 172.20.189.131(56747) idle
           ├─110983 postgres: zabbix zabbix 172.20.189.131(63287) idle
           ├─110984 postgres: zabbix zabbix 172.20.189.131(63289) idle
           ├─110985 postgres: zabbix zabbix 172.20.189.131(63291) idle
           └─110987 postgres: zabbix zabbix 172.20.189.131(63293) idle

May 10 13:57:43 svlz6zabxsql1.agpm.adm systemd[1]: Starting PostgreSQL 13 database server...
May 10 13:57:43 svlz6zabxsql1.agpm.adm postmaster[1214]: 2022-05-10 13:57:43.356 UTC [1214] LOG:  red...ss
May 10 13:57:43 svlz6zabxsql1.agpm.adm postmaster[1214]: 2022-05-10 13:57:43.356 UTC [1214] HINT:  Fu...".
May 10 13:57:43 svlz6zabxsql1.agpm.adm systemd[1]: Started PostgreSQL 13 database server.
Hint: Some lines were ellipsized, use -l to show in full."

#### liste des packages de postgres installés (avec la version)
[root@svlz6zabxsql1 ~]# yum list installed | grep postgres
postgresql13.x86_64              13.6-1PGDG.rhel7               @postgres-13
postgresql13-libs.x86_64         13.6-1PGDG.rhel7               @postgres-13
postgresql13-server.x86_64       13.6-1PGDG.rhel7               @postgres-13
```


> ici nous sommes en version 13

### connaitre les fichiers de configuration de postgres

```bash
[root@svlz6zabxsql1 ~]# ls /var/lib/pgsql/
13
```


> version 13

```bash
[root@svlz6zabxsql1 ~]# ls -la /var/lib/pgsql/13/
total 0
drwx------ 4 postgres postgres 33 Feb  9 08:00 .
drwx------ 3 postgres postgres 58 Feb  9 08:00 ..
drwx------ 2 postgres postgres  6 Feb  9 08:00 backups
drwx------ 2 postgres postgres  6 Feb  9 08:00 data
```


## Liste des tables de la database avant l'upgrade
Cela permettra de vérifier après l'upgrade que les tables sont les mêmes

```bash
## connexion à la database
[auto@svlz6zabxsql1 ~]$ sudo su - postgres

## on se connecte à labase avec le compte zabbix
-bash-4.2$ psql -U zabbix -h svlz6zabxsql1 -p 5432 -w


## liste les tables
SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' ORDER BY table_name;
### affiche la liste des tables
```

> _Remarque_: pour sortir du prompt posgresql taper `\q`

## Faire un snapshot des serveurs pour permettre un retour arrière rapide en cas de problème
Se connecter sur [VSphere](https://srwexvc6.agpm.adm/) et créer un snapshot des machines nécessaires


## Installer postgres 14

### On récupère la version postgres 14

```bash
#### on vérifie que le repo pour postgres14 soit présent
[root@svlz6zabxsql1 ~]# ls -la /etc/yum.repos.d/
total 36
drwxr-x---   2 root root  143 Jan 14 13:04 .
drwxr-xr-x. 83 root root 8192 May 10 13:55 ..
-rw-r-----   1 root root  226 Jan 11 08:20 centos-base.repo
-rw-r-----   1 root root  236 Jan 11 08:20 centos-extras.repo
-rw-r-----   1 root root  239 Jan 11 08:20 centos-updates.repo
-rw-r-----   1 root root  194 Jan 11 08:20 epel.repo
-rw-r-----   1 root root  238 Jan 14 13:04 postgres-13.repo
-rw-r-----   1 root root  216 Jan 11 08:20 zabbix.repo

#### ce n'est pas le cas -> lancer le role centosrepositories avec postgres14
(.venv) [adm5259sys@svlindus1 /data/adm5259sys]$cd mdalifol/mdalifol_private/ansible/02xx-repo-upgrade/0201-ansible-ajout_repo_nexus/
(.venv) [adm5259sys@svlindus1 /data/adm5259sys/mdalifol/mdalifol_private/ansible/02xx-repo-upgrade/0201-ansible-ajout_repo_nexus]$vi group_vars/all

##------------------------------
  - name: postgres-14
    enabled: true
    description: "PostgreSQL 14 repo"
##------------------------------    

(.venv) [adm5259sys@svlindus1 /data/adm5259sys/mdalifol/mdalifol_private/ansible/02xx-repo-upgrade/0201-ansible-ajout_repo_nexus]$ansible -i inventories/inventory svlz6zabxsql1 -m ping

svlz6zabxsql1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
(.venv) [adm5259sys@svlindus1 /data/adm5259sys/mdalifol/mdalifol_private/ansible/02xx-repo-upgrade/0201-ansible-ajout_repo_nexus]$ansible-playbook -i inventories/inventory --limit svlz6zabxsql1 deploy.yaml
...

PLAY RECAP ***********************************************************************************************
svlz6zabxsql1              : ok=7    changed=5    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

#### on se reconnecte sur le serveur postgres et on vérifie que le repo est maintenant ajouté
[root@svlz6zabxsql1 ~]# ls -la /etc/yum.repos.d/
total 44
drwxr-x---   2 root root  195 Jun  2 14:05 .
drwxr-xr-x. 83 root root 8192 Jun  2 14:05 ..
-rw-r-----   1 root root  251 Jun  2 14:05 centos-base.repo
-rw-r-----   1 root root  261 Jun  2 14:05 centos-extras.repo
-rw-r-----   1 root root  264 Jun  2 14:05 centos-updates.repo
-rw-r-----   1 root root  219 Jun  2 14:05 epel.repo
-rw-r-----   1 root root  263 Jun  2 14:05 postgres-13.repo
-rw-r-----   1 root root  263 Jun  2 14:05 postgres-14.repo
-rw-r-----   1 root root  279 Jun  2 14:05 postgres-common.repo
-rw-r-----   1 root root  241 Jun  2 14:05 zabbix.repo


#### on cherche la version de postgresql14
[root@svlz6zabxsql1 ~]# yum search postgresql14
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
centos-base                                                                        | 2.2 kB  00:00:00
centos-extras                                                                      | 1.5 kB  00:00:00
centos-updates                                                                     | 1.5 kB  00:00:00
epel                                                                               | 3.3 kB  00:00:00
postgres-13                                                                        | 2.2 kB  00:00:00
postgres-14                                                                        | 2.2 kB  00:00:00
postgres-common                                                                    | 1.5 kB  00:00:00
zabbix                                                                             | 1.5 kB  00:00:00
(1/3): postgres-14/7/x86_64/group_gz                                               |  244 B  00:00:00
(2/3): postgres-14/7/x86_64/primary                                                |  54 kB  00:00:00
(3/3): postgres-common/7/x86_64/primary                                            |  71 kB  00:00:00
postgres-14                                                                                       285/285
postgres-common                                                                                   374/374
======================================= N/S matched: postgresql14 ========================================
...
postgresql14.x86_64 : PostgreSQL client programs and libraries
postgresql14-libs.x86_64 : The shared libraries required for any PostgreSQL clients
postgresql14-server.x86_64 : The programs needed to create and run a PostgreSQL server
...

  Name and summary matches only, use "search all" for everything.

#### on récupère cette version
[root@svlz6zabxsql1 ~]# yum install -y  postgresql14.x86_64 postgresql14-server.x86_64 postgresql14-libs.x86_64
...
Installed:
  postgresql14.x86_64 0:14.3-1PGDG.rhel7               postgresql14-libs.x86_64 0:14.3-1PGDG.rhel7
  postgresql14-server.x86_64 0:14.3-1PGDG.rhel7

Complete!


[root@svlz6zabxsql1 ~]# yum list installed | grep postgres
postgresql13.x86_64              13.6-1PGDG.rhel7               @postgres-13
postgresql13-libs.x86_64         13.6-1PGDG.rhel7               @postgres-13
postgresql13-server.x86_64       13.6-1PGDG.rhel7               @postgres-13
postgresql14.x86_64              14.3-1PGDG.rhel7               @postgres-14
postgresql14-libs.x86_64         14.3-1PGDG.rhel7               @postgres-14
postgresql14-server.x86_64       14.3-1PGDG.rhel7               @postgres-14

```

## Faire un backup de la database postgres en v13
En plus du snapshot on fait un backup de la database pour pouvoir faire une restauration en cas d'erreur.

```bash

-bash-4.2$ /usr/pgsql-14/bin/pg_dump --username=zabbix --host=svlz6zabxsql1 --port=5432 -w zabbix > 20220603_backup.sql


-bash-4.2$ ls -la
total 1113876
drwx------   4 postgres postgres        206 Jun  3 10:06 .
drwxr-xr-x. 31 root     root           4096 Jan 14 13:01 ..
drwx------   4 postgres postgres         33 Feb  9 08:00 13
drwx------   4 postgres postgres         51 Jun  3 08:47 14
-rw-r--r--   1 postgres postgres 1140577042 Jun  3 11:57 20220603_backup.sql
-rw-------   1 postgres postgres       1495 Jun  3 10:12 .bash_history
-rwx------   1 postgres postgres        266 Jun  3 08:35 .bash_profile
-rw-------   1 postgres postgres        251 Jun  3 10:05 .psql_history

```


## On arrête postgres13

```bash
## postgres13 et en train de tourner
[root@svlz6zabxsql1 ~]# systemctl status postgresql-13.service
● postgresql-13.service - PostgreSQL 13 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-13.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/postgresql-13.service.d
           └─override.conf
   Active: active (running) since Tue 2022-05-10 13:57:43 UTC; 3 weeks 2 days ago
 ...

[root@svlz6zabxsql1 ~]# systemctl stop postgresql-13

[root@svlz6zabxsql1 ~]# systemctl status postgresql-13.service
● postgresql-13.service - PostgreSQL 13 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-13.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/postgresql-13.service.d
           └─override.conf
   Active: inactive (dead) since Fri 2022-06-03 08:30:53 UTC; 11min ago
     Docs: https://www.postgresql.org/docs/13/static/
 Main PID: 1214 (code=exited, status=0/SUCCESS)

May 10 13:57:43 svlz6zabxsql1.agpm.adm systemd[1]: Starting PostgreSQL 13 database server...
May 10 13:57:43 svlz6zabxsql1.agpm.adm postmaster[1214]: 2022-05-10 13:57:43.356 UTC [1214] LOG:  redirecting log output to logging collector process
May 10 13:57:43 svlz6zabxsql1.agpm.adm postmaster[1214]: 2022-05-10 13:57:43.356 UTC [1214] HINT:  Future log output will appear in directory "log".
May 10 13:57:43 svlz6zabxsql1.agpm.adm systemd[1]: Started PostgreSQL 13 database server.
Jun 03 08:30:53 svlz6zabxsql1.agpm.adm systemd[1]: Stopping PostgreSQL 13 database server...
Jun 03 08:30:53 svlz6zabxsql1.agpm.adm systemd[1]: Stopped PostgreSQL 13 database server.
```

> En se connectant à l'[IHM zabbix](https://zabbix.svc.agpm.adm/) on voit une erreur liée à la database: OK


## On initialise la nouvelle database
**/!\\** NE PAS ETRE ROOT, se connecter avec le compte: **prod**


```bash
[prod@svlz6zabxsql1 ~]$ /usr/pgsql-14/bin/initdb -D /data/prod/pgdata/14/data
The files belonging to this database system will be owned by user "prod".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /data/prod/pgdata/14/data ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... UTC
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

initdb: warning: enabling "trust" authentication for local connections
You can change this by editing pg_hba.conf or using the option -A, or
--auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    /usr/pgsql-14/bin/pg_ctl -D /data/prod/pgdata/14/data -l logfile start




[prod@svlz6zabxsql1 ~]$ ls -la pgdata/14/data/
total 60
drwx------ 19 prod prod  4096 Jun  3 13:22 .
drwxrwxr-x  3 prod prod    18 Jun  3 12:53 ..
drwx------  5 prod prod    41 Jun  3 13:22 base
drwx------  2 prod prod  4096 Jun  3 13:22 global
drwx------  2 prod prod     6 Jun  3 13:22 pg_commit_ts
drwx------  2 prod prod     6 Jun  3 13:22 pg_dynshmem
-rw-------  1 prod prod  4789 Jun  3 13:22 pg_hba.conf
-rw-------  1 prod prod  1636 Jun  3 13:22 pg_ident.conf
drwx------  4 prod prod    68 Jun  3 13:22 pg_logical
drwx------  4 prod prod    36 Jun  3 13:22 pg_multixact
drwx------  2 prod prod     6 Jun  3 13:22 pg_notify
drwx------  2 prod prod     6 Jun  3 13:22 pg_replslot
drwx------  2 prod prod     6 Jun  3 13:22 pg_serial
drwx------  2 prod prod     6 Jun  3 13:22 pg_snapshots
drwx------  2 prod prod     6 Jun  3 13:22 pg_stat
drwx------  2 prod prod     6 Jun  3 13:22 pg_stat_tmp
drwx------  2 prod prod    18 Jun  3 13:22 pg_subtrans
drwx------  2 prod prod     6 Jun  3 13:22 pg_tblspc
drwx------  2 prod prod     6 Jun  3 13:22 pg_twophase
-rw-------  1 prod prod     3 Jun  3 13:22 PG_VERSION
drwx------  3 prod prod    60 Jun  3 13:22 pg_wal
drwx------  2 prod prod    18 Jun  3 13:22 pg_xact
-rw-------  1 prod prod    88 Jun  3 13:22 postgresql.auto.conf
-rw-------  1 prod prod 28750 Jun  3 13:22 postgresql.conf

```

Cela permet de générer le répertoire `/var/lib/pgsql/14`
Avant il y a ceci:
```bash
[root@svlz6zabxsql1 ~]# ls -la /var/lib/pgsql/
total 12
drwx------   3 postgres postgres   58 Feb  9 08:00 .
drwxr-xr-x. 31 root     root     4096 Jan 14 13:01 ..
drwx------   4 postgres postgres   33 Feb  9 08:00 13
-rw-------   1 postgres postgres   10 Mar  8 15:11 .bash_history
-rwx------   1 postgres postgres  266 May 10 13:55 .bash_profile
```

Après il y a cela:
```bash
[root@svlz6zabxsql1 ~]# ls -la /var/lib/pgsql/
total 12
drwx------   3 postgres postgres   58 Feb  9 08:00 .
drwxr-xr-x. 31 root     root     4096 Jan 14 13:01 ..
drwx------   4 postgres postgres   33 Feb  9 08:00 13
drwx------   4 postgres postgres    3 Jun  9 15:00 14
-rw-------   1 postgres postgres   10 Mar  8 15:11 .bash_history
-rwx------   1 postgres postgres  266 May 10 13:55 .bash_profile
```


## On lance l'upgrade
Toujours avec le compte **prod**.

### On vérifie que tout est ok avant de lancer l'upgrade de la database

```bash
[prod@svlz6zabxsql1 ~]$ /usr/pgsql-14/bin/pg_upgrade --old-bindir /usr/pgsql-13/bin --new-bindir /usr/pgsql-14/bin --old-datadir /data/prod/pgdata/13/data --new-datadir /data/prod/pgdata/14/data --link --check
Performing Consistency Checks
-----------------------------
Checking cluster versions                                   ok
Checking database user is the install user                  ok
Checking database connection settings                       ok
Checking for prepared transactions                          ok
Checking for system-defined composite types in user tables  ok
Checking for reg* data types in user tables                 ok
Checking for contrib/isn with bigint-passing mismatch       ok
Checking for user-defined encoding conversions              ok
Checking for user-defined postfix operators                 ok
Checking for presence of required libraries                 ok
Checking database user is the install user                  ok
Checking for prepared transactions                          ok
Checking for new cluster tablespace directories             ok

*Clusters are compatible*

```

### On upgrade la database

```bash
[prod@svlz6zabxsql1 ~]$ /usr/pgsql-14/bin/pg_upgrade --old-bindir /usr/pgsql-13/bin --new-bindir /usr/pgsql-14/bin --old-datadir /data/prod/pgdata/13/data --new-datadir /data/prod/pgdata/14/data --link
Performing Consistency Checks
-----------------------------
Checking cluster versions                                   ok
Checking database user is the install user                  ok
Checking database connection settings                       ok
Checking for prepared transactions                          ok
Checking for system-defined composite types in user tables  ok
Checking for reg* data types in user tables                 ok
Checking for contrib/isn with bigint-passing mismatch       ok
Checking for user-defined encoding conversions              ok
Checking for user-defined postfix operators                 ok
Creating dump of global objects                             ok
Creating dump of database schemas
                                                            ok
Checking for presence of required libraries                 ok
Checking database user is the install user                  ok
Checking for prepared transactions                          ok
Checking for new cluster tablespace directories             ok

If pg_upgrade fails after this point, you must re-initdb the
new cluster before continuing.

Performing Upgrade
------------------
Analyzing all rows in the new cluster                       ok
Freezing all rows in the new cluster                        ok
Deleting files from new pg_xact                             ok
Copying old pg_xact to new server                           ok
Setting oldest XID for new cluster                          ok
Setting next transaction ID and epoch for new cluster       ok
Deleting files from new pg_multixact/offsets                ok
Copying old pg_multixact/offsets to new server              ok
Deleting files from new pg_multixact/members                ok
Copying old pg_multixact/members to new server              ok
Setting next multixact ID and offset for new cluster        ok
Resetting WAL archives                                      ok
Setting frozenxid and minmxid counters in new cluster       ok
Restoring global objects in the new cluster                 ok
Restoring database schemas in the new cluster
                                                            ok
Adding ".old" suffix to old global/pg_control               ok

If you want to start the old cluster, you will need to remove
the ".old" suffix from /data/prod/pgdata/13/data/global/pg_control.old.
Because "link" mode was used, the old cluster cannot be safely
started once the new cluster has been started.

Linking user relation files
                                                            ok
Setting next OID for new cluster                            ok
Sync data directory to disk                                 ok
Creating script to delete old cluster                       ok
Checking for extension updates                              ok

Upgrade Complete
----------------
Optimizer statistics are not transferred by pg_upgrade.
Once you start the new server, consider running:
    /usr/pgsql-14/bin/vacuumdb --all --analyze-in-stages

Running this script will delete the old cluster s data files:
    ./delete_old_cluster.sh

```
> /!\ le fichier `/data/prod/pgdata/14/data/postgresql.conf` n'a pas la bonne configuration, prendre la configuration du fichier `/data/prod/pgdata/13/data//data/prod/pgdata/13/data/.conf` et le reporter
