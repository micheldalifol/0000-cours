# upgrade_postgres_actions_a_faire




### on va faire l'upgrade avec la commande pb_upgrade
> l'option `--link` -ou `-k` est très importante parce que si on lance un upgrade de 500 Go dans cette option, cela va copier les 500 Go pour l'attribuer à la "nouvelle" db et on aura 1000Go

### vérification de l'upgrade avant l'exécution (avec le compte postgres)
`/usr/pgsql-14/bin/pg_upgrade --old-bindir /usr/pgsql-13/bin --new-bindir /usr/pgsql-14/bin --old-datadir /data/prod/pgdata/13/data --new-datadir /data/prod/pgdata/14/data --link --check`
> cela prend plus du temps si la db est importante 

### upgrade de la db
`/usr/pgsql-14/bin/pg_upgrade --old-bindir /usr/pgsql-13/bin --new-bindir /usr/pgsql-14/bin --old-datadir /data/prod/pgdata/13/data --new-datadir /data/prod/pgdata/14/data --link`

#### apparait 2 script qui sont proposés
* `analyse_new_cluster.sh`    à faire IMPORTANT
* `delete_old_cluster.sh`     à ne faire que si nous ne voulons pas revenir sur l'ancienne version

#### analyse_new_cluster.sh
* lors de l'upgrade les copies des statistiques des requêtes ne sont pas faites
* cela va ralentir énormément l'utilisation de la db 




### avant de lancer cette analse il faut repasser en root et lancer la db 
```bash
systemctl start postgresql-14
systemctl status postgresql-14
```

### on repasse avec le compte postgres et on lance le script d'analyse
`./analyse_new_cluster.sh`
* cela peut prendre du temps (quelques heures)


### il faut ensuite se logguer à la db et vérifier ce qu'il s'est passé lors de l'upgrade (certainement les index sont différents)
`-bash-4.2$ psql -U zabbix -h svlz6zabxsql1 -p 5432 -w`


### on reindex la database afin de remplacer les index de l'ancienne db
```sql
REINDEX DATABASE;

-- on liste toutes les tables de la db (pour vérifier qu'elles sont les mêmes que celles de la version précédentes)
SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' ORDER BY table_name;
```

### on se connecte à l'IHM (après relance de la stack) et on vérifie que tout est ok

### avec le compte postgres, on supprime maintenant le vieux cluster (version 13)
`./delete_old_cluster.sh`
