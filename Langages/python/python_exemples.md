# EXAMPLES PYTHON




## SCRIPT PYTHON CREANT UN FICHIER A PARTIR D'UN FICHIER LISTE ET D'UN FICHIER TEMPLATE JINJA2

Arborescence du projet

```bash
.
├── fichiers_tests
│   └── list
├── script4.py
├── templates
│   └── providers.jinja2
└── test.txt
```

Le but est d'avoir un script python qui va créer un fichier `11_providers.tf` à partir du template jinja `providers.tf` en remplaçant la variable du template par les valeurs présentent dans le fichier `fichiers_tests/list`

Dans le cas pratique, cette liste sera générée par une commande aws cli pour récupérer la liste des ID des accounts
Le fichier `11_providers.tf` est créé en dynamique pour permettre de faire un switch account sur chacun d'un pour pouvoir ensuite exécuter les tâches à accomplir   

Dans notre exemple voici le contenu des fichiers   

```bash
vi fichiers_tests/list
#----------------------------------------------
toto tata titi    ## liste d'ID sur 1 seule ligne séparé par un espace
#----------------------------------------------

vi templates/providers.jinja2
#----------------------------------------------
provider "aws" {
  alias  = "switchAccount-{{account}}"
  assume_role {
    role_arn     = "arn:aws:iam::{{ account }}:role/roleTransverse"
  }
}
#----------------------------------------------

vi script4.py
#----------------------------------------------
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))       ## important créer un répertoire "templates" au même niveau que le script
template = env.get_template('providers.jinja2')               ## ajouter le nom du fichier templates dans le répertoire templates


def createProviderFile() :                                    ## creation de la fonction qui va faire toutes les action
  with open("./fichiers_tests/list", "r") as file :           ## ouverture du fichier en lecture qui contient les ID des accounts
    maList = file.read()
    test = myList.rstrip()                                    ## pour supprimer les caractères en fin de ligne (y compris les "\n")
    test = test.split(" ")                                  ## splitter cette liste pour chaque account ID
    
    compter = 0                                               ## créer une boucle sur cette liste ⚠️ PEUT ETRE mettre "1" en valeur suivant le résultat lors de l'exécution du script
    nbElementList = len(test)                                 ## connaitre le nb d'ID dans la liste
    
    while compter < nbElementList :                           ## démarrer la boucle sur chaque ID de la liste
      account = test[compter]
      output_from_parsed_template = template.render(account=account)      ## définir dans le template l'ID de l'aacount à utiliser
      
      with open("./11_providers.tf", "a") as file :
        file.write("\n\n"+ output_from_parsed_template)                                 ## crée le fichier 11_providers.tf avec les données du fichier "list" et du template -> OK
    
      compter +=1                                             ## changer d'ID dans la boucle
    
createProviderFile()                                         ## exécuter la fonction
#----------------------------------------------
```

On exécute le script `python script4.py`

➡️ le fichier `11_providers.tf` suivant se crée
```bash


provider "aws" {
  alias  = "switchAccount-toto"
  assume_role {
    role_arn     = "arn:aws:iam::toto:role/roleTransverse"
  }
}


provider "aws" {
  alias  = "switchAccount-tata"
  assume_role {
    role_arn     = "arn:aws:iam::tata:role/roleTransverse"
  }
}


provider "aws" {
  alias  = "switchAccount-titi"
  assume_role {
    role_arn     = "arn:aws:iam::titi:role/roleTransverse"
  }
}
```




## SCRIPT PYTHON CREANT UN FICHIER A PARTIR D'UN FICHIER LISTE ET D'UN FICHIER TEMPLATE JINJA2 AVEC BOUCLE FOR

Arborescence du projet

```bash
.
├── fichiers_tests
│   └── listAccountID
├── script3.py
├── templates
│   └── main.jinja2
```


Le but est de créer un fichier avec du contenu ET de faire une boucle pour 1 partie unique dans ce fichier à partir des valeurs contenues dans 1 autre fichier (listAccountID)     

```bash
vi listAccountId
#----------------------------------------------
 toto tata titi
#----------------------------------------------

vi fichiers_tests/main.jinja2
#----------------------------------------------

module "listAccounts" {
  source                  = "./aws_org_listaccounts"
  count                   = length(var.business_lines_parent_id)
  bl_parent_id_template   = var.business_lines_parent_id[count.index]
}
#-----------------------------------------------------

module "switchRole" {
  source                      = "./aws_switch_role"
  providers = {
    {%- for accountId in account  %}
    aws.{{accountId}}                       = aws.switchAccount-{{accountId-}}      ## remarque: me "-" en fin de cette ligne ET après le 1er "%" en début de boucle permettent de ne pas sauter une ligne entre chaque valeur dans le résultat
    {% endfor %}
  }

  shortEnv                    = substr("${var.aws_env}", 0, 1)
  region                      = var.region_short
  aws_region                  = var.region
  appstackcode                = var.appstackcode
  teamproject                 = var.teamproject
  logs_cloudwatch_retention   = var.logs_cloudwatch_retention
  logs_s3_retention           = var.logs_s3_retention
  bl                          = var.bl
  count                       = length(local.list_accounts_ids)
  accountId                   = local.list_accounts_ids[count.index]
  accountRunPipeline          = data.aws_caller_identity.current.account_id
#  role_transversalRole        = var.role_transversalRole
}
#-----------------------------------------------------
#----------------------------------------------

vi script3.py
#----------------------------------------------
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))     ## important créer un répertoire "templates" au même niveau que le script
template = env.get_template('main.jinja2')                  ## ajouter le nom du fichier template dans le répertoire templates


def createMainFile() :
  with open("./fichiers_tests/listAccountID", "r") as file :
    myList = file.read()
    # print(myList)
    newList = myList.rstrip()                           ## suppression espace en fin de ligne
    newList = newList.lstrip()                          ## suppression espace en début de ligne
    # print(newList)
    newList = newList.split(" ")
    # print(newList)
    # print(type(newList))
    # print(newList[0])
    # print(type(newList[0]))
   


    output_from_parsed_template = template.render(account=newList)      ## on prend la liste pour boucler dessus
    # print(output_from_parsed_template)                                  ### si on prend "account", on va demander de boucler sur la valeur -> ça va boucler sur "t", "o", "t", "o" de toto, idem sur tata et titi en réécrivant dans le fichier le contenu du teemplate
    with open("./10_main.tf", "a") as file :
      file.write(output_from_parsed_template)

  
createMainFile()
#----------------------------------------------
```



On exécute le script `python script3.py`
En résultat on obient ce fichier      





```bash
cat 10_main.tf
#----------------------------------------------

module "listAccounts" {
  source                  = "./aws_org_listaccounts"
  count                   = length(var.business_lines_parent_id)
  bl_parent_id_template   = var.business_lines_parent_id[count.index]
}
#-----------------------------------------------------

module "switchRole" {
  source                      = "./aws_switch_role"
  providers = {
    aws.toto                       = aws.switchAccount-toto         ## les 3 lignes souhaitées
    aws.tata                       = aws.switchAccount-tata
    aws.titi                       = aws.switchAccount-titi
  }

  shortEnv                    = substr("${var.aws_env}", 0, 1)
  region                      = var.region_short
  aws_region                  = var.region
  appstackcode                = var.appstackcode
  teamproject                 = var.teamproject
  logs_cloudwatch_retention   = var.logs_cloudwatch_retention
  logs_s3_retention           = var.logs_s3_retention
  bl                          = var.bl
  count                       = length(local.list_accounts_ids)
  accountId                   = local.list_accounts_ids[count.index]
  accountRunPipeline          = data.aws_caller_identity.current.account_id
#  role_transversalRole        = var.role_transversalRole
}
#-----------------------------------------------------
#----------------------------------------------
```