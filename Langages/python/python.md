# <span style="color: #CE5D6B"> **PYTHON** </span>


_______________________________________________________________________________________________________________________    


<span style="color: #CE5D6B">

##  **PYTHON - INTRODUCTION** 
</span>


[Documentation officielle python](https://www.python.org/doc/)        


Il existe 3 manière d'utiliser python    
- avec un editeur de texte (Atom, vim...)    
- avec un IDE (PyCharm, Spyder)       
- avec un notebook (Jupyter) ➡️ très utilisé en datasciences     



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - INSTALLATION PYTHON, PIP, VENV**  
</span>


```bash
## linux ubuntu 
cd ~ 
sudo apt update && sudo apt upgrade
sudo apt install python3-pip
sudo apt install python3-venv
python3 -m venv .venv
source .venv/bin/activate

## linux redhat 


## macOSx
brew update && brew upgrade 
brew install python python3 pip virtualenv virtualenvwrapper 

mkdir ~/.virtualenvs
vi .zshrc
#-------------------------------------------
# Setting PATH for Python 3 installed by brew
export PATH=/usr/local/share/python:$PATH

# Configuration for virtualenv
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/bin/virtualenvwrapper.sh
#-------------------------------------------
```



_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **PYTHON - LES BASES** 
</span>



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - LES BASES - TYPES DE DONNEES**  
</span>



Les types de données permettent de définir quels types d'actions/operations vont pouvoir être exécutés sur les datas    
![python-data_type](./images/python-data_type.png)        




=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - LES BASES - NOMBRES**  
</span>


Les nombres peuvent être **entiers** (**int**) ou **flottants** (**float**)     
Pour connaitre le type de la valeur OU de la variable il suffit d'écrire `type(<valeur / nomvariable>)` cela retourne le type  

_exemple_    
```python
mavariable = 2

type(mavariable) 
 int  ## retourne le type de la valeur de la variable "mavariable"

type(4)
 int  ## retourne le type de la valeur 4

type(6.3)
 float  ## retourne le type de la valeur 6,3

type(6/3)
 float    ## parce que la valeur la valeur de l'opération "6/3" est "2.0" qui est un float  

type(int(6/3))
 int  ## on force la valeur de l'opération "6/3" a être un entier (soit "2" et pas "2,0")    ## la valeur est tronquée
```





=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - LES BASES - VARIABLES**  
</span>


La déclaration d'une variable se fait de la manière suivante: `nomMaVariable = <valeur ma variable>`        
Une fois la variable déclarée on peut faire des actions avec celle-ci        

On peut effectuer des actions entre plusieurs variables MAIS il faut quelles soient de types équivalent sinon erreur       


On peut faire des affectations multiples de variables     
```python
a, b, c = 2.5, 3.2, "salut!"     ## a=2,5  b=3,2 c="salut!" qui est de type str (string)
```


Il existe des mots réservés chez python (**keywords**) qui ne peuvent pas être utilisés pour nommer une variable   
Depuis python 3.8 il en existe 35:         
![python-keywords_3.8](./images/python-keywords_3.8.png)        





=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - LES BASES - CHAINES DE CARACTERES**  
</span>


_exemple_         
```python
x="salut"
y='toto'
z="il n'a pas plu aujourd'hui"    ## on peut mettre des simples quotes dans les double quotes ou inversement  

print(x+y)
 saluttoto

print(x+" "+y)
 salut toto     ## avec l'espace   

age = 25  
print(y+" a "+age+" ans")
 TypeError: cannot concatenate 'str' and 'int' objects   

## pour cela faire    
print(y+" a "+str(age)+" ans")
 toto a 25 ans    ## on a forcé le type de la variable age à 1 type str  

### on ne peut pas forcer 1 str à être 1 int
### tout semble pouvoir devenir de type str (essai avec une liste ➡️ OK)
```


=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - LES BASES - BOOLEENS**  
</span>


Ils peuvent prendre 2 valeurs: **True** ou **False**    (T et F en majuscule)         
Ils sont souvent utilisé avec les conditions  

_exemple_         
```python
print(5<9)
 True   
```





=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - LES BASES - LISTES TUPLES DICTIONNAIRES**  
</span>


**List**   
  ➡️ `[<valeur1>, <valeur2>, ..., <valeurN>]` les valeurs pouvant être de n'importe quel type  (aussi dans 1 même liste)      
  ➡️ Une liste est 1 ensemble ordonné ➡️ à l'affichage conserve le même ordre       
  ➡️ les valeurs d'1 liste sont modifiables      



**Tuple**   
  ➡️ `(<valeur1>, <valeur2>, ..., <valeurN>)`  les valeurs pouvant être de n'importe quel type           
  ➡️ les valeurs d'un tuple sont immuables (on ne peut pas les changer)    



**Dictionary**            
  ➡️ `{<cle1>:<valeurCle1>, <cle2>:<valeurCle2>, ..., <cleN>:<valeurCleN>}`   c'est un "json"       
  ➡️ collection ordonnée, modifiable




_exemple_        
```python
maListe = [1, 3, 35, 87]
mesMots = ["toto", "tata", "titi"]  
print(mesMots) 
 ["toto", "tata", "titi"]   ## affiche la liste de mots    


monTuple = (1, 3, 35, 87)    
mesMots = ("toto", "tata", "titi")
print(mesMots) 
 ("toto", "tata", "titi")   ## affiche le tuple de mots  


monDic = {"prenom":"toto", "salaire":100000}
print(monDic)
 {"prenom":"toto", "salaire":100000}

monDic.keys() 
 dict_keys(["prenom", "salaire"])

monDic.values()
 dict_values(["toto", 100000])


## changement de type    
keys = list(monDic.keys())
print(keys)
 ["prenom", "salaire"]      ## c'est une liste

---
ceci est un bloc de commentaires
---


```

>_Remarque_: la commande `runfile(<pathFile>', wdir='<pathPython')` exécute le contenu d'un fichier (avec windows)

=======================================================================================================================          

_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **PYTHON - FONCTIONS ET METHODES** 
</span>



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - FONCTIONS ET METHODES - FONCTIONS**  
</span>



Il existe 2 types de fonctions:     
- **fonction intégrées**            
- **fonctions définies**  ➡️ on utilise le mot clé `def` pour ces fonctions    

_exemple de fonctions intégrées_   
![python-fonctions_integree](./images/python-fonctions_integrees.png)         


[Documentation python - fonctions intégrées](https://docs.python.org/3/library/functions.html)       

Pour connaitre les fonctions intégrées (**built-in functions**) on peut écrire la commande `dir(__builtins__)`          
Pour avoir la doc de ces fonctions `print(pow.__doc__)` avec  `pow()` 1 fonction intégrée   


Les fonctions définies sont les fonctions "personnalisées"    
```python
def nomFonction (argument1,argument2):
  param1
```

_exemple_         
```python
## definition de la fonction
def printmessage():
  print("bonjour")              ## une indentation EST OBLIGATOIRE

def carre(x):
  print(x*x)

def direbonjour(nom,prenom):
  print("bonjour Monsieurs: "+nom+" "+prenom)


## appel de la fonction
printMessage() 
  bonjour   ## retour le resultat de la fonction

carre(3)
 9 ## affiche le carré de 3   

direBonjour("Toto", "titi")
  bonjour Monsieur: Toto titi     ## Toto est le nom, titi est le prénom
```



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - FONCTIONS ET METHODES - METHODES**  
</span>


Une methode est une fonction liée à 1 objet    

`<objet>.<methode>(<argument>)`     

_exemple_  
```python
sujet = "système information géographique"
sujet.count("e")
 2                  ## la méthode "count" est lié à l'objet "sujet" et son résultat est de compter le nombre de "e" dans le string "sujet" (sensible à la casse  ➡️ 1 "e" n'est pas 1 "é")
```


Un type d'objet permet d'accéder à sa classe
Il est donc possible d'accéder aux méthodes de la classe    

Dans l'exemple ci-dessus la variable "sujet" est de type str (c'est un string) ➡️ donc "sujet" est aussi un objet de la classe str 

```python
print(type(sujet))
 <class 'str'>          ## ce qui est retourné
```

Cela peut donc aussi se faire pour les classes int, float, list....  

Lorsqu'on souhaite appeler une methode liée à la classe str on peut faire dans l'IDE  (les methodes vont être proposées)   
```python
sujet.capitalize() 
 Système information géographique     ## commence par une lettre capitale
```







=======================================================================================================================          

_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **PYTHON - MANIPULATION - CHAINES ET LISTES** 
</span>




=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - MANIPULATION - STRINGS**  
</span>


Une chaine de caractères (string) est de type **str**, donc de classe **str**     
Des methodes y sont associées     

_exemple_        
```python
var1 = "Python est un Langage de Programmation"   


var1.upper()
 'PYTHON EST UN LANGAGE DE PROGRAMMATION'   ## tout en majuscule          

var1.lower()
 'python est un lmangage de programmation'  ## tout est en minuscule   

var1.title()
 'Python Est Un Langage De Programmation'   ## chaque mot commence par 1 majuscule

var1.islower()
 False                ## le string n'est pas en minuscule dans sa totalité
```


**INDEXATION D'UN STRING**          

  **Le 1er numéro d'index est 0**              
  Chaque caractère a 1 index différent    

```python
x= "Les amis"

print(x[1])
 "e"    ## le "e" de "Les"   

print(x[-2])
 "i"  ## le "i" de "amis"   ➡️ dernier caractère du string, pas besoin de connaitre le nb de caractères dans le string
```




**SLICING D'UN STRING (DECOUPAGE)**          
 On utilise pour cela les ":"    

```python
sujet = "système information géographique"

sujet[8:19]
 'information'      ## n'affiche que les caractères d'indices 8 à 19 - 19 représente l'espace, 8 est inclu, 19 n'est pas inclu

sujet[10:]
 'formation géographique'   ## on ne met pas la limite ➡️ jusqu'à la fin

sujet[:6]
 'systèm'   #" ➡️ le "e" de système a l'indice 6, il n'est pas inclu dans le retour, pour l'avoir il faudrait mettre l'indice 7 

sujet.split(" ")
 ['système', 'information', 'géographique'] ## ➡️ l'espace est le séparateur pour casser le string   le type est une liste  
 ## cette méthode n'écrase pas la valeur de la variable sujet

type(sujet.split(" ")) 
 list

sujet.replace("tème", "bla")
 'sysbla information géographique'  ## la partie "tème" a été remplacée par "bla"
 ## cette méthode n'écrase pas la valeur de la variable sujet
```



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - MANIPULATION - LISTES**  
</span>


Il existe des fonctions python qui permettent d'effectuer des actions sur 1 liste   

_exemples_     
```python
villes = ["Lyon", "Bandol", "Nice"]


print(len(villes))
 3    ## donne le nombre d'éléments dans la liste "villes"

villes.sort(reverse = True)
print(villes)
 ['Nice', 'Lyon', 'Bandol']   ## liste triée dans l'inverse de l'ordre alphabétique
 ## ne fonctionne que si tous les éléments sont de même type dans la liste

villes.sort() 
print(villes)
 ['Bandol', 'Lyon', 'Nice']

print(villes[2])
 'Nice'   ## commence à l'index 0 comme dans les str

print(viles[-1])
 'Nice'

print(villes[1:2])
 ['Lyon']   ## comme pour les str on récupère les éléments à partir de l'index de départ (inclus) jusqu'à l'index de fin (exclu)

del(villes[2])
print(villes) 
  ['Lyon', 'Bandol']    ## l'élément d'indice 2 est supprimé de la liste
```


[Les listes contiennent elles aussi des méthodes spécifiques](https://www.w3schools.com/python/python_ref_list.asp)        

![python-method_list](./images/python-method_list.png)

```python
villes = ["Lyon", "Bandol", "Nice", "Aix"]
villes2 = ["Castellet", "Beausset"]

villes.append("Paris")
print(villes)
  ["Lyon", "Bandol", "Nice", "Aix", "Paris"]    ## ajoute en fin de liste

villes.insert(2, "Marseille")
print(villes)
  ["Lyon", "Bandol", "Marseille" , "Nice", "Aix"]   ## ajoute l'élément pour qu'il ait l'index "2"

villes.extend(villes2)
print(villes)
 ["Lyon", "Bandol", "Nice", "Aix", "Castellet", "Beausset"]

villes.index('Aix')
 3
```


=======================================================================================================================          

_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

##  **PYTHON - OPERATEURS DE COMPARAISON** 
</span>


![python-operateur_comparaison](./images/python-operateur_comparaison.png)

Ces opérateurs sont essentiellement utilisés dans les conditions


```python
x = 5 

x == 7
 False    ## x n'est pas égal à 7

x != 7 
 True
```

_______________________________________________________________________________________________________________________      


<span style="color: #CE5D6B">

##  **PYTHON - CONDITIONS ET BOUCLES** 
</span>



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - CONDITIONS ET BOUCLES - CONDITIONS**  
</span>


Dans les conditions on utilise les valeurs booléennes  

Python fourni les conditions suivantes

```python
#######################################################
## syntaxe
if condition :
  bloc instructions
elif condition :
  bloc instructions
else :
  bloc instructions
#######################################################

#######################################################
## exemples
#-------------------------------------------
x = 10
if(x > 15):
  print(x, " est plus grand que 15")

elif(x == 15):
  print(x, " égal 15")

else:
  print(x, " est plus petit que 15")
#-------------------------------------------
## résultat
 '10 est plus petit que 15'
#######################################################

#######################################################
#-------------------------------------------
nom = "toto"
age = 23
if nom == "Toto" and age == 23 :
  print("Votre nom est Toto, et vous avez 23 ans")

if nom == "Toto" or nom == "To*" :
  print("Votre nom est Toto ou commence par To")
#-------------------------------------------

#######################################################

#######################################################
#-------------------------------------------
nom = "toto"

if nom in ["toto", "To"] :
  print("Votre nom est Toto ou To")
#-------------------------------------------

#######################################################

#######################################################
#-------------------------------------------
x = 10 
maList = ["Sara", "Marie", "Sam"]

if(x>15):
  if(nom in maList):
    print("Bonjour ",nom)
  else:
    print("Votre nom de figure pas dans la liste")
else:
  print("La valeur ",x," est plus petite que 15")
#-------------------------------------------

#######################################################
```




=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - CONDITIONS ET BOUCLES - BOUCLES FOR**  
</span>



Il existe 2 types de boucles:     
 - boucle bornée      ➡️ on connait le nombre d'itérations de la boucle   ➡️ `for`               
 - boucle non bornée  ➡️ on ne connait pas le nb d'itérations             ➡️ `while`            


Syntaxe boucle for
`for indice in sequence`


_exemple_           
```python
#################################################
for i in range(4):                          ## range(4) == [0, 1, 2, 3]
  print("Je suis le numéro :", i)

## résultat
 Je suis le numéro : 0
 Je suis le numéro : 1
 Je suis le numéro : 2
 Je suis le numéro : 3
#################################################

#################################################
for x in range(3, 6):               ## 3 indice de début; 6 indice de fin (exclu)
  print(x)

##resultat
3
4
5
#################################################

#################################################
for x in range(3, 8, 2):    ## 3 indice de début; 8 indice de fin (exclu); 2 étape (tous les 2 indices)
  print(x)

##resultat
3
5
7
#################################################

#################################################
## boucle for dans 1 liste 
maListe = ["Toto", "tata", "titi"]

for item in maList :
  print(item)

##resultat
"Toto"
"tata"
"titi"
#################################################

#################################################
maList = ["Toto", "tata", "titi", "popo", "pipi", "papa"]

for item in range(len(maList)):
  print(item)

##resultat
0
1
2
3
4
5
#################################################
```





=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - CONDITIONS ET BOUCLES - BOUCLES WHILE**  
</span>


La boucle `while` est une boucle qui itère tant qu'on est dans la condition de départ        

Syntaxe:       
```python
while condition:
  instructions
```

_exemple_     
```python
#################################################
compter = 0

while compter < 5 :   ## tant que la valeur de "compter" n'a pas atteint la valeur "5" la condition est à "True", donc on exécute la boucle
  print(compter)
  compter +=1       ## compter = compter + 1

##resultat
0
1
2
3
4
#################################################

#################################################
maList = ["Toto", "tata", "titi", "popo", "pipi", "papa"]


compter = 0
nbElementListe = len(maList)
print(nbElementListe)

while compter < nbElementListe :  
  account = maList[compter]
  print(account)
  compter +=1

##resultat
6               ## nb d'éléments dans la liste
Toto
tata
titi
popo
pipi
papa
#################################################
```




=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - CONDITIONS ET BOUCLES - CONTINUE BREAK**  
</span>


`continue` et `break` sont ce qu'on appelle des controleurs de flux    

```python
#################################################
val = range(1,6)

for in in val : 
  if i == 3 :
    continue
  print("bonjour", i)     ## lorsque i == 3 on continu la boucle SANS exécuter l'instruction (qui est print)

##resultat
bonjour 1
bonjour 2
bonjour 4
bonjour 5
#################################################

#################################################
val = range(1,6)

for in in val : 
  if i == 3 :
    break
  print("bonjour", i)     ## lorsque i == 3 on stoppe la boucle: on sort de la boucle

print("sorti de la boucle")

##resultat
bonjour 1
bonjour 2
sorti de la boucle
#################################################
```



=======================================================================================================================          

_______________________________________________________________________________________________________________________       


<span style="color: #CE5D6B">

##  **PYTHON - SAISIE ET ERREURS** 
</span>




=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - SAISIE**  
</span>


La saisie est l'utilisation de la fonction `input()` qui permet d'ajouter une "saisie" par le clavier de l'utilisateur    

`a = input("Saisir votre valeur :")`


**La fonction `input()` retourne comme type un string (str)**


```python
val1 = input("Entrez la 1ere valeur : ")
val2 = input("Entrez la 2e valeur : ")

print(val1+val2)   
print("--------------")
print(int(val1)+int(val2))

## resultat
Entrez la 1ere valeur : 5   ## 5 est ajouté au clavier par le user
Entrez la 2e valeur : 7     ## 7 est ajouté au clavier par le user
57
--------------
12      ## on a convertit le type des variables en integer
```



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - ERREURS**  
</span>

Pour aider dans les erreurs qui s'affiche on utilise le "gestionnaire des erreurs"   
Cela consiste à mettre en place les blocs `try` et `except`

```python
#---------------------------------------------------
val1 = input("Entrez la 1ere valeur : ")

val1 = int(val1)        ## on transforme au format int
print(val1+1)           ## on ajoute 1 à val1

##resultat
Entrez la 1ere valeur : 6 ## 6 ajouté par le user
7       ## 6+1 = 7

Entrez la 1ere valeur : bonjour
ValueError: invalid literal for int() with base 10: 'bonjour'   ## bonjour ne peut pas être un str     
#---------------------------------------------------

#---------------------------------------------------
### modif code  
val1 = input("Entrez la 1ere valeur : ")

try:
  val1 = int(val1)
  print(val1+1)
except ValueError:
  print("vous devez renseigner un nombre entier")

##resultat
Entrez la 1ere valeur : 6 ## 6 ajouté par le user
7       ## 6+1 = 7

Entrez la 1ere valeur : bonjour
'vous devez renseigner un nombre entier'
#---------------------------------------------------

#---------------------------------------------------
### modif code  
val1 = input("Entrez la 1ere valeur : ")

try:
  val1 = int(val1)
  print(val1+1)
except :                          ## on laisse pour toutes erreurs possibles
  print("vous devez renseigner un nombre entier")

##resultat
Entrez la 1ere valeur : 6 ## 6 ajouté par le user
7       ## 6+1 = 7

Entrez la 1ere valeur : bonjour
'vous devez renseigner un nombre entier'
#---------------------------------------------------
```



=======================================================================================================================          

_______________________________________________________________________________________________________________________         


<span style="color: #CE5D6B">

##  **PYTHON - GESTION DES FICHIERS** 
</span>





=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - FICHIERS - LECTURE**  
</span>


Il n'est pas nécessaire d'importer une bibliothèque pour lire ou écrire dans les fichiers      
Python fournit par defaut des fonctions intégrées pour cela       
[Documentation python - file method](https://www.w3schools.com/python/python_ref_file.asp)            
![python-method_file](./images/python-method_file.png)                        


_exemple_       
```bash
vi data.txt
#--------------------------------
Une ligne d 1 fichier
Une autre ligne de fichier
la derniere ligne de texte
#--------------------------------


file = open("data.txt", "r")    ## fonction qui permet d'ouvrir en lecture le fichier data.txt
## "r" permet la lecture    
## "w" permet l'écriture   
## "a" permet l'append (ajout en fin de fichier)

lire = file.read()              ## on va lire le fichier data.txt  

print(lire)                    ## on va afficher le contenu du fichier data.txt

print("type de 'lire' : ", type(lire))    ## affiche le type de 'lire'  "class str" dans ce cas

file.close()                    ## ferme le fichier data.txt  !! IMPORTANT de fermer le fichier !!
####################################################################################################

## autre methode de lecture d'un fichier
file = open("data.txt", "r")

lire = file.readlines()

print(lire)  
  ['Une ligne d 1 fichier\n', 'Une autre ligne de fichier\n', 'la derniere ligne de texte\n']

print("type de 'lire' : ", type(lire)) 
  type de 'lire' : <class 'list'>

file.close()
```


>_Remarque_: dans le fonction `open()`, on a 2 arguments
> ➡️ 1er argument: le fichier avec son chemin ➡️ si nous sommes sur du windows, nous avons dans le chemin des "\", il faut **rajouter la lettre "r" avant le debut du chemin** pour indiquer que les "\" doivent se lire comme des "/"  
> ➡️ `open(r"C:\Users\Desktop\data.txt", "a")`   



```python
## équivalent    

with open(r"C:\Users\Desktop\data.txt", "r") as file : 
  print(file.read())

## affiche le contenu du fichier et le ferme automatiquement
```





=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - FICHIERS - ECRITURE**  
</span>


```bash
vi data.txt
#--------------------------------
Une ligne d 1 fichier
Une autre ligne de fichier
la derniere ligne de texte
#--------------------------------

with open(r"C:\Users\Desktop\data.txt", "w") as file :      ## dans la fonction open() en 2e argument on a mis "w" 
  file.write("bonjour")

cat data.txt
#--------------------------------
bonjour
#--------------------------------
```



=======================================================================================================================          

<span style="color: #EA9811">

###  **PYTHON - FICHIERS - AJOUT**  
</span>


Rajout d'éléments dans 1 fichier    

L'ajout "basique" se fait à la suite du dernier élément (PAS à la ligne)     

```bash
######################################################################
## ajout "classique" :  PAS à la ligne
vi data.txt
#--------------------------------
bonjour
#--------------------------------

with open(r"C:\Users\Desktop\data.txt", "a") as file :       
  file.write("salut")

cat data.txt
#--------------------------------
bonjoursalut
#--------------------------------
######################################################################

######################################################################
## ajout à la ligne: rajouter "\n" avant la valeur à ajouter
vi data.txt
#--------------------------------
bonjour
#--------------------------------

with open(r"C:\Users\Desktop\data.txt", "a") as file :       
  file.write("\nsalut")                                   ## ajout du "\n" pour le retour à la ligne

cat data.txt
#--------------------------------
bonjour
salut
#--------------------------------
######################################################################

######################################################################
## 
vi data.txt
#--------------------------------
bonjour
#--------------------------------

with open(r"C:\Users\Desktop\data.txt", "a") as file :       
  file.write("\nsalut\ncoucou")

cat data.txt
#--------------------------------
bonjour
salut
coucou
#--------------------------------
######################################################################

######################################################################
## 
vi data.txt
#--------------------------------
bonjour
#--------------------------------

maList = ["toto", "titi", "tata"]     ## ajout d'une liste

with open(r"C:\Users\Desktop\data.txt", "a") as file :       
  file.writlines(maList)

cat data.txt
#--------------------------------
bonjourtototititata                        ## tout apparait sur 1 seule ligne
#--------------------------------
######################################################################

######################################################################
## 
vi data.txt
#--------------------------------
bonjour
#--------------------------------

maList = ["\ntoto", "\ntiti", "\ntata"]     ## ajout d'une liste

with open(r"C:\Users\Desktop\data.txt", "a") as file :       
  file.writlines(maList)

cat data.txt
#--------------------------------
bonjour
toto
titi
tata                        
#--------------------------------
######################################################################
```


=======================================================================================================================          

_______________________________________________________________________________________________________________________          


<span style="color: #CE5D6B">

##  **PYTHON - CLASSES ET OBJETS** 
</span>


Cette partie est de la programmation orientée objet

Création d'une classe    

```python
class Humain:
  nom = "Sam"
  age = 15

  def bonjour(self):                 ## si on ne met pas le self on va avoir 1 erreur à l'appel de cette méthode "TypeError"
    print("bonjour cher ami")


maVar = Humain()

## on peut ensuite accéder aux attributs et aux methodes de la classe   
print(maVar.age)
 15  
print(maVar.bonjour())
 bonjour
```

Création de plusieurs objets à partir d'une classe


```python
class Humain:
  ## constructeur
  def __init__(self,nom,age):       ## cette méthode doit apparaitre en 1er dans la classe
    self.nom = nom
    self.age = age

  def bonjour(self):
    print("bonjour")

## creation des objets à partir de la classe
h1 = Humain("toto", 18)
h2 = Humain("tata", 28)
h3 = Humain("titi", 8)

## affichage des objets
print(h1.nom,h1.age)
print(h2.nom,h2.age)
print(h3.nom,h3.age)

h1.bonjour()                        ## appel de la methode bonjour

 toto 18          ## ce qui s'affiche
 tata 28
 titi 8
 bonjour
```



_______________________________________________________________________________________________________________________          


<span style="color: #CE5D6B">

##  **PYTHON - EXECUTION D'UN SCRIPT** 
</span>



Comment appeler un script python pour son exécution ?

```bash
cd /cheminProjetContenantScriptPython 
ls    ## affiche script.py 

python script.py    ## exécute le script
```




_______________________________________________________________________________________________________________________  

# FIN