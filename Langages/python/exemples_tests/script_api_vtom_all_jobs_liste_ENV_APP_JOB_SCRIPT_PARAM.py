#!/data/.venv/bin/python



############ -------- #!/usr/bin/python3.6

# 0 - echo -n "user:password" | base64 -w 0 ;
# 1 - liste de tous les jobs (interface web): on recupere ENVIRONNEMENT, APPLICATION et job_id (credentials encodes en base64 => echo user:password | base64)
#         curl -s -H 'Authorization: Basic xxxxxxxxxxxxxxxxxxxx' http://<IP_VTOM>:30080/api/job/list
# 2 - parametres de chaque job (interface web): on recupere NAME, SCRIPT (et PARAMETRES s'il y en a)
#         curl -s -H 'Authorization: Basic xxxxxxxxxxxxxxxxxx' http://<IP_VTOM>:30080/api/job/getById?id=JOBac1463b300004ae15dcd17c700019ece
# 3 - recuperation caracteristiques application (swagger-api): mode execution (token parametre dans l'API VTOM)
#         curl -s -k -X GET "https://<IP_VTOM>:30002/vtom/public/domain/1.0/environments/>NOM_ENVIRONNEMENT>/applications/<NOM_APPLICATION>" -H "accept: application/json" -H "X-API-KEY: <TOKEN_API_VTOM>"
# 4 - recuperation caracteristiques "job" (swagger-api): agent + mode execution (token parametre dans l'API VTOM)
#         curl -s -k -X GET "https://<IP_VTOM>:30002/vtom/public/domain/1.0/environments/>NOM_ENVIRONNEMENT>/applications/<NOM_APPLICATION>/jobs/<NOM_JOB>" -H "accept: application/json" -H "X-API-KEY: <TOKEN_API_VTOM>"


# import librairies necessaires
import json
import requests
import re
import os
import urllib3
urllib3.disable_warnings()
from datetime import datetime

print('Debut du traitement :', str(datetime.now()))

# variables
# MODE_DEBUG : 0 = silencieux / 1 = verbeux
MODE_DEBUG = 1
headers = {'Authorization': 'Basic Q0FJTExFUjpOZ2tiOGVzMTMwMDQ9MQ==',}
FIC_OUT='/data/private/VTOM/liste_ENV_APP_JOB_SCRIPT_PARAM.csv'

# curl liste de tous les jobs et affectation a une variable/objet
response = requests.get('http://172.20.99.179:30080/api/job/list', headers=headers)

# Check the response
if response:
    if MODE_DEBUG == 1:
      # Print the response status code
      print('\nThe status code of the response is %d' %response.status_code)

      # Print the JSON content
      #print('The JSON content is: \n%s' %response.json())

      # Print the success message
      #print('\nThe request is handled successfully.')
    
    # suppression fichier sortie si existe
    filePath = FIC_OUT
    if os.path.exists(filePath):
        os.remove(filePath)
    else:
        print("Can not delete the file as it doesn't exists")

    # creation fichier sortie et ecriture noms colonnes
    with open (FIC_OUT, 'a') as f:
      print('ENVIRONNEMENT;AGENT;APPLICATION;APPLI_MODE_EXECUTION;JOB;JOB_MODE_EXECUTION;SCRIPT;NB_PARAMETRES;PARAMETRES', file=f)

    # Write response into "liste_all_jobs_vtom.json" file
    with open('liste_all_jobs_vtom.json', 'w') as json_output_file:
      json.dump(response.json(), json_output_file)

    # on boucle sur toutes les lignes du fichier liste_all_jobs_vtom.json
    fichier_json = open('liste_all_jobs_vtom.json', 'r')
    # fichier_json = open('fic_test.json', 'r')
    with fichier_json as fichier:
      
      # decode un fichier json et le transforme en dictionnaire python
      data = json.load(fichier)

      # print (data)
      for row in data['result']['rows']:
        
        # print(row['environmentName'])
        ligne = ""
        ENVIRONNEMENT = ""
        APPLICATION = ""
        APPLI_MODE_EXECUTION = ""
        SUBMISSION_UNIT = ""
        AGENT = ""

        ligne=(row['environmentName'],row['applicationName'],row['id'])
        
        ENVIRONNEMENT = (row['environmentName'])
        APPLICATION = (row['applicationName'])
        APPLI_MODE_EXECUTION = (row['execMode'])
        # si on veut limiter a un ENV + APPLI :
        #   - decommenter une ligne ci-dessous
        #   - augmenter le retrait (A DROITE de l'actuel) de la ligne 95 (if MODE_DEBUG == 1:) à la ligne 262 (print('Invalid responseJob.'))
        # if (ENVIRONNEMENT == 'BANCAIRE_EDI' and APPLICATION == 'ST_SOCIET_IARD') or (ENVIRONNEMENT == 'REF_TOTO_EX' and APPLICATION == 'ENC_FLUX_TIP_J'):
        # if (ENVIRONNEMENT == 'REF_TOTO_EX' and APPLICATION == 'TERME60FL_M_1'):
        # if (ENVIRONNEMENT == 'ENV_MaC' and APPLICATION == 'ENC_FLUX_TIP_J'):
        if MODE_DEBUG == 1:
          print(ligne)
          print('ENVIRONNEMENT           = ', ENVIRONNEMENT)
          print('APPLICATION             = ', APPLICATION)
          print('APPLI_MODE_EXECUTION    = ', APPLI_MODE_EXECUTION) 
        ######################################################################
        # APPLICATION
        ######################################################################
        # recherche mode execution de cette application
        headersResponseApiAppli = {
                # 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
                # 'accept': 'application/json',
                # 'X-API-KEY': 'Kv7iNe78I420M7ef',
                'X-API-KEY': 'Ic14U0yvHs9qF0hv',
                # 'X-API-KEY': 'S3Y3aU5lNzhJNDIwTTdlZg==',
                # 'Connection': 'close',
                }
        requeteResponseApiAppli = ('https://172.20.99.179:30002/vtom/public/domain/1.0/environments/')
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str(ENVIRONNEMENT)
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str('/applications/')
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str(APPLICATION)
        if MODE_DEBUG == 1:
          print('requeteResponseApiAppli =', requeteResponseApiAppli) 
            
        responseResponseApiAppli = requests.get(requeteResponseApiAppli, headers=headersResponseApiAppli, verify=False)
        if responseResponseApiAppli:
            if MODE_DEBUG == 1:
             # Print the response status code                
              print('\nThe status code of the requeteResponseApiAppli is %d' %responseResponseApiAppli.status_code)
            # load json reponse dans une variable
            dataResponseApiAppli = responseResponseApiAppli.json()
            if MODE_DEBUG == 1:
              print('affichage du contenu du fichier json resultant')
              print(dataResponseApiAppli)
            # definition des variables
            APPLI_MODE_EXECUTION=(dataResponseApiAppli['execMode'])
            if 'submitUnit' not in dataResponseApiAppli:
                AGENT = 'null'
            else:
                SUBMISSION_UNIT=(dataResponseApiAppli['submitUnit'])
                AGENT = str(SUBMISSION_UNIT)          
            if MODE_DEBUG == 1:
              print('APPLI_MODE_EXECUTION    = ', APPLI_MODE_EXECUTION)
              print('AGENT                   = ', AGENT)
        else:    
            # Print the error message for the invalid response
            print('Invalid response responseResponseApiAppli.')
        #######################################################################
        # JOB
        #######################################################################
        # params = (('id', 'JOBac1463b300004ae15dcd17c700019ece'),)
        params = (('id', row['id']),)
        responseJob = requests.get('http://172.20.99.179:30080/api/job/getById', headers=headers, params=params)
        # print(responseJob.json())
        if responseJob:
            JOB_NAME = ""
            JOB_SCRIPT = ""
            if MODE_DEBUG == 1:
              # Print the response status code
              print('\nThe status code of the responseJob is %d' %responseJob.status_code)
            # load json reponse dans une variable
            dataJob = responseJob.json()
            if MODE_DEBUG == 1:
              # affichage du contenu du fichier json resultant
              print(dataJob)
              # affichage du nom du job
              print(dataJob['result']['name'])
              # affichage du nom du script
              print(dataJob['result']['Script'])
            # definition des variables
            JOB_NAME=(dataJob['result']['name'])
            JOB_SCRIPT=(dataJob['result']['Script'])
            # JOB_MODE_EXECUTION=(dataJob['result']['execMode'])
            if MODE_DEBUG == 1:
              print('JOB_NAME                = ', JOB_NAME)
              print('JOB_SCRIPT              = ', JOB_SCRIPT)
            #############################################################################
            # caracteristiques jobs :  "unite de soumission" et "mode execution" 
            ############################################################################
            # recherche unite soumission de ce job
            headersResponseApiJob = {
                    # 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
                    # 'accept': 'application/json',
                    # 'X-API-KEY': 'Kv7iNe720M7ef',
                    'X-API-KEY': 'Ic14U0s9qF0hv',
                    # 'X-API-KEY': 'S3Y3aU5hJNDIwTTdlZg==',
                    # 'Connection': 'close',
                    }
            requeteResponseApiJob = ('https://172.20.99.179:30002/vtom/public/domain/1.0/environments/')
            requeteResponseApiJob = str(requeteResponseApiJob) + str(ENVIRONNEMENT)
            requeteResponseApiJob = str(requeteResponseApiJob) + str('/applications/')
            requeteResponseApiJob = str(requeteResponseApiJob) + str(APPLICATION)
            requeteResponseApiJob = str(requeteResponseApiJob) + str('/jobs/')
            requeteResponseApiJob = str(requeteResponseApiJob) + str(JOB_NAME)
            if MODE_DEBUG == 1:
              print(requeteResponseApiJob) 
            
            NB_PARAMETRES = ""
            JOB_PARAMETRES = ""  
            SUBMISSION_UNIT = "" 
            JOB_PARAMETRES = ""
            responseResponseApiJob = requests.get(requeteResponseApiJob, headers=headersResponseApiJob, verify=False)
            if responseResponseApiJob:
              # if MODE_DEBUG == 1:                
                # Print the response status code
                # print('\nThe status code of the requeteResponseApiJob is %d' %requeteResponseApiJob.status_code)
              # load json reponse dans une variable
              dataResponseApiJob = responseResponseApiJob.json()
              if MODE_DEBUG == 1:
                # affichage du contenu du fichier json resultant
                print(dataResponseApiJob)
              # definition des variables
              JOB_MODE_EXECUTION=(dataResponseApiJob['execMode'])
              if MODE_DEBUG == 1:
                print('JOB_MODE_EXECUTION = ', JOB_MODE_EXECUTION)
              if AGENT == 'null' or AGENT == "":
                if 'submitUnit' not in dataResponseApiJob:
                  AGENT = 'null'
                else:
                  SUBMISSION_UNIT=(dataResponseApiJob['submitUnit'])
                  AGENT = str(SUBMISSION_UNIT)
              if MODE_DEBUG == 1:    
                print('SUBMISSION_UNIT = ', SUBMISSION_UNIT)
                print('AGENT = ', AGENT)  
              if AGENT == 'null':
                if MODE_DEBUG == 1:
                  print('agent is nullllllll')   
                # on va chercher l'unite de soumission parametree par defaut sur l'environnement
                requeteResponseEnvByDefault = ('https://172.20.99.179:30002/vtom/public/domain/1.0/environments/')
                requeteResponseEnvByDefault = str(requeteResponseEnvByDefault) + str(ENVIRONNEMENT)
                # curl -X GET "https://localhost:30002/vtom/public/domain/1.0/environments/
                #ENV_MaC?api_key=J4g9A5xdSsqb" -H  "accept: application/json" -H  "X-API-KEY: J4g9T6sqb"          
                requeteResponseEnvByDefault = requests.get(requeteResponseEnvByDefault, headers=headersResponseApiJob, verify=False)
                if requeteResponseEnvByDefault:
                  dataResponseEnvByDefault = requeteResponseEnvByDefault.json()
                  SUBMISSION_UNIT_BY_DEFAULT=(dataResponseEnvByDefault['submitUnit'])
                  AGENT = str(SUBMISSION_UNIT_BY_DEFAULT)
                  if MODE_DEBUG == 1: 
                    print(dataResponseEnvByDefault)
                    print('SUBMISSION_UNIT_BY_DEFAULT = ', SUBMISSION_UNIT_BY_DEFAULT)
                    print('AGENT = ', AGENT)                                
            else:    
                # Print the error message for the invalid response
                print('Invalid response responseResponseApiJob.')
            # tous les jobs n'ont pas de parametres 
            if "Parameters" in (dataJob['result']):
              if MODE_DEBUG == 1:                
                print("la cle Parameters est presente")
              # compte le nb de parametres
              NB_PARAMETRES=len(dataJob['result']['Parameters'])
              # print(dataJob['result']['Parameters'])
              JOB_PARAMETRES=(dataJob['result']['Parameters'])
              # le champ contient les valeurs a supprimer : '[]
              # ex : ['EX-TEK', 'WebAgpm', 'METIER', 'Jboss', 'Reboot', '***']
              JOB_PARAMETRES=re.sub(r"[\['\]]","",str(JOB_PARAMETRES))
              # resultat : EX-TEK,WebAgpm,METIER,Jboss,Reboot,***
            else:
              JOB_PARAMETRES=""
              NB_PARAMETRES=""
            # realisation de la ligne csv
            # init list
            ligne_csv=[]
            # ajout de chaque element
            ligne_csv.append(ENVIRONNEMENT)
            ligne_csv.append(AGENT)
            ligne_csv.append(APPLICATION)
            ligne_csv.append(APPLI_MODE_EXECUTION)
            ligne_csv.append(JOB_NAME)
            ligne_csv.append(JOB_MODE_EXECUTION)
            ligne_csv.append(JOB_SCRIPT)
            ligne_csv.append(str(NB_PARAMETRES))
            ligne_csv.append(JOB_PARAMETRES)
            # ajout du separateur entre chaque element
            ligne_csv_finale=';'.join(ligne_csv)
            if MODE_DEBUG == 1:            
              print(ligne_csv_finale)
            # ecriture de chaque ligne dans le fichier de sortie
            with open (FIC_OUT, 'a') as f:
                print(ligne_csv_finale, file=f) 
        else:
            print('Invalid responseJob.')
else:
    # Print the error message for the invalid response
    print('Invalid response.')

print('Fin   du traitement :', str(datetime.now()))

exit()

## Python3 code to demonstrate
## removal of bad_chars
## using replace()
#
## initializing bad_chars_list
#bad_chars = [';', ':', '!', "*"]
#
## initializing test string
#test_string = "Ge;ek * s:fo ! r;Ge * e*k:s !"
#
## printing original string
#print ("Original String : " + test_string)
#
## using replace() to
## remove bad_chars
#for i in bad_chars :
#	test_string = test_string.replace(i, '')
#
## printing resultant string
#print ("Resultant list is : " + str(test_string))


