#---------------------------------------------------------

import os
import json
import sys
import subprocess     ## get stout from another script
#---------------------------------------------------------

#---------------------------------------------------------
## MODE DEBUG 
modeDebug = 1       ## mode debug actif complet
# modeDebug = 0     ## mode debug inactif
#---------------------------------------------------------

#---------------------------------------------------------
## récupération du retour des autres scripts pour condition su rl'exécution du script manage_exceptions.py
### https://www.python-simple.com/python-modules-autres/lancement-process.php





#---------------------------------------------------------
# ACTIONS  

## 1. Vérification de l'existence des fichiers OBLIGATOIRE
if modeDebug == 1 :
  print(" 001 - run checkIfFileExist.py to check if all the file we need are presents")

runScriptCheckIfFileExists = subprocess.run(['python3', './scripts/checkIfFileExist.py', 'startListFile'], check = True, capture_output = True)
if modeDebug == 1 :
  print(" 002 - get the result of the script checkIfFileExist.py")
  print(runScriptCheckIfFileExists)

if modeDebug == 1 :
  print(" 003 - code retour du script checkIfFileExist.py")
  print(runScriptCheckIfFileExists.stdout)
  # print(type(runScriptCheckIfFileExists.stdout))
  # print(str(runScriptCheckIfFileExists.stdout))
  # print(type(str(runScriptCheckIfFileExists.stdout)))
  
if "Exit_" in str(runScriptCheckIfFileExists.stdout) : 
  print("Exit_2001: a file doesn't exist")
  exit()
  



## 2. Vérification si le fichier list_exceptions.json est vide
if modeDebug == 1 :
  print(" 011 - run checkIfFileEmpty.py to check if all the file we need are presents")
  
runScriptCheckIfFileEmpty = subprocess.run(['python3', './scripts/checkIfFileEmpty.py', './List_exceptions.json'], check = True, capture_output = True)
if modeDebug == 1 :
  print(" 012 - get the result of the script checkIfFileEmpty.py")
  print(runScriptCheckIfFileEmpty)

if modeDebug == 1 :
  print(" 013 - code retour du script checkIfFileEmpty.py")
  print(runScriptCheckIfFileEmpty.stdout)
  # print(type(runScriptCheckIfFileExists.stdout))
  # print(str(runScriptCheckIfFileExists.stdout))
  # print(type(str(runScriptCheckIfFileExists.stdout)))
  
if "Exit_" in str(runScriptCheckIfFileEmpty.stdout) : 
  print("Exit_2002: no account in this environment")
  exit()



## 3. Cas Exceptions: Exécution du script de création du fichier terraform (10_main.tf) avec gestion des exceptions
if modeDebug == 1 :
  print(" 021 - run createTfFile.py to check if all the file we need are presents")
  
runScriptCreateFile = subprocess.run(['python3', './scripts/createTfFile.py'], check = True, capture_output = True)
if modeDebug == 1 :
  print(" 022 - get the result of the script createTfFile.py")
  print(runScriptCreateFile)

if modeDebug == 1 :
  print(" 023 - code retour du script createTfFile.py")
  print(runScriptCreateFile.stdout)
  # print(type(runScriptCheckIfFileExists.stdout))
  # print(str(runScriptCheckIfFileExists.stdout))
  # print(type(str(runScriptCheckIfFileExists.stdout)))
  
if "Exit_" in str(runScriptCreateFile.stdout) : 
  print("Exit_2011: no module here !!")
  exit()





## Vérification de l'exécution complète du script
if modeDebug == 1 :
  print("fin - je suis à la fin de mon script manage_exception.py")