import sys
from jinja2 import Environment, FileSystemLoader

templateFileName = sys.argv[1]+'.jinja2'
# print(templateFileName)



##########################################
## excéction python3 ./scripts/create-main-file-with-exception.py main_<nomModule>
## excéction python3 ./scripts/create-main-file-with-exception.py main_dns  pour le module dns
##########################################


def createMainFile() :
  with open("./listAccountID2", "r") as file :
    newList = file.read().rstrip().lstrip().split(" ")
    # print(newList)

    compter = 0
    nbElementList = len(newList)
    
    while compter < nbElementList : 
      env = Environment(loader=FileSystemLoader('templates'))
      template = env.get_template(templateFileName)
      # print(template)
      account = newList[compter]
      output_from_parsed_template = template.render(account=account)
      # print(output_from_parsed_template)
      
      with open("./10_main.tf", "a") as file :
        file.write("\n\n"+ output_from_parsed_template)
    
      compter +=1
  
createMainFile()