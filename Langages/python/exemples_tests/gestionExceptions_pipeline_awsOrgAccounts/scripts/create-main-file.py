from jinja2 import Environment, FileSystemLoader

# env = Environment(loader=FileSystemLoader('templates'))
# template = env.get_template('main.jinja2')


def createMainFile() :
  with open("./listAccountID", "r") as file :
    myList = file.read()
    # newList = myList.rstrip()
    # newList = newList.lstrip()
    # newList = newList.split(" ")
    newList = myList.rstrip().lstrip().split(" ")

   
    compter = 0
    nbElementList = len(newList)
    
    while compter < nbElementList : 
      env = Environment(loader=FileSystemLoader('templates'))
      template = env.get_template('main.jinja2')
      account = newList[compter]
      output_from_parsed_template = template.render(account=account)
      
      with open("./10_main.tf", "a") as file :
        file.write("\n\n"+ output_from_parsed_template)
    
      compter +=1
  
createMainFile()