import os
import sys
from jinja2 import Environment, FileSystemLoader

#####################################################################################################
#######       SCRIPT A UTILISER AVEC EN PARAMETRE LE CHEMIN RELATIF DU FICHIER A TESTER       #######
## exemple: python3 ./scripts/checkIfFileEmpty.py ./List_exceptions.json
#####################################################################################################

#----------------------------------------------- 
fileName = sys.argv[1]


def conditionFileName (fileName) : 
  file = os.path.basename(fileName)
  size = os.path.getsize(fileName) 
#  print("1001 - je suis ici path = "+fileName)
#  print("1002 - je suis ici file = "+file)
#  print("1003 - je suis ici size = "+str(size))
  
  if fileName == "./List_exceptions.json" :
    fileName = "./List_exceptions.json"
    file = os.path.basename(fileName)
    size = os.path.getsize(fileName)
#    print("1011 - je suis ici path = "+fileName)
#    print("1012 - je suis ici file = "+file)
#    print("1013 - je suis ici size = "+str(size))
    conditionFileNameIsException (fileName)
    
  elif fileName == "./listAccountID" :
    fileName = "./listAccountID"
    file = os.path.basename(fileName)
    size = os.path.getsize(fileName)
#    print("1021 - je suis ici path = "+fileName)
#    print("1022 - je suis ici file = "+file)
#    print("1023 - je suis ici size = "+str(size))
    conditionFileNameIsListAccountID (fileName)
    
  else :
    file = os.path.basename(fileName)
    size = os.path.getsize(fileName)
#    print("1031 - je suis ici path = "+fileName)
#    print("1032 - je suis ici file = "+file)
#    print("1033 - je suis ici size = "+str(size))
    sizeOfFile(fileName)


def conditionFileNameIsException (fileName) :
  file = os.path.basename(fileName)
  size = os.path.getsize(fileName)
#  print("001 - je suis ici path = "+fileName)
#  print("002 - je suis ici file = "+file)
#  print("003 - je suis ici size = "+str(size))
  
  if sizeOfFile(fileName) == 0 :    
    fileName = "./listAccountID"
#    print("004 - je suis ici path = "+fileName)
    file = os.path.basename(fileName)
    # size = os.path.getsize(fileName) 
#    print("005 - je suis ici file = "+file)
#    print("006 - je suis là taille fichier "+str(sizeOfFile(fileName)))
    conditionFileName (fileName)
    
    
  elif sizeOfFile(fileName) == 1 :
    print("007 - file size = "+str(sizeOfFile(fileName)))
    
  else: 
    print("exit_2102: That is impossible, check if the file "+ file + " exists !")
    exit()


def conditionFileNameIsListAccountID (fileName) : 
  file = os.path.basename(fileName)
  size = os.path.getsize(fileName)
#  print("011 - je suis ici path = "+fileName)
#  print("012 - je suis ici file = "+file)
#  print("013 - je suis ici size = "+str(size))
  
  if sizeOfFile(fileName) == 0 :
#    print("014 - je suis ici path = "+fileName)
#    print("015 - je suis ici size = "+str(size))
    print ("Exit_2002: no account in this environment")
    exit()
    
  elif sizeOfFile(fileName) == 1 : 
    print ("016 - The file "+ file +" is not empty")
#    print("016 - je suis ici path = "+fileName)
#    print("017 - je suis ici size = "+str(size))
    exec(open("./scripts/create-main-file.py").read())    
    print("The file 10_main.tf is created")  
    
  else: 
    print("exit_2102: That is impossible, check if the file "+ file + " exists !")
    exit()   


def sizeOfFile(fileName) : 
#  print("101 - "+fileName)
  file = os.path.basename(fileName)
  size = os.path.getsize(fileName)
#  print("102 - "+file)
#  print("103 - "+str(size))
  
  if size == 0:
#    print("111 - "+file)
    print("111 - The file "+ file +" is empty")
    return 0
  
  elif size > 0: 
#    print("121 - "+file)
    print ("121 - The file "+ file +" is not empty")
    return 1
  
  else:
    print("exit_2102: That is impossible, check if the file "+ file + " exists !")
    exit() 



conditionFileName (fileName)    ## OK 

