#---------------------------------------------------------
import os
import itertools
import json
import sys
#---------------------------------------------------------

#---------------------------------------------------------
## MODE DEBUG 
modeDebug = 1       ## mode debug actif complet
# modeDebug = 0     ## mode debug inactif
#---------------------------------------------------------

#---------------------------------------------------------
## Variables

## récupération de la liste complète des accountIDs
listAllAccountsPerEnv = open("./ListAccountID").read().replace(' ', '\n').split('\n')
if modeDebug == 1 : 
    print("  1. -----------------------------------------------------\n")
    print("  1. listAllAccountsPerEnv : list of accounts for the environment")
    print(listAllAccountsPerEnv)            ## ['tata', 'titi', 'bobo', 'baba', 'bibi']
    print(type(listAllAccountsPerEnv[1]))       ## str
nbAcc = len(listAllAccountsPerEnv)        ## pour pouvoir boucler sur la liste des accounts
if modeDebug == 1 : 
    print(nbAcc)
    print("  1. -----------------------------------------------------\n")
#---------------------------------------------------------



#---------------------------------------------------------
## Fonctions

## FONCTION creation fichier avec liste complete par env (cas SANS exceptions)
def createModulesFullList() : 
  with open('./templates/main.jinja2', 'r') as f1:
      for line in f1.readlines():
          if 'module "' in line:            ## équivalent du `grep 'module "'`
              modules = line.split('"')[1].split('-')[0]
              with open("./modules_full_list", "a") as f2 :
                f2.write(modules + " ")         ## don't write a file, append it in a list and the function will return the list


## FONCTION creation liste des modules avec au moins 1 exception
def createModulesExceptionsList() :
  with open('./List_exceptions.json', 'r') as f1:
      for line in f1.readlines():
          if '": [' in line:            ## équivalent du grep
              modules = line.split('"')[1].split('-')[0]
              with open("./modules_exceptions_list", "a") as f2 :
                f2.write(modules + " ")                

## FONCTION creation de la liste complete des possibilités modules/accounts   
def createFullList() :  
  with open('./modules_full_list', 'r') as f3:
      module = f3.read().rstrip().lstrip().split(" ")
      nbMod = len(module)
      if modeDebug == 1 : 
          print("       2.1 -----------------------------------------------------\n")
          print("       2.1 fonction createFullList :")
          print("       2.1 NbModules = ")
          print("       "+ nbMod)
          print("       2.1 -----------------------------------------------------\n")
  with open('./listAccountID', 'r') as f4:
      account = f4.read().rstrip().lstrip().split(" ")
  result = list(itertools.product(module, account))     ## crée toutes les possibilités possibles entre tous les accounts et tous les modules
  if modeDebug == 1 : 
      print("       2.2 -----------------------------------------------------\n")
      print("       2.2 fonction createFullList :")
      print("       2.2 result : "+result)
      print("       2.2 -----------------------------------------------------\n")
  fullList = json.loads(json.dumps(result))       ## permets d'avoir un json au format list -> peut être exploitable 
  if modeDebug == 1 : 
      print("       2.3 -----------------------------------------------------\n") 
      print("       2.3 fonction createFullList :")
      print("       2.3 fullList : "+fullList)
      print("       2.3 -----------------------------------------------------\n") 
  return fullList



## FONCTION comparaison et creation du nouveau fichier listAccountID2
def createNewFileForAccountPerModule(): 
  os.system("cat tmp01-accList | grep -v null > ./accList")     ## creation fichier accList en supprimant les valeurs nulles
  if modeDebug == 1 : 
      print("         3.0 -----------------------------------------------------\n")
      print("         3.0 cat fichier ./accList  \n")
      os.system("cat ./accList")
      print("         3.0 -----------------------------------------------------\n")
      
  ## récupération de liste des accounts du module en cours ayant au moins une exception 
  listAccExcepInMod = open("./accList").read().replace('"', '').replace(' ', '\n').split('\n')
  listAccExcepInMod = list(filter(None, listAccExcepInMod))     ## supprime les éléments vides dans la liste
  if modeDebug == 1 : 
      print("    12. -----------------------------------------------------\n")
      print("    12. listAccExcepInMod : list of accounts in the current module with at least 1 exception for the environment")
      print(listAccExcepInMod)            ## 
      print(type(listAccExcepInMod))       ## str
      print("    12. -----------------------------------------------------\n")
  nbAccExceptInMod = len(listAccExcepInMod)        ## pour pouvoir boucler sur la liste des accounts
  if modeDebug == 1 : 
      print("    13. -----------------------------------------------------\n")
      print("    13. nb de accounts du module avec au moins 1 exception par env\n")
      print(nbAccExceptInMod)
      print("    13. -----------------------------------------------------\n")
  
## comment créer le fichier listAccountID2 à partir des 2 fichiers ??
  for accEl in listAllAccountsPerEnv :
      if modeDebug == 1 : 
          print("           3.1 -----------------------------------------------------\n")
          print("           3.1 fonction createNewFileForAccountPerModule :")
          print("           3.1  account element : ")
          print("           "+ accEl)           
          print("           3.1 -----------------------------------------------------\n")
      ## si accEl != de TOUS LES account d'exceptions ALORS on l'ajoute           
      if accEl not in  listAccExcepInMod :           ## si les 2 valeurs sont différentes
          if modeDebug == 1 : 
              print("             3.4.3 -----------------------------------------------------\n")
              print("             3.4.3 fonction createNewFileForAccountPerModule - boucle exception - comparaison :")
              print("             3.4.3 j'écris accEl dans le fichier ./tmp02-accList : " +accEl)
              print("             3.4.3 -----------------------------------------------------\n")
          with open("./tmp02-accList", "a") as tmpfileExceptMod :     ## alors j'écris la valeur dans 1 nouveau fichier
              tmpfileExceptMod.write(accEl + " ")
      else : 
          if modeDebug == 1 : 
             print("              3.4.4 -----------------------------------------------------\n") 
             print("              3.4.4  account dans la liste des exceptions -> n ajoute pas\n") 
             print("              3.4.4 -----------------------------------------------------\n") 
          pass      ## sinon je passe a l'itération suivante                      


  if modeDebug == 1 : 
      print("       5. -----------------------------------------------------\n")
      print("       5. fonction createNewFileForAccountPerModule - fichier tmp02-accList :")
      print("       5. file tmp02-accList :")
      os.system("cat ./tmp02-accList")
      print("       5. -----------------------------------------------------\n")

  ## supression des doublons dans le fichier 
  with open("./tmp02-accList", "r") as newFile :
      f = newFile.read().replace(' ', '\n').split('\n')
      tmpList = []  ## creation d'une liste temporaire
      [tmpList.append(n) for n in f if n not in tmpList]
      tmpList = list(filter(None, tmpList))
      if modeDebug == 1 : 
          print("       6.1 -----------------------------------------------------\n")
          print("       6.1 fonction createNewFileForAccountPerModule - suppression doublon_1 :")
          print("       6.1 tmpList : ")
          print("       "+str(tmpList))
          print("       6.1 -----------------------------------------------------\n")
      compter = 0 
      nb = len(tmpList)
      if modeDebug == 1 :
          print("       6.2 -----------------------------------------------------\n")
          print("       6.2 fonction createNewFileForAccountPerModule - suppression doublon_2 - longueur liste account pour module :")
          print("       "+str(nb))
          print("       6.2 -----------------------------------------------------\n")
      while compter < nb :
          with open("./listAccountID2" , "a") as fileAccMod : 
              fileAccMod.write(tmpList[compter] +" ")
          compter +=1      


      

## FONCTION cleanup_file
def cleanup_file() :
  ## suppression des fichiers
    os.remove("./accList")
    os.remove("./tmp01-accList")
    os.remove("./tmp02-accList")
    os.remove("./listAccountID2")


def cleanup_file_noExcept() :                 ## dans le cas où il n'y a pas de d'exceptions pour 1 module
      os.remove("./listAccountID2")
      
      
                
## FONCTION creation du fichier lisAccountID2
def createMainTerraformFile() :
    compterModName = 0
    while compterModName < nbMod :
        modName = listAllModulesPerEnv[compterModName]
        if modeDebug == 1 :
            print("  7.1 -----------------------------------------------------\n")
            print("  7.1 fonction createMainTerraformFile - boucle module :")
            print("  7.1 nom du module en cours : ")
            print(modName)
            print("  7.1 -----------------------------------------------------\n")
        match modName :
            case "logdns" :
                if modeDebug == 1 :
                    print("     7.2 -----------------------------------------------------\n")
                    print("     7.2 fonction createMainTerraformFile - boucle module - module logdns :")
                    print("     7.2 Je suis dans la comparaison " +modName+" :")
                    print("     7.2 -----------------------------------------------------\n")
                    if any(modName == 'logdns' for modName in listModulesWithMin1Exception):       ## pour continuer si pas d'exceptions
                        if modeDebug == 1 :
                            print("         7.2.1 -----------------------------------------------------\n")
                            print("         7.2.1 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.2.1 -----------------------------------------------------\n")
                        os.system("jq '.logdns[].accountID' ./List_exceptions.json > ./tmp01-accList")  
                        if modeDebug == 1 :
                            print("         7.2.2 fonction createMainTerraformFile - boucle module - module logdns - fichier tmp01-accList")  
                        os.system("cat ./tmp01-accList")   
                        createNewFileForAccountPerModule()
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_dns")
                        cleanup_file()
                    else : 
                        if modeDebug == 1 :
                            print("         7.2.3 -----------------------------------------------------\n")
                            print("         7.2.3 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.2.3 -----------------------------------------------------\n")
                        os.system("cp ./listAccountID ./listAccountID2")
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_dns")
                        cleanup_file_noExcept()
            case "encrypt" : 
                if modeDebug == 1 :
                    print("     7.3 fonction createMainTerraformFile - boucle module - module encrypt :")
                    print("     7.3 Je suis dans la comparaison " +modName+" :")
                    if any(modName == 'encrypt' for modName in listModulesWithMin1Exception): 
                        os.system("jq '.encrypt[].accountID' ./List_exceptions.json > ./tmp01-accList")
                        if modeDebug == 1 :
                            print("         7.3.1 -----------------------------------------------------\n")
                            print("         7.3.1 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.3.1 -----------------------------------------------------\n")
                            print("         7.3.2 fonction createMainTerraformFile - boucle module - module encrypt - fichier tmp01-accList")  
                        os.system("cat ./tmp01-accList")  
                        createNewFileForAccountPerModule()
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_encrypt")
                        cleanup_file()
                    else : 
                        if modeDebug == 1 :
                            print("         7.3.3 -----------------------------------------------------\n")
                            print("         7.3.3 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.3.3 -----------------------------------------------------\n")
                        os.system("cp ./listAccountID ./listAccountID2")
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_encrypt")
                        cleanup_file_noExcept()
            case "config" :
                if modeDebug == 1 :
                    print("     7.4 fonction createMainTerraformFile - boucle module - module config :")
                    print("     7.4 Je suis dans la comparaison " +modName+" :")
                    if any(modName == 'config' for modName in listModulesWithMin1Exception): 
                        os.system("jq '.config[].accountID' ./List_exceptions.json > ./tmp01-accList")
                        if modeDebug == 1 :
                            print("         7.4.1 -----------------------------------------------------\n")
                            print("         7.4.1 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.4.1 -----------------------------------------------------\n")
                            print("         7.4.2 fonction createMainTerraformFile - boucle module - module config - fichier tmp01-accList")  
                        os.system("cat ./tmp01-accList")  
                        createNewFileForAccountPerModule()
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_config")
                        cleanup_file()
                    else : 
                        if modeDebug == 1 :
                            print("         7.4.3 -----------------------------------------------------\n")
                            print("         7.4.3 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.4.3 -----------------------------------------------------\n")
                        os.system("cp ./listAccountID ./listAccountID2")
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_config")
                        cleanup_file_noExcept()
            case "ssm" : 
                if modeDebug == 1 :
                    print("     7.5 fonction createMainTerraformFile - boucle module - module ssm :")
                    print("     7.5 Je suis dans la comparaison " +modName+" :")
                    if any(modName == 'ssm' for modName in listModulesWithMin1Exception): 
                        os.system("jq '.ssm[].accountID' ./List_exceptions.json > ./tmp01-accList")
                        if modeDebug == 1 :
                            print("         7.5.1 -----------------------------------------------------\n")
                            print("         7.5.1 on est dans le cas où "+modName+" a au moins 1 exception\n")
                            print("         7.5.1 -----------------------------------------------------\n")
                            print("         7.5.2 fonction createMainTerraformFile - boucle module - module ssm - fichier tmp01-accList")  
                        os.system("cat ./tmp01-accList")  
                        createNewFileForAccountPerModule()
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_ssm")
                        cleanup_file()
                    else : 
                        if modeDebug == 1 :
                            print("         7.5.3 -----------------------------------------------------\n")
                            print("         7.5.3 on est dans le cas où "+modName+" n'a aucune exception\n")
                            print("         7.5.3 -----------------------------------------------------\n")                            
                        os.system("cp ./listAccountID ./listAccountID2")
                        os.system("python3 ./scripts/create-main-file-with-exception.py main_ssm")
                        cleanup_file_noExcept() 
            case _: 
                if modeDebug == 1 :
                    print("       7.6 -----------------------------------------------------\n")
                print("       7.6 Exit_2011 : no module here !!")
                exit()
        compterModName +=1
    
#---------------------------------------------------------



#---------------------------------------------------------
## ACTIONS

## creation du fichier modules_full_list nécessaire pour la suite
createModulesFullList()   

createModulesExceptionsList()

## récupération de la liste complète des modules
listAllModulesPerEnv = open("./modules_full_list").read().replace(' ', '\n').split('\n')
listAllModulesPerEnv = list(filter(None, listAllModulesPerEnv))       ## pour supprimer les élément vide
if modeDebug == 1 :
    print("    8. -----------------------------------------------------\n")
    print("    8. listAllModulesPerEnv : list modules")
    print(listAllModulesPerEnv)        ## ['logdns', 'encrypt', 'config', 'ssm']
    print(type(listAllModulesPerEnv[1]))       ## str
    print("    8. -----------------------------------------------------\n")
nbMod = len(listAllModulesPerEnv)        ## pour pouvoir boucler sur la liste des accounts
if modeDebug == 1 :
    print("    9. -----------------------------------------------------\n")
    print("    9. nombre de modules par env\n")
    print(nbAcc)
    print("    9. -----------------------------------------------------\n")


## récupération de liste des modules ayant une exception 
listModulesWithMin1Exception = open("./modules_exceptions_list").read().replace(' ', '\n').split('\n')
listModulesWithMin1Exception = list(filter(None, listModulesWithMin1Exception)) 
if modeDebug == 1 : 
    print("    10. -----------------------------------------------------\n")
    print("    10. listModulesWithMin1Exception : list of modules with at least 1 exception for the environment")
    print(listModulesWithMin1Exception)            ## ['tata', 'titi', 'bobo', 'baba', 'bibi']
    print(type(listModulesWithMin1Exception[1]))       ## str
    print("    10. -----------------------------------------------------\n")
nbModExcept = len(listModulesWithMin1Exception)        ## pour pouvoir boucler sur la liste des accounts
if modeDebug == 1 : 
    print("    11. -----------------------------------------------------\n")
    print("    11. nb de modules avec exception par env\n")
    print(nbModExcept)
    print("    11. -----------------------------------------------------\n")



## creation de toutes les possibilités possibles modules/accounts par environment
# createFullList()

## run fonction principale du script
createMainTerraformFile()


## clean le projet
os.remove("./modules_full_list")
os.remove("./modules_exceptions_list")


#################################################################################################################################
#################################################################################################################################
#################################################################################################################################