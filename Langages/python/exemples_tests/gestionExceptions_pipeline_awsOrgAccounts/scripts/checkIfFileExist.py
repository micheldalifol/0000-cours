
#---------------------------------------------------------
import os
import sys
#---------------------------------------------------------

#####################################################################################################
###############            SCRIPT A UTILISER EN ETANT A LA RACINE DU PROJET           ###############
## exemple: python3 ./scripts/checkIfFileExists.py <nomFichierATester>
### cas debut de pipeline: python3 ./scripts/checkIfFileExists.py startListFile
#####################################################################################################

if sys.argv[1] == "startListFile" :
  startListFile = ["./listAccountID", "./List_exceptions.json", "./templates/main.jinja2", "./templates/main_dns.jinja2", "./templates/main_encrypt.jinja2", "./templates/main_config.jinja2", "./templates/main_ssm.jinja2", "./scripts/create-main-file.py", "./scripts/createTfFile.py", "./scripts/create-main-file-with-exception.py"]
  
  for file in startListFile :
    # print(sys.argv[1])
    if os.path.exists(file):
      print('The file '+ os.path.basename(file) +' exists!')
    else:
      print('Exit_2001: The file '+ os.path.basename(file) + ' does not exist.')    ## OK  -> voir pour n'afficher QUE le nom du fichier pas son chemin
      exit()      ## OK si erreur on sort
      
elif sys.argv[1] == "./exceptions_managed.json" :
  if os.path.exists(sys.argv[1]):
    print('The file '+ os.path.basename(sys.argv[1]) +' exists!')
  else:
    print('Exit_2011: The file '+ os.path.basename(sys.argv[1]) + ' does not exist.')    ## OK  -> voir pour n'afficher QUE le nom du fichier pas son chemin
    exit()      ## OK si erreur on sort
    
else:
  print('Exit_2101: The file '+ os.path.basename(sys.argv[1]) + ' is not in this project.')    ## OK  -> voir pour n'afficher QUE le nom du fichier pas son chemin
  exit()      ## OK si erreur on sort 
