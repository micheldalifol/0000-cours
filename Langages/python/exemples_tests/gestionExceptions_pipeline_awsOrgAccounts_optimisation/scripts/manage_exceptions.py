#------------------------------------------------------------------------------------------------------------------
import os
import json
import sys
import inspect
import shutil     ## copy file
from shutil import copyfile   ## copy file
from jinja2 import Environment, FileSystemLoader    ## pour l'utilisation des templates jinja2
#------------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------------------------
## MODE DEBUG 
modeDebug = 1       ## mode debug actif complet
# modeDebug = 0     ## mode debug inactif
#------------------------------------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------------------------------------
## Variables
#########################################################################################################
## PARTIE VERIFICATION SI LES FICHIERS OBLIGATOIRES SONT PRESENTS
#########################################################################################################

#========================================================================================
## liste de vérification des fichiers existants OBLIGATOIRE
startListFile = ["./listAccountID", "./List_exceptions.json", "./templates/main.jinja2", "./templates/main_logdns.jinja2", "./templates/main_encrypt.jinja2", "./templates/main_config.jinja2", "./templates/main_ssm.jinja2", "./scripts/create-main-file.py", "./scripts/createTfFile.py", "./scripts/create-main-file-with-exception.py"]
if modeDebug == 1 : 
    print("  001011 -----------------------------------------------------\n")
    print("  001011 startListFile : list of all required files")
    print(startListFile)
    print(type(startListFile))
    print("  001011 -----------------------------------------------------\n")
#========================================================================================  
cleanUpList = ["./listAccountID2"]
#========================================================================================  

#########################################################################################################
## PARTIE VERIFICATION SI LES FICHIERS SONT VIDES
#########################################################################################################

#========================================================================================
## variable pour exécution de vérif si fichiers vides (2 fichiers à tester: ./List_exceptions.json ET ./listAccountID)
fileNameCheckIfEmpty = "./List_exceptions.json"
if modeDebug == 1 : 
    print("  002003 -----------------------------------------------------\n")
    print("  002003 fileNameCheckIfEmpty : name of file to start the verification if empty")
    print("  002003 -----------------------------------------------------\n")
#========================================================================================

#########################################################################################################
## PARTIE COMPARAISON ET CREATION DU FICHIER 10_main.tf
#########################################################################################################

#========================================================================================
## récupération de la liste complète des accountIDs
listAllAccountsPerEnv = open("./listAccountID").read().replace(' ', '\n').split('\n')
listAllAccountsPerEnv = list(filter(None, listAllAccountsPerEnv))     ## pour supprimer les éléments de la liste vide si présent
if modeDebug == 1 : 
    print("  003011 -----------------------------------------------------\n")
    print("  003011  listAllAccountsPerEnv : list of accounts for the environment")
    print(listAllAccountsPerEnv)
    print(type(listAllAccountsPerEnv[1]))
nbAcc = len(listAllAccountsPerEnv)
if modeDebug == 1 : 
    print(nbAcc)
    print("  003011 -----------------------------------------------------\n")
#========================================================================================
 
#------------------------------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------------------------------
## FONCTIONS
#########################################################################################################
## PARTIE VERIFICATION SI LES FICHIERS OBLIGATOIRES SONT PRESENTS
#########################################################################################################

#========================================================================================
## vérification si les fichiers sont présents
def checkIfFilesExist(startListFile) : 
  if modeDebug == 1 : 
      print("    001012 -----------------------------------------------------\n")
      print("    001012 listOFiles : name of file :")
      print(startListFile)
      print(type(startListFile))
      print("    001012 -----------------------------------------------------\n") 
  list = inspect.getfullargspec(checkIfFilesExist)
  if modeDebug == 1 : 
      print("    001013 -----------------------------------------------------\n")
      print("    001013 listOFiles : name of file :" + list[0][0])
      print(type(list[0][0]))
      print("    001013 -----------------------------------------------------\n") 
  
  if list[0][0] == "startListFile" :
    for file in startListFile :
      if os.path.exists(file):
        print('The file '+ os.path.basename(file) +' exists!')
      else:
        print('Exit_2001: The file '+ os.path.basename(file) + ' does not exist.')
        exit()
  elif list[0][0] == "./exceptions_managed.json" :
    if os.path.exists(file):
      print('The file '+ os.path.basename(file) +' exists!')
    else:
      print('Exit_2011: The file '+ os.path.basename(file) + ' does not exist.')
      exit()
  else:
    print('Exit_2101: The file '+ os.path.basename(file) + ' is not in this project.')
    exit()
#========================================================================================


#########################################################################################################
## PARTIE VERIFICATION SI LES FICHIERS SONT VIDES
#########################################################################################################

#========================================================================================
## Récuperation du nom et de la taille du fichier fileName == ./List_exceptions.json OU ./listAccountID
def conditionFileName(fileName) : 
  size = os.path.getsize(fileName) 
  if modeDebug == 1 : 
      print("    002012 -----------------------------------------------------\n")
      print("    002012 file : name of file : " + fileName)
      print("    002012. size : name of file")
      print(size)
      print("    002012. -----------------------------------------------------\n") 
  
  if fileName == "./List_exceptions.json" or fileName == "./listAccountID":
    size = os.path.getsize(fileName)
    if modeDebug == 1 : 
        print("      002013 -----------------------------------------------------\n")
        print("      002013 file : " + fileName)
        print("      002013 -----------------------------------------------------\n")     
    conditionFileNameIsException(fileName)       
  else :
    size = os.path.getsize(fileName)
    if modeDebug == 1 : 
        print("      002014 -----------------------------------------------------\n")
        print("      002014 file : other file :" + fileName)
        print("      002014 -----------------------------------------------------\n")
    sizeOfFile(fileName)
#========================================================================================

#========================================================================================
## Récuperation du nom et de la taille du fichier ./listAccountID -> cas ./List_exceptions.json vide
def conditionFileNameIsException(fileName) :
  size = os.path.getsize(fileName)
  if modeDebug == 1 : 
      print("    002022 -----------------------------------------------------\n")
      print("    002022 file : other file :" + fileName)
      print("    002022 file : size of file : ")
      print(size)
      print("    002022 -----------------------------------------------------\n")

  if sizeOfFile(fileName) == 0 : 
    if fileName ==  "./List_exceptions.json" : 
      print("      002023 case : size of file " + fileName + " is empty")
      if modeDebug == 1 : 
          print("        002023 -----------------------------------------------------\n")
          print("        002023 run  conditionFileName('./listAccountID')")
          print("        002023 -----------------------------------------------------\n")
      sizeOfFile('./listAccountID')
    elif fileName ==  "./listAccountID" :
      print ("      002024 - Exit_2002: no account in this environment")
      exit()
    else :
      print("      002025 case : size of file " + fileName + " is empty")
  elif sizeOfFile(fileName) == 1 :
    if modeDebug == 1 : 
        print("        002026 -----------------------------------------------------\n")
        print("        002026 case : size of file " + fileName + " is NOT empty")
        print("        002026 -----------------------------------------------------\n")
    print("       002026 - file size = "+str(sizeOfFile(fileName)))   
  else: 
    print("exit_2102: That is impossible, check if the file "+ fileName + " exists !")
    exit()
#========================================================================================


#========================================================================================
## Récuperation du nom et de la taille du fichier ./listAccountID
def createMainFileWithoutException(fileName) :
  if fileName == './listAccountID' :
    if modeDebug == 1 : 
        print("      002032 -----------------------------------------------------\n")
        print("      002032 run createMainFileWithoutException")
        print("      002032 copy file ./listAccoutID to ./listAccoutID2")
        print("      002032 -----------------------------------------------------\n")
    shutil.copyfile("./listAccountID", "./listAccountID2") 
    if modeDebug == 1 : 
        print("      002033 -----------------------------------------------------\n")
        print(os.listdir('.'))      ## équivlent ls -la
        print("      002033 the file ./listAccoutID2 is created")
        print("      002032 -----------------------------------------------------\n")  
        print("      002033 -----------------------------------------------------\n")  
        print("      002033  create the file 10_main.tf \n") 
        print("      002033 -----------------------------------------------------\n") 
    listAllModulesPerEnv = createModulesFullList()
    if modeDebug == 1 : 
        print("      002034 -----------------------------------------------------\n")
        print("      002034 listAllModulesPerEnv \n")
        print(listAllModulesPerEnv)
        print("      002034 -----------------------------------------------------\n")  
    for modName in  listAllModulesPerEnv: 
      createMainTfFile(modName) 
    os.remove("./listAccountID2")  
    print("      002035 Exit_001 : The file 10_main.tf is created") 
    exit()   
#========================================================================================

#========================================================================================
## Récupération de la taille des fichiers 
def sizeOfFile(fileName) : 
  file = os.path.basename(fileName)
  size = os.path.getsize(fileName)
  if modeDebug == 1 : 
      print("      002042 -----------------------------------------------------\n")
      print("      002042  file " + file)
      print("      002042 -----------------------------------------------------\n")
  
  if size == 0:
    if modeDebug == 1 : 
        print("      002043 -----------------------------------------------------\n")
        print("      002043 case : size of file " + fileName + " is empty")
        print("      002043 -----------------------------------------------------\n")
    if fileName ==  "./listAccountID" :
      print ("      002024 - Exit_2002: no account in this environment")
      exit()
    return 0
  elif size > 0: 
    if modeDebug == 1 : 
        print("      002044 -----------------------------------------------------\n")
        print("      002044 case : size of file " + fileName + " is NOT empty")
        print("      002044 -----------------------------------------------------\n")
        if fileName == './listAccountID' :
          if modeDebug == 1 : 
              print("          002045 -----------------------------------------------------\n")
              print("          002045 case : createMainFileWithoutException")
              print("          002045 -----------------------------------------------------\n") 
          createMainFileWithoutException('./listAccountID')  
    return 1
  else:
    print("exit_2102: That is impossible, check if the file "+ file + " exists !")
    exit() 
#========================================================================================


#########################################################################################################
## PARTIE COMPARAISON ET CREATION DU FICHIER 10_main.tf
#########################################################################################################

#========================================================================================
## creation de la liste complete des modules par env (SANS exceptions)
def createModulesFullList() : 
  moduleFullList = []
  with open('./templates/main.jinja2', 'r') as f1:
      for line in f1.readlines():
        if modeDebug == 1 : 
            print("        003012 -----------------------------------------------------\n")
            print("        003012 createModulesFullList")
            print(line)
            print("        003012 -----------------------------------------------------\n")
        if 'module "' in line:            ## équivalent du `grep 'module "'`
            modules = line.split('"')[1].split('-')[0]
            if modeDebug == 1 : 
                print("          003013 -----------------------------------------------------\n")
                print("          003013 createModulesFullList - the module is :" + modules)
                print("          003013 -----------------------------------------------------\n")
            moduleFullList.append(modules)
            if modeDebug == 1 : 
                print("          003014 -----------------------------------------------------\n")
                print("          003014 createModulesFullList - the Full List of modules is :")
                print(moduleFullList)
                print("          003014 -----------------------------------------------------\n")
      f1.close()
  return moduleFullList
#========================================================================================


#========================================================================================
## creation liste des modules avec au moins 1 exception
def createModulesExceptionsList() :
  moduleListException = []                                                  ## doit être défini ici sinon erreur avec la methode append()
  if modeDebug == 1 : 
    print("  003021 -----------------------------------------------------\n")
    print("  003021  moduleListException : list of modules with at least 1 exception :")
    print(moduleListException)
    print(type(moduleListException))
    print("  003021 -----------------------------------------------------\n")
    
  with open('./List_exceptions.json', 'r') as f1:         ## Remarque: si le fichier json est vide -> erreur
      data = json.load(f1)
      if modeDebug == 1 : 
          print("      003022 -----------------------------------------------------\n")
          print("      003022 createModulesExceptionsList")
          print("      003022 f1")
          print(f1)
          print("      003022 data")
          print(data)
          print("      003022 -----------------------------------------------------\n")
      for line in data:
        if modeDebug == 1 : 
            print("      003023 -----------------------------------------------------\n")
            print("      003023 line")
            print(line)
            print("      003023 -----------------------------------------------------\n")
        moduleListException.append(line)
        if modeDebug == 1 : 
            print("        003025 -----------------------------------------------------\n")
            print("        003025 createModulesExceptionsList - the list of modules with at least 1 exception is :")
            print(moduleListException)
            print("        003025 -----------------------------------------------------\n")
      f1.close()
  return moduleListException           
#========================================================================================


#========================================================================================                
## récupération des accounts par module dans le ficheir ./List_exceptions.json
### équivalent de la commande   `jq '.logdns[].accountID' ./List_exceptions.json`
def getAccountIdFromExceptionsListForModule(modName): 
  with open('./List_exceptions.json', 'r') as f1:         ## Remarque: si le fichier json est vide -> erreur
      data = json.load(f1)
      if modeDebug == 1 : 
          print("      003022 -----------------------------------------------------\n")
          print("      003022 createModulesExceptionsList")
          print("      003022 f1")
          print(f1)
          print("      003022 data")
          print(data)
          print("      003022 -----------------------------------------------------\n")
      for line in data:
        if modeDebug == 1 : 
            print("      003023 -----------------------------------------------------\n")
            print("      003023 line")
            print(line)
            print("      003023 -----------------------------------------------------\n") 
        if line == modName :  
          if modeDebug == 1 : 
              print("      003042 -----------------------------------------------------\n")
              print("      003042 modName")
              print(modName)
              print("      003042 data[modName]")
              print(data[modName])
              print("      003042 -----------------------------------------------------\n") 
          
  return data[modName]
#======================================================================================== 


#========================================================================================                
## boucle pour récupérer le accountsID des exceptions par module
def getExceptionAccountID(listExceptAccountID) :
  for accID in listExceptAccountID :
    accountIDExcept = accID['accountID']
    if modeDebug == 1 : 
      print("        003112 -----------------------------------------------------\n")
      print("        003112 accountIDExcept with exception for the module in progress :" + accountIDExcept)
      print("        003112 -----------------------------------------------------\n") 
    listAccExcept = [accountIDExcept]
    if modeDebug == 1 : 
      print("        003113 -----------------------------------------------------\n")
      print("        003113 accountIDExcept with exception for the module in progress :")
      print(listAccExcept)
      print(type(listAccExcept))
      print("        003113 -----------------------------------------------------\n") 
  return listAccExcept
#======================================================================================== 


#======================================================================================== 
## Comparaison d'accountID pour créer le fichier ./liastAccountID2
def comparisonIfExcept(myListAccountByModule) :
  if modeDebug == 1 : 
      print("        003122 -----------------------------------------------------\n")
      print("        003122 myListAccountByModule : list of accounts in the current module with at least 1 exception for this environment")
      print(myListAccountByModule) 
      print(type(myListAccountByModule))
      print("        003122 -----------------------------------------------------\n")
  listAccExcepInMod = getExceptionAccountID(myListAccountByModule)
  if modeDebug == 1 : 
      print("        003123 -----------------------------------------------------\n")
      print("        003123 listAccExcepInMod : list of accounts in the current module with at least 1 exception for this environment")
      print(listAccExcepInMod)
      print(type(listAccExcepInMod))
      print("        003123 -----------------------------------------------------\n")
  for acc in listAllAccountsPerEnv :
    if modeDebug == 1 : 
        print("          003132 -----------------------------------------------------\n")
        print("          003132 listAllAccountsPerEnv : list of accounts for this environment")
        print(listAllAccountsPerEnv)
        print("          003132 acc : account in the full list of accounts for this environment")
        print(acc)
        print(type(acc))
        print("          003132 -----------------------------------------------------\n")
    if acc not in listAccExcepInMod : 
      if modeDebug == 1 : 
        print("            003142 -----------------------------------------------------\n")
        print("            003142 listAccExcepInMod : list of exception accounts for this module for this environment")
        print(listAccExcepInMod)
        print("            003142 -----------------------------------------------------\n")
      with open("./listAccountID2", "a") as f :
        f.write(acc+' ')
        if modeDebug == 1 : 
          print("            003152 -----------------------------------------------------\n")
          print("            003152 a file ./listAccountID2 created")
          print("            003152 -----------------------------------------------------\n")
#========================================================================================


#========================================================================================                
## creation du fichier lisAccountID2
def createMainTfFile(modName) : 
  templateFileName = "main_"+modName+'.jinja2'
  with open("./listAccountID2", "r") as file :
    newList = file.read().rstrip().lstrip().split(" ")
    compter = 0
    nbElementList = len(newList)
    
    while compter < nbElementList : 
      env = Environment(loader=FileSystemLoader('templates'))
      template = env.get_template(templateFileName)
      account = newList[compter]
      output_from_parsed_template = template.render(account=account)
      
      with open("./10_main.tf", "a") as file :
        file.write("\n\n"+ output_from_parsed_template)
      compter +=1  
#========================================================================================  


#========================================================================================                
## creation du fichier lisAccountID2
def createFileListAccountID2() : 
  if modeDebug == 1 : 
    print("        003032 -----------------------------------------------------\n")
    print("        003032 createFileListAccountID2")
    print("        003032 run the function createModulesFullList to get the list of all modules :")
    print("        003032 -----------------------------------------------------\n") 
  listAllModulesPerEnv = createModulesFullList()
  if modeDebug == 1 : 
    print("        003034 -----------------------------------------------------\n")
    print("        003034 run the function createModulesExceptionsList to get the list of all modules :")
    print("        003034 -----------------------------------------------------\n") 
  listModulesWithMin1Exception = createModulesExceptionsList()

  compterModName = 0
  nbMod = len(listAllModulesPerEnv)
  if modeDebug == 1 : 
    print("        003035 -----------------------------------------------------\n")
    print("        003035 number of modules in the full List :")
    print(nbMod)
    print("        003035 -----------------------------------------------------\n") 
  
  while compterModName < nbMod :
    modName = listAllModulesPerEnv[compterModName]
    if modeDebug == 1 :
        print("      003102 -----------------------------------------------------\n")
        print("      003102  createMainTerraformFile - loop on module :")
        print("      003102  module name in progress : " + modName)
        print("      003102 -----------------------------------------------------\n")
    match modName :
        case "logdns" :
          if modeDebug == 1 :
              print("         003103 -----------------------------------------------------\n")
              print("         003103 createMainTerraformFile - loop module - module logdns :")
              print("         003103 comparison " +modName+" :")
              print("         003103 -----------------------------------------------------\n")
          if any(modName == 'logdns' for modName in listModulesWithMin1Exception) :
            if modeDebug == 1 :
                print("            003104 -----------------------------------------------------\n")
                print("            003104  "+modName+" has at least 1 exception\n")
                print("            003104 -----------------------------------------------------\n")
            myListAccountByModule = getAccountIdFromExceptionsListForModule(modName)
            if modeDebug == 1 :
                print("            003105 -----------------------------------------------------\n")
                print("            003105  myListAccountByModule\n")
                print(myListAccountByModule)
                print(type(myListAccountByModule))
                print("            003105 -----------------------------------------------------\n")
            comparisonIfExcept(myListAccountByModule)
            createMainTfFile(modName)
            os.remove("./listAccountID2")
          else : 
            if modeDebug == 1 :
                print("         003106 -----------------------------------------------------\n")
                print("         003106  "+modName+" has NO exception\n")
                print("         003106 create file ./listAccountID2\n")
                print("         003106 -----------------------------------------------------\n")
            shutil.copyfile("./listAccountID", "./listAccountID2")
        case "encrypt" :
          if modeDebug == 1 :
              print("         003103 -----------------------------------------------------\n")
              print("         003103 createMainTerraformFile - loop module - module encrypt :")
              print("         003103 comparison " +modName+" :")
              print("         003103 -----------------------------------------------------\n")
          if any(modName == 'logdns' for modName in listModulesWithMin1Exception) :
            if modeDebug == 1 :
                print("            003104 -----------------------------------------------------\n")
                print("            003104  "+modName+" has at least 1 exception\n")
                print("            003104 -----------------------------------------------------\n")
            myListAccountByModule = getAccountIdFromExceptionsListForModule(modName)
            if modeDebug == 1 :
                print("            003105 -----------------------------------------------------\n")
                print("            003105  myListAccountByModule\n")
                print(myListAccountByModule)
                print(type(myListAccountByModule))
                print("            003105 -----------------------------------------------------\n")
            comparisonIfExcept(myListAccountByModule)
            createMainTfFile(modName)
            os.remove("./listAccountID2")
          else : 
            if modeDebug == 1 :
                print("         003106 -----------------------------------------------------\n")
                print("         003106  "+modName+" has NO exception\n")
                print("         003106 create file ./listAccountID2\n")
                print("         003106 -----------------------------------------------------\n")
            shutil.copyfile("./listAccountID", "./listAccountID2")
        case "config" :
          if modeDebug == 1 :
              print("         003103 -----------------------------------------------------\n")
              print("         003103 createMainTerraformFile - loop module - module config :")
              print("         003103 comparison " +modName+" :")
              print("         003103 -----------------------------------------------------\n")
          if any(modName == 'logdns' for modName in listModulesWithMin1Exception) :
            if modeDebug == 1 :
                print("            003104 -----------------------------------------------------\n")
                print("            003104  "+modName+" has at least 1 exception\n")
                print("            003104 -----------------------------------------------------\n")
            myListAccountByModule = getAccountIdFromExceptionsListForModule(modName)
            if modeDebug == 1 :
                print("            003105 -----------------------------------------------------\n")
                print("            003105  myListAccountByModule\n")
                print(myListAccountByModule)
                print(type(myListAccountByModule))
                print("            003105 -----------------------------------------------------\n")
            comparisonIfExcept(myListAccountByModule)
            createMainTfFile(modName)
            os.remove("./listAccountID2")
          else : 
            if modeDebug == 1 :
                print("         003106 -----------------------------------------------------\n")
                print("         003106  "+modName+" has NO exception\n")
                print("         003106 create file ./listAccountID2\n")
                print("         003106 -----------------------------------------------------\n")
            shutil.copyfile("./listAccountID", "./listAccountID2")      
    compterModName +=1
#======================================================================================== 

#------------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
## ACTIONS

#########################################################################################################
## PARTIE VERIFICATION SI LES FICHIERS OBLIGATOIRES SONT PRESENTS
#########################################################################################################
if modeDebug == 1 :
  print("###################################################################################################################\n")
  print("001001 - PARTIE VERIFICATION SI LES FICHIERS OBLIGATOIRES SONT PRESENTS")
checkIfFilesExist(startListFile)

if modeDebug == 1 :
  print("###################################################################################################################\n")

#########################################################################################################
## PARTIE VERIFICATION SI LES FICHIERS SONT VIDES
#########################################################################################################
if modeDebug == 1 :
  print("###################################################################################################################\n")
  print("002001 - PARTIE VERIFICATION SI LES FICHIERS SONT VIDES")
conditionFileName(fileNameCheckIfEmpty)

if modeDebug == 1 :
  print("###################################################################################################################\n")
  
#########################################################################################################
## PARTIE COMPARAISON ET CREATION DU FICHIER 10_main.tf
#########################################################################################################
if sizeOfFile(fileNameCheckIfEmpty) > 0 :
  if modeDebug == 1 :
    print("###################################################################################################################\n")
    print("003001 - PARTIE COMPARAISON ET CREATION DU FICHIER 10_main.tf")  
  if modeDebug == 1 :
    print("run the function createFileListAccountID2 \n")
  createFileListAccountID2()

  if modeDebug == 1 :
    print("###################################################################################################################\n")
else : 
  print("There is no exceptions")
  
#########################################################################################################
## PARTIE FIN DE SCRIPT
#########################################################################################################
## Vérification de l'exécution complète du script
if modeDebug == 1 :
  print("###################################################################################################################\n")
  print("fin - je suis à la fin de mon script manage_exception.py")
  print("###################################################################################################################\n")
#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------