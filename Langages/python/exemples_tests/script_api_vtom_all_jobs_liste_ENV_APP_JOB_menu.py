#!/data/.venv/bin/python


###############################################################
# 
# 
# usage: script_api_vtom_all_jobs_liste_ENV_APP_JOB_menu.py [-h] [-v] [-f] [-p]
# 
# Extraction API des objets VTOM
# 
# optional arguments:
# -h, --help     show this help message and exit
# 
# -v, --verbose  MODE_DEBUG active (defaut inactive)
# -f, --full     Full extraction (defaut Demo Extract)
# -p, --prod     Choix PROD (defaut PPROD)
# 
# 
###############################################################


# import librairies necessaires
import json
import requests
import re
import os
import urllib3
import argparse
urllib3.disable_warnings()
from datetime import datetime

print('=1= Debut du traitement :', str(datetime.now()))

##########################
# variables à modifier
##########################

# headers = {'Authorization': 'Basic Q0FJTExFMDQ9MQ==',}
headers = {'Authorization': 'Basic R0FJTEEhBTkU=',}


X_API_KEY_Value_prod  = 'Ic0hv'
X_API_KEY_Value_pprod = 'N54pbx'

###############################################################
# Choix mode verbose MODE_DEBUG(1) ou non MODE_DEBUG(0)
# Par defaut non verbose(1)
###############################################################
MODE_DEBUG = 0

###############################################################
# Choix extraction complete(1) ou partielle(0)
# Par defaut partielle(0)
###############################################################
full_extract = 0

##########################################
# Choix PPROD(0) ou PROD(1)
# PPROD par defaut(0)
##########################################
choix_prod_pprod = 0

##########################
# Menu
##########################
parser = argparse.ArgumentParser(description="Extraction API des objets VTOM")
group = parser.add_argument_group()
group.add_argument("-v", "--verbose", action="store_true", help="MODE_DEBUG active  (defaut inactive)")
group.add_argument("-f", "--full",    action="store_true", help="Full extraction    (defaut Demo Extract)")
group.add_argument("-p", "--prod",    action="store_true", help="Choix PROD         (defaut PPROD)")
#parser.add_argument("x", type=int, help="the API1-param")
#parser.add_argument("y", type=int, help="the API2-param")
args = parser.parse_args()

if args.verbose:
 MODE_DEBUG = 1
if args.full:
 full_extract = 1
if args.prod:
 choix_prod_pprod = 1

print("=1= MODE_DEBUG {}  full_extract {}  choix_prod_pprod {}".format(MODE_DEBUG, full_extract, choix_prod_pprod))


##########################
# Autres variables
##########################

# Adresse VTOM srwmcordo 192.168.176.179
# Adresse VTOM srwexordo 172.20.99.179
adr_vtom_pprod   = '192.168.176.179'
adr_vtom_prod    = '172.20.99.179'

port_Api         = ':30080'
port_Api_swagger = ':30002'

liste_ENV_APP_JOB_SCRIPT_PARAM_prod_csv =  'liste_ENV_APP_JOB_SCRIPT_PARAM_prod.csv'
liste_ENV_APP_JOB_SCRIPT_PARAM_pprod_csv = 'liste_ENV_APP_JOB_SCRIPT_PARAM_pprod.csv'

liste_all_jobs_vtom_prod_json =            'liste_all_jobs_vtom_prod.json'
liste_all_jobs_vtom_pprod_json =           'liste_all_jobs_vtom_pprod.json'

# FIC_OUT='/data/private/VTOM/liste_ENV_APP_JOB_SCRIPT_PARAM_pprod.csv'
# FIC_OUT_PROD= '/data/scripts/VTOM/extractTraitements/Test_Python/liste_ENV_APP_JOB_SCRIPT_PARAM_prod.csv'
# FIC_OUT_PPROD='/data/scripts/VTOM/extractTraitements/Test_Python/liste_ENV_APP_JOB_SCRIPT_PARAM_pprod.csv'
path_fic_out =  os.getcwd()
FIC_OUT_PROD =  str(path_fic_out) + str('/') + str(liste_ENV_APP_JOB_SCRIPT_PARAM_prod_csv)
FIC_OUT_PPROD = str(path_fic_out) + str('/') + str(liste_ENV_APP_JOB_SCRIPT_PARAM_pprod_csv)
 

# Affichage info PWD, LOGIN, SYS
if MODE_DEBUG == 1:
 print('=1aa=   pwd       ', os.getcwd())
 print('=1ab=   login     ', os.getlogin())
 print('=1ac=   uname     ', os.uname())


if choix_prod_pprod == 1:
 #####################
 # Choix PROD
 #####################
 adr_vtom = adr_vtom_prod
 FIC_OUT  = FIC_OUT_PROD
 liste_all_jobs_vtom_json = liste_all_jobs_vtom_prod_json
 X_API_KEY_Value = X_API_KEY_Value_prod

else:
 #####################
 # Choix PPROD
 #####################
 adr_vtom = adr_vtom_pprod
 FIC_OUT  = FIC_OUT_PPROD
 liste_all_jobs_vtom_json = liste_all_jobs_vtom_pprod_json
 X_API_KEY_Value = X_API_KEY_Value_pprod



if MODE_DEBUG == 1:
 print('=1ef=   IP VTOM   ', adr_vtom)
 print('=1eg=   FIC_OUT   ', FIC_OUT)
 print('=1eh=   fic liste_all_jobs_vtom_json')
 print('                  ', liste_all_jobs_vtom_json)
 print('=1ei=   X-API-KEY ', X_API_KEY_Value)




if full_extract == 1:
  MODE_DEBUG = 0


# curl liste de tous les jobs et affectation a une variable/objet
# Requete API http://192.168.176.179:30080/api/job/list
# response = requests.get('http://192.168.176.179:30080/api/job/list', headers=headers)
response_http = str('http://')
response_http = str(response_http) + str(adr_vtom)
response_http = str(response_http) + str(port_Api)
response_http = str(response_http) + str('/api/job/list')

if MODE_DEBUG == 1:
  print('=1ad=   requete API liste de tous les jobs ', response_http)

try:
    response = requests.get(str(response_http), headers=headers)
except:
    print("# Erreur acces page job list : ", str(response_http))
    exit()

# print(response)
# print(response.json())

# Check the response
#if response:
if response.status_code == 200:
    if MODE_DEBUG == 1:
      # Print the response status code
      print('=1a= The status code of the response is %d\n' %response.status_code)

      # Print the JSON content
      # print('=1ae= The JSON content is : \n%s' %response.json())
      print('=1ae= The JSON content is : liste de tous les jobs ... \n')

      # Print the success message
      print('=1b= The request is handled successfully.\n')

    # suppression fichier sortie si existe
    filePath = FIC_OUT
    if os.path.exists(filePath):
        os.remove(filePath)
    else:
        print("=1c= Can not delete the file as it doesn't exists\n")

    # creation fichier sortie et ecriture noms colonnes
    with open (FIC_OUT, 'a') as f:
      print('ENVIRONNEMENT;AGENT;APPLICATION;DATE;APPLI_MODE_EXECUTION;JOB;JOB_MODE_EXECUTION;SCRIPT;NB_PARAMETRES;PARAMETRES', file=f)

    # Write response into "liste_all_jobs_vtom_pprod.json" file
    if MODE_DEBUG == 1: 
      print('=1cc= Write response into liste_all_jobs_vtom_json file\n')
    with open(str(liste_all_jobs_vtom_json), 'w') as json_output_file:
      json.dump(response.json(), json_output_file)

    if MODE_DEBUG == 1:
      print('=1ccc= on boucle sur toutes les lignes du fichier liste_all_jobs_vtompprod.json\n')

    # on boucle sur toutes les lignes du fichier liste_all_jobs_vtompprod.json
    fichier_json = open(str(liste_all_jobs_vtom_json), 'r')
    # fichier_json = open('fic_test.json', 'r')
    with fichier_json as fichier:
      
      # decode un fichier json et le transforme en dictionnaire python
      data = json.load(fichier)

      # print(data['rc'])
      
      #print (data)
      if data['rc'] != 0:
        print('##################################################################################################')
        print('# ERREUR')
        print('#   ligne : ', data)
        print('#      rc : ', data['rc'])
        print('#  errmsg : ', data['errmsg'])
        print("# Verifier la ligne suivante svp : # headers = {'Authorization': 'Basic xxxxxxxxxxxxxxxxxxx',}")
        print('##################################################################################################')
        exit()

      # exit()

      for row in data['result']['rows']:
        
        #print(row['environmentName'])
        ligne = ""
        ENVIRONNEMENT = ""
        APPLICATION = ""
        APPLI_MODE_EXECUTION = ""
        SUBMISSION_UNIT = ""
        AGENT = ""

        ligne=(row['environmentName'],row['applicationName'],row['id'])
        #if MODE_DEBUG == 1:
        #print(ligne)

        ENVIRONNEMENT = (row['environmentName'])
        APPLICATION=(row['applicationName'])
        APPLI_MODE_EXECUTION = (row['execMode'])
        # si on veut limiter a un ENV + APPLI :
        #   - decommenter une ligne ci-dessous
        #   - augmenter le retrait (A DROITE de l'actuel) de la ligne 95 (if MODE_DEBUG == 1:) à la ligne 262 (print('Invalid responseJob.'))
        if ( not full_extract and not ((ENVIRONNEMENT == 'BANCAIRE_EDI' and APPLICATION == 'ST_SOCIET_IARD') or (ENVIRONNEMENT == 'REF_CLEVA_EX' and APPLICATION == 'ENC_FLUX_TIP_J') or (ENVIRONNEMENT == 'REF_CLEVA_EX' and APPLICATION == 'DEBUT_CLEVA_J_1') or (ENVIRONNEMENT == 'TECHNIQUE' and APPLICATION == 'JBOV12D'))):
          continue
        #if (ENVIRONNEMENT == 'BANCAIRE_EDI' and APPLICATION == 'ST_SOCIET_IARD'):
        #if (ENVIRONNEMENT == 'TECHNIQUE' and APPLICATION == 'JBOV12D'):

        ###############################
        if MODE_DEBUG == 1:
          print('=2= ENVIRONNEMENT           = ', ENVIRONNEMENT)
          print('=3= APPLICATION             = ', APPLICATION)
          print('=4= APPLI_MODE_EXECUTION    = ', APPLI_MODE_EXECUTION)
        ######################################################################
        # APPLICATION
        ######################################################################
        # recherche mode execution de cette application
        headersResponseApiAppli = {
                # 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
                # 'accept': 'application/json',
                'X-API-KEY': str(X_API_KEY_Value),
                # 'X-API-KEY': 'N546D5u0IoabSpbx',
                # 'X-API-KEY': 'xxxxxxxxxxxxxxxxxxxxxxxxxx',
                # 'Connection': 'close',
                }
        # requeteResponseApiAppli = ('https://192.168.176.179:30002/vtom/public/domain/1.0/environments/')
        # Requete API https://192.168.176.179:30002/vtom/public/domain/1.0/environments/
        requeteResponseApiAppli = str('https://')
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str(adr_vtom)
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str(port_Api_swagger)
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str('/vtom/public/domain/1.0/environments/')
        if MODE_DEBUG == 1:
          print('\n=4a= requete API ENVIRONNEMENT & APPLI ', requeteResponseApiAppli)
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str(ENVIRONNEMENT)
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str('/applications/')
        requeteResponseApiAppli = str(requeteResponseApiAppli) + str(APPLICATION)
        if MODE_DEBUG == 1:
          print('\n=5= requeteResponseApiAppli =', requeteResponseApiAppli)

        try:
            responseResponseApiAppli = requests.get(requeteResponseApiAppli, headers=headersResponseApiAppli, verify=False)
        except:
            print("ERREUR acces page ENV & APP: ", str(requeteResponseApiAppli))
            exit()

        #if responseResponseApiAppli:
        if responseResponseApiAppli.status_code == 200:          
             if MODE_DEBUG == 1:
                # Print the response status code
                #print('\nThe status code of the response is %d' %response.status_code)
                #print('\nThe status code of the requeteResponseApiAppli is %d' %requeteResponseApiAppli)
                print('\n=5b= The status code of the responseResponseApiAppli is %d' %responseResponseApiAppli.status_code)
             dataResponseApiAppli = responseResponseApiAppli.json()
             if MODE_DEBUG == 1:
               # affichage du contenu du fichier json resultant
               print("\n=6= affichage du contenu du fichier json resultant :")
               print("=6a= \n", dataResponseApiAppli)
             # definition des variables
             
             #===# DATE #===#
             if 'date' not in dataResponseApiAppli:
                DATE_APPLI='null'
             else:
                DATE_APPLI=(dataResponseApiAppli['date'])
             if MODE_DEBUG == 1:
              print('\n=6aa= DATE_APPLI = ', DATE_APPLI)
             APPLI_MODE_EXECUTION=(dataResponseApiAppli['execMode'])
             if 'submitUnit' not in dataResponseApiAppli:
                AGENT = 'null'
             else:
                SUBMISSION_UNIT=(dataResponseApiAppli['submitUnit'])
                AGENT = str(SUBMISSION_UNIT)
             if MODE_DEBUG == 1:            
                print('=7= APPLI_MODE_EXECUTION = ', APPLI_MODE_EXECUTION)                
                print('=8= AGENT = ', AGENT)               
        else:    
           # Print the error message for the invalid response
           # print('=5b= The status code of the responseResponseApiAppli is %d' %responseResponseApiAppli.status_code)
           print("####################################################################################")
           print('# ERREUR')
           print('# Invalid response responseResponseApiAppli : ', responseResponseApiAppli.status_code)
           print("# Verifier la ligne suivante svp : # X_API_KEY_Value_[p]prod = 'xxxxxxxxxxxx'")
           print("####################################################################################")

           if MODE_DEBUG == 1:
              # Print the response status code
              #print('\nThe status code of the response is %d' %response.status_code)
              #print('\nThe status code of the requeteResponseApiAppli is %d' %requeteResponseApiAppli)
              print('\n=5b= The status code of the responseResponseApiAppli is %d' %responseResponseApiAppli.status_code)

           exit()   

        #######################################################################
        # JOB
        #######################################################################
        # params = (('id', 'JOBac1463b300004ae15dcd17c700019ece'),)
        params = (('id', row['id']),)
        if MODE_DEBUG == 1:
            print('\n=9= http://192.168.176.179:30080/api/job/getById?id=',row['id'])
            print('=10= headers=', headers)
            print('=11= params=', params)
        # responseJob = requests.get('http://192.168.176.179:30080/api/job/getById', headers=headers, params=params)
        requeteResponseApiJobId = str('http://')
        requeteResponseApiJobId = str(requeteResponseApiJobId) + str(adr_vtom)
        requeteResponseApiJobId = str(requeteResponseApiJobId) + str(port_Api)
        requeteResponseApiJobId = str(requeteResponseApiJobId) + str('/api/job/getById')
        if MODE_DEBUG == 1:
          print('=11a= requete API liste jobs par ID ', requeteResponseApiJobId)

        try:
            responseJob = requests.get(requeteResponseApiJobId, headers=headers, params=params)
        except:
            print("ERREUR acces page JOB by ID : ", str(requeteResponseApiJobId))
            exit()
    
        # print(responseJob.json())
        #if responseJob:
        if responseJob.status_code == 200:
              JOB_NAME = ""
              JOB_SCRIPT = ""
              if MODE_DEBUG == 1:
                # Print the response status code
                print('=12= The status code of the responseJob is %d\n' %responseJob.status_code)
              # load json reponse dans une variable
              dataJob = responseJob.json()
              if MODE_DEBUG == 1:
                # affichage du contenu du fichier json resultant
                print("=12a= ", dataJob)
                # affichage du nom du job
                print("\n=12b= JOB ", dataJob['result']['name'])
                # affichage du nom du script
                print("=12c= SCRIPT ", dataJob['result']['Script'])
              # definition des variables
              JOB_NAME=(dataJob['result']['name'])
              JOB_SCRIPT=(dataJob['result']['Script'])
              # JOB_MODE_EXECUTION=(dataJob['result']['execMode'])
              if MODE_DEBUG == 1:
                print('=13= JOB_NAME = ', JOB_NAME)
                print('=14= JOB_SCRIPT = ', JOB_SCRIPT)
              #############################################################################
              # caracteristiques jobs :  "unite de soumission" et "mode execution" 
              ############################################################################
              # recherche unite soumission de ce job
              headersResponseApiJob = {
                      # 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
                      # 'accept': 'application/json',
                      'X-API-KEY': str(X_API_KEY_Value),
                      # 'X-API-KEY': 'N546D5u0IoabSpbx',
                      # 'X-API-KEY': 'xxxxxxxxxxxxxxxxxxxxx',
                      # 'Connection': 'close',
                      }
              # requeteResponseApiJob = ('https://192.168.176.179:30002/vtom/public/domain/1.0/environments/')
              requeteResponseApiJob = str('https://')
              requeteResponseApiJob = str(requeteResponseApiJob) + str(adr_vtom)
              requeteResponseApiJob = str(requeteResponseApiJob) + str(port_Api_swagger)
              requeteResponseApiJob = str(requeteResponseApiJob) + str('/vtom/public/domain/1.0/environments/')
              if MODE_DEBUG == 1:
                print('\n=14a= requete API ENVIRONNEMENT & APPLI & JOB \n', requeteResponseApiAppli)
              requeteResponseApiJob = str(requeteResponseApiJob) + str(ENVIRONNEMENT)
              requeteResponseApiJob = str(requeteResponseApiJob) + str('/applications/')
              requeteResponseApiJob = str(requeteResponseApiJob) + str(APPLICATION)
              requeteResponseApiJob = str(requeteResponseApiJob) + str('/jobs/')
              requeteResponseApiJob = str(requeteResponseApiJob) + str(JOB_NAME)
              if MODE_DEBUG == 1:
                print("\n=15= \n", requeteResponseApiJob) 
  
              NB_PARAMETRES = ""
              JOB_PARAMETRES = ""  
              SUBMISSION_UNIT = ""  
              JOB_PARAMETRES = ""

              try:       
                  responseResponseApiJob = requests.get(requeteResponseApiJob, headers=headersResponseApiJob, verify=False)
              except:
                  print("ERREUR acces page ENV, APP & JOB : ", str(requeteResponseApiJob))
                  exit()

              if responseResponseApiJob:
                if MODE_DEBUG == 1:
                  # Print the response status code
                  print('\n=16= The status code of the responseResponseApiJob is %d' %responseResponseApiJob.status_code)
                # load json reponse dans une variable
                dataResponseApiJob = responseResponseApiJob.json()
                if MODE_DEBUG == 1:
                  # affichage du contenu du fichier json resultant
                  print("\n=17= ", dataResponseApiJob)
                # definition des variables
                JOB_MODE_EXECUTION=(dataResponseApiJob['execMode'])
                if MODE_DEBUG == 1:
                  print('\n=18= JOB_MODE_EXECUTION = ', JOB_MODE_EXECUTION)
                if AGENT == 'null' or AGENT == "":
                  if 'submitUnit' not in dataResponseApiJob:
                    AGENT = 'null'
                  else:
                    SUBMISSION_UNIT=(dataResponseApiJob['submitUnit'])
                    AGENT = str(SUBMISSION_UNIT)
                if MODE_DEBUG == 1: 
                  print('=19= SUBMISSION_UNIT = ', SUBMISSION_UNIT)
                  print('=20= AGENT = ', AGENT)
                  print('=20a= DATE_APPLI = ', DATE_APPLI)
                # Recherche AGENT ou DATE par default
                if AGENT == 'null' or DATE_APPLI == 'null':
                  if MODE_DEBUG == 1:
                    print('\n=21= agent or date_appli is nullllllll')   
                  # on va chercher l'unite de soumission parametree par defaut sur l'environnement
                  # requeteResponseEnvByDefault = ('https://192.168.176.179:30002/vtom/public/domain/1.0/environments/')
                  requeteResponseEnvByDefault = str('https://')
                  requeteResponseEnvByDefault = str(requeteResponseEnvByDefault) + str(adr_vtom)
                  requeteResponseEnvByDefault = str(requeteResponseEnvByDefault) + str(port_Api_swagger)
                  requeteResponseEnvByDefault = str(requeteResponseEnvByDefault) + str('/vtom/public/domain/1.0/environments/')
                  
                  requeteResponseEnvByDefault = str(requeteResponseEnvByDefault) + str(ENVIRONNEMENT)
                  if MODE_DEBUG == 1:
                    print('\n=21a= requete API ENVIRONNEMENT \n', requeteResponseEnvByDefault)
                  
                  # curl -X GET "https://localhost:30002/vtom/public/domain/1.0/environments/
                  #ENV_MaC?api_key=xxxxxxxxxxxxxxxxxx" -H  "accept: application/json" -H  "X-API-KEY: xxxxxxxxxxxxxxxxxxx"          

                  try:
                      requeteResponseEnvByDefault = requests.get(requeteResponseEnvByDefault, headers=headersResponseApiJob, verify=False)
                  except:
                      print("ERREUR acces page ENV: ", str(requeteResponseEnvByDefault))
                      exit()

                  if requeteResponseEnvByDefault:
                    dataResponseEnvByDefault = requeteResponseEnvByDefault.json()
                    if MODE_DEBUG == 1:
                      print("\n=22= ", dataResponseEnvByDefault)
                    if AGENT == 'null':
                      SUBMISSION_UNIT_BY_DEFAULT=(dataResponseEnvByDefault['submitUnit'])
                      AGENT = str(SUBMISSION_UNIT_BY_DEFAULT)
                      if MODE_DEBUG == 1:
                        print('\n=23= SUBMISSION_UNIT_BY_DEFAULT = ', SUBMISSION_UNIT_BY_DEFAULT)
                    if DATE_APPLI == 'null':
                      DATE_BY_DEFAULT=(dataResponseEnvByDefault['date'])
                      DATE_APPLI = str(DATE_BY_DEFAULT)
                      if MODE_DEBUG == 1:
                        print('\n=24a= DATE_BY_DEFAULT = ', DATE_BY_DEFAULT)
                    if MODE_DEBUG == 1: 
                      print('=24= AGENT = ', AGENT)
                      print('=24b= DATE_APPLI = ', DATE_APPLI)
              else:    
                  # Print the error message for the invalid response
                  print('Invalid response responseResponseApiJob.')
                  exit()
              # tous les jobs n'ont pas de parametres 
              if "Parameters" in (dataJob['result']):
                if MODE_DEBUG == 1:
                  print("\n=25= la cle Parameters est presente")
                # compte le nb de parametres
                NB_PARAMETRES=len(dataJob['result']['Parameters'])
                if MODE_DEBUG == 1:
                 print('\n=25a=', dataJob['result']['Parameters'])
                JOB_PARAMETRES=(dataJob['result']['Parameters'])
                # le champ contient les valeurs a supprimer : '[]
                # ex : ['EX-TEK', 'WebAgpm', 'METIER', 'Jboss', 'Reboot', '***']
                JOB_PARAMETRES=re.sub(r"[\['\]]","",str(JOB_PARAMETRES))
                # resultat : EX-TEK,WebAgpm,METIER,Jboss,Reboot,***
              else:
                JOB_PARAMETRES=""
                NB_PARAMETRES=""
              # realisation de la ligne csv
              # init list
              ligne_csv=[]
              # ajout de chaque element
              ligne_csv.append(ENVIRONNEMENT)
              ligne_csv.append(AGENT)
              ligne_csv.append(APPLICATION)
              ligne_csv.append(DATE_APPLI)
              ligne_csv.append(APPLI_MODE_EXECUTION)
              ligne_csv.append(JOB_NAME)
              ligne_csv.append(JOB_MODE_EXECUTION)
              ligne_csv.append(JOB_SCRIPT)
              ligne_csv.append(str(NB_PARAMETRES))
              ligne_csv.append(JOB_PARAMETRES)
              # ajout du separateur entre chaque element
              ligne_csv_finale=';'.join(ligne_csv)
              if MODE_DEBUG == 1:            
                print('\n=26= CSV == : ', ligne_csv_finale, '\n\n')
              # ecriture de chaque ligne dans le fichier de sortie
              with open (FIC_OUT, 'a') as f:
                  print(ligne_csv_finale, file=f)
        else:
             print('Invalid responseJob.')
             exit()
else:  
    # Print the error message for the invalid response
    print('Invalid response.')

print('=1111= Fin du traitement :', str(datetime.now()))

exit()


