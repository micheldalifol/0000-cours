from jinja2 import Environment, FileSystemLoader

## créer un fichier script.sh à partir d'un fichier template et d'un fichier liste

env = Environment(loader=FileSystemLoader('templates'))
template = env.get_template('cleanUp-terraformState.jinja2')

def createProviderFile() :
  with open("./fichiers_tests/tfstate_deleted_account", "r") as f1 :
    #myList = f1.readlines()              ## pour lire ligne par ligne
    myList = f1.read().splitlines()       ## pour lire ligne par ligne en enlevant les "\n"
    #print(myList)
    #print(type(myList))
    

  output_from_parsed_template = template.render(lines=myList)
  print(output_from_parsed_template)
      
  with open("./cleanUp-terraformState.sh", "a") as f2 :
    f2.write(output_from_parsed_template)

    
createProviderFile()


