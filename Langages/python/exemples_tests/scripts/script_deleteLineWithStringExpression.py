## suppression depuis 1 fichier (avec creation d'un nouveau fichier) toutes les lignes contenant 'ber' 
with open('./fichiers_tests/months.txt', 'r') as fr:     ## ouvre le fichier "month" qui contient tous les mois de l'année (en anglais) 1 par ligne
  lines = fr.readlines()

  with open('./fichiers_tests/months_2.txt', 'w') as fw: ## crée un nouveau fichier 
      for line in lines:                ## en bouclant ligne par ligne
          
          # find() returns -1
          # if no match found
          if line.find('ber') == -1:    ## supprimer la ligne si elle contient "ber"
              fw.write(line)            
print("Deleted lines done")