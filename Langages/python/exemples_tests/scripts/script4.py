

# with open("./fichiers_tests/list", "r") as file :
#   maList = file.read()
#   print(maList)

#   compter = 0
#   nbElementList = len(maList)
#   print(nbElementList)

#   while compter < nbElementList :  
#     account = maList[compter]
    
#     ## create providers folder form jinja2 template
    
#     print(account)
#     with open("./11_providers.tf", "a") as file : 
#       file.write("\n\naccount")  
#     compter +=1

## n'affiche pas ce qu'il faut: 
## - chaque caractère a 1 indice (il faut que chaque élément - 1 element par ligne - ait 1 indice)
## - la variable "account" n'est pas prise en compte dans la création du fichier providers.tf (le mot account apparait)

#----------------------------------------------------------------------------------------

# with open("./fichiers_tests/list", "r") as file :
#   maList = file.read()
#   # print(maList)
#   # print(type(maList))
  
#   test = maList.split(" ")
#   # print(test)
#   # print(type(test))
  
  
#   compter = 0
#   nbElementList = len(test)
#   print(nbElementList)

#   while compter < nbElementList :  
#     account = test[compter]
    
#     ## create providers folder form jinja2 template
    
#     print(account)
#     with open("./11_providers.tf", "a") as file : 
#       file.write("\n\n"+ account)             ## crée mon fichier avec ma valeur d'ID -> OK
#     compter +=1

#----------------------------------------------------------------------------------------
# from jinja2 import Template

# myTemp = Template(" {{ account }} ")



# with open("./fichiers_tests/list", "r") as file :
#   maList = file.read()
#   # print(maList)
#   # print(type(maList))
  
#   test = maList.split(" ")
#   # print(test)
#   # print(type(test))
  
  
#   compter = 0
#   nbElementList = len(test)
#   #print(nbElementList)



#   while compter < nbElementList :  
#     account = test[compter]
    
#     ## create providers folder form jinja2 template
#     msg = myTemp.render(account=account)                ## utilisation d'une variable template -> OK
# #    print(msg)
    
    
#   #  print(account)
#     with open("./11_providers.tf", "a") as file : 
#       file.write("\n\n"+ msg)             
#     compter +=1

#----------------------------------------------------------------------------------------

# from jinja2 import Template

# myTemp = Template(" {{ account }} ")

# def createProviderFile() : 
#   with open("./fichiers_tests/list", "r") as file :
#     maList = file.read()
#     test = maList.split(" ")
    
#     compter = 0
#     nbElementList = len(test)
    
#     while compter < nbElementList : 
#       account = test[compter]
#       msg = myTemp.render(account=account)
      
#       with open("./11_providers.tf", "a") as file :
#         file.write("\n\n"+ msg)                                 ## crée le fichier 11_providers.tf avec les données du fichier "list"  -> OK
    
#       compter +=1
    
# createProviderFile()


#----------------------------------------------------------------------------------------


from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))       ## important créer un répertoire "templates" au même niveau que le script
template = env.get_template('providers.jinja2')               ## ajouter le nom du fichier templates dans le répertoire templates


def createProviderFile() : 
  with open("./fichiers_tests/listAccountID", "r") as file :
    maList = file.read()
    test = maList.rstrip()                                    ## pour supprimer les caractères en fin de ligne (avec \n par ex)
    test = test.split(" ")                                    ## pour séparer chaque "valeur" sur 1 ligne
    
    compter = 0
    nbElementList = len(test)
    
    while compter < nbElementList : 
      account = test[compter]
      output_from_parsed_template = template.render(account=account)
      
      with open("./11_providers.tf", "a") as file :
        file.write("\n\n"+ output_from_parsed_template)                                 ## crée le fichier 11_providers.tf avec les données du fichier "list" et du template -> OK
    
      compter +=1
      compter +=1
    
createProviderFile()


#----------------------------------------------------------------------------------------



#----------------------------------------------------------------------------------------



#----------------------------------------------------------------------------------------