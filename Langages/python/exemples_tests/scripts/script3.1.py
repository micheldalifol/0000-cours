from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))
template = env.get_template('main.jinja2')


def createMainFile() :
  with open("./fichiers_tests/listAccountID", "r") as file :
    myList = file.read()
    # print(myList)
    newList = myList.rstrip()                        ## suppression espace en fin de ligne
    newList = newList.lstrip()                          ## suppression espace en début de ligne
    # print(newList)
    newList = newList.split(" ")
    # print(newList)
    # print(type(newList))
    # print(newList[0])
    # print(type(newList[0]))
   


    output_from_parsed_template = template.render(account=newList)
    # print(output_from_parsed_template)
    with open("./10_main.tf", "a") as file :
      file.write(output_from_parsed_template)

  
createMainFile()