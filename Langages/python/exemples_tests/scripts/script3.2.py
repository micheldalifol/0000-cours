from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))
template = env.get_template('main2.jinja2')


def createMainFile() :
  with open("./fichiers_tests/listAccountID", "r") as file :
    myList = file.read()
    # print(myList)
    newList = myList.rstrip()                        ## suppression espace en fin de ligne
    newList = newList.lstrip()                          ## suppression espace en début de ligne
    # print(newList)
    newList = newList.split(" ")
    # print(newList)
    # print(type(newList))
    # print(newList[0])
    # print(type(newList[0]))
   
    compter = 0
    nbElementList = len(newList)
    
    while compter < nbElementList : 
      account = newList[compter]
      output_from_parsed_template = template.render(account=account)
      
      with open("./12_main-reviewdev.tf", "a") as file :
        file.write(output_from_parsed_template)                                 ## crée le fichier 11_providers.tf avec les données du fichier "list" et du template -> OK
    
      compter +=1
  
createMainFile()