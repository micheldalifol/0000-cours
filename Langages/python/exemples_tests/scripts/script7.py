## connaitre le nb de string dans 1 fichier
### permet de lister le nombre d'account dans 1 fichier

fileNameList = ["./fichiers_tests/listAccountID-reviewdev", "./fichiers_tests/listAccountID"]


def readAccount() :
  
  compt = 0
  nbElList = len(fileNameList)
#  print(nbElList)
  
  while compt < nbElList :
    fileName =  fileNameList[compt]
    with open(fileName, "r") as file :
      myList = file.read()
      newList = myList.rstrip()                           ## suppression espace en fin de ligne
      newList = newList.lstrip()                          ## suppression espace en début de ligne
      newList = newList.split(" ")
      #print(newList)
    
      compter = 0
      nbElementList = len(newList)
      
      while compter < nbElementList : 
        account = newList[compter]
        print(account)                               ## crée le fichier 11_providers.tf avec les données du fichier "list" et du template -> OK
      
        compter +=1

    compt +=1
  
#### comparer les valeurs entre les 2 fichiers
  
readAccount()