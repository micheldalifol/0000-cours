#!/bin/bash
###########################################
## créer un fichier liste à partir d'un ancien fichier et d'une liste
###########################################

#set -x 



echo "---------------------------------------------------------------"
echo "créer la liste des tstates communs"
fileList=$(cat ./fichiers_tests/listAccountID)
for account in ${fileList} ; do cat ./fichiers_tests/tfstate.old | grep ${account}  >> ./fichiers_tests/tfstate_bash_new ; done

echo "---------------------------------------------------------------"
echo "création du fichier pour supprimer les tfstates"
grep -vxFf ./fichiers_tests/tfstate_bash_new ./fichiers_tests/tfstate.old | grep -v "data." >> ./fichiers_tests/tfstate_deleted_account
