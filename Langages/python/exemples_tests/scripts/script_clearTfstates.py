## suppression depuis 1 fichier (avec creation d'un nouveau fichier) toutes les lignes contenant la portion de string souhaitée


#---------------------------------------------------------------------------------------------------------------------

# with open('./fichiers_tests/tfstate.old', 'r') as fr:     ## ouvre le fichier "tfstate.old" qui contient tous les tfstates AVANT exécution de la pipeline
#   lines = fr.readlines()

#   with open('./fichiers_tests/tfstate.new', 'w') as fw: ## crée un nouveau fichier 
#       for line in lines:                ## en bouclant ligne par ligne
          
#           # find() returns -1
#           # if no match found
#           if line.find('toto') == -1:    ## supprimer la ligne si elle contient "toto"
#               fw.write(line)            
# print("clean-up done")          ## OK ça marche bien

#---------------------------------------------------------------------------------------------------------------------



#---------------------------------------------------------------------------------------------------------------------

# with open('./fichiers_tests/tfstate.old', 'r') as fr:     ## ouvre le fichier "tfstate.old" qui contient tous les tfstates AVANT exécution de la pipeline
#   lines = fr.readlines()


#   with open("./fichiers_tests/listAccountID", "r") as file :
#     myList = file.read()
#     newList = myList.rstrip()
#     newList = newList.lstrip()
#     newList = newList.split(" ")
#     #print(newList)
    
#     compter = 0
#     nbElementList = len(newList)
#     #print(nbElementList)
    
#     while compter < nbElementList : 
#       account = newList[compter]
#       print("account = " +account)
    
#       with open('./fichiers_tests/tfstate2.new', 'a') as fw: ## crée un nouveau fichier   
#           for line in lines:                ## en bouclant ligne par ligne
              
#               # find() returns -1
#               # if no match found
#               if line.find(account) == -1:    ## supprimer la ligne si elle contient "toto"
#                   fw.write(line)  
#       compter +=1   
                       
# print("clean-up done")          ## supprime correctement MAIS REPREND TOUT pour chaque account dans la boucle while


#---------------------------------------------------------------------------------------------------------------------
