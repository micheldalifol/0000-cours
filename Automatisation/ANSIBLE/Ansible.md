# ANSIBLE

## Présentation

Créé en 2012, récupéré en 2015 par RedHat. Codé en python.
Possède une grosse [documentation](https://docs.ansible.com/) et beaucoup de modules.
Il a des librairies supplémentaires qui sont [ansible galaxy](https://galaxy.ansible.com/), [ansible vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html), ...
Travaille en **ssh et est agentless**, donc en fait en _push_.
Peut-être intégré dans la CICD.

## DEFINITIONS
### control node
>Noeud central de ansible. C'est lui qui fait tous les déploiements. Il doit donc être sécurisé.
Il possède les clés ssh et les passwords de tout le SI.

### manage node
>Ce sont les cibles. Ils doivent avoir un connexion ssh pour le user qui est utilisé par ansible et qui doit pouvoir étendre ses droits

### inventory
>Il s'agit de l'inventaire des machines en format fichier yaml (ou ini).

C'est aussi les fichiers de variables attachées à ces machines (`host_vars` ou`group_vars`)
Ce fichier peut être statique ou dynamiques (à partir de script qui tapent sur des API)
Il peut utiliser des pattern pour les nomenclatures de serveurs, on peut donc utiliser des regex pour les définir.

### groupes
>Dans 1 inventaire on a des machines qu'on peut "typer". On peut donc regrouper tous les serveurs de même type.

Cela se défini avec les groupes **parent/enfant**.
Le parent le plus "haut" est `all`

### group_vars
> Ce sont les répertoires dans lesquels on va créer un fichier qui va renseigner les variables appartenant à un groupe.

C'est un fichier yaml.

### host_vars
Même principe que group_vars mais pour un serveur spécifiquement.
>_Remarque:_ Si les 2 variables sont présentes, `host_vars` va surcharger les données des variables renseignées dans `group_vars` spécifiques pour 1 machine.
>Ce fichier aurait 1 nom du type server1.yaml

> exemple d'inventory
  inventory.yml
  host_vars/
  group_vars/

### tasks
>Ce sont les actions ponctuelles: créer un user, un répertoire, un fichier, donner les droits....
>1 task == 1 action

C'est un fichier yaml.

### modules
Ensemble d'actions ciblées qui permettent de faire des actions.
Chacune des ces actions de modules sont utilisables par des tasks.
Chacune de ces actions de modules est utilisable par des tasks.
Ces modules sont créés par des membres de la communauté ansible.

### roles
Un role est un ensemble d'actions cohérentes.
Un role peut avoir une ou plusieurs tasks.
Un role peut ne contenir qu'une seule task puisqu'il doit être le plus générique possible: peut être réutilisé
Chaque role doit être le plus indépendant les 1 des autres pour pouvoir être réutilisé.
Il est important de versionner ces roles.

### playbooks
C'est le fichier yaml qui permet la correspondance entre les inventaires, variables et roles pour lancer les actions.

### plugins
Permet d'augmenter la capacité d'ansible.
C'est utilisé rarement.

## INSTALLATION

Tout est bien expliqué dans le documentation ansible sur le chapitre installation.
Il existe plusieurs méthodes:
- en utilisant les dépots
- en utilisant pip

En utilisant pip on peut choisir la version d'ansible. Si rien n'est définit, ce sera la version latest qui sera installée.

```bash
pip3 install ansible
# se re connecter au serveur pour sourcer tous les fichiers installés

ansible --version
# permet d'afficher la version installer et de confirmer qu'il est bien installé
```

> _Remarque_: Il faut python installé sur le serveur cible. Si ce n'est pas le cas, on peut l'installé à distance `ansible <hostCible> --becomme -m raw -a "yum install -y python3"`
> `-m raw` pour le module raw qui est le seul module d'ansible qui n'utilise pas python

## ssh
On va générer une paire de clé pour pouvoir utiliser ssh avec ansible.

```bash
# sur le serveur control node (qui porte ansible)
ssh-keygen -t rsa -b 4096 -C "clé ssh ansible" -f ansible
## cela nous génère une paire de clé ansible et ansible.pub

# mettre les bons droits sur les clés
chmod 600 ansible ansible.pub

# on copie la clé ansible.pub sur les serveurs cibles dans authorized_keys
ssh-copy-id <user>@<ipCible>

## on peut renforcer la sécurité dans authorized_keys en rajoutant avant la clé le serveur, le user, ....
vi authorized_keys
########
from "<ipControlNode>" user "<nomUserProprietaireCle" <clePub>
#######

# faire la 1ere connexion du serveur ansible vers le serveur cible (pour stocker le fingerprint)
ssh -i ~/.ssh/ansible <user>@<serveurCible>
## une autre méthode (meilleure) est d'utiliser un agent ssh

# vérifier si un agent ssh est actif
ssh-add -l
Could not open a connection to your athentification agent
#s'il n'y en a pas d'actif on en active 1
eval 'ssh-agent'
Agent pid 11709

##on vérifie que l'agent est actif et s'il a une clé embarquée
ssh-add -l
The agent has no identities

# on ajoute la clé à l'agent
shh-add
Enter passphrase to /home/<user>/.ssh/ansible:

## on re vérifie tout
ssh-add -l
xxx
# une partie de la clé est affichée, la connexion peut se faire sans avoir à retaper le mot de passe à chaque fois

# connexion au serveur cible
ssh <user>@<serveurCible>
## la connexion est faite
```
> _Remarque_: on peut aussi utiliser le fichier .ssh/config pour ajouter le serveur Cible avec le user qui se connecte

Il nous reste à vérifier maintenant que ansible peut se connecter au serveur cible

```bash
ansible –i <inventaire> -u <userAnsible> all –m ping
SUCCESS
...
```

## CONFIGURATION ANSIBLE

### ansible.cfg
> Le fichier de configuration de ansible est le fichier `ansible.cfg`

On peut utiliser la variable d'environnement `ANSIBLE_CONFIG` à partir de laquelle on va pouvoir définir l'emplacement à partir duquel `ansible.cfg` va être chargé.
Par défaut ce fichier est au plus proche du fichier playbook.

>exemple

```bash
[defaults]
host_key_checking = false
retry_files_enabled = false
roles_path = ./roles
private_key_file = ./sshkey
forks = 20
force_color = 1
any_errors_fatal = true

ansible_managed = Ansible managed

[ssh_connection]
ssh_args = -C -o GSSAPIAuthentication=no -o GSSAPIKeyExchange=no -o GSSAPIRenewalForcesRekey=no -o ControlMaster=auto -o ControlPersist=60s
fact_caching = memory
fact_caching_connection = /tmp
pipelining = true
```

### ansible-config
`ansible-config` est un binaire qui prend  3 arguments

- list
- dump
- view
```bash
ansible-config view
## permet de voir le ansible.cfg qui est pris en compte

ansible-config list
## permet de voir toutes les variables et leurs valeurs

ansible-config dump
## permet de lister toutes les variables ansible
```

Avec ses configurations, il est possible de
* ne pas avoir la demande de fingerprints avec "yes" à renseigner

```bash
[defaults]
host_key_checking = false
```

* avoir la durée du run de chaque task
```bash
[defaults]
callback_whitelist = profile_tasks
```

* utiliser le pipelining (qui permet de gagner du temps dans l'exécution d'ansible)
```bash
[defaults]
pipelining = True
```
>_Remarque_: sans ceci il va y avoir toutes ces étapes supplémentaires:  création de directory; envoi de fichier python via sftp/scp; run python

* ajouter des configuration au ssh args
```bash
[defaults]
ssh_args = -o ControlMaster=auto -o ControlPersis=60s
```
> On partage plusieurs connexions sur la même session et on augmente à 60s la persistance de la connexion

```bash
[defaults]
ssh_args = -o ControlMaster=auto -o ControlPersis=60s -o PreferredAuthentications=publickey
```
> on dit en plus que la connexion se fera par la publicKey par défaut

* parraléliser les installations ansibles sur les serveurs

```bash
[defaults]
forks = 30
```
> on va faire les install sur 30 serveurs à la fois

* supprimer les gather_facts
```bash
[defaults]
gather_facts= no
```
> cela permet de gagner du temps car beaucoup d'infos (environ 200) ne vont pas être cherchées pour lancer le playbook
>**/!\\** si on met "no" nous n'en récupérons aucun, or nous en avons besoin de certains suivants les actions

```bash
[defaults]
fact_caching = jsonfile
fact_caching_timeout = 3600
fact_caching_connection = /tmp/mycachedir
```
> on récupère les gather_facts 1 fois dans un fichier json pour une durée d'une heure, comme cela pas la peine de les recharger

## CLI ANSIBLE

Ces commandes sont peu utilisées à part `ansible-playbook`.
Elles sont utilisées pour faire des tests .

### ping: test de connexion
```bash
ansible -i <inventoraireFile> -u <user> -m ping
```
* option `-i` permet de donner un inventaire
* option `-u` permet de donner un user
* option `-b` permet de passer les commandes qui suivent en élévation de privilèges (comme sudo)
* option `-k` permet de demander un password
* option `-vvv` permet d'être en mode verbeux complet
* option `-K` permet une élévation de privilège à distance (sudo)
* option `-C -D` permet de faire un dry-run sur le code et de donner les modif du code (comme un diff)
* option `-e` permet de définir des variables dans la commande
* option `-f` permet de modifier le forks pour le nombre de serveurs en parallèle
* option `--one-line` permet de mettre les logs ansible sur 1 seule ligne (format json)
* option `-m` permet de passer directement des modules (ex: `-m command -a` permet de passer une commande comme `uptime` ...;`-m bdebug -a` pour le debug )

## LES MODULES DANS LA CLI ANSIBLE

```bash
#syntaxe
ansible -i <inventory> -m <module> -a "<ce que vous souhaitez>"
# exemple
ansible -i iventory -m commande uptime

# le module shell permet de faire une commande complexe (avec des pipes par exemple)
ansible -i inventory -m shell -a "ps -edf | grep php | grep dali | wc -l" --one-line
## cela va afficher le nombre de process php qui sont lancé par le user dali sur 1 seule ligne

# module raw qui fait des installations sans avoir python d'installé sur la machine cible
ansible -i inventory -b -K -m raw -a "yum install -y git"
#on va installer git de cette manière avec une élévation de privilège (`-b` et `-K`) sinon l'installation ne pourra pas se faire

# /!\ il existe les modules apt et yum pour faire des installations qui sont "mieux"
ansible -i inventory -b -K -m yum -a "name=nginx state=latest"
## cela va installer nginx dans sa dernière version

# le module service permet de modifier l'état du service
ansible -i inventory -b -K -m service -a "name=nginx state=stopped"
## le service nginx va être arrêté

# le module copy permet de copier un fichier
ansible -i inventory -m copy -a <source> <destination>

# le module fetch permet de faire l'inverse de copy, il récupère un fichier distant
ansible -i inventory -m fecth-a <source> <destination> flat=yes
## flat=yes permet d'écraser lefichier s'il existe déjà

# le module setup est lié aux gather_facts
ansible -i inventory -m setup
## appelle tous les gather_facts
ansible -i inventory -m setup -a "filter=<nomFiltre>"
## va récupérer que les gather_facts des filtres renseignés






```




## FIN
