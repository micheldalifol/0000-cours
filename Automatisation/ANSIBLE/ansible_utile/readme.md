# 

## Commandes

```bash
ansible -i inventories/<environnement>/inventory all -m ping
ansible-playbook -i inventories/<environnement>/inventory deploy.yml
```
>Remarque: pour limiter sur un ou quelques serveurs penser à l'option **--limit nomServer1,nomServer2**


