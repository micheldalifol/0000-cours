# DOCKER

=====================================================================================================
### RAPELS A CONNAITRE
=====================================================================================================
#### NAMESPACES
Les **namespaces permettent d'isoler un processus**.
Ils limitent le container à ce qu'il peur voir.

**Il existe 6 namespaces** différents qui peuvent être utilisés pour la construction de containers :
	- **pid**: 		isolation de l'espace des processus
	- **net**:		donne une stack réseau privée
	- **mount**:	table de montage privée
	- **uts**:		nom du host
	- **ipc**:		isole les communication inter processus
	- **user**: 	mapping des UID/GIS entre l'hôte et les containers
> cela permet de donner un accès root à un utilisateur sur un container sans que cet utilisateur soit root sur l'hôte
> chaque processus est visible depuis l'hôte

Les informations des namespaces sont dans `/proc/<pid>/ns`
> un namespace va dire au container ce qu'il peut voir

#### CONTROL GROUPS : cgroups
Les **cgroups limitent les ressources qu'un processus peut utiliser** (donc d'un container puisqu'1 container = 1 processus)
	- **RAM**
	- **CPU**
	- **I/O**
	- **Network**
> ces cgroups sont définis dans `/sys/fs/csgroups` et possèdent chacun leur propre arbre de processus
> un cgroups va dire au container ce qu'il peut utiliser

#### FICHIERS DE CONFIGURATION DU DAEMON DOCKER

- dans systemd (centos, redhat...):		`/etc/systemd/system/docker.service.d`
- dans system V (ubuntu...):					`/etc/docker/default`


## INSTALLATION DE DOCKER

#### Installation Linux
```bash
yum-utils
yum-config-manager
yum install -y
systemctl start docker
systemctl enable docker
docker version
docker ps -a
```

#### Installation MacOSX
```bash
brew install docker
# brew install docker-machine
brew install docker-compose
docker version
docker ps -a
```

=====================================================================================================
### PARTIE SPECIFIQUE DOCKER
=====================================================================================================

## IMAGES
Les tags sont très importants parce qu'ils permettent de visualiser les versions (et de voir les modifications ou corrections apportées)
	`docker tag <imageSource>:<version> <imageDest>:<version>`

>_Remarque_: **lorsque la dernière version d'une image est créée, mentionner les 2 tags**: le numéro de version ET "latest" (sinon docker ne trouvera pas la version latest)
	`docker tag monImage:v2.3 monImage:latest`

> comme ça la version 2.3 de l'image "monimage" est aussi tagguée "latest", elle est reconnue par docker lors des `docker run|pull`

Pour pouvoir récupérer et pousser des images à partir du docker hub (ou registry privé), il faut s'y connecter.
`docker login`															connexion par défaut au docker hub
`docker push <nomImage>:<version>`					upload la version de l'image sur le docker hub
`docker pull <nomImage>:<version>`					download la version de l'image sur le docker hub

`docker login <cheminRegistry>`						connexion au registry souhaité
`docker tag <imageSource>:<version> <registry><nomImage>:<version>`
> si on ne fait pas cette action de "tagguer" la version de l'image sur le registry distant, on ne pourra pas faire d'upload de cette image

Il est conseillé de faire un commit avant pour avoir des détails sur la version
`docker commit -m "commentaire de la version" <IDContainer> <registry><nomImage>:<version>`		
`docker push <registry><nomImage>:<version>`		upload en local la version de l'image dans le registry souhaité
`docker pull <registry><nomImage>:<version>`		download en local la version de l'image dans le registry souhaité

> exemples: à partir de gitlab
	`docker login registry.gitlab.com`				connexion à gitlab
	sur la doc de notre gitlab (ou autre), les commandes `docker push` et `docker pull` sont définies

## CREER UN REGISTRY EN LOCAL
[documentation docker regitry](https://docs.docker.com/registry/deploying/#running-on-localhost)


```docker
## on crée une registry en local
docker run -d -p 5000:5000 --restart=always --name mdali_registry registry:2

## on crée une image sur cette registry
docker tag 28066e06c5ab localhost:5000/mynode_dockerfile

## on push sur la registry qu'on vient de créer
docker push localhost:5000/mynode_dockerfile
### si on a dans la réponse un sha1 c'est que c'est OK

## pour vérifier, une fois que l'image est supprimée en local on peut faire un pull pour vérifier que tout est OK
docker pull localhost:5000/mynode_dockerfile
```

## COMMANDES DOCKER
|Action  | Syntaxe | Commande | Remarque |
|:-------- | :-----: | :-----: | -----: |
| `run`|				`docker run`|			`docker run alpine`	|				création d'un container|
| `ls`|					`docker ls`	|			`docker ls`		|					liste les containers |
| `ìnspect`|				`docker inspect`	|	`docker inspect monImage`	|		affiche les information de l'image |
| `logs`|				`docker logs`	|					|						affiche les logs du container|
| `exec`|					`docker exec`	|			|								lancement d'un processus (ou connexion à un container up en background)|
| `stop|start`|				`docker stop`	|				|							arrêt (relance) d'un container sans le supprimer|
| `rm`|					`docker rm`			|				|						suppression d'un container |
| `pull`	|				`docker pull`		|		`docker pull <nomImage>:<version>`					|			récupère 1 image à partir d'un regitry|
| `push`	|				`docker push`		|		`docker push <nomImage>:<version>`		|						pousse 1 image à partir d'un regitry |
| `commit`		|			`docker commit`		|		`docker commit -m "commentaire de la version" <IDContainer> <registry><nomImage>:<version>`		|						permet de faire un commit de l'image en l'état pour créer sa propre image (ex nomme les versions) |
| `history`		|			`docker history`	|			|							permet d'afficher toutes les couches avec les actions de ces couches d'une image |
| `run`|				`docker run`|			`docker run alpine`	|				création d'un container|
| option de port `-p` | 		`docker <commandeDocker> -p <portHote>:<portContainer>`| | |
| option `-d`	|			`docker run -d <nomContainer>`	| |							lance un container en backgroup|
| option `-ti`	|			`docker run -ti <nomContainer>`		| |						ouvre un terminal en interactif du container |
| option `--name`		|	`docker run -name <nomContainer> <imageCont>`	| |			ajoute un nom "clair" au container créé |
| option `--hostname`	|	`docker run --hostname <nomHostname> <imageCont>`	| |		ajoute un nom "clair" au hostname du container  |
|`docker system prune`	| | |																supprime TOUT ce qui est inactif (container, network, volume, image) |
| `docker cp`  | `docker cp <nomContainer><chemin><fichierACopier> <cheminDestinationFichier>` | `docker cp container1:/opt/toto/test.req .` | copie en local le fichier qui est sur le container |

## DOCKERFILE
Les options les plus utilisées de dockerfile
- **FROM**								indique l'image à partir de on crée notre image
- **MAINTAINER**					(facultatif) auteur de l'image créée
- **RUN**									exécute une commande dans l'image (souvent des updates)
- **EXPOSE**							port de l'application
- **COPY**								copie des ressources depuis la machine locale vers l'image
- **ENTRYPOINT**					défini le point d'entrée de l'application (processus maitre)
- **CMD**									commande (arguments) exécutée lors de l'instanciation de l'image
- **VOLUME**							définition d'un volume (en dehors de l'image: permet d'avoir la persistance/partage de données)
- **WORKDIR**							répertoire de travail de l'image
- **ENV**									création de variables d'environnement

### EXEMPLE DE FICHIER DOCKERFILE
Cas de création d'un container node à partir d'une image node et de 2 fichiers de configuration:`server.js` et `package.json`

```dockerfile
FROM node

RUN mkdir /usr/src/app
ADD ["./server.js","./package.json", "/usr/src/app"]
WORKDIR /usr/src/app
RUN npm -y install
EXPOSE 8080
ENTRYPOINT npm start
```
> Remarque: la virgule après le dernier fichier à ajouter dans ADD est nécessaire

Pour créer ce serveur il faut lancer la commande (une fois dans le répertoire où se trouvent le fichier Dockerfile et les 2 fichiers de config)
```bash
# pour construire l'image souhaitée
docker build -f ./Dockerfile  --tag mynode_dockerfile .

# pour vérifier que l'image a bien été créée
docker images
REPOSITORY          TAG       IMAGE ID       CREATED          SIZE
mynode_dockerfile   latest    28066e06c5ab   22 minutes ago   1GB

# pour executer le container à partir de l'image créée sur le port 8080
docker run -d -p 8080:8080 mynode_dockerfile
```

## EXPORTER SON IMAGE DUR DOCKER-HUB
`docker save -o <image>.tar <image>`	[doc officielle docker save](https://docs.docker.com/engine/reference/commandline/save/)

## CHARGER UNE IMAGE IMPOTER EN TANT QU'ARCHIVE
`docker load -i <cheminImage>.tar`		[doc officielle docker load](https://docs.docker.com/engine/reference/commandline/load/)


## VOLUME
Les volumes peuvent être des répertoires persistants. Pour cela on crée un volume de 2 manières:
* avec la commande `docker volume`			[doc officielle docker volume](https://docs.docker.com/engine/reference/commandline/volume_create/)
* dans 1 fichier `docker-compose.yml`

#### syntaxe
`docker run -v <host_path>:<container_path> <nomContainer>`


### EXEMPLE DE CREATION DE VOLUME


```docker
docker volume create redis-stockage
redis-stockage

docker volume ls
DRIVER    VOLUME NAME
local     2efc7a04d1d19b7f2c6cc2fde9f6c207bc7187afd6dfc63848eab597f672475d
local     redis-stockage

docker run --net=formation -v redis-stockage:/data --name redis -d redis redis-server --appendonly yes
539136dd7796a69b25a3baadf8f954e868206925b55e619d645a3852630de6f6


docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS      NAMES
539136dd7796   redis     "docker-entrypoint.s…"   9 seconds ago   Up 5 seconds   6379/tcp   redis


docker run -it -v redis-volume:/montestvolumepartage ubuntu bash
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
405f018f9d1d: Pull complete
Digest: sha256:b6b83d3c331794420340093eb706a6f152d9c1fa51b262d9bf34594887c2c7ac
Status: Downloaded newer image for ubuntu:latest


root@645090713d50:/# ls -la montestvolumepartage/
total 8
drwxr-xr-x 2 root root 4096 Jul  5 08:04 .
drwxr-xr-x 1 root root 4096 Jul  5 08:04 ..


docker volume inspect redis-volume
[
   {
       "CreatedAt": "2022-07-05T08:04:32Z",
       "Driver": "local",
       "Labels": null,
       "Mountpoint": "/var/lib/docker/volumes/redis-volume/_data",
       "Name": "redis-volume",
       "Options": null,
       "Scope": "local"
   }
]
```



## RESEAU
Pour que des containers puissent communiquer entre eux ils doivent appartenir au même reseau.
Pour cela il faut créer le reseau:
* avec la commande `docker network`		[doc officielle docker network](https://docs.docker.com/engine/reference/commandline/network/)
* dans 1 fichier `docker-compose.yml`

### EXEMPLE DE CREATION DE RESEAU
On crée un reseau _formation_ dans lequel on va ensuite ajouter les containers à partir de l'image créée avec le dockerfile

```docker
docker network ls
--------------------------
NETWORK ID     NAME      DRIVER    SCOPE
2a2745a04293   bridge    bridge    local
749fae787d7e   host      host      local
468b08afb5ad   none      null      local
--------------------------

## on crée un nouveau reseau
docker network create formation

docker network ls
--------------------------
NETWORK ID     NAME      DRIVER    SCOPE
2a2745a04293   bridge    bridge    local
749fae787d7e   host      host      local
468b08afb5ad   none      null      local
983712aef0cd   formation host      local
--------------------------

## on ajoute les containers au reseau avec l'option --net=formation

docker run -d  --name node1 --net=formation mynode_dockerfile
docker run -d  --name node2 --net=formation mynode_dockerfile
```


## DOCKER COMPOSE

Un fichier `docker-compose.yml` permet de créer plusieurs services, volumes, reseau dans 1 seul fichier

### EXEMPLE DE FICHIER DOCKER-COMPOSE.yml
Cas de création d'un cluster swarm d'une stack elk à partir d'un fichier `elk.yml`

```yml

version: '3.9'

services:
  elasticsearch:
    image: elasticsearch:8.3.1              # docker service create --name=elasticsearch --network=logging -p 9200:9200 -p 9300:9300  -e "xpack.security.enabled=false" -e "xpack.security.http.ssl.enabled=false" -e "xpack.security.transport.ssl.enabled=false" -e "cluster.initial_master_nodes=elasticsearch" -e "node.name=elasticsearch" elasticsearch:8.3.1
    ports:
      - "9200:9200"
      - "9300:9300"
    restart: always
    environment:
      xpack.security.enabled: "false"
      xpack.security.http.ssl.enabled: "false"
      xpack.security.transport.ssl.enabled: "false"
      cluster.initial_master_nodes: "elasticsearch"
      node.name: "elasticsearch"
    volumes:
      - elasticsearch-data-volume:/usr/share/elasticsearch/data
    networks:
      - logging

  kibana:       # docker create service --name=kibana --network=logging -p 5601:5601 -e "ELASTICSEARCH_HOSTS: http://elasticsearch:9200" kibana:8.3.1
    image: kibana:8.3.1
    restart: always
    environment:
      SERVER_NAME: kibana
      ELASTICSEARCH_HOSTS: "http://elasticsearch:9200"
    ports:
      - "5601:5601"
    depends_on:
      - elasticsearch
    networks:
      - logging

  logstash:
    image: logstash:8.3.1
    restart: always
    depends_on:
      - elasticsearch
    # placement:
      # constraints:
        # - node.labels.role == manager
    command: |
      -e '
      input {
        gelf { }
        heartbeat { }
      }
      filter {
        ruby {
          code => "
            event.to_hash.keys.each { |k| event[ k.gsub('"'.'"','"'_'"') ] = event.remove(k) if k.include?'"'.'"' }
          "
        }
      }
      output {
        elasticsearch {
          hosts => ["elasticsearch:9200"]
        }
        stdout {
          codec => rubydebug
        }
      }'
    ports:
      - "12201:12201/udp"       # pour le receveur GELP: conteneur de test et des options --log-driver gelf --log-opt gelf- address=udp://127.0.0.1:12201
    networks:
      - logging

volumes:
  elasticsearch-data-volume:
    driver: local

networks:
  logging:
    driver: overlay     # docker network create -d overlay logging (type overlay s'étend sur plusieurs machines)
```
Pour lancer la stack (dans ce cas un docker swarm init à été fait, donc on lance les commandes pour le swarm)
`docker stack deploy -c ./elk.yml elk`

**SINON**
on lance la commande `docker-compose -f ./elk.yml up -d`


### COMMANDES DOCKER COMPOSE
|Action  | Syntaxe | Commande | Remarque |
|:-------- | :-----: | :-----: | -----: |
|`up` | `docker-compose up`  | `docker-compose -f <nomFicherDockerCompose> up -d`  | création d'une application + le "run" de l'image |
|`down` | `docker-compose down` |   `docker-compose down` | suppression d'une application (avec ses volumes utilisés si on rajoute l'option `--volumes`) |
|`start` | `docker-compose start`  |`docker-compose start` | démarrage d'une application (les containers de l'appli) |
|`stop` |  `docker-compose stop` |`docker-compose stop` | arrêt d'une application |
|`build` | `docker-compose build`  |  `docker-compose build` | construction des images des services référencées dans le fichier docker-compose.yaml  |
|`rm` | `docker-compose rm`  | `docker-compose rm`  | supprime les services|
|`pull` | `docker-compose pull`  |   | met à jour les images contenues dans le fichier docker-compose |
|`logs` | `docker-compose logs`  |   | visualisation des logs de l'application |
|`scale` | `docker-compose scale`  |   | modification du nombre de container pour un service |
|`ps` | `docker-compose ps`  | `docker-compose ps`  | liste des containers de l'application |

## DOCKER SWARM
[doc officielle docker swarm](https://docs.docker.com/engine/swarm/)

Docker swarm est le cluster de docker. Il fonctionne avec des fichiers yaml de type docker-compose.
**Il faut 2N+1 noeuds manager et des noeuds workers**.
A la création du cluster on effectue les étapes suivantes:

```docker
## une fois les machines créées, on se connecte sur 1 des machines qui aura le role de master
docker swarm init

### génére des token pour ajouter des machines en tant de managers ou workers

## pour ajouter les machines
docker swarm join --token xxx <ipMaster du cluster swarm>
```

[doc officielle docker node](https://docs.docker.com/engine/reference/commandline/node/)
Pour connaitres les noeuds des machines et leur roles
`docker node ls`

[doc officielle docker stack](https://docs.docker.com/engine/reference/commandline/stack/)
Ensuite pour lancer les stacks sur le cluster on lance la commande
`docker stack deploy -c <nomFicher>.yml <nomComposant>`

[doc officielle docker service](https://docs.docker.com/engine/reference/commandline/service/)
Pour connaitre les services et leurs états
`docker service ps`


## DOCKER MACHINE
[install docker machine](https://docker-docs.netlify.app/machine/install-machine/#install-machine-directly)

Docker machine nous permet de créer des VMs sur notre host ayant docker d'installé.

```docker
## cas d une création de machine avec docker sur windows utilisant de driver hyper-v s appelant "MySwitch"
docker-machine --debug create --driver hyperv --hyperv-virtual-switch "MySwitch" node1

## permettre de faire les commandes depuis l'hote comme si  on était sur le node1
docker-machine env node1

## affiche des commandes à effectuer
#---------------------------------------
# $Env:DOCKER_TLS_VERIFY = "1"
# $Env:DOCKER_HOST = "tcp://10.140.25.159:2376"
# $Env:DOCKER_CERT_PATH = "C:\Users\Administrateur\.docker\machine\machines\node1"
# $Env:DOCKER_MACHINE_NAME = "node1"
# $Env:COMPOSE_CONVERT_WINDOWS_PATHS = "true"
#---------------------------------------

## se connecter en ssh sur le node1
docker-machine ssh node1

# pour connaitre la liste des noeuds et leurs IPs
docker-machine ls
```

## 	SOCKET D'ECOUTE
Le socket d'acoute permet de commander docker à distance par exemple (utile avec potainer de docker swarm)

Editer/créer :
```bash
sudo vim /etc/systemd/system/docker.service.d/startup_options.conf

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
```
puis
```bash
sudo systemctl daemon-reload
sudo systemctl restart docker
```
Test

```bash
docker -H tcp://127.0.0.1:2375 ps
```

## SECURITE DES CONTAINERS
Par défaut lorsqu'on lance un container docker, l'utilisateur est root.
Le but est de changer cet utilisateur pour qu'il n'ait pas les droits admin. (lorsqu'on lance un ps sur l'hote on voit que le processus a pour propriétaire root)
Il faut utiliser la méthode [**dockremap**](https://docs.docker.com/engine/security/userns-remap/).
Cette méthode peut être utilisée sur les users et sur les groups.

Il existe un container [**docker bench**](https://hub.docker.com/r/docker/docker-bench-security/) qui permet de faire tous les tests de sécurités de containers possibles.
[doc utilisation](https://www.techrepublic.com/article/how-to-use-docker-bench-for-security-to-audit-your-container-deployments/)

## ERREURS CONTAINERS DOCKER

###systemctl not has been booted

```bash
dali@dali ~ % docker exec -ti dali_test bash
[root@acf6200f6dc7 /]# systemctl
System has not been booted with systemd as init system (PID 1). Cant operate.
Failed to connect to bus: Host is down

[root@acf6200f6dc7 /]# service
bash: service: command not found

### SOLUTION
dali@dali ~ % docker run -itd --name dali_priv --privileged centos /usr/sbin/init
8626b9ade03713369dafe42f5a885f1d37db3d11ef64713c8753ebac7b608aff
dali@dali ~ % docker exec -it dali_priv /bin/bash
[root@8626b9ade037 /]# systemctl
UNIT                                   LOAD   ACTIVE     SUB       DESCRIPTION                                            
dev-vda1.device                        loaded activating tentative /dev/vda1                                              
-.mount                                loaded active     mounted   Root Mount                                             
dev-mqueue.mount                       loaded active     mounted   POSIX Message Queue File System                        
etc-hostname.mount                     loaded active     mounted   /etc/hostname                                          
...                                 
systemd-tmpfiles-clean.timer           loaded active     waiting   Daily Cleanup of Temporary Directories                 

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.

52 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
[root@8626b9ade037 /]#
```
