# INSTALLATION CLUSTER KUBERNETES

Le but est de créer un cluster kubernetes avec 3 neouds tout en commandes avec `kubectl`, `kubeadm` et `kubelet`.

## Documentation
[Documentation Kubernetes](https://kubernetes.io/fr/docs/home/)
[Documentation kubeadm](https://kubernetes.io/fr/docs/reference/setup-tools/kubeadm/)
[Documentation kubelet](https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/)
[Documentation confluence](https://agpm.atlassian.net/wiki/spaces/TI/pages/1740080649/Kubernetes)

## Pré-requis
[Documentation officielle installation kubernetes] (https://kubernetes.io/fr/docs/setup/_print/)
[Pre-requis installation kubernetes] (https://kubernetes.io/fr/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)



## Création des serveurs
Création des serveurs via terraform.

### modification des fichiers
```bash 
vi terraform.tfvars
vi export.sh
vi ./finalize/inventories/hp/group_vars/all
vi ./finalize/inventories/hp/group_vars/database
vi ./finalize/inventories/hp/group_vars/dns
vi ./finalize/inventories/hp/group_vars/kube
vi ./finalize/inventories/hp/group_vars/mail
vi ./finalize/inventories/hp/group_vars/storage
vi ./finalize/inventories/hp/inventory
```
### commandes exécutées
```bash
## partie création des serveurs via terraform
source export.sh
sh init.sh
terraform validate
terraform plan -out plan.out
terraform apply plan.out


## une fois les fichiers dans le répertoire finalize mis à jour
### exécution du role ansible finalize pour finaliser la création des serveurs
cd finalize
ansible -i inventories/hp/inventory all -m ping
ansible-playbook -i inventories/hp/inventory finalize.yaml
ansible-playbook -i inventories/hp/inventory configure.yaml
```

## Installation




## Configuration



## Utilisation



## Upgrade



## Backup - Restore



## Troubleshooting



# FIN