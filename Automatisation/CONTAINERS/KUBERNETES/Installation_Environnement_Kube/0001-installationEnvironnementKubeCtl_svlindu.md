# INSTALLATION DE L'ENVIRONNEMENT KUBERCTL SUR SERVEUR D'INDUSTRIALISATION

## Documentation officielle
[kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)

1. Download de kubectl
   https://storage.googleapis.com/kubernetes-release/release/v1.23.7/bin/linux/amd64/kubectl

2 solutions:
- le curl fonctionne: `curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.7/bin/linux/amd64/kubectl`
- le curl ne fonctionne pas: on renseigne directement le lien qui télécharge le binaire https://storage.googleapis.com/kubernetes-release/release/v1.23.7/bin/linux/amd64/kubectl
- dans le 2e cas (curl ne fonctionne pas): on met sur daliIndus le binaire avec filezilla

2. Exécution de kubectl
```bash
(.venv) [dali@daliIndus ~]$ll | grep kube
100691293  45M -rw-r--r--  1 dali  45M Jun 29 09:25 kubectl

(.venv) [dali@daliIndus ~]$chmod +x kubectl

(.venv) [dali@daliIndus ~]$ll | grep kube
100691293  45M -rwxr-xr--  1 dali  45M Jun 29 09:25 kubectl

(.venv) [dali@daliIndus ~]$./kubectl version --client
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.7", GitCommit:"42c05a547468804b2053ecf60a3bd15560362fc2", GitTreeState:"clean", BuildDate:"2022-05-24T12:30:55Z", GoVersion:"go1.17.10", Compiler:"gc", Platform:"linux/amd64"}
```

3. Déplacer kubectl dans son PATH pour **ne pas utiliser** le compte root
```bash
(.venv) [dali@daliIndus ~]$mv kubectl .venv/bin/
(.venv) [dali@daliIndus ~]$ll .venv/bin/ | grep kube
100691293  45M -rwxr-xr-- 1 dali  45M Jun 29 09:25 kubectl
```

4. On vérifie que kubectl est toujours fonctionnel
```bash
(.venv) [dali@daliIndus ~]$kubectl version --client
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.7", GitCommit:"42c05axxxfc2", GitTreeState:"clean", BuildDate:"2022-05-24T12:30:55Z", GoVersion:"go1.17.10", Compiler:"gc", Platform:"linux/amd64"}
```

5. On crée un répertoire `.kube` dans lequel on pourra mettre toutes les config liées à kubernetes
```bash
(.venv) [dali@daliIndus ~]$mkdir .kube
```

6. On crée le fichier .kube/config à partir du mail reçu qui contient toutes les informations nécessaires pour la connexion au cluster kube
```bash
(.venv) [dali@daliIndus ~]$vi .kube/config
#-------------------------------------------
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: xxx
    server: https://kube.xx
  name: kube
contexts:
- context:
    cluster: kube
    user: dali
  name:
current-context: dali@kube
kind: Config
preferences: {}
users:
- name: dali
  user:
    token: xxx
#-------------------------------------------
```

7. On vérifie qu'on est l'accès au cluster kube
```bash
(.venv) [dali@daliIndus ~]$kubectl cluster-info
Kubernetes control plane is running at https://kube.xx

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

(.venv) [dali@daliIndus ~]$kubectl get nodes
NAME              STATUS   ROLES                  AGE   VERSION
svlkube1   Ready    control-plane,master   9d    v1.23.7
svldkube2   Ready    control-plane,master   9d    v1.23.7
svlkube3   Ready    control-plane,master   9d    v1.23.7
```
C'est OK
