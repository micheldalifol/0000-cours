```text
jmi@portable-jmi:~$ wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
--2023-01-12 17:19:36--  https://apt.releases.hashicorp.com/gpg
Résolution de apt.releases.hashicorp.com (apt.releases.hashicorp.com)… [sudo] Mot de passe de jmi : 2600:9000:2117:cc00:18:566b:ecc0:93a1, 2600:9000:2117:3600:18:566b:ecc0:93a1, 2600:9000:2117:1200:18:566b:ecc0:93a1, ...
Connexion à apt.releases.hashicorp.com (apt.releases.hashicorp.com)|2600:9000:2117:cc00:18:566b:ecc0:93a1|:443… connecté.
requête HTTP transmise, en attente de la réponse… 200 OK
Taille : 3195 (3,1K) [binary/octet-stream]
Enregistre : ‘STDOUT’

-                                         100%[===================================================================================>]   3,12K  --.-KB/s    ds 0s      

2023-01-12 17:19:36 (149 MB/s) — envoi vers sortie standard [3195/3195]

[sudo] Mot de passe de jmi : 

^�N��KS�z̕'Վxq��rU����5c<��STi����3�M��3۲�N��diȾ�즲���ǂ�W�z���L�U����&��~a=��Db�"{AK�C�o��ɱ�T�|-AF.    z���r������հ�(�͜�G`F�O�~����K��o.��h�)�6�B�ȯ�IE�á-���������S�6��<O+�:3��Q]����縚�8��娮�Y�*�ϼ�B&�m�9{���{��l`K�*Ǝ��p�^�O�QHashiCorp Security (HashiCorp Package Signing) <security+packaging@hashicorp.com>�8!�2����N���p�A���!�{^�N�

jmi@portable-jmi:~$ ls -l /usr/share/keyrings/hashicorp-archive-keyring.gpg
-rw-r--r-- 1 root root 2301 janv. 12 17:20 /usr/share/keyrings/hashicorp-archive-keyring.gpg

jmi@portable-jmi:~$  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com jammy main

jmi@portable-jmi:~$ sudo apt update && sudo apt install terraform
Atteint :1 http://security.ubuntu.com/ubuntu jammy-security InRelease
Atteint :2 http://fr.archive.ubuntu.com/ubuntu jammy InRelease                                                        
Atteint :3 http://fr.archive.ubuntu.com/ubuntu jammy-updates InRelease                                                
Réception de :4 https://apt.releases.hashicorp.com jammy InRelease [12,0 kB]                                          
Atteint :5 http://fr.archive.ubuntu.com/ubuntu jammy-backports InRelease
Réception de :6 https://apt.releases.hashicorp.com jammy/main amd64 Packages [74,4 kB]
Réception de :7 https://apt.releases.hashicorp.com jammy/main i386 Packages [23,7 kB]
110 ko réceptionnés en 1s (151 ko/s)
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances... Fait
Lecture des informations d'état... Fait      
11 paquets peuvent être mis à jour. Exécutez « apt list --upgradable » pour les voir.
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances... Fait
Lecture des informations d'état... Fait      
Les NOUVEAUX paquets suivants seront installés :
  terraform
0 mis à jour, 1 nouvellement installés, 0 à enlever et 11 non mis à jour.
Il est nécessaire de prendre 19,5 Mo dans les archives.
Après cette opération, 61,3 Mo d'espace disque supplémentaires seront utilisés.
Réception de :1 https://apt.releases.hashicorp.com jammy/main amd64 terraform amd64 1.3.7 [19,5 MB]
19,5 Mo réceptionnés en 0s (42,1 Mo/s)
Sélection du paquet terraform précédemment désélectionné.
(Lecture de la base de données... 206678 fichiers et répertoires déjà installés.)
Préparation du dépaquetage de .../terraform_1.3.7_amd64.deb ...
Dépaquetage de terraform (1.3.7) ...
Paramétrage de terraform (1.3.7) ...

jmi@portable-jmi:~$ whereis terraform
terraform: /usr/bin/terraform

jmi@portable-jmi:~$ terraform -h
Usage: terraform [global options] <subcommand> [args]

The available commands for execution are listed below.
The primary workflow commands are given first, followed by
less common or more advanced commands.

Main commands:
  init          Prepare your working directory for other commands
  validate      Check whether the configuration is valid
  plan          Show changes required by the current configuration
  apply         Create or update infrastructure
  destroy       Destroy previously-created infrastructure

All other commands:
  console       Try Terraform expressions at an interactive command prompt
  fmt           Reformat your configuration in the standard style
  force-unlock  Release a stuck lock on the current workspace
  get           Install or upgrade remote Terraform modules
  graph         Generate a Graphviz graph of the steps in an operation
  import        Associate existing infrastructure with a Terraform resource
  login         Obtain and save credentials for a remote host
  logout        Remove locally-stored credentials for a remote host
  output        Show output values from your root module
  providers     Show the providers required for this configuration
  refresh       Update the state to match remote systems
  show          Show the current state or a saved plan
  state         Advanced state management
  taint         Mark a resource instance as not fully functional
  test          Experimental support for module integration testing
  untaint       Remove the 'tainted' state from a resource instance
  version       Show the current Terraform version
  workspace     Workspace management

Global options (use these before the subcommand, if any):
  -chdir=DIR    Switch to a different working directory before executing the
                given subcommand.
  -help         Show this help output, or the help for a specified subcommand.
  -version      An alias for the "version" subcommand.

jmi@portable-jmi:~$ terraform version
Terraform v1.3.7
on linux_amd64

```