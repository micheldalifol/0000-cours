```bash
jmi@portable-jmi:~$ cd Documents/formationTerraform/
jmi@portable-jmi:~/Documents/formationTerraform$ ll
total 36
drwxrwxr-x 8 jmi jmi 4096 janv. 20 13:03 ./
drwxr-xr-x 6 jmi jmi 4096 janv. 12 20:30 ../
-rw------- 1 jmi jmi 1678 janv. 12 18:54 devops-jmi.pem
drwxr-xr-x 3 jmi jmi 4096 janv. 16 14:14 tp2/
drwxrwxr-x 3 jmi jmi 4096 janv. 17 13:22 tp3/
drwxr-xr-x 4 jmi jmi 4096 janv. 19 18:37 tp4/
drwxrwxr-x 2 jmi jmi 4096 janv. 19 17:07 tp4_aConserver/
drwxr-xr-x 4 jmi jmi 4096 janv. 19 19:04 tp5/
drwxr-xr-x 5 jmi jmi 4096 janv. 20 13:07 tp6_pcPerso/
jmi@portable-jmi:~/Documents/formationTerraform$ cd tp6_pcPerso/
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso$ ll
total 20
drwxr-xr-x 5 jmi jmi 4096 janv. 20 13:07 ./
drwxrwxr-x 8 jmi jmi 4096 janv. 20 13:03 ../
drwxrwxr-x 2 jmi jmi 4096 janv. 20 13:35 dev/
drwxrwxr-x 3 jmi jmi 4096 janv. 20 12:48 modules/
drwxrwxr-x 2 jmi jmi 4096 janv. 20 13:35 prod/
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso$ tree
.
├── dev
│   └── main.tf
├── modules
│   └── ec2module
│       ├── main.tf
│       └── variables.tf
└── prod
    └── main.tf

4 directories, 4 files
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso$ cd dev/
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ terraform init
Initializing modules...
- ec2 in ../modules/ec2module

Initializing the backend...

Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v4.51.0...
- Installed hashicorp/aws v4.51.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ terraform validate
Success! The configuration is valid.

jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ terraform plan
module.ec2.data.aws_ami.app_ami: Reading...
module.ec2.data.aws_ami.app_ami: Read complete after 1s [id=ami-0fe472d8a85bc7b0e]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.ec2.aws_eip.lb will be created
  + resource "aws_eip" "lb" {
      + allocation_id        = (known after apply)
      + association_id       = (known after apply)
      + carrier_ip           = (known after apply)
      + customer_owned_ip    = (known after apply)
      + domain               = (known after apply)
      + id                   = (known after apply)
      + instance             = (known after apply)
      + network_border_group = (known after apply)
      + network_interface    = (known after apply)
      + private_dns          = (known after apply)
      + private_ip           = (known after apply)
      + public_dns           = (known after apply)
      + public_ip            = (known after apply)
      + public_ipv4_pool     = (known after apply)
      + tags_all             = (known after apply)
      + vpc                  = true
    }

  # module.ec2.aws_instance.myec2 will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0fe472d8a85bc7b0e"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.nano"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "dev-jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "ec2-dev-jmi"
        }
      + tags_all                             = {
          + "Name" = "ec2-dev-jmi"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # module.ec2.aws_security_group.allow_ssh_http_https will be created
  + resource "aws_security_group" "allow_ssh_http_https" {
      + arn                    = (known after apply)
      + description            = "Allow http and https inbound traffic"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "TLS from VPC"
              + from_port        = 443
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 443
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "http from VPC"
              + from_port        = 80
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "ssh from VPC"
              + from_port        = 22
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "dev-jmi-sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run "terraform apply" now.
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ terraform apply
module.ec2.data.aws_ami.app_ami: Reading...
module.ec2.data.aws_ami.app_ami: Read complete after 1s [id=ami-0fe472d8a85bc7b0e]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.ec2.aws_eip.lb will be created
  + resource "aws_eip" "lb" {
      + allocation_id        = (known after apply)
      + association_id       = (known after apply)
      + carrier_ip           = (known after apply)
      + customer_owned_ip    = (known after apply)
      + domain               = (known after apply)
      + id                   = (known after apply)
      + instance             = (known after apply)
      + network_border_group = (known after apply)
      + network_interface    = (known after apply)
      + private_dns          = (known after apply)
      + private_ip           = (known after apply)
      + public_dns           = (known after apply)
      + public_ip            = (known after apply)
      + public_ipv4_pool     = (known after apply)
      + tags_all             = (known after apply)
      + vpc                  = true
    }

  # module.ec2.aws_instance.myec2 will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0fe472d8a85bc7b0e"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.nano"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "dev-jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "ec2-dev-jmi"
        }
      + tags_all                             = {
          + "Name" = "ec2-dev-jmi"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # module.ec2.aws_security_group.allow_ssh_http_https will be created
  + resource "aws_security_group" "allow_ssh_http_https" {
      + arn                    = (known after apply)
      + description            = "Allow http and https inbound traffic"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "TLS from VPC"
              + from_port        = 443
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 443
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "http from VPC"
              + from_port        = 80
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "ssh from VPC"
              + from_port        = 22
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "dev-jmi-sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.ec2.aws_security_group.allow_ssh_http_https: Creating...
module.ec2.aws_security_group.allow_ssh_http_https: Creation complete after 4s [id=sg-0701fba7c0d59e483]
module.ec2.aws_instance.myec2: Creating...
module.ec2.aws_instance.myec2: Still creating... [10s elapsed]
module.ec2.aws_instance.myec2: Provisioning with 'remote-exec'...
module.ec2.aws_instance.myec2 (remote-exec): Connecting to remote host via SSH...
module.ec2.aws_instance.myec2 (remote-exec):   Host: 3.87.162.53
module.ec2.aws_instance.myec2 (remote-exec):   User: ec2-user
module.ec2.aws_instance.myec2 (remote-exec):   Password: false
module.ec2.aws_instance.myec2 (remote-exec):   Private key: true
module.ec2.aws_instance.myec2 (remote-exec):   Certificate: false
module.ec2.aws_instance.myec2 (remote-exec):   SSH Agent: true
module.ec2.aws_instance.myec2 (remote-exec):   Checking Host Key: false
module.ec2.aws_instance.myec2 (remote-exec):   Target Platform: unix
module.ec2.aws_instance.myec2: Still creating... [20s elapsed]
module.ec2.aws_instance.myec2 (remote-exec): Connected!
module.ec2.aws_instance.myec2 (remote-exec): Topic nginx1.12 has end-of-support date of 2019-09-20
module.ec2.aws_instance.myec2 (remote-exec): Installing nginx
module.ec2.aws_instance.myec2: Still creating... [30s elapsed]
module.ec2.aws_instance.myec2 (remote-exec): Loaded plugins: extras_suggestions,
module.ec2.aws_instance.myec2 (remote-exec):               : langpacks, priorities,
module.ec2.aws_instance.myec2 (remote-exec):               : update-motd
module.ec2.aws_instance.myec2 (remote-exec): Existing lock /var/run/yum.pid: another copy is running as pid 3093.
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory :  88 M RSS (382 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:40 2023 - 00:07 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Uninterruptible, pid: 3093
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory : 170 M RSS (463 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:40 2023 - 00:09 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Running, pid: 3093
module.ec2.aws_instance.myec2 (remote-exec): Cleaning repos: amzn2-core
module.ec2.aws_instance.myec2 (remote-exec):      ...: amzn2extra-docker
module.ec2.aws_instance.myec2 (remote-exec):      ...: amzn2extra-nginx1.12
module.ec2.aws_instance.myec2 (remote-exec): 11 metadata files removed
module.ec2.aws_instance.myec2 (remote-exec): 4 sqlite files removed
module.ec2.aws_instance.myec2 (remote-exec): 0 metadata files removed
module.ec2.aws_instance.myec2 (remote-exec): Loaded plugins: extras_suggestions,
module.ec2.aws_instance.myec2 (remote-exec):               : langpacks, priorities,
module.ec2.aws_instance.myec2 (remote-exec):               : update-motd
module.ec2.aws_instance.myec2 (remote-exec): Existing lock /var/run/yum.pid: another copy is running as pid 3085.
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory :  36 M RSS (329 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:39 2023 - 00:13 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Sleeping, pid: 3085
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory :  39 M RSS (331 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:39 2023 - 00:15 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Running, pid: 3085
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory :  66 M RSS (358 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:39 2023 - 00:17 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Running, pid: 3085
module.ec2.aws_instance.myec2: Still creating... [40s elapsed]
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory : 117 M RSS (435 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:39 2023 - 00:19 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Running, pid: 3085
module.ec2.aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
module.ec2.aws_instance.myec2 (remote-exec):   The other application is: yum
module.ec2.aws_instance.myec2 (remote-exec):     Memory : 120 M RSS (435 MB VSZ)
module.ec2.aws_instance.myec2 (remote-exec):     Started: Sun Jan 22 15:13:39 2023 - 00:21 ago
module.ec2.aws_instance.myec2 (remote-exec):     State  : Uninterruptible, pid: 3085
module.ec2.aws_instance.myec2 (remote-exec): amzn2extra-nginx | 1.3 kB     00:00
module.ec2.aws_instance.myec2 (remote-exec): amzn2extra-nginx1. |  23 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): Resolving Dependencies
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-filesystem = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-all-modules = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-filesystem for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libprofiler.so.0()(64bit) for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package gperftools-libs.x86_64 0:2.6.1-1.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-all-modules.noarch 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-stream = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-mail = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-xslt-filter = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-perl = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-image-filter = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-geoip = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-filesystem.noarch 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-geoip.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-image-filter.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: gd for package: 1:nginx-mod-http-image-filter-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libgd.so.2()(64bit) for package: 1:nginx-mod-http-image-filter-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-perl.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-xslt-filter.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libxslt.so.1(LIBXML2_1.0.18)(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libxslt.so.1(LIBXML2_1.0.11)(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libxslt.so.1()(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libexslt.so.0()(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-mod-mail.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package nginx-mod-stream.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package gd.x86_64 0:2.0.35-27.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libfontconfig.so.1()(64bit) for package: gd-2.0.35-27.amzn2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libXpm.so.4()(64bit) for package: gd-2.0.35-27.amzn2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libX11.so.6()(64bit) for package: gd-2.0.35-27.amzn2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): ---> Package libxslt.x86_64 0:1.1.28-6.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package fontconfig.x86_64 0:2.13.0-4.3.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: fontpackages-filesystem for package: fontconfig-2.13.0-4.3.amzn2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: dejavu-sans-fonts for package: fontconfig-2.13.0-4.3.amzn2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): ---> Package libX11.x86_64 0:1.6.7-3.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libX11-common >= 1.6.7-3.amzn2.0.2 for package: libX11-1.6.7-3.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libxcb.so.1()(64bit) for package: libX11-1.6.7-3.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): ---> Package libXpm.x86_64 0:3.5.12-1.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package dejavu-sans-fonts.noarch 0:2.33-6.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: dejavu-fonts-common = 2.33-6.amzn2 for package: dejavu-sans-fonts-2.33-6.amzn2.noarch
module.ec2.aws_instance.myec2 (remote-exec): ---> Package fontpackages-filesystem.noarch 0:1.44-8.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package libX11-common.noarch 0:1.6.7-3.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package libxcb.x86_64 0:1.12-1.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Processing Dependency: libXau.so.6()(64bit) for package: libxcb-1.12-1.amzn2.0.2.x86_64
module.ec2.aws_instance.myec2 (remote-exec): --> Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): ---> Package dejavu-fonts-common.noarch 0:2.33-6.amzn2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): ---> Package libXau.x86_64 0:1.0.8-2.1.amzn2.0.2 will be installed
module.ec2.aws_instance.myec2 (remote-exec): --> Finished Dependency Resolution

module.ec2.aws_instance.myec2 (remote-exec): Dependencies Resolved

module.ec2.aws_instance.myec2 (remote-exec): ========================================
module.ec2.aws_instance.myec2 (remote-exec):  Package
module.ec2.aws_instance.myec2 (remote-exec):     Arch   Version
module.ec2.aws_instance.myec2 (remote-exec):              Repository            Size
module.ec2.aws_instance.myec2 (remote-exec): ========================================
module.ec2.aws_instance.myec2 (remote-exec): Installing:
module.ec2.aws_instance.myec2 (remote-exec):  nginx
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12 533 k
module.ec2.aws_instance.myec2 (remote-exec): Installing for dependencies:
module.ec2.aws_instance.myec2 (remote-exec):  dejavu-fonts-common
module.ec2.aws_instance.myec2 (remote-exec):     noarch 2.33-6.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core            64 k
module.ec2.aws_instance.myec2 (remote-exec):  dejavu-sans-fonts
module.ec2.aws_instance.myec2 (remote-exec):     noarch 2.33-6.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           1.4 M
module.ec2.aws_instance.myec2 (remote-exec):  fontconfig
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 2.13.0-4.3.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           253 k
module.ec2.aws_instance.myec2 (remote-exec):  fontpackages-filesystem
module.ec2.aws_instance.myec2 (remote-exec):     noarch 1.44-8.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core            10 k
module.ec2.aws_instance.myec2 (remote-exec):  gd x86_64 2.0.35-27.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           146 k
module.ec2.aws_instance.myec2 (remote-exec):  gperftools-libs
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 2.6.1-1.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           274 k
module.ec2.aws_instance.myec2 (remote-exec):  libX11
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1.6.7-3.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           606 k
module.ec2.aws_instance.myec2 (remote-exec):  libX11-common
module.ec2.aws_instance.myec2 (remote-exec):     noarch 1.6.7-3.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           165 k
module.ec2.aws_instance.myec2 (remote-exec):  libXau
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1.0.8-2.1.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core            29 k
module.ec2.aws_instance.myec2 (remote-exec):  libXpm
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 3.5.12-1.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core            57 k
module.ec2.aws_instance.myec2 (remote-exec):  libxcb
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1.12-1.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           216 k
module.ec2.aws_instance.myec2 (remote-exec):  libxslt
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1.1.28-6.amzn2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2-core           240 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-all-modules
module.ec2.aws_instance.myec2 (remote-exec):     noarch 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  17 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-filesystem
module.ec2.aws_instance.myec2 (remote-exec):     noarch 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  17 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-mod-http-geoip
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  24 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-mod-http-image-filter
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  27 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-mod-http-perl
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  37 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-mod-http-xslt-filter
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  26 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-mod-mail
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  55 k
module.ec2.aws_instance.myec2 (remote-exec):  nginx-mod-stream
module.ec2.aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  76 k

module.ec2.aws_instance.myec2 (remote-exec): Transaction Summary
module.ec2.aws_instance.myec2 (remote-exec): ========================================
module.ec2.aws_instance.myec2 (remote-exec): Install  1 Package (+20 Dependent packages)

module.ec2.aws_instance.myec2 (remote-exec): Total download size: 4.2 M
module.ec2.aws_instance.myec2 (remote-exec): Installed size: 14 M
module.ec2.aws_instance.myec2 (remote-exec): Downloading packages:
module.ec2.aws_instance.myec2 (remote-exec): (1/21): dejavu-fon |  64 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (2/21): dejavu-san | 1.4 MB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (3/21): fontconfig | 253 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (4/21): fontpackag |  10 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (5/21): gd-2.0.35- | 146 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (6/21): gperftools | 274 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (7/21): libX11-1.6 | 606 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (8/21): libX11-com | 165 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (9/21): libXau-1.0 |  29 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (10/21): libXpm-3. |  57 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (11/21): libxcb-1. | 216 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (12/21): libxslt-1 | 240 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (13/21): nginx-all |  17 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (14/21): nginx-fil |  17 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (15/21): nginx-mod |  24 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (16/21): nginx-1.1 | 533 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (17/21): nginx-mod |  27 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (18/21): nginx-mod |  37 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (19/21): nginx-mod |  55 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (20/21): nginx-mod |  26 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): (21/21): nginx-mod |  76 kB   00:00
module.ec2.aws_instance.myec2 (remote-exec): ----------------------------------------
module.ec2.aws_instance.myec2 (remote-exec): Total      9.5 MB/s | 4.2 MB  00:00
module.ec2.aws_instance.myec2 (remote-exec): Running transaction check
module.ec2.aws_instance.myec2 (remote-exec): Running transaction test
module.ec2.aws_instance.myec2 (remote-exec): Transaction test succeeded
module.ec2.aws_instance.myec2 (remote-exec): Running transaction
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpac [        ]  1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpac [#       ]  1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpac [##      ]  1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpac [###     ]  1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpac [#####   ]  1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpac [######  ]  1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontpackages-fil    1/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [        ]  2/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [####    ]  2/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [######  ]  2/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [####### ]  2/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu-fonts-com    2/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [        ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [#       ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [##      ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [###     ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [####    ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [#####   ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [######  ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu- [####### ]  3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : dejavu-sans-font    3/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [        ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [#       ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [##      ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [###     ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [####    ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [#####   ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [######  ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontcon [####### ]  4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : fontconfig-2.13.    4/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [        ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [#       ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [##      ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [###     ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [####    ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [#####   ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [######  ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [####### ]  5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11-common-1.    5/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ]  6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#       ]  6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [##      ]  6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####    ]  6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#####   ]  6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [######  ]  6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-filesyst    6/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXau- [        ]  7/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXau- [##      ]  7/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXau- [######  ]  7/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXau- [####### ]  7/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXau-1.0.8-2.1    7/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [        ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [#       ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [##      ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [###     ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [####    ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [#####   ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [######  ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb- [####### ]  8/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxcb-1.12-1.am    8/21
module.ec2.aws_instance.myec2: Still creating... [50s elapsed]
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [        ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [#       ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [##      ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [###     ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [####    ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [#####   ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [######  ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11- [####### ]  9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libX11-1.6.7-3.a    9/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXpm- [        ] 10/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXpm- [####    ] 10/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXpm- [#####   ] 10/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXpm- [####### ] 10/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libXpm-3.5.12-1.   10/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [        ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [#       ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [##      ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [###     ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [####    ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [#####   ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [######  ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [####### ] 11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gd-2.0.35-27.amz   11/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [        ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [#       ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [##      ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [###     ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [####    ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [#####   ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [######  ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt [####### ] 12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : libxslt-1.1.28-6   12/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [        ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [#       ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [##      ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [###     ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [####    ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [#####   ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [######  ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperfto [####### ] 13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : gperftools-libs-   13/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 14/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 14/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   14/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 15/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 15/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   15/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 16/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [###     ] 16/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 16/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   16/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 17/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [###     ] 17/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [######  ] 17/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 17/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-stre   17/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 18/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####    ] 18/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 18/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-mail   18/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#       ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [##      ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [###     ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####    ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#####   ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [######  ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-1.12.2-2   19/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 20/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 20/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   20/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 21/21
module.ec2.aws_instance.myec2 (remote-exec):   Installing : 1:nginx-all-modu   21/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http    1/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : gperftools-libs-    2/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : libxslt-1.1.28-6    3/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : dejavu-fonts-com    4/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http    5/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : dejavu-sans-font    6/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-all-modu    7/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : libXau-1.0.8-2.1    8/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : fontconfig-2.13.    9/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : fontpackages-fil   10/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : libX11-1.6.7-3.a   11/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http   12/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http   13/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-stre   14/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-mail   15/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-filesyst   16/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-1.12.2-2   17/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : libX11-common-1.   18/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : libxcb-1.12-1.am   19/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : libXpm-3.5.12-1.   20/21
module.ec2.aws_instance.myec2 (remote-exec):   Verifying  : gd-2.0.35-27.amz   21/21

module.ec2.aws_instance.myec2 (remote-exec): Installed:
module.ec2.aws_instance.myec2 (remote-exec):   nginx.x86_64 1:1.12.2-2.amzn2.0.2

module.ec2.aws_instance.myec2 (remote-exec): Dependency Installed:
module.ec2.aws_instance.myec2 (remote-exec):   dejavu-fonts-common.noarch 0:2.33-6.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   dejavu-sans-fonts.noarch 0:2.33-6.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   fontconfig.x86_64 0:2.13.0-4.3.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   fontpackages-filesystem.noarch 0:1.44-8.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   gd.x86_64 0:2.0.35-27.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   gperftools-libs.x86_64 0:2.6.1-1.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   libX11.x86_64 0:1.6.7-3.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   libX11-common.noarch 0:1.6.7-3.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   libXau.x86_64 0:1.0.8-2.1.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   libXpm.x86_64 0:3.5.12-1.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   libxcb.x86_64 0:1.12-1.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   libxslt.x86_64 0:1.1.28-6.amzn2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-all-modules.noarch 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-filesystem.noarch 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-mod-http-geoip.x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-mod-http-image-filter.x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-mod-http-perl.x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-mod-http-xslt-filter.x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-mod-mail.x86_64 1:1.12.2-2.amzn2.0.2
module.ec2.aws_instance.myec2 (remote-exec):   nginx-mod-stream.x86_64 1:1.12.2-2.amzn2.0.2

module.ec2.aws_instance.myec2 (remote-exec): Complete!
module.ec2.aws_instance.myec2 (remote-exec):   0  ansible2                 available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =2.4.2  =2.4.6  =2.8  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):   2  httpd_modules            available    [ =1.0  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):   3  memcached1.5             available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =1.5.1  =1.5.16  =1.5.17 ]
module.ec2.aws_instance.myec2 (remote-exec):   4 *nginx1.12=latest         enabled      [ =1.12.2 ]
module.ec2.aws_instance.myec2 (remote-exec):   6  postgresql10             available    [ =10  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):   9  R3.4                     available    [ =3.4.3  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  10  rust1                    available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =1.22.1  =1.26.0  =1.26.1  =1.27.2  =1.31.0  =1.38.0
module.ec2.aws_instance.myec2 (remote-exec):           =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  18  libreoffice              available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =5.0.6.2_15  =5.3.6.1  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  19  gimp                     available    [ =2.8.22 ]
module.ec2.aws_instance.myec2 (remote-exec):  20  docker=latest            enabled      \
module.ec2.aws_instance.myec2 (remote-exec):         [ =17.12.1  =18.03.1  =18.06.1  =18.09.9  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  21  mate-desktop1.x          available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =1.19.0  =1.20.0  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  22  GraphicsMagick1.3        available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =1.3.29  =1.3.32  =1.3.34  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  23  tomcat8.5                available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =8.5.31  =8.5.32  =8.5.38  =8.5.40  =8.5.42  =8.5.50
module.ec2.aws_instance.myec2 (remote-exec):           =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  24  epel                     available    [ =7.11  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  25  testing                  available    [ =1.0  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  26  ecs                      available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  27  corretto8                available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =1.8.0_192  =1.8.0_202  =1.8.0_212  =1.8.0_222  =1.8.0_232
module.ec2.aws_instance.myec2 (remote-exec):           =1.8.0_242  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  29  golang1.11               available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =1.11.3  =1.11.11  =1.11.13  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  30  squid4                   available    [ =4  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  32  lustre2.10               available    \
module.ec2.aws_instance.myec2 (remote-exec):         [ =2.10.5  =2.10.8  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  33  java-openjdk11           available    [ =11  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  34  lynis                    available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  36  BCC                      available    [ =0.x  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  37  mono                     available    [ =5.x  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  38  nginx1                   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  39  ruby2.6                  available    [ =2.6  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  40  mock                     available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  41  postgresql11             available    [ =11  =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  42  php7.4                   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  43  livepatch                available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  44  python3.8                available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  45  haproxy2                 available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  46  collectd                 available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  47  aws-nitro-enclaves-cli   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  48  R4                       available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  49  kernel-5.4               available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  50  selinux-ng               available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  51  php8.0                   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  52  tomcat9                  available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  53  unbound1.13              available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  54  mariadb10.5              available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  55  kernel-5.10              available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  56  redis6                   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  57  ruby3.0                  available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  58  postgresql12             available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  59  postgresql13             available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  60  mock2                    available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  61  dnsmasq2.85              available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  62  kernel-5.15              available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  63  postgresql14             available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  64  firefox                  available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  65  lustre                   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  66  php8.1                   available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec):  67  awscli1                  available    [ =stable ]
module.ec2.aws_instance.myec2 (remote-exec): * Extra topic has reached end of support.
module.ec2.aws_instance.myec2: Creation complete after 52s [id=i-083872e96296e6411]
module.ec2.aws_eip.lb: Creating...
module.ec2.aws_eip.lb: Provisioning with 'local-exec'...
module.ec2.aws_eip.lb (local-exec): Executing: ["/bin/sh" "-c" "echo PUBLIC IP: 54.205.80.123 ; ID: i-083872e96296e6411 ; AZ: us-east-1c; >> infos_ec2.txt"]
module.ec2.aws_eip.lb (local-exec): PUBLIC IP: 54.205.80.123
module.ec2.aws_eip.lb (local-exec): /bin/sh: 1: ID:: not found
module.ec2.aws_eip.lb (local-exec): /bin/sh: 1: AZ:: not found
module.ec2.aws_eip.lb: Creation complete after 3s [id=eipalloc-01156e72454729ea9]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ 

jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ terraform destroy
module.ec2.data.aws_ami.app_ami: Reading...
module.ec2.aws_security_group.allow_ssh_http_https: Refreshing state... [id=sg-0701fba7c0d59e483]
module.ec2.data.aws_ami.app_ami: Read complete after 1s [id=ami-0fe472d8a85bc7b0e]
module.ec2.aws_instance.myec2: Refreshing state... [id=i-083872e96296e6411]
module.ec2.aws_eip.lb: Refreshing state... [id=eipalloc-01156e72454729ea9]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # module.ec2.aws_eip.lb will be destroyed
  - resource "aws_eip" "lb" {
      - allocation_id        = "eipalloc-01156e72454729ea9" -> null
      - association_id       = "eipassoc-0bb45a0172a4e6bf7" -> null
      - domain               = "vpc" -> null
      - id                   = "eipalloc-01156e72454729ea9" -> null
      - instance             = "i-083872e96296e6411" -> null
      - network_border_group = "us-east-1" -> null
      - network_interface    = "eni-08ec43d563d0d6809" -> null
      - private_dns          = "ip-172-31-82-216.ec2.internal" -> null
      - private_ip           = "172.31.82.216" -> null
      - public_dns           = "ec2-54-205-80-123.compute-1.amazonaws.com" -> null
      - public_ip            = "54.205.80.123" -> null
      - public_ipv4_pool     = "amazon" -> null
      - tags                 = {} -> null
      - tags_all             = {} -> null
      - vpc                  = true -> null
    }

  # module.ec2.aws_instance.myec2 will be destroyed
  - resource "aws_instance" "myec2" {
      - ami                                  = "ami-0fe472d8a85bc7b0e" -> null
      - arn                                  = "arn:aws:ec2:us-east-1:026071546831:instance/i-083872e96296e6411" -> null
      - associate_public_ip_address          = true -> null
      - availability_zone                    = "us-east-1c" -> null
      - cpu_core_count                       = 1 -> null
      - cpu_threads_per_core                 = 1 -> null
      - disable_api_stop                     = false -> null
      - disable_api_termination              = false -> null
      - ebs_optimized                        = false -> null
      - get_password_data                    = false -> null
      - hibernation                          = false -> null
      - id                                   = "i-083872e96296e6411" -> null
      - instance_initiated_shutdown_behavior = "stop" -> null
      - instance_state                       = "running" -> null
      - instance_type                        = "t2.nano" -> null
      - ipv6_address_count                   = 0 -> null
      - ipv6_addresses                       = [] -> null
      - key_name                             = "devops-jmi" -> null
      - monitoring                           = false -> null
      - placement_partition_number           = 0 -> null
      - primary_network_interface_id         = "eni-08ec43d563d0d6809" -> null
      - private_dns                          = "ip-172-31-82-216.ec2.internal" -> null
      - private_ip                           = "172.31.82.216" -> null
      - public_dns                           = "ec2-54-205-80-123.compute-1.amazonaws.com" -> null
      - public_ip                            = "54.205.80.123" -> null
      - secondary_private_ips                = [] -> null
      - security_groups                      = [
          - "dev-jmi-sg",
        ] -> null
      - source_dest_check                    = true -> null
      - subnet_id                            = "subnet-09fff88dd6bad0055" -> null
      - tags                                 = {
          - "Name" = "ec2-dev-jmi"
        } -> null
      - tags_all                             = {
          - "Name" = "ec2-dev-jmi"
        } -> null
      - tenancy                              = "default" -> null
      - user_data_replace_on_change          = false -> null
      - vpc_security_group_ids               = [
          - "sg-0701fba7c0d59e483",
        ] -> null

      - capacity_reservation_specification {
          - capacity_reservation_preference = "open" -> null
        }

      - credit_specification {
          - cpu_credits = "standard" -> null
        }

      - enclave_options {
          - enabled = false -> null
        }

      - maintenance_options {
          - auto_recovery = "default" -> null
        }

      - metadata_options {
          - http_endpoint               = "enabled" -> null
          - http_put_response_hop_limit = 1 -> null
          - http_tokens                 = "optional" -> null
          - instance_metadata_tags      = "disabled" -> null
        }

      - private_dns_name_options {
          - enable_resource_name_dns_a_record    = false -> null
          - enable_resource_name_dns_aaaa_record = false -> null
          - hostname_type                        = "ip-name" -> null
        }

      - root_block_device {
          - delete_on_termination = true -> null
          - device_name           = "/dev/xvda" -> null
          - encrypted             = false -> null
          - iops                  = 100 -> null
          - tags                  = {} -> null
          - throughput            = 0 -> null
          - volume_id             = "vol-01550dd4cc0eaa52b" -> null
          - volume_size           = 8 -> null
          - volume_type           = "gp2" -> null
        }
    }

  # module.ec2.aws_security_group.allow_ssh_http_https will be destroyed
  - resource "aws_security_group" "allow_ssh_http_https" {
      - arn                    = "arn:aws:ec2:us-east-1:026071546831:security-group/sg-0701fba7c0d59e483" -> null
      - description            = "Allow http and https inbound traffic" -> null
      - egress                 = [
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = ""
              - from_port        = 0
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "-1"
              - security_groups  = []
              - self             = false
              - to_port          = 0
            },
        ] -> null
      - id                     = "sg-0701fba7c0d59e483" -> null
      - ingress                = [
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "TLS from VPC"
              - from_port        = 443
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 443
            },
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "http from VPC"
              - from_port        = 80
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 80
            },
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "ssh from VPC"
              - from_port        = 22
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 22
            },
        ] -> null
      - name                   = "dev-jmi-sg" -> null
      - owner_id               = "026071546831" -> null
      - revoke_rules_on_delete = false -> null
      - tags                   = {} -> null
      - tags_all               = {} -> null
      - vpc_id                 = "vpc-047d4f6939fac5bcf" -> null
    }

Plan: 0 to add, 0 to change, 3 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

module.ec2.aws_eip.lb: Destroying... [id=eipalloc-01156e72454729ea9]
module.ec2.aws_eip.lb: Destruction complete after 3s
module.ec2.aws_instance.myec2: Destroying... [id=i-083872e96296e6411]
module.ec2.aws_instance.myec2: Still destroying... [id=i-083872e96296e6411, 10s elapsed]
module.ec2.aws_instance.myec2: Still destroying... [id=i-083872e96296e6411, 20s elapsed]
module.ec2.aws_instance.myec2: Still destroying... [id=i-083872e96296e6411, 30s elapsed]
module.ec2.aws_instance.myec2: Destruction complete after 30s
module.ec2.aws_security_group.allow_ssh_http_https: Destroying... [id=sg-0701fba7c0d59e483]
module.ec2.aws_security_group.allow_ssh_http_https: Destruction complete after 2s

Destroy complete! Resources: 3 destroyed.
jmi@portable-jmi:~/Documents/formationTerraform/tp6_pcPerso/dev$ 


```
