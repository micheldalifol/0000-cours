```json
{
  "version": 4,
  "terraform_version": "1.3.7",
  "serial": 2,
  "lineage": "3f5696de-b0b6-49f4-3dd4-2edd4bb2e4ac",
  "outputs": {},
  "resources": [
    {
      "module": "module.ec2",
      "mode": "data",
      "type": "aws_ami",
      "name": "app_ami",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "architecture": "x86_64",
            "arn": "arn:aws:ec2:us-east-1::image/ami-0fe472d8a85bc7b0e",
            "block_device_mappings": [
              {
                "device_name": "/dev/xvda",
                "ebs": {
                  "delete_on_termination": "true",
                  "encrypted": "false",
                  "iops": "0",
                  "snapshot_id": "snap-07f96a026c914c20a",
                  "throughput": "0",
                  "volume_size": "8",
                  "volume_type": "gp2"
                },
                "no_device": "",
                "virtual_name": ""
              }
            ],
            "boot_mode": "",
            "creation_date": "2022-12-16T01:56:15.000Z",
            "deprecation_time": "2024-12-16T01:56:14.000Z",
            "description": "Amazon Linux 2 AMI 2.0.20221210.1 x86_64 HVM gp2",
            "ena_support": true,
            "executable_users": null,
            "filter": [
              {
                "name": "name",
                "values": [
                  "amzn2-ami-hvm*"
                ]
              }
            ],
            "hypervisor": "xen",
            "id": "ami-0fe472d8a85bc7b0e",
            "image_id": "ami-0fe472d8a85bc7b0e",
            "image_location": "amazon/amzn2-ami-hvm-2.0.20221210.1-x86_64-gp2",
            "image_owner_alias": "amazon",
            "image_type": "machine",
            "imds_support": "",
            "include_deprecated": false,
            "kernel_id": "",
            "most_recent": true,
            "name": "amzn2-ami-hvm-2.0.20221210.1-x86_64-gp2",
            "name_regex": null,
            "owner_id": "137112412989",
            "owners": [
              "amazon"
            ],
            "platform": "",
            "platform_details": "Linux/UNIX",
            "product_codes": [],
            "public": true,
            "ramdisk_id": "",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "root_snapshot_id": "snap-07f96a026c914c20a",
            "sriov_net_support": "simple",
            "state": "available",
            "state_reason": {
              "code": "UNSET",
              "message": "UNSET"
            },
            "tags": {},
            "timeouts": null,
            "tpm_support": "",
            "usage_operation": "RunInstances",
            "virtualization_type": "hvm"
          },
          "sensitive_attributes": []
        }
      ]
    },
    {
      "module": "module.ec2",
      "mode": "managed",
      "type": "aws_eip",
      "name": "lb",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "address": null,
            "allocation_id": "eipalloc-05fa155bd16e44068",
            "associate_with_private_ip": null,
            "association_id": "eipassoc-0f50475311f204f71",
            "carrier_ip": "",
            "customer_owned_ip": "",
            "customer_owned_ipv4_pool": "",
            "domain": "vpc",
            "id": "eipalloc-05fa155bd16e44068",
            "instance": "i-03f6c7313806adbf9",
            "network_border_group": "us-east-1",
            "network_interface": "eni-003969adef9ea8bc5",
            "private_dns": "ip-172-31-91-247.ec2.internal",
            "private_ip": "172.31.91.247",
            "public_dns": "ec2-44-209-193-197.compute-1.amazonaws.com",
            "public_ip": "44.209.193.197",
            "public_ipv4_pool": "amazon",
            "tags": null,
            "tags_all": {},
            "timeouts": null,
            "vpc": true
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiZGVsZXRlIjoxODAwMDAwMDAwMDAsInJlYWQiOjkwMDAwMDAwMDAwMCwidXBkYXRlIjozMDAwMDAwMDAwMDB9fQ==",
          "dependencies": [
            "module.ec2.aws_instance.myec2",
            "module.ec2.aws_security_group.allow_ssh_http_https",
            "module.ec2.data.aws_ami.app_ami"
          ]
        }
      ]
    },
    {
      "module": "module.ec2",
      "mode": "managed",
      "type": "aws_instance",
      "name": "myec2",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "ami": "ami-0fe472d8a85bc7b0e",
            "arn": "arn:aws:ec2:us-east-1:026071546831:instance/i-03f6c7313806adbf9",
            "associate_public_ip_address": true,
            "availability_zone": "us-east-1c",
            "capacity_reservation_specification": [
              {
                "capacity_reservation_preference": "open",
                "capacity_reservation_target": []
              }
            ],
            "cpu_core_count": 1,
            "cpu_threads_per_core": 1,
            "credit_specification": [
              {
                "cpu_credits": "standard"
              }
            ],
            "disable_api_stop": false,
            "disable_api_termination": false,
            "ebs_block_device": [],
            "ebs_optimized": false,
            "enclave_options": [
              {
                "enabled": false
              }
            ],
            "ephemeral_block_device": [],
            "get_password_data": false,
            "hibernation": false,
            "host_id": "",
            "host_resource_group_arn": null,
            "iam_instance_profile": "",
            "id": "i-03f6c7313806adbf9",
            "instance_initiated_shutdown_behavior": "stop",
            "instance_state": "running",
            "instance_type": "t2.nano",
            "ipv6_address_count": 0,
            "ipv6_addresses": [],
            "key_name": "devops-jmi",
            "launch_template": [],
            "maintenance_options": [
              {
                "auto_recovery": "default"
              }
            ],
            "metadata_options": [
              {
                "http_endpoint": "enabled",
                "http_put_response_hop_limit": 1,
                "http_tokens": "optional",
                "instance_metadata_tags": "disabled"
              }
            ],
            "monitoring": false,
            "network_interface": [],
            "outpost_arn": "",
            "password_data": "",
            "placement_group": "",
            "placement_partition_number": 0,
            "primary_network_interface_id": "eni-003969adef9ea8bc5",
            "private_dns": "ip-172-31-91-247.ec2.internal",
            "private_dns_name_options": [
              {
                "enable_resource_name_dns_a_record": false,
                "enable_resource_name_dns_aaaa_record": false,
                "hostname_type": "ip-name"
              }
            ],
            "private_ip": "172.31.91.247",
            "public_dns": "ec2-34-201-250-17.compute-1.amazonaws.com",
            "public_ip": "34.201.250.17",
            "root_block_device": [
              {
                "delete_on_termination": true,
                "device_name": "/dev/xvda",
                "encrypted": false,
                "iops": 100,
                "kms_key_id": "",
                "tags": null,
                "throughput": 0,
                "volume_id": "vol-09cdcfcdee29c236d",
                "volume_size": 8,
                "volume_type": "gp2"
              }
            ],
            "secondary_private_ips": [],
            "security_groups": [
              "prod-jmi-sg"
            ],
            "source_dest_check": true,
            "subnet_id": "subnet-09fff88dd6bad0055",
            "tags": {
              "Name": "ec2-prod-jmi"
            },
            "tags_all": {
              "Name": "ec2-prod-jmi"
            },
            "tenancy": "default",
            "timeouts": null,
            "user_data": null,
            "user_data_base64": null,
            "user_data_replace_on_change": false,
            "volume_tags": null,
            "vpc_security_group_ids": [
              "sg-0f6155d8a68cd516d"
            ]
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6MTIwMDAwMDAwMDAwMCwidXBkYXRlIjo2MDAwMDAwMDAwMDB9LCJzY2hlbWFfdmVyc2lvbiI6IjEifQ==",
          "dependencies": [
            "module.ec2.aws_security_group.allow_ssh_http_https",
            "module.ec2.data.aws_ami.app_ami"
          ]
        }
      ]
    },
    {
      "module": "module.ec2",
      "mode": "managed",
      "type": "aws_security_group",
      "name": "allow_ssh_http_https",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:us-east-1:026071546831:security-group/sg-0f6155d8a68cd516d",
            "description": "Allow http and https inbound traffic",
            "egress": [
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "",
                "from_port": 0,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "-1",
                "security_groups": [],
                "self": false,
                "to_port": 0
              }
            ],
            "id": "sg-0f6155d8a68cd516d",
            "ingress": [
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "TLS from VPC",
                "from_port": 443,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 443
              },
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "http from VPC",
                "from_port": 80,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 80
              },
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "ssh from VPC",
                "from_port": 22,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 22
              }
            ],
            "name": "prod-jmi-sg",
            "name_prefix": "",
            "owner_id": "026071546831",
            "revoke_rules_on_delete": false,
            "tags": null,
            "tags_all": {},
            "timeouts": null,
            "vpc_id": "vpc-047d4f6939fac5bcf"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6OTAwMDAwMDAwMDAwfSwic2NoZW1hX3ZlcnNpb24iOiIxIn0="
        }
      ]
    }
  ],
  "check_results": null
}
```