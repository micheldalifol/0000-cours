```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform apply
data.aws_ami.app_ami: Reading...
data.aws_ami.app_ami: Read complete after 1s [id=ami-0fe472d8a85bc7b0e]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip.lb will be created
  + resource "aws_eip" "lb" {
      + allocation_id        = (known after apply)
      + association_id       = (known after apply)
      + carrier_ip           = (known after apply)
      + customer_owned_ip    = (known after apply)
      + domain               = (known after apply)

.. / ..

              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "jmi-sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_security_group.allow_ssh_http_https: Creating...
aws_security_group.allow_ssh_http_https: Creation complete after 3s [id=sg-009de0bb1760e51be]
aws_instance.myec2: Creating...
aws_instance.myec2: Still creating... [10s elapsed]
aws_instance.myec2: Still creating... [20s elapsed]
aws_instance.myec2: Still creating... [30s elapsed]
aws_instance.myec2: Provisioning with 'local-exec'...
aws_instance.myec2 (local-exec): Executing: ["/bin/sh" "-c" "echo PUBLIC_IP: 18.209.179.123 ID: i-01defc405d9496062 AZ: us-east-1e >> ./infos_ec2.txt"]
aws_instance.myec2: Provisioning with 'remote-exec'...
aws_instance.myec2 (remote-exec): Connecting to remote host via SSH...
aws_instance.myec2 (remote-exec):   Host: 18.209.179.123
aws_instance.myec2 (remote-exec):   User: ec2-user
aws_instance.myec2 (remote-exec):   Password: false
aws_instance.myec2 (remote-exec):   Private key: true
aws_instance.myec2 (remote-exec):   Certificate: false
aws_instance.myec2 (remote-exec):   SSH Agent: false
aws_instance.myec2 (remote-exec):   Checking Host Key: false
aws_instance.myec2 (remote-exec):   Target Platform: unix
aws_instance.myec2: Still creating... [40s elapsed]
aws_instance.myec2 (remote-exec): Connecting to remote host via SSH...
aws_instance.myec2 (remote-exec):   Host: 18.209.179.123
aws_instance.myec2 (remote-exec):   User: ec2-user
aws_instance.myec2 (remote-exec):   Password: false
aws_instance.myec2 (remote-exec):   Private key: true
aws_instance.myec2 (remote-exec):   Certificate: false
aws_instance.myec2 (remote-exec):   SSH Agent: false
aws_instance.myec2 (remote-exec):   Checking Host Key: false
aws_instance.myec2 (remote-exec):   Target Platform: unix
aws_instance.myec2: Still creating... [50s elapsed]
aws_instance.myec2: Still creating... [1m0s elapsed]
aws_instance.myec2 (remote-exec): Connecting to remote host via SSH...
aws_instance.myec2 (remote-exec):   Host: 18.209.179.123
aws_instance.myec2 (remote-exec):   User: ec2-user
aws_instance.myec2 (remote-exec):   Password: false
aws_instance.myec2 (remote-exec):   Private key: true
aws_instance.myec2 (remote-exec):   Certificate: false
aws_instance.myec2 (remote-exec):   SSH Agent: false
aws_instance.myec2 (remote-exec):   Checking Host Key: false
aws_instance.myec2 (remote-exec):   Target Platform: unix
aws_instance.myec2: Still creating... [1m10s elapsed]

.. / ..
```