# <span style="color: #CE5D6B"> **Formation Terraform : formation _Udemy_ Dirane Taffen _Terraform: Les bases indispensables [2023]_** </span>

<span style="color: #CE5D6B">

## **MEMO** 
</span>

```text
insertFichierLog  : [Définition des termes Kubernetes](./definition_termes_kubernetes.md)  
sommaire          : [Resume des commandes](./keycloak.md#resume-des-commandes)
siteReference     : https://xavki.blog/terraform-formation-francais/#7gtzumVHZtE
```

<span style="color: #CE5D6B">

## **CONNEXION SITE UDEMY**
</span>

1. [Informations de connexion à Udemy](./fichiersSecurises/infosConnexionUdemy.md)  

## Historique  

Terraform voit le jour en 2014. Il s’agit d’un environnement open source d'Infrastructure as Code (IaC). Edité par l'américain HashiCorp, il a pour finalité d'automatiser le provisioning et le management de n'importe quelle infrastructure cloud ou ressources IT.

En amont, les opérations à réaliser seront codées dans un langage ad hoc propre à Terraform et baptisé HashiCorp Configuration Language (HCL). Une fois créés, les fichiers de configuration HCL seront exécutés via l'interface en lignes de commande Terraform CLI. L'ensemble des tâches spécifiées dans le fichier seront alors appliquées. Il pourra s'agit de déployer ou paramétrer des serveurs physiques (bare metal), des machines virtuelles, des containers logiciels, des clusters, ou encore des équipements réseau. Aux côtés du langage HCL, Terraform prend également en charge les fichiers de configuration au format Json.

## Avantages Infrastructure as Code (IaC)  

- Réutilisation
- Evolutivité
- Collaboration

## Avantages Terraform

- Multi-Cloud (de multiple providers)  
- Gratuit  
- Langage facile à lire  
- Extensible  
- S’intégre avec les outils de Configuration Management  
- Multi OS  

## TP0 : création compte AWS gratuit  

### TP0 - Avertissement

J'ai un peu galéré pour créer ce compte car l'interface a changé entre le moment où la formation a été écrite et la création.  
De ce fait la création "step by step" est absente de ce tutorial.  

### TP0 - Connexion compte AWS  

[https://signin.aws.amazon.com/](http://)  /!\ choisir _Utilisateur racine /!\ **qui ne sera pas l'utilisateur technique**

![image](./photosFormationTerraform/102_connexionAws.png)  

pwd :  « classique2PourCeTypeDeSite »  

![image](./photosFormationTerraform/103_connexionAws.png)  

MFA => google authenticator sur mobile

![image](./photosFormationTerraform/104_connexionAws.png)  

### TP0 - Détail compte  

![image](./photosFormationTerraform/99_awsDetailCompte.png)  

![image](./photosFormationTerraform/100_awsDetailCompte.png)  

### TP0 - Détail facturation  

Il est obligatoire de saisir les références d'une CB perso pour activer  

![image](./photosFormationTerraform/101_preferencesPaiement.png)  

### TP0 - Création d'un compte **utilisateur** avec droits _full admin_ pour déployer les ressources  

![image](./photosFormationTerraform/105_creationUserAvecDroitsAdmin.png)  

![image](./photosFormationTerraform/106_creationUserAvecDroitsAdmin.png)

![image](./photosFormationTerraform/107_creationUserAvecDroitsAdmin.png)  

![image](./photosFormationTerraform/108_creationUserAvecDroitsAdmin.png)  

### TP0 - Connexion avec user _jmi_

[https://026071546831.signin.aws.amazon.com/console](https://)  

[Login + mdp de connexion](./fichiersSecurises/infosConnexionAwsUserJmi.md)  


![image](./photosFormationTerraform/109_creationUserAvecDroitsAdmin.png)  

![image](./photosFormationTerraform/110_creationUserAvecDroitsAdmin.png)  

### TP0 - Creation des cles de connexion accessKey + secretKey

![image](./photosFormationTerraform/111_creationCles.png)  

![image](./photosFormationTerraform/112_idEtSecret.png)

[accessKey + secretKey](./fichiersSecurises/accessKeyEtSecretKey.md)  

![image](./photosFormationTerraform/113_dashboardUserAvecCles.png)  

### TP0 - Recherche EC2  

![image](./photosFormationTerraform/114_rechercheEC2.png)  

Cliquer sur n'importe quel item dans ce "pave" permet d'acceder au dashboard "New EC2 Experience"

### TP0 - Creation d'une paire de clés

![image](./photosFormationTerraform/115_creationKeyPair.png)  

![image](./photosFormationTerraform/116_parametresKeyPair.png)  

![image](./photosFormationTerraform/117_visuKeyPair.png)  

![image](./photosFormationTerraform/118_visuKeyPair_pem.png)  

## TP1 - installation de terraform (sur linux centOS7 / environnement pro)  

### TP1 - Installation sur serveur pro

- Download
- Copie en local
- Ajout au $PATH
- Rendre executable

```text
COMMANDES
---------

echo $PATH
export http{,s}_proxy=proxylinuxinternet.agpm.adm:3128
wget https://releases.hashicorp.com/terraform/1.3.5/terraform_1.3.5_linux_amd64.zip
unzip terraform_1.3.5_linux_amd64.zip
mv terraform /data/adm5224opit/.venv/bin/
chmod u+x /data/adm5224opit/.venv/bin/terraform
whereis terraform
terraform version
```

[log TP1 Installation sur serveur pro](./logs/TP1_installationTerraformServeurPro.md)  

### TP1 bis - Installation de terraform sur pc perso  

#### Commandes d'installation

```text
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list

sudo apt update && sudo apt install terraform
```

### TP1 bis - Logs d'installation

[log TP1-bis Installation sur serveur perso](./logs/TP1-bis_installationTerraformServeurPerso.md)  

## Definitions  

- provider  : plugin qui permet de communiquer avec la plateforme à provisionner (aws, azure, vSphere, etc)  
- ressource : type d'objet à créer sur la plateforme à provisionner  

## TP2 - deploiement 1ere ressource sur AWS  

- Récupérez le secret et access key de votre compte (dans les paramètres sécurité de votre compte dans IAM)
- Créez un paire de clé dans EC2 et nommez cette clé devops **_votre prenom_**, un fichier devops **_votre prenom.pem_** sera téléchargé (conservez la jalousement)
- Créez une fichier ec2.tf dans un répertoire nommé tp 2
- Renseignez les informations permettant de créer une VM avec l’image centos suivante : centos7 minimal v20190919.0.0 (ami0083662ba17882949)
- ATTENTION nous travaillerons uniquement dans la region US East (N. Virginia) **us-east-1** dans toute cette formation
- Vérifiez que votre instance est bien créée et observez le contenu de fichier tfstate
- Modifiez le fichier ec2.tf afin d’y inclure le tag de votre instance : “Name: ec2 votre prenom
- Appliquez la modification et constatez les changement apportées ainsi que dans le fichier tfstate
- Supprimez votre ec2

### TP2 - Création 1ere ressource : vm EC2  

#### TP2 - definition terraform : registry terraform  

On va chercher les noms et definitions de chaque objet dans la registry du provider sur Terraform.

[https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance](https://)  

![image](./photosFormationTerraform/126_registryTerraform.png)  

#### TP2 - ressource.tf : creation fichier ressource  

```text
                                            => section declarative : plugin qui permet de discuter avec l'API
provider "aws" {                            =>      provider
  region     = "us-east-1"                  =>      region AWS
  access_key = "xxxxxxxxxxxxxxxxxxxx"       =>      access_key
  secret_key = "xxxxxxxxxxxxxxxxxxxx"       =>      secret_key
}
                                            => section declarative : ressource
resource "aws_instance" "myec2" {           =>      type de ressource
                                                    nom de ressource  
  ami = "ami-0083662ba17882949"             => nomImage aws
  instance_type = "t2.micro"                => type de l'instance aws
  key_name = "devops-jmi"                   => nom de la cle
}
```
  
Une fois le fichier créé, on lance la commande **terraform init** pour que terraform telecharge le plugin du provider defini dans le fichier ec2.tf (ici "aws")

#### TP2 - terraform init : installation du plugin  

Il est nécessaire d'avoir une connexion avec les flux ouverts vers internet

```text
jmi@portable-jmi:./tp2$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v4.49.0...
- Installed hashicorp/aws v4.49.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```  

Lancer la commande **terraform plan** pour la suite

```text
jmi@portable-jmi:./tp2$ terraform plan
╷
│ Error: Failed to load plugin schemas
│ 
│ Error while loading schemas for plugin components: Failed to obtain provider schema: Could not load the schema for
│ provider registry.terraform.io/hashicorp/aws: failed to instantiate provider "registry.terraform.io/hashicorp/aws" to
│ obtain schema: fork/exec
│ .terraform/providers/registry.terraform.io/hashicorp/aws/4.49.0/linux_amd64/terraform-provider-aws_v4.49.0_x5:
│ permission denied..
╵
jmi@portable-jmi:./tp2$ 
```

Cette erreur est due au fait que j'ai cree les scripts sur la cle USB qui est en "noexec"

Copie dans un fs "exec"

```text
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ ls -al
total 12
drwxr-xr-x 2 jmi jmi 4096 janv. 12 20:31 .
drwxrwxr-x 3 jmi jmi 4096 janv. 12 20:30 ..
-rw-r--r-- 1 jmi jmi  377 janv. 12 20:25 ec2.tf
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/aws versions matching "4.49.0"...
- Installing hashicorp/aws v4.49.0...
- Installed hashicorp/aws v4.49.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

#### TP2 - terraform plan : visualisation du plan d'execution

```text
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.myec2 will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = (known after apply)
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags_all                             = (known after apply)
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run "terraform apply" now.
jmi@portable-jmi:~/Documents/formationTerraform/tp2$
```

#### TP2 - terraform apply  : execution du plan

```text
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ terraform apply

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.myec2 will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = (known after apply)
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags_all                             = (known after apply)
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_instance.myec2: Creating...
aws_instance.myec2: Still creating... [10s elapsed]
aws_instance.myec2: Still creating... [20s elapsed]
aws_instance.myec2: Still creating... [30s elapsed]
aws_instance.myec2: Creation complete after 34s [id=i-0dc1656d35dcb8c91]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ 
```
  
#### TP2 - visualisation de l'instance sur la plateforme AWS  

![image](./photosFormationTerraform/119_creationInstanceEC2.png)

![image](./photosFormationTerraform/120_visuInstancePlateformeAWS.png)

![image](./photosFormationTerraform/121_detailInstanceEC2.png)

#### TP2 - modification instance : ajout tag

```text
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  access_key = "xxxxxxxxxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxxx"
}

resource "aws_instance" "myec2" {
  ami = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  key_name = "devops-jmi"
  tags = {
    name = "ec2-jmi"
  }
}
```

#### TP2 - Formattage de fichier avec terraform  

Fichier avant

```text
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  access_key = "xxxxxxxxxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxxx"
}

resource "aws_instance" "myec2" {
  ami = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  key_name = "devops-jmi"
  tags = {
    name = "ec2-jmi"
  }
}
```

```text
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ terraform fmt
ec2.tf
```

Fichier après  

```text
jmi@portable-jmi:~/Documents/formationTerraform/tp2$ more ec2.tf 
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.49.0"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "xxxxxxxxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxx"
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  key_name      = "devops-jmi"
  tags = {
    name = "ec2-jmi"
  }
}

```

## Rendre les deploiements dynamiques  

### Attribut et output

- Customization de votre déploiement     -> attribut
- Récupération d’informations dynamiques -> output

### Referencer les differents types de ressources

```text
provider "aws" {
  region     = "us-east-1"
  access_key = "PUT-YOUR-ACCESS-KEY-HERE"
  secret_key = "PUT-YOUR-SECRET-KEY-HERE"
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
}

resource "aws_eip" "lb" {                               ==> ressource elasticIP cad @IP publique
  vpc = true
}

resource "aws_eip_association" "eip_assoc" {            ==> affectation de l'@IP à l'objet "eip_assoc"
  instance_id   = aws_instance.myec2.id                         id de l'instance
  allocation_id = aws_eip.lb..id                                id de l'ip
}
```

### Variables

#### Pour passer des variables il existe plusieurs solutions  

##### directement en ligne de commande dans le "terraform plan"  

```text
terraform plan -var="instancetype=t2.small"
```

##### charger toutes les variables depuis un fichier  

```text
terraform plan -var-file="custom.tfvars"
```

##### Windows specific commands

```text
setx TF_VAR_instancetype ms.large
echo %TF_VAR_instancetype
```

##### Linux / MAC specific commands

```text
export TF_VAR_instancetype t2.nano
echo %TF_VAR_instancetype
```

#### Définir le type de variables

```text
variable "usernumber" {
  type = number
}

variable "elb_name" {
  type = string
}

variable "az" {
  type = list
}

variable "timeout" {
  type = number
}

elb_name="myelb"
timeout="400"
az=["us-east-1a","us-east-1b"]
```

### Data source  

```text
provider "aws" {
  region     = "us-east-1"
  access_key = "PUT-YOUR-ACCESS-KEY-HERE"
  secret_key = "PUT-YOUR-SECRET-KEY-HERE"
}

data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
}

resource "aws_instance" "instance-1" {
  ami = data.aws_ami.app_ami.id
  instance_type = "t2.micro"
}
```

## TP3 : déploiement d'une infrastructure dynamique  

- L’objectif est de déployer une instance ec2 avec une ip publique et un security group
- IP publique: vous allez créer une ip publique pour votre EC2
- Security Group: créez une security group pour ouvrir le port 80 et 443, attachez cette security group à votre IP publique
- EC2
  - Votre ec2 doit avoir une taille variabilisée, la valeur par defaut devrait être t2.nano et la valeur à surcharger sera t2.micro
  - L’image AMI à utiliser sera l’image la plus à jour de AMAZON LINUX
  - Spécifiez la key pair à utiliser (devops-_votre prénom_)
  - Attachez l’ip publique à votre instance
  - Variabilisez le Tag afin qu’il contienne au moins le tag: « Name: ec2-_votre prenom_ » le N est bien en majuscule
- Supprimez vos ressources avec terraform destroy
- Créez un dossier tp-3 comme vous l’avez fait au tp-2 pour conserver votre code

### TP3 - fichier ec2.tf

```text
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.49.0"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "myec2" {
  ami             = data.aws_ami.app_ami.id
  instance_type   = var.instancetype
  key_name        = "devops-jmi"
  tags            = var.aws_common_tag
  security_groups = ["${aws_security_group.allow_http_https.name}"]
  root_block_device {
    delete_on_termination = true
  }
}

resource "aws_security_group" "allow_http_https" {
  name        = "jmi-sg"
  description = "Allow http and https inbound traffic"

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.myec2.id
  vpc      = true
}
```

### TP3 - fichier terraform.tfvars  

```text
instancetype = "t2.micro"
aws_common_tag = {
  Name = "ec2-jmi"
}
```

### TP3 - fichier variables.tf

```text
variable instancetype {
  type        = string
  description = "set aws instance type"
  default     = "t2.nano"
}

variable aws_common_tag {
  type        = map
  description = "Set aws tag"
  default = {
    Name = "ec2-jmi"
  }
}
```

### TP3 - logs d'execution

![image](./photosFormationTerraform/122_tp3_viewInstance.png)  

![image](./photosFormationTerraform/123_tp3_viewVolume.png)  

![image](./photosFormationTerraform/124_tp3_viewSecurityGroup.png)  

![image](./photosFormationTerraform/125_tpm_viewElasticIp.png)  

Les logs d'exécution sont ici : ./photosFormationTerraform/tp3Logs.txt

## Terraform provisionners : récupération d'informations remote/local  

Une fois l'infrastructure déployée, il peut etre interessant de deployer l'application au moyen d'outils tels qu'ansible.
Pour cela il faut que terraform recupere des informations telles que l'@ ip par exemple et la stocker en local pour permettre à ansible de deployer l'application ou encore de pouvoir exécuter des scripts sur des hôtes distants.  

### **local-exec** exemple de recuperation d'adresse IP en local pour que le deploiement avec ansible puisse se realiser  

```text
resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"

  provisioner "local-exec" {
    command = "echo ${aws_instance.myec2.private_ip} >> private_ips.txt
  }
}
```

### **remote-exec** exemple de commande à executer sur l'hote distant  

```text
resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  key_name      = "kplabs-terraform"

  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras install -y ",
      "sudo systemctl start nginx"
      ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./kplabs-terraform.pem")        ==> fonction "file" (qui permet d'utiliser un fichier)
      host        = self.public_ip                        ==> self = objet en train d'etre créé : voir qq lignes + haut "resource "aws_instance" "myec2""
    }
  }
}
```

Les données que l'on peut retrouver en "output" correspondent à celles qu'on peut retrouver dans la section "reference".  

### TP4 - Deployer nginx et enregistrer l'IP

- A partir du code du tp 3, vous allez le modifier pour installer nginx sur votre VM
- Vous allez récupérer l’ip , id et la zone de disponibilité de la vm et vous les mettrez dans un fichier nommé infos_ec2.txt
- Supprimez vos ressources avec terraform destroy
- Créez un dossier tp 4 comme vous l’avez fait au tp 3 pour conserver votre code

#### TP4 - fichier ec2.tf

```text
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.49.0"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "xxxxxxxxxxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxxxx"
}

data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "myec2" {
  ami             = data.aws_ami.app_ami.id
  instance_type   = var.instancetype
  key_name        = "devops-jmi"
  tags            = var.aws_common_tag
  security_groups = ["${aws_security_group.allow_ssh_http_https.name}"]
  root_block_device {
    delete_on_termination = true
  }
  provisioner "local-exec" {
    command = "echo PUBLIC_IP: ${aws_instance.myec2.public_ip} ; ID: ${aws_instance.myec2.id} ; AZ: ${aws_instance.myec2.availability_zone} >> ./infos_ec2.txt"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras install -y nginx1.12",
      "sudo systemctl start nginx"
      ]
  connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("../devops-jmi.pem")
      host        = self.public_ip   
    }
  }
}

resource "aws_security_group" "allow_ssh_http_https" {
  name        = "jmi-sg"
  description = "Allow http, https and ssh inbound/outbound traffic"

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.myec2.id
  vpc      = true
}
```

#### TP4 - fichier terraform.tfvars 

```text
instancetype = "t2.micro"
aws_common_tag = {
  Name = "ec2-jmi"
}
```

#### TP4 - fichier variables.tf

```text
variable instancetype {
  type        = string
  description = "set aws instance type"
  default     = "t2.nano"
}

variable aws_common_tag {
  type        = map
  description = "Set aws tag"
  default = {
    Name = "ec2-jmi"
  }
}
```

Il y a eu plusieurs plantages essentiellement dûs à des problèmes de syntaxe (log1) ou de mauvaise declaration (log2)

#### TP4 - log1 : erreur de syntaxe sur fichier ec2.tf

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ source ../export.sh
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform init
There are some problems with the configuration, described below.

The Terraform configuration must be valid before initialization so that
Terraform can determine which modules and providers need to be installed.
╷
│ Error: Unclosed configuration block
│
│ On ec2.tf line 35: There is no closing brace for this block before the end of the file. This may be caused by incorrect brace nesting elsewhere
│ in this file.
╵
```

##### TP4 - log2 : erreur de syntaxe sur fichier ec2.tf

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform plan
╷
│ Error: Reference to undeclared resource
│
│   on ec2.tf line 31, in resource "aws_instance" "myec2":
│   31:   security_groups = ["${aws_security_group.allow_ssh_http_https.name}"]
│
│ A managed resource "aws_security_group" "allow_ssh_http_https" has not been declared in the root module.
╵
```

#### TP4 - terraform plan OK  

[log TP4 terraform plan](./logs/TP4_terraformPlan.md)  

#### TP4 - terraform apply KO

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform apply
data.aws_ami.app_ami: Reading...
data.aws_ami.app_ami: Read complete after 1s [id=ami-0fe472d8a85bc7b0e]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip.lb will be created
  + resource "aws_eip" "lb" {
      + allocation_id        = (known after apply)
      + association_id       = (known after apply)
      + carrier_ip           = (known after apply)
      + customer_owned_ip    = (known after apply)
      + domain               = (known after apply)
      + id                   = (known after apply)

.. / .. 
      + name                   = "jmi-sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_security_group.allow_ssh_http_https: Creating...
aws_security_group.allow_ssh_http_https: Creation complete after 3s [id=sg-0c4ca09b6ed839f28]
aws_instance.myec2: Creating...
aws_instance.myec2: Still creating... [10s elapsed]
aws_instance.myec2: Still creating... [20s elapsed]
aws_instance.myec2: Still creating... [30s elapsed]
aws_instance.myec2: Provisioning with 'local-exec'...
aws_instance.myec2 (local-exec): Executing: ["/bin/sh" "-c" "echo PUBLIC_IP: 54.85.117.203 ; ID: i-011d38d1975c7b478 ; AZ: us-east-1e >> ./infos_ec2.txt"]
aws_instance.myec2 (local-exec): PUBLIC_IP: 54.85.117.203
aws_instance.myec2 (local-exec): /bin/sh: ID:: command not found
aws_instance.myec2 (local-exec): /bin/sh: AZ:: command not found
╷
│ Error: local-exec provisioner error
│
│   with aws_instance.myec2,
│   on ec2.tf line 35, in resource "aws_instance" "myec2":
│   35:   provisioner "local-exec" {
│
│ Error running command 'echo PUBLIC_IP: 54.85.117.203 ; ID: i-011d38d1975c7b478 ; AZ: us-east-1e >> ./infos_ec2.txt': exit status 127. Output:
│ PUBLIC_IP: 54.85.117.203
│ /bin/sh: ID:: command not found
│ /bin/sh: AZ:: command not found
│
╵
```

##### TP4 - terraform destroy 1

[log TP4 terraform destroy](./logs/TP4_terraformdestroy0.md)  

Le problème vient de la ligne. Il semblerait que le ";" soit "mal" interprété. Je supprime le ";" et relance

```text
command = "echo PUBLIC_IP: ${aws_instance.myec2.public_ip} ; ID: ${aws_instance.myec2.id} ; AZ: ${aws_instance.myec2.availability_zone} >> ./infos_ec2.txt"
```

##### TP4 - terraform apply 2 : KO

[log TP4 terraform destroy](./logs/TP4_terraformdestroy1.md)  

##### TP4 - terraform destroy 2

[log TP4 terraform destroy](./logs/TP4_terraformdestroy2.md)  

Probleme de connexion en SSH ... je ne sais pas si le flux est ouvert pour faire du SSH depuis l'environnement "travail" (serveur slvindus1 => AWS).
Je vais tenter à partir de mon pc perso avant d'aller plus loin

Effectivement on ne peut pas faire de ssh vers l'exterieur depuis le serveur svlindus1 puisque les mêmes fichiers de code fonctionnent très bien sur le pc perso  

[log TP4 terraform apply](./logs/TP4_terraformApply.md)  

![image](./photosFormationTerraform/127_provisionner1.png)  

![image](./photosFormationTerraform/128_provisionner2.png)  

Reste un souci : le serveur nginx est injoignable  
Pour le provisionner "local-exec" il ne faut pas le positionner dans la ressource "aws_instance" 

```
  provisioner "local-exec" {
    command = "echo PUBLIC_IP: ${aws_instance.myec2.public_ip} ID: ${aws_instance.myec2.id} AZ: ${aws_instance.myec2.availability_zone} >> ./infos_ec2.txt"
    }
```

Mais plutot dans la ressource "aws_eip"

```text
  provisioner "local-exec" {
    command = "echo PUBLIC_IP: ${aws_eip.lb.public_ip} ID: ${aws_instance.myec2.id} AZ: ${aws_instance.myec2.availability_zone} >> ./infos_ec2.txt"
```

Log du fichier du provisionner local-exec :

```text
PUBLIC_IP: 3.82.249.134 ID: i-09d58b4845e330df2 AZ: us-east-1c
PUBLIC_IP: 23.22.17.174 ID: i-0096a76801a89a53d AZ: us-east-1e
```

![image](./photosFormationTerraform/129_serveurNginxOk.png)  

## Workflow de creation : localisation des fichiers "terraform.tfstate" dans un S3 Bucket

Un bucket S3 c'est un peu comme un serveur FTP auquel on aurait donné des super-pouvoirs et géré dans le cloud.  
C'est dans ce serveur que vont être stockés les fichiers terraform.tfstate

### TP5 - Remote Backend

- Créez un s3 nommé terraform-backend-**votre prénom**
- Modifiez votre rendu du tp-4 afin d’y intégrer le stockage du tfstate sur votre s3
- Vérifiez après avoir lancer un déploiement que le fichier sur le drive est bien créé et contient bien les infos à jour
- Supprimez vos ressources avec terraform destroy
- Créez un dossier tp-5 comme vous l’avez fait au tp-4 pour conserver votre code

#### TP5 - creation du S3

![image](./photosFormationTerraform/130_creationS3.png)  

Conserver tous les paramètres par défaut

![image](./photosFormationTerraform/131_visuS3.png)

Il est nécessaire de refaire un "terraform init" car ajout de la ressource S3

```text
jmi@portable-jmi:~/Documents/formationTerraform/tp5$ terraform plan -out plan.out
╷
│ Error: Backend initialization required, please run "terraform init"
│ 
│ Reason: Initial configuration of the requested backend "s3"
│ 
│ The "backend" is the interface that Terraform uses to store state,
│ perform operations, etc. If this message is showing up, it means that the
│ Terraform configuration you're using is using a custom configuration for
│ the Terraform backend.
│ 
│ Changes to backend configurations require reinitialization. This allows
│ Terraform to set up the new configuration, copy existing state, etc. Please run
│ "terraform init" with either the "-reconfigure" or "-migrate-state" flags to
│ use the current configuration.
│ 
│ If the change reason above is incorrect, please verify your configuration
│ hasn't changed and try again. At this point, no changes to your existing
│ configuration or state have been made.
```

#### TP5 - terraform init  

[log TP5 terraform init](./logs/TP5_terraformInit.md)  

#### TP5 - terraform plan  

Pas d'infos sur la ressource S3 dans les logs car elle est à créer manuellement sur l'interface AWS.  

[log TP5 terraform plan](./logs/TP5_terraformPlan2.md)  

#### TP5 - terraform apply

[log TP5 terraform apply](./logs/TP5_terraformApply.md)  

![image](./photosFormationTerraform/132_tfstateUploadé.png)

![image](./photosFormationTerraform/133_visuNginxOk.png)

![image](./photosFormationTerraform/134_proprietesS3.png)

![image](./photosFormationTerraform/135_autorisationsS3.png)

#### TP5 - repository du code

[https://github.com/eazytrainingfr/terraform-training/tree/master/tp-5](https://)  

## Modules

Même fonctionnement que les rôles dans ansible.

![image](./photosFormationTerraform/136_exempleArborescenceAvecModules.png)

Aller sur le site de terraform registry pour récupérer les différents modules déjà écrits ou **creez en un**.  
Dans le cadre du TP6 on va en creer un.  

### TP-6: Module ec2

- Créez un dossier tp-6 comme vous l’avez fait au tp-5 pour conserver votre code
- Créez un module ec2module afin de déployer l’instance de la façon que vous l’avez fait aux tps précédents (ec2 + security group + ip publique)
- Créez ensuite deux dossiers, prod et dev, chacun avec un terraform (main.tf ) utilisant le module ec2module créé pour déployer une instance avec respectivement pour taille t2.micro pour la prod et t2.nano pour la dev
- Veuillez également à surcharger le tag pour que ai cette forme : « Name: ec2-prod-**votre prenom** » pour la prod et « Name: ec2-dev-**votre prenom** » pour la Dev
- Lancez ensuite la création de votre ec2 de prod et de dev
- Vérifiez que les ec2 portent bien le bon nom (Tag) et ont la bonne taille correspondant à l’environnement
- Supprimez vos ressources avec terraform destroy

### TP6 - arborescence finale

```text
├── devops-jmi.pem          ==> repertoire securise
└── tp6_pcPerso
    ├── dev
    │   └── main.tf         ==> environnement "dev"
    │                       ==>      informations sensibles (access key + secret key)
    |                       ==>      surcharge des variables
    ├── modules
    │   └── ec2module
    │       ├── main.tf
    │       └── variables.tf
    └── prod
        └── main.tf         ==> environnement "prod"
                            ==>      informations sensibles (access key + secret key)
                            ==>      surcharge des variables        
```

[log TP6 Execution commandes modules - DEV](./logs/TP6_utilisationModules_dev.md) 

![image](./photosFormationTerraform/137_uploadTfstateDansS3.png)  

![image](./photosFormationTerraform/137_uploadTfstateDansS3.png)  

[log TP6 tfstate - DEV](./logs/137_uploadTfstateDansS3.md)  

![image](./photosFormationTerraform/138_visuCreationDev.png)  

[log TP6 Execution commandes modules - PROD](./logs/TP6_utilisationModules_prod.md) 

![image](./photosFormationTerraform/139_TP6visuCreationProd.png)  

## Mini projet  

- Ecrivez un module pour créer une instance ec2 utilisant la dernière version de ubuntu bionic (qui
s’attachera l’ebs et l’ip publique) dont la taille et le tag seront variabilisés
- Ecrivez un module pour créer un volume ebs dont la taille sera variabilisée
- Ecrivez un module pour une ip publique (qui s’attachera la security group)
- Ecrivez un module pour créer une security qui ouvrira le 80 et 443
- Créez un dossier app qui va utiliser les 4 modules pour déployer une ec2, bien-sûr vous allez surcharger les variables afin de rendre votre application plus dynamique
- A la fin du déploiement, installez nginx et enregistrez l’ip publique dans un fichier nommé ip_ec2.txt (ces éléments sont à intégrer dans le rôle ec2)
- A la fin de votre travail, poussez votre rôle sur github et envoyez nous le lien de votre repo à eazytrainingfr@gmail.com et nous vous dirons si votre solution respecte les bonnes pratiques  

### Mini projet - arborescence

```text
tp8_miniProjet/
├── app                           => on doit se deplacer dans ce repertoire pour exécuter les commandes terraform
│   └── main.tf                   # fichier principal
└── modules
    ├── ebs
    │   ├── main.tf
    │   ├── output.tf
    │   └── variable.tf
    ├── ec2
    │   ├── main.tf
    │   ├── output.tf
    │   └── variable.tf
    ├── eip
    │   ├── main.tf
    │   └── output.tf
    └── sg
        ├── main.tf
        ├── output.tf
        └── variable.tf
```

### Mini projet - détail fichier : app/main.tf  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md"]
  }

# Creation du sg
module "sg" {
  source = "../modules/sg"
}

# Creation du volume
module "ebs" {
    source    = "../modules/ebs"
    disk_size = 5
}

# Creation de l'eip
module "eip" {
  source = "../modules/eip"
}

# Creation de l'ec2
module "ec2" {
    source        = "../modules/ec2"
    instance_type = "t2.micro"
    public_ip     = module.eip.output_eip_ip
    sg_name       = module.sg.output_sg_name
}

# Creation des associations necessaires # 22:28
resource "aws_eip_association" "eip_assoc" {
    instance_id   = module.ec2.output_ec2_id
    allocation_id = module.eip.output_eip_id
}

resource "aws_volume_attachment" "ebs_att" {
    device_name = "/dev/sdh"
    volume_id   = module.ebs.output_id_volume
    instance_id = module.ec2.output_ec2_id
}
```

### Mini projet - détail fichier : modules/ebs/main.tf  

```text
resource "aws_ebs_volume" "my_vol" {
  availability_zone = var.AZ
  size              = var.disk_size
  tags = {
    Name = "${var.maintainer}-ebs"
  }
}
```

### Mini projet - détail fichier : modules/ebs/output.tf  

```text
output "output_id_volume" {
  value = aws_ebs_volume.my_vol.id
}
```

### Mini projet - détail fichier : modules/ebs/variable.tf  

```text
variable "maintainer" {
  type    = string
  default = "jmi"
}

variable "disk_size" {
  type    = number
  default = 2
}

variable "AZ" {
  type    = string
  default = "us-east-1b"
}
```

### Mini projet - détail fichier : modules/ec2/main.tf  

```text
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] #canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

resource "aws_instance" "mini-projet-ec2" {
  ami               = data.aws_ami.ubuntu.id
  instance_type     = var.instance_type
  key_name          = var.ssh_key
  availability_zone = var.AZ
  security_groups   = ["${var.sg_name}"]
  tags              = { 
    Name = "${var.maintainer}-ec2"
  }
  
root_block_device {
    delete_on_termination = true
  }

  provisioner "local-exec" {
     command = "echo PUBLIC IP: ${var.public_ip} > IP_ec2.txt"
  }

  provisioner "remote-exec" {
     inline = [
       "sudo apt update -y",
       "sudo apt install -y nginx",
       "sudo systemctl start nginx",
       "sudo systemctl enable nginx"
     ]

   connection {
     type        = "ssh"
     user        = var.user
     private_key = file("/home/jmi/Documents/formationTerraformForBeginners/${var.ssh_key}.pem")
     host        = self.public_ip
    }
  }
  
}
```

### Mini projet - détail fichier : modules/ec2/output.tf  

```text
output "output_ec2_id" {
  value = aws_instance.mini-projet-ec2.id
}

output "output_ec2_AZ" {
  value = aws_instance.mini-projet-ec2.availability_zone
}
```

### Mini projet - détail fichier : modules/ec2/variable.tf  

```text
variable "maintainer" {
  type    = string
  default = "jmi"
}

variable "instance_type" {
  type    = string
  default = "t2.nano"
}

variable "ssh_key" {
  type    = string
  default = "devops-jmi"
}

variable "sg_name" {
  type    = string
  default = "NULL"
}

variable "public_ip" {
  type    = string
  default = "NULL"
}

variable "AZ" {
  type    = string
  default = "us-east-1b"
}

variable "user" {
  type    = string
  default = "ubuntu"
}
```

### Mini projet - détail fichier : modules/eip/main.tf  

```text
resource "aws_eip" "my_eip" {
  vpc = true
  }
```

### Mini projet - détail fichier : modules/eip/output.tf  

```text
output "output_eip_ip" {
  value = aws_eip.my_eip.public_ip
}

output "output_eip_id" {
  value = aws_eip.my_eip.id
}
```

### Mini projet - détail fichier : modules/sg/main.tf  

```text
resource "aws_security_group" "my_sg" {
  name        = "${var.maintainer}-sg"
  description = "Allow ssh, http and https inbound traffic"

  ingress {
    description      = "https from all"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "http from all"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "ssh from all"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.maintainer}-sg"
  }
}
```

### Mini projet - détail fichier : modules/sg/output.tf  

```text
output "output_sg_name" {
  value = aws_security_group.my_sg.name
}
```

### Mini projet - détail fichier : modules/sg/variable.tf  

```text
variable "maintainer" {
  type    = string
  default = "jmi"
}
```

### Mini projet - logs d'exécution

[log Mini projet](./logs/TP8_logs.md)  

### Mini projet - captures d'écran suite exécution  

![image](./photosFormationTerraform/140_visuEc2.png)  

![image](./photosFormationTerraform/141_visuEc2IpPublique.png)  

![image](./photosFormationTerraform/142_visuInstallNginx.png)  

![image](./photosFormationTerraform/143_detailsEc2.png)  