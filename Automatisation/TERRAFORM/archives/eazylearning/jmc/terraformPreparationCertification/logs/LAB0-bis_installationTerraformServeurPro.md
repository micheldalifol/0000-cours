(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase]$ echo $PATH
/data/adm5224opit/.venv/bin:/data/adm5224opit/.local/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
```

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ export http{,s}_proxy=proxylinuxinternet.agpm.adm:3128
(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ wget https://releases.hashicorp.com/terraform/1.3.5/terraform_1.3.5_linux_amd64.zip
--2023-01-10 12:50:36--  https://releases.hashicorp.com/terraform/1.3.5/terraform_1.3.5_linux_amd64.zip
Resolving proxylinuxinternet.agpm.adm (proxylinuxinternet.agpm.adm)... 172.20.96.222
Connecting to proxylinuxinternet.agpm.adm (proxylinuxinternet.agpm.adm)|172.20.96.222|:3128... connected.
Proxy request sent, awaiting response... 200 OK
Length: 19469337 (19M) [application/zip]
Saving to: ‘terraform_1.3.5_linux_amd64.zip’

100%[=========================================================================================================>] 19,469,337  68.9MB/s   in 0.3s

2023-01-10 12:50:37 (68.9 MB/s) - ‘terraform_1.3.5_linux_amd64.zip’ saved [19469337/19469337]

(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ unzip terraform_1.3.5_linux_amd64.zip

(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ mv terraform /data/adm5224opit/.venv/bin/

(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ chmod u+x /data/adm5224opit/.venv/bin/terraform

(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ whereis terraform
terraform: /data/adm5224opit/.venv/bin/terraform

(.venv) [adm5224opit@svlindus1 /data/adm5224opit]$ terraform version
Terraform v1.3.5
on linux_amd64