# <span style="color: #CE5D6B"> **Formation Terraform : formation _Udemy_ Dirane Taffen _Terraform: Preparation à la certification [2023]_** </span>

<span style="color: #CE5D6B">

## **MEMO** 
</span>

```text
insertFichierLog  : [log](./definition_termes_kubernetes.md)  
insertImage       : ![image](./photosFormationTerraform/103_connexionAws.png)  
sommaire          : [Resume des commandes](./keycloak.md#resume-des-commandes)
url               : [https://signin.aws.amazon.com/](http://)  
siteReference     : https://xavki.blog/terraform-formation-francais/#7gtzumVHZtE
```

## **Informations de connexion au site Udemy**  

[url + login + password](./fichiersSecurises/infosConnexionUdemy.md)  

## **Ressources**  

### support de cours  

[pdf fourni](./ressources/terraform%2Bca.pdf)  

### scripts utilisés dans cette formation  

[https://github.com/eazytrainingfr/terraform-certified-associate](http://)  

### archive des scripts  

[archive de tous les scripts](./ressources/terraform-certified-associate-master.zip)  

### ajout proxy selon situation  

Selon l'environnement dans lequel on se situe il peut être nécessaire de paramétrer un proxy pour se connecter à l'extérieur.  

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ more ../export.sh
#!/bin/bash
export http{,s}_proxy=http://proxylinuxinternet.agpm.adm:3128

(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ source ../export.sh
```

## **Détail de la certification**  

Consulter le site de Hashicorp <https://developer.hashicorp.com/terraform/tutorials/certification/associate-review>  

## **Compte AWS**  

## LAB0 : création compte AWS gratuit  

### LAB0 - Avertissement

J'ai un peu galéré pour créer ce compte car l'interface a changé entre le moment où la formation a été écrite et la création.  
De ce fait la création "step by step" est absente de ce tutorial.  

### LAB0 - Connexion compte AWS  

[https://signin.aws.amazon.com/](http://)  /!\ choisir _Utilisateur racine /!\ **qui ne sera pas l'utilisateur technique**

![image](./photosFormationTerraform/102_connexionAws.png)  

pwd :  « classique2PourCeTypeDeSite »  

![image](./photosFormationTerraform/103_connexionAws.png)  

MFA => google authenticator sur mobile

![image](./photosFormationTerraform/104_connexionAws.png)  

### LAB0 - Détail compte  

![image](./photosFormationTerraform/99_awsDetailCompte.png)  

![image](./photosFormationTerraform/100_awsDetailCompte.png)  

### LAB0 - Détail facturation  

Il est obligatoire de saisir les références d'une CB perso pour activer  

![image](./photosFormationTerraform/101_preferencesPaiement.png)  

### LAB0 - Création d'un compte **utilisateur** avec droits _full admin_ pour déployer les ressources  

![image](./photosFormationTerraform/105_creationUserAvecDroitsAdmin.png)  

![image](./photosFormationTerraform/106_creationUserAvecDroitsAdmin.png)

![image](./photosFormationTerraform/107_creationUserAvecDroitsAdmin.png)  

![image](./photosFormationTerraform/108_creationUserAvecDroitsAdmin.png)  

### LAB0 - Connexion avec user _jmi_

[https://026071546831.signin.aws.amazon.com/console](https://)  

[Login + mdp de connexion](./fichiersSecurises/infosConnexionAwsUserJmi.md)  

![image](./photosFormationTerraform/109_creationUserAvecDroitsAdmin.png)  

![image](./photosFormationTerraform/110_creationUserAvecDroitsAdmin.png)  

### LAB0 - Creation des cles de connexion accessKey + secretKey

![image](./photosFormationTerraform/111_creationCles.png)  

![image](./photosFormationTerraform/112_idEtSecret.png)

[accessKey + secretKey](./fichiersSecurises/accessKeyEtSecretKey.md)  

![image](./photosFormationTerraform/113_dashboardUserAvecCles.png)  

### LAB0 - Recherche EC2  

![image](./photosFormationTerraform/114_rechercheEC2.png)  

Cliquer sur n'importe quel item dans ce "pave" permet d'acceder au dashboard "New EC2 Experience"

### LAB0 - Creation d'une paire de clés

![image](./photosFormationTerraform/115_creationKeyPair.png)  

![image](./photosFormationTerraform/116_parametresKeyPair.png)  

![image](./photosFormationTerraform/117_visuKeyPair.png)  

![image](./photosFormationTerraform/118_visuKeyPair_pem.png)  

## LAB0 bis - installation de terraform (sur linux centOS7 / environnement pro)  

### LAB0 bis - Installation sur serveur pro

- Download
- Copie en local
- Ajout au $PATH
- Rendre executable

```text
COMMANDES
---------

echo $PATH
export http{,s}_proxy=proxylinuxinternet.agpm.adm:3128
wget https://releases.hashicorp.com/terraform/1.3.5/terraform_1.3.5_linux_amd64.zip
unzip terraform_1.3.5_linux_amd64.zip
mv terraform /data/adm5224opit/.venv/bin/
chmod u+x /data/adm5224opit/.venv/bin/terraform
whereis terraform
terraform version
```

[log LAB0 Installation sur serveur pro](./logs/LAB0_installationTerraformServeurPro.md)  

### LAB0 ter - Installation de terraform sur pc perso  

#### LAB0 ter - Commandes d'installation

```text
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list

sudo apt update && sudo apt install terraform
```

### LAB0 ter - Logs d'installation

[log LAB0 ter - Installation sur serveur perso](./logs/LAB0-bis_installationTerraformServeurPerso.md)  

### **Fichier de login et password**  

Dans tous ces travaux pratiques la connexion au provider (ici aws) se fera au moyen d'un fichier contenant login et mot de passe de la forme suivante :  

```text
provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

[default]  
AWS_ACCESS_KEY_ID=xxxxxxxxxxxxxxxxx  
AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx  

```

## **Gestion des providers**  

Un provider permet de communiquer avec un prestataire service de ressource infra : cloud, on premise, etc.  
Les providers sont versionnés indépendamment des versions de terraform.  
Chaque provider possède plusieurs version de son plugin.  

[interface provider](./photosFormationTerraform/119_schemaProvider.png)  

### Syntaxe du bloc provider : choix de la version  

```text
provider "aws" {
  region  = "us-east-1"
  version = "2.7"
}

-------------------------------------------------------------------
 version number arguments |            description                 |
------------------------------------------------------------------ |
         >=1.0            |    greater than equal to the version   |
         <=1.0            |    less    than equal to the version   |
         ~>2.0            |    any version in the 2.x range        |
    >=2.10,<=2.30         |    any version between 2.10 and 2.30   |
-------------------------------------------------------------------    
```

### LAB1 - Versionning des providers  

Le versioning de provider est déprécié... la formation semble anciennne et n'a pas été mise à jour  .... cela pourra servir pour des codes un peu anciens ...  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB1$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/aws versions matching ">= 2.10.0, <= 2.30.0"...
- Installing hashicorp/aws v2.30.0...
- Installed hashicorp/aws v2.30.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

╷
│ Warning: Version constraints inside provider configuration blocks are deprecated
│ 
│   on ec2.tf line 5, in provider "aws":
│    5:   version    = ">=2.10,<=2.30"
│ 
│ Terraform 0.13 and earlier allowed provider version constraints inside the provider configuration block, but that is now deprecated and will be removed in a future
│ version of Terraform. To silence this warning, move the provider version constraint into the required_providers block.
╵

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB1$ terraform version
Terraform v1.3.7
on linux_amd64
+ provider registry.terraform.io/hashicorp/aws v2.30.0
```

### LAB1 - Community Provider  

Si on n'utilise pas de provider "officiel" Hashicorp, alors le "terraform init" ne téléchargera pas le plugin nécessaire.  
Il sera nécessaire de copier manuellement le provider.  

![image](./photosFormationTerraform/120_rechercheCommunityProvider.png)  

#### LAB1 - Installation provider communautaire  

Créer l'arborescence du projet tel que l'atttend terraform : "nomProjet/.terraform/providers/plugins/"  
On pourra y copier le fichier précédemment téléchargé & dézippé : "nomProjet/.terraform/providers/plugins/nomFichierProviderTelechargeDézippé"  

![image](./photosFormationTerraform/121_installationProviderCommunautaire.png)  

#### LAB1 - terraform init  

```text
mi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB2$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/kubernetes...
- Installing hashicorp/kubernetes v2.17.0...
- Installed hashicorp/kubernetes v2.17.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

## **Attributs et outputs**  

### Définitions  

Attribut : caractéristique d'une ressource  
Output   : valeur retournée après la création de l'objet  

### LAB3 - Outputs  

Création d'un fichier "LAB3/attributes.tf" dans lequel on a différents blocks :

- provider  
- resource : elasticip  

```text
provider "aws" {
  region     = "us-east-1"
  access_key = "xxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxxxx"
}

resource "aws_eip" "lb" {
  vpc = true
}

output "eip" {
  value = "aws_eip.lb"
}

resource "aws_s3_bucket" "mys3" {
  bucket = "jmi-bucket"
}

output "mys3bucket" {
  value = aws_s3_bucket.mys3
}
```

#### LAB3 - Output : terraform init/validate  

```text
mi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB3$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v4.53.0...
- Installed hashicorp/aws v4.53.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB3$ terraform validate
Success! The configuration is valid.
```

#### LAB3 - Output : terraform plan  

[log LAB3 - terraform plan](./logs/LAB3_terraformPlan.md)  

#### LAB3 - Output : terraform apply  

[log LAB3 - terraform apply](./logs/LAB3-terraformApply.md)  

##### LAB3 - outputs

```text
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

eip = "aws_eip.lb"
mys3bucket = {
  "acceleration_status" = ""
  "acl" = tostring(null)
  "arn" = "arn:aws:s3:::jmi-bucket"
  "bucket" = "jmi-bucket"
  "bucket_domain_name" = "jmi-bucket.s3.amazonaws.com"
  "bucket_prefix" = tostring(null)
  "bucket_regional_domain_name" = "jmi-bucket.s3.amazonaws.com"
  "cors_rule" = tolist([])
  "force_destroy" = false
  "grant" = toset([
    {
      "id" = "2163c048e33cdf5f6622d9520832a9500160663f6c51ec611a5e3069556a9ac7"
      "permissions" = toset([
        "FULL_CONTROL",
      ])
      "type" = "CanonicalUser"
      "uri" = ""
    },
  ])
  "hosted_zone_id" = "Z3AQBSTGFYJSTF"
  "id" = "jmi-bucket"
  "lifecycle_rule" = tolist([])
  "logging" = tolist([])
  "object_lock_configuration" = tolist([])
  "object_lock_enabled" = false
  "policy" = ""
  "region" = "us-east-1"
  "replication_configuration" = tolist([])
  "request_payer" = "BucketOwner"
  "server_side_encryption_configuration" = tolist([])
  "tags" = tomap(null) /* of string */
  "tags_all" = tomap({})
  "timeouts" = null /* object */
  "versioning" = tolist([
    {
      "enabled" = false
      "mfa_delete" = false
    },
  ])
  "website" = tolist([])
  "website_domain" = tostring(null)
  "website_endpoint" = tostring(null)
  ```

##### LAB3 - Visualisation de création des objets  

![création S3 bucket](./photosFormationTerraform/122_creationS3Bucket.png)  

![visualisation EC2](./photosFormationTerraform/123_visuEC2.png)  

![détail elactic ip](./photosFormationTerraform/124_detailEIP.png)  

## **Type de données**  

![differents types de donnees](./photosFormationTerraform/125_differentsTypesDeDonnees.png)  

### Définition du type  

En général se definit dans un fichier "variables.tf"  

### Définition de la valeur de la variable  

En général se définit dans un fichier "terraform.tfvars"  

### LAB4 - Création elb  

Arborescence :  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB4$ tree
.
├── elb.tf                => 
├── terraform.tfvars      => fichier obligatoire si surcharge de valeurs de variables
└── variables.tf          => fichier de definition du nom et type des variables
```

On vérifie que les valeurs des variables aient bien été surchargées

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB4$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_elb.bar will be created
  + resource "aws_elb" "bar" {
      + arn                         = (known after apply)
      + availability_zones          = [
          + "us-east-1b",                                     => variable surchargée
          + "us-west-1a",                                     => variable surchargée
        ]
      + connection_draining         = true
      + connection_draining_timeout = 400                     => variable surchargée
      + cross_zone_load_balancing   = true
      + desync_mitigation_mode      = "defensive"
      + dns_name                    = (known after apply)
      + id                          = (known after apply)
      + idle_timeout                = 400                     => variable surchargée
      + instances                   = (known after apply)
      + internal                    = (known after apply)
      + name                        = "myelb"                 => variable surchargée
      + security_groups             = (known after apply)
      + source_security_group       = (known after apply)
.. / ..

etc .....
```

## **Manipuler les maps et les listes**  

### Manipuler les maps et les listes : définitions  

Maps : ensemble "clé + valeur"  
Liste : tableaux ou tables qui contiennent des informations séparées par un ","  

![exemple Liste Map](./photosFormationTerraform/126_exempleListeMap.png)  

### LAB5 - Utilisation de variables  
  
```text
provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = var.types["us-east-1"]
  
  root_block_device {
    delete_on_termination = true
  }
}
  variable "list" {
    type = list
    default = ["m5.large","m5.xlarge","t2.medium"]
  }

  variable "types" {
    type = map
    default = {
      us-east-1  = "t2.micro"
      us-west-2  = "t2.nano"
      us-south-1 = "t2.small"
    }
  }


jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB5$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.myec2 will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"             => valeur surchargee
 
.. / ..

          + volume_type           = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.
```

Utilisation de liste  

```text
provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = var.list[1]
  
  root_block_device {
    delete_on_termination = true
  }
}
  variable "list" {
    type = list
    default = ["m5.large","m5.xlarge","t2.medium"]
  }

  variable "types" {
    type = map
    default = {
      us-east-1  = "t2.micro"
      us-west-2  = "t2.nano"
      us-south-1 = "t2.small"
    }
  }

jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB5$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.myec2 will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "m5.xlarge"            => valeur surchargee
      + ipv6_address_count                   = (known after apply)
.. / ..
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

```

## **Count & Count Index**  

### Objectifs  

- Reduire la quantite de lignes de code  
- Regrouper les actions similaires  
- Rendre le code plus lisible  

### Creation de 3 ressources  

Au lieu de répéter n fois le même code on utilise le code suivant  

```text
resource "aws_iam_user" "lb" {
  name  = "loadbalancer.${count.index}"
  count = 5
  path  = "/system/"
}
```

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB6$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.myec2[0] will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
.. / ..

  # aws_instance.myec2[1] will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
.. / ..

  # aws_instance.myec2[2] will be created
  + resource "aws_instance" "myec2" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)

      .. / ..

    }

Plan: 3 to add, 0 to change, 0 to destroy.
```

![log complet](./logs/LAB6-creationPlusieursRessources.md)  

### Creation de 5 utilisateurs  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

variable "elb_names" {
  type = list
  default = ["dev-loadbalancer","stage-loadbalancer","prod-loadbalancer"]
}

resource "aws_iam_user" "lb" {
  name  = var.elb_names[count.index]
  count = 3
  path  = "/system/"
}

jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB6_bis$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_iam_user.lb[0] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "dev-loadbalancer"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # aws_iam_user.lb[1] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "stage-loadbalancer"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # aws_iam_user.lb[2] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "prod-loadbalancer"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.
```

## **Expression conditionnelle : ajout de conditions**  

### LAB7 - Fichier "conditionnal.tf"  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "dev" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  count         = var.istest == true ? 3 : 0    => si "istest" est "true" alors "count = 3"
}

resource "aws_instance" "prod" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.large"
  count         = var.istest == false ? 1 : 0
}

variable "istest" {}
```

### LAB7 - Fichier "terraform.tfvars"  

```text
istest = false
```

### LAB7 - terraform plan 1  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB7$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.prod[0] will be created
  + resource "aws_instance" "prod" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.large"

.. / .. 

Plan: 1 to add, 0 to change, 0 to destroy.
```

### LAB7 - terraform plan 2  

Si on modifie la valeur du test à "false"  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB7$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.dev[0] will be created
  + resource "aws_instance" "dev" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)

.. / ..

  # aws_instance.dev[1] will be created
  + resource "aws_instance" "dev" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)

.. / ..

  # aws_instance.dev[2] will be created
  + resource "aws_instance" "dev" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"

.. / .. 

Plan: 3 to add, 0 to change, 0 to destroy.

```

## **Local values**  

Exemple d'une chaine de caractere ou d'une valeur qui doit se répéter plusieurs fois  

![exemple local values](./photosFormationTerraform/127_localValues.png)  

### LAB8 - fichier "localValues.tf  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

locals {
  common_tags = {
    Owner   = "EAZYTreaning"
    service = "backend"
  }
}

resource "aws_instance" "app-dev" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  tags          = local.common_tags
}

resource "aws_instance" "db-dev" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  tags          = local.common_tags
}

resource "aws_ebs_volume" "db-ebs" {
  availability_zone = "us-west-2a"
  size              = 8
  tags              = local.common_tags
}
```

### LAB8 - terraform plan

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB8$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_ebs_volume.db-ebs will be created
  + resource "aws_ebs_volume" "db-ebs" {
      + arn               = (known after apply)
      + availability_zone = "us-west-2a"
      + encrypted         = (known after apply)
      + final_snapshot    = false
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 8
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Owner"   = "EAZYTreaning"
          + "service" = "backend"
        }
      + tags_all          = {
          + "Owner"   = "EAZYTreaning"
          + "service" = "backend"
        }
      + throughput        = (known after apply)
      + type              = (known after apply)
    }

  # aws_instance.app-dev will be created
  + resource "aws_instance" "app-dev" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
../..
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
 ../..
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Owner"   = "EAZYTreaning"
          + "service" = "backend"
        }
      + tags_all                             = {
          + "Owner"   = "EAZYTreaning"
          + "service" = "backend"
        }
      + tenancy                              = (known after apply)
../..
          + volume_type           = (known after apply)
        }
    }

  # aws_instance.db-dev will be created
  + resource "aws_instance" "db-dev" {
      + ami                                  = "ami-0b5eea76982371e91"
      + arn                                  = (known after apply)
 ../..
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
../..
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Owner"   = "EAZYTreaning"
          + "service" = "backend"
        }
      + tags_all                             = {
          + "Owner"   = "EAZYTreaning"
          + "service" = "backend"
        }
      + tenancy                              = (known after apply)
../..
    }

Plan: 3 to add, 0 to change, 0 to destroy.
```

## **Fonctions**

### Plusieurs types de fonctions  

On ne peut pas définir ses propres fonctions.  
La liste des fonctions est disponible sur le site de Terraform : [https://developer.hashicorp.com/terraform/language/functions](http://)  

 ![Liste fonctions Terraform](./photosFormationTerraform/128_siteTerraformFunctions.png)  

Terraform permet de tester les fonctions via une console : "Terraform console"  

```text
[adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ echo 'split(",", "foo,bar,baz")' | terraform console
tolist([
  "foo",
  "bar",
  "baz",
])
```

ou encore  

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform console
> max(10,9,24)
24
>
```

### LAB9 - creation de ressources en utilisant des fonctions (fichier "functions.tf")  

Creation de :  

- 2 instances dont le nom est "app-dev"  
- ami      = ami-0470e33cd681b2476  
- type     = t2.micro  
- key_name = "login-key" dont valeur = "hello EAZYTraining"  
- tag      = tag1 = "firstec2", tag2 = ""secondec2"  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

locals {
    time = formatdate("DD MM YYYY hh:mm ZZZ", timestamp())
}

variable "region" {
    default = "ap-south-1"
}

variable "tags" {
    type = list
    default = ["firstec2","secondec2"]
}

variable "ami" {
    type = map
    default = {
    "us-east-1"  = "ami-0323c3dd2da7fb37d"
    "us-west-2"  = "ami-0d6621c01e8c2de2c"
    "ap-south-1" = "ami-0470e33cd681b2476"
    }
}

resource "aws_key_pair" "loginkey" {
  key_name   = "login-key"
  public_key = file("${path.module}/id_rsa.pub")
}

resource "aws_instance" "app-dev" {                           
   ami = lookup(var.ami,var.region)                           
   instance_type = "t2.micro"                                 
   key_name = aws_key_pair.loginkey.key_name                  
   count = 2

   tags = {
     Name = element(var.tags,count.index)                     
   }
}

output "timestamp" {
  value = local.time
}
```

### LAB9 - creation fichier cle publique (bidon)  

```text
hello EAZYTraining
```

### LAB9 - terraform plan  

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.app-dev[0] will be created
  + resource "aws_instance" "app-dev" {
      + ami                                  = "ami-0470e33cd681b2476"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
.. / ..
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "login-key"
      + monitoring                           = (known after apply)
.. / ..
      + tags                                 = {
          + "Name" = "firstec2"
        }
      + tags_all                             = {
          + "Name" = "firstec2"
        }
.. / ..
    }

  # aws_instance.app-dev[1] will be created
  + resource "aws_instance" "app-dev" {
      + ami                                  = "ami-0470e33cd681b2476"
      + arn                                  = (known after apply)
.. / ..
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
.. / ..
      + tags                                 = {
          + "Name" = "secondec2"
        }
      + tags_all                             = {
          + "Name" = "secondec2"
        }
.. / ..
    }

  # aws_key_pair.loginkey will be created
  + resource "aws_key_pair" "loginkey" {
      + arn             = (known after apply)
      + fingerprint     = (known after apply)
      + id              = (known after apply)
      + key_name        = "login-key"
      + key_name_prefix = (known after apply)
      + key_pair_id     = (known after apply)
      + key_type        = (known after apply)
      + public_key      = "hello EAZYTraining"
      + tags_all        = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + timestamp = (known after apply)
```

[log terraform plan full](./logs/LAB9_terraformPlanFull.md)  

## **Dynamic Bloc**  

### Dynamic bloc : cas d'usage  

Utile lorsqu'on souhaite répéter un bloc dans une section.  
Permet de réduire la quantité de code.  
Facilite la lisibilité du code.  

![exemple Dynamic Bloc](./photosFormationTerraform/129_exempleDynamicBloc.png)  

### LAB10 - creation Security Group avec plusieurs ports (genre "boucle for")  

```text
provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

variable "sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [8200, 8201,8300, 9200, 9500]
}

resource "aws_security_group" "dynamicsg" {       => creation d'un sg avec ouverture ingress/egress pour les ports definis dans variable sg_ports 
  name        = "dynamic-sg"
  description = "Ingress for Vault"

  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = var.sg_ports
    iterator = egress
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}
```

### LAB10 - terraform plan  

[log terraform plan](./logs/LAB10_dynamicBloc.md)  

## **Tainting resources**  

### Tainting resources : cas d'usage  

Imaginons qu'une ressource ait été créée et que plusieurs modifications aient lieu qui la rendent complètement différente, voire plus du tout fonctionnelle.  
L'idée est de dire qu'au prochain "terraform apply", si terraform trouve cette ressource, on demande à Terraform de la supprimer et de la recréer à son état d'origine.  

### LAB11 - Tainting resources : fichier "taint.tf"  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/data/adm5224opit/private/terraform/terraformBase/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "myec2" {
  ami           = "ami-012cc038cc685a0d7"
  instance_type = "t2.micro"
  key_name      = "devops-jmi"
  tags = {
    Name = "ec2-jmi-new"
  }
}
```

### LAB11 - Tainting resources : création initiale de la ressource  

[création initiale de la ressource](./logs/LAB11_creationRessourceInitiale.md)  

### LAB11 - Visualisation de la ressource ec2 créée  

![Visualisation de la ressource ec2 créée](./photosFormationTerraform/130_creationInitialeRessource.png)  

### LAB11 - Modification de la ressource ec2 créée  

![Arret de la ressource ec2](./photosFormationTerraform/131_arretManuelRessource.png)  

![Visualisation de la ressource ec2 arretee](./photosFormationTerraform/132_visuArretManuelRessource.png)  

![Visualisation de la ressource ec2 arretee](./photosFormationTerraform/133_modificationRessource.png)  

![Visualisation de la ressource ec2 arretee](./photosFormationTerraform/134_modificationRessource2.png)  

### LAB11 - Visualisation de la ressource ec2 modifiée  

![Visualisation de la ressource ec2 arretee](./photosFormationTerraform/135_ressourceModifiee.png)  

### LAB11 - Tainting resources : taint de la ressource  

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform taint aws_instance.myec2
Resource instance aws_instance.myec2 has been marked as tainted.
```

### LAB11 - Tainting resources : terraform plan numéro 2  => recréation ec2 origine  

```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform plan
aws_instance.myec2: Refreshing state... [id=i-038326a3cd58ba861]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # aws_instance.myec2 is tainted, so must be replaced
-/+ resource "aws_instance" "myec2" {
      ~ arn                                  = "arn:aws:ec2:us-east-1:026071546831:instance/i-038326a3cd58ba861" -> (known after apply)
      ~ associate_public_ip_address          = false -> (known after apply)
      ~ availability_zone                    = "us-east-1b" -> (known after apply)
      ~ cpu_core_count                       = 1 -> (known after apply)
      ~ cpu_threads_per_core                 = 1 -> (known after apply)
      ~ disable_api_stop                     = false -> (known after apply)
      ~ disable_api_termination              = false -> (known after apply)
      ~ ebs_optimized                        = false -> (known after apply)
      - hibernation                          = false -> null
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      ~ id                                   = "i-038326a3cd58ba861" -> (known after apply)
      ~ instance_initiated_shutdown_behavior = "stop" -> (known after apply)
      ~ instance_state                       = "stopped" -> (known after apply)
      ~ instance_type                        = "t2.nano" -> "t2.micro"
      ~ ipv6_address_count                   = 0 -> (known after apply)
.. / ..
    }

Plan: 1 to add, 0 to change, 1 to destroy.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run "terraform apply"
now.
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform apply
aws_instance.myec2: Refreshing state... [id=i-038326a3cd58ba861]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # aws_instance.myec2 is tainted, so must be replaced
-/+ resource "aws_instance" "myec2" {
      ~ arn                                  = "arn:aws:ec2:us-east-1:026071546831:instance/i-038326a3cd58ba861" -> (known after apply)
      ~ associate_public_ip_address          = false -> (known after apply)
      ~ availability_zone                    = "us-east-1b" -> (known after apply)
      ~ cpu_core_count                       = 1 -> (known after apply)
      ~ cpu_threads_per_core                 = 1 -> (known after apply)
      ~ disable_api_stop                     = false -> (known after apply)
      ~ disable_api_termination              = false -> (known after apply)
      ~ ebs_optimized                        = false -> (known after apply)
      - hibernation                          = false -> null
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      ~ id                                   = "i-038326a3cd58ba861" -> (known after apply)
      ~ instance_initiated_shutdown_behavior = "stop" -> (known after apply)
      ~ instance_state                       = "stopped" -> (known after apply)
      ~ instance_type                        = "t2.nano" -> "t2.micro"
      ~ ipv6_address_count                   = 0 -> (known after apply)
../..            
```

[terraform plan 2](./logs/LAB11_terraformPlan2.md)  

![Visualisation de la ressource ec2 arretee](./photosFormationTerraform/136_ressourceRecree.png)  

## **Splat Expression**  

### Splat Expression : cas d'usage  

Plusieurs utilisateurs à créer pour lesquels on doit aafficher l'ARN qui est l'ID unique de la ressource à créer.  
On va utiliser un élément qui va parcourir l'ensemble des utilisateurs qui ont été créés.  

### LAB12 - splat.tf  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_iam_user" "lb" {
  name  = "iamuser.${count.index}"
  count = 3
  path  = "/system/"
}

output "arns" {
  value = aws_iam_user.lb[*].arn            =>n utilisation du caractere "*" pour compter
}
```

### LAB12 - terraform plan  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB12$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_iam_user.lb[0] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "iamuser.0"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # aws_iam_user.lb[1] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "iamuser.1"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # aws_iam_user.lb[2] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "iamuser.2"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + arns = [
      + (known after apply),
      + (known after apply),
      + (known after apply),
    ]
```

### LAB12 - terraform apply  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB12$ terraform apply

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_iam_user.lb[0] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "iamuser.0"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # aws_iam_user.lb[1] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "iamuser.1"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # aws_iam_user.lb[2] will be created
  + resource "aws_iam_user" "lb" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "iamuser.2"
      + path          = "/system/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + arns = [
      + (known after apply),
      + (known after apply),
      + (known after apply),
    ]

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_iam_user.lb[1]: Creating...
aws_iam_user.lb[2]: Creating...
aws_iam_user.lb[0]: Creating...
aws_iam_user.lb[1]: Creation complete after 1s [id=iamuser.1]
aws_iam_user.lb[2]: Creation complete after 1s [id=iamuser.2]
aws_iam_user.lb[0]: Creation complete after 1s [id=iamuser.0]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

Outputs:

arns = [
  "arn:aws:iam::026071546831:user/system/iamuser.0",
  "arn:aws:iam::026071546831:user/system/iamuser.1",
  "arn:aws:iam::026071546831:user/system/iamuser.2",
]
```

## **Terraform graph**  

### Terraform graph : cas d'usage  

Lorsque plusieurs ressources ont été créées, il peut être nécessaire d'avoir une vue d'ensemble des dépendances, d'organisation, etc ...  

![terraform graph](./photosFormationTerraform/137_terraformGraphFormation.png)  

### LAB13 - creation de ressources : fichier conditional.tf  

```text
provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}


resource "aws_instance" "dev" {
   ami = "ami-012cc038cc685a0d7"
   instance_type = "t2.micro"
   count = var.istest == true ? 3 : 0
}

resource "aws_instance" "prod" {
   ami = "ami-012cc038cc685a0d7"
   instance_type = "t2.large"
   count = var.istest == false ? 1 : 0
}

variable "istest" {}
```

### LAB13 - creation de ressources : fichier terraform.tfvars  

```text
istest = true
```

### LAB13 - terraform apply  

[terraform apply](./logs/LAB13_terraformApply.md)  

### LAB13 - visualisation des ressources créées  

![creationToutesRessources](./photosFormationTerraform/138_creationToutesRessources.png)  

![creationInstances](./photosFormationTerraform/139_instancesCreees.png)  

![creationVolumes](./photosFormationTerraform/140_volumesCrees.png)  

### LAB13 - Installation utilitaire Graphviz  

```text
sudo apt install graphviz -y
```

[installation Graphviz](./logs/LAB13_installationGraphviz.md)  

### LAB13 - Création du graphe  

```text
jmi@portable-jmi:~/Documents/formationTerraformCertification/labs/LAB13$ terraform graph | dot -Tsvg > graph.svg
```

### LAB13 - Visualisation du graphe  

Ouvrir le fichier ".svg" avec un navigateur.  

![visuGraphe](./photosFormationTerraform/141_visuGraphe.png)  
