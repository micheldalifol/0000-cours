# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.30.0"
  constraints = ">= 2.10.0, <= 2.30.0"
  hashes = [
    "h1:vHtCq2IrW1T2kItx3sbgt6YQhDs0+StPYBf0qyFwBi8=",
    "zh:04ee56b7f4e94681c505c89cf73aa17c40def05b5ecd8d2817b757c41f3e680c",
    "zh:0ff90e1c3c1459bd7c74b8fd9ac8eee4a4752891000b33a5ce1eb370db729494",
    "zh:1a3686a6f5d938fa27b77eccefcbd9da611851c18d79e94381c62fb151b47315",
    "zh:1eaa164b1cbbc55e3a8942b218b3044d56eeeb38b0dc6f3dd9b338b30a4a3feb",
    "zh:28952c1fa144159d3b234199755eee166f21ad4e2e95ed829b022f3085cee052",
    "zh:2c4337316acd375fd33a72478d49e0722ad4ae6cc099dab8b6a7d7018d54f672",
    "zh:2cc0c2b56ed86464e9bf97da64cab2902ea39dbe83120ad67165dc1d4cb7ac15",
    "zh:43c4a1b022c3d490b23180a38cc375385084bd2911e6b1e01a22700e543f9d15",
    "zh:5fb10b4646b059c8e8e8e5eb6d7cd63648c49d72dab8df718cc3dd5e89a12b6c",
    "zh:92f9e201931d7e926a9878beac43ac11009ebfa27de3afb99db81dae4649d8f1",
    "zh:c9ad2babe508af04cd809871c233132c99ce7528f56790bff35bdf30cf07d76b",
    "zh:ee258263e63e0c2da568a7eed0960db5dc36157f2c745a2b6d49d0833fef5883",
  ]
}
