provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
  version    = ">=2.10,<=2.30"
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  key_name      = "devops-jmi"
  tags = {
    name = "ec2-jmi"
  }
  root_block_device {
    delete_on_termination = true
  }
}