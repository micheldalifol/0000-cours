provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = var.list[1]
  
  root_block_device {
    delete_on_termination = true
  }
}
  variable "list" {
    type = list
    default = ["m5.large","m5.xlarge","t2.medium"]
  }

  variable "types" {
    type = map
    default = {
      us-east-1  = "t2.micro"
      us-west-2  = "t2.nano"
      us-south-1 = "t2.small"
    }
  }