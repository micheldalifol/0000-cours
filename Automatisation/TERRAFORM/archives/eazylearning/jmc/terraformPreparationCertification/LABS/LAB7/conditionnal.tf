provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "dev" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  count         = var.istest == true ? 3 : 0
}

resource "aws_instance" "prod" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.large"
  count         = var.istest == false ? 1 : 0
}

variable "istest" {}