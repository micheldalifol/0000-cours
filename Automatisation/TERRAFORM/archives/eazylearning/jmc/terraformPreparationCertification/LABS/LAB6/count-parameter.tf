provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "myec2" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  count         = 3
}