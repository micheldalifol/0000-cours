provider "aws" {
  region     = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_eip" "lb" {
  vpc = true
}

output "eip" {
  value = "aws_eip.lb"
}

resource "aws_s3_bucket" "mys3" {
  bucket = "jmi-bucket"
}

output "mys3bucket" {
  value = aws_s3_bucket.mys3
}
