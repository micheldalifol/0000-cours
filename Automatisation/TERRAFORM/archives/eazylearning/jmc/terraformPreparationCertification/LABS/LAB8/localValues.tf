provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

locals {
  common_tags = {
    Owner   = "EAZYTreaning"
    service = "backend"
  }
}

resource "aws_instance" "app-dev" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  tags          = local.common_tags
}

resource "aws_instance" "db-dev" {
  ami           = "ami-0b5eea76982371e91"
  instance_type = "t2.micro"
  tags          = local.common_tags
}

resource "aws_ebs_volume" "db-ebs" {
  availability_zone = "us-west-2a"
  size              = 8
  tags              = local.common_tags
}