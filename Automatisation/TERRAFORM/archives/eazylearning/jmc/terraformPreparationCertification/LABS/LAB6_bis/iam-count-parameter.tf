provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

variable "elb_names" {
  type = list
  default = ["dev-loadbalancer","stage-loadbalancer","prod-loadbalancer"]
}

resource "aws_iam_user" "lb" {
  name  = var.elb_names[count.index]
  count = 3
  path  = "/system/"
}