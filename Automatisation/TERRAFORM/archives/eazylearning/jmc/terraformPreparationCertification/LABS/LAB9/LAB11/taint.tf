provider "aws" {
  region                   = "us-east-1"
  shared_credentials_files = ["/media/jmi/JMCEMTEC/Formation/terraformPreparationCertification/fichiersSecurises/accessKeyEtSecretKey.md"]
}

resource "aws_instance" "myec2" {
  ami           = "ami-012cc038cc685a0d7"
  instance_type = "t2.micro"
  key_name      = "devops-jmi"
  tags = {
    Name = "ec2-jmi-new"
  }
}