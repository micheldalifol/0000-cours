variable "maintainer" {
  type    = string
  default = "jmi"
}

variable "disk_size" {
  type    = number
  default = 2
}

variable "AZ" {
  type    = string
  default = "us-east-1b"
}