```text
jmi@portable-jmi:~/Documents/formationTerraform/tp4$ terraform apply "plan.out"
aws_security_group.allow_ssh_http_https: Creating...
aws_security_group.allow_ssh_http_https: Creation complete after 3s [id=sg-0fe0d4875e4bf551d]
aws_instance.myec2: Creating...
aws_instance.myec2: Still creating... [10s elapsed]
aws_instance.myec2: Still creating... [20s elapsed]
aws_instance.myec2: Still creating... [30s elapsed]
aws_instance.myec2: Provisioning with 'local-exec'...
aws_instance.myec2 (local-exec): Executing: ["/bin/sh" "-c" "echo PUBLIC_IP: 3.82.249.134 ID: i-09d58b4845e330df2 AZ: us-east-1c >> ./infos_ec2.txt"]
aws_instance.myec2: Provisioning with 'remote-exec'...
aws_instance.myec2 (remote-exec): Connecting to remote host via SSH...
aws_instance.myec2 (remote-exec):   Host: 3.82.249.134
aws_instance.myec2 (remote-exec):   User: ec2-user
aws_instance.myec2 (remote-exec):   Password: false
aws_instance.myec2 (remote-exec):   Private key: true
aws_instance.myec2 (remote-exec):   Certificate: false
aws_instance.myec2 (remote-exec):   SSH Agent: true
aws_instance.myec2 (remote-exec):   Checking Host Key: false
aws_instance.myec2 (remote-exec):   Target Platform: unix
aws_instance.myec2 (remote-exec): Connected!
aws_instance.myec2 (remote-exec): Topic nginx1.12 has end-of-support date of 2019-09-20
aws_instance.myec2 (remote-exec): Installing nginx
aws_instance.myec2: Still creating... [40s elapsed]
aws_instance.myec2 (remote-exec): Loaded plugins: extras_suggestions,
aws_instance.myec2 (remote-exec):               : langpacks, priorities,
aws_instance.myec2 (remote-exec):               : update-motd
aws_instance.myec2 (remote-exec): Existing lock /var/run/yum.pid: another copy is running as pid 3083.
aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
aws_instance.myec2 (remote-exec):   The other application is: yum
aws_instance.myec2 (remote-exec):     Memory : 171 M RSS (464 MB VSZ)
aws_instance.myec2 (remote-exec):     Started: Thu Jan 19 16:14:06 2023 - 00:10 ago
aws_instance.myec2 (remote-exec):     State  : Running, pid: 3083
aws_instance.myec2 (remote-exec): Cleaning repos: amzn2-core
aws_instance.myec2 (remote-exec):      ...: amzn2extra-docker
aws_instance.myec2 (remote-exec):      ...: amzn2extra-nginx1.12
aws_instance.myec2 (remote-exec): 11 metadata files removed
aws_instance.myec2 (remote-exec): 4 sqlite files removed
aws_instance.myec2 (remote-exec): 0 metadata files removed
aws_instance.myec2 (remote-exec): Loaded plugins: extras_suggestions,
aws_instance.myec2 (remote-exec):               : langpacks, priorities,
aws_instance.myec2 (remote-exec):               : update-motd
aws_instance.myec2 (remote-exec): Existing lock /var/run/yum.pid: another copy is running as pid 3110.
aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
aws_instance.myec2 (remote-exec):   The other application is: yum
aws_instance.myec2 (remote-exec):     Memory :  36 M RSS (329 MB VSZ)
aws_instance.myec2 (remote-exec):     Started: Thu Jan 19 16:14:06 2023 - 00:13 ago
aws_instance.myec2 (remote-exec):     State  : Sleeping, pid: 3110
aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
aws_instance.myec2 (remote-exec):   The other application is: yum
aws_instance.myec2 (remote-exec):     Memory :  42 M RSS (335 MB VSZ)
aws_instance.myec2 (remote-exec):     Started: Thu Jan 19 16:14:06 2023 - 00:15 ago
aws_instance.myec2 (remote-exec):     State  : Running, pid: 3110
aws_instance.myec2 (remote-exec): Another app is currently holding the yum lock; waiting for it to exit...
aws_instance.myec2 (remote-exec):   The other application is: yum
aws_instance.myec2 (remote-exec):     Memory : 112 M RSS (403 MB VSZ)
aws_instance.myec2 (remote-exec):     Started: Thu Jan 19 16:14:06 2023 - 00:17 ago
aws_instance.myec2 (remote-exec):     State  : Running, pid: 3110
aws_instance.myec2 (remote-exec): amzn2extra-nginx | 1.3 kB     00:00
aws_instance.myec2 (remote-exec): amzn2extra-nginx1. |  23 kB   00:00
aws_instance.myec2 (remote-exec): Resolving Dependencies
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package nginx.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-filesystem = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-all-modules = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-filesystem for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libprofiler.so.0()(64bit) for package: 1:nginx-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package gperftools-libs.x86_64 0:2.6.1-1.amzn2 will be installed
aws_instance.myec2 (remote-exec): ---> Package nginx-all-modules.noarch 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-stream = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-mail = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-xslt-filter = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-perl = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
aws_instance.myec2: Still creating... [50s elapsed]
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-image-filter = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
aws_instance.myec2 (remote-exec): --> Processing Dependency: nginx-mod-http-geoip = 1:1.12.2-2.amzn2.0.2 for package: 1:nginx-all-modules-1.12.2-2.amzn2.0.2.noarch
aws_instance.myec2 (remote-exec): ---> Package nginx-filesystem.noarch 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-geoip.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-image-filter.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: gd for package: 1:nginx-mod-http-image-filter-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libgd.so.2()(64bit) for package: 1:nginx-mod-http-image-filter-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-perl.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): ---> Package nginx-mod-http-xslt-filter.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: libxslt.so.1(LIBXML2_1.0.18)(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libxslt.so.1(LIBXML2_1.0.11)(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libxslt.so.1()(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libexslt.so.0()(64bit) for package: 1:nginx-mod-http-xslt-filter-1.12.2-2.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): ---> Package nginx-mod-mail.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): ---> Package nginx-mod-stream.x86_64 1:1.12.2-2.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package gd.x86_64 0:2.0.35-27.amzn2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: libfontconfig.so.1()(64bit) for package: gd-2.0.35-27.amzn2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libXpm.so.4()(64bit) for package: gd-2.0.35-27.amzn2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libX11.so.6()(64bit) for package: gd-2.0.35-27.amzn2.x86_64
aws_instance.myec2 (remote-exec): ---> Package libxslt.x86_64 0:1.1.28-6.amzn2 will be installed
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package fontconfig.x86_64 0:2.13.0-4.3.amzn2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: fontpackages-filesystem for package: fontconfig-2.13.0-4.3.amzn2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: dejavu-sans-fonts for package: fontconfig-2.13.0-4.3.amzn2.x86_64
aws_instance.myec2 (remote-exec): ---> Package libX11.x86_64 0:1.6.7-3.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: libX11-common >= 1.6.7-3.amzn2.0.2 for package: libX11-1.6.7-3.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Processing Dependency: libxcb.so.1()(64bit) for package: libX11-1.6.7-3.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): ---> Package libXpm.x86_64 0:3.5.12-1.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package dejavu-sans-fonts.noarch 0:2.33-6.amzn2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: dejavu-fonts-common = 2.33-6.amzn2 for package: dejavu-sans-fonts-2.33-6.amzn2.noarch
aws_instance.myec2 (remote-exec): ---> Package fontpackages-filesystem.noarch 0:1.44-8.amzn2 will be installed
aws_instance.myec2 (remote-exec): ---> Package libX11-common.noarch 0:1.6.7-3.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): ---> Package libxcb.x86_64 0:1.12-1.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Processing Dependency: libXau.so.6()(64bit) for package: libxcb-1.12-1.amzn2.0.2.x86_64
aws_instance.myec2 (remote-exec): --> Running transaction check
aws_instance.myec2 (remote-exec): ---> Package dejavu-fonts-common.noarch 0:2.33-6.amzn2 will be installed
aws_instance.myec2 (remote-exec): ---> Package libXau.x86_64 0:1.0.8-2.1.amzn2.0.2 will be installed
aws_instance.myec2 (remote-exec): --> Finished Dependency Resolution

aws_instance.myec2 (remote-exec): Dependencies Resolved

aws_instance.myec2 (remote-exec): ========================================
aws_instance.myec2 (remote-exec):  Package
aws_instance.myec2 (remote-exec):     Arch   Version
aws_instance.myec2 (remote-exec):              Repository            Size
aws_instance.myec2 (remote-exec): ========================================
aws_instance.myec2 (remote-exec): Installing:
aws_instance.myec2 (remote-exec):  nginx
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12 533 k
aws_instance.myec2 (remote-exec): Installing for dependencies:
aws_instance.myec2 (remote-exec):  dejavu-fonts-common
aws_instance.myec2 (remote-exec):     noarch 2.33-6.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core            64 k
aws_instance.myec2 (remote-exec):  dejavu-sans-fonts
aws_instance.myec2 (remote-exec):     noarch 2.33-6.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core           1.4 M
aws_instance.myec2 (remote-exec):  fontconfig
aws_instance.myec2 (remote-exec):     x86_64 2.13.0-4.3.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core           253 k
aws_instance.myec2 (remote-exec):  fontpackages-filesystem
aws_instance.myec2 (remote-exec):     noarch 1.44-8.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core            10 k
aws_instance.myec2 (remote-exec):  gd x86_64 2.0.35-27.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core           146 k
aws_instance.myec2 (remote-exec):  gperftools-libs
aws_instance.myec2 (remote-exec):     x86_64 2.6.1-1.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core           274 k
aws_instance.myec2 (remote-exec):  libX11
aws_instance.myec2 (remote-exec):     x86_64 1.6.7-3.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2-core           606 k
aws_instance.myec2 (remote-exec):  libX11-common
aws_instance.myec2 (remote-exec):     noarch 1.6.7-3.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2-core           165 k
aws_instance.myec2 (remote-exec):  libXau
aws_instance.myec2 (remote-exec):     x86_64 1.0.8-2.1.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2-core            29 k
aws_instance.myec2 (remote-exec):  libXpm
aws_instance.myec2 (remote-exec):     x86_64 3.5.12-1.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2-core            57 k
aws_instance.myec2 (remote-exec):  libxcb
aws_instance.myec2 (remote-exec):     x86_64 1.12-1.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2-core           216 k
aws_instance.myec2 (remote-exec):  libxslt
aws_instance.myec2 (remote-exec):     x86_64 1.1.28-6.amzn2
aws_instance.myec2 (remote-exec):              amzn2-core           240 k
aws_instance.myec2 (remote-exec):  nginx-all-modules
aws_instance.myec2 (remote-exec):     noarch 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  17 k
aws_instance.myec2 (remote-exec):  nginx-filesystem
aws_instance.myec2 (remote-exec):     noarch 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  17 k
aws_instance.myec2 (remote-exec):  nginx-mod-http-geoip
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  24 k
aws_instance.myec2 (remote-exec):  nginx-mod-http-image-filter
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  27 k
aws_instance.myec2 (remote-exec):  nginx-mod-http-perl
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  37 k
aws_instance.myec2 (remote-exec):  nginx-mod-http-xslt-filter
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  26 k
aws_instance.myec2 (remote-exec):  nginx-mod-mail
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  55 k
aws_instance.myec2 (remote-exec):  nginx-mod-stream
aws_instance.myec2 (remote-exec):     x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):              amzn2extra-nginx1.12  76 k

aws_instance.myec2 (remote-exec): Transaction Summary
aws_instance.myec2 (remote-exec): ========================================
aws_instance.myec2 (remote-exec): Install  1 Package (+20 Dependent packages)

aws_instance.myec2 (remote-exec): Total download size: 4.2 M
aws_instance.myec2 (remote-exec): Installed size: 14 M
aws_instance.myec2 (remote-exec): Downloading packages:
aws_instance.myec2 (remote-exec): (1/21): dejavu-fon |  64 kB   00:00
aws_instance.myec2 (remote-exec): (2/21): fontconfig | 253 kB   00:00
aws_instance.myec2 (remote-exec): (3/21): dejavu-san | 1.4 MB   00:00
aws_instance.myec2 (remote-exec): (4/21): fontpackag |  10 kB   00:00
aws_instance.myec2 (remote-exec): (5/21): gd-2.0.35- | 146 kB   00:00
aws_instance.myec2 (remote-exec): (6/21): gperftools | 274 kB   00:00
aws_instance.myec2 (remote-exec): (7/21): libX11-com | 165 kB   00:00
aws_instance.myec2 (remote-exec): (8/21): libX11-1.6 | 606 kB   00:00
aws_instance.myec2 (remote-exec): (9/21): libXau-1.0 |  29 kB   00:00
aws_instance.myec2 (remote-exec): (10/21): libxcb-1. | 216 kB   00:00
aws_instance.myec2 (remote-exec): (11/21): libXpm-3. |  57 kB   00:00
aws_instance.myec2 (remote-exec): (12/21): libxslt-1 | 240 kB   00:00
aws_instance.myec2 (remote-exec): (13/21): nginx-all |  17 kB   00:00
aws_instance.myec2 (remote-exec): (14/21): nginx-1.1 | 533 kB   00:00
aws_instance.myec2 (remote-exec): (15/21): nginx-fil |  17 kB   00:00
aws_instance.myec2 (remote-exec): (16/21): nginx-mod |  24 kB   00:00
aws_instance.myec2 (remote-exec): (17/21): nginx-mod |  27 kB   00:00
aws_instance.myec2 (remote-exec): (18/21): nginx-mod |  37 kB   00:00
aws_instance.myec2 (remote-exec): (19/21): nginx-mod |  26 kB   00:00
aws_instance.myec2 (remote-exec): (20/21): nginx-mod |  55 kB   00:00
aws_instance.myec2 (remote-exec): (21/21): nginx-mod |  76 kB   00:00
aws_instance.myec2 (remote-exec): ----------------------------------------
aws_instance.myec2 (remote-exec): Total      9.4 MB/s | 4.2 MB  00:00
aws_instance.myec2 (remote-exec): Running transaction check
aws_instance.myec2 (remote-exec): Running transaction test
aws_instance.myec2 (remote-exec): Transaction test succeeded
aws_instance.myec2 (remote-exec): Running transaction
aws_instance.myec2 (remote-exec):   Installing : fontpac [        ]  1/21
aws_instance.myec2 (remote-exec):   Installing : fontpac [#       ]  1/21
aws_instance.myec2 (remote-exec):   Installing : fontpac [##      ]  1/21
aws_instance.myec2 (remote-exec):   Installing : fontpac [###     ]  1/21
aws_instance.myec2 (remote-exec):   Installing : fontpac [#####   ]  1/21
aws_instance.myec2 (remote-exec):   Installing : fontpac [######  ]  1/21
aws_instance.myec2 (remote-exec):   Installing : fontpackages-fil    1/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [        ]  2/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [####    ]  2/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [######  ]  2/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [####### ]  2/21
aws_instance.myec2 (remote-exec):   Installing : dejavu-fonts-com    2/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [        ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [#       ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [##      ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [###     ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [####    ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [#####   ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [######  ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu- [####### ]  3/21
aws_instance.myec2 (remote-exec):   Installing : dejavu-sans-font    3/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [        ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [#       ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [##      ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [###     ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [####    ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [#####   ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [######  ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontcon [####### ]  4/21
aws_instance.myec2 (remote-exec):   Installing : fontconfig-2.13.    4/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [        ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [#       ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [##      ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [###     ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [####    ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [#####   ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [######  ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [####### ]  5/21
aws_instance.myec2 (remote-exec):   Installing : libX11-common-1.    5/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ]  6/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#       ]  6/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [##      ]  6/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####    ]  6/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#####   ]  6/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [######  ]  6/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-filesyst    6/21
aws_instance.myec2 (remote-exec):   Installing : libXau- [        ]  7/21
aws_instance.myec2 (remote-exec):   Installing : libXau- [##      ]  7/21
aws_instance.myec2 (remote-exec):   Installing : libXau- [######  ]  7/21
aws_instance.myec2 (remote-exec):   Installing : libXau- [####### ]  7/21
aws_instance.myec2 (remote-exec):   Installing : libXau-1.0.8-2.1    7/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [        ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [#       ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [##      ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [###     ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [####    ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [#####   ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [######  ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb- [####### ]  8/21
aws_instance.myec2 (remote-exec):   Installing : libxcb-1.12-1.am    8/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [        ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [#       ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [##      ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [###     ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [####    ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [#####   ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [######  ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11- [####### ]  9/21
aws_instance.myec2 (remote-exec):   Installing : libX11-1.6.7-3.a    9/21
aws_instance.myec2 (remote-exec):   Installing : libXpm- [        ] 10/21
aws_instance.myec2 (remote-exec):   Installing : libXpm- [####    ] 10/21
aws_instance.myec2 (remote-exec):   Installing : libXpm- [#####   ] 10/21
aws_instance.myec2 (remote-exec):   Installing : libXpm- [####### ] 10/21
aws_instance.myec2 (remote-exec):   Installing : libXpm-3.5.12-1.   10/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [        ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [#       ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [##      ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [###     ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [####    ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [#####   ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [######  ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0. [####### ] 11/21
aws_instance.myec2 (remote-exec):   Installing : gd-2.0.35-27.amz   11/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [        ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [#       ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [##      ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [###     ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [####    ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [#####   ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [######  ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt [####### ] 12/21
aws_instance.myec2 (remote-exec):   Installing : libxslt-1.1.28-6   12/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [        ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [#       ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [##      ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [###     ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [####    ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [#####   ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [######  ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperfto [####### ] 13/21
aws_instance.myec2 (remote-exec):   Installing : gperftools-libs-   13/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 14/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 14/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   14/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 15/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 15/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   15/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 16/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [###     ] 16/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 16/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   16/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 17/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [###     ] 17/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [######  ] 17/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 17/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-stre   17/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 18/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####    ] 18/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 18/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-mail   18/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#       ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [##      ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [###     ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####    ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [#####   ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [######  ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-1.12.2-2   19/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 20/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [####### ] 20/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-mod-http   20/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx [        ] 21/21
aws_instance.myec2 (remote-exec):   Installing : 1:nginx-all-modu   21/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http    1/21
aws_instance.myec2 (remote-exec):   Verifying  : gperftools-libs-    2/21
aws_instance.myec2 (remote-exec):   Verifying  : libxslt-1.1.28-6    3/21
aws_instance.myec2 (remote-exec):   Verifying  : dejavu-fonts-com    4/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http    5/21
aws_instance.myec2 (remote-exec):   Verifying  : dejavu-sans-font    6/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-all-modu    7/21
aws_instance.myec2 (remote-exec):   Verifying  : libXau-1.0.8-2.1    8/21
aws_instance.myec2 (remote-exec):   Verifying  : fontconfig-2.13.    9/21
aws_instance.myec2 (remote-exec):   Verifying  : fontpackages-fil   10/21
aws_instance.myec2 (remote-exec):   Verifying  : libX11-1.6.7-3.a   11/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http   12/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-http   13/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-stre   14/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-mod-mail   15/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-filesyst   16/21
aws_instance.myec2 (remote-exec):   Verifying  : 1:nginx-1.12.2-2   17/21
aws_instance.myec2 (remote-exec):   Verifying  : libX11-common-1.   18/21
aws_instance.myec2 (remote-exec):   Verifying  : libxcb-1.12-1.am   19/21
aws_instance.myec2 (remote-exec):   Verifying  : libXpm-3.5.12-1.   20/21
aws_instance.myec2 (remote-exec):   Verifying  : gd-2.0.35-27.amz   21/21

aws_instance.myec2 (remote-exec): Installed:
aws_instance.myec2 (remote-exec):   nginx.x86_64 1:1.12.2-2.amzn2.0.2

aws_instance.myec2 (remote-exec): Dependency Installed:
aws_instance.myec2 (remote-exec):   dejavu-fonts-common.noarch 0:2.33-6.amzn2
aws_instance.myec2 (remote-exec):   dejavu-sans-fonts.noarch 0:2.33-6.amzn2
aws_instance.myec2 (remote-exec):   fontconfig.x86_64 0:2.13.0-4.3.amzn2
aws_instance.myec2 (remote-exec):   fontpackages-filesystem.noarch 0:1.44-8.amzn2
aws_instance.myec2 (remote-exec):   gd.x86_64 0:2.0.35-27.amzn2
aws_instance.myec2 (remote-exec):   gperftools-libs.x86_64 0:2.6.1-1.amzn2
aws_instance.myec2 (remote-exec):   libX11.x86_64 0:1.6.7-3.amzn2.0.2
aws_instance.myec2 (remote-exec):   libX11-common.noarch 0:1.6.7-3.amzn2.0.2
aws_instance.myec2 (remote-exec):   libXau.x86_64 0:1.0.8-2.1.amzn2.0.2
aws_instance.myec2 (remote-exec):   libXpm.x86_64 0:3.5.12-1.amzn2.0.2
aws_instance.myec2 (remote-exec):   libxcb.x86_64 0:1.12-1.amzn2.0.2
aws_instance.myec2 (remote-exec):   libxslt.x86_64 0:1.1.28-6.amzn2
aws_instance.myec2 (remote-exec):   nginx-all-modules.noarch 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-filesystem.noarch 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-mod-http-geoip.x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-mod-http-image-filter.x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-mod-http-perl.x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-mod-http-xslt-filter.x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-mod-mail.x86_64 1:1.12.2-2.amzn2.0.2
aws_instance.myec2 (remote-exec):   nginx-mod-stream.x86_64 1:1.12.2-2.amzn2.0.2

aws_instance.myec2 (remote-exec): Complete!
aws_instance.myec2 (remote-exec):   0  ansible2                 available    \
aws_instance.myec2 (remote-exec):         [ =2.4.2  =2.4.6  =2.8  =stable ]
aws_instance.myec2 (remote-exec):   2  httpd_modules            available    [ =1.0  =stable ]
aws_instance.myec2 (remote-exec):   3  memcached1.5             available    \
aws_instance.myec2 (remote-exec):         [ =1.5.1  =1.5.16  =1.5.17 ]
aws_instance.myec2 (remote-exec):   4 *nginx1.12=latest         enabled      [ =1.12.2 ]
aws_instance.myec2 (remote-exec):   6  postgresql10             available    [ =10  =stable ]
aws_instance.myec2 (remote-exec):   9  R3.4                     available    [ =3.4.3  =stable ]
aws_instance.myec2 (remote-exec):  10  rust1                    available    \
aws_instance.myec2 (remote-exec):         [ =1.22.1  =1.26.0  =1.26.1  =1.27.2  =1.31.0  =1.38.0
aws_instance.myec2 (remote-exec):           =stable ]
aws_instance.myec2 (remote-exec):  18  libreoffice              available    \
aws_instance.myec2 (remote-exec):         [ =5.0.6.2_15  =5.3.6.1  =stable ]
aws_instance.myec2 (remote-exec):  19  gimp                     available    [ =2.8.22 ]
aws_instance.myec2 (remote-exec):  20  docker=latest            enabled      \
aws_instance.myec2 (remote-exec):         [ =17.12.1  =18.03.1  =18.06.1  =18.09.9  =stable ]
aws_instance.myec2 (remote-exec):  21  mate-desktop1.x          available    \
aws_instance.myec2 (remote-exec):         [ =1.19.0  =1.20.0  =stable ]
aws_instance.myec2 (remote-exec):  22  GraphicsMagick1.3        available    \
aws_instance.myec2 (remote-exec):         [ =1.3.29  =1.3.32  =1.3.34  =stable ]
aws_instance.myec2 (remote-exec):  23  tomcat8.5                available    \
aws_instance.myec2 (remote-exec):         [ =8.5.31  =8.5.32  =8.5.38  =8.5.40  =8.5.42  =8.5.50
aws_instance.myec2 (remote-exec):           =stable ]
aws_instance.myec2 (remote-exec):  24  epel                     available    [ =7.11  =stable ]
aws_instance.myec2 (remote-exec):  25  testing                  available    [ =1.0  =stable ]
aws_instance.myec2 (remote-exec):  26  ecs                      available    [ =stable ]
aws_instance.myec2 (remote-exec):  27  corretto8                available    \
aws_instance.myec2 (remote-exec):         [ =1.8.0_192  =1.8.0_202  =1.8.0_212  =1.8.0_222  =1.8.0_232
aws_instance.myec2 (remote-exec):           =1.8.0_242  =stable ]
aws_instance.myec2 (remote-exec):  29  golang1.11               available    \
aws_instance.myec2 (remote-exec):         [ =1.11.3  =1.11.11  =1.11.13  =stable ]
aws_instance.myec2 (remote-exec):  30  squid4                   available    [ =4  =stable ]
aws_instance.myec2 (remote-exec):  32  lustre2.10               available    \
aws_instance.myec2 (remote-exec):         [ =2.10.5  =2.10.8  =stable ]
aws_instance.myec2 (remote-exec):  33  java-openjdk11           available    [ =11  =stable ]
aws_instance.myec2 (remote-exec):  34  lynis                    available    [ =stable ]
aws_instance.myec2 (remote-exec):  36  BCC                      available    [ =0.x  =stable ]
aws_instance.myec2 (remote-exec):  37  mono                     available    [ =5.x  =stable ]
aws_instance.myec2 (remote-exec):  38  nginx1                   available    [ =stable ]
aws_instance.myec2 (remote-exec):  39  ruby2.6                  available    [ =2.6  =stable ]
aws_instance.myec2 (remote-exec):  40  mock                     available    [ =stable ]
aws_instance.myec2 (remote-exec):  41  postgresql11             available    [ =11  =stable ]
aws_instance.myec2 (remote-exec):  42  php7.4                   available    [ =stable ]
aws_instance.myec2 (remote-exec):  43  livepatch                available    [ =stable ]
aws_instance.myec2 (remote-exec):  44  python3.8                available    [ =stable ]
aws_instance.myec2 (remote-exec):  45  haproxy2                 available    [ =stable ]
aws_instance.myec2 (remote-exec):  46  collectd                 available    [ =stable ]
aws_instance.myec2 (remote-exec):  47  aws-nitro-enclaves-cli   available    [ =stable ]
aws_instance.myec2 (remote-exec):  48  R4                       available    [ =stable ]
aws_instance.myec2 (remote-exec):  49  kernel-5.4               available    [ =stable ]
aws_instance.myec2 (remote-exec):  50  selinux-ng               available    [ =stable ]
aws_instance.myec2 (remote-exec):  51  php8.0                   available    [ =stable ]
aws_instance.myec2 (remote-exec):  52  tomcat9                  available    [ =stable ]
aws_instance.myec2 (remote-exec):  53  unbound1.13              available    [ =stable ]
aws_instance.myec2 (remote-exec):  54  mariadb10.5              available    [ =stable ]
aws_instance.myec2 (remote-exec):  55  kernel-5.10              available    [ =stable ]
aws_instance.myec2 (remote-exec):  56  redis6                   available    [ =stable ]
aws_instance.myec2 (remote-exec):  57  ruby3.0                  available    [ =stable ]
aws_instance.myec2 (remote-exec):  58  postgresql12             available    [ =stable ]
aws_instance.myec2 (remote-exec):  59  postgresql13             available    [ =stable ]
aws_instance.myec2 (remote-exec):  60  mock2                    available    [ =stable ]
aws_instance.myec2 (remote-exec):  61  dnsmasq2.85              available    [ =stable ]
aws_instance.myec2 (remote-exec):  62  kernel-5.15              available    [ =stable ]
aws_instance.myec2 (remote-exec):  63  postgresql14             available    [ =stable ]
aws_instance.myec2 (remote-exec):  64  firefox                  available    [ =stable ]
aws_instance.myec2 (remote-exec):  65  lustre                   available    [ =stable ]
aws_instance.myec2 (remote-exec):  66  php8.1                   available    [ =stable ]
aws_instance.myec2 (remote-exec):  67  awscli1                  available    [ =stable ]
aws_instance.myec2 (remote-exec): * Extra topic has reached end of support.
aws_instance.myec2: Creation complete after 56s [id=i-09d58b4845e330df2]
aws_eip.lb: Creating...
aws_eip.lb: Creation complete after 2s [id=eipalloc-0d1bb6d4e048d4bc2]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.
jmi@portable-jmi:~/Documents/formationTerraform/tp4$ ll
total 56
drwxr-xr-x 4 jmi jmi  4096 janv. 19 17:14 ./
drwxrwxr-x 6 jmi jmi  4096 janv. 19 17:11 ../
-rw-r--r-- 1 jmi jmi  2154 janv. 19 17:07 ec2.tf
-rw-rw-r-- 1 jmi jmi    63 janv. 19 17:14 infos_ec2.txt
-rw-rw-r-- 1 jmi jmi  5474 janv. 19 17:13 plan.out
drwxr-xr-x 2 jmi jmi  4096 janv. 19 17:07 taf/
drwxr-xr-x 3 jmi jmi  4096 janv. 19 17:10 .terraform/
-rw-r--r-- 1 jmi jmi  1406 janv. 19 17:10 .terraform.lock.hcl
-rw-rw-r-- 1 jmi jmi 11571 janv. 19 17:14 terraform.tfstate
-rw-r--r-- 1 jmi jmi    68 janv. 19 17:07 terraform.tfvars
-rw-r--r-- 1 jmi jmi   244 janv. 19 17:07 variables.tf
jmi@portable-jmi:~/Documents/formationTerraform/tp4$ more infos_ec2.txt 
PUBLIC_IP: 3.82.249.134 ID: i-09d58b4845e330df2 AZ: us-east-1c
```