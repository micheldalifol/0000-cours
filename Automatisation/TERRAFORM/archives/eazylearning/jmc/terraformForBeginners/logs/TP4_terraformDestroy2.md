```text
(.venv) [adm5224opit@svlindus1 /data/adm5224opit/private/terraform/terraformBase/enCours]$ terraform destroy
data.aws_ami.app_ami: Reading...
aws_security_group.allow_ssh_http_https: Refreshing state... [id=sg-0511b8b9bbe132a08]
data.aws_ami.app_ami: Read complete after 1s [id=ami-0fe472d8a85bc7b0e]
aws_instance.myec2: Refreshing state... [id=i-05b860da231d3dc0d]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_instance.myec2 will be destroyed
  - resource "aws_instance" "myec2" {
      - ami                                  = "ami-0fe472d8a85bc7b0e" -> null
      - arn                                  = "arn:aws:ec2:us-east-1:026071546831:instance/i-05b860da231d3dc0d" -> null
      - associate_public_ip_address          = true -> null
      - availability_zone                    = "us-east-1c" -> null
      - cpu_core_count                       = 1 -> null
      - cpu_threads_per_core                 = 1 -> null
      - disable_api_stop                     = false -> null
      - disable_api_termination              = false -> null
      - ebs_optimized                        = false -> null
      - get_password_data                    = false -> null
      - hibernation                          = false -> null
      - id                                   = "i-05b860da231d3dc0d" -> null
      - instance_initiated_shutdown_behavior = "stop" -> null
      - instance_state                       = "running" -> null
      - instance_type                        = "t2.nano" -> null
      - ipv6_address_count                   = 0 -> null
      - ipv6_addresses                       = [] -> null
      - key_name                             = "devops-jmi" -> null
      - monitoring                           = false -> null
      - primary_network_interface_id         = "eni-0345f035d908b9e95" -> null
      - private_dns                          = "ip-172-31-91-152.ec2.internal" -> null
      - private_ip                           = "172.31.91.152" -> null
      - public_dns                           = "ec2-44-210-142-67.compute-1.amazonaws.com" -> null
      - public_ip                            = "44.210.142.67" -> null
      - secondary_private_ips                = [] -> null
      - security_groups                      = [
          - "jmi-sg",
        ] -> null
      - source_dest_check                    = true -> null
      - subnet_id                            = "subnet-09fff88dd6bad0055" -> null
      - tags                                 = {
          - "Name" = "ec2-jmi"
        } -> null
      - tags_all                             = {
          - "Name" = "ec2-jmi"
        } -> null
      - tenancy                              = "default" -> null
      - user_data_replace_on_change          = false -> null
      - vpc_security_group_ids               = [
          - "sg-0511b8b9bbe132a08",
        ] -> null

      - capacity_reservation_specification {
          - capacity_reservation_preference = "open" -> null
        }

      - credit_specification {
          - cpu_credits = "standard" -> null
        }

      - enclave_options {
          - enabled = false -> null
        }

      - maintenance_options {
          - auto_recovery = "default" -> null
        }

      - metadata_options {
          - http_endpoint               = "enabled" -> null
          - http_put_response_hop_limit = 1 -> null
          - http_tokens                 = "optional" -> null
          - instance_metadata_tags      = "disabled" -> null
        }

      - private_dns_name_options {
          - enable_resource_name_dns_a_record    = false -> null
          - enable_resource_name_dns_aaaa_record = false -> null
          - hostname_type                        = "ip-name" -> null
        }

      - root_block_device {
          - delete_on_termination = true -> null
          - device_name           = "/dev/xvda" -> null
          - encrypted             = false -> null
          - iops                  = 100 -> null
          - tags                  = {} -> null
          - throughput            = 0 -> null
          - volume_id             = "vol-0231443712f898828" -> null
          - volume_size           = 8 -> null
          - volume_type           = "gp2" -> null
        }
    }

  # aws_security_group.allow_ssh_http_https will be destroyed
  - resource "aws_security_group" "allow_ssh_http_https" {
      - arn                    = "arn:aws:ec2:us-east-1:026071546831:security-group/sg-0511b8b9bbe132a08" -> null
      - description            = "Allow http, https and ssh inbound/outbound traffic" -> null
      - egress                 = [
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = ""
              - from_port        = 0
              - ipv6_cidr_blocks = [
                  - "::/0",
                ]
              - prefix_list_ids  = []
              - protocol         = "-1"
              - security_groups  = []
              - self             = false
              - to_port          = 0
            },
        ] -> null
      - id                     = "sg-0511b8b9bbe132a08" -> null
      - ingress                = [
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "TLS from VPC"
              - from_port        = 443
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 443
            },
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "http from VPC"
              - from_port        = 80
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 80
            },
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "ssh from VPC"
              - from_port        = 22
              - ipv6_cidr_blocks = []
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 22
            },
        ] -> null
      - name                   = "jmi-sg" -> null
      - owner_id               = "026071546831" -> null
      - revoke_rules_on_delete = false -> null
      - tags                   = {} -> null
      - tags_all               = {} -> null
      - vpc_id                 = "vpc-047d4f6939fac5bcf" -> null
    }

Plan: 0 to add, 0 to change, 2 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

aws_instance.myec2: Destroying... [id=i-05b860da231d3dc0d]
aws_instance.myec2: Still destroying... [id=i-05b860da231d3dc0d, 10s elapsed]
aws_instance.myec2: Still destroying... [id=i-05b860da231d3dc0d, 20s elapsed]
aws_instance.myec2: Still destroying... [id=i-05b860da231d3dc0d, 30s elapsed]
aws_instance.myec2: Destruction complete after 31s
aws_security_group.allow_ssh_http_https: Destroying... [id=sg-0511b8b9bbe132a08]
aws_security_group.allow_ssh_http_https: Destruction complete after 1s

Destroy complete! Resources: 2 destroyed.

```