```text
jmi@portable-jmi:~$ cd Documents/formationTerraformForBeginners/tp8
bash: cd: Documents/formationTerraformForBeginners/tp8: Aucun fichier ou dossier de ce type
jmi@portable-jmi:~$ cd Documents/formationTerraformForBeginners/tp8_miniProjet/
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet$ terraform plan
╷
│ Error: No configuration files
│ 
│ Plan requires configuration to be present. Planning without a configuration would mark everything for destruction, which is normally not what is desired. If you
│ would like to destroy everything, run plan with the -destroy option. Otherwise, create a Terraform configuration file (.tf file) and try again.
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet$ terraform init
Terraform initialized in an empty directory!

The directory has no Terraform configuration files. You may begin working
with Terraform immediately by creating Terraform configuration files.
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet$ cd app/
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform init
Initializing modules...

Initializing the backend...

Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
╷
│ Error: Failed to query available provider packages
│ 
│ Could not retrieve the list of available versions for provider hashicorp/aws: could not connect to registry.terraform.io: Failed to request discovery document: Get
│ "https://registry.terraform.io/.well-known/terraform.json": dial tcp: lookup registry.terraform.io on 127.0.0.53:53: server misbehaving
╵

jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform init
Initializing modules...

Initializing the backend...

Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
- Using previously-installed hashicorp/aws v4.52.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform plan
╷
│ Error: Unsupported attribute
│ 
│   on main.tf line 26, in module "ec2":
│   26:     public_ip     = module.eip.output_eip
│     ├────────────────
│     │ module.eip is a object
│ 
│ This object does not have an attribute named "output_eip".
╵
╷
│ Error: Unsupported block type
│ 
│   on ../modules/ebs/main.tf line 4, in resource "aws_ebs_volume" "my_vol":
│    4:   tags {
│ 
│ Blocks of type "tags" are not expected here. Did you mean to define argument "tags"? If so, use the equals sign to assign it a value.
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform validate
╷
│ Error: Unsupported attribute
│ 
│   on main.tf line 26, in module "ec2":
│   26:     public_ip     = module.eip.output_eip
│     ├────────────────
│     │ module.eip is a object
│ 
│ This object does not have an attribute named "output_eip".
╵
╷
│ Error: Unsupported block type
│ 
│   on ../modules/ebs/main.tf line 4, in resource "aws_ebs_volume" "my_vol":
│    4:   tags {
│ 
│ Blocks of type "tags" are not expected here. Did you mean to define argument "tags"? If so, use the equals sign to assign it a value.
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform validate
╷
│ Error: Unsupported block type
│ 
│   on ../modules/ebs/main.tf line 4, in resource "aws_ebs_volume" "my_vol":
│    4:   tags {
│ 
│ Blocks of type "tags" are not expected here. Did you mean to define argument "tags"? If so, use the equals sign to assign it a value.
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ grep -r tag
grep: .terraform/providers/registry.terraform.io/hashicorp/aws/4.52.0/linux_amd64/terraform-provider-aws_v4.52.0_x5 : fichiers binaires correspondent
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ cd ..
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet$ grep -r tag
modules/ec2/main.tf:  tags              = { 
modules/ebs/main.tf:  tags_name         = "${var.maintainer}-ebs"
modules/sg/main.tf:  tags = {
prod/main.tf:  aws_common_tag = {
grep: app/.terraform/providers/registry.terraform.io/hashicorp/aws/4.52.0/linux_amd64/terraform-provider-aws_v4.52.0_x5 : fichiers binaires correspondent
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet$ grep -r tag
modules/ec2/main.tf:  tags              = { 
modules/ebs/main.tf:  tag_name          = "${var.maintainer}-ebs"
modules/sg/main.tf:  tags = {
prod/main.tf:  aws_common_tag = {
grep: app/.terraform/providers/registry.terraform.io/hashicorp/aws/4.52.0/linux_amd64/terraform-provider-aws_v4.52.0_x5 : fichiers binaires correspondent
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet$ cd -
/home/jmi/Documents/formationTerraformForBeginners/tp8_miniProjet/app
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform validate
╷
│ Error: Unsupported argument
│ 
│   on ../modules/ebs/main.tf line 4, in resource "aws_ebs_volume" "my_vol":
│    4:   tag_name          = "${var.maintainer}-ebs"
│ 
│ An argument named "tag_name" is not expected here.
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform validate
Success! The configuration is valid.

jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
╷
│ Error: configuring Terraform AWS Provider: no valid credential sources for Terraform AWS Provider found.
│ 
│ Please see https://registry.terraform.io/providers/hashicorp/aws
│ for more information about providing credentials.
│ 
│ Error: failed to refresh cached credentials, no EC2 IMDS role found, operation error ec2imds: GetMetadata, request send failed, Get "http://169.254.169.254/latest/meta-data/iam/security-credentials/": dial tcp 169.254.169.254:80: i/o timeout
│ 
│ 
│   with provider["registry.terraform.io/hashicorp/aws"],
│   on main.tf line 1, in provider "aws":
│    1: provider "aws" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ ll /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
-rw-r--r-- 1 jmi jmi 99 janv. 30 19:54 /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ more /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
aws_access_key_id=AKIAQMEP4HPHX54URHAV  
aws_secret_key=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  
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ vi /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
╷
│ Error: configuring Terraform AWS Provider: no valid credential sources for Terraform AWS Provider found.
│ 
│ Please see https://registry.terraform.io/providers/hashicorp/aws
│ for more information about providing credentials.
│ 
│ Error: failed to refresh cached credentials, no EC2 IMDS role found, operation error ec2imds: GetMetadata, request send failed, Get "http://169.254.169.254/latest/meta-data/iam/security-credentials/": dial tcp 169.254.169.254:80: i/o timeout
│ 
│ 
│   with provider["registry.terraform.io/hashicorp/aws"],
│   on main.tf line 1, in provider "aws":
│    1: provider "aws" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
╷
│ Error: configuring Terraform AWS Provider: no valid credential sources for Terraform AWS Provider found.
│ 
│ Please see https://registry.terraform.io/providers/hashicorp/aws
│ for more information about providing credentials.
│ 
│ Error: failed to refresh cached credentials, no EC2 IMDS role found, operation error ec2imds: GetMetadata, request send failed, Get "http://169.254.169.254/latest/meta-data/iam/security-credentials/": dial tcp 169.254.169.254:80: i/o timeout
│ 
│ 
│   with provider["registry.terraform.io/hashicorp/aws"],
│   on main.tf line 1, in provider "aws":
│    1: provider "aws" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply -h
Usage: terraform [global options] apply [options] [PLAN]

  Creates or updates infrastructure according to Terraform configuration
  files in the current directory.

  By default, Terraform will generate a new plan and present it for your
  approval before taking any action. You can optionally provide a plan
  file created by a previous call to "terraform plan", in which case
  Terraform will take the actions described in that plan without any
  confirmation prompt.

Options:

  -auto-approve          Skip interactive approval of plan before applying.

  -backup=path           Path to backup the existing state file before
                         modifying. Defaults to the "-state-out" path with
                         ".backup" extension. Set to "-" to disable backup.

  -compact-warnings      If Terraform produces any warnings that are not
                         accompanied by errors, show them in a more compact
                         form that includes only the summary messages.

  -destroy               Destroy Terraform-managed infrastructure.
                         The command "terraform destroy" is a convenience alias
                         for this option.

  -lock=false            Don't hold a state lock during the operation. This is
                         dangerous if others might concurrently run commands
                         against the same workspace.

  -lock-timeout=0s       Duration to retry a state lock.

  -input=true            Ask for input for variables if not directly set.

  -no-color              If specified, output won't contain any color.

  -parallelism=n         Limit the number of parallel resource operations.
                         Defaults to 10.

  -state=path            Path to read and save state (unless state-out
                         is specified). Defaults to "terraform.tfstate".

  -state-out=path        Path to write state to that is different than
                         "-state". This can be used to preserve the old
                         state.

  If you don't provide a saved plan file then this command will also accept
  all of the plan-customization options accepted by the terraform plan command.
  For more information on those options, run:
      terraform plan -help
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ cd /media/
jmi@portable-jmi:/media$ ll
total 12
drwxr-xr-x   3 root root 4096 déc.   7  2021 ./
drwxr-xr-x  20 root root 4096 déc.   7  2021 ../
drwxr-x---+  3 root root 4096 janv. 31 17:04 jmi/
jmi@portable-jmi:/media$ cd -
/home/jmi/Documents/formationTerraformForBeginners/tp8_miniProjet/app
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ ll /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
-rw-r--r-- 1 jmi jmi 108 janv. 31 17:32 /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ chmod 600 /media/jmi/JMCEMTEC/Formation/terraformForBeginners/fichiersSecurises/accessKeyEtSecretKey.md
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
╷
│ Error: configuring Terraform AWS Provider: no valid credential sources for Terraform AWS Provider found.
│ 
│ Please see https://registry.terraform.io/providers/hashicorp/aws
│ for more information about providing credentials.
│ 
│ Error: failed to refresh cached credentials, no EC2 IMDS role found, operation error ec2imds: GetMetadata, request send failed, Get "http://169.254.169.254/latest/meta-data/iam/security-credentials/": dial tcp 169.254.169.254:80: i/o timeout
│ 
│ 
│   with provider["registry.terraform.io/hashicorp/aws"],
│   on main.tf line 1, in provider "aws":
│    1: provider "aws" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
╷
│ Error: configuring Terraform AWS Provider: no valid credential sources for Terraform AWS Provider found.
│ 
│ Please see https://registry.terraform.io/providers/hashicorp/aws
│ for more information about providing credentials.
│ 
│ Error: failed to refresh cached credentials, no EC2 IMDS role found, operation error ec2imds: GetMetadata, request send failed, Get "http://169.254.169.254/latest/meta-data/iam/security-credentials/": dial tcp 169.254.169.254:80: i/o timeout
│ 
│ 
│   with provider["registry.terraform.io/hashicorp/aws"],
│   on main.tf line 1, in provider "aws":
│    1: provider "aws" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.ec2.data.aws_ami.ubuntu: Reading...
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = (known after apply)
    }

  # module.ebs.aws_ebs_volume.my_vol will be created
  + resource "aws_ebs_volume" "my_vol" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1"
      + encrypted         = (known after apply)
      + final_snapshot    = false
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 5
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "jmi-ebs"
        }
      + tags_all          = {
          + "Name" = "jmi-ebs"
        }
      + throughput        = (known after apply)
      + type              = (known after apply)
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be created
  + resource "aws_instance" "mini-projet-ec2" {
      + ami                                  = "ami-0c96a2f29d78f9d2f"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = "us-east-1"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "jmi-ec2"
        }
      + tags_all                             = {
          + "Name" = "jmi-ec2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # module.eip.aws_eip.my_eip will be created
  + resource "aws_eip" "my_eip" {
      + allocation_id        = (known after apply)
      + association_id       = (known after apply)
      + carrier_ip           = (known after apply)
      + customer_owned_ip    = (known after apply)
      + domain               = (known after apply)
      + id                   = (known after apply)
      + instance             = (known after apply)
      + network_border_group = (known after apply)
      + network_interface    = (known after apply)
      + private_dns          = (known after apply)
      + private_ip           = (known after apply)
      + public_dns           = (known after apply)
      + public_ip            = (known after apply)
      + public_ipv4_pool     = (known after apply)
      + tags_all             = (known after apply)
      + vpc                  = true
    }

  # module.sg.aws_security_group.my_sg will be created
  + resource "aws_security_group" "my_sg" {
      + arn                    = (known after apply)
      + description            = "Allow ssh, http and https inbound traffic"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "http from all"
              + from_port        = 80
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "https from all"
              + from_port        = 443
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 443
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "ssh from all"
              + from_port        = 22
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "jmi-sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name" = "jmi-sg"
        }
      + tags_all               = {
          + "Name" = "jmi-sg"
        }
      + vpc_id                 = (known after apply)
    }

Plan: 6 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: 

Apply cancelled.
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.ec2.data.aws_ami.ubuntu: Reading...
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = (known after apply)
    }

  # module.ebs.aws_ebs_volume.my_vol will be created
  + resource "aws_ebs_volume" "my_vol" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1"
      + encrypted         = (known after apply)
      + final_snapshot    = false
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 5
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "jmi-ebs"
        }
      + tags_all          = {
          + "Name" = "jmi-ebs"
        }
      + throughput        = (known after apply)
      + type              = (known after apply)
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be created
  + resource "aws_instance" "mini-projet-ec2" {
      + ami                                  = "ami-0c96a2f29d78f9d2f"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = "us-east-1"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "jmi-ec2"
        }
      + tags_all                             = {
          + "Name" = "jmi-ec2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # module.eip.aws_eip.my_eip will be created
  + resource "aws_eip" "my_eip" {
      + allocation_id        = (known after apply)
      + association_id       = (known after apply)
      + carrier_ip           = (known after apply)
      + customer_owned_ip    = (known after apply)
      + domain               = (known after apply)
      + id                   = (known after apply)
      + instance             = (known after apply)
      + network_border_group = (known after apply)
      + network_interface    = (known after apply)
      + private_dns          = (known after apply)
      + private_ip           = (known after apply)
      + public_dns           = (known after apply)
      + public_ip            = (known after apply)
      + public_ipv4_pool     = (known after apply)
      + tags_all             = (known after apply)
      + vpc                  = true
    }

  # module.sg.aws_security_group.my_sg will be created
  + resource "aws_security_group" "my_sg" {
      + arn                    = (known after apply)
      + description            = "Allow ssh, http and https inbound traffic"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "http from all"
              + from_port        = 80
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "https from all"
              + from_port        = 443
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 443
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = "ssh from all"
              + from_port        = 22
              + ipv6_cidr_blocks = [
                  + "::/0",
                ]
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "jmi-sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name" = "jmi-sg"
        }
      + tags_all               = {
          + "Name" = "jmi-sg"
        }
      + vpc_id                 = (known after apply)
    }

Plan: 6 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.eip.aws_eip.my_eip: Creating...
module.ebs.aws_ebs_volume.my_vol: Creating...
module.sg.aws_security_group.my_sg: Creating...
module.eip.aws_eip.my_eip: Creation complete after 1s [id=eipalloc-08df6745683b0664d]
module.sg.aws_security_group.my_sg: Creation complete after 4s [id=sg-07c8323c44f463aeb]
module.ec2.aws_instance.mini-projet-ec2: Creating...
╷
│ Error: creating EBS Volume: InvalidZone.NotFound: The zone 'us-east-1' does not exist.
│ 	status code: 400, request id: 6f11af65-9e56-4ea3-8548-02a95f20a59f
│ 
│   with module.ebs.aws_ebs_volume.my_vol,
│   on ../modules/ebs/main.tf line 1, in resource "aws_ebs_volume" "my_vol":
│    1: resource "aws_ebs_volume" "my_vol" {
│ 
╵
╷
│ Error: creating EC2 Instance: InvalidParameterValue: Invalid availability zone: [us-east-1]
│ 	status code: 400, request id: 18bf78cb-88d9-4b77-9bde-5251a0789e73
│ 
│   with module.ec2.aws_instance.mini-projet-ec2,
│   on ../modules/ec2/main.tf line 13, in resource "aws_instance" "mini-projet-ec2":
│   13: resource "aws_instance" "mini-projet-ec2" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.eip.aws_eip.my_eip: Refreshing state... [id=eipalloc-08df6745683b0664d]
module.ec2.data.aws_ami.ubuntu: Reading...
module.sg.aws_security_group.my_sg: Refreshing state... [id=sg-07c8323c44f463aeb]
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = "eipalloc-08df6745683b0664d"
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = (known after apply)
    }

  # module.ebs.aws_ebs_volume.my_vol will be created
  + resource "aws_ebs_volume" "my_vol" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1"
      + encrypted         = (known after apply)
      + final_snapshot    = false
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 5
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "jmi-ebs"
        }
      + tags_all          = {
          + "Name" = "jmi-ebs"
        }
      + throughput        = (known after apply)
      + type              = (known after apply)
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be created
  + resource "aws_instance" "mini-projet-ec2" {
      + ami                                  = "ami-0c96a2f29d78f9d2f"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = "us-east-1"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "jmi-ec2"
        }
      + tags_all                             = {
          + "Name" = "jmi-ec2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 4 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: ^[[D

Apply cancelled.
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.ec2.data.aws_ami.ubuntu: Reading...
module.eip.aws_eip.my_eip: Refreshing state... [id=eipalloc-08df6745683b0664d]
module.sg.aws_security_group.my_sg: Refreshing state... [id=sg-07c8323c44f463aeb]
module.ec2.data.aws_ami.ubuntu: Read complete after 0s [id=ami-0c96a2f29d78f9d2f]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = "eipalloc-08df6745683b0664d"
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = (known after apply)
    }

  # module.ebs.aws_ebs_volume.my_vol will be created
  + resource "aws_ebs_volume" "my_vol" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1"
      + encrypted         = (known after apply)
      + final_snapshot    = false
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 5
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "jmi-ebs"
        }
      + tags_all          = {
          + "Name" = "jmi-ebs"
        }
      + throughput        = (known after apply)
      + type              = (known after apply)
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be created
  + resource "aws_instance" "mini-projet-ec2" {
      + ami                                  = "ami-0c96a2f29d78f9d2f"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = "us-east-1"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "jmi-ec2"
        }
      + tags_all                             = {
          + "Name" = "jmi-ec2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 4 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.ebs.aws_ebs_volume.my_vol: Creating...
module.ec2.aws_instance.mini-projet-ec2: Creating...
╷
│ Error: creating EBS Volume: InvalidZone.NotFound: The zone 'us-east-1' does not exist.
│ 	status code: 400, request id: 68174692-e2a4-4cd6-b10f-93ed8207ff13
│ 
│   with module.ebs.aws_ebs_volume.my_vol,
│   on ../modules/ebs/main.tf line 1, in resource "aws_ebs_volume" "my_vol":
│    1: resource "aws_ebs_volume" "my_vol" {
│ 
╵
╷
│ Error: creating EC2 Instance: InvalidParameterValue: Invalid availability zone: [us-east-1]
│ 	status code: 400, request id: dee33653-4a2a-467f-86f2-8c5f683e329d
│ 
│   with module.ec2.aws_instance.mini-projet-ec2,
│   on ../modules/ec2/main.tf line 13, in resource "aws_instance" "mini-projet-ec2":
│   13: resource "aws_instance" "mini-projet-ec2" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.ec2.data.aws_ami.ubuntu: Reading...
module.eip.aws_eip.my_eip: Refreshing state... [id=eipalloc-08df6745683b0664d]
module.sg.aws_security_group.my_sg: Refreshing state... [id=sg-07c8323c44f463aeb]
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = "eipalloc-08df6745683b0664d"
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = (known after apply)
    }

  # module.ebs.aws_ebs_volume.my_vol will be created
  + resource "aws_ebs_volume" "my_vol" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1b"
      + encrypted         = (known after apply)
      + final_snapshot    = false
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 5
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "jmi-ebs"
        }
      + tags_all          = {
          + "Name" = "jmi-ebs"
        }
      + throughput        = (known after apply)
      + type              = (known after apply)
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be created
  + resource "aws_instance" "mini-projet-ec2" {
      + ami                                  = "ami-0c96a2f29d78f9d2f"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = "us-east-1"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "jmi-ec2"
        }
      + tags_all                             = {
          + "Name" = "jmi-ec2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 4 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.ebs.aws_ebs_volume.my_vol: Creating...
module.ec2.aws_instance.mini-projet-ec2: Creating...
module.ebs.aws_ebs_volume.my_vol: Still creating... [10s elapsed]
module.ebs.aws_ebs_volume.my_vol: Creation complete after 12s [id=vol-0ee8c88bd1dc3e3e0]
╷
│ Error: creating EC2 Instance: InvalidParameterValue: Invalid availability zone: [us-east-1]
│ 	status code: 400, request id: 34b64032-2fe6-48d9-807a-b43966bfb1ec
│ 
│   with module.ec2.aws_instance.mini-projet-ec2,
│   on ../modules/ec2/main.tf line 13, in resource "aws_instance" "mini-projet-ec2":
│   13: resource "aws_instance" "mini-projet-ec2" {
│ 
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.ebs.aws_ebs_volume.my_vol: Refreshing state... [id=vol-0ee8c88bd1dc3e3e0]
module.ec2.data.aws_ami.ubuntu: Reading...
module.eip.aws_eip.my_eip: Refreshing state... [id=eipalloc-08df6745683b0664d]
module.sg.aws_security_group.my_sg: Refreshing state... [id=sg-07c8323c44f463aeb]
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = "eipalloc-08df6745683b0664d"
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = "vol-0ee8c88bd1dc3e3e0"
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be created
  + resource "aws_instance" "mini-projet-ec2" {
      + ami                                  = "ami-0c96a2f29d78f9d2f"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = "us-east-1b"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "devops-jmi"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "jmi-sg",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "jmi-ec2"
        }
      + tags_all                             = {
          + "Name" = "jmi-ec2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.ec2.aws_instance.mini-projet-ec2: Creating...
module.ec2.aws_instance.mini-projet-ec2: Still creating... [10s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still creating... [20s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still creating... [30s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Provisioning with 'local-exec'...
module.ec2.aws_instance.mini-projet-ec2 (local-exec): Executing: ["/bin/sh" "-c" "echo PUBLIC IP: 35.172.32.105 > IP_ec2.txt"]
╷
│ Error: Invalid function argument
│ 
│   on ../modules/ec2/main.tf line 42, in resource "aws_instance" "mini-projet-ec2":
│   42:      private_key = file("/home/jmi/Documents/formationTerraform/${var.ssh_key}.pem")
│     ├────────────────
│     │ while calling file(path)
│     │ var.ssh_key is "devops-jmi"
│ 
│ Invalid value for "path" parameter: no file exists at "/home/jmi/Documents/formationTerraform/devops-jmi.pem"; this function works only with files that are
│ distributed as part of the configuration source code, so if this file will be created by a resource in this configuration you must instead obtain this result from
│ an attribute of that resource.
╵
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ ll /home/jmi/Documents/formationTerraform/devops-jmi.pem
ls: impossible d'accéder à '/home/jmi/Documents/formationTerraform/devops-jmi.pem': Aucun fichier ou dossier de ce type
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform apply
module.ec2.data.aws_ami.ubuntu: Reading...
module.ebs.aws_ebs_volume.my_vol: Refreshing state... [id=vol-0ee8c88bd1dc3e3e0]
module.eip.aws_eip.my_eip: Refreshing state... [id=eipalloc-08df6745683b0664d]
module.sg.aws_security_group.my_sg: Refreshing state... [id=sg-07c8323c44f463aeb]
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]
module.ec2.aws_instance.mini-projet-ec2: Refreshing state... [id=i-0f3745d659e9cc602]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be created
  + resource "aws_eip_association" "eip_assoc" {
      + allocation_id        = "eipalloc-08df6745683b0664d"
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + network_interface_id = (known after apply)
      + private_ip_address   = (known after apply)
      + public_ip            = (known after apply)
    }

  # aws_volume_attachment.ebs_att will be created
  + resource "aws_volume_attachment" "ebs_att" {
      + device_name = "/dev/sdh"
      + id          = (known after apply)
      + instance_id = (known after apply)
      + volume_id   = "vol-0ee8c88bd1dc3e3e0"
    }

  # module.ec2.aws_instance.mini-projet-ec2 is tainted, so must be replaced
-/+ resource "aws_instance" "mini-projet-ec2" {
      ~ arn                                  = "arn:aws:ec2:us-east-1:026071546831:instance/i-0f3745d659e9cc602" -> (known after apply)
      ~ associate_public_ip_address          = true -> (known after apply)
      ~ cpu_core_count                       = 1 -> (known after apply)
      ~ cpu_threads_per_core                 = 1 -> (known after apply)
      ~ disable_api_stop                     = false -> (known after apply)
      ~ disable_api_termination              = false -> (known after apply)
      ~ ebs_optimized                        = false -> (known after apply)
      - hibernation                          = false -> null
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      ~ id                                   = "i-0f3745d659e9cc602" -> (known after apply)
      ~ instance_initiated_shutdown_behavior = "stop" -> (known after apply)
      ~ instance_state                       = "running" -> (known after apply)
      ~ ipv6_address_count                   = 0 -> (known after apply)
      ~ ipv6_addresses                       = [] -> (known after apply)
      ~ monitoring                           = false -> (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      ~ placement_partition_number           = 0 -> (known after apply)
      ~ primary_network_interface_id         = "eni-085a2ec77759d9a4a" -> (known after apply)
      ~ private_dns                          = "ip-172-31-5-26.ec2.internal" -> (known after apply)
      ~ private_ip                           = "172.31.5.26" -> (known after apply)
      ~ public_dns                           = "ec2-44-201-40-120.compute-1.amazonaws.com" -> (known after apply)
      ~ public_ip                            = "44.201.40.120" -> (known after apply)
      ~ secondary_private_ips                = [] -> (known after apply)
      ~ subnet_id                            = "subnet-0a3d7612131c396e7" -> (known after apply)
        tags                                 = {
            "Name" = "jmi-ec2"
        }
      ~ tenancy                              = "default" -> (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      ~ vpc_security_group_ids               = [
          - "sg-07c8323c44f463aeb",
        ] -> (known after apply)
        # (9 unchanged attributes hidden)

      ~ capacity_reservation_specification {
          ~ capacity_reservation_preference = "open" -> (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      - credit_specification {
          - cpu_credits = "standard" -> null
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      ~ enclave_options {
          ~ enabled = false -> (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      ~ maintenance_options {
          ~ auto_recovery = "default" -> (known after apply)
        }

      ~ metadata_options {
          ~ http_endpoint               = "enabled" -> (known after apply)
          ~ http_put_response_hop_limit = 1 -> (known after apply)
          ~ http_tokens                 = "optional" -> (known after apply)
          ~ instance_metadata_tags      = "disabled" -> (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      ~ private_dns_name_options {
          ~ enable_resource_name_dns_a_record    = false -> (known after apply)
          ~ enable_resource_name_dns_aaaa_record = false -> (known after apply)
          ~ hostname_type                        = "ip-name" -> (known after apply)
        }

      ~ root_block_device {
          ~ device_name           = "/dev/sda1" -> (known after apply)
          ~ encrypted             = false -> (known after apply)
          ~ iops                  = 100 -> (known after apply)
          + kms_key_id            = (known after apply)
          - tags                  = {} -> null
          ~ throughput            = 0 -> (known after apply)
          ~ volume_id             = "vol-04fdb4f7a368e190d" -> (known after apply)
          ~ volume_size           = 8 -> (known after apply)
          ~ volume_type           = "gp2" -> (known after apply)
            # (1 unchanged attribute hidden)
        }
    }

Plan: 3 to add, 0 to change, 1 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.ec2.aws_instance.mini-projet-ec2: Destroying... [id=i-0f3745d659e9cc602]
module.ec2.aws_instance.mini-projet-ec2: Still destroying... [id=i-0f3745d659e9cc602, 10s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still destroying... [id=i-0f3745d659e9cc602, 20s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still destroying... [id=i-0f3745d659e9cc602, 30s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Destruction complete after 31s
module.ec2.aws_instance.mini-projet-ec2: Creating...
module.ec2.aws_instance.mini-projet-ec2: Still creating... [10s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still creating... [20s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still creating... [30s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Provisioning with 'local-exec'...
module.ec2.aws_instance.mini-projet-ec2 (local-exec): Executing: ["/bin/sh" "-c" "echo PUBLIC IP: 35.172.32.105 > IP_ec2.txt"]
module.ec2.aws_instance.mini-projet-ec2: Provisioning with 'remote-exec'...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Connecting to remote host via SSH...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Host: 3.235.13.26
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   User: ubuntu
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Password: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Private key: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Certificate: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   SSH Agent: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Checking Host Key: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Target Platform: unix
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Connecting to remote host via SSH...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Host: 3.235.13.26
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   User: ubuntu
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Password: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Private key: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Certificate: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   SSH Agent: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Checking Host Key: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Target Platform: unix
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Connecting to remote host via SSH...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Host: 3.235.13.26
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   User: ubuntu
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Password: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Private key: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Certificate: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   SSH Agent: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Checking Host Key: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Target Platform: unix
module.ec2.aws_instance.mini-projet-ec2: Still creating... [40s elapsed]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Connecting to remote host via SSH...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Host: 3.235.13.26
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   User: ubuntu
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Password: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Private key: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Certificate: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   SSH Agent: true
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Checking Host Key: false
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   Target Platform: unix
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Connected!
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:1 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [Waiting for headers] [1 InRelease 1
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Hit:2 http://archive.ubuntu.com/ubuntu bionic InRelease
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [Waiting for headers] [1 InRelease 1
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [2 InRelease gpgv 242 kB] [Waiting f
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:3 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [2 InRelease gpgv 242 kB] [3 InRelea
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [2 InRelease gpgv 242 kB] [3 InRelea
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [2 InRelease gpgv 242 kB] [Waiting f
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [Waiting for headers]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:4 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [83.3 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [4 InRelease 28.7 kB/83.3 kB 34%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [1 InRelease gpgv 88.7 kB] [4 InRele
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [1 InRelease gpgv 88.7 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:5 http://archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [8570 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [1 InRelease gpgv 88.7 kB] [5 Packag
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [5 Packages 336 kB/8570 kB 4%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [3 InRelease gpgv 88.7 kB] [5 Packag
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:6 http://security.ubuntu.com/ubuntu bionic-security/main amd64 Packages [2546 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [3 InRelease gpgv 88.7 kB] [5 Packag
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [5 Packages 6107 kB/8570 kB 71%] [6 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [4 InRelease gpgv 83.3 kB] [5 Packag
module.ec2.aws_instance.mini-projet-ec2: Still creating... [50s elapsed]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:7 http://archive.ubuntu.com/ubuntu bionic/universe Translation-en [4941 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [4 InRelease gpgv 83.3 kB] [7 Transl
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 55% [7 Translation-en 2380 kB/4941 kB 4
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 55% [5 Packages store 0 B] [7 Translati
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:8 http://security.ubuntu.com/ubuntu bionic-security/main Translation-en [441 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 55% [5 Packages store 0 B] [7 Translati
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 61% [5 Packages store 0 B] [7 Translati
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:9 http://security.ubuntu.com/ubuntu bionic-security/restricted amd64 Packages [1084 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 63% [5 Packages store 0 B] [7 Translati
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 66% [5 Packages store 0 B] [9 Packages 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:10 http://archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [151 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 66% [5 Packages store 0 B] [10 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 67% [5 Packages store 0 B] [9 Packages 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:11 http://archive.ubuntu.com/ubuntu bionic/multiverse Translation-en [108 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 68% [5 Packages store 0 B] [11 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 68% [5 Packages store 0 B] [9 Packages 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:12 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [2878 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 68% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 69% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:13 http://security.ubuntu.com/ubuntu bionic-security/restricted Translation-en [150 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 71% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:14 http://security.ubuntu.com/ubuntu bionic-security/universe amd64 Packages [1257 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 72% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 79% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:15 http://security.ubuntu.com/ubuntu bionic-security/universe Translation-en [291 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 80% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 81% [5 Packages store 0 B] [12 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/main Translation-en [528 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 82% [5 Packages store 0 B] [16 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:17 http://security.ubuntu.com/ubuntu bionic-security/multiverse amd64 Packages [19.1 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 82% [5 Packages store 0 B] [16 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:18 http://security.ubuntu.com/ubuntu bionic-security/multiverse Translation-en [3908 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 83% [5 Packages store 0 B] [16 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:19 http://archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [1115 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 83% [5 Packages store 0 B] [19 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:20 http://archive.ubuntu.com/ubuntu bionic-updates/restricted Translation-en [155 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 87% [5 Packages store 0 B] [20 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:21 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 Packages [1870 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 87% [5 Packages store 0 B] [21 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:22 http://archive.ubuntu.com/ubuntu bionic-updates/universe Translation-en [405 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 93% [5 Packages store 0 B] [22 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:23 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse amd64 Packages [25.6 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [23 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:24 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse Translation-en [6088 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:25 http://archive.ubuntu.com/ubuntu bionic-backports/main amd64 Packages [53.3 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [25 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:26 http://archive.ubuntu.com/ubuntu bionic-backports/main Translation-en [14.6 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [26 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [Waiting for
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:27 http://archive.ubuntu.com/ubuntu bionic-backports/universe amd64 Packages [18.1 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [27 Packages
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [Waiting for
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:28 http://archive.ubuntu.com/ubuntu bionic-backports/universe Translation-en [8668 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B] [28 Translat
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 94% [5 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [6 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [8 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [7 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [7 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [10 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 95% [11 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [9 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [13 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [14 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 96% [15 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [12 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [17 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [18 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 97% [16 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [19 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [20 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [Working]              4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [21 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [Working]              4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 98% [22 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [Working]              4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [23 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [Working]              4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [24 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [Working]              4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [25 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [Working]              4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [26 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 100% [Working]             4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 100% [27 Packages store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 100% [Working]             4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 100% [28 Translation-en store 0 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 100% [Working]             4446 kB/s 0s
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Fetched 26.9 MB in 6s (4146 kB/s)
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 3%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 3%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 4%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 4%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 5%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 5%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 5%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 5%
module.ec2.aws_instance.mini-projet-ec2: Still creating... [1m0s elapsed]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 21%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 31%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 31%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 46%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 46%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 46%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 46%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 46%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 46%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 48%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 55%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 55%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 61%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 61%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 65%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 65%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 67%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 67%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 72%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 72%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 83%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 83%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 88%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 88%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 92%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 92%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 94%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 94%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 97%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 97%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 99%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 99%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 99%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 99%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 99%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 99%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... Done
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 50%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 50%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading state information... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading state information... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading state information... Done
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 27 packages can be upgraded. Run 'apt list --upgradable' to see them.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... 100%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading package lists... Done
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 50%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree... 50%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Building dependency tree
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading state information... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading state information... 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Reading state information... Done
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): The following additional packages will be installed:
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   fontconfig-config fonts-dejavu-core
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libfontconfig1 libgd3 libjbig0
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libjpeg-turbo8 libjpeg8
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-http-geoip
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-http-image-filter
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-http-xslt-filter
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-mail
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-stream libtiff5
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libwebp6 libxpm4 nginx-common
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   nginx-core
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Suggested packages:
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libgd-tools fcgiwrap nginx-doc
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   ssl-cert
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): The following NEW packages will be installed:
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   fontconfig-config fonts-dejavu-core
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libfontconfig1 libgd3 libjbig0
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libjpeg-turbo8 libjpeg8
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-http-geoip
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-http-image-filter
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-http-xslt-filter
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-mail
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libnginx-mod-stream libtiff5
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   libwebp6 libxpm4 nginx nginx-common
module.ec2.aws_instance.mini-projet-ec2 (remote-exec):   nginx-core
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0 upgraded, 18 newly installed, 0 to remove and 27 not upgraded.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Need to get 2464 kB of archives.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): After this operation, 8218 kB of additional disk space will be used.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:1 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libjpeg-turbo8 amd64 1.5.2-0ubuntu5.18.04.6 [111 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 0% [1 libjpeg-turbo8 0 B/111 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 5% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:2 http://archive.ubuntu.com/ubuntu bionic/main amd64 fonts-dejavu-core all 2.37-1 [1041 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 5% [2 fonts-dejavu-core 0 B/1041 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 40% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:3 http://archive.ubuntu.com/ubuntu bionic/main amd64 fontconfig-config all 2.12.6-0ubuntu2 [55.8 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 40% [3 fontconfig-config 0 B/55.8 kB 0%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 43% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:4 http://archive.ubuntu.com/ubuntu bionic/main amd64 libfontconfig1 amd64 2.12.6-0ubuntu2 [137 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 43% [4 libfontconfig1 0 B/137 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 48% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:5 http://archive.ubuntu.com/ubuntu bionic/main amd64 libjpeg8 amd64 8c-2ubuntu8 [2194 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 48% [5 libjpeg8 0 B/2194 B 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 49% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:6 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libjbig0 amd64 2.1-3.1ubuntu0.18.04.1 [27.0 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 49% [6 libjbig0 0 B/27.0 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 51% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:7 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libtiff5 amd64 4.0.9-5ubuntu0.9 [154 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 51% [7 libtiff5 0 B/154 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 57% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:8 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libwebp6 amd64 0.6.1-2ubuntu0.18.04.1 [186 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 57% [8 libwebp6 0 B/186 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 65% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:9 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libxpm4 amd64 1:3.5.12-1ubuntu0.18.04.2 [34.8 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 65% [9 libxpm4 0 B/34.8 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 67% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:10 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libgd3 amd64 2.2.5-4ubuntu0.5 [119 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 67% [10 libgd3 0 B/119 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 72% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:11 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 nginx-common all 1.14.0-0ubuntu1.11 [37.2 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 72% [11 nginx-common 0 B/37.2 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 74% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:12 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libnginx-mod-http-geoip amd64 1.14.0-0ubuntu1.11 [11.0 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 74% [12 libnginx-mod-http-geoip 0 B/11.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 76% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:13 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libnginx-mod-http-image-filter amd64 1.14.0-0ubuntu1.11 [14.3 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 76% [13 libnginx-mod-http-image-filter 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 77% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:14 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libnginx-mod-http-xslt-filter amd64 1.14.0-0ubuntu1.11 [12.8 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 77% [14 libnginx-mod-http-xslt-filter 0
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 79% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libnginx-mod-mail amd64 1.14.0-0ubuntu1.11 [41.8 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 79% [15 libnginx-mod-mail 0 B/41.8 kB 0
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 81% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libnginx-mod-stream amd64 1.14.0-0ubuntu1.11 [63.5 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 81% [16 libnginx-mod-stream 0 B/63.5 kB
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 84% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:17 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 nginx-core amd64 1.14.0-0ubuntu1.11 [413 kB]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 84% [17 nginx-core 0 B/413 kB 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Get:18 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 nginx all 1.14.0-0ubuntu1.11 [3596 B]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 99% [18 nginx 0 B/3596 B 0%]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): 100% [Working]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Fetched 2464 kB in 0s (12.6 MB/s)
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preconfiguring packages ...
                                                       Selecting previously unselected package libjpeg-turbo8:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 5%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 10%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 15%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 20%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 25%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 30%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 35%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 40%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 45%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 50%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 55%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 60%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 65%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 70%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 75%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 80%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 85%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 90%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 95%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 100%
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): (Reading database ... 58138 files and directories currently installed.)
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../00-libjpeg-turbo8_1.5.2-0ubuntu5.18.04.6_amd64.deb ...
Progress: [  1%] [..................] 2 (remote-exec): Unpacking libjpeg-turbo8:amd64 (1.5.2-0ubuntu5.18.04.6) ...
Progress: [  3%] [..................] 2 (remote-exec): Selecting previously unselected package fonts-dejavu-core.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../01-fonts-dejavu-core_2.37-1_all.deb ...
Progress: [  4%] [..................] 2 (remote-exec): Unpacking fonts-dejavu-core (2.37-1) ...
Progress: [  7%] [#.................] 2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Selecting previously unselected package fontconfig-config.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../02-fontconfig-config_2.12.6-0ubuntu2_all.deb ...
Progress: [  8%] [#.................] 2 (remote-exec): Unpacking fontconfig-config (2.12.6-0ubuntu2) ...
Progress: [ 10%] [#.................] 2 (remote-exec): Selecting previously unselected package libfontconfig1:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../03-libfontconfig1_2.12.6-0ubuntu2_amd64.deb ...
Progress: [ 11%] [#.................] 2 (remote-exec): Unpacking libfontconfig1:amd64 (2.12.6-0ubuntu2) ...
Progress: [ 13%] [##................] 2 (remote-exec): Selecting previously unselected package libjpeg8:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../04-libjpeg8_8c-2ubuntu8_amd64.deb ...
Progress: [ 14%] [##................] 2 (remote-exec): Unpacking libjpeg8:amd64 (8c-2ubuntu8) ...
Progress: [ 16%] [##................] 2 (remote-exec): Selecting previously unselected package libjbig0:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../05-libjbig0_2.1-3.1ubuntu0.18.04.1_amd64.deb ...
Progress: [ 18%] [###...............] 2 (remote-exec): Unpacking libjbig0:amd64 (2.1-3.1ubuntu0.18.04.1) ...
Progress: [ 20%] [###...............] 2 (remote-exec): Selecting previously unselected package libtiff5:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../06-libtiff5_4.0.9-5ubuntu0.9_amd64.deb ...
Progress: [ 21%] [###...............] 2 (remote-exec): Unpacking libtiff5:amd64 (4.0.9-5ubuntu0.9) ...
Progress: [ 23%] [####..............] 2 (remote-exec): Selecting previously unselected package libwebp6:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../07-libwebp6_0.6.1-2ubuntu0.18.04.1_amd64.deb ...
Progress: [ 24%] [####..............] 2 (remote-exec): Unpacking libwebp6:amd64 (0.6.1-2ubuntu0.18.04.1) ...
Progress: [ 26%] [####..............] 2 (remote-exec): Selecting previously unselected package libxpm4:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../08-libxpm4_1%3a3.5.12-1ubuntu0.18.04.2_amd64.deb ...
Progress: [ 27%] [####..............] 2 (remote-exec): Unpacking libxpm4:amd64 (1:3.5.12-1ubuntu0.18.04.2) ...
Progress: [ 30%] [#####.............] 2 (remote-exec): Selecting previously unselected package libgd3:amd64.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../09-libgd3_2.2.5-4ubuntu0.5_amd64.deb ...
Progress: [ 31%] [#####.............] 2 (remote-exec): Unpacking libgd3:amd64 (2.2.5-4ubuntu0.5) ...
Progress: [ 33%] [#####.............] 2 (remote-exec): Selecting previously unselected package nginx-common.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../10-nginx-common_1.14.0-0ubuntu1.11_all.deb ...
Progress: [ 34%] [######............] 2 (remote-exec): Unpacking nginx-common (1.14.0-0ubuntu1.11) ...
Progress: [ 36%] [######............] 2 (remote-exec): Selecting previously unselected package libnginx-mod-http-geoip.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../11-libnginx-mod-http-geoip_1.14.0-0ubuntu1.11_amd64.deb ...
Progress: [ 37%] [######............] 2 (remote-exec): Unpacking libnginx-mod-http-geoip (1.14.0-0ubuntu1.11) ...
Progress: [ 40%] [#######...........] 2 (remote-exec): Selecting previously unselected package libnginx-mod-http-image-filter.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../12-libnginx-mod-http-image-filter_1.14.0-0ubuntu1.11_amd64.deb ...
Progress: [ 41%] [#######...........] 2 (remote-exec): Unpacking libnginx-mod-http-image-filter (1.14.0-0ubuntu1.11) ...
Progress: [ 43%] [#######...........] 2 (remote-exec): Selecting previously unselected package libnginx-mod-http-xslt-filter.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../13-libnginx-mod-http-xslt-filter_1.14.0-0ubuntu1.11_amd64.deb ...
Progress: [ 44%] [#######...........] 2 (remote-exec): Unpacking libnginx-mod-http-xslt-filter (1.14.0-0ubuntu1.11) ...
Progress: [ 46%] [########..........] 2 (remote-exec): Selecting previously unselected package libnginx-mod-mail.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../14-libnginx-mod-mail_1.14.0-0ubuntu1.11_amd64.deb ...
Progress: [ 47%] [########..........] 2 (remote-exec): Unpacking libnginx-mod-mail (1.14.0-0ubuntu1.11) ...
Progress: [ 49%] [########..........] 2 (remote-exec): Selecting previously unselected package libnginx-mod-stream.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../15-libnginx-mod-stream_1.14.0-0ubuntu1.11_amd64.deb ...
Progress: [ 51%] [#########.........] 2 (remote-exec): Unpacking libnginx-mod-stream (1.14.0-0ubuntu1.11) ...
Progress: [ 53%] [#########.........] 2 (remote-exec): Selecting previously unselected package nginx-core.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../16-nginx-core_1.14.0-0ubuntu1.11_amd64.deb ...
Progress: [ 54%] [#########.........] 2 (remote-exec): Unpacking nginx-core (1.14.0-0ubuntu1.11) ...
Progress: [ 56%] [##########........] 2 (remote-exec): Selecting previously unselected package nginx.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Preparing to unpack .../17-nginx_1.14.0-0ubuntu1.11_all.deb ...
Progress: [ 57%] [##########........] 2 (remote-exec): Unpacking nginx (1.14.0-0ubuntu1.11) ...
Progress: [ 59%] [##########........] 2 (remote-exec): Setting up libjbig0:amd64 (2.1-3.1ubuntu0.18.04.1) ...
Progress: [ 62%] [###########.......] 2 (remote-exec): Setting up fonts-dejavu-core (2.37-1) ...
Progress: [ 64%] [###########.......] 2 (remote-exec): Setting up nginx-common (1.14.0-0ubuntu1.11) ...
Progress: [ 65%] [###########.......] 2 (remote-exec): 
module.ec2.aws_instance.mini-projet-ec2: Still creating... [1m10s elapsed]
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /lib/systemd/system/nginx.service.
Progress: [ 66%] [###########.......] 2 (remote-exec): Setting up libjpeg-turbo8:amd64 (1.5.2-0ubuntu5.18.04.6) ...
Progress: [ 68%] [############......] 2 (remote-exec): Setting up libnginx-mod-mail (1.14.0-0ubuntu1.11) ...
Progress: [ 70%] [############......] 2 (remote-exec): Setting up libxpm4:amd64 (1:3.5.12-1ubuntu0.18.04.2) ...
Progress: [ 73%] [#############.....] 2 (remote-exec): Setting up libnginx-mod-http-xslt-filter (1.14.0-0ubuntu1.11) ...
Progress: [ 75%] [#############.....] 2 (remote-exec): Setting up libnginx-mod-http-geoip (1.14.0-0ubuntu1.11) ...
Progress: [ 77%] [#############.....] 2 (remote-exec): Setting up libwebp6:amd64 (0.6.1-2ubuntu0.18.04.1) ...
Progress: [ 79%] [##############....] 2 (remote-exec): Setting up libjpeg8:amd64 (8c-2ubuntu8) ...
Progress: [ 81%] [##############....] 2 (remote-exec): Setting up fontconfig-config (2.12.6-0ubuntu2) ...
Progress: [ 84%] [###############...] 2 (remote-exec): Setting up libnginx-mod-stream (1.14.0-0ubuntu1.11) ...
Progress: [ 86%] [###############...] 2 (remote-exec): Setting up libtiff5:amd64 (4.0.9-5ubuntu0.9) ...
Progress: [ 88%] [###############...] 2 (remote-exec): Setting up libfontconfig1:amd64 (2.12.6-0ubuntu2) ...
Progress: [ 90%] [################..] 2 (remote-exec): Setting up libgd3:amd64 (2.2.5-4ubuntu0.5) ...
Progress: [ 92%] [################..] 2 (remote-exec): Setting up libnginx-mod-http-image-filter (1.14.0-0ubuntu1.11) ...
Progress: [ 95%] [#################.] 2 (remote-exec): Setting up nginx-core (1.14.0-0ubuntu1.11) ...
Progress: [ 96%] [#################.] 2 (remote-exec): 
Progress: [ 97%] [#################.] 2 (remote-exec): Setting up nginx (1.14.0-0ubuntu1.11) ...
Progress: [ 99%] [#################.] 2 (remote-exec): Processing triggers for systemd (237-3ubuntu10.56) ...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Processing triggers for ufw (0.36-0ubuntu0.18.04.2) ...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Processing triggers for ureadahead (0.100.0-21) ...
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Processing triggers for libc-bin (2.27-3ubuntu1.6) ...

module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Synchronizing state of nginx.service with SysV service script with /lib/systemd/systemd-sysv-install.
module.ec2.aws_instance.mini-projet-ec2 (remote-exec): Executing: /lib/systemd/systemd-sysv-install enable nginx
module.ec2.aws_instance.mini-projet-ec2: Creation complete after 1m19s [id=i-00ccfaa36666a3c09]
aws_volume_attachment.ebs_att: Creating...
aws_eip_association.eip_assoc: Creating...
aws_eip_association.eip_assoc: Creation complete after 2s [id=eipassoc-0f1a3246b970a815c]
aws_volume_attachment.ebs_att: Still creating... [10s elapsed]
aws_volume_attachment.ebs_att: Still creating... [20s elapsed]
aws_volume_attachment.ebs_att: Creation complete after 22s [id=vai-4211767009]

Apply complete! Resources: 3 added, 0 changed, 1 destroyed.
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ ssh ubuntu@35.172.32.105
The authenticity of host '35.172.32.105 (35.172.32.105)' can't be established.
ED25519 key fingerprint is SHA256:dTsgax72mPapL1S0N0NSWlhYF9cIGvSOEITixdJuAvU.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '35.172.32.105' (ED25519) to the list of known hosts.
ubuntu@35.172.32.105: Permission denied (publickey).
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ terraform destroy
module.eip.aws_eip.my_eip: Refreshing state... [id=eipalloc-08df6745683b0664d]
module.ebs.aws_ebs_volume.my_vol: Refreshing state... [id=vol-0ee8c88bd1dc3e3e0]
module.ec2.data.aws_ami.ubuntu: Reading...
module.sg.aws_security_group.my_sg: Refreshing state... [id=sg-07c8323c44f463aeb]
module.ec2.data.aws_ami.ubuntu: Read complete after 1s [id=ami-0c96a2f29d78f9d2f]
module.ec2.aws_instance.mini-projet-ec2: Refreshing state... [id=i-00ccfaa36666a3c09]
aws_volume_attachment.ebs_att: Refreshing state... [id=vai-4211767009]
aws_eip_association.eip_assoc: Refreshing state... [id=eipassoc-0f1a3246b970a815c]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_eip_association.eip_assoc will be destroyed
  - resource "aws_eip_association" "eip_assoc" {
      - allocation_id        = "eipalloc-08df6745683b0664d" -> null
      - id                   = "eipassoc-0f1a3246b970a815c" -> null
      - instance_id          = "i-00ccfaa36666a3c09" -> null
      - network_interface_id = "eni-021408d678b59a3ec" -> null
      - private_ip_address   = "172.31.3.44" -> null
      - public_ip            = "35.172.32.105" -> null
    }

  # aws_volume_attachment.ebs_att will be destroyed
  - resource "aws_volume_attachment" "ebs_att" {
      - device_name = "/dev/sdh" -> null
      - id          = "vai-4211767009" -> null
      - instance_id = "i-00ccfaa36666a3c09" -> null
      - volume_id   = "vol-0ee8c88bd1dc3e3e0" -> null
    }

  # module.ebs.aws_ebs_volume.my_vol will be destroyed
  - resource "aws_ebs_volume" "my_vol" {
      - arn                  = "arn:aws:ec2:us-east-1:026071546831:volume/vol-0ee8c88bd1dc3e3e0" -> null
      - availability_zone    = "us-east-1b" -> null
      - encrypted            = false -> null
      - final_snapshot       = false -> null
      - id                   = "vol-0ee8c88bd1dc3e3e0" -> null
      - iops                 = 100 -> null
      - multi_attach_enabled = false -> null
      - size                 = 5 -> null
      - tags                 = {
          - "Name" = "jmi-ebs"
        } -> null
      - tags_all             = {
          - "Name" = "jmi-ebs"
        } -> null
      - throughput           = 0 -> null
      - type                 = "gp2" -> null
    }

  # module.ec2.aws_instance.mini-projet-ec2 will be destroyed
  - resource "aws_instance" "mini-projet-ec2" {
      - ami                                  = "ami-0c96a2f29d78f9d2f" -> null
      - arn                                  = "arn:aws:ec2:us-east-1:026071546831:instance/i-00ccfaa36666a3c09" -> null
      - associate_public_ip_address          = true -> null
      - availability_zone                    = "us-east-1b" -> null
      - cpu_core_count                       = 1 -> null
      - cpu_threads_per_core                 = 1 -> null
      - disable_api_stop                     = false -> null
      - disable_api_termination              = false -> null
      - ebs_optimized                        = false -> null
      - get_password_data                    = false -> null
      - hibernation                          = false -> null
      - id                                   = "i-00ccfaa36666a3c09" -> null
      - instance_initiated_shutdown_behavior = "stop" -> null
      - instance_state                       = "running" -> null
      - instance_type                        = "t2.micro" -> null
      - ipv6_address_count                   = 0 -> null
      - ipv6_addresses                       = [] -> null
      - key_name                             = "devops-jmi" -> null
      - monitoring                           = false -> null
      - placement_partition_number           = 0 -> null
      - primary_network_interface_id         = "eni-021408d678b59a3ec" -> null
      - private_dns                          = "ip-172-31-3-44.ec2.internal" -> null
      - private_ip                           = "172.31.3.44" -> null
      - public_dns                           = "ec2-35-172-32-105.compute-1.amazonaws.com" -> null
      - public_ip                            = "35.172.32.105" -> null
      - secondary_private_ips                = [] -> null
      - security_groups                      = [
          - "jmi-sg",
        ] -> null
      - source_dest_check                    = true -> null
      - subnet_id                            = "subnet-0a3d7612131c396e7" -> null
      - tags                                 = {
          - "Name" = "jmi-ec2"
        } -> null
      - tags_all                             = {
          - "Name" = "jmi-ec2"
        } -> null
      - tenancy                              = "default" -> null
      - user_data_replace_on_change          = false -> null
      - vpc_security_group_ids               = [
          - "sg-07c8323c44f463aeb",
        ] -> null

      - capacity_reservation_specification {
          - capacity_reservation_preference = "open" -> null
        }

      - credit_specification {
          - cpu_credits = "standard" -> null
        }

      - ebs_block_device {
          - delete_on_termination = false -> null
          - device_name           = "/dev/sdh" -> null
          - encrypted             = false -> null
          - iops                  = 100 -> null
          - tags                  = {
              - "Name" = "jmi-ebs"
            } -> null
          - throughput            = 0 -> null
          - volume_id             = "vol-0ee8c88bd1dc3e3e0" -> null
          - volume_size           = 5 -> null
          - volume_type           = "gp2" -> null
        }

      - enclave_options {
          - enabled = false -> null
        }

      - maintenance_options {
          - auto_recovery = "default" -> null
        }

      - metadata_options {
          - http_endpoint               = "enabled" -> null
          - http_put_response_hop_limit = 1 -> null
          - http_tokens                 = "optional" -> null
          - instance_metadata_tags      = "disabled" -> null
        }

      - private_dns_name_options {
          - enable_resource_name_dns_a_record    = false -> null
          - enable_resource_name_dns_aaaa_record = false -> null
          - hostname_type                        = "ip-name" -> null
        }

      - root_block_device {
          - delete_on_termination = true -> null
          - device_name           = "/dev/sda1" -> null
          - encrypted             = false -> null
          - iops                  = 100 -> null
          - tags                  = {} -> null
          - throughput            = 0 -> null
          - volume_id             = "vol-08e85aaf4d1e163be" -> null
          - volume_size           = 8 -> null
          - volume_type           = "gp2" -> null
        }
    }

  # module.eip.aws_eip.my_eip will be destroyed
  - resource "aws_eip" "my_eip" {
      - allocation_id        = "eipalloc-08df6745683b0664d" -> null
      - association_id       = "eipassoc-0f1a3246b970a815c" -> null
      - domain               = "vpc" -> null
      - id                   = "eipalloc-08df6745683b0664d" -> null
      - instance             = "i-00ccfaa36666a3c09" -> null
      - network_border_group = "us-east-1" -> null
      - network_interface    = "eni-021408d678b59a3ec" -> null
      - private_dns          = "ip-172-31-3-44.ec2.internal" -> null
      - private_ip           = "172.31.3.44" -> null
      - public_dns           = "ec2-35-172-32-105.compute-1.amazonaws.com" -> null
      - public_ip            = "35.172.32.105" -> null
      - public_ipv4_pool     = "amazon" -> null
      - tags                 = {} -> null
      - tags_all             = {} -> null
      - vpc                  = true -> null
    }

  # module.sg.aws_security_group.my_sg will be destroyed
  - resource "aws_security_group" "my_sg" {
      - arn                    = "arn:aws:ec2:us-east-1:026071546831:security-group/sg-07c8323c44f463aeb" -> null
      - description            = "Allow ssh, http and https inbound traffic" -> null
      - egress                 = [
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = ""
              - from_port        = 0
              - ipv6_cidr_blocks = [
                  - "::/0",
                ]
              - prefix_list_ids  = []
              - protocol         = "-1"
              - security_groups  = []
              - self             = false
              - to_port          = 0
            },
        ] -> null
      - id                     = "sg-07c8323c44f463aeb" -> null
      - ingress                = [
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "http from all"
              - from_port        = 80
              - ipv6_cidr_blocks = [
                  - "::/0",
                ]
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 80
            },
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "https from all"
              - from_port        = 443
              - ipv6_cidr_blocks = [
                  - "::/0",
                ]
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 443
            },
          - {
              - cidr_blocks      = [
                  - "0.0.0.0/0",
                ]
              - description      = "ssh from all"
              - from_port        = 22
              - ipv6_cidr_blocks = [
                  - "::/0",
                ]
              - prefix_list_ids  = []
              - protocol         = "tcp"
              - security_groups  = []
              - self             = false
              - to_port          = 22
            },
        ] -> null
      - name                   = "jmi-sg" -> null
      - owner_id               = "026071546831" -> null
      - revoke_rules_on_delete = false -> null
      - tags                   = {
          - "Name" = "jmi-sg"
        } -> null
      - tags_all               = {
          - "Name" = "jmi-sg"
        } -> null
      - vpc_id                 = "vpc-047d4f6939fac5bcf" -> null
    }

Plan: 0 to add, 0 to change, 6 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

aws_eip_association.eip_assoc: Destroying... [id=eipassoc-0f1a3246b970a815c]
aws_volume_attachment.ebs_att: Destroying... [id=vai-4211767009]
aws_eip_association.eip_assoc: Destruction complete after 2s
aws_volume_attachment.ebs_att: Still destroying... [id=vai-4211767009, 10s elapsed]
aws_volume_attachment.ebs_att: Destruction complete after 11s
module.ebs.aws_ebs_volume.my_vol: Destroying... [id=vol-0ee8c88bd1dc3e3e0]
module.ec2.aws_instance.mini-projet-ec2: Destroying... [id=i-00ccfaa36666a3c09]
module.ebs.aws_ebs_volume.my_vol: Still destroying... [id=vol-0ee8c88bd1dc3e3e0, 10s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still destroying... [id=i-00ccfaa36666a3c09, 10s elapsed]
module.ebs.aws_ebs_volume.my_vol: Destruction complete after 11s
module.ec2.aws_instance.mini-projet-ec2: Still destroying... [id=i-00ccfaa36666a3c09, 20s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Still destroying... [id=i-00ccfaa36666a3c09, 30s elapsed]
module.ec2.aws_instance.mini-projet-ec2: Destruction complete after 31s
module.eip.aws_eip.my_eip: Destroying... [id=eipalloc-08df6745683b0664d]
module.sg.aws_security_group.my_sg: Destroying... [id=sg-07c8323c44f463aeb]
module.eip.aws_eip.my_eip: Destruction complete after 1s
module.sg.aws_security_group.my_sg: Destruction complete after 2s

Destroy complete! Resources: 6 destroyed.
jmi@portable-jmi:~/Documents/formationTerraformForBeginners/tp8_miniProjet/app$ 
```
