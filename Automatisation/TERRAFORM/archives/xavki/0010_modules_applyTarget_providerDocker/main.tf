# variables                     ## elles sont présentes dans le fichier terraform.tfvars
variable "local" {}

variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}

## on appelle les modules
module "docker_install" {                 ## docker_install est le nom que nous choississons pour ce module
  source      = "./modules/docker_install"    ## on va chercher les fichiers du module

  ## les variables étant dans terraform.tfvars il faut qu'elles soient prises en compte par le module
  local_mod       = var.local
  ssh_host_mod    = var.ssh_host        ## ssh_host_mod est le nom de la variable ssh_host dans le module qui prend la valeur de la variable ssh_hosts de terraform.tfvars
  ssh_user_mod    = var.ssh_user
  ssh_key_mod     = var.ssh_key          ## ces variables sont "exportées" vers le fichier ./modules/docker_install/variables.tf
}

## 
module "docker_run" {
  source      = "./modules/docker_run"

  ## les variables étant dans terraform.tfvars il faut qu'elles soient prises en compte par le module
  ssh_host_mod    = var.ssh_host        ## ssh_host_mod est le nom de la variable ssh_host dans le module qui prend la valeur de la variable ssh_hosts de terraform.tfvars
}

