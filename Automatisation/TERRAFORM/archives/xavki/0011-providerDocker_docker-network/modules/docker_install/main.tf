
## récupération du script d'installation de docker en local car non ouvert depuis machine cible
resource "null_resource" "local_mod" {
  provisioner "local-exec" {
    command = "curl -fsSl get.docker.com -o get-docker.sh"
  }
}


## mettre 1 timeout de 5 secondes avant de faire la suite (le temps du download de get-docker.sh)
resource "time_sleep" "wait_5_seconds" {
  depends_on = [null_resource.local_mod]

  create_duration = "5s"
}

## connexion à la machine cible
resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user_mod
    host        = var.ssh_host_mod
    private_key = file(var.ssh_key_mod)
  }
  provisioner "file" {
    source      = "./get-docker.sh"
    destination = "/tmp/get-docker.sh"
  }
}

## mettre 1 timeout de 20s avant de faire la suite (le temps du cp de get-docker.sh + connexion)
resource "time_sleep" "wait_20_seconds" {
  depends_on = [null_resource.local_mod]

  create_duration = "20s"
}

resource "null_resource" "ssh_cible" {
  connection {
    type        = "ssh"
    user        = var.ssh_user_mod
    host        = var.ssh_host_mod
    private_key = file(var.ssh_key_mod)
  }
  provisioner "remote-exec" {
    inline  = [
#      "sudo yum update >/dev/null",
      "chmod 755 /tmp/get-docker.sh",
#      "sudo ./tmp/get-docker.sh >/dev/null",
    ]
  }
  # provisioner "file" {
  #   source      = "${path.module}/startup-options.conf"    ## la variable ${path.module} est là pour donner le bon chemin vers le fichier
  #   destination = "/tmp/startup-options.conf"
  # }
  # provisioner "remote-exec" {
  #   inline  = [
  #     "sudo mkdir -p /etc/systemd/system/docker.service.d/",
  #     "sudo cp -a /tmp/startup-options.conf /etc/systemd/system/docker.service.d/startup-options.conf",
  #     "sudo systemctl daemon-reload",
  #     "sudo systemctl restart docker",
  #     "sudo usermod -aG docker toto"
  #   ]
  # }
}


