
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {
  host  = "tcp://${var.ssh_host_mod}:2375"
}

resource "docker_network" "daliNetwork" {
  name  = "dalinetwork"
}

resource "docker_image" "nginx" {
  name  = "harbor.svc.agpm.fr/ext-dockerhub/library/nginx:latest"
}

resource "docker_container" "nginx" {
  image   = docker_image.nginx.latest
  name    = "tata"
  ports {
    internal  = 80
    external  = 80
  }
}