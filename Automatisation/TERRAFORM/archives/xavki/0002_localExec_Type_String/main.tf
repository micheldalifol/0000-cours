variable "maVariableTypeString" {
  type = string
  default = "127.0.0.1.gitlab.test"
}
resource "null_resource" "node1" {
  provisioner "local-exec" {
    command = "echo ${var.maVariableTypeString} > hosts.txt"
  }
}

# output "maVariableTypeString" {
#   value = var.maVariableTypeString
# }

output "test" {
  value = var.maVariableTypeString
}