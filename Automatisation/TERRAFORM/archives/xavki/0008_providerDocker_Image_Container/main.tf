variable "local" {}

variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}

## récupération du script d'installation de docker en local car non ouvert depuis machine cible
resource "null_resource" "local" {
  provisioner "local-exec" {
    command = "curl -fsSl get.docker.com -o get-docker.sh"
  }
}


## connexion à la machine cible
resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }
  provisioner "file" {
    source      = "./get-docker.sh"
    destination = "/tmp/get-docker.sh"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo apt update -qq >/dev/null",
      "sudo chmod 755 get-docker.sh",
      "sudo ./get-docker.sh >/dev/null"
    ]
  }
  provisioner "file" {
    source      = "startup-options.conf"
    destination = "/tmp/startup-options.conf"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo mkdir -p /etc/systemd/system/docker.service.d/",
      "sudo cp -a /tmp/startup-options.conf /etc/systemd/system/docker.service.d/startup-options.conf",
      "sudo systemctl daemon-reload",
      "sudo systemctl restart docker",
      "sudo usermod -aG docker toto"
    ]
  }
}

output "host" {
  value = var.ssh_host
}
output "user" {
  value = var.ssh_user
}
