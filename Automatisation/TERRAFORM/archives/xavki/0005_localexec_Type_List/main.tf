variable "tata" {
  default   = ["127.0.0.1 localhost","192.168.1.133 gitlab.test","blablabla sdkjbvk"]
}

resource "null_resource" "test" {
  count = "${length(var.tata)}"
  provisioner "local-exec" {
    command = "echo '${element(var.tata, count.index)}' >> hosts.txt"
  }
}