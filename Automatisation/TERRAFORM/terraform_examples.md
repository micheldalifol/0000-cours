# <span style="color: #CE5D6B"> **TERRAFORM EXAMPLES** </span>




_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **FOR JOIN FORMAT TOSET** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------
business_lines_parent_id = ["toto", "tata", "titi"]   # test
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "test1" {
  value = [for s in var.business_lines_parent_id : format("%s", s)]
}

output "test2" {
  value = join(", ", [for s in var.business_lines_parent_id : format("%s", s)])
}

output "test3" {
  value = toset(var.business_lines_parent_id)
}

output "test4" {
  value = var.business_lines_parent_id
}

output "test5" {
  value = var.business_lines_parent_id[*]
}
#-------------------------------------------------
```

sortie     
```bash
Changes to Outputs:
  + test1  = [
      + "toto",
      + "tata",
      + "titi",
    ]
  + test2 = "toto, tata, titi"        ## 1 seule variable type string avec toutes les valeurs
  + test3 = [
      + "tata",
      + "toto",
      + "titi",
    ]
  + test4 = [
      + "tata",
      + "toto",
      + "titi",
    ]
  + test5 = [
      + "tata",
      + "toto",
      + "titi",
    ]
```
  ➡️ test1, test3, test4, test5 donnent le même résultat




_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **FOR_EACH dans OUTPUT** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------
business_lines_parent_id = ["toto", "tata", "titi"]   # test
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "test3" {
  for_each = toset(var.business_lines_parent_id)
  value = each.value
}
#-------------------------------------------------
```

sortie     
```bash
│ Error: Unsupported argument
│ 
│   on 99_output.tf line 15, in output "test3":
│   15:   for_each = toset(var.business_lines_parent_id)
│ 
│ An argument named "for_each" is not expected here.
```
  ➡️ pas de for_each dans un output



_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **COUNT in OUTPUT** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------
business_lines_parent_id = ["toto", "tata", "titi"]   # test
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "test4" {
  count = length(var.business_lines_parent_id)
  value = var.business_lines_parent_id[count.index]
}
#-------------------------------------------------
```

sortie     
```bash
│ Error: Unsupported argument
│ 
│   on 99_output.tf line 19, in output "test4":
│   19:   count = length(var.business_lines_parent_id)
│ 
│ An argument named "count" is not expected here.
│ The "count" object can only be used in "module", "resource", and "data"
│ blocks, and only when the "count" argument is set.
```
  ➡️ **count** ne peut être utilisé QUE dans 1 module, resource ou data


_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **COUNT dans module** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------
business_lines_parent_id = ["ou-sblu-r2afewdw", "ou-sblu-hd5gbas7"]   # alpha/Dev, alpha/dev/bl_3
#-------------------------------------------------

vi variables.tf
#-------------------------------------------------
variable "business_lines_parent_id" {
  type = list(string)
  default = []
  description = "The ID of the Organizational Unit where are located all the business lines OUs"
}
#-------------------------------------------------

vi main.tf
#-------------------------------------------------
module "listAccounts" {
  source    = "./aws_org_listaccounts"
  count     = length(var.business_lines_parent_id)
  bl_parent_id_template = var.business_lines_parent_id[count.index]
}
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "modAccounts" {
  value       = module.listAccounts
}
#-------------------------------------------------


vi module/<nomModule>/variables.tf 
#-------------------------------------------------
variable "bl_parent_id_template" {
  type = string
  default = ""
  description = "The ID of the Organizational Unit where are located all the business lines OUs"
}
#-------------------------------------------------

vi module/<nomModule>/data.tf
#-------------------------------------------------
data "aws_organizations_organizational_units" "ou" {
  parent_id = var.bl_parent_id_template
}
#-------------------------------------------------

vi module/<nomModule>/locals.tf
#-------------------------------------------------
locals {
  accounts_list  = data.aws_organizations_organizational_units.ou
  env_id         = var.bl_parent_id_template
}
#-------------------------------------------------

vi module/<nomModule>/outputs.tf
#-------------------------------------------------
output "accounts_list" {
  value = local.accounts_list
}

output "env_id" {
  value = local.env_id
}
#-------------------------------------------------
```

sortie     
```bash
Changes to Outputs:
  + modAccounts = [
      + {
          + accounts_list = {
              + children  = [
                  + {
                      + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-hd5gbas7"
                      + id   = "ou-sblu-hd5gbas7"
                      + name = "BL 3"
                    },
                  + {
                      + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-qd5mwcd2"
                      + id   = "ou-sblu-qd5mwcd2"
                      + name = "BL 1"
                    },
                  + {
                      + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-xp1wctwr"
                      + id   = "ou-sblu-xp1wctwr"
                      + name = "BL 4"
                    },
                  + {
                      + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-a4zx0jem"
                      + id   = "ou-sblu-a4zx0jem"
                      + name = "BL 2"
                    },
                ]
              + id        = "ou-sblu-r2afewdw"
              + parent_id = "ou-sblu-r2afewdw"
            }
          + env_id        = "ou-sblu-r2afewdw"
        },
      + {
          + accounts_list = {
              + children  = []
              + id        = "ou-sblu-hd5gbas7"
              + parent_id = "ou-sblu-hd5gbas7"
            }
          + env_id        = "ou-sblu-hd5gbas7"
        },
    ]
```
  ➡️ une variable de type list(string) avec un `count` et un `count.index` dans le `main.tf` partie **module** permet d'envoyer 1 à 1 les valeurs de la variable en la formattant en type string


_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **OUTPUT LIST OBJECT** 
</span>

Précédemment nous avons ceci comme resultat
```bash
modAccounts        = [
  + {
      + account_list_id     = [
          + "890730305681",
          + "466526841974",
          + "794142261191",
          + "240880849955",
        ]
      + account_list_in_env = {
          + accounts  = [
              + {
                  + arn    = "arn:aws:organizations::976441041624:account/o-679ee8wcxs/890730305681"
                  + email  = "dev03.alpha.aws@thalescloud.io"
                  + id     = "890730305681"
                  + name   = "Thales ALPHA Dev-03 Account"
                  + status = "ACTIVE"
                },
              + {
                  + arn    = "arn:aws:organizations::976441041624:account/o-679ee8wcxs/466526841974"
                  + email  = "dev01.alpha.aws@thalescloud.io"
                  + id     = "466526841974"
                  + name   = "Thales Alpha Dev-01 Account"
                  + status = "ACTIVE"
                },
              + {
                  + arn    = "arn:aws:organizations::976441041624:account/o-679ee8wcxs/794142261191"
                  + email  = "dev04.alpha.aws@thalescloud.io"
                  + id     = "794142261191"
                  + name   = "Thales ALPHA Dev-04 Account"
                  + status = "ACTIVE"
                },
              + {
                  + arn    = "arn:aws:organizations::976441041624:account/o-679ee8wcxs/240880849955"
                  + email  = "dev02.alpha.aws@thalescloud.io"
                  + id     = "240880849955"
                  + name   = "Thales Alpha Dev-02 Account"
                  + status = "ACTIVE"
                },
            ]
          + id        = "ou-sblu-r2afewdw"
          + parent_id = "ou-sblu-r2afewdw"
        }
      + bl_list_in_env      = {
          + children  = [
              + {
                  + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-hd5gbas7"
                  + id   = "ou-sblu-hd5gbas7"
                  + name = "BL 3"
                },
              + {
                  + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-qd5mwcd2"
                  + id   = "ou-sblu-qd5mwcd2"
                  + name = "BL 1"
                },
              + {
                  + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-xp1wctwr"
                  + id   = "ou-sblu-xp1wctwr"
                  + name = "BL 4"
                },
              + {
                  + arn  = "arn:aws:organizations::976441041624:ou/o-679ee8wcxs/ou-sblu-a4zx0jem"
                  + id   = "ou-sblu-a4zx0jem"
                  + name = "BL 2"
                },
            ]
          + id        = "ou-sblu-r2afewdw"
          + parent_id = "ou-sblu-r2afewdw"
        }
      + env_id              = "ou-sblu-r2afewdw"
      + numb_account_by_env = 4
      + numb_bl_by_env      = 4
    },
]
```

et nous ne voulons que les IDs comme resultat dans notre nouvel output (account_list_id) qui est une list(string)


code          
```bash
vi var.tfvars
#-------------------------------------------------
business_lines_parent_id = ["ou-sblu-r2afewdw", "ou-sblu-hd5gbas7"]   # alpha/Dev, alpha/dev/bl_3
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "testAccounts_listId" {
  value       = flatten(module.listAccounts[*].account_list_id)
}
#-------------------------------------------------
```

sortie     
```bash
Changes to Outputs:
+ modAccounts_listId  = [
    + [
        + "890730305681",
        + "466526841974",
        + "794142261191",
        + "240880849955",
      ],
    + [
        + "890730305681",
      ],
  ]
+ testAccounts_listId = [
    + "890730305681",
    + "466526841974",
    + "794142261191",
    + "240880849955",
    + "890730305681",
  ]
```
  ➡️ `flatten()` sort une liste d'objet en 1 seule liste ➡️ il est maintenant possible de faire un `count` ou un `for` (PAS de `for_each`)








_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **SUPPRIMER LES DOUBLONS DANS UNE LISTE** 
</span>

code          
```bash
vi var.tfvars
vi var.tfvars
#-------------------------------------------------
business_lines_parent_id = ["ou-sblu-r2afewdw", "ou-sblu-hd5gbas7"]   # alpha/Dev, alpha/dev/bl_3
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "testAccounts_listId" {
  value       = distinct(flatten(module.listAccounts[*].account_list_id))
}
#-------------------------------------------------
```

sortie     
```bash
Changes to Outputs:
  + modSwitchRole = [
      + {
          + account_id_template = "890730305681"      ## n'apparait qu'1 seule fois ➡️ plus de doublon
        },
      + {
          + account_id_template = "466526841974"
        },
      + {
          + account_id_template = "794142261191"
        },
      + {
          + account_id_template = "240880849955"
        },
    ]
```
  ➡️ `distinct()`  permet de supprimer les doublons dans 1 liste




_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **EXECUTER UN MODULE SEULEMENT SI LA CONDITION EST REMPLIE** 
</span>

Le but est d'exécuter le module seulement si le nom de l'environnement n'est pas "dev"

code          
```bash
vi var.tfvars
#-------------------------------------------------
aws_env                  = "dev"
#-------------------------------------------------

vi main.tf
#-------------------------------------------------
module "encrypt-toto" {
 source    = "./aws_org_acc_encryption"
 count = var.aws_env != "dev" ? 1 : 0           ## condition pour que le module ne soit exécuté QUE SI l'environnement N'EST PAS "dev"
 providers = {
   aws                       = aws.encrypt-toto 
 }
 shortEnv                    = substr("${var.aws_env}", 0, 1)
 region                      = var.region_short
 aws_region                  = var.region
 appstackcode                = var.appstackcode
 bl                          = var.bl
 accountRunPipeline          = data.aws_caller_identity.current.account_id
}
#-------------------------------------------------
```

sortie     
  ➡️ dans l'exemple ci-dessus comme la variable est "dev" le module ne sera pas exécuté



_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **EXECUTER UN MODULE SEULEMENT SI LA CONDITION EST REMPLIE -> UNE VALEUR EST DANS 1 LISTE** 
</span>

Le but est d'exécuter le module seulement si la region est dans la liste des regions pour la branche définies

code          
```bash
vi var.tfvars
#-------------------------------------------------
regions        = ["eu-west-1", "eu-central-1"]
#-------------------------------------------------

vi main.tf
#-------------------------------------------------
module "aws_s3_dns_log_ew1" {
  source                  = "./modules/aws_s3_dns_log"
  count                   = contains(var.regions, "eu-west-1") ? 1 : 0    ## SEULEMENT SI "eu-west-1" fait partie de la liste var.regions -> ici oui
  providers = {
    aws                   = aws.eu-west-1
  }
  aws_env                 = var.aws_env
  shortEnv                = var.shortEnv
  logs_s3_retention       = var.logs_s3_retention_region 
  appstackcode            = var.appstackcode 
}

module "aws_s3_dns_log_as1" {
  source                  = "./modules/aws_s3_dns_log"
  count                   = contains(var.regions, "ap-southeast-1") ? 1 : 0 ## SEULEMENT SI "ap-southeast-1" fait partie de la liste var.regions -> ici non
  providers = {
    aws                   = aws.ap-southeast-1
  }
  aws_env                 = var.aws_env
  shortEnv                = var.shortEnv
  logs_s3_retention       = var.logs_s3_retention_region 
  appstackcode            = var.appstackcode 
}
#-------------------------------------------------
```

sortie     
  ➡️ dans l'exemple ci-dessus le ée module ne sera pas exécuté





_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **USE LOCAL-EXEC PROVISIONER WITH NULL_RESOURCE** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------
##debug provisioner local-exec 
resource "null_resource" "debug_localexec" {
  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    command = <<-EOT
      echo "region : ${data.aws_region.current.name}"
      echo "accountId : ${data.aws_caller_identity.current.account_id}"
    EOT
  }
}
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "local-exec_multi" {
  value = null_resource.cli_command_local
}

output "local-exec_single" {
  value = null_resource.cli_local_single_command
}
#-------------------------------------------------
```

sortie     
```bash
 # module.encrypt_211429699615_us-east-1[0].null_resource.block_public_access_ebs_snapshots will be destroyed
  # (because null_resource.block_public_access_ebs_snapshots is not in configuration)
  + resource "null_resource" "block_public_access_ebs_snapshots" {
      + id = "8265728641389420890" -> null
    }
```
  ➡️ le resultat qui s'affiche n'est pas utilisable en tant que variable !!



_______________________________________________________________________________________________________________________    

<span style="color: #CE5D6B">

##  **MISE EN PLACE D'UNE CONDITION POUR UTILISATION D'UNE VARIABLE** 
</span>


Dans l'example ci-dessous, en fonction du nom du projet gitlab (cas AWS Commercial ou cas AWS Us Gov), la variable utilisée pour les regions AWS ne sera pas la même     
Dans le cas des repo "US Gov", les noms des projets commencent tous par **awsgow** ➡️ ceci sera donc utilisé dans la condition


code          
```bash
## fichier variable "récupérant" les variables gitlab
vi gitlab_variables.yml
#-------------------------------------------------
variables:
  # General
  TF_VAR_project_name: $CI_PROJECT_NAME
#-------------------------------------------------


## fichier comportant les 2 listes de regions
vi var.tfvars
#-------------------------------------------------
Commercial_whiteListRegions          = [
            "eu-west-1",
            "sa-east-1",
            "us-east-1",
            "us-west-2"
          ]

USGov_whiteListRegions               = [
            "us-gov-west-1",
            "us-gov-east-1"
          ]
#-------------------------------------------------


vi locals.tfvars
#-------------------------------------------------
locals { 
  
  endBranchEnvName   = substr("${var.branchEnv_name}", -3, -1)
  cleanbranchEnvName = trimsuffix(var.branchEnv_name, local.endBranchEnvName)
  cplt_policy_name   = "${substr("${var.aws_env}", 0, 1)}-${local.cleanbranchEnvName}"

  ## cette partie est mise en place pour n'avoir la variable "whiteListRegion" que dans le fichier ./vars/terraform.commons.tfvars
  ## -> avec cette condition SI le nom du projet conteint "awsgow" ALORS la liste des regions sera USGov_whiteListRegions SINON Commercial_whiteListRegions
  ## IF the project name contains "awsgov" THEN use as whitelistRegion the variable USGov_whiteListRegions, ELSE the variable Commercial_whiteListRegions
  whiteListRegions = strcontains(var.project_name, "awsgov") ? var.USGov_whiteListRegions : var.Commercial_whiteListRegions
 
}
#-------------------------------------------------

vi main.tfvars
#-------------------------------------------------
module "scp" {
  source                    = "./modules/nomModule"
  cplt_policy_name          = local.cplt_policy_name
  listOfScps                = var.listOfScps 
  whiteListRegions          = local.whiteListRegions 
  count                     = length(var.listOfScps)      ## pour pouvoir boucler sur chaque fichier définis dans la variable "listOfScps" définie dans les fichiers terraform.xxx.tfvars
  tpl_file                  = "./policies/${var.listOfScps[count.index]}.tftpl"       ## boucle sur chaque fichier défini dans la variable "listOfScps"
}
#-------------------------------------------------

vi modules/nomModule/locals.tf
#-------------------------------------------------
locals {
  raw_file        = trimsuffix(var.tpl_file, ".tftpl")
  path_removed    = trimspace(local.raw_file)
  scp_name        = basename(local.path_removed)
  clear_file      = templatefile(var.tpl_file, {                        ## on va chercher le template pour créer le fichier en lui assignant des variables
    regions           = var.whiteListRegions                                ## variable "regions" définie pour les templates
    arn_partition     = data.aws_partition.current.partition                ## variable "arn_partition" définie pour les templates
  })
}
#-------------------------------------------------

vi modules/nomModule/main.tfvars
#-------------------------------------------------
resource "aws_organizations_policy" "this" {
  name    = "scp-${local.scp_name}-${var.cplt_policy_name}.json" 
  content = local.clear_file
}
#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------
output "whiteListRegions" {
  value       = var.whiteListRegions
}
#-------------------------------------------------
```

sortie     
```bash
          + whiteListRegions         = [
              + "eu-west-1",
              + "sa-east-1",
              + "us-east-1",
              + "us-west-2",
            ]
```
  ➡️ la liste des régions est bien définies en fonction du nom du projet



_______________________________________________________________________________________________________________________   
  

<span style="color: #CE5D6B">

##  **CREATION DE FICHIER JSON A PARTIR DE TEMPLATES .tftpl ET DE LA FONCTION templatefile()** 
</span>

Le but est la creation de fichiers json à partir de templates en utilisant des variables et en encodant au format json pour avoir la bonne syntaxe et ne pas avoir de whitespaces

code          
```bash
vi ./policies/xx.tftpl
#--------------------------------------------------------------------------
${
  jsonencode({
  "Statement": [
    {
      ...
}
)}
#--------------------------------------------------------------------------

vi modules/scp/04_locals.tf
#--------------------------------------------------------------------------
locals {
  raw_file        = trimsuffix(var.tpl_file, ".tftpl")
  path_removed    = trimspace(local.raw_file)
  scp_name        = basename(local.path_removed)
  clear_file      = templatefile(var.tpl_file, {                        ## on va chercher le template pour créer le fichier en lui assignant des variables
    regions           = var.whiteListRegions                                ## variable "regions" définie pour les templates
    arn_partition     = data.aws_partition.current.partition                ## variable "arn_partition" définie pour les templates
  })
}
#--------------------------------------------------------------------------

## exemple de fichier tftpl avec variable à utiliser  
vi ./policies/WhiteListRegions.tftpl
#--------------------------------------------------------------------------
${
  jsonencode({
  "Statement": [
    {
      "Condition": {
        "StringNotEquals": {
          "aws:RequestedRegion": [ for region in regions : region ],          ## ici on fait une boucle sur la variables "regions" qui est une liste
        },
        "StringNotLike": {
          "aws:PrincipalARN": [
            "arn:${arn_partition}:iam::*:role/tata",     ## ici on utilise la variable "arn_partition" pour commercial ou US Gov
            "arn:${arn_partition}:iam::*:role/toto",
            "arn:${arn_partition}:iam::*:role/titi"
          ]
        }
      },
      "Effect": "Deny",
      "NotAction": [
        "iam:*",
        "organizations:*",
        "support:*"
      ],
      "Resource": "*",
      "Sid": "WhiteListRegions"
    }
  ],
  "Version": "2012-10-17"
}
)}
#--------------------------------------------------------------------------
```


  ➡️ le fichier créé est un fichier json correctement formatté



_______________________________________________________________________________________________________________________  

  
<span style="color: #CE5D6B">

##  **** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------

#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------

#-------------------------------------------------
```

sortie     
```bash

```
  ➡️ 



_______________________________________________________________________________________________________________________  


  
<span style="color: #CE5D6B">

##  **** 
</span>

code          
```bash
vi var.tfvars
#-------------------------------------------------

#-------------------------------------------------

vi outputs.tf
#-------------------------------------------------

#-------------------------------------------------
```

sortie     
```bash

```
  ➡️ 



_______________________________________________________________________________________________________________________  