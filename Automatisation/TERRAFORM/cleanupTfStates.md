# FAIRE DU MENAGE DANS LES TF STATES D'UNE PIPELINE AVANT DE L'EXECUTER

Le but est de voir comment faie du ménage dans les tf states avant l'exécution de la pipeline     
Dans notre cas, il s'agit de la pipeline qui déploie les SCPs sur AWS avec une Organizations Units cible qui a été supprimée de AWS Organizations mais qui était utilisée dans la pipeline avec terraform     

Pour cela on va récupérer en local les tfstates (pour pouvoir les lire), faire les modifs(via API directement)


## ACTIONS A FAIRE   

1. Aller dans le repo gitlab du projet est créer un Access Token dans `Settings/Access Token`
   `FrqRHjy9V7XfxT3MQ4Px`   
   ![gitlab_accessToken](./images/cleanupTfStates/gitlab_accessToken.png)          

2. Aller dans dans osn ficheir `~/.bashrc` afin de mettre en place les variables suivantes 
```bash
vi ~/.bashrc
#---------------------------------------------------------
export CI_ENVIRONMENT_SLUG="cplmgtreviewdev"
export TF_VAR_TF_HTTP_USERNAME=user
export TF_VAR_TF_HTTP_PASSWORD=FrqRHjy9V7XfxT3MQ4Px

export GITLAB_PROJECT_NAME=aws_org_policy
export GITLAB_PROJECT_ID=2382
export GITLAB_STATE_NAME=${CI_ENVIRONMENT_SLUG}
export GITLAB_TF_ADDRESS=https://gitlab.sre.ops.gcloud.thalescloud.io/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_STATE_NAME}
#---------------------------------------------------------
```

avec:     
 -  `CI_ENVIRONMENT_SLUG` le nom du tfstate qu'on souhaite modifier (dans ce cas la branche review, l'environnement dev)  
    ➡️on récupère cette info depuis la console gitlab où sont présents les tfstates (`Operate/terraform states`)      
    ![gitlab_tsfstates](./images/cleanupTfStates/gitlab_tsfstates.png)            

 - `TF_VAR_TF_HTTP_USERNAME` un nom par defaut (`user`) dans notre cas, cela peut aussi être son email....          
 
 - `TF_VAR_TF_HTTP_PASSWORD` l'access token précédemment créé   
 
 - `GITLAB_PROJECT_NAME`  le nom du projet (dans gitlab)         
 
 - `GITLAB_PROJECT_ID` l'ID du projet dans gitlab (`Settings/General`)    


3. On fait un terraform init avec la commande suivante          
```bash
terraform init -backend-config="address=${GITLAB_TF_ADDRESS}" -backend-config="lock_address=${GITLAB_TF_ADDRESS}/lock" -backend-config="unlock_address=${GITLAB_TF_ADDRESS}/lock" -backend-config="username=${TF_VAR_TF_HTTP_USERNAME}" -backend-config="password=${TF_VAR_TF_HTTP_PASSWORD}" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5" -reconfigure
```
 ➡️ cette action peut prendre du temps    

4. On fait un `terraform state list` (pour avoir la liste complète des tf states du projet)


5. On supprime les ressources qu'on ne veut pas conserver avec la commande `terraform state rm <ressource>`     
   ➡️ les ressources peuvent être les "module" et les "data"          
   ➡️ penser à échapper les " parce que cette commande à besoin d'un string pour être effectuée     

6. Vérifier que ce qu'on veut supprimer n'apparait plus dans le code de la pipeline         

7. Faire un commit puis un push        

8. Exécuter la pipeline  ➡️ plus d'erreur, les tfstates ont été mis à jour       

9. Supprimer l'access token du repo   





### EXEMPLE          

```bash
vi ~/.bashrc

#-----------------------------------------------------------------------------
## TERRAFORM TFSTATES CLEANUP
export CI_ENVIRONMENT_SLUG="cplmgtreviewdev"
export TF_VAR_TF_HTTP_USERNAME=user
export TF_VAR_TF_HTTP_PASSWORD=FrqRHjy9V7XfxT3MQ4Px

export GITLAB_PROJECT_NAME=aws_org_policy
export GITLAB_PROJECT_ID=2382
export GITLAB_STATE_NAME=${CI_ENVIRONMENT_SLUG}
export GITLAB_TF_ADDRESS=https://gitlab.sre.ops.gcloud.thalescloud.io/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_STATE_NAME}
#-----------------------------------------------------------------------------

## exécute le terraform init
terraform init -backend-config="address=${GITLAB_TF_ADDRESS}" -backend-config="lock_address=${GITLAB_TF_ADDRESS}/lock" -backend-config="unlock_address=${GITLAB_TF_ADDRESS}/lock" -backend-config="username=${TF_VAR_TF_HTTP_USERNAME}" -backend-config="password=${TF_VAR_TF_HTTP_PASSWORD}" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5" -reconfigure



## check the terraorm states
terraform state list


## cleanup the tfstates for this ou_id: 
#### on ajoute 1 \ avec les quotes
terraform state rm    
terraform state rm 
terraform state rm 
terraform state rm 
```