# <span style="color: #CE5D6B">TERRAFORM</span>


_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

## GENERALITES
</span>

[Terraform](https://www.terraform.io/)
[documentation officielle Terraform](https://learn.hashicorp.com/terraform?utm_source=terraform_io)

> **Terraform** est une infrastructure open source en tant qu'outil logiciel de code qui fournit un flux de travail CLI cohérent pour gérer des centaines de services cloud. Terraform codifie les API cloud dans des fichiers de configuration déclaratifs.
> Ses actions se décomposent en _3 grandes parties_ pour pouvoir définir une **IAC: Infrastructure As Code**:
> * **Write**: Écrit l'infrastructure en tant que code à l'aide de fichiers de configuration déclaratifs. Le langage de configuration HashiCorp (HCL) permet des descriptions concises des ressources à l'aide de **blocs**, d'**arguments** et d'**expressions**. Pour cela il est utilisé des fichiers de configuration déclaratifs pour gérer le cycle de vie complet d'une infrastructure: _créer de nouvelles ressources, gérer celles qui existent déjà et détruire celles qui ne sont plus nécessaires_
> * **Plan**: Exécute `terraform plan` pour vérifier si le plan d'exécution d'une configuration correspond aux attentes avant de provisionner ou de modifier l'infrastructure.
> * **Apply**: Applique les modifications à des centaines de fournisseurs de cloud avec `terraform apply ` pour atteindre l'état souhaité de la configuration.


Pour déployer l'infrastructure avec Terraform :

**Scope** - Identifiez l'infrastructure de votre projet.              
**Author** - Rédigez la configuration de votre infrastructure.              
**Initialize** - Installez les plugins dont Terraform a besoin pour gérer l'infrastructure.              
**Plan** - Prévisualisez les modifications que Terraform apportera pour correspondre à votre configuration.          
**Apply** - Effectuez les modifications prévues.            


<div class="alert" style='padding:0.1em; background-color:#F6D2F3; color:#69337A'>
<span>

⚠️                   
Dans le code terraform il faut se rappeler que:           
- **resource**  ➡️ va permet de créer quelque chose sur la cible (équivalent d'une `Requête POST, PUT, DELETE`)                           
- **data**      ➡️ va permettre de récupérer quelque chose depuis la cible (équivalent d'une `Requête GET`)                  
⚠️               

</span></div>

_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

## INSTALLATION DE TERRAFORM
</span>

[All Terraform Tutorials](https://learn.hashicorp.com/terraform?utm_source=terraform_io)


=======================================================================================================================  

<span style="color: #EA9811">

### Installation sur debian
</span>


S'assurer que les systèmes soient à jour et que les packages gnupg, software-properties-common et curl sont installés.
```bash
## 1.
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl

## 2. Ajouter la clé GPG HashiCorp
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

## 3. Ajouter le repo linux HashiCorp officiel
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

## 4. Update le repo et installation de terraform CLI
sudo apt-get update && sudo apt-get install terraform
```



=======================================================================================================================  

<span style="color: #EA9811">

### Installation sur centos / RHEL
</span>


```bash
## 1. Installer yum-config-manager
sudo yum install -y yum-utils

## 2. Ajouter le repo linux HashiCorp officiel
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo

## 3. Installation de terraform CLI
sudo yum -y install terraform
```


=======================================================================================================================  

<span style="color: #EA9811">

### Installation su Fedora
</span>


```bash
## 1. Installer dnf-config-manager
sudo dnf install -y dnf-plugins-core

## 2. Ajouter le repo linux HashiCorp officiel
sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo

## 3. Installation de terraform CLI
sudo dnf -y install terraform
```


=======================================================================================================================  

<span style="color: #EA9811">

### Installation sur amazon linux
</span>


```bash
## 1. Installer yum-config-manager
sudo yum install -y yum-utils

## 2. Ajouter le repo linux HashiCorp officiel
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo

## 3. Installation de terraform CLI
sudo yum -y install terraform
```


=======================================================================================================================  

<span style="color: #EA9811">

### Vérification de la bonne installation
</span>


```bash
terraform -help
```
> Cela doit afficher l'aide de terraform si l'installation a bien été faite

OU
```bash
terraform -help plan
```

> _ Remarque_:
> * Si vous obtenez une erreur indiquant que terraform est introuvable, votre **variable d'environnement PATH** n'a pas été configurée correctement. Veuillez revenir en arrière et vous assurer que votre variable PATH contient le répertoire dans lequel Terraform a été installé.
> * Si vous utilisez **Bash** ou **Zsh**, vous pouvez activer la complétion par tabulation pour les commandes Terraform. Pour activer la saisie semi-automatique, assurez-vous d'abord qu'un fichier de configuration existe pour le shell que vous avez choisi.
```bash
##
touch ~/.bashrc
## OU
touch ~/.zshrc

##
terraform -install-autocomplete
### Une fois le support de saisie semi-automatique installé, vous devrez redémarrer votre shell
```
[quick start tutorial](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/certification-associate-tutorials#quick-start-tutorial)



_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B"> 

## DOCUMENTATIONS TERRAFORM
</span>


Une documentation Terraform CLI existe avec l'ensemble des commandes terraform utilisable   

Une documentation terraform par provider existe et est nécessaire pour pouvoir mettre en place nos Infra-As-Code 

[Documentation Terraform CLI](https://developer.hashicorp.com/terraform/cli/commands)     
 
[Documentation Terraform Providers](https://registry.terraform.io/)        
[Documentation Terraform AWS](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)      
[Documentation Terraform GCP](https://registry.terraform.io/providers/hashicorp/google/latest/docs)      
[Documentation Terraform Azure](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs)      
[Documentation Terraform Remote](https://registry.terraform.io/providers/tenstad/remote/latest/docs)      
[Documentation Terraform Local](https://registry.terraform.io/providers/hashicorp/local/latest/docs)      
[Documentation Terraform Vault](https://registry.terraform.io/providers/hashicorp/vault/latest/docs)      
[Documentation Terraform tls](https://registry.terraform.io/providers/hashicorp/tls/latest/docs)      
[Documentation Terraform Docker](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs)      
[Documentation Terraform Kubernetes](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)      


[Documentation Terraform - AWS Provider](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)      


=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - CLI** 
</span>


[Documentation terraform: terraform cli](https://developer.hashicorp.com/terraform/cli)    

Toutes les commandes terraform commencent par la commande `terraform`     

[Documentation - terraform cheatsheet](https://spacelift.io/blog/terraform-commands-cheat-sheet)    


Les commandes basiques de terraform sont: 

```bash
terraform version                               # connaitre la version de terraform installée
terraform -chdir=<path_to/tf> <subcommand>      # pour exécuter terraform à partir des fichiers tf
terraform init                                  # pour initialiser le projet terraform (avec le provider)
terraform plan                                  # pour créer le plan terraform
terraform apply                                 # pour appliquer le plan terraform
terraform destroy                               # pour supprimer ce qui est dans le plan terraform
```


```bash
# pour sauver le plan dans 1 fichier à part
terraform plan -out <planName>  

# pour ecrire un plan de destruction
terraform plan -destroy 

# pour appliquer un plan specifique
terraform plan <planName> 

# pour appliquer des changements seulement sur certaines cibles du plan
terraform apply -target=<resourceName>  

# pour appliquer une variable au plan et l'appliquer
terraform apply -var myVariable=<variable>  

# pour connaitre la liste des providers utilisés
terraform providers
```

Pour avoir l'aide depuis le terminal:
```bash
terraform   # entree        affiche toute les commandes terraform qu'on peut faire 

## pour l'aide des sous-commandes  
terraform <sous-commande> -h      # affiche l'aide pour la sous-commande: ex: terraform plan -h 
```

Les étapes de terraform sont: 
```bash
# une fois dans le répertoire de travail terraform (qui contient au moins 1 fichier main.tf)
terraform init 
terraform plan
terraform apply

terraform destroy
```



=======================================================================================================================    


<span style="color: #EA9811">

### **PROVIDERS TERRAFORM**        
</span>

Chaque provider permet l'ajout de `resource` ou `data`          
Sans provider aucune action ne peut être implémenté dans l'infrastructure       

La liste des providers utilisés par Terraform est fournit dans la page [Terrafform Registry - providers](https://registry.terraform.io/browse/providers?ajs_aid=36bcbc06-4779-4683-a40c-74014f366ff9&product_intent=terraform)          


.......................................................................................................................    

<span style="color: #11E3EA">

#### **RECOMMANDATIONS DE PROVIDER**
</span>


Chaque provider est releasé séparemment par Terraform      
Terraform recommande l'utilisation de bloc `required_providers` avec la version "minimale" et "supérieure" pour pouvoir exécuter le `terraform init`       


```bash
terraform {
  required_providers {                                ## bloc OBLIGATOIREMENT imbriqué dans un bloc "terraform"
    mycloud = {                                       ## "mycloud" est le nom local définit pour ce provider
      source  = "mycorp/mycloud"                      ## source du provider (donné dans la doc terraform mentionnée plus haut)
      version = ">= 1.0"                              ## version "minimale" et "supérieure" pouvant être utilisée
    }
  }
}
```

.......................................................................................................................    

<span style="color: #11E3EA">

#### **CONFIGURATION DE PROVIDER**    
</span>


Les configurations de providers se font dans le **module racine**       
Les modules enfants récupèrent les configuration depuis ce module racine (explications dans la suite du document)       

Un bloc `provider` se définit de la manière suivante   

```bash
provider "google" {                             ## "google" est un nom local définit dans le bloc "required_providers"
  project = "acme-app"                          ## "project" et "region" sont des arguments du bloc provider
  region  = "us-central1"
}
```



La définition d'un provider se fait dans bloc `provider` DANS le module Racine UNIQUEMENT    
Il est ensuite partagé implicitement à tous les enfants du projet (sauf dans le cas de déclaration explicite de provider pour 1 mule enfant)          

1 module appelé par d'autres modules NE DOIT contenir AUCUN bloc `provider`      

1 module ayant ses propre configurations de providers ne peut pas faire de `for_each`, `count` ou `depends_on`    

Il FAUT s'assurer que toutes les ressources appartenant à 1 configuration particulière de provider soient détruites pour pouvoir supprimer le bloc `provider` dans la configuration (dans le cas contraire une erreur apparait)     




=======================================================================================================================    


<span style="color: #EA9811">

### **PROVIDERS DANS LES MODULES**    

</span>

**Chaque module DOIT déclarer ses propres providers** avec la version minimale à utiliser et en utilisant le bloc `required_providers`    

```bash
terraform {
  required_providers {
    aws = {                             ## "aws" nom local dans le module définit pour ce provider
      source  = "hashicorp/aws"
      version = ">= 2.7.0"              ## version minimale à utiliser
    }
  }
}
```


.......................................................................................................................    

<span style="color: #11E3EA">

#### **ALIAS DE PROVIDER DANS LES MODULES**
</span>

L'utilisation de l'argument `configuration_aliases` dans la configuration du provider du module enfant permet de déclarer plusieurs providers dans ce module    

```bash
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.7.0"
      configuration_aliases = [ aws.alternate, aws.toto ]       ## ces 2 providers ont été définis dans le bloc module du module root
    }
  }
}
```


.......................................................................................................................    

<span style="color: #11E3EA">

#### **HERITAGE IMPLICITE DU PROVIDER**
</span>

Un module enfant hérite des configuration du provider par défaut de son parent     


```bash
#-----------------------------------------------
## module root
provider "aws" {                          ## le provider par défaut est celui qui ne contient pas l'argument "alias"
  region = "us-west-1"
}

module "child" {
  source = "./child"
}
#-----------------------------------------------

#-----------------------------------------------
## module enfant
resource "aws_s3_bucket" "example" {
  bucket = "provider-inherit-example"           ## pas besoin de définir chez l'enfant le provider par defaut
}
#-----------------------------------------------
```


.......................................................................................................................    

<span style="color: #11E3EA">

#### **PROVIDERS EXPLICITES**
</span>

L'utilisation sur le module Racine de l'argument `providers` dans le bloc `module` est OBLIGATOIRE pour définir explicitement les configurations des providers dans le module enfant    

⚠️ Cela est identique à l'ajout de l'arguement `provider` dans chaque `resource` et/ou `data` du module enfant     
⚠️ MAIS dans le cas où plusieurs providers dont définis dans le bloc `providers` du bloc `module` du module racine, il faudra définir le provider pour chaque `resource` et/ou `data`  

```bash
#######################################################
## module root

#### décalration initiale du provider
#-----------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.7.0" 
    }
  }
}
#-----------------------------------------------

#### déclaration des providers
#-----------------------------------------------
provider "aws" {
  alias  = "usw1"
  region = "us-west-1"
}

provider "aws" {
  alias  = "usw2"
  region = "us-west-2"
}
#-----------------------------------------------

#### déclaration des providers dans le module
#-----------------------------------------------
module "example" {
  source    = "./example"
  providers = {
    aws.src = aws.usw1          ## "aws.src" et "aws.dst" son des noms définis pour le module "example"
    aws.dst = aws.usw2
  }
}
#-----------------------------------------------

#######################################################

#######################################################

## module enfant

#### déclaration des providers sur le module
#-----------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.7.0"
      configuration_aliases = [ aws.src, aws.dst ]
    }
  }
}
#-----------------------------------------------

#### décalaration des resources et/ou data dans le module (ici cas avec 2 providers)
#-----------------------------------------------
resource "aws_instance" "web_src" {
  provider               = aws.src                  ## définition du provider
  ami                    = "ami-a0cfeed8"
  instance_type          = "t2.micro"
  user_data              = file("init-script.sh")

  tags = {
    Name = random_pet.name.id
  }
}

resource "aws_instance" "web_dst" {
  provider               = aws.dst                  ## définition du provider        
  ami                    = "ami-a0cfeed8"
  instance_type          = "t2.micro"
  user_data              = file("init-script.sh")

  tags = {
    Name = random_pet.name.id
  }
}
#-----------------------------------------------
#######################################################
```

Les 2 ressources sont les mêmes mais sur des providers différents (le nom de la ressource est différent): dans ce cas ces ressources ne seront pas créer dans la même région

>_Remarque: il est possible de mettre en place des blocs dynamic afin de "réduire" le code



=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - CONFIGURATION LANGUAGE** 
</span>


Le langage terraform a pour syntaxe ceci  

```bash
resource "aws_vpc" "main" {
    cidr_block     =   var.base_cidr_block
}

<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
    # block body
    <IDENTIFIER>    =   <EXPRESSION>    # argument
}
```

La syntaxe de terraform consiste en 3 élements:
- **blocks**        ➡️ containers pour les objets tels que les ressources    
- **arguments**     ➡️ assigne une valeur à un nom    
- **expressions**   ➡️ représente une valeur    



Les fichiers terraform ont une extension particlière: `.tf` ou `.tf.json`    
Le répertoire de travail s'appelle le `root directory` dans lequel se trouve le fichier `main.tf` et les différents modules     

Il existe des fichiers pour overrider ce qui est indiqué: 

```bash
# fichier terraform config 
resource "aws_instance" "web" {
    instance_type   =   "t2.micro"
    ami             =   "ami-408c7f28"
}

## fichier override
resource "aws_instance" "web" {
    ami    =   "foo"
}

## vont donner lors de l'apply 
resource "aws_instance" "web" {
    instance_type   =   "t2.micro"
    ami             =   "foo"
}
```


=======================================================================================================================  

  

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - USING PROVIDERS** 
</span>

Pour pouvoir utiliser les services d'un cloud provider il faut d'abord configurer le provider à utiliser     

_exemple: provider aws_     

```yaml
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}
```



.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - PROVIDERS - AUTHENTIFICATION ET CONFIGURATION** 
</span>


La configuration du provider AWS peut provenir de plusieurs sources, qui sont appliquées dans l'ordre suivant :     
 1. Paramètres dans la configuration du provider          
 2. Variables d'environnement            
 3. Fichiers d'informations d'identification partagés       
 4. Fichiers de configuration partagés         
 5. Identifiants du conteneur         
 6. Identifiants du profil d'instance et région           

Le provider AWS prend en charge l'hypothèse d'un rôle IAM, soit dans le paramètre de bloc de configuration du fournisseur `assume_role`, soit dans un [profil nommé](https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/cli-configure-role.html).     

Un **profil nommé** est la définition d'un profile dans le fichier `~/.aws/config` de la forme suivante: 
```bash
[profile marketingadmin]
role_arn = arn:aws:iam::123456789012:role/marketingadminrole
source_profile = user1
```


Le provider AWS prend en charge l'hypothèse d'un rôle IAM à l'aide de la fédération d'identité Web et d'OpenID Connect (OIDC). Cela peut être configuré soit à l'aide de variables d'environnement, soit dans un profil nommé.

A l'utilisation d'un profil nommé le provider AWS supporte les credentials provenant d'une source extérieure    
```yaml
provider "aws" {
  region     = "us-west-2"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}
```

Il est possible d'ajouter d'autres paramètres: 
- `profile`         
- `shared_config_files`         
- `shared_credentials_files`       


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - VARIABLES ENVIRONNEMENT** 
</span>


Les variables d'environnement suivantes peuvent être utilisées pour fournir les valeurs des champs des parametres du provider AWS    
- `AWS_ACCESS_KEY_ID`           
- `AWS_SECRET_ACCESS_KEY`           
- `AWS_SESSION_TOKEN`           
- `AWS_REGION`           
- `AWS_DEFAULT_REGION`           

On les utilise de la manière suivante: 

```bash
export AWS_ACCESS_KEY_ID="anaccesskey"
export AWS_SECRET_ACCESS_KEY="asecretkey"
export AWS_REGION="us-west-2"

vi <NomProjet>/providers.tf
#--------------------------------------------------
provider "aws" {}
#--------------------------------------------------

terraform plan
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - CREDENTIALS FILES ET SHARED CONFIGURATION** 
</span>


Le provider AWS peut obtenir des informations d'identification et d'autres paramètres à partir des fichiers de configuration et d'informations d'identification partagés     
Par défaut, ces fichiers se trouvent dans `$HOME/.aws/config` et `$HOME/.aws/credentials` sous Linux et macOS, ainsi que `%USERPROFILE%\.aws\config` et `%USERPROFILE%\.aws\credentials` sous Windows

Si aucun profilé nommé spécifié, le profil par `default` sera utilisé.      
Ce profil utilise la variable `AWS_PROFILE`    

```bash
vi <NomProjet>/providers.tf
#--------------------------------------------------
provider "aws" {
  shared_config_files      = ["/Users/tf_user/.aws/conf"]
  shared_credentials_files = ["/Users/tf_user/.aws/creds"]
  profile                  = "customprofile"
}
#--------------------------------------------------
```



+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - CONTAINER CREDENTIALS** 
</span>

Si vous exécutez Terraform sur CodeBuild ou ECS et que vous avez configuré un rôle de tâche IAM, Terraform peut utiliser le IAM Task Role du conteneur.      
Cette prise en charge est basée sur le fait que les variables d'environnement `AWS_CONTAINER_CREDENTIALS_RELATIVE_URI` et `AWS_CONTAINER_CREDENTIALS_FULL_URI` sont automatiquement définies par ces services ou manuellement pour une utilisation avancée.

Si vous exécutez Terraform sur EKS et avez configuré les rôles IAM pour les Service Accounts (IRSA), Terraform peut utiliser le rôle du pod.       
Cette prise en charge est basée sur les variables d'environnement `AWS_ROLE_ARN` et `AWS_WEB_IDENTITY_TOKEN_FILE` définies automatiquement par Kubernetes ou manuellement pour une utilisation avancée.




+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - INSTANCE PROFILE CREDENTIALS ET REGION** 
</span>


Lorsque le provider AWS s'exécute sur une instance EC2 avec un ensemble de profils d'instance IAM, le provider peut obtenir des informations d'identification à partir du service de métadonnées d'instance EC2.           
Un endpoint personnalisé pour le service de métadonnées peut être fourni à l'aide du paramètre `ec2_metadata_service_endpoint` ou de la variable d'environnement `AWS_EC2_METADATA_SERVICE_ENDPOINT`.




+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - IAM ASSUMING ROLE** 
</span>


S'il dispose d'un **role ARN**, le provider AWS tentera d'assumer ce rôle à l'aide des informations d'identification fournies.

```yaml
provider "aws" {
  assume_role {
    role_arn     = "arn:aws:iam::123456789012:role/ROLE_NAME"
    session_name = "SESSION_NAME"
    external_id  = "EXTERNAL_ID"
  }
}
```

[Utilisation de `assumeRole` pour le provisionnement de ressources AWS à travers de multiples accounts](https://developer.hashicorp.com/terraform/tutorials/aws/aws-assumerole)       

Le `assumeRole` permet d'avoir des privilèges supplémentaires temporaires afin de pouvoir faire des actions spécifiques     

Pour configurer un accès `assumeRole` un role IAM doit être spécifié avec les droits souhaités et quelles entités peuvent l'assumer    
`AssumeRole` peut accorder l'accès au sein ou entre les comptes AWS           
Si vous administrez plusieurs comptes AWS, vous pouvez utiliser la configuration AssumeRole pour activer l'accès étendu à tous les comptes sans avoir à gérer des utilisateurs individuels dans chaque compte dont ils peuvent avoir besoin pour interagir avec les ressources         

Le provider AWS Terraform peut utiliser les informations d'identification `AssumeRole` pour s'authentifier auprès d'AWS        


_Exemple_:       
Utilisez Terraform pour définir un rôle IAM qui permet aux utilisateurs d'un compte d'assumer un rôle dans un deuxième compte et d'y provisionner des instances AWS             
Configurez ensuite un provider AWS pour utiliser les informations d'identification AssumeRole et déployer une instance EC2 sur plusieurs comptes    

```bash
#########################################################
# Mise en place de la configuration IAM

## creation du fichier de credentials aws
vi ~/.aws/credentials
#--------------------------------------------------
[source]
aws_access_key_id=<ACCOUNT 1 KEY>
aws_secret_access_key=<ACCOUNT 1 SECRET KEY>

[destination]
aws_access_key_id=<ACCOUNT 2 KEY>
aws_secret_access_key=<ACCOUNT 2 SECRET KEY>
#--------------------------------------------------

## supprime la déclaration "potentielle" de credentials avant d'exécuter terraform pour que les valeurs prises en compte dans le fichier credentials précédent soient loadées
unset AWS_SESSION_TOKEN AWS_SECRET_ACCESS_KEY AWS_ACCESS_KEY_ID


## creation du fichier terraform
cd <nomProjet>
vi providers.tf
#--------------------------------------------------
provider "aws" {
  alias   = "source"
  profile = "source"
  region  = "us-east-2"
}

provider "aws" {
  alias   = "destination"
  profile = "destination"
  region  = "us-east-2"
}
#--------------------------------------------------


## appel des différents accounts avec le assumeRole pour donner les droits 
vi main.tf
#--------------------------------------------------
data "aws_caller_identity" "source" {
  provider = aws.source
}

data "aws_iam_policy_document" "assume_role" {
  provider = aws.destination
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.source.account_id}:root"]
    }
  }
}
#--------------------------------------------------

## mise en place de la référence à la policy donnant les droits dans le assumeRole
vi main.tf 
#--------------------------------------------------
data "aws_iam_policy" "ec2" {
  provider = aws.destination
  name     = "AmazonEC2FullAccess"
}

resource "aws_iam_role" "assume_role" {
  provider            = aws.destination
  name                = "assume_role"
  assume_role_policy  = data.aws_iam_policy_document.assume_role.json
  managed_policy_arns = [data.aws_iam_policy.ec2.arn]
}
#--------------------------------------------------


#########################################################
# Creation du role IAM 

terraform init
terraform apply

#########################################################
# Modification de la configuration des instances EC2   

vi main.tf
#--------------------------------------------------
provider "aws" {
  region  = "us-east-2"
  profile = "source"            ## ⚠️ c'est au niveau de l'alias source qu'on met l'arn en place

  assume_role {
    role_arn = "<ROLE_ARN>"   ## on remplace <ROLE_ARN> par l'arn du nouveau role créé dans l'étape précédente
  }
}
#--------------------------------------------------

## ajout de commantaires des credentials (pour simulation) de l'account "destination" 
vi ~/.aws/credentials
#--------------------------------------------------
[source]
aws_access_key_id=<ACCOUNT 1 KEY>
aws_secret_access_key=<ACCOUNT 1 SECRET KEY>

# [destination]
# aws_access_key_id=<ACCOUNT 2 KEY>
# aws_secret_access_key=<ACCOUNT 2 SECRET KEY>
#--------------------------------------------------

## exécution de terraform 
terraform init
terraform apply
```
 ➡️ en se loggant à la console AWS avec l'account "destination" on voit que la configuration des instances EC2 a été modifiée












+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - ASSUMING IAM ROLE USING A WEB IDENTITY** 
</span>

S'il dispose d'un role ARN et d'un token provenant d'un provider d'identité Web, le provider AWS tentera d'assumer ce rôle à l'aide des informations d'identification fournies.


```yaml
provider "aws" {
  assume_role_with_web_identity {
    role_arn                = "arn:aws:iam::123456789012:role/ROLE_NAME"
    session_name            = "SESSION_NAME"
    web_identity_token_file = "/Users/tf_user/secrets/web-identity-token"
  }
}
```



+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - USING EXTERNAL CREDENTIALS PROCESS** 
</span>


Pour utiliser un processus externe pour obtenir des informations d'identification, le processus doit être configuré dans un profil nommé, y compris le profil par défaut. Le profil est configuré dans un fichier de configuration partagé


```yaml
provider "aws" {
  profile = "customprofile"
}
```

```bash
vi ~/.aws/credentials
#--------------------------------------------------
[profile customprofile]
credential_process = custom-process --username jdoe
#--------------------------------------------------
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

<span style="color: #965DCE">

#####  **TF FUNDAMENTALS - PROVIDERS - AUTH - BLOCK ASSUME ROLE** 
</span>


Le bloc de configuration assume_role prend en charge les arguments suivants :        
- `duration`: Durée de la session de prise de rôle (ex: `1h`, `2h45`, `30m15s`)        

- `external_id`: Identifiant externe à utiliser lors de l'acceptation du rôle.    

- `policy`: Politique IAM JSON décrivant des autorisations restrictives supplémentaires pour le rôle IAM assumé   

- `policy_arns`: Ensemble de noms de ressources Amazon (ARN) de stratégies IAM décrivant des autorisations restrictives supplémentaires pour le rôle IAM assumé   

- `role_arn`: **OBLIGATOIRE**  - ARN du rôle IAM à assumer    

- `session_name`: Nom de session à utiliser lors de l'acceptation du rôle    

- `source_identity`: Identité source spécifiée par le principal assumant le rôle      

- `tags`: Carte des balises de session d'assumer le rôle          

- `transitive_tag_keys`: Ensemble de clés de balise de session de rôle à transmettre à toutes les sessions suivantes         





+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    

.......................................................................................................................    

=======================================================================================================================  

  

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - WORKING WITH RESOURCES** 
</span>



.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - RESOURCES** 
</span>


Les **ressources** sont la partie la + importante du langage terraform     
Un **block de ressources** decrit les objets de l'infrastructure comme un reseau virtuel, les instances, les contenus comme les enregistrements DNS     



```bash
resource "aws_instance" "web" {
    instance_type   =   "t2.micro"
    ami             =   "ami-408c7f28"
}
```

Un **block ressource** à besoin d'un **type** (`"aws_instance"`) et d'un **nom** (`"web"`).    
Le type est fourni par la documentation terraform, le nom est à définir (il doit être unique) et sera résutilisé dans le projet terraform à différents niveaux en cas de besoin   

Les **resources type arguments** comme `instance_type` ou `ami` sont définis dans la documentation terraform en fonction de son type parent (`"aws_instance"`)    




Les ressources:
- **providers**         ➡️ plugins terraform qui offre une collection de resources types   
- **arguments**         ➡️ sont specifiques en fonction de la resource type selectionnée    
- **documentation**     ➡️ decrit pour chaque provider ses resources types et ses arguments    

_exemple_: [Documentation terraform resource type: aws_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)    






.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - META ARGUMENTS** 
</span>

[Documentation terraform: meta arguments](https://developer.hashicorp.com/terraform/language/meta-arguments/module-providers)    

Ces arguments ne dépendents pas d'un resource type, ils peuvent être utilisés avec n'importe quelle resource type    

- **`depends_on`**        ➡️ specifie les dependances cachées nécessaires    
- **`count`**             ➡️ crée une multitude de resources instances selon un compte    
- **`for_each`**          ➡️ crée une boucle selon une map ou un tableau      
- **`provider`**          ➡️ spécifie quel provider utiliser pour une resource      
- **`lifecycle`**         ➡️ permet de configurer des cycles de vie de resources personalisées    
- **`provisioner`** et **`connection`**     ➡️ depuis la machine locale (généralement), fait des actions supplémentaires après la création de la resource     




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - OPERATION TIMEOUTS** 
</span>


Il existe certaines resources types qui autorise le block **timeouts** à l'intérieur de celle-ci.    
Il existe pour permettre d'attendre 1 certain temps avant d'affirmer que l'action est en erreur    


```bash
resource "aws_db_instance" "example" {
    # ....

    timeouts {
        create  =   "60m"
        delete  =   "2h"
    }
}
```

Le valeurs (au format string) qui sont acceptées: 
- **"60m"**     ➡️ minutes      
- **"10s"**     ➡️ secondes        
- **"2h"**      ➡️ heures    



.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - HOW CONFIGURATION IS APPLIED ?** 
</span>

Les 4 actions de configuration qui peuvent se produire:

1. **create**                   ➡️ créer une ressource mais qui n'est associée à aucune infrastructure     
2. **destroy**                  ➡️ detruit la ressource qui existe dans le state mais n'existe plus dans la configuration    
3. **update in-place**          ➡️ update une ressource qui a des arguments qui ont changés   
4. **destroy and re-create**    ➡️ detruit et recrée une ressource dont les arguments ont changés     




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - RESOURCE BEHAVIOR** 
</span>


[Documentation terraform: resource behavior](https://developer.hashicorp.com/terraform/language/resources/behavior)    

- **accessing resource attributes**
    - on peut accéder grace aux **expressions** dans un module aux ressources d'un même module. On peut utiliser ces informations pour configurer d'autres ressources    
    - informations en lecture seules obtenues par appel API    
    - les providers inclus des **data sources** qui sont des types utilisées seulement pour chercher des informations    

- **resource dependencies**    
    - la plupart des dépendances de ressources sont pris en compte automatiquement    
    - terraform analyse toutes les expressions dans un block de ressource pour trouver les references d'autres objets et traiter ces réferences dans l'ordre définit pendant la creation, update, ou destruction de ressources    
    - il n'est pas généralement nécessaire de spécifier manuellement les dépendances entre les ressources, cependant certainement ne sont pas reconnues si non fait     


- **local-only resources**    
    - ce sont des ressources particulières qui sont utiles à terraform lui-même: ssh keys, self-signed certs, random ids...








.......................................................................................................................    

=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - INPUT VARIABLES** 
</span>


Les **input variables** (variables d'entrées) sont comme des arguments de fonctions    

Elles servent de paramètres aux modules terraform.    
Elles permettent aux aspects du module d'être personnalisés sans altérer le module lui-même    
Elles permettent aux modules d'être partagées entre différentes configurations   




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - DECLARING AN INPUT VARIABLE** 
</span>


[Documentation terraform: input variables](https://developer.hashicorp.com/terraform/language/values/variables)    


Une input variable doit être déclarée avec un block de variables   


```bash
variable "image_id" {
    type    =   string
}

variable "availability_zone_names" {
    type    =   list(string)
    default =   ["us-west-1a"]
}

variable "docker_ports" {
    type    =   list(object({
        internal    =   number
        external    =   number
        protocol    =   string
    }))
    default =   [
        {
        internal    =   8300
        external    =   8300
        protocol    =   "tcp"
        }
    ]
}
```

- **variable block name**: ex: `"image_id"`, `"availability_zone_names"`, `"docker_ports"`    
    ➡️ nom unique   
    ➡️ ⚠️ ne doit pas être: **source**, **version**, **providers**, **count**, **for_each**, **lifecycle**, **depends_on**, **locals** ⚠️  



Les **arguments de ces variables** sont: 
- **default**
- **type**    
- **description**  
- **validation**  
- **sensitive**    


- Pour l'**argument type**, les contraintes qu'il attend:
    - **string**
    - **number**
    - **bool**


- Pour l'**argument type**, les constructors qu'il attend:
    - **list(<type>)**    
    - **set(<type>)**    
    - **map(<type>)**    
    - **objetc({<attribute> = <type>, ... })**    
    - **tuple([<type>, ...])**    


![tf_fundamentals-inputvariables_argumentexemples](./images/tf_fundamentals-inputvariables_argumentexemples.png)   
![tf_fundamentals-inputvariables_argumentexemples](./images/tf_fundamentals-inputvariables_argumentexemples02.png)   


.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - USING INPUT VARIABLE VALUES** 
</span>


la valeur d'une variable est accessible avec l'expression `var.<varName>`   


```bash
resource "aws_instance" "web" {
    instance_type   =   "t2.micro"
    ami             =   "var.image_id"
}
```

> _Remarque_: cette valeur n'est accessible qu'à l'intérieur d'un même module



.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - HOW TO ASSIGN VALUE TO ROOT MODULE ?** 
</span>


![tf_fundamentals-inputvariables_assignValueToRootModule](./images/tf_fundamentals-inputvariables_assignValueToRootModule.png)    
![tf_fundamentals-inputvariables_assignValueToRootModule](./images/tf_fundamentals-inputvariables_assignValueToRootModule02.png)   



.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - ENVIRONMENT VARIABLES** 
</span>

![tf_fundamentals-inputvariables_environementVariables](./images/tf_fundamentals-inputvariables_environementVariables.png)    





.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - VARIABLES PRIORITIES** 
</span>

![tf_fundamentals-inputvariables_variablesPriority](./images/tf_fundamentals-inputvariables_variablesPriority.png)    




.......................................................................................................................   

=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - DECLARING OUTPUT VARIABLES** 
</span>


[Documentation Terraform: output values](https://developer.hashicorp.com/terraform/language/values/outputs)    
[Documentation A Cloud Guru: how to use terraform inputs and outputs](https://acloudguru.com/blog/engineering/how-to-use-terraform-inputs-and-outputs)    



Une **variable output** est une variable dont sa valeur sera écrite en retour des actions terraform (dans le terminal)    
Elles peuvent être:
- un module enfant qui peut les utiliser pour exposer un sous-ensemble de essources d'un module parent    
- un root module qui peut les utiliser pour imprimer les valeurs dans la CLI    
- les outputs de root module peuvent accéder via d'autre configuration (`terraform_remote_state`) aux données sources    



Chaque valeur output doit être d'abord défini dans un block d'output   

```bash
output "instance_ip_addr" {
    value   =   aws_instance.server.private.ip
}
```

  ➡️ Le **nom label** après le **mot clé output** DOIT être un iditifiant valide (`"instance_ip_addr"`)   
  ➡️ La **valeur de l'argument** prend comme expression le résultat que le user attend en retour (`aws_instance.server.private.ip`)        

.......................................................................................................................    


<span style="color: #11E3EA">

####  **Arguments et contraintes d'output variables** 
</span>



Pour accéder aux outputs d'un module enfant dans un module parent, les outputs de modules enfant sont disponibles en tant qu'expressions de la forme `module.<modulename>.<outputName>`    


Les arguments possibles dans une déclaration sont:
- **`descrition`**    
- **`sensitive`**    
- **`depends_on`**    


_exemple pour **description**_:
```bash
output "instance_ip_addr" {
    value           =   aws_instance.server.private_ip
    desccription    =   "The private IP adress of the main server instance"
}
```

  ➡️ La desccription doit être claire et représentative de la valeur qui sera retournée


_exemple pour **sensitive**_:
```bash
# main.tf

module "foo" { 
    source      =   "./mod"
}

resource "test_instance" "x" {
    some_attribute  =   module.mod.a
}

output "out" {
    value       =   "xyz"
    sensitive   =   true
}

output "a" {
    value       =   "secret"
    sensitive   =   true    
}

```

  ➡️ Va retourner comme resultat:

```bash
# test_instance.x will be created
+ resource "test_instance" "x" {
    + some_attribute    =   (sensitive)
}

Plan: 1 to add, 0 to change, 0 to destroy.

Changes to Ouputs:
  + out = (sensitive value)
```



_exemple pour **depends_on**_:
```bash
output "instance_ip_addr" {
    value           =   aws_instance.server.private_ip
    desccription    =   "The private IP adress of the main server instance"

    depends_on = [
        # Security group rule must be created before this IP adress could
        # actually be used, otherwise the services will  be unreacheable
        aws_security_group_rule.local_access,
    ]
}
```

  ➡️ l'argument **depends_on** doit être utilisé en "dernier" recours. Il devrait toujours y avoir un commentaire expliquant pourquoi il est utilisé   




.......................................................................................................................    


=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - DECLARING LOCAL VARIABLES** 
</span>


[Documentation terraform: local variables](https://developer.hashicorp.com/terraform/language/values/locals)    

les **local values** sont comme de fonctions temporaires de **variables locales**    
Elles permettent de:    
- donner un nom à une expression    
- utiliser une varieble plusieurs fois dans un module sans la réperter    

Une variable locale doit être définie dans un **bloc locals**   

```bash
locals {
    service_name    =   "forum"
    owner           =   "Community team"
}
```

  ➡️ Un ensemble de valeurs locales liées peuvent être définies dans 1 même bloc    
  ➡️ Les expressions ne sont pas limitées. Elles peuvent faire référence à d'autres valeurs dans le module    


Lorsqu'une **valeur locale** (local value) est définie, on peut y faire référence avec l'expression `local.<name>`
    
Les **local values** ne sont accessibles que depuis les expressions DANS le module lorsqu'elles sont déclarées

```bash
resource "aws_instance" "exemple" {
    # ...

    tags    =   local.common_tags
}
```



=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - MODULES** 
</span>


[Documentation terraform: modules](https://developer.hashicorp.com/terraform/language/modules)    

**Un module est un container de ressources multiples utilisées ensemble**.    
Un module est un collection de fichiers `.tf` et/ou `.tf.json` dans un même répertoire    

Les modules sont le moyen principal de packager et reutiliser des configuration de ressources avec terraform    

**Root Module**:     
TOUS les projets terraform ont au moins 1 module ➡️ le **root module** qui consiste à avoir des resssources déinies dans un fichier `.tf` dans le répertoire principal du projet terraform    


**Child Modules**:    
Le root module peut appeler d'autes modules appelés **child modules**.   
Ces modules enfants comportent des ressources avec leurs configurations.    
Un child module plusieurs fois avec la même configuration et plusieurs configurations peuvent appeler le même child module    

**Modules Publiés**:    
En plus des modules créés depuis nos fichiers locaux, terraform peut charger des modules provenant d'un registry publique ou privé. Il est donc possible de publier des modules pour que d'autres les utilises    



....................................................................................................................... 


<span style="color: #11E3EA">

####  **Utilisation de modules** 
</span>


[Documentation terraform: module block syntax](https://developer.hashicorp.com/terraform/language/modules/syntax)     


**Appeler un child module**: 

```bash
module "servers" {
  source = "./app-cluster"

  servers = 5
}
```

  ➡️ un **root module** qui comprend un module block qui appelle un child module    
  ➡️ le label après le **module keyword** (`"servers"`) est un **local name** qui peut être utilisé pour référencer le module


**Les 4 types d'aguments de modules**:
- l'argument **source** est nécessaire pour tous les modules       
- la **version argument** qui est recommandé pour les modules depuis un registry (modules publiés)    
- La **input variable** arguments    
- les **meta argument** comme `for_each` et `depends_on`    


_exemple module depuis registry: version_:
```bash
module "consul" {
  source = "hashicorp/consul/aws"
  version = "0.1.0"

  servers = 3
}
```
  ➡️ le module provenant d'un registry est `source = "hashicorp/consul/aws"`  (registry hashicorp)    
  ➡️ sa version est `version = "0.1.0"`    
  ➡️ un meta argument est aussi utilisé ici (de type **count**): `servers = 3`  
    il pourrait y avoir comme type de meta arguments:
    - **`count`**    
    - **`for_each`**    
    - **`providers`**    
    - **`depends_on`**    




**Accessing Module Output Values**: 


 Les ressources définies dans 1 module sont encapsulée, le fait d'appeler un module ne donne pas un accès direct à celles-ci. Cependant un child module peut déclarer des **output values** qui permettent un export des valeurs de ces ressources, et celles-là sont accessible en appelant le module    



Par exemple le module **app-cluster** a une output value `instance_ids`qui lorsqu'elle est appelée fait référence à l'expression `module.servers.instance_ids`

```bash
module "servers" {
  source = "./app-cluster"

  servers = 5
}
```


```bash
resource "aws_elb" "example" {
  # ...

  instances = module.servers.instance_ids
}
```




**Transferring Resource State Into Modules**:

Eclater le code terraform en plusieurs child modules, ou déplacer des resouces block entre plusieurs modules permet un plus grande modularité.    
L'utilisation de la commande `terraform state mv` que le child module block a été déplacé dans un module différent    
  ➡️ les resources doivent alors être préfixées comme cela `module.<modulename>`    
  ➡️ si un module est appelé avec **count** ou **for_each** il doit être prefixé avec `module.<modulename>[index]`    




**Tainting Resources within a Module**:     
La commande `terraform taint` peut être utilisée pour taint des ressouces specifiques dans un module    
`terraform taint module.salt_master.aws_instance.salt_master`    

  ➡️ Il n'est pas possible de taint un module entier    
  ➡️ chaque resource d'un module que l'on souhaite taint doit être faite séparement    



  

....................................................................................................................... 

=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - MODULE SOURCES** 
</span>


[Documentation terraform: module sources](https://developer.hashicorp.com/terraform/language/modules/sources)    

Les **arguments sources** dans un **module block** disent à terraform où trouver le code source pour le child module souhaité    

Terraform utilise cela lors de la phase `terraform init` pour download le code source dans un préertoire sur le disque local, ainsi les commandes terraform peuvent l'utiliser    

Le module installer supportent l'installation depuis un un nombre de ources types différentes:
- **[local paths](https://developer.hashicorp.com/terraform/language/modules/sources#local-paths)**    
- **[terraform registry](https://developer.hashicorp.com/terraform/language/modules/sources#terraform-registry)**    
- **[Github](https://developer.hashicorp.com/terraform/language/modules/sources#github)**    
- **[bitbucket](https://developer.hashicorp.com/terraform/language/modules/sources#bitbucket)**    
- **[Git](https://developer.hashicorp.com/terraform/language/modules/sources#generic-git-repository)**    
- **[HTTP URLs](https://developer.hashicorp.com/terraform/language/modules/sources#http-urls)**    
- **[S3 Buckets](https://developer.hashicorp.com/terraform/language/modules/sources#s3-bucket)**    
- **[GCS buckets](https://developer.hashicorp.com/terraform/language/modules/sources#gcs-bucket)**     


- **[Module in Package Sub-directories](https://developer.hashicorp.com/terraform/language/modules/sources#modules-in-package-sub-directories)**    



.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: local path** 
</span>

Les références de local path permettent de factoriser des parties d'une configuration dans un référentiel source unique.    

```bash
module "consul" {
  source = "./consul"
}
```

Un local path DOIT commencer par `./` ou `../`

Un local path n'est pas "installé" comme les autres sources: les fichiers sont déjà présents en local et peuvent donc être utilisés directement. Leur code est automatiquement updaté si le parent module est upgradé   



.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: terraform registry** 
</span>


Un module registry terraform est la manière native de partagé des modules terraform.    
Ces registry utilsent des index.    

Pour utiliser un module publique il faut mentionner ceci: `<NAMESPACE>/<NAME>/<PROVIDER>` (au niveau de `source`)        

```bash
module "consul" {
  source = "hashicorp/consul/aws"
  version = "0.1.0"
}
```

Il est possible de devoir ajouter le champ `<HOSTNAME>/` dans le chemin de la source en fonction de l'arborescence du registry    
```bash
module "consul" {
  source = "app.terraform.io/example-corp/k8s-cluster/azurerm"
  version = "1.1.0"
}
```

>_Remarque_: si on utilise la SaaS version de terraform cloud, il s'agit d'un registry privé dont le hostname est `app.terraform.io`    








.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: github** 
</span>


Terraform reconnait l'url `github.com` et l'interprête en tant que ressource git.    
Idem pour les liens en ssh de github    

```bash
# url github.com
module "consul" {
  source = "github.com/hashicorp/example"
}


## lien ssh github
module "consul" {
  source = "git@github.com:hashicorp/example.git"
}
```






.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: bitbucket** 
</span>


Il fait de même pour l'url `bitbucket.com`    

```bash
module "consul" {
  source = "bitbucket.org/hashicorp/terraform-consul-aws"
}
```



.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: git** 
</span>


Les repo git doivent être définis en amont de cette manière avant de pouvori être utilisé     

```bash
## exemple git en https
module "vpc" {
  source = "git::https://example.com/vpc.git"
}

## exemple git en ssh
module "storage" {
  source = "git::ssh://username@example.com/storage.git"
}
```
  ➡️ il faut préfixer avec `git::`


Dans le cas d'un repo git, il sera peut nécessaire d'avoir les identifiants si ce repo est privé    


.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: HTTP URLs** 
</span>


Lorsqu'on utilise une url HTTP ou HTTPS, terraform va faire une requête `GET`    
Cela va renvoyer une redirection.    

Terraform va faire un **append** avec l'argument `terraform-get=1` à la requête qui donne l'URL avant la réponse du GET.   
Si le retour est en `200`, terraform ira chercher dans les répertoires de l'url avec dans son header `X-Terraform-Get`    
Dans la reponse HTML un meta element avec le nom `terraform-get` sera présent: `<meta name="terraform-get" content="github.com/hashicorp/example" />`    


**Pour récupérer des archives depuis HTTP**: 

```bash
module "vpc" {
  source = "https://example.com/vpc-module.zip"
}
```
  ➡️ on peut récupérer les archives dans les formats **zip**, **tar.bz2**, **tar.gz**, **tar.xz**    

Si le format de l'archive n'est pas défini dans le nom de l'archive il faut faire ceci    
```bash
module "vpc" {
  source = "https://example.com/vpc-module?archive=zip"
}
```






.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: S3 Bucket** 
</span>


Dans le cas où nous stockons sur un S3 Bucket il faut prefixer l'url avec `s3::`   

```bash
module "consul" {
  source = "s3::https://s3-eu-west-1.amazonaws.com/examplecorp-terraform-modules/vpc.zip"
}
```

Dans ce cas il faut avoir pour se connecter au S3 Bucket ses identifiants: **AWS_ACCESS_KEY_ID** et un **AWS_SECRET_ACCESS_KEY**; OU `.aws/credential` dans son environnement






.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - Modules Sources: GCS Bucket** 
</span>


Comme pour aws, il faut prefixer avec `gcs::`

```bash
module "consul" {
  source = "gcs::https://www.googleapis.com/storage/v1/modules/foomodule.zip"
}
```

Là aussi il faut avoir les identifiants: **GOOGLE_OAUTH_ACCESS_TOKEN**, **GOOGLE_APPLICATION_CREDENTIALS**



.......................................................................................................................  

=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - USING EXPRESSIONS AND FUNCTIONS** 
</span>


[Documentation terraform: expressions](https://developer.hashicorp.com/terraform/language/expressions)    
[Documentation terraform lab: create a dynamic expression](https://developer.hashicorp.com/terraform/tutorials/configuration-language/expressions?utm_source=WEBSITE&utm_medium=WEB_IO&utm_offer=ARTICLE_PAGE&utm_content=DOCS)    


.....................................................................................................      
[Documentation terraform: terraform CLI](https://developer.hashicorp.com/terraform/cli)        
[Documentation terraform: terraform cli - console](https://developer.hashicorp.com/terraform/cli/commands/console)    
.....................................................................................................      



Terraform configuration language support des expressions complexes qui autorisent à calculer ou à générer des valeurs pour votre configuration d'infrastructure    

Une expression peut être un simple string/integer ou des valeurs complexes qui peuvent être créées dynamiquement    

La valeur d'expression la plus simple est par exemple `"hello"` ou `2`.     

Les expressions peuvent être utilisées à plusieurs endroits dans le langage Terraform, mais certains contextes limitent les constructions d'expression autorisées, telles que l'exigence d'une valeur littérale d'un type particulier ou l'interdiction des références aux attributs de ressource.



![tfFundamentals_DiffbetweenExpressionAndFunctions](./images/tfFundamentals_DiffbetweenExpressionAndFunctions.png)    


.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Types and Values** 
</span>

Ils documentent les types de données que les expressions Terraform peuvent résoudre et les syntaxes littérales pour les valeurs de ces types    


**Le resultat d'une expression est une valeur**    

Toutes les values ont un type qui dicte vers quoi cette valeur peut-être utilisée et quelles transformations peuvent lui être appliquées    

**Les différents types de value** : 
- **string**                ➡️ chaine de caractere          ➡️ `"hello"`    
- **number**                ➡️ valeur numérique             ➡️ `15`; `3.234`    
- **bool**                  ➡️ valeur booléeene             ➡️ `true` / `false`    
- **list** (`tuple`)        ➡️ tableau de valeurs           ➡️ `["us-west1a", "us-east-1a"]`    
- **map** (`object`)        ➡️ clé/valeur (json)            ➡️ `{name = "Mabel", age = 52}`    
- **null**                  ➡️ nulle (absence ou omission)  ➡️  `null`    




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Strings and templates** 
</span>

Ils documentent les syntaxes des strings, y compris les séquences d'interpolation et les directives de modèle

Les **strings** sont les expressions les plus complexe mais aussi les plus utilisées.    

**Les sigles d'échapement**:    

| Sequence	| Replacement |
| :--------------- | -----:|
|`\n`	        | Newline |
|`\r`           | Carriage Return |
|`\t`	        | Tab |
|`\"`           | Literal quote (without terminating the string) |
|`\\`	        | Literal backslash |
|`\uNNNN`       | Unicode character from the basic multilingual plane (NNNN is four hex digits) |
|`\UNNNNNNNN`   | Unicode character from supplementary planes (NNNNNNNN is eight hex digits) |
| `$${`         | Literal ${, without beginning an interpolation sequence. |
| `%%{`         | Literal %{, without beginning a template directive sequence. |


**Heredoc** :    

```bash
<<EOT
hello
world
EOT

```

**Generating JSON ou YAML**:      

Pour générer un json ou un yaml, ne pas utiliser le "heredoc"   

```bash
  example = jsonencode({
    a = 1
    b = "hello"
  })
```

**Indented Heredocs** : 

Si nous ne souhaitons pas que chaque ligne commence par un espace, on utilise le "heredoc" (pour qui un espace EST un espace)  

```bash
## bloc heredoc SANS espace  ➡️ "<<EOF"
block {
  value = <<EOT
hello
world
EOT
}

## bloc heredoc AVEC espace ➡️ "<<-EOF"
block {
  value = <<-EOT
  hello
    world
  EOT
}
```


**Interpolation**:     

Lorsqu'on utilise `${}` on va interpreter une variable  (à utiliser lorsque c'est entre double quotes)           

```bash
"Hello, ${var.name}!"
```


**Directives** : 

Lorsqu'on utilise `%{}` on crée une directive (cas d'une condition).     
Cela s'écrit de la forme: `%{if <BOOL>}/%{else}/%{endif}`  

```bash
"Hello, %{ if var.name != "" }${var.name}%{ else }unnamed%{ endif }!"

## OU
<<EOT
%{ for ip in aws_instance.example.*.private_ip }
server ${ip}
%{ endfor }
EOT
```

**Whitespace Stripping** : 

Pour supprimer les espaces blancs on rajoute `~` comme marqueur    

```bash
<<EOT
%{ for ip in aws_instance.example.*.private_ip ~}
server ${ip}
%{ endfor ~}
EOT
```

➡️ va afficher par exemple    
```
server 10.1.16.154
server 10.1.16.1
server 10.1.16.34
```




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: References to Values** 
</span>

Ils documentent comment faire référence à des valeurs nommées telles que des variables et des attributs de ressource.

Ils existe différents types de **named values** : 
- **resources**    
- **input variables**    
- **local values**    
- **child module outputs**    
- **data sources**    
- **filesystem and workspace info**    
- **block-local values**     


**Resources**:   
`<RESSOURCE TYPE>.<NAME>`   

La valeur d'une ressource peut varier. Elle peut être `count` ou `for_each`    



**Input Variables** :     
`var.<NAME>`    

Ces variables ont des contraintes (vues précédemment)    


**Local Values** : 
`local.<NAME>`    

Ells font références à des valeurs locales qui sont definies dans des block `locals`    



**Child Module Outputs** :     
`module.<MODULE NAME>`    

Cela correspond à des blocks de modules     


**Data sources** :     
`data.<DATDATYPE>.<NAME>`    

C'est un objet représentant des data ressources et qui donne le type et le nom de la data source    
[Documentation terraform: data source](https://developer.hashicorp.com/terraform/language/data-sources)    

_exemple block data source_: 
```bash
data "aws_ami" "example" {
  most_recent = true

  owners = ["self"]
  tags = {
    Name   = "app-server"
    Tested = "true"
  }
}
```





.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Operators** 
</span>

Ils documentent les opérateurs arithmétiques, de comparaison et logiques

Il s'agit des opérateurs arithmétiques et logiques     

- `a + b`    
- `a - b`    
- `a * b`    
- `a / b`    
- `a % b`    
- `-a`  (multiplie "a" par "-1")    

- `a == b`   
- `a != b`    

- `a < b`    
- `a <= b`    
- `a > b`    
- `a >= b`     

- `a || b`    
- `a && b`    
- `!a`   




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Function calls** 
</span>

Ils documentent la syntaxe pour appeler les fonctions intégrées de Terraform.

Les fonctions peuvent être utilisées dans les expressions pour transformer et combiner des valeurs   
`<FUNCTION NAME>(<ARGUMENT 1>, <ARGUMENT 2>)`    

_exemple de fonction: min_: `min(55, 3453, 2)`   ➡️ renvoie la valeur la plus petite    


![tfFundamentals_FunctionCalls](./images/tfFundamentals_FunctionCalls.png)    
![tfFundamentals_FunctionCalls](./images/tfFundamentals_FunctionCalls02.png)    

.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Conditionnal Expressions** 
</span>


[Documentation terraform - Conditionnal Expressions](https://developer.hashicorp.com/terraform/language/expressions/conditionals)     

Ils documentent le `<CONDITION> ? <TRUE VAL> : <FALSE VAL>` expression, qui choisit entre deux valeurs en fonction d'une condition booléenne.

- `<CONDITION>`         ➡️ le **IF** de la condition    
- `?`                   ➡️ le **THEN** de la condition    
- `:`                   ➡️ le **ELSE** de la condition    

`condition ? true_val : false_val`  ➡️ SI `condition` est **true** alors le resultat est `true_val` SINON le résultat est `false_val`


Une expression conditionnelle commune est de définir un resultat par edfaut pour remplacer les valeurs invalides:       
`var.a != "" ? var.a : "default-a"` ➡️ SI `var.a` est un string vide ALORS la valeur de `default-a` remplacera la valeur actuelle de `var.a`           










.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: For Expressions** 
</span>


[Documentation terraform - For expression](https://developer.hashicorp.com/terraform/language/expressions/for)      

Ils documentent des expressions comme `[for s in var.list : upper(s)]`, qui peuvent transformer une valeur de type complexe en une autre valeur de type complexe.

  ➡️ Cette expression "`for`" itère sur chaque élément de `var.list`, puis évalue l'expression "`upper(s)`" avec "s" défini sur chaque élément respectif. Il construit ensuite une nouvelle valeur de tuple avec tous les résultats de l'exécution de cette expression dans le même ordre


**Input Types** :     
`[for k, v in var.map : length(k) + length(v)]`    
  ➡️ Pour une `map` ou un type d'objet, comme ci-dessus, le symbole `k` fait référence à la clé ou au nom d'attribut de l'élément actuel.    




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Splat Expressions** 
</span>


[Documentation terraform - splat expressions](https://developer.hashicorp.com/terraform/language/expressions/splat)         

Ils documentent des expressions comme `var.list[*].id`, qui peuvent extraire des collections plus simples à partir d'expressions plus compliquées.








.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Dynamic Blocks** 
</span>

[Documentation terraform - Dynamic blocks](https://developer.hashicorp.com/terraform/language/expressions/dynamic-blocks)        


Ils documentent un moyen de créer plusieurs blocs imbriqués reproductibles dans une ressource ou une autre construction


Un `dynamic block` peut se trouver dans les blocs `resource`, `data`, `provider` et `provisioner`     
Il ressemble à un `for`: il fait une boucle mais fournit un block imbriqué plutôt que des valeurs complexes

Un `dynamic block` ne peut générer que des arguments appartenant au type de `resource`, à la `data`, au `provider` ou au `provisioner` en cours de configuration           
Il n'est pas possible de générer des blocs de méta-arguments tels que des `lifecycle` et de `provisioner`, car Terraform doit les traiter avant de pouvoir évaluer les expressions en toute sécurité         



Ces **dynamic blocks** permettent d'avoir un code simplifié   
_exemple_ 

```bash
###############################################################
## cas SANS dynamic block

resource "azurerm_virtual_network" "dynamic_block" {
  name                = "vnet-dynamicblock-example-centralus"
  resource_group_name = azurerm_resource_group.dynamic_block.name
  location            = azurerm_resource_group.dynamic_block.location
  address_space       = ["10.10.0.0/16"]

  subnet {
    name           = "snet1"
    address_prefix = "10.10.1.0/24"
  }

  subnet {
    name           = "snet2"
    address_prefix = "10.10.2.0/24"
  }

  subnet {
    name           = "snet3"
    address_prefix = "10.10.3.0/24"
  }

  subnet {
    name           = "snet4"
    address_prefix = "10.10.4.0/24"
  }
}

###############################################################
## cas AVEC dynamic block

variable "subnets" {
  description = "list of values to assign to subnets"
  type = list(object({
    name           = string
    address_prefix = string
  }))
}

subnets = [
  { name = "snet1", address_prefix = "10.10.1.0/24" },
  { name = "snet2", address_prefix = "10.10.2.0/24" },
  { name = "snet3", address_prefix = "10.10.3.0/24" },
  { name = "snet4", address_prefix = "10.10.4.0/24" }
]

resource "azurerm_virtual_network" "dynamic_block" {
  name                = "vnet-dynamicblock-example-centralus"
  resource_group_name = azurerm_resource_group.dynamic_block.name
  location            = azurerm_resource_group.dynamic_block.location
  address_space       = ["10.10.0.0/16"]

  dynamic "subnet" {
    for_each = var.subnets
    iterator = item   #optional
    content {
      name           = item.value.name
      address_prefix = item.value.address_prefix
    }
  }
}
###############################################################
```


Un dynamic block peut contenir les 4 arguments suivants: 
- **`for_each`**: **OBLIGATOIRE** ➡️ fournit la valeur complexe sur laquelle parcourir          
- **`iterator`**: _optionnel_  ➡️ définit le nom d'une variable temporaire qui représente l'élément actuel de la valeur complexe (si omis, le nom de la variable sera celle du block: dans l'exemple ci-dessus **subnet** ➡️ `name = subnet.value.name` dans la partie `content`)               
- **`labels`**: _optionnel_  ➡️ est une list of strings qui spécifie les blocs labels, dans l'ordre, à utiliser pour chaque bloc généré. Il est possible d'utiliser la variable temporaire `iterator` pour cette valeur       
> Spécifie le type de bloc imbriqué à générer. Dans l'exemple ci-dessus, le label est `subnet`. Une ressource subnet sera générée pour chaque élément de la variable `var.subnets` 
- **`content`**: **OBLIGATOIRE** ➡️ bloc imbriqué qui définit le corps de chaque bloc généré. Il est possible d'utiliser la variable temporaire `iterator` pour cette valeur        





Puisque l'argument `for_each` accepte n'importe quelle collection, vous pouvez utiliser une `expression for` ou une `expression splat` pour transformer une collection existante           

L'objet itérateur (`subnet` dans l'exemple ci-dessus) a 2 attributs :               
- **`key`**:  map key (map➡️ key:value) ou l'index de la liste pour l'élément actuel. Si l'expression `for_each` produit une valeur définie, alors la `key` est identique à la `value` et ne doit pas être utilisée.
- **`value`** est la valeur de l’élément actuel.

>_Remarque_: Dans l'exemple ci-dessus la variable `subnets` est de type `map` ➡️ voilà pourquoi on défini dans le dynamic bloc `value.name` dans le cas où on souhaite avoir la valeur de la clé `name`



La valeur du `for_each` doit être une collection avec un élément par bloc imbriqué souhaité           
Si vous devez déclarer des `resource` basées sur une `data` imbriquée ou des combinaisons d'éléments de plusieurs `data`, vous pouvez utiliser des expressions et des fonctions Terraform pour dériver une valeur appropriée              
Pour quelques exemples courants de telles situations, consultez les fonctions [`flatten`](https://developer.hashicorp.com/terraform/language/functions/flatten) et [`setproduct`](https://developer.hashicorp.com/terraform/language/functions/setproduct).





Certains providers définissent des types de ressources qui incluent plusieurs niveaux de blocs imbriqués les uns dans les autres       
Vous pouvez générer ces structures imbriquées dynamiquement si nécessaire en imbriquant des `dynamic block` dans la partie `content` d'autres `dynamic block`                   


```bash
variable "load_balancer_origin_groups" {
  type = map(object({
    origins = set(object({
      hostname = string
    }))
  }))
}


  dynamic "origin_group" {
    for_each = var.load_balancer_origin_groups
    content {
      name = origin_group.key

      dynamic "origin" {
        for_each = origin_group.value.origins
        content {
          hostname = origin.value.hostname
        }
      }
    }
  }

```


Lorsque vous utilisez des `dynamic block` imbriqués, il est particulièrement important de prêter attention au symbole de l'itérateur pour chaque bloc         

Dans l'exemple ci-dessus, `origin_group.value` fait référence à l'élément actuel du bloc externe, tandis que `origin.value` fait référence à l'élément actuel du bloc interne

Si un type de ressource particulier définit des blocs imbriqués qui ont le même nom de type que l'un de leurs parents, vous pouvez utiliser l'argument `iteraor` dans chacun des `dynamic block` pour choisir un symbole d'itérateur différent qui facilite la distinction entre les 2.



.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Types and Constraints** 
</span>

Ils documentent la syntaxe pour faire référence à un type, plutôt qu'à une valeur de ce type. Les variables d'entrée attendent cette syntaxe dans leur argument de type




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - EXPRESSIONS: Version Constraints** 
</span>


Ils documentent la syntaxe des chaînes spéciales qui définissent un ensemble de versions logicielles autorisées. Terraform utilise des contraintes de version à plusieurs endroits





.......................................................................................................................    

=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - BACKEND CONFIGURATION** 
</span>

[Documentation terraform: backend configuration](https://developer.hashicorp.com/terraform/language/settings/backends/configuration)    


Chaque configuration terrafform spécifie un backend.    
- hashicorp recommande aux utilisateurs débutant de terraform d'utilser un **local backend**    
- dans une équipe, travailler avec un **remote backend**    

Un backend définit où seront effectuer les opétarions terraform et où seront stockés les snapshots de state      


**D'où viennent les backends?** 
  ➡️ Terraform comprend une sélection intégrée de backends et ils se sont QUE des backends.   
  ➡️ Il n'est pas possible de charger d'autres backends comme des plugins    

**Où sont-ils utilisés?**    
  ➡️ La configuration backend n'est utilisée que par terraform cli. 
  ➡️ terraform cloud et entreprise utilisent leur propre storage de state   

**Que font ces backends?**    
  ➡️ Il y a 2 comportements qui sont déterminés par le backend:  
    - où les states sont stockés
    - où les opérations sont réalisées   


**A quoi ressemble un block backend ?**    
```bash
terraform {
    backend "remote" {
        organization    =   "corp_example"

        workspaces {
            name        =   "ex-app-prod"
        }
    }
}
```

Ces blocks de backends définissent où et comment sont configurés les backends    



**Limitations du backend block**:    
  ➡️ 1 configuration peut fournir SEULEMENT 1 backend block       
  ➡️ 1 backend block NE PEUT PAS référer à des named velues (input variables, locals....)    


**A savoir sur les backends terraform**: 
  1. Si la configuration d'un backend change  ➡️ relancer le `terraform init`    
  2. Quand le backend change, Terraform donne la possibilité de migrer les states    
  3. Hashicorp recommande de faire un backup manuel des states en copiant le fichier `terraform.tfstate`   




.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - BACKEND: Local backend** 
</span>


```bash
terraform {
    backend "local" {
        path = "/path/to/terraform.tfstate"
    }
}
```

Il a besoin de 2 variables de configuration:     
  - **path**                ➡️ le chemin du fichier `terraform.tfstate`        
  - **workspace_dir**       ➡️ le chemin du workspace non-defaut    


Les data sources de configuration d'un local backend:    
```bash
data "terraform_remote_state" "zland" {
    backend  = "local"

    config   = {
        path = "${path.module}/../../terraform.state"
    }
}
```


.......................................................................................................................    

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - BACKEND: Remote backend** 
</span>

Les remote backends conservent le terraform.tfsate et réalisent les opérations terraform


```bash
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "example_corp"

    workspaces {
      name = "my-app-prod"
    }
  }
}
```

Un remote backend peut travailler avec plusieurs workspaces. Ici il est mentionné "my-app-prod" mais il pourrait aussi y avoir par exemple "my-app-dev"    

  ➡️ Cela peut aussi être présenté de cette manière : 

```bash
# main.tf
#-----------------------------------------------------
terraform {
    required_version    = "~> 0.12.0"

    backend "remote" {}
}
#-----------------------------------------------------


## le fichier main.tf va chercher la configuration du backend qui est dans le fichier backend.hcl  

# backend.hcl
#-----------------------------------------------------
workspaces { name = "workspace" }
hostname        = "app.terraform.io"
organization    = "example_corp"
#-----------------------------------------------------
```

Il est aussi possible d'utiliser la CLI pour définir le backend utilisé. Pour cela on note dans la commande le chemin vers le fichier de backend souhaité    

`terraform init -backend-config=backend.hcl`    





  ➡️ avec un fichier pour les data sources: 

```bash
data "terraform_remote_state" "foo" {
    backend =  "remote"  

    config  = {
      organization  = "example_corp"

      workspaces  = {
        name      = "workspace"
      }
    }
}
```

avec comme variables de configuration: 
  - **hostname** (optionnel)       
  - **organization**  (obligatoire)      
  - **token** (optionnel)    ➡️ il est recommandé de ne pas le mettre par securité dans ce fichier     
  - **workspaces** (obligatoire)  ➡️ avec son **name** (optionel) et/ou son **prefix** (optionnel) dans le cas de multiples workspaces     

.......................................................................................................................  

<span style="color: #11E3EA">

####  **TF FUNDAMENTALS - BACKEND: Autres backends** 
</span>


![tfFundamentals_backends_S3Bucket](./images/tfFundamentals_backends_S3Buckets.png)    
![tfFundamentals_backends_Azurem01](./images/tfFundamentals_backends_Azurem01.png)    
![tfFundamentals_backends_Azurem02](./images/tfFundamentals_backends_Azurem02.png)    






.......................................................................................................................    


=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - WORKING WITH TEMPLATES** 
</span>


L'utilisation de templates avec Terraform se fait de plusieurs manières:      
- utilisation de fichiers templates     
- utilisation de la fonction `templatefile`       


Ces templates peuvent être utilisés pour la création d'arguments "dynamiques"     

[Documentation terraform - templatefile function](https://developer.hashicorp.com/terraform/language/functions/templatefile)            
[Documentation terraform - template_file](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file)      
[Documentation terraform - template directory module](https://registry.terraform.io/modules/hashicorp/dir/template/latest)      
[Documentation terraform - module-template](https://registry.terraform.io/modules/jakoberpf/template/module/latest)         
[Documentation spacelift - terraform templates](https://spacelift.io/blog/terraform-templates)               
[Tuto dev.to - terraform uses templates](https://dev.to/gdenn/how-to-use-templates-in-terraform-14ni)      
[Tuto KodeKloud - terraform templates](https://kodekloud.com/blog/terraform-template/)       
[Tuto Jhooq.com - terraform templates](https://jhooq.com/terraform-template/)     
[Tuto devopsschool.com - terraform templates](https://www.devopsschool.com/blog/templating-in-terraform-using-template_file-with-sample-program/)           



.......................................................................................................................       

<span style="color: #11E3EA">

####  **TERRAFORM - TEMPLATES - templatefile FUNCTION** 
</span>

`templatefile` lit le fichier au chemin indiqué et restitue son contenu sous forme de template à l'aide d'un ensemble fourni de variables de modèle           
syntaxe: `templatefile(path, vars)`          


La syntaxe du template est la même que celle des _[string templates](https://developer.hashicorp.com/terraform/language/expressions/strings#string-templates)_ dans le langage Terraform, y compris les séquences d'interpolation délimitées par `${ ... }`. Cette fonction permet simplement de prendre en compte des séquences de templates plus longues dans un fichier séparé pour plus de lisibilité.

**Les argument _vars_ DOIVENT être des objets**.       

**Dans 1 fichier template chaque clé de la map est une variable disponible pour l'interpolation**         

Un template peut  utiliser toute autre fonction disponible dans Terraform, sauf que les appels récursifs au `templatefile` ne sont pas autorisés.

Les noms de variables doivent chacun commencer par une lettre, suivie de zéro ou plusieurs lettres, chiffres ou underscore

Cette fonction ne peut être utilisée qu'avec les fichiers qui existent déjà sur le disque au début d'une exécution Terraform. Les fonctions ne participent pas au graphe de dépendances, cette fonction ne peut donc pas être utilisée avec des fichiers générés dynamiquement lors d'une opération Terraform    

`*.tftpl` est l'extension recommandée à utiliser pour les fichiers templates           



_exemples de templates_:           

```bash
############################################################################################################
## CAS DE LIST
############################################################################################################
vi backends.tftpl 
#---------------------------------------
%{ for addr, port in ip_addrs ~}
backend ${addr}:${port}
%{ endfor ~}
#---------------------------------------

## exécution de la fontion templatefile dans la cli terraform
templatefile("${path.module}/backends.tftpl", { port = 8080, ip_addrs = ["10.0.0.1", "10.0.0.2"] })
#---------------------------------------
backend 10.0.0.1:8080
backend 10.0.0.2:8080
#---------------------------------------
############################################################################################################

############################################################################################################
## CAS DE MAP
############################################################################################################
vi config.tftpl
#---------------------------------------
%{ for config_key, config_value in config }
set ${config_key} = ${config_value}
%{ endfor ~}
#---------------------------------------

## exécution de la fontion templatefile dans la cli terraform
templatefile(
               "${path.module}/config.tftpl",
               {
                 config = {
                   "x"   = "y"
                   "foo" = "bar"
                   "key" = "value"
                 }
               }
              )
#---------------------------------------
set foo = bar
set key = value
set x = y
#---------------------------------------
############################################################################################################
```


**Génération de json ou yaml depuis un template**       

Il est complexe de générer depuis un format string du json ou du yaml.        
Avec l'utilisation des templates la génération est simplifiée


```bash
############################################################################################################
## GENERATION DE JSON
############################################################################################################
${jsonencode({
  "backends": [for addr in ip_addrs : "${addr}:${port}"],
})}
############################################################################################################
## GENERATION DE YAML
############################################################################################################
${yamlencode({
  "backends": [for addr in ip_addrs : "${addr}:${port}"],
})}
############################################################################################################

## avec le même fichier template
vi backends.tftpl
#---------------------------------------
{"backends":["10.0.0.1:8080","10.0.0.2:8080"]}
#---------------------------------------

## le resultat est
#---------------------------------------
locals {
  backend_config_json = jsonencode({
    "backends": [for addr in ip_addrs : "${addr}:${port}"],
  })
}
#---------------------------------------
```







.......................................................................................................................       

<span style="color: #11E3EA">

####  **TERRAFORM - TEMPLATES - template_file** 
</span>

La data source `template_file` restitue un template à partir d'un template string, qui est généralement chargée à partir d'un fichier externe

_exemple_           
```bash
data "template_file" "init" {
  template = "${file("${path.module}/init.tpl")}"                     ## argument obligatoire
  vars = {      
    consul_address = "${aws_instance.consul.private_ip}"
  }
}
```




.......................................................................................................................       

<span style="color: #11E3EA">

####  **TERRAFORM - TEMPLATES - TEMPLATE DIRECTORY MODULE** 
</span>


Il s'agit d'un module Terraform réservé au calcul (c'est-à-dire un module qui n'effectue aucun appel d'API) qui rassemble tous les fichiers sous un répertoire de base particulier et restitue ceux qui ont un suffixe particulier en tant que fichier template Terraform

```bash
module "template_files" {
  source = "hashicorp/dir/template"

  base_dir = "${path.module}/src"
  template_vars = {
    # Pass in any values that you wish to use in your templates.
    vpc_id = "vpc-abc123"
  }
}
```

Les `files` output sont 1 map contruite à partir des chemins de fichiers relatifs au répertoire de base vers les objets avec les attributs suivants:          
- `content_type` : type MIME à utiliser pour le fichier             
- `content` : contenu du fichier après un rendu de template           
- `source_path` : localisation du filesysteme locald'un fichier non-template           
- `digests` : 1 map contenant les résultats d'application de plusieurs algo de hashage (`md5`, `sha1`, `sha256`...) sur le fichier content



Cas de l'utilisation de ce module pour l'uplaod de fichiers dans S3

```bash
resource "aws_s3_bucket_object" "static_files" {
  for_each = module.template_files.files

  bucket       = "example"
  key          = each.key
  content_type = each.value.content_type

  # The template_files module guarantees that only one of these two attributes
  # will be set for each file, depending on whether it is an in-memory template
  # rendering result or a static file on disk.
  source  = each.value.source_path
  content = each.value.content

  # Unless the bucket has encryption enabled, the ETag of each object is an
  # MD5 hash of that object.
  etag = each.value.digests.md5
}
```


.......................................................................................................................  












=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - WORKING WITH STATE** 
</span>


[Documentation terraform: state](https://developer.hashicorp.com/terraform/language/state)    
[Documentation terraform: cli manipulating state](https://developer.hashicorp.com/terraform/cli/state)    



Les **states** sont des exigences pour terraform pour appliquer les fonctions    
  - terraform nécessite des databases pour mapper la configuration de terraform    
  - terraform utilise sa propre structure de states pour mapper les configurations en resssources    

Les states peuvent être conservés en **local** ou à **distance** (pour un travail d'équipe ➡️ remote backend) dans un fichier `terraform.tfstate    `


**Que font les states ?**     
- Terraform traque les **metadatas** comme les dépendances des ressources. Pour assurer les opérations terraform conserve le set des dépendances le plus recent dans ses states    
- **Performance**: terraform conserve un cache des valeurs des attributs pour ses ressources dans ses states   
- **Synchronisation**: en travail collaboratif, travaiiler avec les states distants pour être les dernières versions synchronisées    






**Comment manipuler les states ?**    

![tfFundamentals-DiifConfigRemoteAndLocalBackendForState](./images/tfFundamentals-DiifConfigRemoteAndLocalBackendForState.png)      

![tfFundamentals_States](./images/tfFundamentals_States.png)    




**10 backends qui supportent plusieurs workspaces**    
- S3 
- AzureRM
- Consul
- COS
- GCS
- Kubernetes
- Local
- Manta
- Postgres
- Remote


**State CLI commands**    

![tFundamentals_States-CLI](./images/tFundamentals_States-CLI.png)    


[Documentation Terraform - commande state](https://developer.hashicorp.com/terraform/cli/commands/state)       

- `terraform state list`    ➡️ liste le contenu des states     
- `terraform state rm`      ➡️ supprime l'item des states     
- `terraform state show`    ➡️ affiche une ressource des states     




[Documentation Terraform - Terraform State](https://developer.hashicorp.com/terraform/language/state)            

Terraform doit stocker l'état de votre infrastructure et de votre configuration managées. Cet état (state) est utilisé par Terraform pour mapper les ressources du monde réel à votre configuration, suivre les métadonnées et améliorer les performances des grandes infrastructures       

Ce state est stocké par défaut dans un fichier local nommé `terraform.tfstate`, mais il est recommandé de le stocker dans Terraform Cloud pour le versionner, le chiffrer et le partager en toute sécurité avec votre équipe

Terraform utilise le state pour déterminer les modifications à apporter à votre infrastructure. Avant toute opération, Terraform effectue une actualisation pour mettre à jour le state avec l'infrastructure réelle

Bien que le format des fichiers tfstates soit uniquement JSON, la modification directe des fichiers d'état est déconseillée. Terraform fournit la commande `terraform state` pour effectuer des modifications de base de l'état à l'aide de la CLI.          
L'utilisation de la CLI a été créée pour pouvoir utiliser des outils linux tels que `grep`, `awk`...       

Terraform attend un mappage un-à-un entre les instances de ressources configurées et les objets distants. Cela est garanti par Terraform étant celui qui crée chaque objet et enregistre son identité dans l'état, ou qui détruit un objet puis supprime sa liaison.    

Si vous ajoutez ou supprimez des liaisons dans l'état par d'autres moyens, par exemple en important des objets créés en externe avec l'importation Terraform (`terraform import`), ou en demandant à Terraform d'"oublier" un objet existant avec `terraform state rm`, vous devrez alors vous assurer assurez-vous que cette règle individuelle est suivie, par exemple en supprimant manuellement un objet que vous avez demandé à Terraform d'"oublier", ou en le réimportant pour le lier à une autre instance de ressource.

La commande `terraform show` a une option -json pour inspecter intégralement le dernier state, ainsi que pour inspecter les fichiers de plan enregistrés qui incluent une copie de l'état précédent au moment où le plan a été créé

**Les states sont une condition nécessaire au fonctionnement de Terraform**         

**Terraform attend à ce que chaque objet distant soit lié à une seule instance de ressource dans la configuration**           
Lors de l'importation d'objets créés en dehors de Terraform, vous devez vous assurer que chaque objet distinct est importé dans une seule instance de ressource             
Outre les mappages entre les ressources et les objets distants, Terraform doit également suivre les métadonnées telles que les dépendances des ressources          
Pour garantir un fonctionnement correct, Terraform conserve une copie de l'ensemble de dépendances le plus récent au sein des states     
Pour une creation ou suppression dans le "bon ordre" (ex: 1 serveur doit être supprimer avant le subnet), terraform maitrise le fonctionnement des providers et cela est gérés par les states     


Les backends sont responsables du stockage des states et de fournir une API pour le verrouillage des states (**state locking**). Le verrouillage d’état est facultatif.     
Les comamndes `terraform console`, `terraform state`, `terraform taint` fonctionnent de la même manière que les states soient stockés en local ou en remote


Les backends déterminent où les states sont stockés. Par exemple, le backend local (par défaut) stocke le state dans un fichier JSON local sur le disque. Le backend Consul stocke le state dans Consul. Ces deux backends fournissent le verrouillage : local via les API système et Consul via les API de verrouillage.              
Lors de l'utilisation d'un backend non local, Terraform ne conservera le state nulle part sur le disque, sauf dans le cas d'une erreur irrécupérable où l'écriture du state sur le backend a échoué. Ce comportement constitue un avantage majeur pour les backends : si des valeurs sensibles se trouvent dans votre state, l'utilisation d'un backend distant vous permet d'utiliser Terraform sans que ce state ne soit jamais conservé sur le disque.              
Dans le cas d'une erreur persistant le state sur le backend, Terraform écrira le state localement. Ceci afin d'éviter la perte de données. Si cela se produit, l'utilisateur final doit transmettre manuellement le state au backend distant une fois l'erreur résolue.

Vous pouvez toujours récupérer manuellement le state du state distant à l'aide de la commande `terraform state pull`. Cela chargera votre state distant et l'affichera sur la sortie standard. Vous pouvez choisir de l'enregistrer dans un fichier ou d'effectuer toute autre opération.           
Vous pouvez également écrire manuellement le state avec `terraform state push`. **Ceci est extrêmement dangereux et doit être évité si possible. Cela écrasera l'état distant**. Cela peut être utilisé pour effectuer des réparations manuelles si nécessaire      

**Avant toute opération sur les tfstates il est recommandé de préparer un backup avec la commande `terraform state pull`**     


S'il est pris en charge par votre backend, Terraform verrouillera votre state pour toutes les opérations susceptibles d'écrire un state. Cela empêche d'autres personnes d'acquérir le verrou et de corrompre potentiellement votre state.     
Le state locking se produit automatiquement sur toutes les opérations susceptibles d'écrire un state. Vous ne verrez aucun message indiquant que cela se produit. Si le state locking échoue, Terraform ne continuera pas. Vous pouvez désactiver le state locking pour la plupart des commandes avec l'indicateur `-lock`, mais cela n'est pas recommandé.        


UTILISATION DES WORKSPACES         

Chaque configuration Terraform est associée à un backend qui définit comment Terraform exécute les opérations et où Terraform stocke les données persistantes, comme les states.           
Les données persistantes stockées dans le backend appartiennent à un workspace. Le backend ne dispose initialement que d'un seul workspace contenant un state Terraform associé à cette configuration. Certains backends prennent en charge plusieurs workspaces nommés, permettant d'associer plusieurs states à une seule configuration. La configuration ne comporte toujours qu'un seul backend, mais vous pouvez déployer plusieurs instances distinctes de cette configuration sans configurer un nouveau backend ni modifier les informations d'authentification.        

[Documentation Terraform - workspaces](https://developer.hashicorp.com/terraform/cloud-docs/workspaces)         
[Documentation Terraform - cli workspaces](https://developer.hashicorp.com/terraform/cli/workspaces)         




Les states Terraform peuvent contenir des données sensibles, en fonction des ressources utilisées et de votre définition de « sensible ». Le state contient les ID de ressource et tous les attributs de ressource. Pour les ressources telles que les bases de données, cela peut contenir des mots de passe initiaux.

Lors de l'utilisation du state local, le state est stocké dans des fichiers JSON en texte brut.

Lors de l'utilisation de remote state, le state n'est conservé en mémoire que lorsqu'il est utilisé par Terraform. Il peut être chiffré au repos, mais cela dépend du backend spécifique du remote state.





Liste de tuto pour comprendre l'utilisation des tfstates:           
[Tutorials terraform - state](https://developer.hashicorp.com/terraform/tutorials/state)       





[Documentation terraform - `terraform state`](https://developer.hashicorp.com/terraform/cli/commands/state)         
[Documentation Terraform - `terraform show`](https://developer.hashicorp.com/terraform/cli/commands/show)         





=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - MANAGING WORKSPACES** 
</span>


[Documentation terraform: managing workspaces](https://developer.hashicorp.com/terraform/cli/workspaces)     


Les **workspaces** sont des instances distinctes de données d'état (**state data**) qui peuvent être utilisées à partir du même répertoire de travail    

Les workspaces nous autorisent à utiliser une copie de notre configuration en tant que plugin ou module caches qui nous permettrons de conserver des states séparés pour chaque collection de ressources que nous gérons    

  ➡️ cela permet d'avoir la même configuration pour différents environnement comme dev/staging/production  
    ▶️ seule certaines valeurs sont différentes pour différentier ces environnements


![tfFundamentals_workspaceCommands](./images/tfFundamentals_workspaceCommands.png)    
![tfFundamentals_workspaceCommands02](./images/tfFundamentals_workspaceCommands02.png)    




=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - LAB1 INSTALLING TERRAFORM AND WORKING WITH TERRAFORM PROVIDERS** 
</span>


![fundamentals_enonce](./images/tfFundamentals_lab1/fundamentals_enonce.png)

aws account:     
username: cloud_user       
password: &8Ispuenacptcqo     
lien: https://837009848164.signin.aws.amazon.com/console?region=us-east-1     


cloud server public IP:     
username: cloud_user       
password: &R0Ys=o]4      
publicIp: 3.94.117.130         



![fundamentals_objectifs](./images/tfFundamentals_lab1/fundamentals_objectifs.png)


1. Log in to the lab server using the credentials provided:

`ssh cloud_user@<PublicIP>`

2. In a web browser, log in to the AWS Management Console using the credentials provided.

**Download And Manually Install the Terraform Binary**



1. Download the appropriate Terraform binary package for the provided lab server VM (Linux 64-bit) using the wget command:

`wget -c https://releases.hashicorp.com/terraform/0.13.4/terraform_0.13.4_linux_amd64.zip`

2. Unzip the downloaded file:

`unzip terraform_0.13.4_linux_amd64.zip`

3. Place the Terraform binary in the PATH of the VM operating system so the binary is accessible system-wide to all users:

`sudo mv terraform /usr/sbin/`

> Note: If prompted, enter the username and password provided for the lab server.

4. Check the Terraform version information:

`terraform version`   

Since the Terraform version is returned, you have validated that the Terraform binary is installed and working properly.



**Clone Over Code for Terraform Providers**    


1. Create a providers directory:

`mkdir providers`

2. Move into the providers directory:

`cd providers/`

3. Create the file main.tf:

`vim main.tf`

4. Paste in the following code from the provided GitHub repo:

```bash
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

provider "aws" {
  alias  = "us-west-2"
  region = "us-west-2"
}


resource "aws_sns_topic" "topic-us-east" {
  provider = aws.us-east-1
  name     = "topic-us-east"
}

resource "aws_sns_topic" "topic-us-west" {
  provider = aws.us-west-2
  name     = "topic-us-west"
}
```

5. To save and exit the file, press Escape and enter :wq.



**Deploy the Code with Terraform Apply**    

1. Enable verbose output logging for Terraform commands using `TF_LOG=TRACE`:

`export TF_LOG=TRACE`

> Note: You can turn off verbose logging at any time using the export TF_LOG= command.

2. Initialize the working directory where the code is located:

`terraform init`

3. Review the actions performed when you deploy the Terraform code:

`terraform plan`

> Note: Two resources will be created, consistent with the providers that were configured in the provided code snippet.

4. Deploy the code:

`terraform apply`

5. When prompted, type **yes** and press **Enter**.

6. Verify that two resources were created with their corresponding Amazon Resource Name (ARN) IDs in the region in which they were spun up.

7. Optionally, verify that the resources were created in their respective regions within the AWS Management Console:

- Navigate to the AWS Management Console in your browser.     
- Verify that you are logged in to the us-east-1 region upon signing in.     
- Click Services.     
- Type SNS in the search bar and select Simple Notification Service from the contextual menu.     
- In the menu on the left, click Topics.     
- Verify that the topic-us-east resource appears in the list.     
- At the top-right, click N. Virginia and select us-west-2.    
- Verify that the topic-us-west resource appears in the list.     

8. Tear down the infrastructure you just created before moving on:     

`terraform destroy --auto-approve`


[fichier logs lab1](./archives/lab1-tf_fundamentals.log)    






=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - LAB2 USING TERRAFORM CLI COMMANDS TO MANIPULATE A DEPLOYMENT** 
</span>


![tfFundamentals_lab2-enoncé](./images/tfFundamentals_lab2/tfFundamentals_lab2-enoncé.png)    

aws account:            
username: cloud_user      
password: #8Ksjdwnklqtqow     
lien: https://816403259377.signin.aws.amazon.com/console?region=us-east-1    


Cloud server terraform controller:     
username: cloud_user      
password: #*9vP[owH     
  terraform-controller: 18.206.46.204        


Ressources complémentaires: https://github.com/linuxacademy/content-hashicorp-certified-terraform-associate-foundations.git 

![tfFundamentals_lab2-objectifs](./images/tfFundamentals_lab2/tfFundamentals_lab2-objectifs.png)    



**Solution**    

1. Log in to the lab server using the credentials provided:

`ssh cloud_user@<Terraform-Controller>`

2. In a web browser, log in to the AWS Management Console using the credentials provided.


**Clone Terraform Code and Switch to the Proper Directory**    


1. Clone the required code from the provided repository:

`git clone https://github.com/linuxacademy/content-hashicorp-certified-terraform-associate-foundations.git`


2. Switch to the directory where the code is located:

`cd content-hashicorp-certified-terraform-associate-foundations/section4-lesson3/`

3. List the files in the directory:

`ls`

The files in the directory should include main.tf and network.tf. These files basically use the ${terraform.workspace} variable to create parallel environments and decide which region the deployment occurs in, depending on the workspace you're in.



**Create a New Workspace**      

1. Check that no workspace other than the **default** one currently exists:

`terraform workspace list`

The output should only show the default workspace. This workspace cannot be deleted.

> Note: When you use the terraform workspace list command to view the existing workspaces, the workspace which you are currently inside will be prepended with an asterisk (*) in front of the workspace name.

2. Create a new workspace named test:

`terraform workspace new test`

You will be automatically switched into the newly created **test** workspace upon successful completion. However, you can confirm this using the `terraform workspace list` command if you'd like.



**Deploy Infrastructure in the Test Workspace and Confirm Deployment via AWS**    

1. In the **test** workspace, initialize the working directory and download the required providers:

`terraform init`


2. View the contents of the main.tf file using the cat command:

```bash
cat main.tf
#----------------------------------------------
provider "aws" {
  region = terraform.workspace == "default" ? "us-east-1" : "us-west-2"
}

#Get Linux AMI ID using SSM Parameter endpoint in us-east-1
data "aws_ssm_parameter" "linuxAmi" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

#Create and bootstrap EC2 in us-east-1
resource "aws_instance" "ec2-vm" {
  ami                         = data.aws_ssm_parameter.linuxAmi.value
  instance_type               = "t3.micro"
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.subnet.id
  tags = {
    Name = "${terraform.workspace}-ec2"
  }
}
#----------------------------------------------
```


3. Note the configurations in the main.tf code, particularly:

- AWS is the selected provider.
- If the code is deployed on the default workspace, the resources will be deployed in the **us-east-1** region.
- If the code is deployed on any other workspace, the resources will be deployed in the **us-west-2** region.
- In the code creating the EC2 virtual machine, we have embedded the $terraform.workspace variable in the Name attribute, so we can easily distinguish those resources when they are created within their respective workspaces by their name: <workspace name>-ec2.


4. View the contents of the network.tf file:

```bash
cat network.tf
#----------------------------------------------
#Create VPC in us-east-1
resource "aws_vpc" "vpc_master" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "${terraform.workspace}-vpc"
  }

}

#Get all available AZ's in VPC for master region
data "aws_availability_zones" "azs" {
  state = "available"
}

#Create subnet # 1 in us-east-1
resource "aws_subnet" "subnet" {
  availability_zone = element(data.aws_availability_zones.azs.names, 0)
  vpc_id            = aws_vpc.vpc_master.id
  cidr_block        = "10.0.1.0/24"

  tags = {
    Name = "${terraform.workspace}-subnet"
  }
}


#Create SG for allowing TCP/22 from anywhere, THIS IS FOR TESTING ONLY
resource "aws_security_group" "sg" {
  name        = "${terraform.workspace}-sg"
  description = "Allow TCP/22"
  vpc_id      = aws_vpc.vpc_master.id
  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${terraform.workspace}-securitygroup"
  }
}
#----------------------------------------------
```

5. Note the configurations in the network.tf code, particularly:
- In the code creating the security group resource, we have embedded the $terraform.workspace variable in the Name attribute, so we can easily distinguish those resources when they are created within their respective workspaces by their name: <workspace name>-securitygroup.


6. Deploy the code in the test workspace:

`terraform apply --auto-approve`

> Note: The `--auto-approve` flag will prevent Terraform from prompting you to enter yes explicitly before it deploys the code.

7. Once the code has executed successfully, confirm that Terraform is tracking resources in this workspace:

`terraform state list`

There should be a number of resources being tracked, including the resources spun up by the code just deployed.

8. Switch over to the default workspace:

`terraform workspace select default`

9. Confirm that Terraform is currently not tracking any resources in this workspace, as nothing has been deployed:

`terraform state list`

The return output should say that No state file was found! for this workspace.

10. Verify that the deployment in the test workspace was successful by viewing the resources that were created in the AWS Management Console:

- Navigate to the AWS Management Console in your browser.
- Click on N. Virginia (the us-east-1 region) at the top-right to engage the Region drop-down, and select US West (Oregon), or us-west-2.
- Expand the Services drop-down and select EC2.
- On the Resources page, click Instances.
- Verify that the test-ec2 instance appears in the list.
- In the menu on the left, click Security Groups.
- Verify that the test-securitygroup resource appears in the list.


**Deploy Infrastructure in the Default Workspace and Confirm Deployment via AWS**    

1. Back in the CLI, verify that you are still within the default workspace:

`terraform workspace list`

Again, the asterisk (*) prepended to the name confirms you are in the default workspace.

2. Deploy the code again, this time in the default workspace:

`terraform apply --auto-approve`

3. Once the code has executed successfully, confirm that Terraform is now tracking resources in this workspace:

`terraform state list`

There should now be a number of resources being tracked, including the resources spun up by the code just deployed.

4. Verify that the deployment in the default workspace was successful by viewing the resources that were created in the AWS Management Console:

- Navigate to the AWS Management Console in your browser.
- Click on Oregon (the us-west-2 region) at the top-right to engage the Region drop-down, and select US East (N. Virginia), or us-east-1.
- As you are already on the Security Groups page, verify that the default-securitygroup resource appears in the list.
- In the menu on the left, click Instances.
- Verify that the default-ec2 instance appears in the list.



**Destroy Resources in the Test Workspace and Delete the Workspace**    

1. Back in the CLI, switch over to the test workspace:

`terraform workspace select test`

2. Tear down the infrastructure you just created in the test workspace:

`terraform destroy --auto-approve`

3. Verify that the resources were terminated in the AWS Management Console:

- Navigate to the AWS Management Console in your browser.
- Click on N. Virginia (the us-east-1 region) at the top-right to engage the Region drop-down, and select US West (Oregon), or us-west-2.
- As you are already on the Instances page, verify that the test-ec2 instance is shutting down or may have already been terminated.
- In the menu on the left, click Security Groups.
- Verify that the test-securitygroup resource no longer appears in the list.

> Note: It may take some time for the resources to be terminated in the AWS Management Console, and you may need to refresh the browser a few times to confirm that the resources have been destroyed.


4. Back in the CLI, switch over to the default workspace:

`terraform workspace select default`

5. Delete the test workspace:

`terraform workspace delete test`



[fichier log lab2](./archives/ab2-tf_Fundamentals.log)






=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - LAB3 BUILDING AND TESTING A BASIC TERRAFORM MODULE** 
</span>


![tf_lab3-enonce](./images/tfFundamentals_lab3/tf_lab3-enonce.png)     


aws account:     
username: cloud_user    
password: #1Xdvysoqdlmmlp    
lien: https://322593434164.signin.aws.amazon.com/console?region=us-east-1    

cloud server terraform controller    
username: cloud_user    
password: Racc5UC*     
terraform-controller: 44.213.134.225    
instant terminal: https://ssh.instantterminal.acloud.guru/?_ga=2.69570064.1046112847.1681884423-449307392.1681884423    


![tf_lab3-objectifs](./images/tfFundamentals_lab3/tf_lab3-objectifs.png)   

Des fichiers nous sont données pour réaliser ce lab   
[main.tf](./archives/tfFundamental-lab3/main.tf)    
[outputs.tf](./archives/tfFundamental-lab3/outputs.tf)  
 
[modules/vpc/main.tf](./archives/tfFundamental-lab3/modules-vpc-main.tf)   
[modules/vpc/variables.tf](./archives/tfFundamental-lab3/modules-vpc-variables.tf)   
[modules/vpc/outputs.tf](./archives/tfFundamental-lab3/modules-vpc-outputs.tf)  



**SOLUTION**

Log in to the lab server using the credentials provided:    
`ssh cloud_user@<Terraform-Controller>`


**Create the Directory Structure for the Terraform Project**    

1. Check the Terraform status using the version command:

`terraform version`    
Since the Terraform version is returned, you have validated that the Terraform binary is installed and functioning properly.

> Note: If you receive a notification that there is a newer version of Terraform available, you can ignore it — the lab will run safely with the version installed on the VM.

2. Create a new directory called terraform_project to house your Terraform code:

`mkdir terraform_project`

3. Switch to this main project directory:

`cd terraform_project`

4. Create a custom directory called modules and a directory inside it called vpc:

`mkdir -p modules/vpc`

5. Switch to the vpc directory using the absolute path:

`cd /home/cloud_user/terraform_project/modules/vpc/`


**Write Your Terraform VPC Module Code**    

1. Using Vim, create a new file called main.tf:

`vim main.tf`

2. In the file, insert and review the provided code:

```bash
provider "aws" {
  region = var.region
}

resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "this" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.1.0/24"
}

data "aws_ssm_parameter" "this" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}
```

3. Press Escape and enter :wq to save and exit the file.

4. Create a new file called variables.tf:

`vim variables.tf`

5. In the file, insert and review the provided code:

```bash
variable "region" {
  type    = string
  default = "us-east-1"
}
```

6. Press Escape and enter :wq to save and exit the file.

7. Create a new file called outputs.tf:

`vim outputs.tf`

8. In the file, insert and review the provided code:

```bash
output "subnet_id" {
  value = aws_subnet.this.id
}

output "ami_id" {
  value = data.aws_ssm_parameter.this.value
}
```

> Note: The code in outputs.tf is critical to exporting values to your main Terraform code, where you'll be referencing this module. Specifically, it returns the subnet and AMI IDs for your EC2 instance.

9. Press Escape and enter :wq to save and exit the file.



**Write Your Main Terraform Project Code**    

1. Switch to the main project directory:

`cd ~/terraform_project`

2. Create a new file called main.tf:

`vim main.tf`

3. In the file, insert and review the provided code:

```bash
variable "main_region" {
  type    = string
  default = "us-east-1"
}

provider "aws" {
  region = var.main_region
}

module "vpc" {
  source = "./modules/vpc"
  region = var.main_region
}

resource "aws_instance" "my-instance" {
  ami           = module.vpc.ami_id
  subnet_id     = module.vpc.subnet_id
  instance_type = "t2.micro"
}
```

> Note: The code in main.tf invokes the VPC module that you created earlier. Notice how you're referencing the code using the source option within the module block to let Terraform know where the module code resides.

4. Press Escape and enter :wq to save and exit the file.

5. Create a new file called outputs.tf:

`vim outputs.tf`

6. In the file, insert and review the provided code:

```bash
output "PrivateIP" {
  description = "Private IP of EC2 instance"
  value       = aws_instance.my-instance.private_ip
}
```

7. Press Escape and enter :wq to save and exit the file.



**Deploy Your Code and Test Out Your Module**   

1. Format the code in all of your files in preparation for deployment:

`terraform fmt -recursive`

2. Initialize the Terraform configuration to fetch any required providers and get the code being referenced in the module block:

`terraform init`

3. Validate the code to look for any errors in syntax, parameters, or attributes within Terraform resources that may prevent it from deploying correctly:

`terraform validate`

You should receive a notification that the configuration is valid.

4. Review the actions that will be performed when you deploy the Terraform code:

`terraform plan`

In this case, it will create 3 resources, which includes the EC2 instance configured in the root code and any resources configured in the module. If you scroll up and view the resources that will be created, any resource with module.vpc in the name will be created via the module code, such as module.vpc.aws_vpc.this.

5. Deploy the code:

`terraform apply --auto-approve`

> Note: The --auto-approve flag will prevent Terraform from prompting you to enter yes explicitly before it deploys the code.

6. Once the code has executed successfully, note in the output that 3 resources have been created and the private IP address of the EC2 instance is returned as was configured in the outputs.tf file in your main project code.

7. View all of the resources that Terraform has created and is now tracking in the state file:

`terraform state list`

The list of resources should include your EC2 instance, which was configured and created by the main Terraform code, and 3 resources with module.vpc in the name, which were configured and created via the module code.

8. Tear down the infrastructure you just created before moving on:

`terraform destroy`

9. When prompted, type yes and press Enter.













=======================================================================================================================    

<span style="color: #EA9811">

###  **TF FUNDAMENTALS - LAB4 EXPLORING TERRAFORM STATE FUNCTIONALITY** 
</span>


![tf_lab4-enonce](./images/tfFundamentals_lab4/tf_lab4-enonce.png)    

cloud server public ip     
username: cloud_user    
password: Rd3|DwAV    
public ip: 3.89.111.214
lien terminal: 
https://ssh.instantterminal.acloud.guru/?_ga=2.74958098.1046112847.1681884423-449307392.1681884423    


[source github supplémentaire](https://github.com/linuxacademy/content-hashicorp-certified-terraform-associate-foundations.git)    


![tf_lab4-objectifs](./images/tfFundamentals_lab4/tf_lab4-objectifs.png)  



**Solution**    

Log in to the lab server using the credentials provided:

`ssh cloud_user@<PublicIP>`  


**Check Terraform and Minikube Status**   

1. Check the Terraform status using the version command:

`terraform version`

Since the Terraform version is returned, you have validated that Terraform is installed and functioning properly.

> Note: If you receive a notification that there is a newer version of Terraform available, you can ignore it — the lab will run safely with the version installed on the VM.

2. Check the minikube status:

`minikube status`

The minikube status command should return host,kubelet, and apiserver in Running state and kubeconfig in Configured state. It will look something similar to this:

```bash
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```


**Clone Terraform Code**    

1. The Terraform code required for this lab has already been cloned onto the provided VM. Switch to the directory where the code is located:

```bash
cd lab_code/
cd section2-hol1/
```

2. View the code in the main.tf file:

`vim main.tf`

The code is configured with Kubernetes as the provider, allowing Terraform to interact with the Kubernetes API to create and destroy resources. Within the kubernetes_deployment resource, the replicas attribute controls the number of deployments, which in turn controls the number of pods being deployed.

3. Press Escape and enter :q! to exit the file.



**Deploy Terraform Code And Observe the State File**   

_Deploy the Cloned Terraform Code_

1. Initialize the working directory and download the required providers:

`terraform init`

2. Review the actions that will be performed when you deploy the Terraform code:

`terraform plan`

In this case, it will create 2 resources as configured in the Terraform code.

3. List the files in the directory:

`ls`

Notice that the list of files does not include the terraform.tfstate at this time. You must deploy the Terraform code for the state file to be created.

4. Deploy the code:

`terraform apply`

5. When prompted, type yes and press Enter.



_Observe How the Terraform State File Tracks Resources_


1. Once the code has executed successfully, list the files in the directory:

`ls`

Notice that the terraform.tfstate file is now listed. This state file tracks all the resources that Terraform has created.

2. Optionally, verify that the pods required were created by the code as configured using kubectl:

`kubectl get pods`

There are currently 2 pods in the deployment.

3. List all the resources being tracked by the Terraform state file using the terraform state command:

`terraform state list`

There are two resources being tracked: kubernetes_deployment.tf-k8s-deployment and kubernetes_service.tf-k8s-service.

4. View the replicas attribute being tracked by the Terraform state file using grep and the kubernetes_deployment.tf-k8s-deployment resource:

`terraform state show kubernetes_deployment.tf-k8s-deployment | egrep replicas`

There should be 2 replicas being tracked by the state file.

5. Open the main.tf file to edit it:

`vim main.tf`

6. Change the integer value for the replicas attribute from 2 to 4.

7. Press Escape and enter :wq to save and exit the file.

8. Review the actions that will be performed when you deploy the Terraform code:

`terraform plan`

In this case, 1 resource will change: the kubernetes_deployment.tf-k8s-deployment resource for which we have updated the replicas attribute in our Terraform code.

9. Deploy the code again:

`terraform apply`

10. When prompted, type yes and press Enter.

11. Optionally, verify that the pods required were created by the code as configured:

`kubectl get pods`

There are now 4 pods in the deployment.

12. View the replicas attribute being tracked by the Terraform state file again:

`terraform state show kubernetes_deployment.tf-k8s-deployment | egrep replicas`

There should now be 4 replicas being tracked by the Terraform state file. It is accurately tracking all changes being made to the Terraform code.


**Tear Down the Infrastructure**

1. Remove the infrastructure you just created:

`terraform destroy`

2. When prompted, type yes and press Enter.

3. List the files in the directory:

`ls`

Notice that Terraform leaves behind a backup file — terraform.tfstate.backup — in case you need to recover to the last deployed Terraform state.

[fichier logs](./archives/lab4_tf_fundamentals.log)    



=======================================================================================================================    

_______________________________________________________________________________________________________________________  




<span style="color: #CE5D6B">

##  **UTILISATION DE TERRAFORM** 
</span>



<span style="color: #EA9811">

###  **XAVKI** 
</span>

<span style="color: #11E3EA">

####  **OUTPUT** 
</span>

La fonction d'**output** au niveau terraform permet d'afficher un retour sur l'action souhaitée.  

[Archive du dossier](./archives/)  
[log du dossier](./fichiers_logs/0001_testOutput.log)  

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash 
##
(.venv) [mdali@dali ~]$terraform init
Terraform initialized in an empty directory!

The directory has no Terraform configuration files. You may begin working
with Terraform immediately by creating Terraform configuration files.

##
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
output "mavariable" {
  value = "Salut"
}
#------------------------------

##
(.venv) [mdali@dali ~]$terraform plan

Changes to Outputs:
  + mavariable = "Salut"

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.


Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run
"terraform apply" now.

##
(.venv) [mdali@dali ~]$terraform apply

Changes to Outputs:
  + mavariable = "Salut"

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes


Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

mavariable = "Salut"

##
(.venv) [mdali@dali ~]$ll
total 12K
74326103    0 drwxr-x--- 2 dali  78 Jan  9 13:04 .
34629625    0 drwxr-x--- 3 dali  29 Jan  9 09:07 ..
84533909 4.0K -rw-r----- 1 dali  83 Jan  9 13:04 main.tf
84533908 4.0K -rw-r----- 1 dali 328 Jan  9 13:04 terraform.tfstate
```
</span></div>


<div class="info" style='padding:0.1em; background-color:#068BE7; color:#EEB1F7'>
<span>

> _Remarque_: modification du fichier **main.tf** sans suppression des states générés précédemment

</span></div>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
##
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
output "mavariable" {
  value = "Salut"
}
output "variable2" {
  value = "toto"
}
#------------------------------

##
(.venv) [mdali@dali ~]$terraform plan

Changes to Outputs:
  + variable2 = "toto"

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.


Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run
"terraform apply" now.

##
(.venv) [mdali@dali ~]$terraform apply

Changes to Outputs:
  + variable2 = "toto"

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes


Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

mavariable = "Salut"
variable2 = "toto"

##
(.venv) [mdali@dali ~]$ll
total 12K
74326103    0 drwxr-x--- 2 dali  78 Jan  9 13:04 .
34629625    0 drwxr-x--- 3 dali  29 Jan  9 09:07 ..
84533909 4.0K -rw-r----- 1 dali  83 Jan  9 13:04 main.tf
84533908 4.0K -rw-r----- 1 dali 328 Jan  9 13:04 terraform.tfstate
84533908 4.0K -rw-r----- 1 dali 328 Jan  9 13:04 terraform.tfstate.backup
```

</span></div>


<span style="color: #11E3EA">

####  **VARIABLES ET LOCAL_EXEC** 
</span>

Le **local_exec** est un outil terraform qui permet de faire du terraform en local: ni sur une machine distante, ni sur un provider distant.  
Il est utilisé en prod pour récupérer des informations des machines qui sont par exemples sur le cloud. Cela permet de pouvoir compiler les datas.  


**Local_exec** est un **provisioner**.  



Le provisioner `local-exec` appelle un exécutable local après la création d'une ressource.    
Cela appelle un processus sur la machine exécutant Terraform, et non sur la ressource.     

Si nous n'avons pas de nouvelle ressource à créer MAIS que nous avons besoin d'exécuter une commande en local il est possible d'utiliser ce qui suit: 

```bash
## exécuter 1 commande en local (machine qui exécute terraform)
resource "null_resource" "command" {
  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]           ## dans le cas où nous exécutons une commande linux
    command = "<commande à exécuter"
  }
}
#----------------------------------------------------------------

## exécuter plusieurs commandes en local
resource "null_resource" "command" {
  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]           ## dans le cas où nous exécutons une commande linux
    command = <<-EOT
      <commande1>
      <commande2>
      <commandeN>
    EOT
  }
}
#----------------------------------------------------------------
```

>_Remarque_: Le même provisioner existe pour exécuter des commandes sur la machine cibe: `remote-exec`    


[Documentation spacelift - provisioner](https://spacelift.io/blog/terraform-provisioners)


>_Remarque_: Dans le cas où nous effectuons ce type de code dans 1 provider qui contient 1 assumeRole afin d'effectuer des actions sur 1 autre account avec d'autres droits que l'account qui exécute la pipeline avec le code terraform ➡️ le assumeRole n'est pas pris en compte ➡️ l'account utilisé par le provisioner est celui qui exécute la pipeline    


>_Remarque_: dans le cas de l'utilisation du provisioner `remote-exec` il faut aussi qu'une connexion soit configurée à ce remote [Documentation Terrafform - configure provisioner](https://developer.hashicorp.com/terraform/language/resources/provisioners/connection)   
> L'utilisation de ce provisioner n'est pas exactement identique [Tuto utilisation remote-exec provisioner](https://robertdebock.nl/learn-terraform/ADVANCED/remote-exec.html)



<span style="color: #965DCE">

#####  **VARIABLE TYPE STRING** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
variable "maVariableTypeString" {
  type = string
  default = "127.0.0.1.gitlab.test"     ## on definit comme valeur de la variable
}
resource "null_resource" "node1" {      ## comme on veut être en local on prend une ressource de type "null_ressource"
  provisioner "local-exec" {               ## et le provider "local_exec"
    command ="echo ${var.maVariableTypeString} > hosts.txt"   ## on exécute la commande
  }
}

output "maVariableTypeString" {         ## peu importe son nom
  value = var.maVariableTypeString      ## pour afficher le contenu de la variable ➡️ doit être celle défini plus haut
}
#------------------------------

(.venv) [mdali@dali ~]$ terraform init
(.venv) [mdali@dali ~]$ terraform plan
(.venv) [mdali@dali ~]$ terraform apply
#------------------------------
...
Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

Outputs:

maVariableTypeString = "127.0.0.1.gitlab.test"
#------------------------------

(.venv) [mdali@dali ~]$ ll
#------------------------------
...
84533915 4.0K -rw-r----- 1 dali   22 Jan  9 15:01 hosts.txt
#------------------------------

(.venv) [mdali@dali ~]$ cat hosts.txt
#------------------------------
127.0.0.1.gitlab.test
#------------------------------
```

</span></div>

[fichier de log local-exec variable type string](fichiers_logs/0002_localExecTypeString.log)  


<span style="color: #965DCE">

#####  **VARIABLE TYPE MAP** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
variable "hosts" {
  default = {           ## on déclare les valeurs de la variable de type map dans le default
    "127.0.0.1"       = "localhost gitlab.local"    ## les variables sont définies en clé/valeur
    "192.169.1.168"   = "gitlab.test"
    "192.169.1.170"   = "prometheus.test"
  }
}
resource "null_resource" "hosts" {    ## on a defini d'appeler la variable "hosts"
  for_each = var.hosts          ## la boucle for_each permet de parcourir la map. la boucle s'applique à la ressource et pas au provisioner (emplacement important dans le code)
  provisioner "local-exec" {
    command ="echo ${each.key} ${each.value} >> hosts.txt"
  }
}

output "hosts" {
  value = var.hosts
}
#------------------------------


(.venv) [mdali@dali ~]$ terraform init
(.venv) [mdali@dali ~]$ terraform plan
(.venv) [mdali@dali ~]$ terraform apply
#------------------------------
...
Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

Outputs:

hosts = {
  "127.0.0.1" = "localhost gitlab.local"
  "192.169.1.168" = "gitlab.test"
  "192.169.1.170" = "prometheus.test"
}
#------------------------------



(.venv) [mdali@dali ~p]$ll
total 16K
82205367    0 drwxr-x--- 3 dali  108 Jan 10 07:45 .
34629625    0 drwxr-x--- 6 dali  126 Jan 10 07:32 ..
82205376 4.0K -rw-r----- 1 dali   89 Jan 10 07:45 hosts.txt
82205368 4.0K -rw-r----- 1 dali  359 Jan 10 07:37 main.tf
 2270598    0 drwxr-x--- 3 dali   23 Jan 10 07:44 .terraform
82205369 4.0K -rw-r--r-- 1 dali 1.2K Jan 10 07:45 .terraform.lock.hcl
82205373 4.0K -rw-r----- 1 dali 1.4K Jan 10 07:45 terraform.tfstate


(.venv) [mdali@dali ~p]$cat hosts.txt
#------------------------------
192.169.1.170 prometheus.test
192.169.1.168 gitlab.test
127.0.0.1 localhost gitlab.local
#------------------------------
```

</span></div>


[fichier logs local-exec type map](fichiers_logs/0003_localExecTypeMap.log)




On ajoute un parametre **triggers** qui se place juste après le **for_each** qui va lui **vérifier le contenu et les valeurs du each** (valeur du triggers: `each.value`)  
Cela permet de valider les actions de terraform lorsqu'on change une valeur.  
Sans cela si on ne change qu'une valeur (pas une clé) terraform retournera qu'il n'a pas d'action à faire.

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
## fichier main.tf vu précédement (pour relancer le terraform)
#------------------------------

(.venv) [mdali@dali ~]$terraform init
(.venv) [mdali@dali ~]$terraform plan
(.venv) [mdali@dali ~]$terraform apply   # ressort les mêmes résultats que précédements

## modification de valeur seule
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
variable "hosts" {
  default = {
    "127.0.0.1"       = "localhost gitlab.local gitlab.me"    ## ajout de gitab.me en valeur
    "192.169.1.168"   = "gitlab.test"
    "192.169.1.170"   = "prometheus.test"
    "192.169.1.180"   = "grafana.test"
  }
}
resource "null_resource" "hosts" {
  for_each = var.hosts
  provisioner "local-exec" {
    command ="echo ${each.key} ${each.value} >> hosts.txt"
  }
}

output "hosts" {
  value = var.hosts
}
#------------------------------

(.venv) [mdali@dali ~]$terraform init
(.venv) [mdali@dali ~]$terraform plan
#------------------------------
...
Plan: 0 added, 0 changed, 0 destroyed.
#------------------------------
(.venv) [mdali@dali ~]$terraform apply
#------------------------------
Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

hosts = {
  "127.0.0.1" = "localhost gitlab.local gitlab.me"
  "192.169.1.168" = "gitlab.test"
  "192.169.1.170" = "prometheus.test"
  "192.169.1.180" = "grafana.test"
}

#------------------------------


## ajout du triggers
(.venv) [mdali@dali ~]$vi main.tf
#------------------------------
variable "hosts" {
  default = {
    "127.0.0.1"       = "localhost gitlab.local gitlab.me"
    "192.169.1.168"   = "gitlab.test"
    "192.169.1.170"   = "prometheus.test"
    "192.169.1.180"   = "grafana.test"
  }
}
resource "null_resource" "hosts" {
  for_each = var.hosts
  triggers = {
    monTrigger = each.value
  }
  provisioner "local-exec" {
    command ="echo ${each.key} ${each.value} >> hosts.txt"
  }
}

output "hosts" {
  value = var.hosts
}
#------------------------------

(.venv) [mdali@dali ~]$terraform init
(.venv) [mdali@dali ~]$terraform plan
#------------------------------
...
  # null_resource.hosts["192.169.1.180"] must be replaced
-/+ resource "null_resource" "hosts" {
      ~ id       = "2589828888573907244" ➡️ (known after apply)
      + triggers = {
          + "monTrigger" = "grafana.test"
        } # forces replacement
    }

Plan: 4 to add, 0 to change, 4 to destroy.
#------------------------------

(.venv) [mdali@dali ~]$terraform apply
#------------------------------
...
Apply complete! Resources: 4 added, 0 changed, 4 destroyed.

Outputs:

hosts = {
  "127.0.0.1" = "localhost gitlab.local gitlab.me"
  "192.169.1.168" = "gitlab.test"
  "192.169.1.170" = "prometheus.test"
  "192.169.1.180" = "grafana.test"
}
#------------------------------
```

</span></div>

[fichier logs local-exec type map](fichiers_logs/0004_localExecTypeMap_triggers.log)


On voit dans les logs que les valeurs ont été rajoutées en fin de fichier et non remplacée parce que le provider `local_exec` n'est pas idempotent dans la création des fihciers.  


<span style="color: #965DCE">

#####  **VARIABLE TYPE LIST** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
(.venv) [mdali@dali ~]$ vi main.tf
#------------------------------
variable "hosts" {
  default   = ["127.0.0.1 localhost","192.168.1.133 gitlab.test"]   ## la liste est un tableau dans le default
}

resource "null_resource" "hosts" {
  count = "${length(var.hosts)}"    ## "count" permet de faire de l'itération (sorte de while)  ➡️ ici on lui demande la "longueur de la liste de la variable hosts => 2 éléments  ➡️ il va boucler 2 fois
  provisioner "local-exec" {
    command = "echo '${element(var.hosts, count.index)}' >> hosts.txt"
    ## element est une fonction (comme en js)
    ## on va demander pour chaque valeur de la variable host (var.hosts) de lui récupérer la valeur de son index (0 ou 1 car 2 éléments)
  }
}
#------------------------------

(.venv) [mdali@dali ~]$terraform init
(.venv) [mdali@dali ~]$terraform plan
#------------------------------
...
Plan: 2 to add, 0 to change, 0 to destroy.
#------------------------------

(.venv) [mdali@dali ~]$terraform apply
#------------------------------
...
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
#------------------------------
```

</span></div>

[fichier de logs local-exec type list](./fichiers_logs/0005_localExecTypeList.log)  


<span style="color: #11E3EA">

####  **LA PRECEDENCE DES VARIABLES** 
</span>

Nous allons parler ici du stockage des variables.  
Il y a 2 manières d'utiliser les variables.  
Il existe différents type de variables:  
- string
- number
- bool (boolean)
- liste
- map

La déclaration d'une variable se fait de cette manière

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
variable "nomMaVariable" {
  type = "<type>"   (string | number | bool ) ## map et list ne se définissent pas
  default = <valeur de la variable>
  ## cas d'une liste  ➡️ ["valeur 1","valeur2"]
  ## cas d'une map    ➡️ {
    ##  "clé1" = "valeur1"
    ##  "clé2" = "valeur2"
    ##  }
  ## cas d'un booleen ➡️ true | false
  ## cas d'un string  ➡️ "ma variable de type string"
  ## cas d'un number  ➡️ 1 | 2 | ...
}
```

</span></div>

<div class="info" style='padding:0.1em; background-color:#068BE7; color:#EEB1F7'>
<span>

> _Remarque_: Si on déclare une variable **SANS** valeur, lors du **terraform apply** la valeur de la variable va nous être demandée dans le prompt  

</span></div>


Les variables peuvent être définies à différents niveaux: il s'agit de la **précédence des variables**  
- 1. environnement      
- 2. fichier : terraform.tfvars
- 3. fichier json : terraform.tfvars.json
- 4. fichier *.auto.tfvars ou *.auto.tfvars.json
- 5. CLI : -var ou -var-file

<div class="info" style='padding:0.1em; background-color:#068BE7; color:#EEB1F7'>
<span>

> _Remarque_: plus le chiffre est haut plus il y a de chance pour que la valeur de la variable sera choisie  

</span></div>




Pour décrire les différences (dans les sous-paragraphes qui suivent) on utilise comme fichier main.tf le fichier suivant:  

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
variable "maVariable" {}

output "outputMaVariable" {
  value = "${var.maVariable}"
}
```

</span></div>

<span style="color: #965DCE">

#####  **Cas variable environnement** 
</span>

On renseigne la variable avec un `export TF_VAR_<nomMaVariable>` puis lors du `terraform apply` elle sera prise en compte.

Pour "désetter" la variable on exécute la commande `unset TF_VAR_<nomMaVariable>`



<span style="color: #965DCE">

#####  **Cas variable fichier terraform.tfvars** 
</span>

Si on crée un fichier `terraform.tfvars` et qu'on renseigne la variable dans ce cas lors du `terraform apply` c'est cette variable qui sera prise en compte même si nous avons fait l'export de cette variable d'environnement: le fichier `terraform.tfvars` est **prédominant à la variable d'environnement**.





<span style="color: #965DCE">

#####  **Cas variable fichier json terraform.tfvars.json** 
</span>

De la même manière si on crée un fichier `terraform.tfvars.json` il sera lui aussi **prédominant au fichier `terraform.tfvars`**.







<span style="color: #965DCE">

#####  **Cas variable fichier __*.auto.tfsvars__ ou __*.auto.tfvars.json__** 
</span>


Idem pour les fichiers en `*.auto.tfvars` (ex: `production.auto.tfvars`) l'emportera sur les précédents.  







<span style="color: #965DCE">

#####  **Cas variable CLI: -var ou -var-file** 
</span>



Le niveau le plus haut pour la prise en compte des  valeurs d'une variable est dans la CLI.  
`terraform apply -var '<nomMaVariable>="<valeurMaVariable>"`  
ou la prise en compte d'un fichier de variable particulier:  
`terraform apply -var-file <nomFichierVariable>.tfvars`  

<div class="info" style='padding:0.1em; background-color:#068BE7; color:#EEB1F7'>
<span>

>_Remarque_: Si dans le CLI on renseigne une variable ET un fichier de variables, la commande est lue de gauche à droite ➡️ **la valeur la "plus à droite" dans la CLI sera prise en compte**  

</span></div>

<div class="note" style='padding:0.1em; background-color:#0A9A7A; color:#1F2624'>
<span>

> _exemple_:
> on crée le fichier `varFile.tfvars` dans lequel on renseigne la variable `'str="toto"'`  
> puis on exécute la commande `terraform apply -var 'str="tata"' -var-file varFile.tfvars`   
> la valeur qui sera prise sera "toto" car `varFile.tfvars` est plus à droite dans la ligne de commande que "`-var`"

</span></div>



<span style="color: #11E3EA">

####  **REMOTE_EXEC ET FILE : COMMANDES VIA SSH** 
</span>

Le **remote_exec** est l'équivalent du _local_exec_ mais sur une machine distante via un protocole de connexion (ex: ssh).  

<div class="info" style='padding:0.1em; background-color:#068BE7; color:#EEB1F7'>
<span>

> _Remarque_: Dnas ce cas on commence à avoir une arborescence dans le répertoire terraform: `main.tf` + `terraform.tfvars`  

</span></div>


<span style="color: #965DCE">

#####  **main.tf** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
variable "ssh_host" {}                  ## on déclare notre variable (ici ssh_host parce qu'on crée un serveur distant)
variable "ssh_user" {}
variable "ssh_key" {}

resource "null_resource" "ssh_target" {     ## on déclare une null_resource  parce qu'on n'est pas à travers un provider et on nomme cette ressource (ssh_target)
  connection {                      ## on renseigne les éléments pour la connexion ssh (ici cas linux)
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)     ## on défini le fichier où est présente la clé ssh du user défini
  }

  provisioner "remote-exec" {
    inline = [                      ## la clause inline permet de déclarer les commandes qu'on souhaite exécuter sur notre serveur cible
      "sudo apt update -qq >/dev/null",
      "sudo apt install -qq -y nginx >/dev/null"
    ]
  }
}
```

</span></div>

Une fois ce fichier créé il faut déclarer les variables renseignées
**ssh_key**   ➡️  `ssh-keygen -b 4096`  ➡️  la clé ssh est générée


On crée le fichier **terraform.tfvars** dans lequel on renseigne les informations sur ces variables.  

<span style="color: #965DCE">

#####  **terraform.tfvars** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
# on déclare où aller chercher la clé ssh qu'on a générée
ssh_key   = "<cheminCompletCleSsh>"

## on renseigne les informations sur les autres variables
ssh_user  = "<nomUser>"       ## correspond au user de la clé ssh créée précédement

## on renseigne l'IP de la machine cible
ssh_host  = "192.169.1.101"
```

</span></div>

<div class="alert" style='padding:0.1em; background-color:#820107; color:#E091E2'>
<span>

⚠️ Penser à copier la clé ssh publique dans le **`.ssh/authorized_keys`** du user sur le serveur cible ⚠️

</span></div>

Ensuite on exécute les commandes terraform: 

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
terraform init
terraform plan
terraform apply
```

</span></div>

Pour valider que l'installation s'est faite correctement, comme il s'agit d'un nginx qui a été installé on peut faire un curl de la machine `curl -v 192.169.1.101` ➡️ doit nous retourner le page nginx et on peut aussi vérifier que le service nginx est bien up `service status nginx`.  



<span style="color: #965DCE">

#####  **Pour aller plus loin** 
</span>


On met en place un **main.tf** plus complet avec les fichiers dont il va avoir besoin pour fonctionner:  

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
vi main.tf
#--------------------------------------------------------
variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}

resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }

  provisioner "remote-exec" {
    inline  = [
      "sudo apt update -qq >/dev/null",
      "sudo apt install -qq -y nginx >/dev/null"
    ]
  }
  provisioner "file" {        ## le provisioner "file" permet de récupérer un fichier en local et de l'envoyer sur le serveur distant (équivalent du module copy ansible)
    source      = "nginx.conf"        ## chemin vers le fichier nginx.conf (comme un template)
    destination = "/tmp/default"      ## on dépose le fichier dans ce répertoire parce que le provisioner file n'a pas les extensions de droits via sudo
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo cp -a /tmp/default /etc/nginx/sites-available/default", ## donc on fait une copie sur fichier en conservant les doits
      "sudo systemctl restart nginx"    ## puis on relance le service
    ]
  }
  provisioner "local-exec" {
    command = "curl ${var.ssh_host}:8000"    ## on lance en local un curl sur le port défini dans le ficiher nginx.conf pour valider le fonctionnement
  }
}

output "host" {             ## puis on affiche les variables
  value = var.ssh_host
}
output "user" {
  value = var.ssh_user
}
output "key" {
  value = var.ssh_key
}
#--------------------------------------------------------

vi nginx.conf       # conf nginx "standard" (/etc/nginx/sites-available/default)
#--------------------------------------------------------
server {
  listen 8000 default_server;               ## port 80 par defaut modifié pour de la rupture de protocole (ex: 8000), cela permet de faire un changement de port mais le service écoute toujours sur le port 80
  listen [::]:8000 default_server;          ## port 80 par defaut ➡️ dans ce cas on le modifie ici aussi

  root /var/www/html;

  index index.html index.htm index.nginx-debian.html;

  server_name_;

  location / {
    try_files $uri $uri/ =404;
  }
}
#--------------------------------------------------------
## dans ce cas ce fichier est au même niveau que le fichier main.tf


## ensuite on exécute terraform
terraform init
terraform plan
#--------------------------------------------------------
...
Plan: 1 to add, 0 to change, 0 to destroy.
#--------------------------------------------------------

terraform apply
#--------------------------------------------------------
...
## resultat du curl

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

Outputs:

host = 192.169.1.101
key = /home/toto/.ssh/id_rsa
user = toto
#--------------------------------------------------------
```

</span></div>




<span style="color: #11E3EA">

####  **REMOTE_EXEC : INSTALLATION DE DOCKER ET SA SOCKET** 
</span>

Nous allons ici utiliser le provider docker.
Pour cela nous avons besoin de clés ssh pour une connexion: nous utilisons la clé précédemment utilisée pour le user "toto".  

De la même manière on copie la clé publique dans le `~/.ssh/authorized_keys` de la machine cible.

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
# on met en place les variables que nous allons utiliser
vi terraform.tfvars
#--------------------------------------------------------
ssh_key   = "/home/toto/.ssh/id_rsa"
ssh_user  = "toto"
ssh_host  = "192.169.1.102"
#--------------------------------------------------------

# 
vi main.tf
#--------------------------------------------------------
variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}

resource "null_resource" "ssh_target" {   ## partie de la connexion ssh
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }

  provisioner "remote-exec" {   ## installation de docker
    inline  = [
      "sudo apt update -qq >/dev/null",
      "curl -fsSL https://get.docker.com -o get-docker.sh",    ## installation de docker via un script
      "sudo chmod 755 get-docker.sh",              ## on rend le script exécutable
      "sudo ./get-docker.sh >/dev/null"           ## on installe docker
    ]
  }
  provisioner "file" {            ## partie de copie du fichier de conf pour l'utilisation de la socket docker
    source      = "startup-options.conf"
    destination = "/tmp/startup-options.conf"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo mkdir -p /etc/systemd/system/docker.service.d/",
      "sudo cp -a /tmp/startup-options.conf /etc/systemd/system/docker.service.d/startup-options.conf",
      "sudo systemctl daemon-reload",
      "sudo systemctl restart docker",
      "sudo usermod -aG docker toto"    ## on ajoute le user toto dans le group docker
    ]
  }
}

output "host" {
  value = var.ssh_host
}
output "user" {
  value = var.ssh_user
}
#--------------------------------------------------------

# création du fichier pour l'utilisation de la socket docker: startup-options.conf
vi startup-options.conf
#--------------------------------------------------------
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://192.169.1.102:2375 -H unix:///var/run/docker.sock
#--------------------------------------------------------
## ce fichier sert à passer les informations de settings à docker.d pour lui dire de lancer la socket sur l'IP définie + lance la socket "unix"

# exécution de terraform
terraform init
terraform plan
terraform apply -auto-approve     ## "-auto-approve" permet de ne pas à avoir à répondre "yes"
#--------------------------------------------------------
...
Apply complete! resources: 1 added, 0 changed, O destroyed

Outputs:

host = 192.169.1.102
user = toto
#--------------------------------------------------------
```

</span></div>

Docker est installé sur la machine distante

Pour valider que la socket fonctionne on exécute la commande `docker -H 192.169.1.102 ps` depuis la machine sur laquelle nous avons exécuter terraform et nous avons le même résultat que si on effectue un `docker ps` directement depuis la machine cible.  

En exécutant maintenant depuis la machine "locale" (où on exécute terraform) on peut donc exécuter docker. 

<div class="note" style='padding:0.1em; background-color:#0A9A7A; color:#1F2624'>
<span>

_exemple_: `docker -H 192.169.1.102 run -d nginx` va lancer un container nginx sur la machine distante.  

</span></div>





<span style="color: #11E3EA">

####  **PROVIDER DOCKER : IMAGE ET CONTAINER** 
</span>


Utilisation du provider docker avec utilisation de la socket docker.
L'utilisation de la socket permet un accès distant au container ➡️ potentielle faille de sécurité: pour corriger ce problème mettre en place le TLS  


Déclaration du provider docker

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
## connexion à la socket docker avec le provider docker
vi docker.tf
#--------------------------------------------------------
terraform {                 ## partie nécessaire pour pouvoir utiliser le provider docker
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {
  host = "tcp://<ipAdressDeLaSocket>:<portConnexion_parDefaut_2375>"  ## OU utilisation de la variable ➡️ 'host    = "tcp://${var.ssh_host}:2375"'
}

## récupération d'une image docker à installer
resource "docker_image" "nginx" {   # sur cette ligne "nginx" est le nom de la resource que nous avons définie
  name = "harbor.svc.agpm.fr/ext-dockerhub/library/nginx:latest"    ## en utilisant le docker-registry harbor
}


## exécuter les containers
resource "docker_container" "nginx" {
  image   = docker_image.nginx.latest
  name    = "enginecks"
  ports {
    internal  = 80
    external  = 80
  }
}
#--------------------------------------------------------
```

</span></div>



Ensuite nous exécutons les commandes terraform "classiques": 

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
terraform init
terraform plan
terraform apply
```

</span></div>


[fichiers de log provider Docker avec nginx et socket](./fichiers_logs/0008_providerDocker_nginx_remote.log)  


<div class="info" style='padding:0.1em; background-color:#068BE7; color:#EEB1F7'>
<span>

> _Remarque_: Pour que ce qui est présenté dans ce chapitre fonctionne "correctement" il faut voir la partie suivante qui concerne les modules: en l'état nous avons une erreur (_Error: Error pinging Docker server_) qui est due au  fait qu'il essaie de contacter la socket docker qui n'existe pas encore ➡️ il faut créer les modules qui vont gérer les interdépendances entre eux.  

> _Remarque_: Si on modifie le fichier `docker.tf` d'une image nginx à une autre image, les machines "nginx" vont être détruites ET les images nginx vont être supprimées lorsqu'on fait un `docker images`

</span></div>





<span style="color: #11E3EA">

####  **LES MODULES** 
</span>


<span style="color: #965DCE">

#####  **Définitions d'un module terraform** 
</span>

Les modules sont des regroupements de fichiers terraform avec une cohérence en matière de ressources.  
Un module au sens terraform est l'équivalent d'un role au niveau ansible.  
Un module est composé de répertoires et de fichiers terraform.  

On parle de **root module**: c'est le répertoire principal dans lequel on fait le `main.tf`  
Par defaut les modules vont utiliser le provisider du _root module_.  

Syntaxe:

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```
vi main.tf
#--------------------------------------------------------
...
module "monModule" {
  source = "./rep_module"
}
...
#--------------------------------------------------------
```
</span></div>


Il est possible de pouvoir instancier le même module:  

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```
module "monModule1" {
  source = "./rep_module"
}
module "monModule2" {
  source = "./rep_module"
}
```

</span></div>

<span style="color: #965DCE">

#####  **Structure d'un module terraform** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
|--- README.md
|--- main.tf
|--- variables.tf
|--- outputs.tf
|--- ...
|--- modules/
|--- |--- nestedA/
|--- |--- |--- README.md
|--- |--- |--- main.tf
|--- |--- |--- variables.tf
|--- |--- |--- outputs.tf
|--- |--- nestedB/
|--- |--- |--- ...
|--- exemples
|--- |--- exempleA/
|--- |--- |--- main.tf
|--- |--- exempleB/
|--- |--- |--- main.tf
|--- ...
```

</span></div>

<span style="color: #965DCE">

#####  **Installation d'un module terraform** 
</span>

[Documentation terraform get](https://developer.hashicorp.com/terraform/cli/commands/get)  

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
## installation des modules
terraform get
terraform init
#------------------------------------------------

## gérer les dépendances
terraform apply -target=module.docker
terraform apply -target=module.postgres
#------------------------------------------------

## ou via des variables

#------------------------------------------------
```

</span></div>



<span style="color: #965DCE">

#####  **Première utilisation de module terraform avec le provider docker** 
</span>



On crée des répertoires pour la mise en place de modules.  
On conserve le terraform.tfvars avec les variables `ssh_host`, `ssh_user`, `ssh_key`.  


<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
## arborescence du projet
tree
#------------------------------------------------
.
├── main.tf
├── modules
│   └── docker_install
├── terraform.tfvars
├── startup-options.conf
├── toto              ## contient les clés ssh
│   └── .ssh
│       └── id_rsa
│       └── id_rsa.pub
#------------------------------------------------



# On copie le main.tf
cp main.tf modules/docker_install/

## on crée un fichier de variables variables.tf contenant les variables du main.tf dans le module
vi modules/docker_install/variables.tf
#------------------------------------------------
variable "local" {}

variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}
#------------------------------------------------

# on nettoie le fichier modules/docker_install/main.tf des variables (suppression des variables copiées)

# on fait la même chose pour les outputs
vi modules/docker_install/outputs.tf
#------------------------------------------------
output "host" {
  value = var.ssh_host
}
output "user" {
  value = var.ssh_user
}
#------------------------------------------------


# on se retrouve avec le fichier modules/docker_install/main.tf qui ressemble à ça
cat modules/docker_install/main.tf
#------------------------------------------------
## récupération du script d'installation de docker en local car non ouvert depuis machine cible
resource "null_resource" "local" {
  provisioner "local-exec" {
    command = "curl -fsSl get.docker.com -o get-docker.sh"
  }
}


## connexion à la machine cible
resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }
  provisioner "file" {
    source      = "./get-docker.sh"
    destination = "/tmp/get-docker.sh"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo yum update >/dev/null",
      "sudo chmod 755 get-docker.sh",
      "sudo ./get-docker.sh >/dev/null"
    ]
  }
  provisioner "file" {
    source      = "${path.module}/startup-options.conf"
    destination = "/tmp/startup-options.conf"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo mkdir -p /etc/systemd/system/docker.service.d/",
      "sudo cp -a /tmp/startup-options.conf /etc/systemd/system/docker.service.d/startup-options.conf",
      "sudo systemctl daemon-reload",
      "sudo systemctl restart docker",
      "sudo usermod -aG docker toto"
    ]
  }
}
#------------------------------------------------

# l'arborescence est maintenant
tree
#------------------------------------------------
.
├── main.tf
├── modules
│   └── docker_install
│       ├── main.tf
│       ├── outputs.tf
│       ├── variables.tf
│       └──startup-options.conf
├── terraform.tfvars
├──toto
│   └── .ssh
│       └── id_rsa
│       └── id_rsa.pub
#------------------------------------------------


# On modifie le main.tf
vi main.tf
#------------------------------------------------
# variables                     ## elles sont présentes dans le fichier terraform.tfvars
variable "local" {}

variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}

## on appelle les modules
module "docker_install" {                 ## docker_install est le nom que nous choississons pour ce module
  source      = "./modules/docker_install"    ## on va chercher les fichiers du module

  ## les varaibles étant dans terraform.tfvars il faut qu'elles soient prises en compte par le module
  local_mod   = var.local
  ssh_host_mod    = var.ssh_host        ## ssh_host_mod est le nom de la variable ssh_host dans le module qui prend la valeur de la variable ssh_hosts de terraform.tfvars
  ssh_user_mod    = var.ssh_user
  ssh_key_mod     = var.ssh_key          ## ces variables sont "exportées" vers le fichier ./modules/docker_install/variables.tf
}

#------------------------------------------------

## on modifie le nom des variables pour les différencier (celles du module vs celles du tfvars) pour la compréhension
vi modules/docker_install/variables.tf
#------------------------------------------------
variable "ssh_host_mod" {}      ## on modifie le nom avec "_mod" pour les différencier -montrer celles du modules) mais on pourrait garder le même nom
variable "ssh_user_mod" {}
variable "ssh_key_mod" {}
#------------------------------------------------
## la variable <variable "local" {}> est supprimée puisque cette partie ne fait pas partie du module d'install docker


## idem pour le fichier modules/docker_install/main.tf
vi modules/docker_install/main.tf
#------------------------------------------------
## récupération du script d'installation de docker en local car non ouvert depuis machine cible
resource "null_resource" "local_mod" {
  provisioner "local-exec" {
    command = "curl -fsSl get.docker.com -o get-docker.sh"
  }
}


## connexion à la machine cible
resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user_mod
    host        = var.ssh_host_mod
    private_key = file(var.ssh_key_mod)
  }
  provisioner "file" {
    source      = "./get-docker.sh"
    destination = "/tmp/get-docker.sh"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo apt update -qq >/dev/null",
      "sudo chmod 755 get-docker.sh",
      "sudo ./get-docker.sh >/dev/null"
    ]
  }
  provisioner "file" {
    source      = "${path.module}/startup-options.conf"
    destination = "/tmp/startup-options.conf"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo mkdir -p /etc/systemd/system/docker.service.d/",
      "sudo cp -a /tmp/startup-options.conf /etc/systemd/system/docker.service.d/startup-options.conf",
      "sudo systemctl daemon-reload",
      "sudo systemctl restart docker",
      "sudo usermod -aG docker toto"
    ]
  }
}
#------------------------------------------------

## idem pour le fichier outputs.tf
vi outputs.tf
#------------------------------------------------
output "host" {
  value = var.ssh_host_mod
}
output "user" {
  value = var.ssh_user_mod
}
#------------------------------------------------


# l'arborescence est 
tree
#------------------------------------------------
.
├── main.tf
├── modules
│   └── docker_install
│       ├── main.tf
│       ├── outputs.tf
│       ├── variables.tf
│       └── startup-options.conf
├── terraform.tfvars
└── toto
#------------------------------------------------
```

</span></div>

On lance le chargement du module et son exécution


[documentation des references de variables terraform](https://developer.hashicorp.com/terraform/language/expressions/references#filesystem-and-workspace-info) 



<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
## on charge le module
terraform get
#------------------------------------------------
- docker_install in modules/docker_install
#------------------------------------------------

terraform init        # on récupère le provider null_resource (pas docker) -> IMPORTANT de respecter l'ordre
#------------------------------------------------
...
- Installing hashicorp/null v3.2.1...
- Installed hashicorp/null v3.2.1 (signed by HashiCorp)
...
Terraform has been successfully initialized!
#------------------------------------------------


terraform plan
#------------------------------------------------
Plan: 2 to add, 0 to change, 0 to destroy.
#------------------------------------------------


terraform apply -auto-approve
#------------------------------------------------
Applied: 2 to add, 0 to change, 0 to destroy.
#------------------------------------------------

## l'arborescence est
tree
#------------------------------------------------
.
├── get-docker.sh                 ## le script d'install de docker qui a été download en local
├── main.tf
├── modules
│   └── docker_install
│       ├── main.tf
│       ├── outputs.tf
│       └── variables.tf
├── startup-options.conf
├── terraform.tfstate
├── terraform.tfvars
└── toto
#------------------------------------------------
```

</span></div>




 











[archives fichiers module provider docker](./archives/xavki/0009_modules_providerDocker/*)  












<span style="color: #965DCE">

#####  **Utilisation des modules terraform apply et target plus variables** 
</span>


Dans cette partie nous allons maintenant continuer avec le provider docker mais en exécutant docker sur la machine cible.  
Nous repartons du projet précédent et nous allons exécuter en plus le provider docker: module **docker_run**   


<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash
## arborescence du projet
tree
.
├── main.tf
├── modules
│   ├── docker_install
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   ├── startup-options.conf
│   │   └── variables.tf
│   └── docker_run
│       ├── main.tf
│       └── variables.tf
├── terraform.tfvars
└── toto

4 directories, 8 files



## étapes terraform
terraform get
terraform init
terraform apply -target module.docker_install -auto-approve   # le "-target" est nécessaire

## si "terraform plan" avant le "apply -target" -> KO


terraform plan
terraform apply -auto-approve
## la machine est up avec le container nginx qui fonctionne
```
</div></span>







<span style="color: #965DCE">

#####  **Provider docker: utilisation du network** 
</span>


Le but est de créer des containers docker dans 1 network défini.  
Pour cela on utilise **docker_network**.  

[Doc terraform docker network](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs/resources/network)  
[Doc terraform docker container](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs/resources/container)    

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash

```

</div></span>




<span style="color: #965DCE">

#####  **Première utilisation de module terraform avec le provider docker** 
</span>

<div class="note" style='padding:0.1em; background-color:#000000; color:#000000'>
<span>

```bash

```

</div></span>

_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

## CREATION D'UN CONTAINER DOCKER
</span>

Il faut dans 1 premier temps que docker soit installé et avec la dernière version.

```bash
## suppression des anciennes versions
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
## mise à jour des outils d'installations
yum install -y yum-utils
## ajout du repo docker
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
## installation de docker et docker engine
dnf install -y docker-ce.x86_64 docker-ce-cli.x86_64
# démarrer le service docker
systemctl start docker
## verifier que docker est up
docker run hello-world
```


Une fois terraform installé on va créer le 1er container docker nginx.
1. créer un répertoire dans lequel on va créer les fichiers terraform et lancer les commandes terraform.
2. créer le fichier de configuration du container nginx
3. vérifie la bonne création du serveur
4. modifier le fichier et relancer pour voir les différences
5. supprimer le container


```bash
dali@dali 00001-learn-terraform-docker-container % ls -la
total 8
drwxr-xr-x  3 dali  staff   96 19 jan 17:39 .
drwxr-xr-x  3 dali  staff   96 19 jan 17:38 ..
-rw-r--r--  1 dali  staff  392 19 jan 17:39 main.tf

dali@dali 00001-learn-terraform-docker-container % vi main.tf
#############################################
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "terraform_nginx"
  ports {
    internal = 80
    external = 9000
  }
}
#############################################
```

....................................................................................................................... 


<span style="color: #11E3EA">

#### on vérifie ce qu'il va se passer si on lance la création du container
</span>

```bash
dali@dali 00001-learn-terraform-docker-container % terraform init

Initializing the backend...
Initializing provider plugins...
- Finding kreuzwerker/docker versions matching "~> 2.16.0"...
- Installing kreuzwerker/docker v2.16.0...
- Installed kreuzwerker/docker v2.16.0 (self-signed, key ID 24E54F214569A8A5)

Partner and community providers are signed by their developers.
If you d like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```


....................................................................................................................... 


<span style="color: #11E3EA">

#### on exécute la création
</span>

```bash
dali@dali 00001-learn-terraform-docker-container % terraform apply

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create
Terraform will perform the following actions:
  # docker_container.nginx will be created
  + resource "docker_container" "nginx" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + entrypoint       = (known after apply)
      + env              = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + init             = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = "json-file"
      + logs             = false
      + must_run         = true
      + name             = "tutorial"
      + network_data     = (known after apply)
      + read_only        = false
      + remove_volumes   = true
      + restart          = "no"
      + rm               = false
      + security_opts    = (known after apply)
      + shm_size         = (known after apply)
      + start            = true
      + stdin_open       = false
      + tty              = false

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }

      + ports {
          + external = 9000
          + internal = 80
          + ip       = "0.0.0.0"
          + protocol = "tcp"
        }
    }

  # docker_image.nginx will be created
  + resource "docker_image" "nginx" {
      + id           = (known after apply)
      + keep_locally = false
      + latest       = (known after apply)
      + name         = "nginx:latest"
      + output       = (known after apply)
      + repo_digest  = (known after apply)
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

docker_image.nginx: Creating...
docker_image.nginx: Still creating... [10s elapsed]
docker_image.nginx: Still creating... [20s elapsed]
docker_image.nginx: Still creating... [30s elapsed]
docker_image.nginx: Still creating... [40s elapsed]
docker_image.nginx: Still creating... [50s elapsed]
docker_image.nginx: Creation complete after 52s [id=sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85nginx:latest]
docker_container.nginx: Creating...
docker_container.nginx: Creation complete after 1s [id=6b1a8a381d06af6185cfb76d19b56f6ecad4daea693947a4e8be0c00b3173bbb]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```


....................................................................................................................... 


<span style="color: #11E3EA">

#### on vérifie que le container a été créé (son nom et son port)
</span>

```bash
dali@dali 00001-learn-terraform-docker-container % docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                  NAMES
6b1a8a381d06   605c77e624dd   "/docker-entrypoint.…"   48 seconds ago   Up 46 seconds   0.0.0.0:9000->80/tcp   terraform_nginx
```

....................................................................................................................... 


<span style="color: #11E3EA">

#### on exécute dans le navigateur http://localhost:9000/
</span>

Le page d'accueil nginx s'affiche


....................................................................................................................... 


<span style="color: #11E3EA">

#### on supprime le container
</span>

```bash
dali@dali 00001-learn-terraform-docker-container % terraform destroy
docker_image.nginx: Refreshing state... [id=sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85nginx:latest]
docker_container.nginx: Refreshing state... [id=7993fc884dfe2e8aade7c7536bb2b0c75bf6fbc0a94bdc1b89589e471e18bcae]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy
Terraform will perform the following actions:
  # docker_container.nginx will be destroyed
  - resource "docker_container" "nginx" {
      - attach            = false -> null
      - command           = [
          - "nginx",
          - "-g",
          - "daemon off;",
        ] -> null
      - cpu_shares        = 0 -> null
      - dns               = [] -> null
      - dns_opts          = [] -> null
      - dns_search        = [] -> null
      - entrypoint        = [
          - "/docker-entrypoint.sh",
        ] -> null
      - env               = [] -> null
      - gateway           = "172.17.0.1" -> null
      - group_add         = [] -> null
      - hostname          = "7993fc884dfe" -> null
      - id                = "7993fc884dfe2e8aade7c7536bb2b0c75bf6fbc0a94bdc1b89589e471e18bcae" -> null
      - image             = "sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85" -> null
      - init              = false -> null
      - ip_address        = "172.17.0.3" -> null
      - ip_prefix_length  = 16 -> null
      - ipc_mode          = "private" -> null
      - links             = [] -> null
      - log_driver        = "json-file" -> null
      - log_opts          = {} -> null
      - logs              = false -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
      - must_run          = true -> null
      - name              = "terraform_nginx" -> null
      - network_data      = [
          - {
              - gateway                   = "172.17.0.1"
              - global_ipv6_address       = ""
              - global_ipv6_prefix_length = 0
              - ip_address                = "172.17.0.3"
              - ip_prefix_length          = 16
              - ipv6_gateway              = ""
              - network_name              = "bridge"
            },
        ] -> null
      - network_mode      = "default" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      - read_only         = false -> null
      - remove_volumes    = true -> null
      - restart           = "no" -> null
      - rm                = false -> null
      - security_opts     = [] -> null
      - shm_size          = 64 -> null
      - start             = true -> null
      - stdin_open        = false -> null
      - sysctls           = {} -> null
      - tmpfs             = {} -> null
      - tty               = false -> null

      - ports {
          - external = 9000 -> null
          - internal = 80 -> null
          - ip       = "0.0.0.0" -> null
          - protocol = "tcp" -> null
        }
    }

  # docker_image.nginx will be destroyed
  - resource "docker_image" "nginx" {
      - id           = "sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85nginx:latest" -> null
      - keep_locally = false -> null
      - latest       = "sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85" -> null
      - name         = "nginx:latest" -> null
      - repo_digest  = "nginx@sha256:0d17b565c37bcbd895e9d92315a05c1c3c9a29f762b011a10c54a66cd53c9b31" -> null
    }

Plan: 0 to add, 0 to change, 2 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

docker_container.nginx: Destroying... [id=7993fc884dfe2e8aade7c7536bb2b0c75bf6fbc0a94bdc1b89589e471e18bcae]
docker_container.nginx: Destruction complete after 0s
docker_image.nginx: Destroying... [id=sha256:605c77e624ddb75e6110f997c58876baa13f8754486b461117934b24a9dc3a85nginx:latest]
docker_image.nginx: Destruction complete after 0s

Destroy complete! Resources: 2 destroyed.
```

....................................................................................................................... 


<span style="color: #11E3EA">

#### on vérifie que le container soit supprimé
</span>

```bash
dali@dali 00001-learn-terraform-docker-container % docker ps
CONTAINER ID   IMAGE     COMMAND       CREATED      STATUS      PORTS     NAMES
```



=======================================================================================================================  

<span style="color: #EA9811">

###  Descriptif fichier `main.tf`
</span>

....................................................................................................................... 


<span style="color: #11E3EA">

#### main.tf
</span>

```bash
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}
provider "docker" {
  host = "unix:///var/run/docker.sock"
}
resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}
resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "terraform_nginx"
  ports {
    internal = 80
    external = 9000
  }
}
```


....................................................................................................................... 


<span style="color: #11E3EA">

##### block `terraform{}`
</span>

Ce bloc contient tous les parametres terraform lié à l'infrastructure y compris le provider.
Dans ce cas le provider docker est **kreuzwerker/docker**.
Il est possible d'ajouter une contrainte de version sur les providers avec un bloc **required_providers**.
[Documentation required_providers](https://www.terraform.io/language/providers/requirements)
Ce bloc doit être placé en début du bloc `terraform{}`
>exemple
```bash
terraform {
  required_providers {
    mycloud = {
      source  = "mycorp/mycloud"
      version = "~> 1.0"
    }
  }
}

provider "mycloud" {
  # ...
}
```

##### block `provider`
> Le bloc **provider** configure le fournisseur spécifié, ici docker.
> Un fournisseur est un plugin que Terraform utilise pour créer et gérer vos ressources.
```bash
provider "docker" {
  host = "unix:///var/run/docker.sock"
}
```

>_Remarque_: Il est possible d'**utiliser plusieurs blocs provider dans votre configuration Terraform** pour gérer les ressources de différents fournisseurs. Vous pouvez même utiliser différents fournisseurs ensemble. Par exemple, vous pouvez transmettre l'ID d'image Docker à un service Kubernetes.

https://learn.hashicorp.com/tutorials/terraform/docker-build?in=terraform/docker-get-started




_______________________________________________________________________________________________________________________    
  
<span style="color: #CE5D6B">

##  **USING TERRAFORM IN A CI/CD ENVIRONEMNT** 
</span>




=======================================================================================================================    

<span style="color: #EA9811">

###  **TERRAFORM CICD - USING GITHUB** 
</span>

![tf_cicd_demoWorkflow](./images/tf_cicd_demoWorkflow.png)    

Se connecter au sandbox AWS pour pouvoir faire la demo qui suit: https://learn.acloud.guru/cloud-playground/cloud-sandboxes 



=======================================================================================================================    

<span style="color: #EA9811">

###  **TERRAFORM CICD - TERRAFFORM CLOUD SETUP** 
</span>


**Les pre requis pour cette partie sont**:
- **AWS ACCESS KEY ID**
- **AWS SECRET ACCESS KEY**



**Objectifs de cette demo**:   
- Créer une organisation et workspace Terraform Cloud    
- Ajout des variables d'environnement    
- Créer une API token    
- Sauver ce token pour plus tard    

[Terraform Cloud](https://app.terraform.io/session)    




➡️ Actions faites sur la conf terraform cloud





=======================================================================================================================    

<span style="color: #EA9811">

###  **TERRAFORM CICD - GITHUB REPOSITORY SETUP** 
</span>



**Les pre requis pour cette partie sont**:   
- github login  https://github.com/login 
- clone du repo https://github.com/JesseHoch/learn-terraform-github-actions 



**Objectifs de cette demo**:   
- Se loguer à GitHub et faire un fork du repo donné
- créer un nouveau secret appelé **TF_API_TOKEN**
- Utiliser l'API token terraform de la demo précédente
- activer le workflow pre configuré   
- clone le repo github actions (depuis un terminal)





=======================================================================================================================    

<span style="color: #EA9811">

###  **TERRAFORM CICD - ACTIONS WORKFLOW** 
</span>


**Les pre requis pour cette partie sont**:  
- le repo github action scloné sur mon environnement


**Objectifs de cette demo**: 
- voir le fichier .github/workflows/terraform.yml
- review du fichier main.tf



[Fichier de logs](./archives/demo_githubActions.log)    




=======================================================================================================================    

<span style="color: #EA9811">

###  **TERRAFORM CICD - CREATING AND MERGING PULL REQUESTS** 
</span>


**Les pre requis pour cette partie sont**:  
- terrafform cloud organization    
- terraform cloud workspace   



**Objectifs de cette demo**: 
- créer une nouvelle branche dans notre repo appelée update-tfc-backend 
- update du main.tf avec notre terraform cloud organization et workspace
- add, commit et push sur github   
- review des ces updates et faire le pull request





=======================================================================================================================    

<span style="color: #EA9811">

###  **TERRAFORM CICD - VERIFYING A PROVISIONED INSTANCE** 
</span>



**Objectifs de cette demo**:     
- verifier que github actions et terraform cloud pour que l'instance ec2 est été provisionnée
- verifier l'instance ec2
- detruire les ressources  



 

_______________________________________________________________________________________________________________________    
  
<span style="color: #CE5D6B">

##  **TERRAFORM AND AWS** 
</span>


Le but de ce TP est de créer et manager une infrastructure sur AWS    

![tf_manage_aws_infra](./images/tf_manage_aws_infra.png)    





[Documentation aws: installing aws cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)    
[Sandbox A Cloud Guru](https://learn.acloud.guru/cloud-playground/cloud-sandboxes)    


**Authentification**

```bash
## installation de aws cli 
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install 
aws --version
```

Depuis la page sandbox on récupére les credentials qui vont nous servir à la configuration de la CLI aws

```bash
## configuration de la cli aws
aws configure
#-------------------------------------------
AWS Access Key ID: <noter l acess key id depuis le navigateur sandbox>
AWS Secret Acess Key:  <noter le secret key id depuis le navigateur sandbox>
Default region name: us-east-1 
Default output format: json
#-------------------------------------------
```

**Contruction de l'infrastructure**     

Nous avons ce fichier de configuration qui est donné (https://raw.githubusercontent.com/linuxacademy/content-terraform-2021/main/main_aws.tf)

```bash
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "app_server" {
  ami           = "ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
```



```bash
## creation du dossier demo-terraform-aws-instance
mkdir demo-terraform-aws-instance
cd demo-terraform-aws-instance/ 

## récupération du fichier de configuration et modification de son nom
wget https://raw.githubusercontent.com/linuxacademy/content-terraform-2021/main/main_aws.tf -O main.tf

## modification du fichier main.tf
vi main.tf
#-------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

### creation du provider aws et définition de la region
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

### creation de la ressource à partir de l'ami définie
resource "aws_instance" "app_server" {
  ami           = "ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
#-------------------------------------------

## initialisation du repertoire de travail
terraform init 

## on reformate le fichier de configuration (pour être propre)
terraform fmt

## on valide les fichiers terraform (pas d'erreurs de syntaxe par exemple)
terraform validate

## on crée de plan de construction de l'infra
terraform plan

## on crée l'infrastructure
terraform apply --auto-approve

## on verifie les states
terraform show 

## on verifie les ressources
terraform state list
```

On se connecte à la `console aws (en définissant la region/EC2/instances` et on voit notre instance


**Manage l'infrastructure**    



```bash
## on va modifier l'AMI de l'instance
vi main.tf
#-------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

### creation du provider aws et définition de la region
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

### creation de la ressource à partir de l'ami définie
resource "aws_instance" "app_server" {
  ami           = "ami-0ab4d1ecf9a1215a"                    ## cette ligne a été modifiée
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
#-------------------------------------------

## on relance la création du plan
terraform plan                            ## il va afficher 1 destroy + 1 add

##on relance la construction de l'infra
terraform apply 

## on vérifie les datas de la ressource 
terraform show                          ## l'ami a été modiée et l'instance est à running

## on supprime l'infra
terraform destroy
```



**Define Input variables**

[fichier variables.tf](https://raw.githubusercontent.com/linuxacademy/content-terraform-2021/main/variables_aws.tf)    

```bash
## creation d'un fichier variables.tf dans lequel on definit instance_name comme variable
vi variables.tf
#-------------------------------------------
variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}
#-------------------------------------------

## update de l'instance aws dans le block de resource de main.tf
vi main.tf
#-------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

### creation du provider aws et définition de la region
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

### creation de la ressource à partir de l'ami définie
resource "aws_instance" "app_server" {
  ami           = "ami-0ab4d1ecf9a1215a"
  instance_type = "t2.micro"

  tags = {
    Name = var.instance_name            ## cette ligne a été modifiée
  }
}
#-------------------------------------------

## creation de l'infra
terraform plan
terraform apply
```

On relaner la creation mais en appliquant un flag de variable en lui donnant comme nom "Batman"

`terraform apply -var "instance_name=Batman"`   

  ➡️ en allant voir dans la console aws (ou en faisant `terraform show`) l'instance à comme Batman




**Utiliser les output variables pour des requêtes**

[fichier de variables](https://raw.githubusercontent.com/linuxacademy/content-terraform-2021/main/outputs_aws.tf)    


```bash
## on crée le fichiers de variables output
vi output.tf
#-------------------------------------------
output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}
#-------------------------------------------

## on crée l'infra
terraform apply       ## il y a des changements liées à e nouveau fichier
#------------------------------------------
### dans le terminal s'affiche les outputs
...
Outputs:
instance_id = "-i-xxxx"
instance_public_ip  = "3.xxx.xxx.xxx"
#------------------------------------------

## pour revoir les outputs sans rien faire de plus
terraform output      ## les outputs se réaffiche

## on supprime l'infra
terraform destroy
```


**Conserver les states à distance**    

[fichier de configuration](https://raw.githubusercontent.com/linuxacademy/content-terraform-2021/main/main_aws.tf)    

```bash
## configuration du remote backend
vi main.tf
#------------------------------------------
terraform {
  backend "remote" {                    ## ce bloc est ajouté
    organization  = "ACG-Terraform-Demos"
    workspaces {
      name  = "example-workspace"
    }
   }                                    ## fin du bloc
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "app_server" {
  ami           = "ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
#------------------------------------------

## on se connecte à terraform cloud
terraform login     ## on continue après l'ction définie ci-dessous
```
Depuis l'IHM on crée un API Token que nous allons utiliser (comme fait ulterieurement)


```bash
## on se connecte à terraform cloud
terraform login
#### on colle le token généré
#### ➡️ affiche Welcome to Terraform Cloud

## on initialise le projet
terraform init
### les states vont se conserver maintenant sur terraform cloud même si on le supprime ici 
rm terraform.tfstate
```

Depuis l'IHM Terraform cloud, on va dans notre orgaisation/workspace/variables
On ajoute les variables **AWS_ACCESS_KEY_ID** et **AWS_SECRET_ACCESS_KEY** à partir des infos du sandbox aws    

Là nous avons configurer les variables

```bash
terraform apply     ## se connecte à AWS et crée l'infra + les states sont conservées dans terraform cloud
```

Depuis l'IHM terraform cloud `organisation/workspaces/states`  ➡️ nous voyons nos states


=======================================================================================================================   

<span style="color: #EA9811">

###  **TERRAFORM LAB 5 - USING TERRAFORM PROVIDERS TO SET UP AN APACHE WEB SERVER ON AWS** 
</span>


[tf_lab5-enonce](./images/tf_lab5/tf_lab5-enonce.png)






=======================================================================================================================  

_______________________________________________________________________________________________________________________  

<span style="color: #CE5D6B">

## UTILISATION AVEC UN PROVIDER CLOUD
</span>


=======================================================================================================================  

<span style="color: #EA9811">

### AWS
</span>




=======================================================================================================================  


<span style="color: #EA9811">

### GOOGLE CLOUD
</span>


=======================================================================================================================   

<span style="color: #EA9811">

### AZURE
</span>





=======================================================================================================================  



_______________________________________________________________________________________________________________________   


<span style="color: #CE5D6B">

## CREATION D'UN CLUSTER KUBERNETES
</span>



_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

## UTILISATION AVEC LE PROVIDER VPHERE
</span>




_______________________________________________________________________________________________________________________  


<span style="color: #CE5D6B">

## TERRAFORM CERTIFICATION
</span>



[Les questions des anales des examins de certification terraform sont dans ce répertoire](./images/tf_exam_certif_tests/)


_______________________________________________________________________________________________________________________  