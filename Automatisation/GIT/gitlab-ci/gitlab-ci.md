# GITLAB CICD

## RAPPELS ET DEFINITIONS

- **CI**: **Integration Continue**  -> ensemble de pratiques utilisées consistant à vérifier à chaque modification du code source que le résultat des modifications ne génère pas de regression. Le but est donc de détecter les anomalies au plus tôt et les corriger avant le déploiement.


- **CD**: **Déploiement Continu**   -> livraisons continues avec des cycles courts. Construire, tester, diffuser une application rapidement.




**Gitlab CI** va permettre d'automatiser les `build`, `test`, `deploiement`...    

L'ensemble des **taches (stage)** sont divisées en **job**. L'ensemble des stages est une **pipeline**.    

Chaque tache est exécutée grâce à des **runners**.     


## MISE EN PLACE D'UNE CICD GITLAB

### MANIFEST: fichier .gitlab-ci.yaml 

Ce fichier `.gitlab-ci.yml` se situe à la racine de notre projet dans l'arborescence gitlab.    
Il est possible de le nommer autrement mais il faudra alors dans ce cas modifier dans les settings du projet cela: `Settings > CI/CD > General pipelines > Custom CI config path`

Ce fichier comprend différent mots clés: **job**, **stage**, **variables**....

### MOTS CLES RESERVES

Voilà la liste des mots clés réservés par gitlab -> si ce mot est utilisé il prendra le sens défini pour gitlab et pas celui qu'on veut lui donner.    
https://docs.gitlab.com/ee/ci/yaml/    




### JOB

[Documentation gitlab mots clés: job](https://docs.gitlab.com/ee/ci/yaml/#job-keywords)  

On peut définir un nombre illimité de job dans un manifeste.    

Un **job** definit ce que nous voulons accomplir dans la pipeline    
- executé par les runners
- exécuté en stages 


Les jobs dans chaque stage sont exécutés en parrallèle
- SI tous les jobs d'un stage sont réussis: la pipeline avance vers le prochain stage
- SI 1 job dans 1 stage est failed, le prochain stage ne sera pas exécuté


Dans la définition d'un job SEULE la déclaration de `script` est obligatoire


### SCRIPT

[Documentation gitlab mots clés: script](https://docs.gitlab.com/ee/ci/yaml/#script)    

C'est la déclaration d'un `job`    

Un job peut avoir 1 ou plusieurs scripts à exécuter.


### BEFORE_SCRIPT ET AFTER_SCRIPT

[Documentation gitlab mots clés: after_script](https://docs.gitlab.com/ee/ci/yaml/#after_script)    
[Documentation gitlab mots clés: before_script](https://docs.gitlab.com/ee/ci/yaml/#before_script)


Ces déclarations permettront d'**exécuter des actions avant et après votre script principal**. Ceci peut être intéressant pour bien diviser les actions à faire lors des jobs, ou bien appeler ou exécuter une action avant et après chaque `job`


### IMAGE

[Documentation gitlab mots clés: image](https://docs.gitlab.com/ee/ci/yaml/#image)

On peut déclarer une image en début de manifeste, ainsi cette image sera utilisée pour tout le manifeste ou à l'intérieur d'un job, dans ce cas elle ne sera utilisée que pour ce job.





### STAGE  

[Documentation gitlab mots clés: stage](https://docs.gitlab.com/ee/ci/yaml/#stage)

Un **stage** définit quand et comment exécuter un job
- Des stages peuvent exécuter des tests après que d'autres stages aient compilé le code


On peut aussi définir un job à effectuer OBLIGATOIREMENT avant ou après la 1er ou dernier stage de la pipeline:    
- `.pre`
- `.post`



### ONLY ET EXCEPT

[Documentation gitlab mots clés: only et except](https://docs.gitlab.com/ee/ci/yaml/#only--except)

Ces deux directives permettent de mettre en place des contraintes sur l'exécution d’une tâche. Vous pouvez dire qu’une tâche s'exécutera uniquement sur l’événement d’un push sur main ou s'exécutera sur chaque push d’une branche sauf main.    

Voici les possibilités :

- **branches** déclenche le `job` quand un un `push` est effectué sur la branche spécifiée.
- **tags** déclenche le `job` quand un `tag` est créé.
- **api** déclenche le `job` quand une deuxième `pipeline` le demande grâce à API pipeline.
- **external** déclenche le `job` grâce à un service de CI/CD autre que GitLab.
- **external_pull_requests** déclanche le `job` lorsqu'un pull-request exterieur à gitlab est fait (pour une création ou update)
- **pipelines** déclenche le `job` grâce à une autre pipeline, utile pour les multiprojets grâce à l’API et le token `CI_JOB_TOKEN`.
- **pushes** déclenche le `job`quand un push est effectué par un utilisateur.
- **schedules** déclenche le `job` par rapport à une planification à paramétrer dans l’interface web.
- **triggers** déclenche le `job` par rapport à un jeton de déclenchement.
- **web** déclenche le `job` par rapport au bouton Run pipeline dans l'interface utilisateur.
- **chat** déclenche le `job` pour les pipelines créés par une commande gitlab chatops



### WHEN

[Documentation gitlab mots clés: when](https://docs.gitlab.com/ee/ci/yaml/#when)

Comme pour les directives `only` et `except`, la directive `when` est une contrainte sur l'exécution de la tâche. Il y a 6 modes possibles :

- **on_success** : le job sera exécuté uniquement si tous les jobs du stage précédent sont passés
- **on_failure** : le job sera exécuté uniquement si un job est en échec
- **always** : le job s'exécutera quoi qu'il se passe (même en cas d’échec)
- **manual** : le job s'exécutera uniquement par une action manuelle
- **delayed**: le job sera exécuté après un delai défini
- **never**: le job ne sera jamais exécuté. Ne peut être utilisé que dans une `rule`



### ALLOW_FAILURE

[Documentation gitlab mots clés: allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure)

Cette directive permet d'accepter qu'un job échoue sans faire échouer la pipeline.    
Elle peut prendre comme valeur `true` ou `false`


### TAGS

[Documentation gitlab mots clés: tags](https://docs.gitlab.com/ee/ci/yaml/#tags)

Permet de forcer les jobs à être exécutés sur un runner specifique





### ENVIRONMENT

[Documentation gitlab mots clés: environment](https://docs.gitlab.com/ee/ci/yaml/#environment)


Cette déclaration permet de définir un environnement spécifique au déploiement. Vous pouvez créer un environnement dans l'interface web de GitLab ou tout simplement laisser GitLab CI/CD le créer automatiquement.

Cette declarative a plusieurs mots clés qui peuvent être définis comme par exemple:
- **name**
- **url**
- ... 




### VARIABLES 

[Documentation gitlab mots clés: variables](https://docs.gitlab.com/ee/ci/yaml/#variables)

Cette déclaration permet de définir des variables pour tous les jobs ou pour un job précis. Ceci revient à déclarer des variables d'environnement.






### CACHE

[Documentation gitlab mots clés: cache](https://docs.gitlab.com/ee/ci/yaml/#cache)

Il s'agit d'un cache qui n'existe seulement que pour cette pipeline. Pour des données utiles seulement au cours de cette pipeline (entre différents job)    
Utilisés pour les tempprary files: on peut créer une arborescence avec "paths" pour les conserver -> à la fin de la pipeline tout est détruit




### ARTIFACTS

[Documentation gitlab mots clés: artifact](https://docs.gitlab.com/ee/ci/yaml/#artifacts)

Dans ce cas on peut sauver les fichiers après la pipeline s'ils sont utiles pour le projet    
La dernière pipeline en succès aura placer les fichiers ici


[Documentation Gitlab- jobs artifacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html)           

Un **job artifact** permet de créer un artifact pour 1 job. 1 artifact étant 1 fichier contenant les logs du job (ce qui apparait en temps normal à l'écran lorsque la pipeline est en cours d'exécution)     
Cela peut être très utile dans plusieurs cas comme: conserver les logs, logs trop important (risque pb de ressources du runner, ...)    

Ces fichiers sont ensuite accessibles depuis la console gitlab dans le `projet/Build/Artifacts`         

![gitlabci-artifactsFolder](./images/gitlabci-artifactsFolder.png)        

Pour le code suivant 
```yaml
.terraform_custom_plan:                             ## nom de l'ancre
  extends:
    - .vault-vars
    - .tf-vars
    - .terraform-cache
  script:
    - export PLAN_CACHE="plan-${ENVSHORT}.cache"
    - export PLAN_JSON="plan-${ENVSHORT}.json" 
      artifacts:
    name: plan-$ENVSHORT                            ## nom de l'artifact
    paths:
      - plan-$ENVSHORT.cache                        ## chemin de l'artifact
      - plan-$ENVSHORT.json

# *********************** Review *****************************
Plan DEV Alpha:                                     ## job
  stage: plan
  extends:
    - .terraform_custom_plan                       ## ancre appelée
  variables:
    ENVSHORT: reviewdev                            ## variable utilisée dans le nom de l'artifact et dans son path
    ou_id: toto
    appstackcode: tata
```




### INCLUDE

[Documentation gitlab mots clés: include](https://docs.gitlab.com/ee/ci/yaml/#include)


Cette fonctionnalité permet d'inclure des "templates". les "templates" peuvent être en local dans votre projet ou à distance.



### ANCHORS

[Documentation gitlab yaml: anchors](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#yaml-anchors-for-scripts)

Cette fonctionnalité permet de faire des templates réutilisables plusieurs fois.



## ADVANCED PRACTICES


### RULES

[Documentation gitlab mots clés: rules](https://docs.gitlab.com/ee/ci/yaml/#rules)

Les `rules` sont une directive de `jobs`. Elles permettent de mettre des conditions pour l'exécution du job.

Les `rules` acceptent ces valeurs:
- **if**: déclenche l'ajout ou non du job dans la pipeline (si les conditions sont atteintes)
- **changes**: spécifie **quand** ajouter un job lors d'un modification d'un fichier spécifique
- **exists**: déclenche le job **quand** certain fichiers existent dans le répertoire
- **allow_failure**: autorise un job à "planter" sans stopper la pipeline
- **variables**: en complément de "if" -> définit des variables spécifiques dans les conditions de `rules`
- **when**: 



### NEEDS

[Documentation gitlab mots clés: needs](https://docs.gitlab.com/ee/ci/yaml/#needs)


Les `needs` sont des directives de `jobs` qui "autorisent" l'exécution du job dans le "désordre" (ne pas suivre la liste de haut en bas dans le manifeste)    

On le renseigne avec un tableau ou une liste.    
Il définit une dépendance entre différents jobs (qui ne font pas parti du même stage)





# FIN