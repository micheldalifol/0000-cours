
# COMMANDES GIT 

Git est un logiciel de gestion de versions décentralisé

__________________________________________________________________________________________________________________________            

## GIT INSTALLATION



<table style="width:100%;">
  <thead>
    <tr>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>OS</strong></span></th>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Liens</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:center;width:33.3333%;">LINUX</td>
      <td style="text-align:left;"><ul><li style="text-align:left;">RHEL ou CentOS <code>sudo dnf install git-all</code></li><li style="text-align:left;">Debian, Ubuntu <code>sudo apt install git-all</code></li></ul></td>
      <td style="text-align:center;width:33.3333%;"><a href="https://git-scm.com/download/linux" rel="Doc Git - installation Git Linux" target="_blank">Doc Git - installation Git Linux<span class="visually-hidden-always"></span></a> </td>
    </tr>
    <tr>
      <td style="text-align:center;width:33.3333%;">MacOSX</td>
      <td style="text-align:left;"><code>brew install git</code></td>
      <td style="text-align:center;width:33.3333%;"><a href="https://git-scm.com/downloads/mac" rel="Doc Git - installation Git Linux" target="_blank">Doc Git - installation Git Mac OsX<span class="visually-hidden-always"></span></a> </td>
    </tr>
    <tr>
      <td style="text-align:center;width:33.3333%;">Windows</td>
      <td style="text-align:left;">Il existe de multiples manières d'installer git sur Windows</td>
      <td style="text-align:center;width:33.3333%;"><a href="https://git-scm.com/downloads/win" rel="Doc Git - installation Git Linux" target="_blank">Doc Git - installation Git Windows<span class="visually-hidden-always"></span></a> </td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="width:33.3333%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Validation Installation</strong></span></th>
      <th style="width:33.3333%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:33.3333%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  <tbody>
    <tr>
      <td style="text-align:center;width:33.3333%;">verification de la version de Git installée</td>
      <td style="text-align:left;"><code>git --version</code></td>
      <td style="text-align:center;width:33.3333%;"><code>git version 2.34.1</code></td>
    </tr>
  </thead>
  </tbody>
</table>


__________________________________________________________________________________________________________________________            

## GIT CONFIGURATION


Dans le case de la 1ere configuration sur l'ordi perso, pas besoin de les refaires (conservées lors des mises à jours), pour les modifier, refaire les comamndes avec les nouvelles valeurs: utilisation de `git config`       
Les variables de configuration git peuvent être conservées dans 3 lieux: `[path]/etc/gitconfig` (avec l'option `--system`), `~/.gitconfig` (avec l'option `--global`) ou `.git/config`


<table style="width:100%;">
  <thead>
    <tr>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Actions</strong></span></th>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left;">comparaison avec le fichier <code>~/.gitconfig</code></td>
      <td style="text-align:left;"><code>cat ~/.gitconfig</code></td>
      <td style="text-align:left;"><code>[user]</code></br><code>        name = DALIFOL Michel</code></br><code>        email = michel.dalifol@xxx.com</code></br><code>[http "https://gitlab.xxx/"]</code></br><code>sslCAInfo = ~/certif.pem</code></br><code>[http]</code></br><code>        proxy=localhost:9999</code></br><code>[push]</code></br><code>        default = simple</code></br><code>[core]</code></br><code>        symlinks = false</code></br><code>        compression = 0</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Définir le nom que vous voulez associer à toutes vos opérations de commit</td>
      <td style="text-align:left;"><code>git config --global user.name "[nom]"</code></td>
      <td style="text-align:left;"><code>git config --global user.name "DALIFOL Michel"</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Définit l'email que vous voulez associer à toutes vos opérations de commit</td>
      <td style="text-align:left;"><code>git config --global user.email "[adresse email]"</code></td>
      <td style="text-align:left;"><code>git config --global user.email "michel.dalifol@xxx.com"</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Active la colorisation de la sortie en ligne de commande</td>
      <td style="text-align:left;"><code>git config --global color.ui auto</code></td>
      <td style="text-align:left;"><code>git config --global color.ui auto</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="width:20%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Validation Configuration</strong></span></th>
      <th style="width:30%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:50%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  <tbody>
    <tr>
      <td style="text-align:left;">Voir la config "global" mise en place (donc dans le fichier <code>~/.gitconfig</code>)</td>
      <td style="text-align:left;"><code>git config --global --list</code></td>
      <td style="text-align:left;"><code>file:/home/mdali/.gitconfig     user.name=DALIFOL Michel</code></br><code>file:/home/mdali/.gitconfig     user.email=michel.dalifol@xxx.com</code></br><code>file:/home/mdali/.gitconfig     http.https://gitlab.xxx/.sslcainfo=~/certif.pem</code></br><code>file:/home/mdali/.gitconfig     http.proxy=localhost:9999</code></br><code>file:/home/mdali/.gitconfig     push.default=simple</code></br><code>file:/home/mdali/.gitconfig     core.symlinks=false</code></br><code>file:/home/mdali/.gitconfig     core.compression=0</code></br><code>file:/home/mdali/.gitconfig     color.ui=auto</code></br></td>
    </tr>
  </thead>
  </tbody>
</table>


[Doc Git - config](https://git-scm.com/docs/git-config.html/fr) 


__________________________________________________________________________________________________________________________     

## GIT AIDE 



<table style="width:100%;">
  <thead>
    <tr>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Actions</strong></span></th>
      <th style="width:33.3333%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Comment avoir l'aide git</td>
      <td style="text-align:left;"><ul><li style="text-align:left;"><code>git help [commande]</code></li><li style="text-align:left;"><code>git [commande] --help</code></li><li style="text-align:left;"><code>man git-[commande]</code></li></ul></td>
    </tr>
  </tbody>
</table>




__________________________________________________________________________________________________________________________     

## REPOT GIT 

<table style="width:100%;">
  <thead>
    <tr>
      <th style="background-color:#faacac;text-align:center;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>GIT INITIALISATION</strong></span></th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th style="width:20%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Actions</strong></span></th>
      <th style="width:30%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:50%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left;">En local création d'un repo Test1 avec fichier readme.md</td>
      <td style="text-align:left;"><code>mkdir ~/Test1</code></br><code>cd ~/Test1</code></br><code>echo "Test1" >> README.md</code></br></td>
      <td style="text-align:left;"><code>mkdir ~/Test1</code></br><code>cd ~/Test1</code></br><code>echo "Test1" >> README.md</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Initialisation de git pour le repo</td>
      <td style="text-align:left;"><code>git init</code></td>
      <td style="text-align:left;"><code>git init</code></br>crée le folder .git avec toutes les infos du repo</td>
    </tr>
    <tr>
      <td style="text-align:left;">Definir la branche main comme branche par defaut du repo</td>
      <td style="text-align:left;"><code>git config --global init.defaultBranch [defaultBranchName]</code></td>
      <td style="text-align:left;"><code>git config --global init.defaultBranch main</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Ajout dans le commit du fichier readme.md, commit du repo puis push sur git</td>
      <td style="text-align:left;"><code>git add README.md</code></br><code>git commit -m "first commit"</code></br><code>git branch -M main</code></br><code>git remote add origin https://gitlab.xxx/PATH/Test1.git</code></br><code>git push -u origin main</code></br></td>
      <td style="text-align:left;"><code>git add README.md</code></br><code>git commit -m "first commit"</code></br><code>git branch -M main</code></br><code>git remote add origin https://gitlab.xxx/PATH/Test1.git</code></br><code>git push -u origin main</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Voir les repo distants (sur gitlab par exemple)</td>
      <td style="text-align:left;"><code>git remote -v </code></td>
      <td style="text-align:left;"><code>origin  https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1 (fetch)</code></br><code>origin  https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1 (push)</code></br></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="width:20%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Validation Initialisation: Toutes les informations sur le repo git en local se trouve dans le répertoire <code>.git</code></strong></span></th>
      <th style="width:30%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:50%;background-color:#06bdf8;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  <tbody>
    <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>ll .git</code></td>
      <td style="text-align:left;"><code>844424930913333       0     -rwxrwxrwx 1 mdali mdali   73 Sep  9 11:17 description</code></br><code>844424930913332       0     drwxrwxrwx 1 mdali mdali 4.0K Sep  9 11:17 branches</code></br><code>844424930913334       0     drwxrwxrwx 1 mdali mdali 4.0K Sep  9 11:17 hooks</code></br><code>844424930913348       0     drwxrwxrwx 1 mdali mdali 4.0K Sep  9 11:17 info</code></br><code>844424930913362       0     -rwxrwxrwx 1 mdali mdali  266 Sep  9 11:17 packed-refs</code></br><code>844424930913350       0     drwxrwxrwx 1 mdali mdali 4.0K Sep  9 11:17 refs</code></br><code>844424930913365       0     drwxrwxrwx 1 mdali mdali 4.0K Sep  9 11:17 logs</code></br><code>1407374884334502      0     drwxrwxrwx 1 mdali mdali 4.0K Sep  9 11:17 ..</code></br><code>1970324837755982      0     -rwxrwxrwx 1 mdali mdali  456 Sep  9 11:17 config</code></br><code>2814749767888238      0     -rwxrwxrwx 1 mdali mdali   23 Sep  9 11:17 HEAD</code></br><code>38562071809628170     8.0K  -rwxrwxrwx 1 mdali mdali 4.2K Nov 20 11:09 index</code></br><code>7318349395428315      0     -rwxrwxrwx 1 mdali mdali   52 Nov 20 11:09 COMMIT_EDITMSG</code></br><code>1407374884334643      0     drwxrwxrwx 1 mdali mdali 4.0K Nov 20 11:09 .</code></br><code>1407374884334667      0     drwxrwxrwx 1 mdali mdali 4.0K Nov 20 11:09 objects</code></br></td>
    </tr>
     <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>cd .git</code></br></td>
      <td style="text-align:left;"><code>cd .git</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>cat config</code></br></td>
      <td style="text-align:left;"><code>[core]</code></br><code>repositoryformatversion = 0</code></br><code>filemode = false</code></br><code>bare = false</code></br><code>logallrefupdates = true</code></br><code>ignorecase = true</code></br><code>[remote "origin"]</code></br><code>url = https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1</code></br><code>fetch = +refs/heads/*:refs/remotes/origin/*</code></br><code>[branch "master"]</code></br><code>        remote = origin</code></br><code>        merge = refs/heads/master</code></br><code>[branch "review"]</code></br><code>        remote = origin</code></br><code>        merge = refs/heads/review</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>cat HEAD</code></br></td>
      <td style="text-align:left;"><code>ref: refs/heads/review </code></br>on est sur la branche review</td>
    </tr>
    <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>cat COMMIT_EDITMSG</code></br></td>
      <td style="text-align:left;"><code>modify file for debug</code></br>commentaire du dernier commit fait</td>
    </tr>
    <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>cat heads/master</code></br></td>
      <td style="text-align:left;"><code>14911e8271...15c46e49</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Liste du contenu du répertoire <code>.git</code> dans 1 repo</td>
      <td style="text-align:left;"><code>cat heads/review</code></br></td>
      <td style="text-align:left;"><code>8c41405f...a0709126821</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#f5d869;text-align:center;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>GIT CLONE</strong></span></th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th style="width:20%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Actions</strong></span></th>
      <th style="width:30%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:50%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Clone du repo distant en local -> peut se faire de 2 manières: https ou ssh</br><ul><li>pour ssh il faut d'abord avoir généré une clé ssh et l'avoir partagé avec le gitlab(github...) sur lequel se trouve le repo distant</li><li>dans le cas de https on peut aussi avoir besoin du token</li></ul></td>
      <td style="text-align:left;"><code>git clone https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1</code></br><code>cd ./Test1</code></td>
      <td style="text-align:left;"><code>git clone https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1</code></br><code>cd ./Test1</code></br>On est dans le repo cloné</td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#5cda6d;text-align:center;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>GIT DAY TO DAY</strong></span></th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th style="width:20%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>Actions</strong></span></th>
      <th style="width:30%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Commandes</span></th>
      <th style="width:50%;background-color:#de9ef9;"><span style="color:rgb(255, 255, 255);font-weight:bold;">Exemples</span></th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>STATUS</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Status des fichiers du repo</td>
      <td style="text-align:left;"><code>git status</code></td>
      <td style="text-align:left;"><code>git status</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>BRANCHES</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Liste des branches du repo</td>
      <td style="text-align:left;"><code>git branch -a</code></td>
      <td style="text-align:left;"><code>git branch -a</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Créer une branche</td>
      <td style="text-align:left;"><ul><li>Deux lignes: créer et basculer sur la nouvelle branche</br><code>git branch [nom_de_ma_branch_nouvelle]</code></br><code>git checkout [nom_de_ma_branch_nouvelle]</code></br></li><li>Une seule ligne: créer et basculer</br><code>git checkout -b [nom_de_ma_branch_nouvelle]</code></li></ul></td>
      <td style="text-align:left;"><ul><li>Deux lignes: créer et basculer sur la nouvelle branche</br><code>git branch [nom_de_ma_branch_nouvelle]</code></br><code>git checkout [nom_de_ma_branch_nouvelle]</code></br></li><li>Une seule ligne: créer et basculer</br><code>git checkout -b [nom_de_ma_branch_nouvelle]</code></li></ul></td>
    </tr>
    <tr>
      <td style="text-align:left;">Supprimer une branche</td>
      <td style="text-align:left;"><ul><li>Si la branch est local et n'est pas créée sur le repo distant</br><code>git branch -d  [nom_de_ma_branch_locale]</code></br></li><li>Si la branch est présente sur le repo distant</br><code>git push origin --delete [nom_de_ma_branch_distante]</code></li></ul></td>
      <td style="text-align:left;"><ul><li>Si la branch est local et n'est pas créée sur le repo distant</br><code>git branch -d  [nom_de_ma_branch_locale]</code></br></li><li>Si la branch est présente sur le repo distant</br><code>git push origin --delete [nom_de_ma_branch_distante]</code></li></ul></td>
    </tr>
    <tr>
      <td style="text-align:left;">Changer de branche</td>
      <td style="text-align:left;"><code>git checkout [nom_de_ma_branch]</code></td>
      <td style="text-align:left;"><code>git checkout [nom_de_ma_branch]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>COMMIT</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Premier commit</td>
      <td style="text-align:left;"><code>git add .</code></br><code>git commit -m "initial commit"</code></br></td>
      <td style="text-align:left;"><code>git add .</code></br>ajoute TOUS les fichiers au commit</br><code>git commit -m "initial commit"</code></br>Message à modifier pour chaque commit</br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Autre commits</td>
      <td style="text-align:left;"><code>git add [NomFichierComplet]</code></br><code>git commit -m "message du commit"</code></br></td>
      <td style="text-align:left;"><code>git add [NomFichierComplet]</code></br>ajoute le(s) fichier(s) au commit</br><code>git commit -m "message du commit"</code></br>Cette commande est utilisée pour enregistrer vos modifications dans le référentiel local, et non pour synchroniser ou écraser les modifications locales avec celles du référentiel</td>
    </tr>
    <tr>
      <td style="text-align:left;">Annuler le dernier commit</td>
      <td style="text-align:left;"><code>git reset HEAD</code></br></td>
      <td style="text-align:left;"><code>git reset HEAD</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Annuler l'avant dernier commit</td>
      <td style="text-align:left;"><code>git reset HEAD^</code></br></td>
      <td style="text-align:left;"><code>git reset HEAD^</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Annuler l'avant avant dernier commit</td>
      <td style="text-align:left;"><code>git reset HEAD~2</code></br></td>
      <td style="text-align:left;"><code>git reset HEAD~2</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Annuler un commit particulier</td>
      <td style="text-align:left;"><code>git reset [commitID]</code></br></td>
      <td style="text-align:left;"><code>git reset d6d9892</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Annuler le dernier commit et force le push</td>
      <td style="text-align:left;"><code>git reset --hard md5_commit</code></br><code>git push --force</code></br></td>
      <td style="text-align:left;"><code>git reset --hard md5_commit</code></br><code>git push --force</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Antidater un commit</td>
      <td style="text-align:left;"><code>git add .</code></br><code>GIT_AUTHOR_DATE="2015-12-12 08:32 +100" git commit -m "Commit antidaté"</code></br></td>
      <td style="text-align:left;"><code>git add .</code></br><code>GIT_AUTHOR_DATE="2015-12-12 08:32 +100" git commit -m "Commit antidaté"</code></br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Annuler/Supprimer un fichier avant un commit: Supposer que vous venez d’ajouter un fichier à Git avec <code>git add</code> et que vous vous apprêtez à le « commiter ». Cependant, vous vous rendez compte que ce fichier est une mauvaise idée et vous voulez annuler votre <code>git add</code></td>
      <td style="text-align:left;"><code>git reset HEAD -- nom_du_fichier_a_supprimer</code></br><code></td>
      <td style="text-align:left;"><code>git reset HEAD -- nom_du_fichier_a_supprimer</code></br><code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Modifier le message du dernier commit</td>
      <td style="text-align:left;"><code>git commit --amend</code></br><code></td>
      <td style="text-align:left;"><code>git commit --amend</code></br><code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Ajouter des fichiers dernier commit</td>
      <td style="text-align:left;"><code>git add [monFichier]</code></br><code><code>git commit --amend</code></br><code></td>
      <td style="text-align:left;"><code>git add [monFichier]</code></br><code><code>git commit --amend</code></br><code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>DIFF</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Afficher la différence entre le contenu du dernier commit et celui du répertoire de travail</td>
      <td style="text-align:left;"><code>git diff HEAD</code></td>
      <td style="text-align:left;"><code>git diff HEAD</code></br>Si pas de difference, rien ne s'affiche</td>
    </tr>
    <tr>
      <td style="text-align:left;">Afficher la différence entre le contenu pointé par A et celui pointé par B</td>
      <td style="text-align:left;"><code>git diff A B</code></td>
      <td style="text-align:left;"><code>git diff A B</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Afficher la différence entre un dossier présent sur deux branches</td>
      <td style="text-align:left;"><code>git diff [source_branch] [target_branch]</code></td>
      <td style="text-align:left;"><code>git diff [source_branch] [target_branch]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>PULL / PUSH</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Mettre à jour un dépot local par rapport au dépot distant</td>
      <td style="text-align:left;"><code>git pull</code></td>
      <td style="text-align:left;"><code>git pull</code></br>Cette commande récupère et fusionne les modifications du référentiel distant vers la branche locale</br></td>
    </tr>
    <tr>
      <td style="text-align:left;">Mettre à jour une branche sécifique d'un dépot local</td>
      <td style="text-align:left;"><code>git pull origin [MA_BRANCHE]</code></td>
      <td style="text-align:left;"><code>git pull origin [MA_BRANCHE]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Envoyer les commits vers le dépot distant</td>
      <td style="text-align:left;"><code>git push</code></td>
      <td style="text-align:left;"><code>git push</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Envoyer les commits d'une branche spécifique vers le dépot distant</td>
      <td style="text-align:left;"><code>git push origin [MA_BRANCHE]</code></td>
      <td style="text-align:left;"><code>git push origin [MA_BRANCHE]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>MOVE</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Renommer un fichier</td>
      <td style="text-align:left;"><code>git mv [nom_du_fichier] [nouveau-nom-fichier]</code></td>
      <td style="text-align:left;"><code>git mv [nom_du_fichier] [nouveau-nom-fichier]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>REMOVE</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Supprimer un fichier du répertoire de travail et de l'index</td>
      <td style="text-align:left;"><code>git rm [nom_du_fichier]</code></td>
      <td style="text-align:left;"><code>git rm [nom_du_fichier]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Supprimer le fichier du système de suivi de version mais le préserve localement</td>
      <td style="text-align:left;"><code>git rm --cached [fichier]</code></td>
      <td style="text-align:left;"><code>git rm --cached [fichier]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>REBASE</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Éditer l'historique avec rebase</td>
      <td style="text-align:left;"><code>git rebase -i HEADxx</code></td>
      <td style="text-align:left;"><code>git rebase -i HEAD~3</code></br>Historique des trois derniers commits</br>Chaque commit est précédé du mot "pick", utiliser tous les mots-clés possibles</br>Pour éditer, un commit changer "pick" par "edit"</br>Enregistrer et quitter l'éditeur afin de prendre les modifs en compte</br>Et suivre les instructions</td>
    </tr>
    <tr>
      <td style="text-align:left;">Pour valider les changements</td>
      <td style="text-align:left;"><code>git rebase [NOM_DE_MA_BRANCHE]</code></td>
      <td style="text-align:left;"><code>git rebase [NOM_DE_MA_BRANCHE]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Dans le cas, ou les commits modifier sont déjà présent sur la branche distante, il faudra "forcer"</td>
      <td style="text-align:left;"><code>git push --force origin [NOM_DE_MA_BRANCHE]</code></td>
      <td style="text-align:left;"><code>git push --force origin [NOM_DE_MA_BRANCHE]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>LOGS</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Afficher les logs</td>
      <td style="text-align:left;"><code>git log</code></td>
      <td style="text-align:left;"><code>git log</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Afficher les X derniers commits</td>
      <td style="text-align:left;"><code>git log -n X</code></td>
      <td style="text-align:left;"><code>git log -n 5</code></br>Affiche les 5 derniers commits</td>
    </tr>
    <tr>
      <td style="text-align:left;">Afficher les commits concernant un dossier</td>
      <td style="text-align:left;"><code>git log --oneline -- [chemin/vers/mon_dossier]</code></td>
      <td style="text-align:left;"><code>git log --oneline -- [chemin/vers/mon_dossier]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Afficher un ensemble de commits par date</td>
      <td style="text-align:left;"><code>git log --since=[date] --until=[date]</code></td>
      <td style="text-align:left;"><code>git log --since=[date] --until=[date]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Représentation de l’historique à partir de HEAD (commit / branch)</td>
      <td style="text-align:left;"><code>git log --oneline --graph --decorate</code></td>
      <td style="text-align:left;"><code>git log --oneline --graph --decorate</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Représentation de l’historique à partir d'un fichier (commit / branch)</td>
      <td style="text-align:left;"><code>git log --oneline --graph --decorate [nom_du_fichier]</code></td>
      <td style="text-align:left;"><code>git log --oneline --graph --decorate [nom_du_fichier]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Afficher les logs de validation avec des output différents pour chaque validation</td>
      <td style="text-align:left;"><code>git log --patch</code></td>
      <td style="text-align:left;"><code>git log --patch</code></br> Affiche les entrées du log et les correctifs introduits par chaque commit. Cette commande est idéale pour examiner de manière détaillée les modifications que chaque commit a apportées aux fichiers</td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>RELEASE</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Créer une release</td>
      <td style="text-align:left;"><code>git checkout -b release/[aaaammyy_xx_dev]</code></td>
      <td style="text-align:left;"><code>git checkout -b release/[aaaammyy_xx_dev]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>TAG</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Lister les Tags</td>
      <td style="text-align:left;"><code>git tag</code></td>
      <td style="text-align:left;"><code>git tag</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Il est recommandé d'utiliser le tag avec les releases</td>
      <td style="text-align:left;"><code>git tag [numVersionTag] [IDduTag]</code></td>
      <td style="text-align:left;"><code>git tag [numVersionTag] [IDduTag]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>MERGE</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Fusioner la branche en cours avec l'historique</td>
      <td style="text-align:left;"><code>git merge [branche]</code></td>
      <td style="text-align:left;"><code>git merge [branche]</code></br>Cette commande est utilisée pour combiner les modifications de deux branches et n'écarte pas automatiquement les modifications locales en faveur de la version du référentiel</td>
    </tr>
    <tr>
      <td style="text-align:left;">Fusioner la branche1 vers la branche2</td>
      <td style="text-align:left;"><code>git merge [branche1] [branche2]</code></td>
      <td style="text-align:left;"><code>git merge [branche1] [branche2]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>FETCH</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Récupèrer les données d'un repos distant SANS les ajouter sur notre repo local</td>
      <td style="text-align:left;"><code>git fetch origin</code></td>
      <td style="text-align:left;"><code>git fetch origin</code></br>git fetch télécharge les commits, les fichiers et les références d'un dépôt distant vers votre dépôt local . La récupération est ce que vous faites lorsque vous voulez voir sur quoi tout le monde a travaillé.</td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>RESET</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Supprimer les fichiers de l'index</td>
      <td style="text-align:left;"><code>git reset [fichier]</code></td>
      <td style="text-align:left;"><code>git reset [fichier]</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Supprimer tout l'historique et les modifications effectuées après le commit spécifié</td>
      <td style="text-align:left;"><code>git reset --hard [commit]</code></td>
      <td style="text-align:left;"><code>git reset --hard [commit]</code></br>Cette commande réinitialise de force le HEAD de la branche actuelle à la validation spécifiée, généralement la dernière validation de la branche distante, ignorant toutes les modifications locales dans le répertoire de travail et l'index</td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>RESTORE</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Désindexer un fichier indexé</td>
      <td style="text-align:left;"><code>git restore --staged [fichierADesindexer]</code></td>
      <td style="text-align:left;"><code>git restore --staged [fichierADesindexer]</code></td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>REMOTES</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Inspecter un repo distant</td>
      <td style="text-align:left;"><code>git remote show origin</code></td>
      <td style="text-align:left;"><code>git remote show origin</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Ajouter un nouveau repo distant Git comme nomCourt de reference</td>
      <td style="text-align:left;"><code>git remote add [nomcourt] [url]</code></td>
      <td style="text-align:left;"><code>git remote add pb https://github.com/user2/projet1</code></br><code>git remote -v</code></br>affiche: <code>pb https://github.com/user2/projet1</code></br><code>git fecth pb</code></br>fait l'action souhaitée: récupérer les infos du user2 sur le projet1 sans le mettre sur mon user du projet1</td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>STASH</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Enregistrer de manière temporaire tous les fichiers sous suivi de version qui ont été modifiés ("remiser son travail")</td>
      <td style="text-align:left;"><code>git stash</code></td>
      <td style="text-align:left;"><code>git stash</code></br>Git stash agit comme un outil de contrôle de version et permet aux développeurs de travailler sur d'autres activités ou de changer de branche dans Git sans avoir à abandonner ou valider les modifications qui ne sont pas prêtes . Les développeurs peuvent simplement stocker les modifications dans leur répertoire de travail et leur état d'index, et travailler dessus plus tard.</td>
    </tr>
    <tr>
      <td style="text-align:left;">Appliquer une remise et la supprimer immédiatement</td>
      <td style="text-align:left;"><code>git stash pop</code></td>
      <td style="text-align:left;"><code>git stash pop</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Lister toutes les remises</td>
      <td style="text-align:left;"><code>git stash list</code></td>
      <td style="text-align:left;"><code>git stash list</code></td>
    </tr>
    <tr>
      <td style="text-align:left;">Supprimer la remise la plus recente</td>
      <td style="text-align:left;"><code>git stash drop</code></td>
      <td style="text-align:left;"><code>git stash drop</code></td>
    </tr>
</tbody>
  <thead>
    <tr>
      <th style="background-color:#a9dcf9;text-align:letf;"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>CHERRY-PICK</strong></span></th>
    </tr>
  </thead>
  <thead>
  <tbody>
    <tr>
      <td style="text-align:left;">Sélectionner un commit d'une branche et de l'appliquer à une autre</td>
      <td style="text-align:left;"><code>git cherry-pick [nomDeLaBranche]</code></td>
      <td style="text-align:left;"><code>git cherry-pick [nomDeLaBranche]</code></br>Cette commande permet au développeur d'appliquer des commits spécifiques d'une branche à une autre, les aidant ainsi à créer de nouvelles branches avec les modifications sélectionnées</br>utile pour annuler des changements</br>La cherry-picking (sélection sélective) permet d'appliquer des validations spécifiques à différentes branches, ce qui garantit que le correctif est intégré à la fois à la branche production et la branche feature sans fusionner toutes les modifications divergentes de l'une ou l'autre branche</td>
    </tr>
  </tbody>
</table>



## DEPOT GIT - MISE A JOUR TOKEN

```bash
##----------------------------------------------------------------
## aller dans le repo
cd [nomRepo] 
#-----------------------------------------------
cd repo_1
#-----------------------------------------------

## affiche les urls avec le token en cours 
git remote -v 	
#========================= exemple =========================
origin  https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1.git (fetch)
origin  https://DALIFOL:TOKEN@gitlab.xxx/PATH/Test1.git (push)
#========================= exemple =========================

## modifie les datas avec le nouveau token 
git remote set-url origin  [url avec nouveau token: TOKEN2] 


## vérifie que le nouveau token soit pris en compte
git remote -v 
#========================= exemple =========================
origin  https://DALIFOL:TOKEN2@gitlab.xxx/PATH/Test1.git (fetch)
origin  https://DALIFOL:TOKEN2@gitlab.xxx/PATH/Test1.git (push)
#========================= exemple =========================


##----------------------------------------------------------------
```




============================================================================================================================================================      ============================================================================================================================================================      

# MEMO COMMANDES ET EXEMPLES 

============================================================================================================================================================      ============================================================================================================================================================      



____________________________________________________________________________________________________________________          

## COPIE DE FICHIERS D'UNE BRANCHE A UNE AUTRE 


On peut faire une copie d'1 ou plusieurs fichiers d'une branche à une autre dans un même repo git   

Pour cela  

```bash
## aller dans la branche cible 
git checkout <targetBranch>

## exécuter la commande suivante
git checkout <branchWithFilesToCopy> -- path/to/your/file

### example
git checkout dev -- config/*.yml      ## copie tous les fichiers yml qui sont dans le répertoire config/
```





____________________________________________________________________________________________________________________       

## GESTION DES CONFLITS LORS D'UN MERGE REQUEST   



```bash
## suppression de la branch alpha en local
git branch -D alpha
#----------------------------------------------------------------------------------------------------
Deleted branch alpha (was c028c24).
#----------------------------------------------------------------------------------------------------

## suppression de la branch review en local
git branch -D review
#----------------------------------------------------------------------------------------------------
error: Cannot delete branch 'review' checked out at '/toto/tata'
#----------------------------------------------------------------------------------------------------

### erreur car nous sommes sur la branche review


## changement de branche vers la branche master
git checkout master
#----------------------------------------------------------------------------------------------------
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
#----------------------------------------------------------------------------------------------------

## suppression de la branch review en local
git branch -D review
#----------------------------------------------------------------------------------------------------
Deleted branch review (was 2b174ce).
#----------------------------------------------------------------------------------------------------


## verif des branches en local 
git branch
#----------------------------------------------------------------------------------------------------
* master
#----------------------------------------------------------------------------------------------------

## upgrade du repo 
git pull
#----------------------------------------------------------------------------------------------------
Already up to date.
#----------------------------------------------------------------------------------------------------

## changement de branche -> alpha
git checkout alpha
#----------------------------------------------------------------------------------------------------
Branch 'alpha' set up to track remote branch 'alpha' from 'origin'.
Switched to a new branch 'alpha'
#----------------------------------------------------------------------------------------------------

## upgrade du repo 
git pull
#----------------------------------------------------------------------------------------------------
Already up to date.
#----------------------------------------------------------------------------------------------------

## changement de branche -> review
git checkout review
#----------------------------------------------------------------------------------------------------
Branch 'review' set up to track remote branch 'review' from 'origin'.
Switched to a new branch 'review'
#----------------------------------------------------------------------------------------------------

## upgrade du repo 
git pull
#----------------------------------------------------------------------------------------------------
Already up to date.
#----------------------------------------------------------------------------------------------------



## verif des branches en local 
git branch
#----------------------------------------------------------------------------------------------------
  alpha
  master
* review
#----------------------------------------------------------------------------------------------------


## recup pour un Merge Request de la branche alpha sur la branche review
git pull origin alpha
#----------------------------------------------------------------------------------------------------
From https://gitlab.com/toto/tata
 * branch            alpha      -> FETCH_HEAD
hint: You have divergent branches and need to specify how to reconcile them.
hint: You can do so by running one of the following commands sometime before
hint: your next pull:
hint:
hint:   git config pull.rebase false  # merge (the default strategy)
hint:   git config pull.rebase true   # rebase
hint:   git config pull.ff only       # fast-forward only
hint:
hint: You can replace "git config" with "git config --global" to set a default
hint: preference for all repositories. You can also pass --rebase, --no-rebase,
hint: or --ff-only on the command line to override the configured default per
hint: invocation.
fatal: Need to specify how to reconcile divergent branches.
#----------------------------------------------------------------------------------------------------


git status
#----------------------------------------------------------------------------------------------------
On branch review
Your branch is up to date with 'origin/review'.

nothing to commit, working tree clean
#----------------------------------------------------------------------------------------------------

## KO


## update du git rebase sur la branche review 
git config pull.rebase false


## recup pour un Merge Request de la branche alpha sur la branche review
git pull origin alpha
#----------------------------------------------------------------------------------------------------
From https://gitlab.xxx.com/toto
 * branch            alpha      -> FETCH_HEAD
Auto-merging .gitlab-ci.yml
Auto-merging 04_locals.tf
Auto-merging 05_variables.tf
CONFLICT (content): Merge conflict in 05_variables.tf
CONFLICT (file location): toto.json added in HEAD inside a directory that was renamed in c028c24ea0cab, suggesting it should perhaps be moved to toto01.json.

...
Automatic merge failed; fix conflicts and then commit the result.
#----------------------------------------------------------------------------------------------------

## beaucoup de conflits à gérer avant de pouvoir faire le merge 



## vérification des conflits 
git status
#----------------------------------------------------------------------------------------------------
On branch review
Your branch is up to date with 'origin/review'.

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Changes to be committed:
     ...

	#============================================================
	## tous les fichiers à gérer 1 à 1 pour pouvoir faire le MR 
	#============================================================

Unmerged paths:
    <liste des fichiers pour lesquels il faut faire une modification>
#----------------------------------------------------------------------------------------------------
```

