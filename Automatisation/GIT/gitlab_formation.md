# GITLAB FORMATION

## GITLAB FONCTIONNEMENT

![gitlabFormation-fonctionnementGitlab](./images/gitlabFormation-fonctionnementGitlab.png)              


[site commandes git utiles](https://dangitgit.com/fr#:~:text=%C3%87a%20sert%20pour%20r%C3%A9cup%C3%A9rer%20une,o%C3%B9%20les%20choses%20fonctionnaient%20correctement.)                     


## GITLAB FEATURES 

De nombreuses fonctionalités devsecops sont mises en place dans gitlab pour permettre une collaboration complete entre les Managers, PO, Dev, Sec, Ops, Compliance

[Doc gitlab - Gitlab Features](https://about.gitlab.com/features)           

--------------------------------------------------------------------------------------------------------------------     

### PLANNING


..................................................................................................................         

#### TEAM PLANNING

Un large éventail de fonctionnalités de planification flexibles vous permettent de planifier, d'organiser et de suivre les progrès de l'équipe sur la plateforme sur laquelle vous créez le logiciel.     


..................................................................................................................         

#### SERVICE DESK

Offrez aux équipes des capacités de centre de services et un suivi des problèmes intégré au sein du flux de travail DevSecOps pour permettre une boucle de rétroaction transparente et efficace.      

La fonctionnalité **Service Desk** est utilisée pour gérer les tickets d'assistance par courrier électronique entrants dans GitLab, mais ne concerne pas la gestion des autorisations au sein d'un workspace     

..................................................................................................................         

#### PORTFOLIO MANAGEMENT

Gérez des initiatives complexes à l'aide d'epics, de tableaux, de feuilles de route, de jalons et bien plus encore au sein de la plateforme sur laquelle vos équipes créent des logiciels           

..................................................................................................................         

#### DESIGN MANAGEMENT

Rationalisez les révisions de conception et centralisez les actifs du projet pour favoriser une collaboration transparente au sein des équipes interfonctionnelles.        

..................................................................................................................         

#### REQUIREMENTS MANAGEMENT (forfait ultimate)

Assurez-vous que vos produits répondent aux normes de l'industrie et aux objectifs commerciaux en utilisant les exigences     

..................................................................................................................         

#### QUALITY MANAGEMENT (forfait ultimate)

Les tests de qualité garantissent que vous répondez aux attentes de vos utilisateurs. Planifiez et suivez les tests et la qualité de votre produit, gérez les résultats des tests et créez des éléments d'action sur une seule plateforme.     

..................................................................................................................         

#### WIKI

Un wiki intégré dans chaque projet facilite le partage et la gestion de la documentation dans le même projet que votre code.     

Les **wikis** prennent en charge le contrôle de version, ce qui permet aux membres de l'équipe de consulter l'historique des modifications et des mises à jour de la documentation, aidant ainsi à comprendre l'évolution du projet lors de la révision du code et de la collaboration      


..................................................................................................................         

#### PAGES

Gérez et déployez facilement des sites Web à l'aide de n'importe quel générateur de site statique avec GitLab.         

..................................................................................................................         

#### TEXT EDITORS

Il existe 2 éditeurs de texte disponibles dans GitLab, l'éditeur de texte enrichi (RTE) et l'éditeur de texte brut (PTE).     
Le PTE nécessite une connaissance de Markdown.      
Le RTE est un éditeur WYSIWYG (ce que vous voyez est ce que vous obtenez) pour le contenu Markdown.       
Les 2 éditeurs de texte sont la pierre angulaire de la vision du groupe Knowledge visant à aider GitLab à devenir une plateforme AllOps.     


..................................................................................................................         

#### MARKDOWN

Markdown est un langage de balisage que vous pouvez utiliser pour ajouter des éléments de formatage aux documents en texte brut.     
Lorsque vous saisissez du texte dans l'interface utilisateur de GitLab, GitLab suppose que le texte est dans le langage Markdown. Le texte est rendu avec un ensemble de styles. Ces styles sont appelés [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html). GitLab Flavored Markdown est un surensemble de Markdown standard.

..................................................................................................................         

#### VALUE STREAM MANAGEMENT

Suivez les indicateurs clés tout au long du cycle de vie du développement logiciel, évaluez l’impact des améliorations des processus et identifiez les obstacles. Comparez les meilleures pratiques entre les équipes pour améliorer le flux de travail et offrir plus rapidement de la valeur client.

..................................................................................................................         

#### DORA METRICS (forfait ultimate)

La surveillance et la visualisation des métriques DORA permettent aux équipes de prendre des décisions basées sur les données et d'améliorer les performances DevOps.     

..................................................................................................................         

#### DEVOPS REPORTS

Découvrez comment les équipes de votre organisation collaborent sur GitLab et mesurez l'efficacité DevOps.    


..................................................................................................................         

--------------------------------------------------------------------------------------------------------------------     

### SOURCE CODE MANAGEMENT

..................................................................................................................         

#### SOURCE CODE MANAGEMENT

La gestion du code source permet aux équipes de collaborer, de coordonner et de partager du code en toute sécurité, leur permettant ainsi de fournir des logiciels plus rapidement.    


..................................................................................................................         

#### CODE REVIEW WORKFLOW

Révisez le code, discutez des modifications, partagez vos connaissances et identifiez les défauts du code parmi les équipes distribuées via des commentaires et des révisions asynchrones. Automatisez, suivez et signalez les révisions de code, et identifiez les améliorations du flux de travail grâce aux analyses de révision de code.


..................................................................................................................         

#### GITLAB CLI

L'interface de ligne de commande (CLI) de GitLab est un outil unifié qui vous permet de gérer et d'interagir avec GitLab directement à partir de la ligne de commande.

..................................................................................................................         

#### WEB IDE

Contribuez facilement directement depuis le navigateur avec tout ce dont vous avez besoin en utilisant un environnement de développement intégré (IDE) complet. GitLab Duo, notre suite de fonctionnalités basée sur l'IA, est accessible dans l'IDE Web.

..................................................................................................................         

#### WORKSPACES

Accélérez et standardisez les flux de travail des développeurs avec des workspaces : des environnements virtuels sécurisés avec des bibliothèques, des dépendances et des outils personnalisables pour chaque projet.

Un **workspace** dans GitLab fournit un emplacement centralisé où les administrateurs peuvent gérer tous les projets et groupes, rationalisant ainsi la surveillance et la gouvernance dans l'ensemble de l'organisation      

Un **workspace** dans GitLab permet une gestion consolidée, fournissant une vue unifiée pour les rapports et les analyses sur tous les projets et groupes inclus, facilitant ainsi une meilleure prise de décision et une meilleure surveillance      



..................................................................................................................         

#### PROTECTED ENVIRONMENT

Les **Protected environments** vous permettent de restreindre les personnes pouvant effectuer un déploiement dans des environnements spécifiques, tels que la production, appliquant ainsi des contrôles d'accès stricts comme l'exige le scénario      



..................................................................................................................         

#### EDITOR EXTENSIONS

Les extensions et plugins de l'éditeur apportent GitLab Duo, notre suite de fonctionnalités basée sur l'IA, et d'autres fonctionnalités GitLab directement dans votre environnement de développement local.

..................................................................................................................         

#### CODE SUGGESTIONS (forfait premium)

AI Assistant pour des suggestions de codage proactives et des saisies semi-automatiques

..................................................................................................................         

--------------------------------------------------------------------------------------------------------------------     

### CONTINUOUS INTEGRATION


..................................................................................................................         

#### 

..................................................................................................................         

--------------------------------------------------------------------------------------------------------------------     

### SECURITY





--------------------------------------------------------------------------------------------------------------------     

### ARTIFACT REGISTRY





--------------------------------------------------------------------------------------------------------------------     

### CONTINUOUS DELIVERY






--------------------------------------------------------------------------------------------------------------------     

### OBSERVABILITY






--------------------------------------------------------------------------------------------------------------------     

### COMPLIANCE






--------------------------------------------------------------------------------------------------------------------     

## WORKFLOW GITLAB

![gitlabFormation-basicGitFlow](./images/gitlabFormation-basicGitFlow.png)                

--------------------------------------------------------------------------------------------------------------------     

### COMPOSANTS DU WORKFLOW GITLAB

<table><thead><tr><th><strong>GitLab Component</strong></th><th><strong>Function</strong></th><th><strong>Also Known As...</strong></th></tr></thead><tbody><tr><td><span style="color:rgb(247, 150, 70)"><strong>Project</strong></span></td><td>L'élément de base où le travail est organisé, géré, suivi et livré pour aider l'équipe à collaborer et à planifier le travail sous forme de issues</td><td><strong>Repository</strong></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Group</strong></span></td><td>Une collection de projets et/ou d’autres groupes. Ce sont comme des dossiers.</td><td><strong>Project</strong></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Issue</strong></span></td><td>Un issue fait partie d’un projet. Il s'agit de l'objet de planification fondamental dans lequel l'équipe documente le cas d'utilisation dans la description, discute de l'approche, estime la taille/l'effort (poids de l'issue), suit le temps/l'effort réel, attribue le travail et suit les progrès.</td><td><strong>Story, Narrative, Ticket</strong></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Epic</strong></span></td><td>Une collection de issues connexes dans différents groupes et projets pour aider à organiser par thème<strong><br></strong></td><td><strong>Initiatives, Themes</strong><span style="color:rgb(247, 150, 70)"><strong><br></strong></span></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Merge Request</strong></span></td><td>Le lien entre le issue et le code réel change. Capture la conception, les détails de mise en œuvre (modifications de code), les discussions (révisions de code), les approbations, les tests (CI Pipeline) et les analyses de sécurité.<strong><br></strong></td><td><strong>Pull Request</strong><span style="color:rgb(247, 150, 70)"><strong><br></strong></span></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Label</strong></span></td><td>Utilisé pour marquer et suivre le travail d'un projet ou d'un groupe et associer les issues à différentes initiatives<strong><br></strong></td><td><strong>Tag</strong></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Board</strong></span></td><td>Une liste visuelle des projets et des issues utiles aux équipes pour gérer leur arriéré de travail, hiérarchiser les éléments et déplacer les issues vers l'équipe ou une étape spécifique du projet..<strong><br></strong></td><td><strong>Kanban</strong></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Milestone</strong></span></td><td>Un sprint ou un ou plusieurs livrables, vous aidant à organiser le code, les issues et les demandes de fusion dans un groupe cohérent<strong><br></strong></td><td><strong>Release</strong><span style="color:rgb(247, 150, 70)"><strong><br></strong></span></td></tr><tr><td><span style="color:rgb(247, 150, 70)"><strong>Roadmap</strong></span></td><td>Une représentation visuelle des différentes epics pour le groupe</td><td></td></tr></tbody></table>



--------------------------------------------------------------------------------------------------------------------     

### ETAPES DU WORKFLOW GITLAB


![gitlabFormation-etapesGitlabFlow](./images/gitlabFormation-etapesGitlabFlow.png)                   


--------------------------------------------------------------------------------------------------------------------     

## ORGANIZATIONS DE GITLAB

Gitlab possede une politique de droits qui suit le schéma suivant: 


![gitlabFormation-politiqueDroits](./images/gitlabFormation-politiqueDroits.png)                


--------------------------------------------------------------------------------------------------------------------     

### GITLAB PROJECTS AND GROUPS VISIBILITY


..................................................................................................................         

#### PRIVATE

Seuls les membres du projet ou du groupe peuvent:         
 - cloner le projet
 - voir le repertoire public `/public`    

Un user avec le role `Guest` NE PEUT PAS cloner le projet

Un private group NE PEUT avoir QUE des private subgroups   


..................................................................................................................         

#### INTERNAL

Tous les users authentifiés (y compris les guests) peuvent:     
 - cloner le projet
 - voir le repertoire public `/public`  

Seulement les membres internes peuvent voir le contenu interne    

Les users externes NE PEUVENT PAS cloner le projet     

Les internal groups peuvent avoir des internal ou private subgroups


..................................................................................................................         

#### PUBLIC

Tout le monde peut: 
 - cloner le projet
 - voir le repertoire public `/public`  

Les public groups peuvent avoir des public, internal ou private subgroups

..................................................................................................................    

--------------------------------------------------------------------------------------------------------------------     

### GITLAB EPICS 

- Les Epics sont définis au niveau du Groupe
- Les Epics peuvent contenir à la fois des issues et des Epics en tant qu'enfants
- Les Epics peuvent être utilisés comme filtre dans les listes de issues et les issue boards
- Les Epics offrent une visibilité sur les Epics enfants, les statuts des issues et la chronologie de la roadmap
- Epics n'a pas de assignee
- Les Epics ne peuvent pas être créées dans les projets


Vous pouvez même opter pour des epics confidentielles         
Les epics confidentielles ne sont visibles que par les membres d'un projet disposant des autorisations suffisantes         
Les epics confidentielles peuvent être utilisées par des projets open source et des entreprises pour garder les vulnérabilités de sécurité privées ou empêcher la fuite d'informations sensibles         


Les Epics sont idéaux pour gérer des initiatives à grande échelle sur plusieurs projets, permettant d'organiser les problèmes et les milestones sous un seul objectif global, parfaits pour suivre les dépendances complexes et les déploiements progressifs.

--------------------------------------------------------------------------------------------------------------------     

### GITLAB ISSUES 


Les issues sont les éléments de base du travail de développement de planification et sont utilisés pour collaborer sur des idées et des travaux de planification dans GitLab         

Un issue doit être créé pou chaque: 
 - modification de code
 - ajout d'une nouvelle fonctionalité
 - discuter de la mise en œuvre d’une nouvelle idée
 - gérer un incident
 - poser des questions
 - signaler des bugs

Les issues sont définis dans le cadre d'un projet et non d'un groupe        



--------------------------------------------------------------------------------------------------------------------     

### GITLAB LABELS

Les labels constituent un moyen puissant et flexible de classer les Epics, les issues et les merge request

Le scope d'un label est :        
 - definir un status
 - support du workflow
 - découpe les items en situations (soit/soit)


Les labels sont utiles pour catégoriser et filtrer les problèmes et fusionner les demandes entre les projets, mais ils ne fournissent pas les capacités de planification hiérarchique et de suivi nécessaires à la gestion de programmes complexes


--------------------------------------------------------------------------------------------------------------------     

## GITLAB CI/CD


**Intégration continue**

Avec l'intégration continue (CI), les développeurs partagent leur nouveau code avec une branche de fonctionnalités dans une demande de merge.    
Cela déclenche un pipeline CI pour créer, tester et valider le nouveau code avant de merge les modifications apportées à la base de code principale.


Les Merge Requests Approval garantissent la qualité et la conformité de la révision du code, mais ne contribuent pas à la planification globale ou au suivi des initiatives multi-projets.     


**Atomic commits** impliquent d'effectuer de petites modifications autonomes. Ainsi, si un problème survient, il est possible d'annuler un commit spécifique sans impacter les autres parties du système

**Déploiement continue**

Le déploiement continu (CD) automatise la livraison du code validé vers un environnement de test.      
Le déploiement continu va plus loin et est une pratique dans laquelle chaque modification de code est transmise via un pipeline et est automatiquement mise en production.

Pour pouvoir utiliser la CI/CD avec gitlab il suffit d'avoir à la racine du projet le fichier `.gitlab-ci.yml` qui contient toutes les actions à effectuer pour l'intégration et le déploiement et avoir des **gitlab runners** configurés pour être utilisés





--------------------------------------------------------------------------------------------------------------------     

### GITLAB MERGE

**Merge Trains** garantit que chaque demande de fusion sera testée par rapport à l'état actuel de la branche dans laquelle elle va être fusionnée avant de finaliser la fusion, contribuant ainsi à prévenir les conflits

**Fast-forward Merge** est une méthode qui fait avancer le pointeur de branche s'il n'y a pas de validations divergentes dans la branche en cours de fusion, mais elle ne gère pas les tests préalables à la fusion ni la résolution de conflits dans un scénario multi-branches


>_Remarque_: Il est possible d'obliger une validation par un tier pour les Merge Request sur une branche spécifique. Dans l'exemple ci-dessous, la branche désignée est la branche master         
![gitlab-MergeRequestValidationMandatoryForMasterBranch](./images/gitlab-MergeRequestValidationMandatoryForMasterBranch.png)



--------------------------------------------------------------------------------------------------------------------     

### GITLAB NAMESPACE  / WORKSPACE

Dans GitLab, un **NAMESPACE** est un emplacement unique qui peut être un utilisateur ou un groupe et peut contenir des projets et des sous-groupes, servant de structure d'organisation et de gestion des autorisations

Les **Namespaces** dans GitLab sont cruciaux pour fournir des chemins et des URL uniques pour les projets et les groupes, ce qui facilite l'organisation et l'accès à ces entités sans conflits de noms     

En utilisant des **namespaces**, les organisations peuvent appliquer des configurations CI/CD spécifiques à différents projets ou groupes au sein du même workspace, adaptant ainsi les pratiques d'automatisation et d'intégration pour répondre aux divers besoins des projets.

Les **namespaces** organisent les projets dans GitLab. Chaque namespace étant distinct, vous pouvez utiliser le même nom de projet dans plusieurs namespaces.

.................................................................

Un **workspace** est un environnement sandbox virtuel pour votre code dans GitLab. Vous pouvez utiliser des workspaces pour créer et gérer des environnements de développement isolés pour vos projets GitLab. Ces environnements garantissent que les différents projets n’interfèrent pas les uns avec les autres.

Chaque **workspace** comprend son propre ensemble de dépendances, de bibliothèques et d'outils, que vous pouvez personnaliser pour répondre aux besoins spécifiques de chaque projet.



--------------------------------------------------------------------------------------------------------------------     

### GITLAB PRE-RECEIVE HOOK

Les **Pre-receive hooks** peuvent être configurés pour appliquer diverses politiques, telles que le formatage des messages de validation, ce qui permet de maintenir une base de code cohérente et organisée         




--------------------------------------------------------------------------------------------------------------------     

### GITLAB CI - BENEFICES

<table><tbody><tr><td><strong><span style="font-size:16px">Fast error detection</span></strong></td><td>Également connu sous le nom de <strong>échouer plus rapidement</strong>. Permet aux développeurs de résoudre les problèmes tout en étant frais dans leur esprit. Permet aux équipes d'exécuter leurs suites de tests automatisés qu'elles élaborent autour de notre projet</td></tr><tr><td><strong><span style="font-size:16px">Reduced integration problems</span></strong></td><td>
Diminue les occurrences fréquentes de « ça fonctionne sur ma machine ». Garantit que notre application est déployable et peut s'exécuter non seulement dans l'environnement du développeur, mais également dans un environnement de production</td></tr><tr><td><strong><span style="font-size:16px">Avoids compounding problems</span></strong></td><td>
Permet aux équipes de se développer plus rapidement, avec plus de confiance, réduisant ainsi le risque de code bogué et d'échecs de déploiement</td></tr></tbody></table>


--------------------------------------------------------------------------------------------------------------------     

### GITLAB CD - BENEFICES


<table><tbody><tr><td><strong><span style="font-size:16px">Ensures every change is releasable</span></strong></td><td>Testez tout, y compris le déploiement, avant de terminer.</td></tr><tr><td><strong><span style="font-size:16px">Lowers risk of each release</span></strong></td><td>Lorsque vous publiez plus fréquemment des modifications mineures, vous détectez les erreurs beaucoup plus tôt dans le processus de développement. Et il est plus facile d’annuler des modifications plus petites lorsque vous en avez besoin</td></tr><tr><td><strong><span style="font-size:16px">Delivers value more frequently</span></strong></td><td>Des déploiements fiables signifient plus de versions.  Publier de nouvelles fonctionnalités tôt et souvent : cela signifie que vous recevez des commentaires plus fréquents, ce qui vous donne la possibilité d'apprendre de vos clients.<span style="font-size:16px"><strong><br></strong></span></td></tr><tr><td><span style="font-size:16px"><strong>Tight customer feedback loops</strong></span></td><td>Commentaires rapides et fréquents des clients sur les modifications : la boucle de feedback client fait partie intégrante de la livraison continue, car elle permet aux clients d'obtenir des informations opportunes et précieuses, garantissant que les fonctionnalités et les mises à jour fournies correspondent étroitement à leurs besoins et attentes</td></tr></tbody></table>



![gitlabFormation-CICD_Flow](./images/gitlabFormation-CICD_Flow.png)            

--------------------------------------------------------------------------------------------------------------------     


## GITLAB PACKAGE AND RELEASE FUNCTIONS 

Les packages GitLab permettent aux organisations d'utiliser GitLab comme référentiel privé pour une variété de gestionnaires de packages courants. Les utilisateurs peuvent créer et publier des packages, qui peuvent être facilement utilisés en tant que dépendance dans les projets en aval


**[Doc gitlab - DevSecOps Lifecycle with Gitlab](https://about.gitlab.com/stages-devops-lifecycle/)**             

--------------------------------------------------------------------------------------------------------------------     

### GITLAB PACKAGE AND RELEASE FUNCTIONS

**GitLab's Package Stage**         

GitLab permet aux équipes de regrouper facilement leurs applications et dépendances, de gérer des conteneurs et de créer des artifacts.     
Le private secure container registry et les artifacts repositories sont intégrés et préconfigurés prêts à l'emploi pour fonctionner de manière transparente avec la gestion du code source de GitLab et les pipelines CI/CD. Garantissez l’accélération DevOps avec des pipelines logiciels automatisés qui circulent librement sans interruption


**Enhanced Package Management (gestion améliorée des packages)**       

L'utilisation du système de packages de GitLab permet aux utilisateurs de rechercher et d'utiliser rapidement des artifacts de build, ce qui améliore leur réutilisation dans toute l'organisation.     
Cela permet à toutes les équipes de collaborer plus facilement et de partager les meilleures pratiques afin de minimiser les délais de mise sur le marché et d'améliorer l'efficacité globale



--------------------------------------------------------------------------------------------------------------------     

### GITLAB PACKAGE AND CONTAINER REGISTRIES

**Package Registry**              

Le GitLab Package Registry agit comme un registre privé ou public pour une variété de packages managers courants.     
Les utilisateurs peuvent créer et publier des packages, qui peuvent être facilement utilisés en tant que dépendance dans les projets en aval

Chaque équipe a besoin d'un endroit pour stocker ses packages et dépendances. Avec le GitLab Package Registry, vous pouvez utiliser GitLab comme registre privé ou public pour une variété de gestionnaires de packages pris en charge. Vous pouvez publier et partager des packages, qui peuvent être consommés en tant que dépendance dans les projets en aval

>**Utilisé pour des fichiers**


**Container Registry**

Le GitLab Container Registry est un registre sécurisé et privé pour les images de conteneurs.     
Il est construit sur un logiciel open source et entièrement intégré à GitLab. Utilisez GitLab CI/CD pour créer et publier des images. Utilisez l'API GitLab pour gérer le registre entre les groupes et les projets

Vous pouvez utiliser le Container Registry intégré pour stocker des images de conteneurs pour chaque projet GitLab. La création, le transfert et la récupération d'images fonctionnent immédiatement avec GitLab CI/CD

>**Utilisé pour des images de conteneurs**



**Terraform Module Registry**             

Le Terraform Module Registry GitLab est un registre sécurisé et privé pour les modules Terraform.      
Vous pouvez utiliser GitLab CI/CD pour créer et publier des modules

Avec le Terraform Module Registry, vous pouvez utiliser les projets GitLab comme registre privé pour les modules Terraform. Vous pouvez créer et publier des modules avec GitLab CI/CD, qui peuvent ensuite être consommés à partir d'autres projets privés

>**Utilisé pour les modules Terraform**     


**Dependency Proxy**       

Le proxy de dépendance est un proxy local pour les images et packages en amont fréquemment utilisés

Le proxy de dépendance met en cache à la fois le manifeste et les blobs pour une image donnée. Ainsi, lorsque vous la demandez à nouveau, Docker Hub n'a pas besoin d'être contacté


>**Proxy local pour les images et packages fréquemment utilisés**


--------------------------------------------------------------------------------------------------------------------     

### RELEASES GITLAB

<table style="width:100%;"><thead><tr><th style="width:33.3333%;background-color:rgb(255, 99, 30);"><span style="color:rgb(255, 255, 255);font-weight:bold;"><strong>A release can include...</strong></span></th><th style="width:33.3333%;background-color:rgb(255, 99, 30);"><span style="color:rgb(255, 255, 255);font-weight:bold;">When you </span><a href="https://docs.gitlab.com/ee/user/project/releases/#create-a-release" rel="noopener noreferrer" target="_blank"><span style="color:rgb(255, 255, 255);font-weight:bold;">create a release</span><span class="visually-hidden-always"></span></a><span style="color:rgb(255, 255, 255);font-weight:bold;">...</span></th><th style="width:33.3333%;background-color:rgb(255, 99, 30);"><span style="color:rgb(255, 255, 255);font-weight:bold;">After a release you can...</span></th></tr></thead><tbody><tr><td style="text-align:center;width:33.3333%;"><ul><li style="text-align:left;">Un snapshot du code source de votre référentiel</li><li style="text-align:left;"><a href="https://docs.gitlab.com/ee/user/packages/generic_packages/index.html" rel="noopener noreferrer" target="_blank">Packages génériques<span class="visually-hidden-always"></span></a> créés à partir de jobs artifacts</li><li style="text-align:left;">Autres métadonnées associées à une version publiée de votre code</li><li style="text-align:left;">Release notes.</li></ul></td><td style="text-align:center;width:33.3333%;"><ul><li style="text-align:left;">GitLab archive automatiquement le code source et l'associe à la version</li><li style="text-align:left;">GitLab crée automatiquement un fichier JSON qui répertorie tout ce qui se trouve dans la version, afin que vous puissiez comparer et auditer les versions. Ce fichier s'appelle <a href="https://docs.gitlab.com/ee/user/project/releases/release_evidence.html" rel="noopener noreferrer" target="_blank">release evidence<span class="visually-hidden-always"></span></a>.</li></ul></td><td style="text-align:center;width:33.3333%;"><ul><li style="text-align:left;">Ajouter des release notes.</li><li style="text-align:left;">Ajoutez un message pour le Git tab associé à la version</li><li style="text-align:left;"><a href="https://docs.gitlab.com/ee/user/project/releases/#associate-milestones-with-a-release" rel="noopener noreferrer" target="_blank">Associez-lui des jalons (milestones)<span class="visually-hidden-always"></span></a>.</li><li style="text-align:left;"><a href="https://docs.gitlab.com/ee/user/project/releases/release_fields.html#release-assets" rel="noopener noreferrer" target="_blank">Attacher des éléments de version (release assets)<span class="visually-hidden-always"></span></a>, comme des runbooks ou des packages.</li></ul></td></tr></tbody></table>



Le déploiement est l'étape du processus de livraison de logiciels lorsque votre application est déployée sur son infrastructure cible finale. 

Vous pouvez déployer votre application en interne ou auprès du public.     
Vous pouvez également prévisualiser une version dans une review app et utiliser des indicateurs de fonctionnalités (feature flags) pour publier des fonctionnalités de manière incrémentielle


**[Review App](https://docs.gitlab.com/ee/ci/review_apps/)**             

Les Review Apps sont un outil de collaboration qui fournit un environnement permettant de présenter les modifications apportées aux produits. Si vous disposez d'un cluster Kubernetes, vous pouvez automatiser cette fonctionnalité dans vos applications à l'aide d'Auto DevOps


Une Gitlab Review App doit avoir: 
 - Automatic Live Preview
   - Code, commit et preview de votre branche dans un environnement réel. Les Review App lancent automatiquement des environnements dynamiques pour vos Merge Requests 
 - One-click to Collaborate
   - Designers et product managers n'auront pas besoin de vérifier votre branche et de l'exécuter dans un environnement de test. Envoyez simplement un lien à l'équipe et laissez-la cliquer
 - Fully-Integrated 
   - Avec GitLab code review, le built-in CI/CD et les review Apps, vous pouvez accélérer votre processus de développement avec un seul outil pour coder, tester et prévisualiser vos modifications
 - Deployment Flexibility
   - Déployez sur Kubernetes, Heroku, FTP ou autres. Vous pouvez déployer n'importe où avec `.gitlab-ci.yml` et vous avez le contrôle total pour déployer autant de types de review app que votre équipe en a besoin



**[Feature Flags](https://docs.gitlab.com/ee/operations/feature_flags.html)**        

Avec les Feature Flags (indicateurs de fonctionnalités), vous pouvez déployer les nouvelles fonctionnalités de votre application en production par lots plus petits. Vous pouvez activer et désactiver une fonctionnalité pour des sous-ensembles d'utilisateurs, ce qui vous aide à réaliser une livraison continue. Les Feature Flags aident à réduire les risques, vous permettant d'effectuer des tests contrôlés et de séparer la fourniture de fonctionnalités du lancement client


**[Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html)**     

GitLab Pages est une fonctionnalité qui vous permet de publier des sites Web statiques directement à partir d'un référentiel dans GitLab.

Vous pouvez l'utiliser pour des sites Web personnels ou professionnels, tels que des portfolios, de la documentation, des manifestes et des présentations professionnelles. Vous pouvez également attribuer n'importe quelle licence à votre contenu.

Pages ne prend pas en charge le traitement dynamique côté serveur



**[Release Evidence](https://docs.gitlab.com/ee/user/project/releases/release_evidence.html)**        

Chaque fois qu'une version est créée, GitLab prend un snapshot des données qui y sont liées. Ces données sont enregistrées dans un fichier JSON appelé Release Evidence. La fonctionnalité comprend des artifacts de test et des milestones liés pour faciliter les processus internes, comme les audits externes.

Pour accéder aux Release Evidences, sur la page Releases, sélectionnez le lien vers le fichier JSON répertorié sous l'en-tête `Evidence collection`

Vous pouvez également utiliser l'API pour générer des Release Evidences pour une version existante. Pour cette raison, chaque version peut avoir plusieurs snapshots de Release Evidence. Vous pouvez consulter les Release Evidence et leurs détails sur la page Releases


Les group milestones permettent de suivre les délais sur plusieurs projets au sein d'un groupe, mais ne disposent pas de la capacité structurelle nécessaire pour gérer des dépendances complexes et des déploiements progressifs sur plusieurs années


--------------------------------------------------------------------------------------------------------------------     

### GITLAB AND AUTODEVOPS 

[Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) vous offre une configuration CI/CD prédéfinie qui vous permet de détecter, créer, tester, déployer et surveiller automatiquement vos applications. Cela facilite la mise en place de chaque projet de manière plus cohérente.

Il est activé par défaut pour tous vos projets mais peut être désactivé par votre administrateur au niveau de l'instance. Il peut être désactivé et activé par les utilisateurs de GitLab.com au niveau du projet, et les utilisateurs autogérés peuvent également l'activer au niveau du groupe ou de l'instance




--------------------------------------------------------------------------------------------------------------------     

## GITLAB SECURITY SCANNING

Alors que la sécurité des logiciels devient de plus en plus importante, de nombreuses entreprises souhaitent introduire des processus standard d'analyse de code dans les flux de développement afin de détecter et de corriger les vulnérabilités de sécurité avant qu'elles ne passent en production. La plateforme DevSecOps de GitLab permet aux utilisateurs d'effectuer des analyses de sécurité dans les pipelines CI/CD, qui peuvent facilement être activées pour vérifier les applications pour détecter les vulnérabilités de sécurité telles que les accès non autorisés, les fuites de données et les attaques par déni de service (DoS)

[Doc gitlab - Applications security](https://docs.gitlab.com/ee/user/application_security/)            

**Secuirty Scanners**          

Liste des scan de sécurité fournis par Gitlab:
 - **Static Application Security Testing (SAST)**: recherche les problèmes connus dans le code source     
 - **Secret Detection**: recherche les secrets codés en dur dans le code source    
 - **Dynamic Application Security Testing (DAST)**: appelle passivement ou activement une application Web ou une API pour rechercher des vulnérabilités de sécurité    
 - **Dependency Scanning**: examine les dépendances du projet pour voir s'il existe des vulnérabilités connues avec ces versions (par exemple, bibliothèque d'analyse YAML tierce)    
 - **Container Scanning**: examine les images Docker de votre projet et recherche les vulnérabilités connues    
 - **Fuzz Testing**: envoie une entrée aléatoire à vos fonctions, essayant de provoquer des problèmes inattendus (par exemple, un mot de passe de 200 caractères ou des caractères klingons en Unicode...)     
 - **Infrastructure-as-Code (IaC) Scanning**: analyse les fichiers de configuration IaC (par exemple, Ansible, Terraform) à la recherche de vulnérabilités de sécurité connues    


**Security Support Features**   

Liste des focntionalités de sécurité fournis par Gitlab:
 - **Security Reports**: montre les vulnérabilités de sécurité à différents endroits et de différentes manières         
 - **Vulnerability Management**: vous permet de visualiser les problèmes à plusieurs endroits dans gitlab et d'accepter/rejeter/marquer pour action    
 - **Policies**: fournit aux équipes de sécurité un moyen d'exiger que les analyses de leur choix soient exécutées chaque fois qu'un pipeline de projet s'exécute selon la configuration spécifiée     


>  Lors de la configuration d'un scanner de sécurité, la définition de policies permet d'indiquer au scanner ce qu'il doit signaler et ce qu'il doit ignorer


--------------------------------------------------------------------------------------------------------------------     
